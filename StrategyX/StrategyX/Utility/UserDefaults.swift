//
//  UserDefaults.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserDefault: NSObject {
    static let shared = UserDefault()
    var userRole = UserRole.superAdminUser
    let key_userModel = "userModel"
    let key_stayIn = "stayIn"
    let key_touchData = "touchData"
    let key_LastUserLoginData = "lastLoginUser"

    //static var isAppLoaded = false
    let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    func isArchiveAction(strStatus:String) -> Bool {
        if strStatus.lowercased() == APIKey.key_archived.lowercased() {
            return true
        }
        return false
    }
    func isIdeaAndProblemArchive(strStatus:String) -> Bool {
        return (strStatus.lowercased() == APIKey.key_archived.lowercased()) || (strStatus.lowercased() == "fixed".lowercased()) || (strStatus.lowercased() == "completed".lowercased())
    }
    func isCanEditForm(strOppoisteID:String) -> Bool {
        return userRole == .superAdminUser || userRole == .companyAdminUser || (userRole == .staffUser && Utility.shared.userData.id == strOppoisteID)
    }
    func isCanArchiveForm(strOppoisteID:String,completionFlag:Int) -> Bool {
        return (userRole == .superAdminUser || userRole == .companyAdminUser || (userRole == .staffUser && Utility.shared.userData.id == strOppoisteID)) && completionFlag == 1
    }
    func isCanRequestForDueDate(strAssignedID:String, strApprovedID:String, strCreatedBy:String) -> Bool {
        return Utility.shared.userData.id == strAssignedID && Utility.shared.userData.id != strApprovedID && Utility.shared.userData.id != strCreatedBy
    }
    func isCanApproveRequestForDueDate(strApprovedID:String) -> Bool {
        return Utility.shared.userData.id == strApprovedID
    }
    func isCanApproveGoal(strAssignedID:String) -> Bool {
        return (userRole == .superAdminUser || userRole == .companyAdminUser || (userRole == .staffUser && Utility.shared.userData.id == strAssignedID))
    }
    func isHiddenProfileForStaffUser(strAssignedID:String) -> Bool {
        return Utility.shared.userData.id == strAssignedID && userRole == .staffUser
    }
    class func setValueForKey(key: String, value: Any) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func removeValueForKey(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func valueForKey(key: String) -> Any? {
        return UserDefaults.standard.value(forKey: key)
    }
    
    class func setValuesFromDict(theDict: [String: Any]) {
        if theDict.count > 0 {
            for (key,value) in theDict {
                UserDefaults.standard.set(value, forKey: key)
            }
            
            UserDefaults.standard.synchronize()
        }
    }
    /*
    class func currentAccessToken() -> String {
        if UserDefaults.standard.value(forKey: APIKeys.KEY_ACCESS_TOKEN) != nil {
            return String(describing: UserDefaults.standard.value(forKey: APIKeys.KEY_ACCESS_TOKEN) as! String)
        } else {
            return ""
        }
    }
    
    class func currentUserID() -> String {
        if UserDefaults.standard.value(forKey: APIKeys.KEY_USER_ID) != nil {
            return String(describing: UserDefaults.standard.value(forKey: APIKeys.KEY_USER_ID) as! String)
        } else {
            return ""
        }
    }
    
    class func buildVersion() -> String {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        return version
    }
    
    class func deviceToken() -> String {
        if let deviceToken = UserDefault.valueForKey(key: APIKeys.KEY_DEVICE_TOKEN) as? String {
            return deviceToken
        }
        
        return ""
    }*/
    
    func updateUserData() {
        guard let userDatafrom = Utility.shared.userData else { return }
        let userdata = JSON(userDatafrom.toJSON())
        removeUserData()
        saveUserData(user: userdata)
    }
    
    func getUserData() {
        if let user  = Default.object(forKey: key_userModel) as? Data, let json = JSON(user).dictionaryObject {
            Utility.shared.userData = User(JSON: json)
        }
    }
    func saveUserData(user:JSON) {
        guard let rawData = try? user.rawData() else { return }
        if let data = user.dictionaryObject {
            Utility.shared.userData = User(JSON: data)
        }
        Default.set(rawData, forKey: key_userModel)
        Default.synchronize()
    }
    func removeUserData() {
        Utility.shared.userData = nil
        Default.removeObject(forKey: key_userModel)
        Default.synchronize()
    }

    // === StayIn ===
    func getStayInUserData() -> StayInUser? {
        if let user  = Default.object(forKey: key_stayIn) as? Data, let json = JSON(user).dictionaryObject {
            return StayInUser(JSON: json)
        }
        return nil
    }
    
    func saveStayInUserData(user:JSON) {
        guard let rawData = try? user.rawData() else { return }
        Default.set(rawData, forKey: key_stayIn)
        Default.synchronize()
    }
    func removeStayInUserData() {
        Default.removeObject(forKey: key_stayIn)
        Default.synchronize()
    }
    
    // === Touch/FaceID Login ===
    func getTouchUserData() -> StayInUser? {
        if let user  = Default.object(forKey: key_touchData) as? Data, let json = JSON(user).dictionaryObject {
            return StayInUser(JSON: json)
        }
        return nil
    }
    
    func saveTouchUserData(user:JSON) {
        guard let rawData = try? user.rawData() else { return }
        Default.set(rawData, forKey: key_touchData)
        Default.synchronize()
    }
    func removeTouchUserData() {
        Default.removeObject(forKey: key_touchData)
        Default.synchronize()
    }
    
    // === Last User Login ===
    func getLastUserLoginData() -> StayInUser? {
        if let user  = Default.object(forKey: key_LastUserLoginData) as? Data, let json = JSON(user).dictionaryObject {
            return StayInUser(JSON: json)
        }
        return nil
    }
    
    func saveLastUserLoginData(user:JSON) {
        guard let rawData = try? user.rawData() else { return }
        Default.set(rawData, forKey: key_LastUserLoginData)
        Default.synchronize()
    }
    func removeLastUserLoginData() {
        Default.removeObject(forKey: key_LastUserLoginData)
        Default.synchronize()
    }
}
