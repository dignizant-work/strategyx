//
//  ParentViewController.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import LinearProgressBarMaterial
import SDWebImage

class ParentViewController: UIViewController {
    
//    var linearBar: LinearProgressBar?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func setupDelegateAndDataSourceForCollectionView(theObject: UICollectionView) {
        
    }
    
    func setupDelegateAndDataSourceForTableView(theObject: UITableView) {
        
    }
    /*
    //MARK:- Show Loader
    func showLoader() {
        linearBar = LinearProgressBar()
        linearBar?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 5)
        configureLinearProgressBar()
        self.view.addSubview(linearBar!)
        linearBar?.startAnimation()
    }
    func configureLinearProgressBar() {
        linearBar?.backgroundColor = UIColor.white
        linearBar?.progressBarColor = appGreen
        linearBar?.heightForLinearBar = 5
    }
    func hideLoader() {
        linearBar?.stopAnimation()
    }*/
    
    // MARK: - IQKeyboardManager
    final func enableIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
    }
    
    final func disableIQKeyboardManager() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
    
    //MARK:- Sidemenu
    final func setSideMenuLeftNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_green_icon")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.addLeftGestures()
    }
    final func setSideMenuRightNavigationBarItem() {
        self.addRightBarButtonWithImage(UIImage(named: "ic_menu_green_icon")!)
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addRightGestures()
    }
    final func removeSideMenuLeftNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
    }
    final func removeSideMenuRightNavigationBarItem() {
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeRightGestures()
    }
    final func setSideMenuBothNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_green_icon")!)
        self.addRightBarButtonWithImage(UIImage(named: "ic_menu_green_icon")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    final func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    //MARK:- BackButton With Title
    final func setBackButtonWithLeftTitle(strTitle:String) {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 210, height: 40))
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.frame = CGRect.init(x: -5, y: 2.5, width: 35, height: 38)
        button.setImage(UIImage.init(named: "ic_right_arrow"), for: .normal)
        button.addTarget(self, action: #selector(rightBackAction), for: .touchUpInside)
        let lbl = UILabel(frame: CGRect.init(x: 35, y: 2.5, width: 175, height: 35))
        lbl.setFont = "B21"
        lbl.text = strTitle
        lbl.setFontColor = 4
        lbl.textAlignment = .left
        view.addSubview(button)
        view.addSubview(lbl)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
    }
    
    @objc func rightBackAction() {
        self.navigationController?.popViewController(animated: false)
    }
    
    deinit {
        print("\(self) DEALLOCATED")
    }
    
}
//MARK:- UIScrollViewDelegate
extension ParentViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -48 { //change 40 to whatever you want
            if  !((self.view as? ViewParentWithoutXIB)?.refreshControl.isRefreshing ?? true) {
                (self.view as? ViewParentWithoutXIB)?.refresh()
            }
        }
    }
}
