//
//  ViewParentWithoutXIB.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage

class ViewParentWithoutXIB: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = appGreen
//        refreshControl.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()

    //MARk:- RefreshController
    @objc func refresh() {
//        refreshControl.beginRefreshing()
    }
    
    // MARK: - View Lifecycle
    deinit {
        print("\(self) DEALLOCATED")
    }
    
    override func addSubview(_ view: UIView) {
        if view.isDescendant(of: self) {
            return
        }
        
        super.addSubview(view)
    }
    
    // MARK: - UI Methods
    func toggleAnyView<T: UIView>(duration: Double, anyView: [T], isHidden: Bool) {
        UIView.animate(withDuration: TimeInterval(duration)) {
            for theView in anyView {
                theView.isHidden = isHidden
            }
        }
    }

}
/*
extension ViewParentWithoutXIB:UIScrollViewDelegate {
    /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -50 { //change 80 to whatever you want
            if  !self.refreshControl.isRefreshing {
                self.refresh()
            }
        }
    }*/
}
*/
