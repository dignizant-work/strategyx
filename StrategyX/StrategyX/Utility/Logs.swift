//
//  Logs.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation

// Usage : In preprocessor macros,
//         Debug = $(inherited) COCOAPODS=1
//         Release = COCOAPODS=1

open class Logs {
    // It will print message, function and line number.
    open class func dLog(_ message: String, _ callingFunction: String = #function, _ line: Int = #line, _ filename: String = #file) {
        #if DEBUG
        print("\(message),\n FUNCTION: \(callingFunction),\n LINE: \(line),\n FILENAME: \(filename)\n")
        #endif
    }
    
    // It will work as Print fuction.
    open class func prints(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        #if DEBUG
        print(items, separator: separator, terminator: terminator)
        #endif
    }
}
