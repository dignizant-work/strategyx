//
//  Validation.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation

struct Validation {
    func isConnectedToInternet() -> Bool {
        return NetworkConnectivity().isInternetConnection()
    }
    static func isValidUserName(strUserName: String?) -> Bool {
        if strUserName == nil {
            return false
        }
        
        let usernameRegEx = "[a-zA-Z0-9_\\p{Arabic}\\p{N}]"
        // "[A-Z0-9a-z._]"
        let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        
        return usernameTest.evaluate(with: strUserName)
    }
    static func isNumericNumber(strNumber: String) -> Bool {
        let numberRegEx = "\\d+(?:\\.\\d+)?"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        return numberTest.evaluate(with:strNumber)
    }
    
    static func isValidEmailAddress(strEmail: String?) -> Bool {
        if strEmail == nil {
            return false
        }
        
        //        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailRegEx = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: strEmail)
    }
    
    static func isStringEmpty(_ theString: String?) -> Bool {
        if theString != nil {
            let str = theString!.trimmingCharacters(in: .whitespaces)
            
            if str.count == 0 {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    static func isPasswordValid(_ theString: String?) -> Bool {
        
        if theString != nil {
            let str = theString ?? ""
            print(str)
            if str.count > 5 {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    static func isValidyoutubeURL(theString:String?) -> (Bool,String?) {
        if theString == nil {
            return (false,nil)
        }
        let youtubeRegEx = "(https?://)(www\\.)?(m.)?youtu(be|.be)?(.com)?(/|/embed/|/v/|/watch\\?v=|/watch\\?.+&v=)([\\w_-]{11})(&.+)?"
        let youtubeTest = NSPredicate(format: "SELF MATCHES %@", youtubeRegEx)
        return (youtubeTest.evaluate(with: theString!),"")
        
    }
    /*
    static func isValidURL(_ theString: String?) -> Bool {
        if theString == nil {
            return false
        }
        
        if let url = URL(string: theString!) {
            return UIApplication.shared.canOpenURL(url)
        }
        
        return false
    }*/
    
    static func isValidPhoneNumber(_ theString: String?) -> Bool {
        if theString != nil {
            if theString!.count >= 8 {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
        
        /*
         if theString != nil {
         let PHONE_REGEX = "[0-9]{10}"
         let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
         let result =  phoneTest.evaluate(with: theString!)
         return result
         } else {
         return false
         }*/
    }
    
    static func isConfirmPasswordMatches(strPassword: String?, strConfirmPassword: String?) -> Bool {
        if strPassword == nil || strConfirmPassword == nil {
            return false
        }
        
        if strPassword! == strConfirmPassword! {
            return true
        } else {
            return false
        }
    }
    
    static func stringValueForTheObject(theValue: Any?) -> String {
        var strValue: String = ""
        
        guard theValue != nil else {
            return strValue
        }
        
        guard !(theValue is NSNull) else {
            return strValue
        }
        
        if ((theValue as? NSNumber) != nil) {
            let theInt = Int(truncating: theValue as! NSNumber)
            strValue  = String(theInt)
        } else {
            if ((theValue as? String) != nil) {
                strValue = theValue as! String
            }
        }
        
        return strValue
    }
    /*
    static func theLanguageKey() -> String {
        return (MOLHLanguage.isArabic() ? "1" : "0")
    }*/
    
    static func theTimeZone() -> String {
        return TimeZone.current.identifier
    }
    
    static func convertAnyToString(obj:Any??) -> String {
        
        if let converted = obj as? String {
            return converted
        } else if let converted = obj as? Int {
            return String(converted)
        } else if let converted = obj as? Float {
            return String(converted)
        } else if let converted = obj as? Double {
            return String(converted)
        }
        return ""
    }
    
}

struct ValidationTexts {
    static let NO_INTERNET_CONNECTION   = "No internet connection".localize
    static let SOMETHING_WENT_WRONG     = "Something went wrong".localize
    static let REQUEST_TIME_OUT         = "Request time out, Please try again.".localize
    static let text_http                = "http://"
    static let NO_NAME                  = "Please enter name".localize
    static let NO_USERNAME              = "Please enter user name".localize
    static let NO_EmailUSERNAME         = "Please enter email or username".localize
    static let NO_WEBSITE               = "Please enter website URL".localize
    static let NO_VALID_WEBSITE         = "Please enter valid website URL".localize
    static let NO_BIO_DESCRIPTION       = "Please enter description".localize
    static let NO_EMAIL                 = "Please enter email".localize
    static let NO_Valid_EMAIL           = "Please enter valid email address".localize
    static let NO_PHONE_NUMBER          = "Please enter phone".localize
    static let NO_VALID_PHONE_NUMBER    = "Please enter valid phone".localize
    static let NO_MOBILE_NUMBER         = "Please enter mobile number".localize
    static let NO_VALID_MOBILE_NUMBER   = "Please enter valid mobile number".localize
    static let NO_VALID_PASSWORD        = "Password must be at least 6 character long".localize
    static let NO_PASSWORD              = "Please enter password".localize
    static let NO_CONFIRM_PASSWORD      = "Password does not match with confirm password".localize
    static let NO_CURRENT_PASSWORD      = "Please enter current password".localize
    static let NO_VALID_CURRENT_PASSWORD = "Current password must be at least 6 character long".localize
    static let NO_NEW_PASSWORD          = "Please enter new password".localize
    static let NO_VALID_NEW_PASSWORD    = "New password must be at least 6 character long".localize
    static let NO_VALID_NEW_CURRENT_PASSWORD = "New password and confirmation of the new password do not match.".localize
    static let NO_VALID_NEW_AND_CURRENT_PASSWORD_DIFFRENT = "Please enter new password different than current password".localize

}

struct PlaceholderTexts {
    
}
