//
//  Utility.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar
import MaterialComponents.MaterialSnackbar_ColorThemer
import SwiftyJSON
import AVKit

class Utility: NSObject {
    private static var instant:Utility?
    
    class var shared : Utility {
        guard let sharedInstance = self.instant else {
            let sharedInstance = Utility()
            self.instant = sharedInstance
            return sharedInstance
        }
        return sharedInstance
    }
    var playerViewController:AVPlayerViewController?

    var userData:User!
    var appCurrentVersion = ""
    
    let manager = MDCSnackbarManager()
    let message = MDCSnackbarMessage()
    
    var getAllCompany:[Company] = []
    var loginData:LoginData?
    var pushData = PushCount()
    var pushNotificationData:PushNotificationData?
    
    
    override init() {
        manager.snackbarMessageViewBackgroundColor = black
        manager.messageFont = R14
    }
    
    func setArrayNotesHistoryStatus() -> [JSON] {
        var arr_noteHistoryStatus:[JSON] = []
        
        var dict = JSON()
        dict["name"].stringValue = APIKey.key_All
        dict["status"] = ""
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Assigned_To
        dict["status"] = "assigned"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_completed
        dict["status"] = "percentage"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_posted_notes
        dict["status"] = "commented"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_revised_due_date
        dict["status"] = "revised"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Work_Date
        dict["status"] = "worked"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Opened_an_action
        dict["status"] = "opened"
        arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Closed_an_action
        dict["status"] = "closed"
        arr_noteHistoryStatus.append(dict)
        
        return arr_noteHistoryStatus
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func setColorBGAccordingToActions(type:String) -> (Int,String) {
        var color = 36
        var typeName = "Action"
        
        switch type.lowercased() {
        case "0": // ("Action",0)
            color = 2
            typeName = "Action"
            break
        case "1": // ("Strategy",1)
            color = 37
            typeName = "Strategy"
            break
        case "2": // ("Risk",2)
            color = 39
            typeName = "Risk"
            break
        case "3": // ("Tactical Project",3)
            color = 36
            typeName = "Project"
            break
        case "4": // ("Focus",4)
            color = 38
            typeName = "Focus"
            break
        case "5":
            color = 35
            typeName = "Recurring"
            break
        case "6": // ("Success Factor",6)
            color = 38
            typeName = "Success"
            break
        case "7": // ("Idea",7)
            color = 40
            typeName = "Idea"
            break
        case "8": // ("Problem",8)
            color = 41
            typeName = "Problem"
            break
        default:
            typeName = "Action"
            color = 36
            break
        }
        return (color,typeName)
    }
    func getStageList() -> [(String,String)] {
        return [("All","all"), ("Submitted","submitted"),("Accepted","accepted"),("Rejected","rejected"), ("Fixing","fixing"), ("On Hold","on_hold"),("Completed","completed"),("Fixed","fixed")]
    }
    func getTypeOfSX() -> [(String,Int)] {
        return [("All",-1),("Strategy",1),("Success Factor",6),("Risk",2),("Tactical Project",3),("Idea",7),("Problem",8),("Focus",4),("Action",0)]
    }
    
    //MARK:- Activity
    func showActivityIndicator() {
        let uiView = UIView(frame: UIScreen.main.bounds)
        let container: UIView = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = black.withAlphaComponent(0.3)
//            UIColorFromHex(0xffffff, alpha: 0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
//            CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = white.withAlphaComponent(0.7)
//            UIColorFromHex(0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40.0, height: 40.0)
//            CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.color = appGreen
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
//            CGPointMake(loadingView.frame.size.width / 2,
//                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        uiView.tag = 8956
        actInd.startAnimating()
        
        AppDelegate.shared.window?.addSubview(uiView)
    }
    
    func stopActivityIndicator() {
        if let view = AppDelegate.shared.window?.subviews.first(where: {$0.tag == 8956}) {
            view.isHidden = true
            view.removeFromSuperview()
        }
    }
    
    
    //MARK:- Bottom Message
    func showAlertAtBottom(strMessage:String) {
        message.text = strMessage
        manager.show(message)
    }
    
    func dispose() {
        Utility.instant = nil
    }
    deinit {
        print("Dealocate \(self)")
    }
}
