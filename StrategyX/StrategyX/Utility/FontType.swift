//
//  FontType.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

struct FontStyle {
    let bold            = "Lato-Bold"
    let light           = "Lato-Light"
    let semibold        = "Lato-Semibold"
    let regular         = "Lato-Regular"
    
    func setFontType(strType:String) -> UIFont? {
        switch strType {
        case "R9":
            return R9
        case "R10":
            return R10
        case "R11":
            return R11
        case "R12":
            return R12
        case "R13":
            return R13
        case "R14":
            return R14
        case "R15":
            return R15
        case "R16":
            return R16
        case "R17":
            return R17
        case "R18":
            return R18
        case "R19":
            return R19
        case "R20":
            return R20
        case "R21":
            return R21
        case "R22":
            return R22
        case "R23":
            return R23
        case "R24":
            return R24
        case "R25":
            return R25
        case "R26":
            return R26
        case "R27":
            return R27
          
        case "B5":
            return B5
        case "B8":
            return B8
        case "B10":
            return B10
        case "B11":
            return B11
        case "B12":
            return B12
        case "B13":
            return B13
        case "B14":
            return B14
        case "B15":
            return B15
        case "B16":
            return B16
        case "B17":
            return B17
        case "B18":
            return B18
        case "B19":
            return B19
        case "B20":
            return B20
        case "B21":
            return B21
        case "B22":
            return B22
        case "B23":
            return B23
        case "B24":
            return B24
        case "B25":
            return B25
        case "B26":
            return B26
        case "B27":
            return B27
            
        case "L10":
            return L10
        case "L11":
            return L11
        case "L12":
            return L12
        case "L13":
            return L13
        case "L14":
            return L14
        case "L15":
            return L15
        case "L16":
            return L16
        case "L17":
            return L17
        case "L18":
            return L18
        case "L19":
            return L19
        case "L20":
            return L20
        case "L21":
            return L21
        case "L22":
            return L22
        case "L23":
            return L23
        case "L24":
            return L24
        case "L25":
            return L25
        case "L26":
            return L26
        case "L27":
            return L27
            
        case "S10":
            return S10
        case "S11":
            return S11
        case "S12":
            return S12
        case "S13":
            return S13
        case "S14":
            return S14
        case "S15":
            return S15
        case "S16":
            return S16
        case "S17":
            return S17
        case "S18":
            return S18
        case "S19":
            return S19
        case "S20":
            return S20
        case "S21":
            return S21
        case "S22":
            return S22
        case "S23":
            return S23
        case "S24":
            return S24
        case "S25":
            return S25
        case "S26":
            return S26
        case "S27":
            return S27
        
            
        default:
            return nil
        }
    }
    
}
let R6  = UIFont(name: FontStyle().regular, size: 6.0)!
let R7  = UIFont(name: FontStyle().regular, size: 7.0)!
let R8  = UIFont(name: FontStyle().regular, size: 8.0)!
let R9  = UIFont(name: FontStyle().regular, size: 9.0)!
let R10 = UIFont(name: FontStyle().regular, size: 10.0)!
let R11 = UIFont(name: FontStyle().regular, size: 11.0)!
let R12 = UIFont(name: FontStyle().regular, size: 12.0)!
let R13 = UIFont(name: FontStyle().regular, size: 13.0)!
let R14 = UIFont(name: FontStyle().regular, size: 14.0)!
let R15 = UIFont(name: FontStyle().regular, size: 15.0)!
let R16 = UIFont(name: FontStyle().regular, size: 16.0)!
let R17 = UIFont(name: FontStyle().regular, size: 17.0)!
let R18 = UIFont(name: FontStyle().regular, size: 18.0)!
let R19 = UIFont(name: FontStyle().regular, size: 19.0)!
let R20 = UIFont(name: FontStyle().regular, size: 20.0)!
let R21 = UIFont(name: FontStyle().regular, size: 21.0)!
let R22 = UIFont(name: FontStyle().regular, size: 22.0)!
let R23 = UIFont(name: FontStyle().regular, size: 23.0)!
let R24 = UIFont(name: FontStyle().regular, size: 24.0)!
let R25 = UIFont(name: FontStyle().regular, size: 25.0)!
let R26 = UIFont(name: FontStyle().regular, size: 26.0)!
let R27 = UIFont(name: FontStyle().regular, size: 27.0)!

let B5 = UIFont(name: FontStyle().bold, size: 5.0)!
let B8 = UIFont(name: FontStyle().bold, size: 8.0)!
let B10 = UIFont(name: FontStyle().bold, size: 10.0)!
let B11 = UIFont(name: FontStyle().bold, size: 11.0)!
let B12 = UIFont(name: FontStyle().bold, size: 12.0)!
let B13 = UIFont(name: FontStyle().bold, size: 13.0)!
let B14 = UIFont(name: FontStyle().bold, size: 14.0)!
let B15 = UIFont(name: FontStyle().bold, size: 15.0)!
let B16 = UIFont(name: FontStyle().bold, size: 16.0)!
let B17 = UIFont(name: FontStyle().bold, size: 17.0)!
let B18 = UIFont(name: FontStyle().bold, size: 18.0)!
let B19 = UIFont(name: FontStyle().bold, size: 19.0)!
let B20 = UIFont(name: FontStyle().bold, size: 20.0)!
let B21 = UIFont(name: FontStyle().bold, size: 21.0)!
let B22 = UIFont(name: FontStyle().bold, size: 22.0)!
let B23 = UIFont(name: FontStyle().bold, size: 23.0)!
let B24 = UIFont(name: FontStyle().bold, size: 24.0)!
let B25 = UIFont(name: FontStyle().bold, size: 25.0)!
let B26 = UIFont(name: FontStyle().bold, size: 26.0)!
let B27 = UIFont(name: FontStyle().bold, size: 27.0)!

let L10 = UIFont(name: FontStyle().light, size: 10.0)!
let L11 = UIFont(name: FontStyle().light, size: 11.0)!
let L12 = UIFont(name: FontStyle().light, size: 12.0)!
let L13 = UIFont(name: FontStyle().light, size: 13.0)!
let L14 = UIFont(name: FontStyle().light, size: 14.0)!
let L15 = UIFont(name: FontStyle().light, size: 15.0)!
let L16 = UIFont(name: FontStyle().light, size: 16.0)!
let L17 = UIFont(name: FontStyle().light, size: 17.0)!
let L18 = UIFont(name: FontStyle().light, size: 18.0)!
let L19 = UIFont(name: FontStyle().light, size: 19.0)!
let L20 = UIFont(name: FontStyle().light, size: 20.0)!
let L21 = UIFont(name: FontStyle().light, size: 21.0)!
let L22 = UIFont(name: FontStyle().light, size: 22.0)!
let L23 = UIFont(name: FontStyle().light, size: 23.0)!
let L24 = UIFont(name: FontStyle().light, size: 24.0)!
let L25 = UIFont(name: FontStyle().light, size: 25.0)!
let L26 = UIFont(name: FontStyle().light, size: 26.0)!
let L27 = UIFont(name: FontStyle().light, size: 27.0)!

let S10 = UIFont(name: FontStyle().semibold, size: 10.0)!
let S11 = UIFont(name: FontStyle().semibold, size: 11.0)!
let S12 = UIFont(name: FontStyle().semibold, size: 12.0)!
let S13 = UIFont(name: FontStyle().semibold, size: 13.0)!
let S14 = UIFont(name: FontStyle().semibold, size: 14.0)!
let S15 = UIFont(name: FontStyle().semibold, size: 15.0)!
let S16 = UIFont(name: FontStyle().semibold, size: 16.0)!
let S17 = UIFont(name: FontStyle().semibold, size: 17.0)!
let S18 = UIFont(name: FontStyle().semibold, size: 18.0)!
let S19 = UIFont(name: FontStyle().semibold, size: 19.0)!
let S20 = UIFont(name: FontStyle().semibold, size: 20.0)!
let S21 = UIFont(name: FontStyle().semibold, size: 21.0)!
let S22 = UIFont(name: FontStyle().semibold, size: 22.0)!
let S23 = UIFont(name: FontStyle().semibold, size: 23.0)!
let S24 = UIFont(name: FontStyle().semibold, size: 24.0)!
let S25 = UIFont(name: FontStyle().semibold, size: 25.0)!
let S26 = UIFont(name: FontStyle().semibold, size: 26.0)!
let S27 = UIFont(name: FontStyle().semibold, size: 27.0)!


