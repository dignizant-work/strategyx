//
//  GlobalStoryboards.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryBoardString: String {
    case authentication             = "Authentication"
    case main                       = "Main"
    case actions                    = "Actions"
    case approval                   = "Approval"
    case resources                  = "Resources"
    case reports                    = "Reports"
    case menu                       = "Menu"
    case leftSideMenu               = "LeftSideMenu"
    case ideasNProblems              = "IdeasNProblems"
    
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func instantiateFromAppStoryboard(appStoryboard: AppStoryBoardString) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
