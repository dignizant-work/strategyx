//
//  ColorType.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

struct ColorType {
    func setColor(index:Int) -> UIColor? {
        switch index {
        case 0:
            return clear
        case 1:
            return white
        case 2:
            return black
        case 3:
            return appGreen
        case 4:
            return appBlack
        case 5:
            return appPlaceHolder
        case 6:
            return appTextFieldBorder
        case 7:
            return appTextLightGrayColor
        case 8:
            return appBgLightGrayColor
        case 9:
            return red
        case 10:
            return appTextBlackShedColor
        case 11:
            return appSepratorColor
        case 12:
            return appYellow
        case 13:
            return appPink
        case 14:
            return blue
        case 15:
            return appStrategyBlue
        case 16:
            return appStrategyYellow
        case 17:
            return appStrategyLightSkyBlue
        case 18:
            return appStrategyPurple
        case 19:
            return appGraphCyanColor
        case 20:
            return appCyanDarkColor
        case 21:
            return appDarkYellow
        case 22:
            return appLevelBGLightYellowColor
        case 23:
            return appLevelBGLightCyanColor
        case 24:
            return lightGray
        case 25:
            return appBadgePinkColor
        case 26:
            return addLightBlueAddSubTaskColor
        case 27:
            return orange
        case 28:
            return appColoeDarkBlue
        case 29:
            return appColoeYellowBorder
        case 30:
            return appColoeYellowBG
        case 31:
            return appColoeDarkYellowText
        case 32:
            return green
        case 33:
            return appColorFaceGreen
        case 34:
            return appColorFilterRed
        case 35:
            return appColorRecurringTask
        case 36:
            return appColorTacticalProjectGray
        case 37:
            return appColorStrategyGoalGreen
        case 38:
            return appColorSuccessFactorBlue
        case 39:
            return appColorRiskRed
        case 40:
            return appColorIdeaYellow
        case 41:
            return appColorProblemRed
        case 42:
            return appColorFocusGoalRed
        case 43:
            return appColorLightOrange
        case 44:
            return appColorLightGreen
        default:
            return nil
        }
    }
}

let clear                           = UIColor.clear
let white                           = UIColor.white
let black                           = UIColor.black
let red                             = UIColor.red
let blue                            = UIColor.blue
let lightGray                       = UIColor.lightGray
let orange                          = UIColor.orange
let green                           = UIColor.green


let appYellow                       = UIColor(red:1, green:0.65, blue:0.15, alpha:1)
let appGreen                        = UIColor(red:0.15, green:0.79, blue:0.5, alpha:1)
let appBlack                        = UIColor(red:0.1, green:0.09, blue:0.23, alpha:1)
let appPink                         = UIColor(red:0.98, green:0.42, blue:0.36, alpha:1)
let appPlaceHolder                  = UIColor(red:0.54, green:0.59, blue:0.63, alpha:1)
let appTextFieldBorder              = UIColor(red:0.86, green:0.88, blue:0.9, alpha:1)
let appTextLightGrayColor           = UIColor(red:0.65, green:0.69, blue:0.76, alpha:1)
let appBgLightGrayColor             = UIColor(red:0.75, green:0.77, blue:0.82, alpha:1)
let appTextBlackShedColor           = UIColor(red:0.39, green:0.45, blue:0.5, alpha:1)
let appSepratorColor                = UIColor(red:0.9, green:0.92, blue:0.93, alpha:1)
let appStrategyBlue                 = UIColor(hexString: "#2569C9")
let appStrategyYellow               = UIColor(hexString: "#DB9D42")
let appStrategyLightSkyBlue         = UIColor(hexString: "#25BDC9")
let appStrategyPurple               = UIColor(hexString: "#8D42DB")
let appGraphCyanColor               = UIColor(red:0, green:0.93, blue:1, alpha:1)
let appCyanDarkColor                = UIColor(red:0.15, green:0.74, blue:0.79, alpha:1)
let appDarkYellow                   = UIColor(red:0.87, green:0.76, blue:0.04, alpha:1)
let appLevelBGLightYellowColor      = UIColor(red:1, green:1, blue:0.93, alpha:1)
let appLevelBGLightCyanColor        = UIColor(red:0.93, green:1, blue:0.95, alpha:1)
let appBadgePinkColor               = UIColor(red:0.85, green:0.25, blue:0.41, alpha:1)
let addLightBlueAddSubTaskColor     = UIColor(red:0.93, green:0.94, blue:0.96, alpha:1)
let appColorFilterGreen             = UIColor(hexString: "#8EC165")
let appColorFilterRed               = UIColor(hexString: "#FB6B5B")
let appColorFilterYellow            = UIColor(hexString: "#FFC333")
let appColorFilterBlue              = UIColor(hexString: "#2EC0C1")
let appColorFilterBlack             = UIColor(hexString: "#2E3E4E")
let appColoeDarkBlue                = UIColor(hexString: "#3379B5")
let appColoeYellowBG                = UIColor(hexString: "#FCF8E3")
let appColoeYellowBorder            = UIColor(hexString: "#FFF7C9")
let appColoeDarkYellowText          = UIColor(hexString: "#8A6D3B")
let appColorFaceGreen               = UIColor(hexString: "#41A045")
let appColorRecurringTask           = UIColor(hexString: "#663399")
let appColorTacticalProjectGray     = UIColor(hexString: "#ababab")
let appColorStrategyGoalGreen       = UIColor(hexString: "#41a045")
let appColorSuccessFactorBlue       = UIColor(hexString: "#4169e1")
let appColorRiskRed                 = UIColor(hexString: "#fa8072")
let appColorIdeaYellow              = UIColor(hexString: "#d2d200")
let appColorProblemRed              = UIColor(hexString: "#eb4646")
let appColorFocusGoalRed            = UIColor(hexString: "#FB6B5B")
let appColorLightOrange             = UIColor(hexString: "#FFB833")
let appColorLightGreen              = UIColor(hexString: "#B7D959")
