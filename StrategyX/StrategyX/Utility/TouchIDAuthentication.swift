/// Copyright (c) 2017 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation
import LocalAuthentication

enum BiometricType {
  case none
  case touchID
  case faceID
}

class BiometricIDAuth {
  static let shared = BiometricIDAuth()
    
  var loginReason = "Logging in with Touch ID"

  func biometricType() -> BiometricType {
    let context = LAContext()

    let _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    if #available(iOS 11.0, *) {
        switch context.biometryType {
        case .none:
            return .none
        case .touchID:
            return .touchID
        case .faceID:
            return .faceID
        @unknown default:
            return .none
        }
    } else {
        // Fallback on earlier versions
        var err: NSError?
        
        // Check if the user is able to use the policy we've selected previously
        guard context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &err) else {
//            image.image = UIImage(named: "TouchID_off")
//            // Print the localized message received by the system
//            message.text = err?.localizedDescription
            if let error = err {
                if error.code == LAError.touchIDNotAvailable.rawValue {
                    return .none
                }
            }
            return .touchID

        }
        return .touchID
    }
  }

  func canEvaluatePolicy() -> Bool {
    let context = LAContext()
    return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
  }

    func authenicateUser(completion:@escaping(String?, Bool) -> Void) {
        let context = LAContext()
        context.localizedFallbackTitle = "Setting"
        
        var authError: NSError?
        var reasonString = "Place your finger on the device home button to verify your identity"
        switch BiometricIDAuth.shared.biometricType() {
        case .faceID:
            reasonString = "Place your face on the device front camera to verify your identity"
            break
        case  .touchID:
            break
        default:
            break
        }
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { [weak self] (success, evaluateError) in
                
                if success {
                    
                    //TODO: User authenticated successfully, take appropriate action
                    DispatchQueue.main.async {
                        // User authenticated successfully, take appropriate action
                        completion(nil, false)
                    }
                    
                    
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        return
                    }
                
                    let isRedirect = self?.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code)
                    if LAError.userFallback.rawValue == error._code {
                        DispatchQueue.main.async {
                            completion(isRedirect?.0, true)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(isRedirect?.0, isRedirect?.1 ?? false)
                        }
                    }
//                    print(self?.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    //                    self?.theController.showAlertAtBottom(message: self?.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code) ?? "Touch ID is not available")
//                    self?.theController.showAlert(msg: self?.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code) ?? "Touch ID is not available")
                    
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                    
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
            let isRedirect = evaluateAuthenticationPolicyMessageForLA(errorCode: error._code)

            completion(isRedirect.0, isRedirect.1)

//            theController.showAlert(msg: evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
            
            //            theController.showAlertAtBottom(message: evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
   /*
  func authenticateUser(completion: @escaping (String?) -> Void) {
    guard canEvaluatePolicy() else {
      completion("Touch ID not available")
      return
    }
    
    context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: loginReason) { (success, evaluateError) in
      if success {
        DispatchQueue.main.async {
          // User authenticated successfully, take appropriate action
          completion(nil)
        }
      } else {
        var message: String!

        if #available(iOS 11.0, *) {
            switch evaluateError {
            case LAError.authenticationFailed?:
                message = "There was a problem verifying your identity."
            case LAError.userCancel?:
                message = "You pressed cancel."
            case LAError.userFallback?:
                message = "You pressed password."
            case LAError.biometryNotAvailable?:
                message = "Face ID/Touch ID is not available."
            case LAError.biometryNotEnrolled?:
                message = "Face ID/Touch ID is not set up."
            case LAError.biometryLockout?:
                message = "Face ID/Touch ID is locked."
            default:
                message = "Face ID/Touch ID may not be configured"
            }
        } else {
            // Fallback on earlier versions
            switch evaluateError {
            case LAError.authenticationFailed?:
                message = "There was a problem verifying your identity."
            case LAError.userCancel?:
                message = "You pressed cancel."
            case LAError.userFallback?:
                message = "You pressed password."
            case LAError.touchIDNotAvailable?:
                message = "Face ID/Touch ID is not available."
            case LAError.touchIDNotEnrolled?:
                message = "Face ID/Touch ID is not set up."
            default:
                message = "Face ID/Touch ID may not be configured"
            }
            
        }
        completion(message)
     }
    }
  }
    */
    
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> (String, Bool) {
        var message = ""
        var redirectToSetting = false

        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because biometric authentication deny by you, Please enable from your device setting"
                redirectToSetting = true
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times. Please enable from your device setting"
                redirectToSetting = true
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication. Please enable from your device setting"
                redirectToSetting = true
            case LAError.biometryLockout.rawValue:
                message = "Too many failed attempts. Please enable from your device setting"
                redirectToSetting = true
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts. Please enable from your device setting"
                redirectToSetting = true

            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device, Please enable from your device setting"
                redirectToSetting = true

            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return (message,redirectToSetting);
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> (String, Bool) {
        
        var message = ""
        var redirectToSetting = false
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials. Please enable from your device setting"
            redirectToSetting = true
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device. Please enable from your device setting"
            redirectToSetting = true

        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = ""
            
        case LAError.userFallback.rawValue:
            message = "Please verify biometric authenication from your device setting"
            redirectToSetting = true
        default:
            let evalute = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
            message = evalute.0
            redirectToSetting = evalute.1
        }
        
        return (message,redirectToSetting)
    }
}
