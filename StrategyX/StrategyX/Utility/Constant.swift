//
//  Constant.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseMessaging

//Intercom
let intercom_IOSApiKey = "ios_sdk-3d7442b05324f46135242cffeaec41d1e8185dae"
let intercom_AppID = "ve920z30"

//Stripe
let stripe_Test = "pk_test_fvfhtn2b3eBhrtipdV2P2kvM00RezvV6Kv"
let stripe_Live = "pk_live_HEMZcYBjbkf8KV8NimN3Po8g00rR3z2hPM"


let realm = try! Realm()

let Default = UserDefaults.standard

let deviceID = UIDevice.current.identifierForVendor?.uuidString ?? "".random()
let deviceType = "2" // change by JD, Before we have passwed 1
var device_token = String().random()
var push_token = Messaging.messaging().fcmToken ?? ""

let lang = "0"

struct DateFormatterInputType {
    static let inputType1 = "yyyy-MM-dd HH:mm:ss"
    static let inputType2 = "yyyy-MM-dd"
    static let inputType3 = "dd MMM yy hh:mm:ss a"
}

struct DateFormatterOutputType {
    
    static let outputType1 = "dd MMM yy hh:mma"
    static let outputType2 = "dd MMM yy"
    static let outputType3 = "yyyy-MM-dd hh:mm a"
    static let outPutType4 = "dd MMM yyyy, hh:mm a"
    static let outputType5 = "dd-MM-yy,hh:mm a"
    static let outputType6 = "dd MMM hh:mma"
    static let outputType7 = "dd-MMM-yyyy hh:mm a"
    static let outputType8 = "yyyy-MM-dd HH:mm:ss"
    static let outputType9 = "hh:mm a"
    static let outputType10 = "dd MMM hh:mma"
    static let outPutType11 = "dd MMM yy, hh:mma"
    static let outputType12 = "HH:mm"
    static let outputType13 = "MM/dd/yyyy"
    static let outputType14 = "hh:mma"
    static let outputType15 = "dd MMM yy\nhh:mm a"

}
