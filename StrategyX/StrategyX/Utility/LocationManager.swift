//
//  LocationManager.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import CoreLocation

// NOTE:- Add this two keys in info.plist
// 1. NSLocationWhenInUseUsageDescription
// 2. NSLocationAlwaysUsageDescription

protocol LocationSharedManagerDelegate {
    func isLocationAuthorizationChanged(isChanged:Bool)
}


class LocationManager: NSObject {
    static let sharedInstance = LocationManager()
    
//    var strLatitude: String  = ""
//    var strLongitude: String = ""
    
    var latitude:NSNumber = NSNumber(value: 0.0)
    var longitude:NSNumber = NSNumber(value: 0.0)
    
    private var coreLocationManager: CLLocationManager? = nil
    var delegate:LocationSharedManagerDelegate?
    
    func startLocationUpdates() {
        if coreLocationManager == nil {
            coreLocationManager = CLLocationManager()
            coreLocationManager?.delegate = self
        }
        
        if (coreLocationManager?.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)))! && CLLocationManager.authorizationStatus() == .notDetermined {
            coreLocationManager?.requestWhenInUseAuthorization()
        }
        
        if CLLocationManager.locationServicesEnabled() {
            coreLocationManager?.startUpdatingLocation()
        }
    }
    
    func stopLocationUpdates() {
        coreLocationManager?.stopUpdatingLocation()
    }
    
    func findAddressFromTheLocation(location: CLLocation, success: @escaping (Bool, String) -> Void) {
        //  let components = URLComponents(string: "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&sensor=true")!
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: "AIzaSyDzqmpWtTufo0IIb0ITlnFX_F-dQmOZyaY")
        let address = URLQueryItem(name: "latlng", value: "\(location.coordinate.latitude),\(location.coordinate.longitude)")
        components.queryItems = [key, address]
        print(components.url!)
        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                print(String(describing: response))
                print(String(describing: error))
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                print("not JSON format expected")
                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                return
            }
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" else {
                    print("no results")
                    print(String(describing: json))
                    return
            }
            print(results)
            DispatchQueue.main.async {
                // now do something with the results, e.g. grab `formatted_address`:
                let Rcomponents = results[0]
                let address_components = Rcomponents["address_components"] as? [[String:Any]]
                //print(address_components?.count)
                var block = ""
                var street = ""
                // print(address_components)
                for(_,Value) in (address_components?.enumerated())!
                {
                    let types = Value["types"] as! [String]
                    for(_,type) in types.enumerated()
                    {
                        if(type == "street_number")
                        {
                            block = Value["long_name"] as! String
                        }
                        if(type == "route")
                        {
                            street = Value["long_name"] as! String
                        }
                        else if(type == "sublocality_level_2")
                        {
                            street = Value["long_name"] as! String
                        }
                    }
                }
                print(block,street)
                let strings = results.compactMap { $0["formatted_address"] as? String }
                if strings.count > 0 {
//                    print(location)
                    //if(location.count > 1)
                    //{
                    //success(true, location[1])
                    //}else{
                    success(true, strings[0])
                    //}
                }
                
            }
        }
        task.resume()
        
    }
    func findStoreItemAddressFromTheLocation(location: CLLocation, success: @escaping (Bool, [String]) -> Void) {
        //  let components = URLComponents(string: "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&sensor=true")!
        var Addresses = [String]()
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: "AIzaSyDzqmpWtTufo0IIb0ITlnFX_F-dQmOZyaY") // use your key using  account of appication
        let address = URLQueryItem(name: "latlng", value: "\(location.coordinate.latitude),\(location.coordinate.longitude)")
        components.queryItems = [key, address]
        print(components.url!)
        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                print(String(describing: response))
                print(String(describing: error))
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                print("not JSON format expected")
                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                return
            }
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" else {
                    print("no results")
                    print(String(describing: json))
                    return
            }
            print(results)
            DispatchQueue.main.async {
                // now do something with the results, e.g. grab `formatted_address`:
                let Rcomponents = results[0]
                let address_components = Rcomponents["address_components"] as? [[String:Any]]
                var block = ""
                var street = ""
                var area = ""
                var building = ""
                for(_,Value) in (address_components?.enumerated())!
                {
                    let types = Value["types"] as! [String]
                    for(_,type) in types.enumerated()
                    {
                        if(type == "street_number")
                        {
                            block = Value["long_name"] as! String
                        }
                        if(type == "route")
                        {
                            street = Value["long_name"] as! String
                        }
                        else if(type == "sublocality_level_2")
                        {
                            street = Value["long_name"] as! String
                        }
                        
                        if(type == "sublocality_level_1")
                        {
                            area = Value["long_name"] as! String
                        }
                        
                        if(type == "establishment")
                        {
                            building = Value["long_name"] as! String
                        }
                        
                    }
                }
                print("block =",block,street,area,building)
                let strings = results.compactMap { $0["formatted_address"] as? String }
                if strings.count > 0
                {
                    print(strings)
                    //if(location.count > 1)
                    //{
                    //success(true, location[1])
                    //}else{
                    Addresses.append(strings[0])
                    Addresses.append(block)
                    Addresses.append(street)
                    Addresses.append(area)
                    Addresses.append(building)
                    
                    print("Addresses-->\(Addresses)")
                    success(true, Addresses)
                    //}
                }
                
            }
        }
        task.resume()
    }
    func getLocationAddress(location: CLLocation) -> String
    {
        
        //  let components = URLComponents(string: "http://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.coordinate.latitude),\(location.coordinate.longitude)&sensor=true")!
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: "AIzaSyDzqmpWtTufo0IIb0ITlnFX_F-dQmOZyaY") // use your key using
        let address = URLQueryItem(name: "latlng", value: "\(location.coordinate.latitude),\(location.coordinate.longitude)")
        components.queryItems = [key, address]
        print(components.url!)
        var locaion = String()
        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                print(String(describing: response))
                print(String(describing: error))
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                print("not JSON format expected")
                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                return
            }
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" else {
                    print("no results")
                    print(String(describing: json))
                    return
            }
            
            DispatchQueue.main.async {
                // now do something with the results, e.g. grab `formatted_address`:
                let strings = results.compactMap { $0["formatted_address"] as? String }
                if strings.count > 0
                {
                    print(strings[0])
                    locaion = strings[0]
                }else{
                    locaion = ""
                }
                
            }
        }
        task.resume()
        return locaion
        
    }
    
    func IsLocationPermissionAuthorized() -> (Bool, Bool)  {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
                return (true, true)
            }
            return (true, false)
        }
        return (false, false)
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            if delegate != nil {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                    self.delegate?.isLocationAuthorizationChanged(isChanged: true)
                })
            }
        } else {
            if delegate != nil {
                delegate?.isLocationAuthorizationChanged(isChanged: false)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            
            //            strLatitude = "29.2805963834138"
            //            strLongitude = "48.0079947622465"
            
            
            let firstLocation = locations[0]
            //            print("firstLocation:=\(firstLocation.coordinate)")
            latitude = NSNumber(value: firstLocation.coordinate.latitude)
            longitude = NSNumber(value: firstLocation.coordinate.longitude)
            
//            strLatitude = String(firstLocation.coordinate.latitude)
//            strLongitude = String(firstLocation.coordinate.longitude)
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("The location updation failed with \(error.localizedDescription)")
        let location = IsLocationPermissionAuthorized()
        if location.0 && location.1 {
            manager.startUpdatingLocation()
        } else {
            if delegate != nil {
                delegate?.isLocationAuthorizationChanged(isChanged: false)
            }
        }
    }
}
