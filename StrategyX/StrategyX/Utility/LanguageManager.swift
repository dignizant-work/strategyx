//
//  LanguageManager.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation

extension String {
    var localize: String {
        return LanguageManager.sharedInstance.getTranslationForKey(self)
    }
}

func Localize(key: String) -> String {
    return LanguageManager.sharedInstance.getTranslationForKey(key)
}

class LanguageManager: NSObject {
    static let sharedInstance = LanguageManager()
    
    func getTranslationForKey(_ key: String) -> String {
        
        let language:String = "Base"
        /*
        if !MOLHLanguage.isArabic() {
            language = "Base"
        } else {
            language = "ar"
        }*/
        
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return (bundle?.localizedString(forKey: key, value: "", table: nil))!
    }
    
    func getTranslationForKey(_ key: String, value:String) -> String {
        let languageCode: String? = UserDefaults.standard.string(forKey: "APP_CURRENT_LANGUAGE")?.lowercased()
        
        
        
        let bundlePath: String? = Bundle.main.path(forResource: languageCode, ofType: "lproj")
        let languageBundle = Bundle(path: bundlePath!)
        var translatedString = languageBundle?.localizedString(forKey: key, value: value, table: nil)
        
        if (translatedString?.count ?? 0) < 1 {
            translatedString = NSLocalizedString(key, comment: value)
        }
        return translatedString!
    }
    
    func setLanuageAs(lan: String) {
        UserDefaults.standard.setValue(lan.lowercased(), forKey: "APP_CURRENT_LANGUAGE")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentLanuage() -> String {
        let languageCode: String? = UserDefaults.standard.string(forKey: "APP_CURRENT_LANGUAGE")?.lowercased()
        
        if (languageCode != nil) {
            return languageCode!
        } else {
            return ""
        }
    }
}
