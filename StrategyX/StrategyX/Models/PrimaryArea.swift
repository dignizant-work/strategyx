import Foundation 
import ObjectMapper 
import RealmSwift

class PrimaryArea: Object,Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var actionCount: Int = 0
    @objc dynamic var descriptionArea: String = ""
	@objc dynamic var completeActionCount: Int = 0
    @objc dynamic var completeActionFlag: Int = 0
	@objc dynamic var userImage: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var createdById: String = ""
    @objc dynamic var companyName: String = ""
    @objc dynamic var companyId: String = ""
    @objc dynamic var assigneeId: String = ""
    @objc dynamic var assignedTo: String = ""
    @objc dynamic var department: String = ""
    @objc dynamic var departmentId: String = ""
    @objc dynamic var notesCount:Int = 0
    @objc dynamic var createdDate: String = ""
    @objc dynamic var archiveGoalFlag: Int = 0

    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()

    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		status <- map["status"]
        descriptionArea <- map ["description"]
		actionCount <- map["action_count"] 
		completeActionCount <- map["complete_action_count"]
        completeActionFlag <- map["complete_action_flag"]
		userImage <- map["user_image"]
        createdBy <- map["created_by"]
        companyName <- map["company_name"]
        companyId <- map["company_id"]
        assigneeId <- map["assignee_id"]
        assignedTo <- map["assigned_to"]
        department <- map["department_name"]
        departmentId <- map["department_id"]
        notesCount <- map["notes_count"]
        createdDate <- map["created_date"]
        createdById <- map["created_by_id"]
        archiveGoalFlag <- map["archive_goal_flag"]
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
	}
}

class PrimaryAreaList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [PrimaryArea] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

