import Foundation 
import ObjectMapper 
import RealmSwift


class StrategyAction:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var companyId: String = ""
	@objc dynamic var visionText: String  = ""
	@objc dynamic var missionText: String = ""
	@objc dynamic var valuesText: String = ""
	@objc dynamic var cultureText: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var modifiedBy: String = ""
    
//    @objc dynamic var coreCompetencText: [String] = []
    @objc dynamic var coreBusinessText: String = ""
    @objc dynamic var missionTextDefault: String = ""
    @objc dynamic var coreBusinessTextDefault: String = ""
    @objc dynamic var valuesTextDefault: String = ""
    @objc dynamic var coreCompetencTextDefault: String = ""
    @objc dynamic var visionTextDefault: String = ""
    @objc dynamic var cultureTextDefault: String = ""
    @objc dynamic var strengthsText: String = ""
    @objc dynamic var strengthsTextDefault: String = ""
    @objc dynamic var weaknessesText: String = ""
    @objc dynamic var weaknessesTextDefault: String = ""
    @objc dynamic var opportunitiesText: String = ""
    @objc dynamic var opportunitiesTextDefault: String = ""
    @objc dynamic var threatsText: String = ""
    @objc dynamic var threatsTextDefault: String = ""

    
    var coreCompetencText = List<String>()

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
//        id <- map["id"]
//        companyId <- map["company_id"]
//        visionText <- map["vision_text"]
//        missionText <- map["mission_text"]
//        valuesText <- map["values_text"]
//        cultureText <- map["culture_text"]
//        status <- map["status"]
//        created <- map["created"]
//        createdBy <- map["created_by"]
//        modified <- map["modified"]
//        modifiedBy <- map["modified_by"]
        
        visionText <- map["vision_text"]
//        coreCompetencText <- map["core_competenc_text"]
        missionText <- map["mission_text"]
        coreBusinessText <- map["core_business_text"]
        missionTextDefault <- map["mission_text_default"]
        companyId <- map["company_id"]
        coreBusinessTextDefault <- map["core_business_text_default"]
        id <- map["id"]
        valuesTextDefault <- map["values_text_default"]
        coreCompetencTextDefault <- map["core_competenc_text_default"]
        visionTextDefault <- map["vision_text_default"]
        cultureTextDefault <- map["culture_text_default"]
        createdBy <- map["created_by"]
        cultureText <- map["culture_text"]
        modified <- map["modified"]
        status <- map["status"]
        valuesText <- map["values_text"]
        created <- map["created"]
        modifiedBy <- map["modified_by"]
        strengthsText <- map["strengths_text"]
        strengthsTextDefault <- map["strengths_text_default"]
        weaknessesText <- map["weaknesses_text"]
        weaknessesTextDefault <- map["weaknesses_text_default"]
        opportunitiesTextDefault <- map["opportunities_text_default"]
        opportunitiesText <- map["opportunities_text"]
        threatsText <- map["threats_text"]
        threatsTextDefault <- map["threats_text_default"]

        
        var coreCompetenctext: [String]?
        coreCompetenctext <- map["core_competenc_text"]
        if let items = coreCompetenctext {
            for item in items {
                self.coreCompetencText.append(item)
            }
        }
	}
} 

class CoreCompetencText: Mappable {
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
    }
}
