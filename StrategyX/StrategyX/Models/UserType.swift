import Foundation 
import ObjectMapper 
import RealmSwift

class UserType: Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var description: String = ""
    @objc dynamic var status: String = ""
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        status <- map["status"]
    }
}

class UserTypeList: Mappable {

	var list:[UserType] = []

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"] 
	}
} 



