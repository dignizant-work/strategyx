import Foundation 
import ObjectMapper 
import RealmSwift

class SuccessFactorList:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var criticalSuccessFactorName: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var focusGoals: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var notesCount: Int = 0
	@objc dynamic var totalAction: Int = 0
	@objc dynamic var complateAction: Int = 0
    @objc dynamic var completeActionFlag: Int = 0
    @objc dynamic var createdById: String = ""

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		criticalSuccessFactorName <- map["critical_success_factor_name"] 
		created <- map["created"] 
		focusGoals <- map["focus_goals"] 
		assigneeId <- map["assignee_id"] 
		assignedTo <- map["assigned_to"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		notesCount <- map["notes_count"] 
		totalAction <- map["total_action"] 
		complateAction <- map["complate_action"]
        completeActionFlag <- map["complete_action_flag"]
        createdById <- map["created_by_id"]
	}
} 

class SuccessFactorAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [SuccessFactorList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}



