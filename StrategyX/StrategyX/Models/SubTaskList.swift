import Foundation 
import ObjectMapper 
import RealmSwift

class SubTaskList:Object, Mappable {

    @objc dynamic var subTaskId: String = ""
    @objc dynamic var subTaskName: String = ""
    @objc dynamic var subTaskStatus: String = ""
    @objc dynamic var subTaskPosition : String = ""
    @objc dynamic var is_old_task: String = ""
    @objc dynamic var is_new_task: String = ""
    @objc dynamic var is_del: String = ""
    @objc dynamic var isSelected: Bool = false
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
        
		subTaskId <- map["id"]
		subTaskName <- map["task_name"]
        subTaskStatus <- map["status"]
        subTaskPosition <- map["position"]
        
        if subTaskStatus.lowercased() == "completed" {
            isSelected = true
        } else {
            isSelected = false
        }
	}
}

class SubTaskAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [SubTaskList] = []
    
    func mapping(map: Map) {
        list <- map["subTask"]
    }
}


