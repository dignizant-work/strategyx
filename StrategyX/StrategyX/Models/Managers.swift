import Foundation 
import ObjectMapper 
import RealmSwift

class Managers: Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var firstname: String = ""
	@objc dynamic var lastname: String = ""
	@objc dynamic var fullName: String = "" 

	required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		firstname <- map["firstname"] 
		lastname <- map["lastname"] 
		fullName <- map["full_name"] 
	}
} 

class ManagersList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [Managers] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
