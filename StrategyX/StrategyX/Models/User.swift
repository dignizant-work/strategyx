//
//  User.swift
//  StrategyX
//
//  Created by Haresh on 12/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class StayInUser:Mappable {
    
    var email = ""
    var password = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
    
}

class User:Object, Mappable {

    @objc dynamic var id: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var companyId: String = ""
    @objc dynamic var timezoneId: String = ""
    @objc dynamic var departmentId: String = ""
    @objc dynamic var roleId: String = ""
    @objc dynamic var firstname: String = ""
    @objc dynamic var lastname: String = ""
    @objc dynamic var mobile: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var usrResetPasswordToken: String = ""
    @objc dynamic var usrActivationCode: String = ""
    @objc dynamic var status: String = ""
    @objc dynamic var created: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var modifiedBy: String = ""
    @objc dynamic var pushToken: String = ""
    @objc dynamic var lastFeatureUpdateIdSeen: String = ""
    @objc dynamic var accessToken: String = ""
    @objc dynamic var companyName:String = ""
    @objc dynamic var userRating: String = ""
    @objc dynamic var departmentName: String = ""
    @objc dynamic var username:String = ""
    @objc dynamic var userType:String = ""
    @objc dynamic var isAgreementAcknowledge:String = ""
    @objc dynamic var countryCode: String = ""
    @objc dynamic var timezoneName: String = ""
    @objc dynamic var agreementAcknowledgeDate: String = ""
    @objc dynamic var createdDateTime: Int = 0
    @objc dynamic var managerId: String = ""

    var managers = List<Managers>()

    override public static func primaryKey() -> String?{
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        password <- map["password"]
        companyId <- map["company_id"]
        timezoneId <- map["timezone_id"]
        departmentId <- map["department_id"]
        roleId <- map["role_id"]
        firstname <- map["firstname"]
        lastname <- map["lastname"]
        mobile <- map["mobile"]
        image <- map["image"]
        usrResetPasswordToken <- map["usr_reset_password_token"]
        usrActivationCode <- map["usr_activation_code"]
        status <- map["status"]
        created <- map["created"]
        modified <- map["modified"]
        createdBy <- map["created_by"]
        modifiedBy <- map["modified_by"]
        pushToken <- map["push_token"]
        lastFeatureUpdateIdSeen <- map["last_feature_update_id_seen"]
        accessToken <- map["access_token"]
        companyName <- map["company_name"]
        userRating <- map["user_rating"]
        departmentName <- map["department_name"]
        username <- map["username"]
        userType <- map["user_type"]
        isAgreementAcknowledge <- map["is_agreement_acknowledge"]
        countryCode <- map["country_code"]
        timezoneName <- map["timezone_name"]
        agreementAcknowledgeDate <- map["agreement_acknowledge_date"]
        createdDateTime <- map["created_date_time"]
        managerId <- map["manager_id"]
        
        var list: [Managers]?
        list <- map["managers"]
        if let tags = list {
            for tag in tags {
                self.managers.append(tag)
            }
        }

    }
    
    
    
}

struct LoginData {
    var firstname: String = ""
    var lastname: String = ""
    var email: String = ""
    var mobile: String = ""
    var phoneCode: String = ""
    var phoneCodeID: String = ""
    var companyName:String = ""
    var companyAddress: String = ""
    var usrResetPasswordToken: String = ""
    var timeZone: String = ""
    var timeZoneID: String = ""
    var website: String = ""
    var city = ""
    var state = ""
    var country = ""
    var billingAddress = ""
    var billingCity = ""
    var billingState = ""
    var billingCountry = ""
    var stripeToken = ""
    
    var userType = "2"
    
    
    
    
    init() {
        
    }

}
