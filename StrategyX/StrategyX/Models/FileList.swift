import Foundation 
import ObjectMapper 
import RealmSwift

class FileList:Object, Mappable {

    @objc dynamic var fileId: String = ""
    @objc dynamic var fileName: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
        
		fileId <- map["id"]
		fileName <- map["file"]
	}
}

class FilesAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [FileList] = []
    
    func mapping(map: Map) {
        list <- map["file"]
    }
}


