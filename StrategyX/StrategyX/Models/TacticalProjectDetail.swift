import Foundation 
import ObjectMapper 
import RealmSwift
import Realm

public class TacticalProjectDetail:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var tactical_projects_id: String = ""
    @objc dynamic var project_name: String = ""
    @objc dynamic var tactical_description: String = ""
    @objc dynamic var category_name: String = ""
    @objc dynamic var category_id: String = ""
    @objc dynamic var department_id:String = ""
    @objc dynamic var department_name:String = ""
    @objc dynamic var company_name:String = ""
    @objc dynamic var company_id:String = ""
    @objc dynamic var cost:String = ""
    @objc dynamic var startdate: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revised_date: String = ""
    @objc dynamic var assigned_to: String = ""
    @objc dynamic var assignee_id: String = ""
    @objc dynamic var tacticalProject_status: String = ""
    @objc dynamic var created_by_id: String = ""
    @objc dynamic var created_date: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var percentage_compeleted: String = ""
    @objc dynamic var created_by:String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var assignedToUserImage:String = ""
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var label: String = ""

    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()

    override public static func primaryKey() -> String?{
        return "id"
    }
    
    required convenience public init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        tactical_projects_id <- map["tactical_projects_id"]
        project_name <- map["project_name"]
        tactical_description <- map["description"]
        category_name <- map["category_name"]
        category_id <- map["category_id"]
        cost <- map["cost"]
        assigned_to <- map["assigned_to"]
        assignee_id <- map["assignee_id"]
        created_by_id <- map["created_by_id"]
        created_date <- map["created_date"]
        modified <- map["modified"]
        percentage_compeleted <- map["percentage_compeleted"]
        created_by <- map["created_by"]
        department_name <- map["department_name"]
        department_id <- map["department_id"]
        company_name <- map["company_name"]
        company_id <- map["company_id"]
        startdate <- map["startdate"]
        duedate <- map["duedate"]
        revised_date <- map["revised_date"]
        tacticalProject_status <- map["tacticalProject_status"]
        revisedColor <- map["revised_color"]
        duedateColor <- map["duedate_color"]
        assignedToUserImage <- map["assigned_to_user_image"]
        label <- map["label"]
        related_title <- map["related_title"]
        relatedId <- map["related_id"]

        
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
    }
    
    
    
}

