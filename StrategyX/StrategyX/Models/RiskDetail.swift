//
//  RiskDetail.swift
//  StrategyX
//
//  Created by Haresh on 12/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class RiskDetail:Object, Mappable {

    @objc dynamic var id: String = ""
    @objc dynamic var risk_id: String = ""
    @objc dynamic var unwanted_event: String = ""
    @objc dynamic var risk_description: String = ""
    @objc dynamic var category_name: String = ""
    @objc dynamic var category_id: String = ""
    @objc dynamic var riskCategoryName: String = ""
    @objc dynamic var riskConsequenceCategoryName: String = ""
    @objc dynamic var residualRiskRating: String = ""
    @objc dynamic var riskSubcategoryId: String = ""
    @objc dynamic var riskSubCategoryName: String = ""
    @objc dynamic var riskCategoryId: String = ""
    @objc dynamic var scopeBoundries: String = ""
    @objc dynamic var riskConsequenceCategoryId: String = ""
    @objc dynamic var level: String = ""
    @objc dynamic var priority: String = ""
    @objc dynamic var assigned_to: String = ""
    @objc dynamic var assignee_id: String = ""
    @objc dynamic var risk_status: String = ""
    @objc dynamic var created_by_id: String = ""
    @objc dynamic var created_date: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var percentage_compeleted: String = ""
    @objc dynamic var total_action: Int = 0
    @objc dynamic var complate_action: Int = 0
    @objc dynamic var assigned_to_user_image: String = ""
    @objc dynamic var created_by:String = ""
    @objc dynamic var department_name:String = ""
    @objc dynamic var department_id:String = ""
    @objc dynamic var company_name:String = ""
    @objc dynamic var company_id:String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var label: String = ""

    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()

    override public static func primaryKey() -> String?{
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        risk_id <- map["risk_id"]
        unwanted_event <- map["unwanted_event"]
        risk_description <- map["description"]
        category_name <- map["category_name"]
        category_id <- map["category_id"]
        riskCategoryName <- map["risk_category_name"]
        riskConsequenceCategoryName <- map["risk_consequence_category_name"]
        residualRiskRating <- map["residual_risk_rating"]
        riskSubcategoryId <- map["risk_subcategory_id"]
        riskSubCategoryName <- map["risk_sub_category_name"]
        riskCategoryId <- map["risk_category_id"]
        scopeBoundries <- map["scope_boundries"]
        riskConsequenceCategoryId <- map["risk_consequence_category_id"]
        level <- map["level"]
        priority <- map["priority"]
        assigned_to <- map["assigned_to"]
        assignee_id <- map["assignee_id"]
        risk_status <- map["risk_status"]
        created_by_id <- map["created_by_id"]
        created_date <- map["created_date"]
        modified <- map["modified"]
        percentage_compeleted <- map["percentage_compeleted"]
        total_action <- map["total_action"]
        complate_action <- map["complate_action"]
        assigned_to_user_image <- map["assigned_to_user_image"]
        created_by <- map["created_by"]
        department_name <- map["department_name"]
        department_id <- map["department_id"]
        company_name <- map["company_name"]
        company_id <- map["company_id"]
        duedate <- map["duedate"]
        revisedDate <- map["revised_date"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        label <- map["label"]
        related_title <- map["related_title"]
        relatedId <- map["related_id"]
        
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
    }

}
