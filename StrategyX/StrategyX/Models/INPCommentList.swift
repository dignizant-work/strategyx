import Foundation 
import ObjectMapper 
import RealmSwift

class INPCommentList:Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var comment: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var userName: String = ""
	@objc dynamic var userImage: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		comment <- map["comment"] 
		created <- map["created"] 
		createdBy <- map["created_by"] 
		userName <- map["user_name"] 
		userImage <- map["user_image"] 
	}
} 

class INPCommentAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [INPCommentList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
