import Foundation 
import ObjectMapper
import Realm
import RealmSwift

class TermsConditionData: Mappable {

	var data: TermsCondition?

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		data <- map["data"] 
	}
} 

class TermsCondition: Object,Mappable {

	@objc dynamic var contentId: String = ""
	@objc dynamic var contentType: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var contentText: String = ""

    /*override public static func primaryKey() -> String?{
        return "id"
    }*/
    
    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		contentId <- map["content_id"] 
		contentType <- map["content_type"] 
		title <- map["title"] 
		contentText <- map["content_text"] 
	}
} 

