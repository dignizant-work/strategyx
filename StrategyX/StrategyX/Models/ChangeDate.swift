import Foundation 
import ObjectMapper 
import RealmSwift

class ChangeDate:Object, Mappable {

	var actionlogId: String = ""
	var revisedDate: String = ""
	var workDate: String = ""
	var actionlogStatus: String = ""
	var completedDate: String = ""
	var workdateColor: String = ""
	var revisedColor: String = ""
    var assigneeID:String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
        assigneeID <- map["assignee_id"]
		actionlogId <- map["id"]
		revisedDate <- map["revised_date"] 
		workDate <- map["work_date"] 
		actionlogStatus <- map["actionlog_status"] 
		completedDate <- map["completed_date"] 
		workdateColor <- map["workdate_color"] 
		revisedColor <- map["revised_color"] 
	}
} 

