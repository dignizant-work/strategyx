import Foundation 
import ObjectMapper
import RealmSwift

public class TacticalProjectArchived:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var tactical_projects_id: String = ""
	@objc dynamic var project_name: String = ""
    @objc dynamic var tactical_description: String = ""
    @objc dynamic var category_name: String = ""
    @objc dynamic var cost: String = ""
    @objc dynamic var startdate: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revised_date: String = ""
    @objc dynamic var assigned_to: String = ""
	@objc dynamic var assignee_id: String = ""
    @objc dynamic var assigned_to_user_image: String = ""
    @objc dynamic var tacticalProject_status: String = ""
    @objc dynamic var created_by_id: String = ""
    @objc dynamic var created_date: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var total_action: Int = 0
    @objc dynamic var complate_action: Int = 0   
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
		id <- map["id"] 
		tactical_projects_id <- map["tactical_projects_id"]
        project_name <- map["project_name"]
		tactical_description <- map["description"]
        category_name <- map["category_name"]
        cost <- map["cost"]
        startdate <- map["startdate"]
        duedate <- map["duedate"]
        revised_date <- map["revised_date"]
        assigned_to <- map["assigned_to"]
        assignee_id <- map["assignee_id"]
        assigned_to_user_image <- map["assigned_to_user_image"]
        tacticalProject_status <- map["tacticalProject_status"]
        created_by_id <- map["created_by_id"]
        created_date <- map["created_date"]
        modified <- map["modified"]
        total_action <- map["total_action"]
        complate_action <- map["complate_action"]
        
	}
}

class TacticalProjectArchivedList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var archived: [TacticalProjectArchived] = []
    
    func mapping(map: Map) {
        archived <- map["data"]
    }
}

