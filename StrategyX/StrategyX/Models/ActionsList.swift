import Foundation 
import ObjectMapper 
import RealmSwift

class ActionsList:Object, Mappable {

	@objc dynamic var actionlogId: String = ""
	@objc dynamic var actionlogTitle: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var assigneeCompanyId: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var workDate: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var completedDate: String = ""
	@objc dynamic var percentageCompeleted: String = "0"
	@objc dynamic var actionlogStatus: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var createdDate: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var tacticalProject: String = ""
	@objc dynamic var strategyGoal: String = ""
    @objc dynamic var assignedToUserImage:String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var notesCount:Int = 0
    @objc dynamic var requestedRevisedDate: String = ""
    @objc dynamic var reasonForReviseDueDate: String = ""
    @objc dynamic var approvedBy:String = ""
    @objc dynamic var reasonForDecline:String = ""
    @objc dynamic var requestFlag:Int = 0
    @objc dynamic var approveFlag:Int = 0
    @objc dynamic var typeText: String = ""
    @objc dynamic var type: Int = 0

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		actionlogId <- map["actionlog_id"] 
		actionlogTitle <- map["actionlog_title"] 
		assignedTo <- map["assigned_to"] 
		assigneeCompanyId <- map["assignee_company_id"] 
		assigneeId <- map["assignee_id"]
        approvedBy <- map["approved_by"]
		workDate <- map["work_date"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		completedDate <- map["completed_date"] 
		percentageCompeleted <- map["percentage_compeleted"] 
		actionlogStatus <- map["actionlog_status"] 
		createdBy <- map["created_by"] 
		createdDate <- map["created_date"] 
		modified <- map["modified"] 
		tacticalProject <- map["tactical_project"] 
		strategyGoal <- map["strategy_goal"]
        assignedToUserImage <- map["assigned_to_user_image"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        notesCount <- map["notes_count"]
        requestedRevisedDate <- map["requested_revised_date"]
        reasonForReviseDueDate <- map["reason_for_revise_duedate"]
        reasonForDecline <- map["reason_for_decline"]
        requestFlag <- map["request_flag"]
        approveFlag <- map["approve_flag"]
        typeText <- map["type_text"]
        type <- map["type"]

	}
}

class ActionsAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [ActionsList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}


