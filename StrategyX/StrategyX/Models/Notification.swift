import Foundation 
import ObjectMapper 
import RealmSwift

class NotificationList: Mappable { 

	var list: [Notification] = []

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 

class Notification: Object,Mappable {

	@objc dynamic var notificationId: String = ""
	@objc dynamic var notificationType: String = ""
    @objc dynamic var notificationRefId: String = ""
	@objc dynamic var notification: String = ""
	@objc dynamic var notificationStatus: String = ""
	@objc dynamic var notificationUserId: String = ""
	@objc dynamic var notificationCreatedDate: String = ""
    @objc dynamic var timeName: String = ""
    @objc dynamic var relatedId: String = ""
    @objc dynamic var relatedTo: String = ""
    @objc dynamic var notificationFor: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        notificationRefId <- map["notification_ref_id"]
        timeName <- map["time_name"]
        relatedId <- map["related_id"]
        notificationStatus <- map["notification_status"]
        notification <- map["notification"]
        notificationId <- map["notification_id"]
        relatedTo <- map["related_to"]
        notificationUserId <- map["notification_user_id"]
        notificationType <- map["notification_type"]
        notificationCreatedDate <- map["notification_created_date"]
        notificationFor <- map["notification_for"]
    }
} 

