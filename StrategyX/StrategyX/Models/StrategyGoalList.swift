import Foundation 
import ObjectMapper 
import RealmSwift

class StrategyGoalList: Object,Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
    @objc dynamic var descriptionArea: String = ""
	@objc dynamic var actionCount: Int = 0
	@objc dynamic var completeActionCount: Int = 0
	@objc dynamic var userImage: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var createdById: String = ""
    @objc dynamic var completeActionFlag: Int = 0
    @objc dynamic var companyName: String = ""
    @objc dynamic var companyId: String = ""
    @objc dynamic var assigneeId: String = ""
    @objc dynamic var assignedTo: String = ""
    @objc dynamic var department: String = ""
    @objc dynamic var departmentId: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var notesCount:Int = 0
    @objc dynamic var createdDate: String = ""
    @objc dynamic var archiveGoalFlag:Int = 0
    @objc dynamic var status: String = ""
    
    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"]
        descriptionArea <- map ["description"]
		actionCount <- map["action_count"] 
		completeActionCount <- map["complete_action_count"]
        completeActionFlag <- map["complete_action_flag"]
		userImage <- map["user_image"]
        createdBy <- map["created_by"]
        createdById <- map["created_by_id"]
        createdDate <- map["created_date"]
        companyName <- map["company_name"]
        companyId <- map["company_id"]
        assigneeId <- map["assignee_id"]
        assignedTo <- map["assigned_to"]
        department <- map["department_name"]
        departmentId <- map["department_id"]
        duedate <- map["duedate"]
        revisedDate <- map["revised_date"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        notesCount <- map["notes_count"]
        archiveGoalFlag <- map["archive_goal_flag"]
        status <- map["status"]
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
	}
} 

class StrategyGoalAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [StrategyGoalList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
