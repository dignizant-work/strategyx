import Foundation 
import ObjectMapper
import RealmSwift

class FeaturesUpdate:Object, Mappable { 

	@objc dynamic var featureupdateId: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var description_feature: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var featureImage: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var videoId:String = ""
    @objc dynamic var videoType : String = ""

    required convenience public init?(map: Map) {
        self.init()
    }
	func mapping(map: Map) {
		featureupdateId <- map["featureupdate_id"] 
		title <- map["title"] 
		description_feature <- map["description"] 
		status <- map["status"] 
		created <- map["created"] 
		featureImage <- map["feature_image"]
        createdBy <- map["created_by"]
        videoId <- map["video_id"]
        videoType <- map["video_type"]
	}
} 

class FeaturesUpdateAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [FeaturesUpdate] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
