import Foundation 
import ObjectMapper 
import RealmSwift

class UserList:Object, Mappable { 

	@objc dynamic var userId: String = ""
	@objc dynamic var companyId: String = ""
	@objc dynamic var timezoneId: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var companyName: String = ""
	@objc dynamic var departmentsCount: String = ""
	@objc dynamic var firstname: String = ""
	@objc dynamic var lastname: String = ""
	@objc dynamic var username: String = ""
	@objc dynamic var userEmail: String = ""
	@objc dynamic var mobileNumber: String = ""
	@objc dynamic var userType: String = ""
	@objc dynamic var userStatus: String = ""
	@objc dynamic var userImage: String = ""
    @objc dynamic var departmentName: String = ""
    @objc dynamic var roleId: String = ""
    @objc dynamic var managerId: String = ""
//    @objc dynamic var managers: [Managers] = []
    
    var managers = List<Managers>()

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		userId <- map["user_id"] 
		companyId <- map["company_id"] 
		timezoneId <- map["timezone_id"] 
		departmentId <- map["department_id"] 
		companyName <- map["company_name"] 
		departmentsCount <- map["departments_count"] 
		firstname <- map["firstname"] 
		lastname <- map["lastname"] 
		username <- map["username"] 
		userEmail <- map["user_email"] 
		mobileNumber <- map["mobile_number"] 
		userType <- map["user_type"] 
		userStatus <- map["user_status"] 
		userImage <- map["user_image"]
        departmentName <- map["department_name"]
        roleId <- map["role_id"]
        managerId <- map["manager_id"]
        
        var list: [Managers]?
        list <- map["managers"]
        if let tags = list {
            for tag in tags {
                self.managers.append(tag)
            }
        }
	}
} 

class UserAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [UserList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

