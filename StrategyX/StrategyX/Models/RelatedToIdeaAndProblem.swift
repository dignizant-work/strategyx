import Foundation 
import ObjectMapper 
import RealmSwift

class RelatedToIdeaAndProblem:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var status:String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
        duedate <- map["duedate"]
        duedateColor <- map["duedate_color"]
        revisedDate <- map["revised_date"]
        revisedColor <- map["revised_color"]
        status <- map["status"]
	}
} 

class RelatedToIdeaAndProblemList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [RelatedToIdeaAndProblem] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
