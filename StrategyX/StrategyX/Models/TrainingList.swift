import Foundation 
import ObjectMapper 
import RealmSwift

class TrainingList:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
	}
}

class TrainingAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [TrainingList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}


