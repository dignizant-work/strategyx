import Foundation 
import ObjectMapper
import RealmSwift

class TagList:Object, Mappable {
    
    @objc dynamic var tag_id: String = ""
    @objc dynamic var tag_name: String = ""
    @objc dynamic var tagStatus: String = ""
    @objc dynamic var is_selected: String = "0"
    @objc dynamic var is_old_selected: String = "0"
    @objc dynamic var is_new_added: String = "0"
    @objc dynamic var is_del: String = "0"
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        tag_id <- map["tag_id"]
        tag_name <- map["tag_name"]
        tagStatus <- map["status"]
    }
}

class TagAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [TagList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

