import Foundation 
import ObjectMapper
import RealmSwift

class IdeaAndProblemList: Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var description_ideaNproblem: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var createdById: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var companyId: String = ""
	@objc dynamic var companyName: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var department: String = ""
	@objc dynamic var type: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var submitted: String = ""
	@objc dynamic var totalAction: Int = 0
	@objc dynamic var complateAction: Int = 0
    @objc dynamic var completeActionFlag: Int = 0
	@objc dynamic var duedateColor: String = ""
	@objc dynamic var revisedColor: String = ""
	@objc dynamic var like: Int = 0
	@objc dynamic var comment: Int = 0
    @objc dynamic var assignedToUserImage = ""
    @objc dynamic var notesCount:Int = 0
    @objc dynamic var isLike:Int = 0
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var label: String = ""
    @objc dynamic var isPrivate: String = "0"
    
    var attachment = List<AttachmentIdea>()
	var voiceNotes = List<VoiceNotes>()

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"]
		description_ideaNproblem <- map["description"]
		duedate <- map["duedate"]
		revisedDate <- map["revised_date"]
		assigneeId <- map["assigned_id"]
		assignedTo <- map["assigned_to"]
		createdById <- map["created_by_id"]
		createdBy <- map["created_by"]
		companyId <- map["company_id"] 
		companyName <- map["company_name"]
		departmentId <- map["department_id"]
		department <- map["department"]
		type <- map["type"]
		status <- map["status"]
		submitted <- map["submitted"]
		totalAction <- map["total_action"]
		complateAction <- map["complete_action"]
		duedateColor <- map["duedate_color"]
		revisedColor <- map["revised_color"]
		like <- map["like"] 
		comment <- map["comment"]
        assignedToUserImage <- map["assigned_to_user_image"]
        notesCount <- map["notes_count"]
        isLike <- map["islike"]
        label <- map["label"]
        related_title <- map["related_title"]
        relatedId <- map["related_id"]
        isPrivate <- map["is_private"]
        if totalAction != 0 {
            completeActionFlag = totalAction == complateAction ? 1 : 0
        }
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
	}
} 

class IdeaAndProblemAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [IdeaAndProblemList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class VoiceNotes: Object, Mappable {

	@objc dynamic var voicenotesId: String = ""
	@objc dynamic var voicenotes: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }


	func mapping(map: Map) {
		voicenotesId <- map["voicenotes_id"]
		voicenotes <- map["voicenotes"] 
	}
} 

class AttachmentIdea: Object, Mappable {

	@objc dynamic var attachmentId: String = ""
	@objc dynamic var attachment: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }


	func mapping(map: Map) {
		attachmentId <- map["attachment_id"] 
		attachment <- map["attachment"] 
	}
} 

