import Foundation 
import ObjectMapper 
import RealmSwift

class ChartList:Object, Mappable {

	@objc dynamic var date: String = ""
	@objc dynamic var totalAction: Int = 0
	@objc dynamic var totalCompleteAction: Int = 0

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		date <- map["date"] 
		totalAction <- map["total_action"] 
		totalCompleteAction <- map["total_complete_action"]
	}
} 

class ChartAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [ChartList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class ChartActionList:Object, Mappable {
    
    @objc dynamic var date: String = ""
    @objc dynamic var completePercentage: String = "0"
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        date <- map["date"]
        completePercentage <- map["complete_percentage"]
    }
}

class ChartActionAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [ChartActionList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

