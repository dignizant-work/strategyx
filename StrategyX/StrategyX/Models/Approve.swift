import Foundation 
import ObjectMapper
import RealmSwift

class Approve:Object, Mappable {

	@objc dynamic var actionlogId: String = ""
	@objc dynamic var actionlogTitle: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var workDate: String = ""
	@objc dynamic var percentageCompeleted: String = ""
	@objc dynamic var actionlogStatus: String = ""
	@objc dynamic var approvedBy: String = ""
	@objc dynamic var createdBy: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""


    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		actionlogId <- map["actionlog_id"] 
		actionlogTitle <- map["actionlog_title"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		workDate <- map["work_date"] 
		percentageCompeleted <- map["percentage_compeleted"] 
		actionlogStatus <- map["actionlog_status"] 
		approvedBy <- map["approved_by"] 
		createdBy <- map["created_by"] 
	}
} 

class ApproveAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [Approve] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
