import Foundation 
import ObjectMapper 
import RealmSwift

class PushCount:Object, Mappable { 

	@objc dynamic var notificationUnread: String = ""
	@objc dynamic var whatsnewUnread: String = ""
	@objc dynamic var trainningVideoCount: String = ""
    @objc dynamic var approvalCount: String = ""
    @objc dynamic var lastVersionUpdateDate: String = ""
    @objc dynamic var softwareUpdateCount: String = ""
    @objc dynamic var lastFeedbackSubmitted: Int = 0
    @objc dynamic var dueDateRequestCount: String = ""
    @objc dynamic var sentApprovalCount: String = ""
    @objc dynamic var forYourApprovalCount: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		notificationUnread <- map["notification_unread"] 
		whatsnewUnread <- map["whatsnew_unread"] 
		trainningVideoCount <- map["trainning_video_count"]
        approvalCount <- map["approval_count"]
        lastVersionUpdateDate <- map["last_version_update_date"]
        softwareUpdateCount <- map["ios_app_update_count"]
        lastFeedbackSubmitted <- map["last_feedback_submitted"]
        sentApprovalCount <- map["sent_approval_count"]
        dueDateRequestCount <- map["duedate_request"]
        forYourApprovalCount <- map["for_your_approval_count"]

 	}
} 

class PushNotificationData:Object, Mappable {
    @objc dynamic var approval: String = ""
    @objc dynamic var whatsnewUnread: String = ""
    @objc dynamic var relatedId: String = ""
    @objc dynamic var relatedTo: String = ""
    @objc dynamic var trainningVideoCount: String = ""
    @objc dynamic var notificationUnread: String = ""
    @objc dynamic var softwareUpdateCount: String = ""
    @objc dynamic var iosAppUpdateCount: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        approval <- map["approval"]
        whatsnewUnread <- map["whatsnew_unread"]
        relatedId <- map["related_id"]
        relatedTo <- map["related_to"]
        trainningVideoCount <- map["trainning_video_count"]
        notificationUnread <- map["notification_unread"]
        softwareUpdateCount <- map["ios_app_update_count"]
        iosAppUpdateCount <- map["ios_app_update_count"]
    }
}
