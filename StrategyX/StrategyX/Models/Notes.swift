import Foundation 
import ObjectMapper 
import RealmSwift

class Notes:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var notes: String = ""
	@objc dynamic var created: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		notes <- map["notes"] 
		created <- map["created"] 
	}
} 

class NotesList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [Notes] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
