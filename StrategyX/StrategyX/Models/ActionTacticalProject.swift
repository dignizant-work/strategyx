import Foundation 
import ObjectMapper
import RealmSwift

public class ActionTacticalProject:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var project_name: String = ""
	@objc dynamic var duedate: String = ""
    @objc dynamic var revised_date: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var status:String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    /*override public static func primaryKey() -> String?{
        return "id"
    }*/
    
	public func mapping(map: Map) {
		id <- map["id"] 
		project_name <- map["project_name"]
        duedate <- map["duedate"]
        revised_date <- map["revised_date"]
        duedateColor <- map["duedate_color"]
        revisedColor <- map["revised_color"]
        status <- map ["status"]

	}
}

class ActionTacticalProjectList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var actionTacticalProject: [ActionTacticalProject] = []
    
    func mapping(map: Map) {
        actionTacticalProject <- map["data"]
    }
}

