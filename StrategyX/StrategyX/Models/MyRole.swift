import Foundation 
import ObjectMapper 
import RealmSwift

class MyRole:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var startDate: String = ""
	@objc dynamic var duration: String = ""
	@objc dynamic var reoccuring: String = ""
	@objc dynamic var customFrequency: String = ""
	@objc dynamic var customEvery: String = ""
	@objc dynamic var customSelected: String = ""
	@objc dynamic var eachOrOnThe: String = ""
	@objc dynamic var isOnThe: String = ""
	@objc dynamic var onThe: String = ""
	@objc dynamic var day: String = ""
    @objc dynamic var createdByName:String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var reoccursString: String = ""
    @objc dynamic var companyName: String = ""
    @objc dynamic var companyId: String = ""
    @objc dynamic var department: String = ""
    @objc dynamic var departmentId: String = ""
    @objc dynamic var assigneeId: String = ""
    @objc dynamic var assignedTo: String = ""

    
    var attachment = List<Attachment>()
    var procedure = List<Procedure>()
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		startDate <- map["start_date"] 
		duration <- map["duration"] 
		reoccuring <- map["reoccuring"] 
		customFrequency <- map["custom_frequency"] 
		customEvery <- map["custom_every"] 
		customSelected <- map["custom_selected"] 
		eachOrOnThe <- map["each_or_on_the"] 
		isOnThe <- map["is_on_the"] 
		onThe <- map["on_the"] 
		day <- map["day"] 
		createdBy <- map["created_by"]
        createdByName <- map["created_by_name"]
		created <- map["created"] 
		modified <- map["modified"] 
		status <- map["status"] 
		reoccursString <- map["Reoccurs_string"]
        companyName <- map["company_name"]
        companyId <- map["company_id"]
        department <- map["department_name"]
        departmentId <- map["department_id"]
        assigneeId <- map["assignee_id"]
        assignedTo <- map["assigned_to"]
       
        var list: [Attachment]?
        list <- map["attachment"]
        if let tags = list {
            for tag in tags {
                self.attachment.append(tag)
            }
        }
        
        var list_procedure:[Procedure]?
        list_procedure <- map["procedure"]
        if let procedure = list_procedure {
            for item in procedure {
                self.procedure.append(item)
            }
        }
	}
} 

class Attachment:Object, Mappable {

    @objc dynamic var fileId: String = ""
    @objc dynamic var file: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        fileId <- map["file_id"]
        file <- map["file"]
    }
} 

class Procedure:Object, Mappable {
    
    @objc dynamic var status: String = ""
    @objc dynamic var procedureId: String = ""
    @objc dynamic var procedureText: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        procedureId <- map["procedure_id"]
        procedureText <- map["procedure_text"]
    }
}


class MyRoleAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [MyRole] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
