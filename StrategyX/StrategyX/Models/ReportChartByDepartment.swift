import Foundation 
import ObjectMapper 
import RealmSwift

class ReportChartByDepartment:Object, Mappable {

	@objc dynamic var title: String = ""
	@objc dynamic var totalActions: Int = 0

	required convenience init?(map: Map){
        self.init()
	} 

	func mapping(map: Map) {
		title <- map["title"] 
		totalActions <- map["total_actions"] 
	}
} 

class ReportChartByDepartmentAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [ReportChartByDepartment] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
