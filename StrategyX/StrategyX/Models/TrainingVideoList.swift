import Foundation 
import ObjectMapper 
import RealmSwift

class TrainingVideoList: Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var videoUrl: String = ""
	@objc dynamic var videoId: String = ""
    @objc dynamic var videoType : String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		videoUrl <- map["video_url"] 
		videoId <- map["video_id"]
        videoType <- map["video_type"]
	}
} 

class TrainingAllVideoList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [TrainingVideoList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
