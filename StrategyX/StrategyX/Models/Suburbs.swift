import Foundation 
import ObjectMapper
import RealmSwift

class Suburbs:Object,Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var suburbName: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        suburbName <- map["suburb_name"]
    }
}

class SuburbsList: Mappable {

    var list:[Suburbs] = []
    
    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 



