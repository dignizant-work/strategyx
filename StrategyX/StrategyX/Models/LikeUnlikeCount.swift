//
//  LikeUnlikeCount.swift
//  StrategyX
//
//  Created by Haresh on 20/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class LikeUnlikeCount:Object, Mappable {
    
    @objc dynamic var like: Int = 0
    @objc dynamic var unlike: Int = 0
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        like <- map["like"]
        unlike <- map["Unlike"]
    }
}

class LikeUnlikeCountList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [LikeUnlikeCount] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
