import Foundation 
import ObjectMapper 
import RealmSwift

class CompanyDetail: Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var abbrColor: String = ""
	@objc dynamic var companieName: String = ""
	@objc dynamic var abbreviation: String = ""
	@objc dynamic var officeEmail: String = ""
	@objc dynamic var website: String = ""
	@objc dynamic var mapAddress: String = ""
	@objc dynamic var city: String = ""
	@objc dynamic var state: String = ""
	@objc dynamic var country: String = ""
	@objc dynamic var descriptionCompany: String = ""
	@objc dynamic var userEmail: String = ""
	@objc dynamic var timezoneId: String = ""
	@objc dynamic var timezoneName: String = ""
	@objc dynamic var userFirstname: String = ""
	@objc dynamic var userLastname: String = ""
	@objc dynamic var userMobile: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var companyDepartment: String = ""
	@objc dynamic var profile: String = ""
	@objc dynamic var status: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		abbrColor <- map["abbr_color"] 
		companieName <- map["companie_name"] 
		abbreviation <- map["abbreviation"] 
		officeEmail <- map["office_email"] 
		website <- map["website"] 
		mapAddress <- map["map_address"] 
		city <- map["city"] 
		state <- map["state"] 
		country <- map["country"] 
		descriptionCompany <- map["description"]
		userEmail <- map["user_email"] 
		timezoneId <- map["timezone_id"] 
		timezoneName <- map["timezone_name"] 
		userFirstname <- map["user_firstname"] 
		userLastname <- map["user_lastname"] 
		userMobile <- map["user_mobile"] 
		departmentId <- map["department_id"] 
		companyDepartment <- map["company_department"] 
		profile <- map["profile"] 
		status <- map["status"] 
	}
} 

