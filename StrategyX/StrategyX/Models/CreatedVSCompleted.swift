import Foundation 
import ObjectMapper 
import RealmSwift

class CreatedVSCompleted:Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var type: String = ""
	@objc dynamic var typeText: String = ""
	@objc dynamic var assigned: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var createdDate: String = ""
	@objc dynamic var workDateAdjusted: String = ""
	@objc dynamic var dueDateAdjusted: String = ""
	@objc dynamic var completedDate: String = ""
	@objc dynamic var totalDays: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var departmentName: String = ""
    @objc dynamic var relatedTo: String = ""
    @objc dynamic var relatedId: String = ""

    required convenience init?(map: Map){
        self.init()
    } 

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		type <- map["type"] 
		typeText <- map["type_text"] 
		assigned <- map["assigned"] 
		departmentId <- map["department_id"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		createdDate <- map["created_date"] 
		workDateAdjusted <- map["work_date_adjusted"] 
		dueDateAdjusted <- map["due_date_adjusted"] 
		completedDate <- map["completed_date"] 
		totalDays <- map["total_days"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		assignedTo <- map["assigned_to"] 
        departmentName <- map["department_name"]
        relatedTo <- map["related_to"]
        relatedId <- map["related_id"]
	}
} 

class CreatedVSCompletedAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [CreatedVSCompleted] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
