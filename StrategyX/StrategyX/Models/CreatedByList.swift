import Foundation 
import ObjectMapper 
import RealmSwift

class CreatedByList:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var createrName: String = ""

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		createrName <- map["creater_name"] 
	}
} 


class CreatedByAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [CreatedByList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
