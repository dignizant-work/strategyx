import Foundation 
import ObjectMapper
import RealmSwift

public class Department:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var name: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
    public func mapping(map: Map) {
        
		id <- map["id"] 
		name <- map["name"]
	}
}

class DepartmentList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var department: [Department] = []
    
    func mapping(map: Map) {
        department <- map["data"]
    }
}

