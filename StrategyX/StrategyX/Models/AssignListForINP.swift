import Foundation 
import ObjectMapper 
import RealmSwift

class AssignListForINP: Object,Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var assignName: String = ""

    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		assignName <- map["assignee_name"] 
	}
} 

class AssignAllListForINP:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [AssignListForINP] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
