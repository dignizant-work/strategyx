import Foundation 
import ObjectMapper 
import RealmSwift

class NotesTacticalProjectList:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var tactical_project_id: String = ""
	@objc dynamic var from_user_id: String = ""
	@objc dynamic var notes_description: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var created_date: String = ""
	@objc dynamic var modified_date: String = ""
	@objc dynamic var from_user_name: String = ""
	@objc dynamic var assigned_to_user_image: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
        
		id <- map["id"]
		tactical_project_id <- map["tactical_project_id"]
		from_user_id <- map["from_user_id"]
		notes_description <- map["description"]
		status <- map["status"]
		created_date <- map["created_date"]
		modified_date <- map["modified_date"]
		from_user_name <- map["action_user_name"]
		assigned_to_user_image <- map["assigned_to_user_image"]
	}
}

class NotesTacticalProjectAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [NotesTacticalProjectList] = []
    
    func mapping(map: Map) {
        list <- map["note_list"]
    }
}


