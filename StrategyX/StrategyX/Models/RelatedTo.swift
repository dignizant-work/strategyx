import Foundation 
import ObjectMapper
import RealmSwift

public class RelatedTo:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var value: Int = 0
	@objc dynamic var option: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
        id <- map["id"]
		value <- map["value"]
		option <- map["option"]
	}
}

class RelatedToList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var relatedTo: [RelatedTo] = []
    
    func mapping(map: Map) {
        relatedTo <- map["data"]
    }
}

