import Foundation 
import ObjectMapper 
import RealmSwift

public class Focus:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var focusTitle: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var assigneeCompanyId: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var completedDate: String = ""
	@objc dynamic var focusStatus: String  = ""
	@objc dynamic var createdById: String = ""
	@objc dynamic var createdDate: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var totalAction: Int = 0
	@objc dynamic var complateAction: Int = 0
    @objc dynamic var completeActionFlag: Int = 0
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var notesCount:Int = 0


    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
    
	public func mapping(map: Map) {
		id <- map["id"] 
		focusTitle <- map["focus_title"] 
		assignedTo <- map["assigned_to"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		assigneeCompanyId <- map["assignee_company_id"] 
		assigneeId <- map["assignee_id"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		completedDate <- map["completed_date"] 
		focusStatus <- map["focus_status"] 
		createdById <- map["created_by_id"] 
		createdDate <- map["created_date"] 
		modified <- map["modified"] 
		totalAction <- map["total_action"] 
		complateAction <- map["complate_action"]
        completeActionFlag <- map["complete_action_flag"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        notesCount <- map["notes_count"]

	}
} 

class FocusList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [Focus] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
