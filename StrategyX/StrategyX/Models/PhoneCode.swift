import Foundation 
import ObjectMapper 
import RealmSwift

class PhoneCode:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var sortname: String = ""
	@objc dynamic var phonecode: String = ""
    @objc dynamic var countryname: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		sortname <- map["sortname"] 
		phonecode <- map["phonecode"]
        countryname <- map["country_name"]
	}
} 

class PhoneCodeList:Object, Mappable {
    
    var list:[PhoneCode] = []
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
