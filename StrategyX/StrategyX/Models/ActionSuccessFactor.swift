import Foundation 
import ObjectMapper 
import RealmSwift

class ActionSuccessFactor:Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var criticalSuccessFactorName: String = ""

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		criticalSuccessFactorName <- map["critical_success_factor_name"] 
	}
} 

class ActionSuccessFactorList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var actionSuccessFactor: [ActionSuccessFactor] = []
    
    func mapping(map: Map) {
        actionSuccessFactor <- map["data"]
    }
}
