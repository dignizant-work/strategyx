import Foundation 
import ObjectMapper 
import RealmSwift

class PriorityOverview:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var type: String = ""
	@objc dynamic var typeText: String = ""
	@objc dynamic var assigned: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var status: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var departmentName: String = ""
	@objc dynamic var duedateColor: String = ""
	@objc dynamic var revisedColor: String = ""
    @objc dynamic var relatedTo: String = ""
    @objc dynamic var relatedId: String = ""
    
    required convenience init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"] 
		type <- map["type"] 
		typeText <- map["type_text"] 
		assigned <- map["assigned"] 
		departmentId <- map["department_id"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		status <- map["status"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		assignedTo <- map["assigned_to"] 
		departmentName <- map["department_name"] 
		duedateColor <- map["duedate_color"] 
		revisedColor <- map["revised_color"]
        relatedTo <- map["related_to"]
        relatedId <- map["related_id"]
	}
} 

class PriorityOverviewAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [PriorityOverview] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}


class PriorityOverviewCount:Object, Mappable {
    
    @objc dynamic var totalStrategyGoals: String = ""
    @objc dynamic var totalRisks: String = ""
    @objc dynamic var totalTacticalProjects: String = ""
    @objc dynamic var totalFocusGoals: String = ""
    @objc dynamic var totalIdeas: String = ""
    @objc dynamic var totalProblems: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        totalStrategyGoals <- map["total_strategy_goals"]
        totalRisks <- map["total_risks"]
        totalTacticalProjects <- map["total_tactical_projects"]
        totalFocusGoals <- map["total_focus_goals"]
        totalIdeas <- map["total_ideas"]
        totalProblems <- map["total_problems"]
    }
}
