import Foundation 
import ObjectMapper 
import RealmSwift

class TacticalProjectList:Object, Mappable {

	@objc dynamic var tacticalProjectsId: String = ""
	@objc dynamic var projectName: String = ""
	@objc dynamic var tacticalProjectDescription: String = ""
	@objc dynamic var categoryName: String = ""
	@objc dynamic var cost: String = ""
	@objc dynamic var startdate: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var tacticalProjectStatus: String = ""
	@objc dynamic var createdById: String = ""
	@objc dynamic var createdDate: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var totalAction: Int = 0
    @objc dynamic var complateAction:Int = 0
    @objc dynamic var completeActionFlag: Int = 0
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var notesCount: Int = 0

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		tacticalProjectsId <- map["tactical_projects_id"]
		projectName <- map["project_name"]
		tacticalProjectDescription <- map["description"]
		categoryName <- map["category_name"]
		cost <- map["cost"]
		startdate <- map["startdate"]
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		assignedTo <- map["assigned_to"]
		assignedToUserImage <- map["assigned_to_user_image"]
		assigneeId <- map["assignee_id"]
		tacticalProjectStatus <- map["tacticalProject_status"]
		createdById <- map["created_by_id"]
		createdDate <- map["created_date"]
		modified <- map["modified"]
		totalAction <- map["total_action"]
        complateAction <- map["complate_action"]
        completeActionFlag <- map["complete_action_flag"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        notesCount <- map["notes_count"]

	}
}

class TacticalProjecAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [TacticalProjectList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}


