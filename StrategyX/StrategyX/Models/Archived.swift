import Foundation 
import ObjectMapper
import RealmSwift

public class Archived:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var risk_id: String = ""
	@objc dynamic var unwanted_event: String = ""
    @objc dynamic var risk_description: String = ""
    @objc dynamic var category_name: String = ""
    @objc dynamic var level: String = ""
    @objc dynamic var priority: String = ""
    @objc dynamic var assigned_to: String = ""
	@objc dynamic var assignee_id: String = ""
    @objc dynamic var risk_status: String = ""
    @objc dynamic var created_by_id: String = ""
    @objc dynamic var created_date: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var percentage_compeleted: String = ""
    @objc dynamic var total_action: Int = 0
    @objc dynamic var complate_action: Int = 0
    @objc dynamic var assigned_to_user_image: String = ""
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
		id <- map["id"] 
		risk_id <- map["risk_id"]
        unwanted_event <- map["unwanted_event"]
		risk_description <- map["description"]
        category_name <- map["category_name"]
        level <- map["level"]
        priority <- map["priority"]
        assigned_to <- map["assigned_to"]
        assignee_id <- map["assignee_id"]
        risk_status <- map["risk_status"]
        created_by_id <- map["created_by_id"]
        created_date <- map["created_date"]
        modified <- map["modified"]
        percentage_compeleted <- map["percentage_compeleted"]
        total_action <- map["total_action"]
        complate_action <- map["complate_action"]
        assigned_to_user_image <- map["assigned_to_user_image"]
	}
}

class ArchivedList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var archived: [Archived] = []
    
    func mapping(map: Map) {
        archived <- map["data"]
    }
}

