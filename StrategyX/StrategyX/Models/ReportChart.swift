import Foundation 
import ObjectMapper 
import RealmSwift

class ReportChart:Object, Mappable {

	@objc dynamic var date: String = ""
	@objc dynamic var totalAction: Int = 0

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		date <- map["date"] 
		totalAction <- map["total_action"] 
	}
} 

class ReportChartAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [ReportChart] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
