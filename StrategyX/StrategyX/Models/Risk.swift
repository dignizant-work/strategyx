import Foundation 
import ObjectMapper
import RealmSwift

public class Risk:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var risk_id: String = ""
	@objc dynamic var unwanted_event: String = ""
    @objc dynamic var risk_description: String = ""
    @objc dynamic var category_name: String = ""
    @objc dynamic var level: String = ""
    @objc dynamic var priority: String = ""
    @objc dynamic var assigned_to: String = ""
	@objc dynamic var assignee_id: String = ""
    @objc dynamic var risk_status: String = ""
    @objc dynamic var created_by_id: String = ""
    @objc dynamic var created_date: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var percentage_compeleted: String = ""
    @objc dynamic var completeActionFlag: Int = 0
    @objc dynamic var total_action: Int = 0
    @objc dynamic var complate_action: Int = 0
    @objc dynamic var assigned_to_user_image: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var notesCount:Int = 0

    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
		id <- map["id"] 
		risk_id <- map["risk_id"]
        unwanted_event <- map["unwanted_event"]
		risk_description <- map["description"]
        category_name <- map["category_name"]
        level <- map["level"]
        priority <- map["priority"]
        assigned_to <- map["assigned_to"]
        assignee_id <- map["assignee_id"]
        risk_status <- map["risk_status"]
        created_by_id <- map["created_by_id"]
        created_date <- map["created_date"]
        duedate <- map["duedate"]
        revisedDate <- map["revised_date"]
        modified <- map["modified"]
        percentage_compeleted <- map["percentage_compeleted"]
        completeActionFlag <- map["complete_action_flag"]
        total_action <- map["total_action"]
        complate_action <- map["complate_action"]
        assigned_to_user_image <- map["assigned_to_user_image"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        notesCount <- map["notes_count"]

	}
}

class RiskList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var risk: [Risk] = []
    
    func mapping(map: Map) {
        risk <- map["data"]
    }
}



class RiskCategory:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var categoryName: String = ""
    @objc dynamic var riskCategory_description: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        categoryName <- map["category_name"]
        riskCategory_description <- map["description"]
    }
}

class RiskCategoryList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var riskCategory: [RiskCategory] = []
    
    func mapping(map: Map) {
        riskCategory <- map["data"]
    }
}


class RiskSubCategory:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var subcategoryName: String = ""
    @objc dynamic var riskSubCategory_description: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        subcategoryName <- map["subcategory_name"]
        riskSubCategory_description <- map["description"]
    }
}

class RiskSubCategoryList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var riskSubCategory: [RiskSubCategory] = []
    
    func mapping(map: Map) {
        riskSubCategory <- map["data"]
    }
}


class RiskConsequenceCategory:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var categoryName: String = ""
    @objc dynamic var riskConsequenceCategory_description: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        categoryName <- map["category_name"]
        riskConsequenceCategory_description <- map["description"]
    }
}

class RiskConsequenceCategoryList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var riskConsequenceCategory: [RiskConsequenceCategory] = []
    
    func mapping(map: Map) {
        riskConsequenceCategory <- map["data"]
    }
}
