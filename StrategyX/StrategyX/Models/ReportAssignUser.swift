import Foundation 
import ObjectMapper 
import RealmSwift

class ReportAssignUser:Object, Mappable { 

    @objc dynamic var id: String = ""
	@objc dynamic var userName: String = ""
	@objc dynamic var departmentId: String = ""

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		userName <- map["user_name"] 
		departmentId <- map["department_id"] 
	}
} 

class ReportAssignUserAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [ReportAssignUser] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}
