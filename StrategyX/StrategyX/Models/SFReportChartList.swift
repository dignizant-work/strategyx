import Foundation 
import ObjectMapper 
import RealmSwift

class SFReportChartList:Object, Mappable {

    @objc dynamic var date: String = ""
    @objc dynamic var target: String = ""
    @objc dynamic var actual: String = ""

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		target <- map["target"] 
		actual <- map["actual"] 
		date <- map["date"] 
	}
} 

class SFReportChartAllList: Mappable {
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [SFReportChartList] = []
    var record_for_name:String = ""
    
    func mapping(map: Map) {
        list <- map["chart_data"]
        record_for_name <- map["record_for_name"]
    }
}
