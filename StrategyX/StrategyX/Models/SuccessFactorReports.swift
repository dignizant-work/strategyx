import Foundation 
import ObjectMapper 
import RealmSwift

class SuccessFactorReports:Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var criticalSuccessFactorId: String = ""
	@objc dynamic var date: String = ""
	@objc dynamic var reportFileOrUrl: String = ""
	@objc dynamic var reportFile: String = ""
	@objc dynamic var reportUrl: String = ""
	@objc dynamic var drivers: String = ""
	@objc dynamic var goal: String = ""
	@objc dynamic var comments: String = ""
	@objc dynamic var actions: String = ""
	@objc dynamic var objective: String = ""
	@objc dynamic var completed: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var modifiedBy: String = ""
    @objc dynamic var focusGoal:String = ""
    @objc dynamic var focusId:String = ""
    @objc dynamic var focusTitle:String = ""
    @objc dynamic var sxrReportAvailable: Int = 0
    
    var actionsList = List<ActionsList>()
	var report = List<Report>()
    
    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		criticalSuccessFactorId <- map["critical_success_factor_id"] 
		date <- map["date"] 
		reportFileOrUrl <- map["report_file_or_url"] 
		reportFile <- map["report_file"] 
		reportUrl <- map["report_url"] 
		drivers <- map["drivers"] 
		goal <- map["goal"] 
		comments <- map["comments"] 
		actions <- map["actions"] 
		objective <- map["objective"] 
		completed <- map["completed"] 
		created <- map["created"] 
		modified <- map["modified"] 
		createdBy <- map["created_by"] 
		modifiedBy <- map["modified_by"]
        focusGoal <- map["focus_goal"]
        focusId <- map["focus_id"]
        focusTitle <- map["focus_title"]
        sxrReportAvailable <- map["sxr_report_available"]
        
        var list: [ActionsList]?
        list <- map["actions_list"]
        if let tags = list {
            for tag in tags {
                self.actionsList.append(tag)
            }
        }
        
        var listReport: [Report]?
        listReport <- map["report"]
        if let tags = listReport {
            for tag in tags {
                self.report.append(tag)
            }
        }
        
//        actionsList <- map["actions_list"] 
//        report <- map["report"] 
	}
} 

class Report:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var criticalSuccessFactorId: String = ""
	@objc dynamic var date: String = ""
	@objc dynamic var reportFileOrUrl: String = ""
	@objc dynamic var reportFile: String = ""
	@objc dynamic var reportUrl: String = ""
	@objc dynamic var drivers: String = ""
	@objc dynamic var goal: String = ""
	@objc dynamic var comments: String = ""
	@objc dynamic var actions: String = ""
	@objc dynamic var objective: String = ""
	@objc dynamic var completed: String = ""
	@objc dynamic var created: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var modifiedBy: String = ""
    @objc dynamic var focusId:String = ""
    @objc dynamic var focusTitle:String = ""
    @objc dynamic var sxrReportAvailable: Int = 0

    var actionsList = List<ActionsList>()

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		criticalSuccessFactorId <- map["critical_success_factor_id"] 
		date <- map["date"] 
		reportFileOrUrl <- map["report_file_or_url"] 
		reportFile <- map["report_file"] 
		reportUrl <- map["report_url"] 
		drivers <- map["drivers"] 
		goal <- map["goal"] 
		comments <- map["comments"] 
		actions <- map["actions"] 
		objective <- map["objective"] 
		completed <- map["completed"] 
		created <- map["created"] 
		modified <- map["modified"] 
		createdBy <- map["created_by"] 
		modifiedBy <- map["modified_by"]
        focusId <- map["focus_id"]
        focusTitle <- map["focus_title"]
        sxrReportAvailable <- map["sxr_report_available"]

        
        var list: [ActionsList]?
        list <- map["actions_list"]
        if let tags = list {
            for tag in tags {
                self.actionsList.append(tag)
            }
        }
	}
} 



