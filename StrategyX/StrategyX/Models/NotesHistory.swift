import Foundation 
import ObjectMapper
import Realm
import RealmSwift

class NotesHistoryAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [NotesHistory] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class NotesHistory:Object, Mappable {

    @objc dynamic var noteId: String = ""
    @objc dynamic var notesHistoryDescription: String = ""
    @objc dynamic var actionUserName: String = ""
    @objc dynamic var percentageCompleted: String = ""
	@objc dynamic var assignedUserName: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var workDate: String = ""
    @objc dynamic var createdDate: String = ""
	@objc dynamic var historyStatus: String = ""
    var notesList = List<NotesHistoryList>()

    /*override public static func primaryKey() -> String?{
        return "id"
    }*/
    
    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		noteId <- map["note_id"] 
		notesHistoryDescription <- map["description"]
		actionUserName <- map["action_user_name"] 
		percentageCompleted <- map["percentage_completed"] 
		assignedUserName <- map["assigned_user_name"] 
		revisedDate <- map["revised_date"] 
		workDate <- map["work_date"] 
		createdDate <- map["created_date"] 
		historyStatus <- map["history_status"] 
		
        var list: [NotesHistoryList]?
        list <- map["note_file"]
        if let tags = list {
            for tag in tags {
                self.notesList.append(tag)
            }
        }
	}
} 



