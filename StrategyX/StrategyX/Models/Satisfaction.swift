//
//  Satisfaction.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Satisfaction:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var createdByName: String = ""
    @objc dynamic var comment: String = ""
    @objc dynamic var commentType: String = ""
    @objc dynamic var stage: String = ""
    @objc dynamic var createdDate: String = ""
    @objc dynamic var createdByFirstname:String = ""
    @objc dynamic var createdByLastname:String = ""
    
    var responds = List<Responds>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        createdBy <- map["created_by"]
        createdByName <- map["created_by_name"]
        createdByFirstname <- map["created_by_firstname"]
        createdByLastname <- map["created_by_lastname"]
        comment <- map["comment"]
        commentType <- map["comment_type"]
        stage <- map["stage"]
        createdDate <- map["created_date"]
        
        var listReport: [Responds]?
        listReport <- map["responds"]
        if let tags = listReport {
            for tag in tags {
                self.responds.append(tag)
            }
        }
    }
}

class SatisfactionAllList:Mappable  {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    var list: [Satisfaction] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}

class Responds:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var createdByName: String = ""
    @objc dynamic var respondComment: String = ""
    @objc dynamic var createdDate: String = ""
    @objc dynamic var createdByFirstname:String = ""
    @objc dynamic var createdByLastname:String = ""

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        createdBy <- map["created_by"]
        createdByName <- map["created_by_name"]
        createdByFirstname <- map["created_by_firstname"]
        createdByLastname <- map["created_by_lastname"]
        respondComment <- map["respond_comment"]
        createdDate <- map["created_date"]
    }
}

class SatisfactionFeedBackCount:Object, Mappable {
    
    @objc dynamic var veryHappy: String = ""
    @objc dynamic var happy: String = ""
    @objc dynamic var notSure: String = ""
    @objc dynamic var notHappy: String = ""
    @objc dynamic var commentCount: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        veryHappy <- map["very_happy"]
        happy <- map["happy"]
        notSure <- map["not_sure"]
        notHappy <- map["not_happy"]
        commentCount <- map["comment_count"]
    }
}
