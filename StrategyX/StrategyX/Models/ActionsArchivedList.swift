import Foundation 
import ObjectMapper 
import RealmSwift

class ActionsArchivedList:Object, Mappable {

	@objc dynamic var actionlogId: String = ""
	@objc dynamic var actionlogTitle: String = ""
    @objc dynamic var assignedToUserImage:String = ""
	@objc dynamic var workDate: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var percentageCompeleted: String = "0"
	@objc dynamic var actionlogStatus: String = ""
	@objc dynamic var approvedBy: String = ""
	@objc dynamic var createdBy: String = ""

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		actionlogId <- map["actionlog_id"] 
		actionlogTitle <- map["actionlog_title"] 
		workDate <- map["work_date"]
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"]
		percentageCompeleted <- map["percentage_compeleted"] 
		actionlogStatus <- map["actionlog_status"]
        assignedToUserImage <- map["assigned_to_user_image"]
        approvedBy <- map["approved_by"]
        createdBy <- map["created_by"]
	}
}

class ActionsArchivedAllList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var list: [ActionsArchivedList] = []
    
    func mapping(map: Map) {
        list <- map["data"]
    }
}


