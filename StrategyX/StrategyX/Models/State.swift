import Foundation 
import ObjectMapper
import RealmSwift

class State:Object,Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var stateName: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        stateName <- map["state_name"]
    }
}

class StateList: Mappable {

	var list:[State] = []

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 



