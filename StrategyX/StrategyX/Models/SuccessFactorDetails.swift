import Foundation 
import ObjectMapper 
import RealmSwift

class SuccessFactorDetail:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var criticalSuccessFactorName: String = ""
	@objc dynamic var descriptionSuccessFactor: String = ""
	@objc dynamic var reportTitle: String = ""
	@objc dynamic var reportUrl: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var assignedToUserImage: String = ""
	@objc dynamic var companyId: String = ""
	@objc dynamic var companyName: String = ""
	@objc dynamic var departmentId: String = ""
	@objc dynamic var departmentName: String = ""
	@objc dynamic var createdById: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var createdDate: String = ""
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var label: String = ""

    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()

    required convenience public init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		criticalSuccessFactorName <- map["critical_success_factor_name"] 
		descriptionSuccessFactor <- map["description"] 
		reportTitle <- map["report_title"] 
		reportUrl <- map["report_url"] 
		assigneeId <- map["assignee_id"] 
		assignedTo <- map["assigned_to"] 
		assignedToUserImage <- map["assigned_to_user_image"] 
		companyId <- map["company_id"] 
		companyName <- map["company_name"] 
		departmentId <- map["department_id"] 
		departmentName <- map["department_name"] 
		createdById <- map["created_by_id"] 
		createdBy <- map["created_by"] 
		createdDate <- map["created_date"]
        label <- map["label"]
        related_title <- map["related_title"]
        relatedId <- map["related_id"]

        
        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
        
        
	}
} 


