import Foundation 
import ObjectMapper
import RealmSwift

public class UserAssign:Object, Mappable {

    @objc dynamic var id: String = ""
    @objc dynamic var user_id: String = ""
//    @objc dynamic var assign_user_name: String = ""
    @objc dynamic var assignee_name: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
		assignee_name <- map["assignee_name"]
	}
}

class userAssignList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var userAssign: [UserAssign] = []
    
    func mapping(map: Map) {
        userAssign <- map["data"]
    }
}


public class StrategyUserAssign:Object, Mappable {
    
    @objc dynamic var assignedTo: String = ""
    @objc dynamic var assigneeId: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        assignedTo <- map["assigned_to"]
        assigneeId <- map["assignee_id"]
    }
}

class StrategyUserAssignList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var strategyUserAssign: [StrategyUserAssign] = []
    
    func mapping(map: Map) {
        strategyUserAssign <- map["data"]
    }
}

