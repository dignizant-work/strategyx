import Foundation 
import ObjectMapper 
import RealmSwift

class Street:Object,Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var streetName: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        streetName <- map["street_name"]
    }
}


class StreetList: Mappable {

    var list:[Street] = []

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 


