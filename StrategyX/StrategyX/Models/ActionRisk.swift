import Foundation 
import ObjectMapper
import RealmSwift

public class ActionRisk:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var risk_id: String = ""
	@objc dynamic var unwanted_event: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var status:String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
		id <- map["id"] 
		risk_id <- map["risk_id"]
        unwanted_event <- map["unwanted_event"]
        duedate <- map["duedate"]
        duedateColor <- map["duedate_color"]
        revisedDate <- map["revised_date"]
        revisedColor <- map["revised_color"]
        status <- map["status"]
	}
}

class ActionRiskList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var actionRisk: [ActionRisk] = []
    
    func mapping(map: Map) {
        actionRisk <- map["data"]
    }
}

