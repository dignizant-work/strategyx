import Foundation 
import ObjectMapper 
import RealmSwift

public class FocusDetail:Object, Mappable { 

	@objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
	@objc dynamic var companyName: String = ""
	@objc dynamic var department: String = ""
    @objc dynamic var departmentId: String = ""
    @objc dynamic var companyId: String = ""
	@objc dynamic var createdById: String = ""
	@objc dynamic var createdBy: String = ""
	@objc dynamic var assigneeId: String = ""
	@objc dynamic var assignedTo: String = ""
	@objc dynamic var createdDate: String = ""
	@objc dynamic var duedate: String = ""
	@objc dynamic var revisedDate: String = ""
	@objc dynamic var isSpecific: String = ""
	@objc dynamic var isMeasure: String = ""
	@objc dynamic var isAttainable: String = ""
	@objc dynamic var isRealistic: String = ""
	@objc dynamic var isTimely: String = ""
	@objc dynamic var focusStatus: String = ""
	@objc dynamic var modified: String = ""
	@objc dynamic var percentageCompeleted: String = ""
    @objc dynamic var description_focus: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var workdateColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var assignedToUserImage:String = ""
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var label: String = ""

    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()


    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }

	public func mapping(map: Map) {
		id <- map["id"] 
		title <- map["focus_title"] 
		companyName <- map["company_name"]
        companyId <- map["company_id"]
        departmentId <- map["department_id"]
		department <- map["department_name"] 
		createdById <- map["created_by_id"] 
		createdBy <- map["created_by"] 
		assigneeId <- map["assignee_id"] 
		assignedTo <- map["assigned_to"] 
		createdDate <- map["created_date"] 
		duedate <- map["duedate"] 
		revisedDate <- map["revised_date"] 
		isSpecific <- map["is_specific"] 
		isMeasure <- map["is_measure"] 
		isAttainable <- map["is_attainable"] 
		isRealistic <- map["is_realistic"] 
		isTimely <- map["is_timely"] 
		focusStatus <- map["focus_status"] 
		modified <- map["modified"] 
		percentageCompeleted <- map["percentage_compeleted"]
        description_focus <- map["description"]
        revisedColor <- map["revised_color"]
        workdateColor <- map["workdate_color"]
        duedateColor <- map["duedate_color"]
        assignedToUserImage <- map["assigned_to_user_image"]
        label <- map["label"]
        related_title <- map["related_title"]
        relatedId <- map["related_id"]

        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }

	}
} 

