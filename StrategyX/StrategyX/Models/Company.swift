import Foundation 
import ObjectMapper
import RealmSwift

public class Company:Object, Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var name: String = ""
    @objc dynamic var department: Int = -1
	@objc dynamic var user: Int = -1
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
	public func mapping(map: Map) {
		id <- map["id"] 
		name <- map["name"]
        department <- map["department"]
		user <- map["user"]
	}
}

class CompanyList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var company: [Company] = []
    
    func mapping(map: Map) {
        company <- map["data"]
    }
}

