import Foundation 
import ObjectMapper
import RealmSwift

class Timezone: Object,Mappable {
    
    @objc dynamic var timezoneId: String = ""
    @objc dynamic var timezoneName: String = ""
    @objc dynamic var timezoneCode: String = ""
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        timezoneId <- map["timezone_id"]
        timezoneName <- map["timezone_name"]
        timezoneCode <- map["timezone_code"]
    }
}

class TimezoneList: Mappable {

	var list:[Timezone] = []

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		list <- map["data"]
	}
} 



