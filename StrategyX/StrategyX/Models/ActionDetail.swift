import Foundation 
import ObjectMapper 
import RealmSwift
import Realm

public class ActionDetail:Object, Mappable {
    
    @objc dynamic var id: String = ""
    @objc dynamic var actionTitle: String = ""
    @objc dynamic var actionLogid: String = ""
    @objc dynamic var action_description: String = ""
    @objc dynamic var createdById: String = ""
    @objc dynamic var createdBy: String = ""
    @objc dynamic var assigneeCompanyId: String = ""
    @objc dynamic var companyName: String = ""
    @objc dynamic var assigneeId: String = ""
    @objc dynamic var assignedTo: String = ""
    @objc dynamic var assignedDepartment = ""
    @objc dynamic var assignedDepartmentId = ""
    @objc dynamic var approvedBy: String = ""
    @objc dynamic var approvedById: String = ""
    @objc dynamic var createdDate: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var workDate: String = ""
    @objc dynamic var revisedDate: String = ""
    @objc dynamic var duration: String = ""
    @objc dynamic var percentageCompeleted: String = ""
    @objc dynamic var completedDate: String = ""
    @objc dynamic var actionlogStatus: String = ""
    @objc dynamic var modified: String = ""
    @objc dynamic var goal: String = ""
    @objc dynamic var tacticalProject: String = ""
    @objc dynamic var risk: String = ""
    @objc dynamic var strategyGoal: String = ""
    @objc dynamic var relatedTitle: String = ""
    @objc dynamic var label: String = ""
    @objc dynamic var note_file_id: String = ""
    @objc dynamic var note_file: String = ""
    @objc dynamic var related_title: String = ""
    @objc dynamic var relatedId: Int = 0
    @objc dynamic var assignedToUserImage:String = ""
    @objc dynamic var requestedRevisedDate: String = ""
    @objc dynamic var reasonForReviseDueDate: String = ""
    @objc dynamic var requestFlag:Int = 0
    @objc dynamic var approveFlag:Int = 0

    
    var tagsList = List<TagList>()
//    var fileList = List<FileList>()
    var subTaskList = List<SubTaskList>()
    
    var attachment = List<AttachmentIdea>()
    var voiceNotes = List<VoiceNotes>()
    
    
    override public static func primaryKey() -> String?{
        return "id"
    }
    
    required convenience public init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        actionTitle <- map["title"]
        actionLogid <- map["id"]
        action_description <- map["description"]
        createdById <- map["created_by_id"]
        createdBy <- map["created_by"]
        assigneeCompanyId <- map["assignee_company_id"]
        companyName <- map["company_name"]
        assigneeId <- map["assignee_id"]
        assignedTo <- map["assigned_to"]
        assignedDepartment <- map["assigned_department"]
        assignedDepartmentId <- map["assigned_department_id"]
        approvedBy <- map["approved_by"]
        approvedById <- map["approved_by_id"]
        createdDate <- map["created_date"]
        duedate <- map["duedate"]
        workDate <- map["work_date"]
        revisedDate <- map["revised_date"]
        duration <- map["duration"]
        percentageCompeleted <- map["percentage_compeleted"]
        completedDate <- map["completed_date"]
        actionlogStatus <- map["actionlog_status"]
        modified <- map["modified"]
        goal <- map["goal"]
        tacticalProject <- map["tactical_project"]
        risk <- map["risk"]
        strategyGoal <- map["strategy_goal"]
        relatedTitle <- map["related_title"]
        label <- map["label"]
        note_file_id <- map["note_file_id"]
        note_file <- map["note_file"]
        relatedId <- map["related_id"]
        assignedToUserImage <- map["assigned_to_user_image"]
        requestedRevisedDate <- map["requested_revised_date"]
        reasonForReviseDueDate <- map["reason_for_revise_duedate"]
        requestFlag <- map["request_flag"]
        approveFlag <- map["approve_flag"]

        /*
        var file: [FileList]?
        file <- map["file"]
        if let files = file {
            for tag in files {
                self.fileList.append(tag)
            }
        }
        */
        var subTask: [SubTaskList]?
        subTask <- map["subTask"]
        if let tags = subTask {
            for tag in tags {
                self.subTaskList.append(tag)
            }
        }
        
        var list: [TagList]?
        list <- map["tag"]
        if let tags = list {
            for tag in tags {
                self.tagsList.append(tag)
            }
        }

        var listsIdea: [AttachmentIdea]?
        listsIdea <- map["attachment"]
        if let items = listsIdea {
            for idea in items {
                self.attachment.append(idea)
            }
        }
        
        var lists: [VoiceNotes]?
        lists <- map["voiceNotes"]
        if let items = lists {
            for note in items {
                self.voiceNotes.append(note)
            }
        }
        
    }
    
    
    
}

