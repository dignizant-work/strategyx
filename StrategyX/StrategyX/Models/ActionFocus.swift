import Foundation 
import ObjectMapper
import RealmSwift

public class ActionFocus:Object, Mappable {

    @objc dynamic var id: String = ""
	@objc dynamic var title: String = ""
    @objc dynamic var revised_date: String = ""
    @objc dynamic var duedate: String = ""
    @objc dynamic var revisedColor:String = ""
    @objc dynamic var duedateColor:String = ""
    @objc dynamic var status:String = ""

    required convenience public init?(map: Map) {
        self.init()
    }
    
    /*override public static func primaryKey() -> String?{
        return "id"
    }*/
    
	public func mapping(map: Map) {
		id <- map["id"] 
		title <- map["title"]
        revised_date <- map["revised_date"]
        duedate <- map["duedate"]
        duedateColor <- map["duedate_color"]
        revisedColor <- map["revised_color"]
        status <- map["status"]
	}
}

class ActionFocusList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var actionFocus: [ActionFocus] = []
    
    func mapping(map: Map) {
        actionFocus <- map["data"]
    }
}

