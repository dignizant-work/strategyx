import Foundation 
import ObjectMapper
import RealmSwift

class Categories: Mappable {

	@objc dynamic var id: String = ""
	@objc dynamic var category_name: String = ""

    required convenience init?(map: Map) {
        self.init()
    }

	func mapping(map: Map) {
		id <- map["id"] 
		category_name <- map["category_name"]
	}
}

class CategoriesList:Mappable  {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    var categories: [Categories] = []
    
    func mapping(map: Map) {
        categories <- map["data"]
    }
}

