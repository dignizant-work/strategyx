//
//  DayAxisValueFormatter.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts

public class DayAxisValueFormatter: NSObject, IAxisValueFormatter {
    weak var chart: LineChartView?
    
    var arrayXlabels : [String] = []
    
    init(chart: LineChartView,array:[String]) {
        self.chart = chart
        self.arrayXlabels = array
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return arrayXlabels[Int(value) % arrayXlabels.count]
    }
    
}
