//
//  Enum+Protocol.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import LocalAuthentication

enum indicatorAllignment:Int {
    case center = 0, left, right
}
enum UserRole:Int {
    //role_id = 1(superAdmin) , role_id = 2(CompanyAdmin) , role_id = 3(staffAdmin), role_id = 4(Manager), role_id = 6(staffLite user)
    case superAdminUser = 1
    case companyAdminUser = 2
    case staffUser = 3
    case manager = 4
    case staffLite = 6
}
enum StrategyCardType:String {
    case vision         = "Vision"
    case mission        = "Mission"
    case value          = "Values"
    case culture        = "Culture"
    case coreBussiness  = "Core Business"
    case coreCompetencies  = "Core Competencies"
    case strengths = "Strengths"
    case weaknesses = "Weaknesses"
    case opportunities = "Opportunities"
    case threats = "Threats"

}

//For LeftSideMenuDelegate
@objc protocol LeftSideMenuDelegate:class {
    @objc func redirectToLeftSideMenuViewController(strTitle:String)
}

//For RightSideMenuDelegate
@objc protocol RightSideMenuDelegate:class {
    @objc func redirectToRightSideMenuViewController(strTitle:String)
}

//For Filter
enum FilterType:Int {
    case strategy               = 1
    case risks                  = 2
    case tacticalProject        = 3
    case focus                  = 4
    case myRole                 = 5
    case action                 = 6
    case timelog                = 7
    case reports                = 8
    case approvals              = 9
    case successFactor          = 10
    case overViewReport         = 11
    case cVScReport             = 12
    case ideaAndProblem         = 13
}

// For Strategy SubLisController
enum StrategySubControllerType:Int {
    case step0 = 0
    case step1 = 1
    case step2 = 2
}
protocol StrategySubControllerDelegate: class {
    func didSelectTableViewCell(row:Int,stpe:StrategySubControllerType,selectedData:SubListSelectedDataModel)
//    func editSelectTableViewCell(row:Int,stpe:StrategySubControllerType,theModel:Any)
    func actionsSelectTableViewCell(row:Int,stpe:StrategySubControllerType,theModel:Any)
    func moreActionTablviewCell(step:StrategySubControllerType,theModel:Any,at indexRow:Int,itemName:String)
}

// For Risks SubLisController
enum RisksSubControllerType:Int {
    case step0 = 0
    case step1 = 1
}
protocol RisksSubControllerDelegate: class {
    func didSelectTableViewCell(row:Int,stpe:RisksSubControllerType)
}

// For Custom Calendar
enum CalendarReoccuring:String {
    case everyDay           = "Every day"
    case everyWeek          = "Every week"
    case mondayToFriday     = "Monday to Friday"
    case everyMonth         = "Every month"
    case everyYear          = "Every year"
    case custom             = "Custom"
}
enum CalendarFrequencyType:String {
    case daily              = "Daily"
    case weekly             = "Weekly"
    case monthly            = "Monthly"
    case yearly             = "Yearly"
}

enum CalendarMonthlyOnTheDayNumberType:String {
    case first              = "first"
    case second             = "second"
    case third              = "third"
    case fourth             = "fourth"
    case fifth              = "fifth"
    case last               = "last"
}

enum CalendarMonthlyOnTheDayType:String {
    case sunday             = "Sunday"
    case monday             = "Monday"
    case tuesday            = "Tuesday"
    case wednesday          = "Wednesday"
    case thursday           = "Thursday"
    case friday             = "Friday"
    case saturday           = "Saturday"
    case day                = "day"
    case weekday            = "weekday"
    case weekendDay         = "weekend day"
}

enum CategoryFor:String
{
    case risk = "0"
    case tacticalProject = "1"
}

enum viewType:String{
    case edit = "0"
    case add = "1"
}

// For chart
enum ChartFlagView:Int {
    case action = 1, focus, tacticalProject, risk, strategy, ideaAndProblem, primary, secondary, strategyGoal
}

// Profile
enum editUserType{
    case editProfile
    case editUser
    case addUser
}

// create account
enum phoneCodeTimezone{
    case phoneCode
    case timeZone
}

// more action
enum MoreAction:String {
    case chart = "Chart"
    case edit = "Edit"
    case delete = "Delete"
    case action = "Action"
    case secondary = "Secondary"
    case goal = "Goal"
    case archived = "Archive"
    case convert = "Convert"
    case approve = "Approve"
}

// Type
enum SelectedFileType:String {
    case audio = "audio"
    case file = "file"
}

enum IdeaAndProblemType:String {
    case idea = "Idea"
    case problem = "Problem"
}

// For Redirection
enum RedirectToModule:String {
    case actionLog          = "action_logs"
    case primaryArea        = "strategy_primary_areas"
    case secondaryArea      = "strategy_secondary_areas"
    case strategyGoal       = "strategy_goals"
    case risk               = "risks"
    case tacticalProject    = "tactical_projects"
    case focuses            = "focuses"
    case myRole             = "my_roles"
    case ideaNproblem       = "ideas_and_problems"
    case successFactor      = "critical_success_factors"
    case whatsNew           = "feature_updates"
    case trainingVideo      = "training_videos"
    case softwareUpdate     = "app_update"
}

// For Notification Type
enum Notificationtype:String {
    case approval           = "approval"
    case assign             = "assign"
    case completed          = "completed"
    case revisedDate        = "revised_date"
    case note               = "note"
    case attachment         = "attachment"
    case whatsNew           = "whats_new"
    case riskLevel          = "risk_level"
    case trainingVideo      = "training_video"
    case idea               = "idea"
    case problem            = "problem"
    case softwareUpdate     = "app_update"
    case requestedToRevise = "requested_to_revise"
    case revisedDateApproved = "revised_date_approved"
    case revisedDateDeclined = "revised_date_declined"
}

//For Comment Type
enum CommentType:String {
    case veryHappy          = "very_happy"
    case happy              = "happy"
    case notSure            = "not_sure"
    case notHappy           = "not_happy"
}

//For Reports Selection section
enum ReportsSelectionSection:String {
    case overdueItems = "Overdue Items"
    case dueDateAdjusted = "Due Date Adjusted"
}

//For Reports By
enum ReportsSelectionBy:String {
    case department = "By Department"
    case person = "Person"
}

// Video Type
enum VideoType:String {
    case vimeo = "vimeo"
    case youtube = "youtube"
}
