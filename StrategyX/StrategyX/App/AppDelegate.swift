//
//  AppDelegate.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 StrategyX. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SlideMenuControllerSwift
//import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import Intercom
import SwiftyJSON

//import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var intercomView:UIView?

    var menuNotificationCount:Int = 0 {
        didSet {
            guard let tabbarController = (AppDelegate.shared.window?.rootViewController as? SlideMenuController)?.mainViewController as? MainTabBarController  else { return }
            guard let tabbarItem = tabbarController.tabBar.items  else { return }
            if menuNotificationCount != 0 {
                tabbarItem[4].badgeValue = "\(menuNotificationCount)"
            } else {
                tabbarItem[4].badgeValue = nil
            }
        }
    }
    
    var approvalNotificationCount:Int = 0 {
        didSet {
            guard let tabbarController = (AppDelegate.shared.window?.rootViewController as? SlideMenuController)?.mainViewController as? MainTabBarController  else { return }
            guard let tabbarItem = tabbarController.tabBar.items  else { return }
            if approvalNotificationCount != 0 {
                tabbarItem[3].badgeValue = "\(approvalNotificationCount)"
            } else {
                tabbarItem[3].badgeValue = nil
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSPlacesClient.provideAPIKey(APIKey.key_google_place_picker)
        UserDefault.shared.getUserData()
        /*
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
         */
        UIViewController().createAudioDirectory()
        UIViewController().listOFAudioFilesFromDirectory()
        UIViewController().createAttachmentDirectory()
        
        //Firebase Config
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        //Register Remote notification
        registerForRemoteNotification()
        
        //Intercom
        Intercom.setApiKey(intercom_IOSApiKey, forAppId: intercom_AppID)
        
//        //Stripe
//        STPPaymentConfiguration.shared().publishableKey = stripe_Test
        
        //IQKeyboardManager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            Utility.shared.appCurrentVersion = appVersion
        }
        
        //Tabbar
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: appBgLightGrayColor], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: appGreen], for: .selected)
        
        //NavigationBar
        //UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        //UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = white
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = appGreen
        
        if Utility.shared.userData != nil {
            updateDefaultValues()
            setupSideMenuController()
        } else {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
       if Utility.shared.userData != nil {
            getPushCount(completion: { [weak self] in
                AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
                AppDelegate.shared.updateMenuNotificationCount()
                
                if let sideMenuContoller = self?.window?.rootViewController as? SlideMenuController {
                    if let parent = sideMenuContoller.rightViewController as? MenuVC, parent.isViewLoaded {
                        parent.viewWillAppear(true)
                    }
                    if let _ = sideMenuContoller.mainViewController as? MainTabBarController, AppDelegate.shared.approvalNotificationCount > 0 {
                        //                        tabbarController.selectedIndex = 3
                        //                        ((tabbarController.viewControllers?[3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC)?.reInitData()
                    }
                }
            })
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    //MARK:- Intercom
    func setupIntercomView() -> UIView {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        AppDelegate.shared.intercomView = UIView(frame: CGRect.init(x: UIScreen.main.bounds.width - 80, y: statusBarHeight + 5, width: 70, height: 34.0))
//        AppDelegate.shared.intercomView?.cornerRadius = 20.0
        AppDelegate.shared.intercomView?.backgroundColor = UIColor.clear
        
        let btn = UIButton(frame: CGRect.init(x: 0, y: 0, width: 70, height: 34))
        btn.tag = 144
        btn.addTarget(self, action: #selector(onBtnIntercomAction(_:)), for: .touchUpInside)
        
//        let textLabel = UILabel(frame: CGRect.init(x: 10, y: 7, width: 60, height: 18))
//        textLabel.textColor = white
//        textLabel.setFont = "R13"
//        textLabel.text = "Live Chat"

        
        let image = UIImageView(frame: CGRect.init(x: 0, y: 1, width: 70, height: 32))
        image.image = UIImage(named: "ic_live_chat")
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        
        let label = UILabel(frame: CGRect.init(x: image.frame.origin.x + image.frame.width - 15, y: image.frame.origin.y - 4, width: 14, height: 14))
        label.backgroundColor = ColorType().setColor(index: 9)
        label.textAlignment = .center
        label.textColor = white
        label.setFont = "R9"
        label.cornerRadius = 7
        label.clipsToBounds = true
        label.tag = 143
        label.text = "0"
        label.isHidden = true

//        AppDelegate.shared.intercomView?.addSubview(textLabel)
        AppDelegate.shared.intercomView?.addSubview(image)
        AppDelegate.shared.intercomView?.addSubview(label)
        AppDelegate.shared.intercomView?.addSubview(btn)
//        AppDelegate.shared.intercomView?.clipsToBounds = true
        AppDelegate.shared.intercomView?.tag = 4843549
        return AppDelegate.shared.intercomView!
    }
    
    @objc func onBtnIntercomAction(_ sender:UIButton) {
        Intercom.presentMessenger()
    }
    
    @objc func intercomUnreadCountChange() {
        let count = Int(Intercom.unreadConversationCount())
        print("cout:=",count)
        guard let lable = AppDelegate.shared.intercomView?.viewWithTag(143) as? UILabel else { return }
        
        lable.text = "\(count)"
        lable.isHidden = count == 0
        if count < 9 {
            lable.frame.size = CGSize(width: 14, height: 14)
        } else if count > 9 && count < 99 {
            lable.frame.size = CGSize(width: 18, height: 14)
        } else if count > 99 {
            lable.frame = CGRect(x: lable.frame.origin.x - 2, y: lable.frame.origin.y, width: 24, height: 14)
        }
    }
    
    func intercomLogin() {
//        Intercom.setLauncherVisible(true)
        Intercom.registerUser(withUserId: Utility.shared.userData.id, email: Utility.shared.userData.email)
        
        //Custom        
        let userAttributes = ICMUserAttributes()
        userAttributes.name = Utility.shared.userData.username
        userAttributes.email = Utility.shared.userData.email
        let companyAttribute = ICMCompany()
        companyAttribute.companyId = Utility.shared.userData.companyId
        companyAttribute.name = Utility.shared.userData.companyName
        userAttributes.companies = [companyAttribute]
        userAttributes.customAttributes = [
            "app_id":intercom_AppID,
//            "name": Utility.shared.userData.username,
//            "email" : Utility.shared.userData.email,
            "Department":Utility.shared.userData.departmentName,
            "Role":Utility.shared.userData.userType,
//            "company": ["company_id": Utility.shared.userData.companyId, "name": Utility.shared.userData.companyName],
            "created_at": Utility.shared.userData.createdDateTime,
            "custom_launcher_selector":"#live-chart"]
        Intercom.updateUser(userAttributes)
        
        NotificationCenter.default.addObserver(self, selector: #selector(intercomUnreadCountChange), name: NSNotification.Name.IntercomUnreadConversationCountDidChange, object: nil)
        
        print("Scale:=\(UIScreen.main.scale)")
        
        if UIApplication.shared.statusBarFrame.height <= 20.0 {
            Intercom.setBottomPadding(UIScreen.main.bounds.height - (UIApplication.shared.statusBarFrame.height + 70))
        } else {
            Intercom.setBottomPadding(UIScreen.main.bounds.height - (UIApplication.shared.statusBarFrame.height + 105))
        }
        
//        Intercom.setBottomPadding(UIScreen.main.bounds.height - (UIApplication.shared.statusBarFrame.height + 70))
        
    
        
//        window?.addSubview(intercomView!)
//        let window = UIApplication.shared.keyWindow
//
//        window?.addSubview(intercomView!)
//        window?.bringSubviewToFront(intercomView!)
        
        
        
    }
    func setIntercomVisiblity(isHidden:Bool) {
//        Intercom.setLauncherVisible(!isHidden)
//        AppDelegate.shared.intercomView?.isHidden = isHidden
    }
    func intercomLogout() {
//        Intercom.setLauncherVisible(false)
        AppDelegate.shared.intercomView?.removeFromSuperview()
        AppDelegate.shared.intercomView = nil
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.IntercomUnreadConversationCountDidChange, object: nil)
        Intercom.logout()
    }

    //MARK:- Member Function
    func setupSideMenuController() {
        guard let window = window else { return}
        let mainVC = MainTabBarController()
        let leftVC = LeftSideMenuVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        leftVC.delegate = mainVC
        let rightVC = MenuVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        rightVC.delegate = mainVC
        
        let slideMenuController = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftVC, rightMenuViewController: rightVC)
        slideMenuController.changeLeftViewWidth(UIScreen.main.bounds.width - 85)
        slideMenuController.changeRightViewWidth(UIScreen.main.bounds.width - 85)
        slideMenuController.opacityView.backgroundColor = appGreen
        slideMenuController.delegate = mainVC
        SlideMenuOptions.opacityViewBackgroundColor = appGreen
        SlideMenuOptions.contentViewScale = 1
        SlideMenuOptions.shadowRadius = 0.5
        SlideMenuOptions.shadowOpacity = 0.2
        SlideMenuOptions.contentViewOpacity = 0.8
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.simultaneousGestureRecognizers = false
        SlideMenuOptions.rightBezelWidth = 16.0
        SlideMenuOptions.leftBezelWidth = 16.0
        
        window.rootViewController = slideMenuController
        window.makeKeyAndVisible()
//        return slideMenuController
    }
    func updateDefaultValues() {
        if let rid = Int(Utility.shared.userData.roleId), let role = UserRole(rawValue: rid) {
            UserDefault.shared.userRole = role
        }
//        UserDefault.shared.userRole = UserRole(rawValue: Int(Utility.shared.userData.roleId)!)!
        
    }
    func updateMenuNotificationCount() {
        var count = 0
        let notificationUnreadCount = (Int(Utility.shared.pushData.notificationUnread) ?? 0)
        count += notificationUnreadCount
        let trainningVideoCount = (Int(Utility.shared.pushData.trainningVideoCount) ?? 0)
        count += trainningVideoCount
        let whatsnewUnreadCount = (Int(Utility.shared.pushData.whatsnewUnread) ?? 0)
        count += whatsnewUnreadCount
        let softwareUpdateCount = (Int(Utility.shared.pushData.softwareUpdateCount) ?? 0)
        count += softwareUpdateCount
        AppDelegate.shared.menuNotificationCount = count
        UIApplication.shared.applicationIconBadgeNumber = count + (Int(Utility.shared.pushData.approvalCount) ?? 0)
    }
    func clearNotificationCount() {
        Utility.shared.pushData.approvalCount = ""
        Utility.shared.pushData.trainningVideoCount = ""
        Utility.shared.pushData.whatsnewUnread = ""
        Utility.shared.pushData.notificationUnread = ""
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func redirectToRootLoginViewController() {
        AppDelegate.shared.intercomLogout()
        UserDefault.shared.removeUserData()
        
        guard let window = window else { return }
        let nvc = AppStoryBoardString.authentication.instance.instantiateViewController(withIdentifier: "NavigationLogin") as! UINavigationController
        window.rootViewController = nil
        window.rootViewController = nvc
        window.makeKeyAndVisible()        
    }
    func presentOnWindow(vc:UIViewController) {
        guard let window = self.window else { return }
        window.rootViewController?.present(vc, animated: true, completion: nil)
    }
    func fadeInRootViewController() -> UIView {
        let imageView = UIImageView(frame: UIScreen.main.bounds)
        imageView.alpha = 1.0
        imageView.tag = 21863
        guard let window = self.window else { return imageView }
        imageView.image = window.rootViewController!.view.asImage()
        window.rootViewController?.view.addSubview(imageView)
        return imageView
    }
    func fadeOutRootViewController() {
        guard let window = self.window else { return }
        if let view = window.rootViewController?.view.viewWithTag(21863) {
            view.removeFromSuperview()
        }
    }

}
//MARK:- UNUserNotificationCenterDelegate
extension AppDelegate:UNUserNotificationCenterDelegate {
    func getPushCount(completion:@escaping() -> Void) {
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_ios_current_version:Utility.shared.appCurrentVersion]
        
        CommanListWebservice.shared.getPushCount(parameter:parameter , success: { [weak self] in
            if let lastDate = Utility.shared.pushData.lastVersionUpdateDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType11) {
                Utility.shared.pushData.lastVersionUpdateDate = lastDate
            } else {
                Utility.shared.pushData.lastVersionUpdateDate = "" 
            }
            
            if let sideMenuContoller = self?.window?.rootViewController as? SlideMenuController {
                if let tabbarController = sideMenuContoller.mainViewController as? MainTabBarController, Utility.shared.pushData.lastFeedbackSubmitted != 0 {
                    tabbarController.redirectToSatisfactionPopupVC()
                }
            }
            
            completion()
        }, failed: { (error) in
            completion()
        })
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                if granted {
                    //                    UIApplication.shared.registerForRemoteNotifications()
                    //                    Default.set(true, forKey: Key_IsNotificationAllow)
                }
                else {
                    //                    Default.set(false, forKey: Key_IsNotificationAllow)
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        Messaging.messaging().apnsToken = deviceToken
        push_token = Messaging.messaging().fcmToken ?? ""
        
        print("FCM Token = \(Messaging.messaging().fcmToken ?? "nill")")
        
//        UserDefault.setValueForKey(key: APIKeys.KEY_DEVICE_TOKEN, value: token)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
        /*
        if error.localizedDescription == "remote notifications are not supported in the simulator" {
            UserDefault.setValueForKey(key: APIKeys.KEY_DEVICE_TOKEN, value: "1468fuhysifs6598464fsdfs4fds8sgsgsg54asdas")
        }*/
        
    }
    
    // For < 10 This Notification Method is called
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Push for ios < 10 version")
        print("User Info = ",userInfo)
        
        if application.applicationState == .active {
            
        }
        else if application.applicationState == .background {
            //            let info = JSON(userInfo)
            //            print("info:=\(info)")
            //            handlePushData(infoData: info)
        }
        
    }
    
    
    // MARK: UNUserNotificationCenter Delegate // >= iOS 10
    // While App on Foreground mode......    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
         // checked push checked
        print("Push for ios 10 version")
        
        print("User Info = ",notification.request.content.userInfo)
        let userInfo = notification.request.content.userInfo as NSDictionary
        
        print("info:=\(userInfo)")
        let dataJson = JSON(userInfo)
        print("dataJson:=",dataJson)
        let dataString = dataJson["gcm.notification.data"].stringValue
        let json = JSON(parseJSON: dataString)
        print("json:=",json)
        let notificationCount = Int(json["notification_count"]["notification_unread"].stringValue) ?? 0
        let approvalCount = Int(json["notification_count"]["approval"].stringValue) ?? 0
        let whatsNewCount = Int(json["notification_count"]["whatsnew_unread"].stringValue) ?? 0
        let traininingVideoCount = Int(json["notification_count"]["trainning_video_count"].stringValue) ?? 0
        let appVersion = Int(json["notification_count"]["ios_app_update_count"].stringValue) ?? 0
        let sentApprovalCount = Int(json["notification_count"]["sent_approval_count"].stringValue) ?? 0
        let yourApprovalCount = Int(json["notification_count"]["for_your_approval_count"].stringValue) ?? 0
        let dueDateApprovalCount = Int(json["notification_count"]["duedate_request"].stringValue) ?? 0

        if Utility.shared.userData != nil {
            
            Utility.shared.pushData.approvalCount = "\(approvalCount)"
            Utility.shared.pushData.notificationUnread = notificationCount == 0 ? "" : "\(notificationCount)"
            Utility.shared.pushData.whatsnewUnread = whatsNewCount == 0 ? "" : "\(whatsNewCount)"
            Utility.shared.pushData.trainningVideoCount = traininingVideoCount == 0 ? "" : "\(traininingVideoCount)"
            Utility.shared.pushData.softwareUpdateCount = "\(appVersion)"
            Utility.shared.pushData.sentApprovalCount = "\(sentApprovalCount)"
            Utility.shared.pushData.forYourApprovalCount = "\(yourApprovalCount)"
            Utility.shared.pushData.dueDateRequestCount = "\(dueDateApprovalCount)"

            AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
            AppDelegate.shared.updateMenuNotificationCount()

            if let sideMenuContoller = self.window?.rootViewController as? SlideMenuController {
                if let parent = sideMenuContoller.rightViewController as? MenuVC {
                    parent.viewWillAppear(true)
                }
                
                
                if let tabbarController = sideMenuContoller.mainViewController as? MainTabBarController {
                    if let vc = tabbarController.viewControllers?[tabbarController.selectedIndex] as? UINavigationController, let index = vc.viewControllers.firstIndex(where: {($0 as? GeneralSettingsVC) != nil }) {
                        (vc.viewControllers[index] as? GeneralSettingsVC)?.updateSoftwareUpdateCount()
                    }
                    if let nvc = tabbarController.viewControllers?[3] as? UINavigationController, nvc.viewControllers.count > 0 {
                        if tabbarController.selectedIndex == 3 {
                            (nvc.viewControllers[0] as? ApprovalsVC)?.reInitData()
                        }
                        (nvc.viewControllers[0] as? ApprovalsVC)?.updateCountFromPush()
                    }
                    //                        tabbarController.selectedIndex = 0
                    //                        ((tabbarController.viewControllers?[3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC)?.reInitData()
                }
            }
            
//            AppDelegate.shared.menuNotificationCount = (Int(Utility.shared.pushData.notificationUnread) ?? 0) + (Int(Utility.shared.pushData.trainningVideoCount) ?? 0) + (Int(Utility.shared.pushData.whatsnewUnread) ?? 0)
            /*
            getPushCount(completion: { [weak self] in
                AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
                AppDelegate.shared.updateMenuNotificationCount()


                if let sideMenuContoller = self?.window?.rootViewController as? SlideMenuController {
                    if let parent = sideMenuContoller.rightViewController as? MenuVC, parent.isViewLoaded {
                        parent.viewWillAppear(true)
                    }
                    if let tabbarController = sideMenuContoller.mainViewController as? MainTabBarController, AppDelegate.shared.approvalNotificationCount > 0 {
//                        ((tabbarController.viewControllers?[3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC)?.reInitData()
                    }
                }
                
            })*/
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tap on Banner Push for ios 10 version")
        
        print("User Info = ",response.notification.request.content.userInfo)
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        
        print("info:=\(userInfo)")
    
        let dataJson = JSON(userInfo)
        let dataString = dataJson["gcm.notification.data"].stringValue
        let json = JSON(parseJSON: dataString)
        print("json:=",json)
//        let notificationCount = Int(json["notification_count"]["notification_unread"].stringValue) ?? 0
//        let approvalCount = Int(json["notification_count"]["approval"].stringValue) ?? 0
        if let object = json["notification_count"].dictionaryObject {
            Utility.shared.pushNotificationData = PushNotificationData(JSON: object)
            Utility.shared.pushNotificationData?.relatedId = json["notification_count"]["related_id"].stringValue
        }
        
        if Utility.shared.userData != nil {
            AppDelegate.shared.updateMenuNotificationCount()
//            Utility.shared.pushData.approvalCount = "\(approvalCount)"
            
            /*Utility.shared.pushData.approvalCount = approvalCount
            Utility.shared.pushData.notificationUnread = notificationCount == 0 ? "" : "\(notificationCount)"
            
            AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
            var count = 0
            let notificationUnreadCount = (Int(Utility.shared.pushData.notificationUnread) ?? 0)
            count += notificationUnreadCount > 0 ? 1 : 0
            let trainningVideoCount = (Int(Utility.shared.pushData.trainningVideoCount) ?? 0)
            count += trainningVideoCount > 0 ? 1 : 0
            let whatsnewUnreadCount = (Int(Utility.shared.pushData.whatsnewUnread) ?? 0)
            count += whatsnewUnreadCount > 0 ? 1 : 0
            AppDelegate.shared.menuNotificationCount = count*/
            
            if let sideMenuContoller = self.window?.rootViewController as? SlideMenuController {
                if let tabbarController = sideMenuContoller.mainViewController as? MainTabBarController {
                    tabbarController.selectedIndex = 0
                    tabbarController.checkPushNotificationData()
                    if let nvc = tabbarController.viewControllers?[3] as? UINavigationController, nvc.viewControllers.count > 0 {
                        if tabbarController.selectedIndex == 3 {
                            (nvc.viewControllers[0] as? ApprovalsVC)?.reInitData()
                        }
                        (nvc.viewControllers[0] as? ApprovalsVC)?.updateCountFromPush()
                    }
                }
            }
            
            getPushCount(completion: { [weak self] in
                AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
                AppDelegate.shared.updateMenuNotificationCount()
                

                if let sideMenuContoller = self?.window?.rootViewController as? SlideMenuController {
                    if let parent = sideMenuContoller.rightViewController as? MenuVC, parent.isViewLoaded {
                        parent.viewWillAppear(true)
                    }
                    
                    if let tabbarController = sideMenuContoller.mainViewController as? MainTabBarController {
                        if let vc = tabbarController.viewControllers?[tabbarController.selectedIndex] as? UINavigationController, let index = vc.viewControllers.firstIndex(where: {($0 as? GeneralSettingsVC) != nil }) {
                            (vc.viewControllers[index] as? GeneralSettingsVC)?.updateSoftwareUpdateCount()
                        }
                        if let nvc = tabbarController.viewControllers?[3] as? UINavigationController, nvc.viewControllers.count > 0 {
                            if tabbarController.selectedIndex == 3 {
                                (nvc.viewControllers[0] as? ApprovalsVC)?.reInitData()
                            }
                            (nvc.viewControllers[0] as? ApprovalsVC)?.updateCountFromPush()
                        }
//                        tabbarController.selectedIndex = 0
//                        ((tabbarController.viewControllers?[3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC)?.reInitData()
                    }
                }
            })
        }
        completionHandler()
    }
    
}

//MARK:- Firebase MessagingDelegate
extension AppDelegate:MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("messaging:=\(messaging)")
    }
}
