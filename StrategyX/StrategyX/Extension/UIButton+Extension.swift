//
//  UIButton+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Loady

extension UIButton {
    
    @IBInspectable var LanguageKey: String {
        
        get {
            return self.title(for: .normal)!
        }
        set {
            
            self.setTitle(LanguageManager.sharedInstance.getTranslationForKey(newValue), for: .normal)
        }
    }
    
    @IBInspectable var setFont:String? {
        set {
            self.titleLabel?.font = FontStyle().setFontType(strType: newValue ?? "")
        }
        get {
            return ""
        }
    }
    
    @IBInspectable var setFontColor:Int {
        set {
            self.setTitleColor(ColorType().setColor(index: newValue), for: .normal)
        }
        get {
            return -1
        }
    }
    /*
    func startIndicator(alignment:RNActivityIndicatorAlignment = .right,indicatorColor:UIColor = appBlack, isShowDisableText:Bool = true, disableTextColor:UIColor = appBlack) {
        if let activityBtn = self as? RNLoadingButton {
            activityBtn.isLoading = true
            activityBtn.hideTextWhenLoading = false
            activityBtn.hideImageWhenLoading = true
            activityBtn.activityIndicatorAlignment = alignment
            activityBtn.activityIndicatorColor = indicatorColor
            activityBtn.isEnabled = false
            activityBtn.backgroundColor = activityBtn.backgroundColor?.withAlphaComponent(0.5)
            if isShowDisableText {
                activityBtn.setTitleColor(disableTextColor, for: UIControl.State.disabled)
                activityBtn.setTitle("connecting...", for: UIControl.State.disabled)
            }
        }
    }
    func stopIndicator() {
        if let activityBtn = self as? RNLoadingButton {
            activityBtn.isLoading = false
            activityBtn.isEnabled = true
            activityBtn.backgroundColor = activityBtn.backgroundColor?.withAlphaComponent(1.0)
        }
    }*/
    
    func loadingIndicator(_ show: Bool, allignment:indicatorAllignment = .center,_ color:UIColor = appGreen, showLoadingText:Bool = false ,loadingTextColor:UIColor = appBlack) {
        let image = self.imageView?.image
        let tag = 808404
        if show {
            self.isEnabled = false
           // self.alpha = 0.5
            self.setImage(nil, for: .normal)
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            switch allignment {
            case .center:
                indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
                break
            case .left:
                indicator.center = CGPoint(x: 20, y: buttonHeight/2)
                break
            case .right:
                indicator.center = CGPoint(x: buttonWidth - 20 - indicator.frame.size.width, y: buttonHeight/2)
                break
            }
            if showLoadingText {
                self.setTitleColor(loadingTextColor, for: UIControl.State.disabled)
                self.setTitle("Please Wait", for: UIControl.State.disabled)
                self.titleLabel?.adjustsFontSizeToFitWidth = true
            }
            indicator.tag = tag
            indicator.color = color
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.titleLabel?.adjustsFontSizeToFitWidth = false
            self.isEnabled = true
            //self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
            self.setImage(image, for: .normal)
        }
    }
    
}

//MARK:- Loady
extension UIButton {
    func startLoadyIndicator(topLineColor:UIColor = appBlack) {
        if let loadyView = self as? LoadyButton {
            if loadyView.loadingIsShowing() {
                stopLoadyIndicator()
                return
            }
            
            // sets animation type
//            loadyView.animationType = LoadyAnimationType.topLine()
            loadyView.setAnimation(LoadyAnimationType.topLine())
            // sets the color that fills the button after percent value changed
            loadyView.backgroundFillColor = appBlack
            
            // sets the indicator color above the button
            loadyView.loadingColor = topLineColor
            
            // sets the indictore view color (dark or light) inside the button
//            loadyView.indicatorViewStyle = .
            
            // some animations have image inside (e.g appstore pause image), this line sets that image
//            loadyView.pauseImage = UIImage(named: "pause.png")
            
            // starts loading animation
            loadyView.startLoading()
            
            // some animations have filling background, or change the circle stroke, this sets the filling percent, number is something between 0 to 100
//            loadyView.fillTheButton(with: 10)
            
            // some animations have circular loading , this sets the percents of circle that are completed, number is something between 0 to 100
//            loadyView.fillTheCircleStrokeLoadingWith(percent: 25)
            
            loadyView.isEnabled = false
            
            loadyView.backgroundColor = loadyView.backgroundColor?.withAlphaComponent(0.5)
            
//            switch type {
//            case .all:
//                break
//            case .android:
//                break
//            case .appstore:
//                break
//            case .backgroundHighlighter:
//                break
//            case .circleAndTick:
//                break
//            case .downloading:
//                break
//            case .fourPhases:
//                break
//            case .indicator:
//                loadyView.backgroundColor = loadyView.backgroundColor?.withAlphaComponent(0.5)
//
//                break
//            case .topLine:
//                loadyView.backgroundColor = loadyView.backgroundColor?.withAlphaComponent(0.5)
//                break
//            case .none:
//                break
//            }
        }
    }
    
    func stopLoadyIndicator() {
        (self as? LoadyButton)?.stopLoading()
        (self as? LoadyButton)?.isEnabled = true
        self.backgroundColor = self.backgroundColor?.withAlphaComponent(1.0)
    }
}
