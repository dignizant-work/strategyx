//
//  UITextView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension UITextView {
    
    @IBInspectable var LanguageKey: String {
        get {
            return self.text!
        }
        set {
            self.text = LanguageManager.sharedInstance.getTranslationForKey(newValue)
            
        }
    }
    
    @IBInspectable var setFont:String? {
        set {
            self.font = FontStyle().setFontType(strType: newValue ?? "")
        }
        get {
            return ""
        }
    }
    
    @IBInspectable var setFontColor:Int {
        set {
            self.textColor = ColorType().setColor(index: newValue)
        }
        get {
            return -1
        }
    }
}
