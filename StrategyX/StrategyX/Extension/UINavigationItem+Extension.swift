//
//  UINavigationItem+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem {
    
    @IBInspectable var LanguageKey: String {
        
        get {
            return self.title ?? ""
        }
        set {
            
            self.title = LanguageManager.sharedInstance.getTranslationForKey(newValue)
        }
    }
}
