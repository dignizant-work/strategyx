//
//  UITextField+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension UITextField {
    
    @IBInspectable var LanguageKey: String {
        
        get {
            return self.placeholder!
        }
        set {
            // self.placeholder = LanguageManager.sharedInstance.getTranslationForKey(newValue, value: "")
            self.placeholder = LanguageManager.sharedInstance.getTranslationForKey(newValue)
        }
    }
    
    @IBInspectable var setFont:String? {
        set {
            self.font = FontStyle().setFontType(strType: newValue ?? "")
        }
        get {
            return ""
        }
    }
    
    @IBInspectable var setFontColor:Int {
        set {
            self.textColor = ColorType().setColor(index: newValue)
        }
        get {
            return -1
        }
    }
    
    @IBInspectable var placeHolderColor: Int {
        get {
            return -1
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: ColorType().setColor(index: newValue) as Any])
        }
    }

}
