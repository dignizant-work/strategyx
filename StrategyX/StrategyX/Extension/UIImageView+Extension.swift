//
//  UIImageView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage
extension UIImageView {
    
    func sd_setImageLoadMultiTypeURL(url:String,placeholder:String = "") {
        let placeImg = UIImage(named:placeholder)
        if url.lowercased().contains(".gif") {
            SDWebImageManager.shared.loadImage(with: URL.init(string: url), options: SDWebImageOptions.queryMemoryData, progress: nil) { (image, data, error, imageCacheType, isDownLoaded, url) in
                if let imageData = data {
                    self.image = UIImage.animatedImage(data: imageData)
                }
            }
        } else {
            self.sd_setImage(with: URL(string: url), placeholderImage: placeImg)
        }
    }

    func sd_setImageLoadMultiTypeURLForList(url:String,completion:@escaping(Bool) -> Void) {
//        let placeImg = UIImage(named:placeholder)
        self.isHidden = !url.isEmpty ? true : false
        if url.lowercased().contains(".gif") {
            SDWebImageManager.shared.loadImage(with: URL.init(string: url), options: SDWebImageOptions.queryMemoryData, progress: nil) { (image, data, error, imageCacheType, isDownLoaded, uRl) in
                if let imageData = data, let geturl = uRl, geturl.absoluteString == url {
                    
                    self.image = UIImage.animatedImage(data: imageData)
                    self.isHidden = false
                    completion(false)
                    return
                }
                completion(true)
                return
            }
        } else {
//            self.sd_setImage(with: URL(string: url), placeholderImage: placeImg)
            self.sd_setImage(with: URL(string: url)) { (image, error, imageCachType, uRl) in
                if let img = image, let geturl = uRl, geturl.absoluteString == url {
                    self.image = img
                    self.isHidden = false
                    completion(false)
                    return
                }
                completion(true)
                return
            }
        }
    }
    
}

import ImageIO
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}



extension UIImage {
    
    static func animatedImage(data: Data) -> UIImage? {
        guard let source: CGImageSource = CGImageSourceCreateWithData(data as CFData, nil), CGImageSourceGetCount(source) > 1 else {
            return UIImage(data: data)
        }
        
        // Collect key frames and durations
        var frames: [(image: CGImage, delay: TimeInterval)] = []
        for i: Int in 0 ..< CGImageSourceGetCount(source) {
            guard let image = CGImageSourceCreateImageAtIndex(source, i, nil), let frame = CGImageSourceCopyPropertiesAtIndex(source, i, nil) as? [String: Any], let gif = frame["{GIF}"] as? [String: Any] else {
                continue
            }
            
            // Mimic WebKit approach to determine frame delay
            if let delay = gif["UnclampedDelayTime"] as? TimeInterval, delay > 0.0 {
                frames.append((image, delay)) // Prefer "unclamped" delay time
            } else if let delay = gif["DelayTime"] as? TimeInterval, delay > 0.0 {
                frames.append((image, delay))
            } else {
                frames.append((image, 0.1)) // WebKit default
            }
        }
        
        // Convert key frames to animated image
        var images: [UIImage] = []
        var duration: TimeInterval = 0.0
        for frame in frames {
            let image = UIImage(cgImage: frame.image)
            for _ in 0 ..< Int(frame.delay * 100.0) {
                images.append(image) // Add fill frames
            }
            duration += frame.delay
        }
        return UIImage.animatedImage(with: images, duration: duration)
    }
}
