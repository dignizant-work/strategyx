//
//  String+Extension.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    var length: Int {
        return self.count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    var isContainsLetters : Bool{
        let letters = CharacterSet.letters
        return self.rangeOfCharacter(from: letters) != nil
    }
    
    var isContainsDigit : Bool{
        let letters = CharacterSet.decimalDigits
        return self.rangeOfCharacter(from: letters) != nil
    }
    var isContainsUpperCase : Bool{
        let letters = CharacterSet.uppercaseLetters
        return self.rangeOfCharacter(from: letters) != nil
    }
    var isContainsLowerCase : Bool{
        let letters = CharacterSet.lowercaseLetters
        return self.rangeOfCharacter(from: letters) != nil
    }
    
    func trimmed() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func capitalizedFirst() -> String {
        let first = self[self.startIndex ..< self.index(startIndex, offsetBy: 1)]
        let rest = self[self.index(startIndex, offsetBy: 1) ..< self.endIndex]
        return first.uppercased() + rest.lowercased()
    }
    
    func convertToUTCDateWithGMT0(formate:String = "yyyy-MM-dd") -> Date? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = formate
        dateFormate.timeZone = TimeZone.init(secondsFromGMT: 0)
        return dateFormate.date(from: self)
    }
    
    func convertToDate(formate:String = "yyyy-MM-dd") -> Date? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = formate
//        dateFormate.timeZone = TimeZone.init(secondsFromGMT: 0)
        return dateFormate.date(from: self)
    }
    
    func convert(fromformate:String = "yyyy-MM-dd", toFormate:String = "yyyy-MM-dd") -> String? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = fromformate
        if let date = dateFormate.date(from: self) {
            dateFormate.dateFormat = toFormate
            return dateFormate.string(from: date)
        }
        return ""
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    
    func validateURL() -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let predicate = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        //            NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        //        let urlTest = NSPredicate.withSubstitutionVariables(predicate)
        
        return predicate.evaluate(with: self)
    }
    
    /*func validateURL() -> Bool {
        let urlRegEx = "^(http(s)?://)?((www)?\\.)?[\\w]+\\.[\\w]+"
        let predicate = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
//            NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
//        let urlTest = NSPredicate.withSubstitutionVariables(predicate)
        
        return predicate.evaluate(with: self)
    }*/

    
    func isValidURL() -> Bool {
        
//        let urlRegEx = "^((http|https)://)?(\\w+[.])+com$"
//        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
//        return urlTest.evaluate(with: self)
        
        guard let url = URL(string: self) else {
                return false
        }
        return UIApplication.shared.canOpenURL(url)
    }
    
    func random(length: Int = 20) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    
    func UTCToLocal(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    var youtubeID: String?
    {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
    //MARK:- Convert Local To UTC Date by passing date formats value
    func localToUTC(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9","*","+"]
        return Set(self).isSubset(of: nums)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func createBulletString(arr:[String]) -> String {
        var strBullet = ""
        let bullet = "\u{2022}"
        for item in arr {
            strBullet += (bullet + " " + item + "\n")
        }
        return strBullet
    }
    
    func setFirstNLastNameChar() -> String {
        let arrStr = self.components(separatedBy: " ")
        var text = ""
        if arrStr.count > 0 {
            text = "\(arrStr[0].trimmed().prefix(1))"
            if arrStr.indices.contains(1) {
                text += "\(arrStr[1].trimmed().prefix(1))"
            }
        }
        return text
    }
    
    func acronym() -> String {
        return String(self.components(separatedBy: .whitespacesAndNewlines).reduce("") { first, next in
            (first) + (next.first.map { String($0) } ?? "")
            }.prefix(2))
//        return self.components(separatedBy: .whitespaces).filter { !$0.isEmpty }.reduce("") { $0.0 + String($0.1.characters.first!) }
    }
    
    func attributedString(color: UIColor,font:UIFont) -> NSAttributedString? {
        let attributes1 = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor: color
            ] as [NSAttributedString.Key : Any]
        
        let attributedString = NSAttributedString(string: "\(self)", attributes: attributes1)
        
        return attributedString
    }
    
    func isContainAudioOrVideoFile() -> Bool {
        let strUrl = (self as NSString).lastPathComponent
        if strUrl.contains(".mp3") || strUrl.contains(".mp4") || strUrl.contains(".wav") || strUrl.contains(".ogg") || strUrl.contains(".m4a") {
            return true
        }
        return false
    }
}


extension Data {
    func isLessThen10MB() -> Bool {
        let fileSize = Double(Float(self.count) / Float(1048576.0)) //Convert in to MB
        print("File size in MB: ", fileSize)
        return fileSize < 10
    }
}
