//
//  UIView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var setBackGroundColor: Int {
        get {
            return -1
        }
        set {
            self.backgroundColor = ColorType().setColor(index: newValue)
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: Int {
        get {
            return -1
        }
        set {
            self.layer.borderColor = ColorType().setColor(index: newValue)?.cgColor
        }
        /*
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }*/
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    
    /** This is the function to get subViews of a view of a particular type
     */
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /** This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T */
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
    
    func rotateArrow(in interval:Double = 0.3, at Angle:CGFloat,transorm:CGAffineTransform = CGAffineTransform.identity)  {
        if Angle == 0 && transorm == CGAffineTransform.identity {
            self.transform = CGAffineTransform.identity
        } else {
            UIView.animate(withDuration: interval) {
                self.transform = CGAffineTransform(rotationAngle: Angle)
            }
        }
    }
    
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
   
    /*
    func rotate(degrees: CGFloat) {
        rotate(radians: CGFloat.pi * degrees / 180.0)
    }
    
    func rotate(radians: CGFloat) {
        self.transform = CGAffineTransform(rotationAngle: radians)
    }*/
    
    func addDashedBorder(color:UIColor,cornerRadius:CGFloat = 4) {
//        let color = color.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func allButtons(isEnable:Bool) {
        let allButtons  = self.allSubViewsOf(type: UIButton.self)
        allButtons.forEach({
            $0.isEnabled = isEnable
        })
    }
    
    func backGroundMessageView(strMsg:String = "No data found",textFont:UIFont = B15, textColor:UIColor = appGreen) -> UIView {
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 10, y: 0, width: self.bounds.size.width - 20, height: self.bounds.size.height))
        noDataLabel.numberOfLines = 0
        noDataLabel.tag           = 97890
        noDataLabel.text          = strMsg
        noDataLabel.font          = textFont
        noDataLabel.textColor     = textColor
        noDataLabel.textAlignment = .center
        (self as? UITableView)?.separatorStyle = .none
        return noDataLabel
    }
    
    func rotate(degrees: CGFloat) {
        rotate(radians: CGFloat.pi * degrees / 180.0)
    }
    
    func rotate(radians: CGFloat) {
        self.transform = CGAffineTransform(rotationAngle: radians)
    }
}

public extension UIView {
    static func loadFromXib<T>(withOwner: Any? = nil, options: [AnyHashable : Any]? = nil) -> T where T: UIView
    {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: "\(self)", bundle: bundle)
        
        guard let view = nib.instantiate(withOwner: withOwner, options: options as? [UINib.OptionsKey : Any]).first as? T else {
            fatalError("Could not load view from nib file.")
        }
        return view
    }
}

extension UICollectionViewCell {
    func cellPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}

extension UITableViewCell {
    func cellPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}

extension UIView {
    func viewNextPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}

