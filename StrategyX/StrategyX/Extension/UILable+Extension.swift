//
//  UILable+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension UILabel {
    
    @IBInspectable var LanguageKey: String {
        
        get {
            return self.text!
        }
        set {
            self.text = LanguageManager.sharedInstance.getTranslationForKey(newValue)
            
        }
    }
    @IBInspectable var setFont:String? {
        set {
            self.font = FontStyle().setFontType(strType: newValue ?? "")
        }
        get {
            return ""
        }
    }
    
    @IBInspectable var setFontColor:Int {
        set {
            self.textColor = ColorType().setColor(index: newValue)
        }
        get {
            return -1
        }
    }
    
    @IBInspectable var showAstrike:Bool {
        set {
            let attribute = NSMutableAttributedString(string: self.text ?? "", attributes: [NSAttributedString.Key.font : self.font ?? UIFont.systemFont(ofSize: 15),
                                                                                            NSAttributedString.Key.foregroundColor : self.textColor ?? white])
            if newValue == true {
                let astrikeAttribute = NSAttributedString(string: " *", attributes: [NSAttributedString.Key.font : B12,
                                                                                     NSAttributedString.Key.foregroundColor: appPink])
                
                attribute.append(astrikeAttribute)
            }
            self.attributedText = NSAttributedString.init(attributedString: attribute)
        }
        get {
            return false
        }
    }
    
    /*
    @IBInspectable var setFont:String? {
        get {
            return self.font.fontName
        }
        set {
            self.font = FontStyle().setFontType(strType: setFont ?? "")
        }
    }
    
    @IBInspectable var setFontColor:Int {
        get {
            return -1
        }
        set {
            self.textColor = ColorType().setColor(index: setFontColor)
        }
    }*/

    
}

@IBDesignable class LabelButton: UILabel {
    
    var onClick: () -> Void = {}
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onClick()
    }
}

@IBDesignable class ImageButton: UIImageView {
    
    var onClick: () -> Void = {}
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onClick()
    }
    
}
