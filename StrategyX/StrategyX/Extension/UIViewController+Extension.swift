//
//  UIViewController+Extension.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import AVKit

extension UIViewController {
    
    @objc func backAction(isAnimation:Bool = true) {
        self.navigationController?.popViewController(animated: isAnimation)
    }
    
    func backToRootAction(isAnimation:Bool = true) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setLogoOnNavigationHeader() {
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "ic_strategyx_logo"))
    }
    
    //Registered Nib
    func nib(_ controllerClass: AnyClass) -> UINib {
        let identifier = String.className(controllerClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        return nib
    }
    
    func push(vc:UIViewController) {
        let view = AppDelegate.shared.fadeInRootViewController()
        
        guard let nvc = self.navigationController else { return }
        nvc.pushViewController(vc, animated: false)
        
        UIView.animate(withDuration: 0.75, delay: 0.05, animations: {
            view.alpha = 0.15
        }, completion: {(finished) in
            view.alpha = 0
            view.removeFromSuperview()
        })
    }
    func showAlert(msg:String) {
        let alertView = UIAlertController(title: "StrategyX",
                                          message: msg,
                                          preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alertView.addAction(okAction)
        self.present(alertView, animated: true)
    }
    func showAlertAtBottom(message:String) {
        Utility.shared.showAlertAtBottom(strMessage: message)
    }
    
    func showAlertAction(title:String = "StrategyX",msg:String,buttonNo:String = "No",buttonYes:String = "Yes", completion:@escaping() -> Void) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let noAction = UIAlertAction(title: buttonNo, style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        let yesAction = UIAlertAction(title: buttonYes, style: .default) { (action) in
            completion()
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func CheckValidFile(strExtension:String) -> Bool
    {
        /*
         files :  'jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf', 'xls', 'doc', 'docx', 'xlsx', 'csv' , 'txt'
         voiceNotes :  'wav', 'aif', 'mp3', 'mid' , 'mp4' , 'WAV', 'AIF', 'MP3', 'MID' , 'MP4'
         company & user image :  'jpeg', 'jpg', 'png', 'gif'
         files :  100MB
         voiceNotes :   100MB
         company & user image :  200kb
         */
        let arrayExtensions = ["jpeg","jpg","gif","png","bmp","pdf","doc","docx","xls","xlsx","msg","csv","txt","mp4","ppt","pptx","pps","wav","m4a","ogg"]
        
        if(arrayExtensions.contains(strExtension.lowercased()))
        {
            return true
        }
        return false
        
    }
    func GetFileExtension(strFileName:String) -> String
    {
        let arrayNames = strFileName.split(separator: ".")
        if(arrayNames.count > 0)
        {
            if arrayNames.count > 1
            {
                return String(arrayNames.last ?? "")
            }
        }
        return ""
    }
    func removeFileFromDocumentDirectory(itemName:String, fileExtension: String) {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(itemName).\(fileExtension)"
        do {
            try fileManager.removeItem(atPath: filePath)
            print("remove file")
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    func setAudioFileToDocumentDirectory(audioData:Data,audioNameWithExtension:String) -> String? {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return nil
        }
        let dataPath = dirPath.appending("/AudioFiles")
//        let dataPath = dirPath


        let filePath = "\(dataPath)/\(audioNameWithExtension)"
        if fileManager.fileExists(atPath: filePath) {
            do {
                try fileManager.removeItem(atPath: filePath)
                print("remove file")
            } catch let error as NSError {
                print(error.debugDescription)
            }
        }
        do {
            try audioData.write(to: URL.init(fileURLWithPath: filePath))
        } catch {
            print("error:=",error)
            return nil
        }
        return filePath
    }
    func createAudioDirectory() {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)

        guard let dirPath = paths.first else {
            return
        }
        let dataPath = dirPath.appending("/AudioFiles")
        print("dataPath:=",dataPath)
        if fileManager.fileExists(atPath: dataPath) {
            print("Path already exist")
            return
        }
        do {
            try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            
        } catch {
            print("error:=",error.localizedDescription)
        }
    }
    func listOFAudioFilesFromDirectory() {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        guard let dirPath = paths.first else { return }
        let dataPath = dirPath.appending("/AudioFiles")
        print("dataPath:=",dataPath)
        do {
           let files =  try fileManager.contentsOfDirectory(atPath: dataPath)
            print("Audio Files:=",files)
        } catch {
            print("error:=",error.localizedDescription)
        }
        
    }
    func removeAudioDirectory() {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        guard let dirPath = paths.first else { return }
        let dataPath = dirPath.appending("/AudioFiles")
        print("dataPath:=",dataPath)
        do {
            try fileManager.removeItem(atPath: dataPath)
        } catch {
            print("error:=",error.localizedDescription)
        }
    }
    func ConvertAudio(audioUrl: URL, outputUrl: URL? = nil,resultClosure: @escaping (URL?) -> Void)
    {
        let audioURL = audioUrl
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        
        let outputUrl = dirPaths[0].appendingPathComponent("\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)")
        print("outputUrl - ",outputUrl)
        //strAudioPath = outputUrl
        let asset = AVAsset.init(url: audioURL)
        
        let exportSession = AVAssetExportSession.init(asset: asset, presetName: AVAssetExportPresetMediumQuality)
        
        // remove file if already exits
        
        let fileManager = FileManager.default
        do{
            try? fileManager.removeItem(at: outputUrl)
            print("convertion success")
        }
        
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.outputURL = outputUrl
        exportSession?.metadata = asset.metadata
        exportSession?.exportAsynchronously(completionHandler: {
            if (exportSession?.status == .completed)
            {
                resultClosure(outputUrl)
                print("AV export succeeded.")
                // outputUrl to post Audio on server
            }
            else if (exportSession?.status == .cancelled)
            {
                resultClosure(nil)
                print("AV export cancelled.")
            }
            else
            {
                resultClosure(nil)
                print ("Error is \(String(describing: exportSession?.error))")
            }
        })
    }
    
    func createAttachmentDirectory() {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        guard let dirPath = paths.first else {
            return
        }
        let dataPath = dirPath.appending("/Attachment")
        print("dataPath:=",dataPath)
        if fileManager.fileExists(atPath: dataPath) {
            print("Path already exist")
            return
        }
        do {
            try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
            
        } catch {
            print("error:=",error.localizedDescription)
        }
    }
    func setAttachmentFileToDocumentDirectory(data:Data,fileNameWithExtension:String) -> String? {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return nil
        }
        let dataPath = dirPath.appending("/Attachment")
        //        let dataPath = dirPath
        
        
        let filePath = "\(dataPath)/\(fileNameWithExtension)"
        if fileManager.fileExists(atPath: filePath) {
            do {
                try fileManager.removeItem(atPath: filePath)
                print("remove file")
            } catch let error as NSError {
                print(error.debugDescription)
            }
        }
        do {
            try data.write(to: URL.init(fileURLWithPath: filePath))
        } catch {
            print("error:=",error)
            return nil
        }
        return filePath
    }
    func removeAttachmentDirectory() {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        
        guard let dirPath = paths.first else { return }
        let dataPath = dirPath.appending("/Attachment")
        print("dataPath:=",dataPath)
        do {
            try fileManager.removeItem(atPath: dataPath)
        } catch {
            print("error:=",error.localizedDescription)
        }
    }
    func fileType() -> [String] {
        let types: [String] = [kUTTypePDF as String,
                               kUTTypePNG as String,
                               kUTTypeJPEG as String,
//                               kUTTypeMP3 as String,
//                               kUTTypeAudio as String,
                               kUTTypeMPEG4 as String,
                               kUTTypeGIF as String,
                               kUTTypeBMP as String,
                               kUTTypeText as String,
                               kUTTypeContent as String,
                               kUTTypePlainText as String,
                               kUTTypeWindowsExecutable as String,
                               "com.microsoft.word.doc",
                               "com.microsoft.excel.xls",
                               "com.microsoft.word.docx",
                               "com.microsoft.excel.xlsx",
                               "com.microsoft.powerpoint.​ppt",
                               "com.microsoft.powerpoint.​pptx",
//                               "com.microsoft.outlook.MSG",
//                               "com.outlook.microsoft.MSG",
//                               "com.outlook.MSG",
//                               "com.microsoft.MSG",
//                               "outlook.microsoft.MSG",
//                               "outlook.MSG",
//                               "outlook.file.MSG",
//                               "fv.document.MSG",
//                               "com.outlook.document.MSG",
//                               "com.microsoft.document.MSG",
//                               "Outlook.File.MSG",
//                               "public.MSG",
//                               "public.application/vnd.ms-outlook",
//                               "public.application/octet-stream",
//                               "public.data",
                               "public.item"
                                ]
        return types
    }
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = appGreen
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor.white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    func generateThumbnail(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    //MARK:- Present Audio Player
    func presentAudioPlayer(url:String, isLocalURl:Bool = false) {
        
        var urlString = URL.init(string: url)!
        if isLocalURl {
            urlString = URL.init(fileURLWithPath: url)
        }
        let playerItem = AVPlayerItem(url: urlString)

        let videoPlayer = AVPlayer.init(playerItem: playerItem)
        videoPlayer.rate = 1.0
        
        Utility.shared.playerViewController = AVPlayerViewController()
        Utility.shared.playerViewController?.player = videoPlayer
        Utility.shared.playerViewController?.delegate = self

        
        self.present(Utility.shared.playerViewController!, animated: true) {
            Utility.shared.playerViewController!.player!.play()
        }
    }
    //MARK:- Present WebView
    func presentWebViewVC(url:String, isLocal:Bool = false) {
        let vc = WebViewVC.init(nibName: "WebViewVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.setTheData(url: url, isLocal: isLocal)
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
   
}
extension UIViewController:AVPlayerViewControllerDelegate {
    public func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        print("error:=",error.localizedDescription)
    }
    public func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
    }
}
extension UIViewController {
    func downloadAudioFileWebService(strURl:String, completion:@escaping(Data) -> Void) {
        Utility.shared.showActivityIndicator()
        /*CommanListWebservice.shared.downloadFileAtDestination(strUrl: strURl, success: { (strFile) in
            DispatchQueue.main.async {
                Utility.shared.stopActivityIndicator()
                completion(strFile)
            }
//            guard let data = fileData else {return}
            
        }, failed: { [weak self] (error) in
            DispatchQueue.main.async {
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
            }
        })*/
        
        CommanListWebservice.shared.downloadFile(strUrl: strURl, success: { (fileData) in
            DispatchQueue.main.async {
                Utility.shared.stopActivityIndicator()
            }
            guard let data = fileData else {return}
            completion(data)
        }, failed: { [weak self] (error) in
            DispatchQueue.main.async {
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
            }
//            Utility.shared.stopActivityIndicator()
        })
    }
}
