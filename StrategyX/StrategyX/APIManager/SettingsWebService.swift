//
//  SettingsWebService.swift
//  StrategyX
//
//  Created by Haresh on 28/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class SettingsWebService: NSObject {

    static let shared = SettingsWebService()
    
    func getAllFeatureList(parameter:[String:Any],success:@escaping(_ list:[FeaturesUpdate], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_FeatureUpdatesList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let featurelist = FeaturesUpdateAllList(JSON: data) {
                            success(featurelist.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                //                failed(ValidationTexts.SOMETHING_WENT_WRONG)
            //                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func userFeedBack(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_UserFeedback, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func userRating(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_UserRating, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllApprovalList(parameter:[String:Any],success:@escaping(_ list:[ActionsList], _ offset:Int, _ approveCount:Int,_ sentCount:Int,_ requestCount:Int) -> Void, failed:@escaping(String,_ approveCount:Int,_ sentCount:Int,_ requestCount:Int) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION,0,0,0)
            return
        }
        WebService().post(endpoint: GET_ActionsList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let approvalList = ActionsAllList(JSON: data) {
                            success(approvalList.list,value[APIKey.key_next_offset].intValue,value[APIKey.key_approval_count].intValue,value[APIKey.key_sent_approval_count].intValue,value[APIKey.key_date_request].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue,value[APIKey.key_approval_count].intValue,value[APIKey.key_sent_approval_count].intValue,value[APIKey.key_date_request].intValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue,value[APIKey.key_approval_count].intValue,value[APIKey.key_sent_approval_count].intValue,value[APIKey.key_date_request].intValue)
                    return
                }
                //                failed(ValidationTexts.SOMETHING_WENT_WRONG)
            //                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION,0,0,0)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT,0,0,0)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG,0,0,0)
                }
                break
            }
        }
    }
    
    func approveActionsApproval(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ChangeActionStatus, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllSatisfactionList(parameter:[String:Any],success:@escaping(_ list:[Satisfaction], _ feedBackCount:SatisfactionFeedBackCount, _ offset:Int) -> Void, failed:@escaping(String, _ feedBackCount:SatisfactionFeedBackCount) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION, SatisfactionFeedBackCount())
            return
        }
        WebService().post(endpoint: POST_SatisfactionList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject, let feedbackData = value[APIKey.key_feedback_counts].dictionaryObject {
                        //                        print("data:=",data)
                        if let Satisfactionlist = SatisfactionAllList(JSON: data) {
                            if let feedbackCount = SatisfactionFeedBackCount(JSON: feedbackData) {
                                success(Satisfactionlist.list,feedbackCount, value[APIKey.key_next_offset].intValue)
                                return
                            }
                            success(Satisfactionlist.list,SatisfactionFeedBackCount(), value[APIKey.key_next_offset].intValue)

                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    if let feedbackData = value[APIKey.key_feedback_counts].dictionaryObject, let feedbackCount = SatisfactionFeedBackCount(JSON: feedbackData) {
                        failed(value[APIKey.key_msg].stringValue, feedbackCount)
                        return
                    }
                    failed(value[APIKey.key_msg].stringValue,SatisfactionFeedBackCount())
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue,SatisfactionFeedBackCount())
                    return
                }
                //                failed(ValidationTexts.SOMETHING_WENT_WRONG)
            //                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION, SatisfactionFeedBackCount())
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT, SatisfactionFeedBackCount())
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG, SatisfactionFeedBackCount())
                }
                break
            }
        }
    }
    
    func addSatisfaction(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_AddSatisfaction, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
