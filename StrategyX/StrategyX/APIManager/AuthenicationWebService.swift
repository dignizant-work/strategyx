//
//  AuthenicationWebService.swift
//  StrategyX
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class AuthenicationService: NSObject {
    static let shared = AuthenicationService()
    
    func login(parameter:[String:Any], success:@escaping(_ user : User,_ data:JSON) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: Login, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        print("data:=",data)
                        
                        
//                        UserDefault.shared.saveUserData(user: value[APIKey.key_data])
//                        success()
//                        return
                        if let user = User(JSON: data) {
                            /*if let rid = Int(user.roleId), let role = UserRole(rawValue: rid) {
                                UserDefault.shared.userRole = role
                            }
                            print("UserDefault.shared.userRole:=",UserDefault.shared.userRole)
//                            OfflineManger.shared.writeSingle(type: user)
                            user.companyId = user.companyId == "0" ? "" : user.companyId
                            Utility.shared.userData = user
                            var userData = value[APIKey.key_data]
                            userData["company_id"].stringValue = user.companyId
                            UserDefault.shared.saveUserData(user: userData)*/
                            success(user, JSON(data))
                            return
                        }
                    }
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func forgotPassword(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: ForgotPassword, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func verificationCode(parameter:[String:Any], success:@escaping(String,String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: POST_SendVerificationCode, parameter: parameter) { (result) in
            switch result {

            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let token = value[APIKey.key_data][APIKey.key_otp_token].string {
                        success(token,value[APIKey.key_msg].stringValue)
                    }
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func resendVerificationCode(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: POST_ResendVerificationCode, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func verifyCode(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: POST_VerificationCode, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func changePassword(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: ChangePassword, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func signup(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: Register, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func resendEmail(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: ResendEmail, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func logout(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: Logout, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getTermsConditions(parameter:[String:Any], success:@escaping(_ detail:TermsCondition) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kGetTermsCondition, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = TermsCondition(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                }
                failed(value[APIKey.key_msg].stringValue)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
