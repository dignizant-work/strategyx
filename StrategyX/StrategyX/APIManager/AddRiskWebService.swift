//
//  AddRiskWebService.swift
//  StrategyX
//
//  Created by Jaydeep on 12/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddRiskWebService: NSObject {
    
    static let shared = AddRiskWebService()
    
    func postAddRisk(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping() -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: kPostAddRisk, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func postEditRisk(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping(RiskDetail) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: kPostEditRisk, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = RiskDetail(JSON: data) {
                        success(detail)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func postAddRiskNotes(parameter:[String:Any], success:@escaping() -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostAddNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    success()
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }

    
    func getRiskCategories(parameter:[String:Any], success:@escaping([RiskCategory]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_RiskCategory, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let categoriesList = RiskCategoryList(JSON: data) {
                            success(categoriesList.riskCategory)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getRiskSubCategories(parameter:[String:Any], success:@escaping([RiskSubCategory]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_RiskSubCategory, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let categoriesList = RiskSubCategoryList(JSON: data) {
                            success(categoriesList.riskSubCategory)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getRiskConsequenceCategories(parameter:[String:Any], success:@escaping([RiskConsequenceCategory]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_RiskConsequenceCategory, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let categoriesList = RiskConsequenceCategoryList(JSON: data) {
                            success(categoriesList.riskConsequenceCategory)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
