//
//  ReportsWebServices.swift
//  StrategyX
//
//  Created by Haresh on 21/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportsWebServices: NSObject {
    static let shared = ReportsWebServices()

    func getReportsDepartment(parameter:[String:Any], success:@escaping([Department]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ReportDepartmentList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let departmentList = DepartmentList(JSON: data) {
                            success(departmentList.department)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getReportUserAssignList(parameter:[String:Any], success:@escaping([ReportAssignUser]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ReportUserList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let assignList = ReportAssignUserAllList(JSON: data) {
                            success(assignList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getNonComplianceChartList(parameter:[String:Any], success:@escaping([ReportChart]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_NonCompliance, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ReportChartAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getNonComplianceByDepartmentChartList(parameter:[String:Any], success:@escaping([ReportChartByDepartment]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_NonComplianceDepartment, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ReportChartByDepartmentAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getDueDateAdjustedChartList(parameter:[String:Any], success:@escaping([ReportChart]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_DueDateAdjusted, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ReportChartAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getDueDateAdjustedBydepartmentChartList(parameter:[String:Any], success:@escaping([ReportChartByDepartment]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_DueDateAdjustedBydepartment, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ReportChartByDepartmentAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getCreatedVSCreatedList(parameter:[String:Any], success:@escaping([CreatedVSCompleted], _ offset:Int, _ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_CreatedVSCompleted, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let createdList = CreatedVSCompletedAllList(JSON: data) {
                            success(createdList.list, value[APIKey.key_next_offset].intValue, value[APIKey.key_assign_user_flag].intValue)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getPriorityOverviewList(parameter:[String:Any], success:@escaping([PriorityOverview],PriorityOverviewCount, _ offset:Int, _ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_PriorityOverview, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let priorityOverviewList = PriorityOverviewAllList(JSON: data) ,let count = value[APIKey.key_count].dictionaryObject,  let countModel = PriorityOverviewCount(JSON: count){
                            success(priorityOverviewList.list,countModel, value[APIKey.key_next_offset].intValue, value[APIKey.key_assign_user_flag].intValue)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
