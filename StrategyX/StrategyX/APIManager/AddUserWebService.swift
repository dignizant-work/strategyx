//
//  CompanyWebService.swift
//  StrategyX
//
//  Created by Jaydeep on 04/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
class AddUserWebService: NSObject {
    
    static let shared = AddUserWebService()
    
    func getAllTimezoneList(parameter:[String:Any],success:@escaping(_ list:[Timezone]) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_TimeZone, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        if let timezoneList = TimezoneList(JSON: data) {
                            success(timezoneList.list)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getCompanyDepartment(parameter:[String:Any], success:@escaping([Department]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_CompanyDepartment, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let departmentList = DepartmentList(JSON: data) {
                            success(departmentList.department)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getCompanyManagers(parameter:[String:Any], success:@escaping([Managers]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_UserManagers, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let managersList = ManagersList(JSON: data) {
                            success(managersList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func uploadAddUserAttachment(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        print("files:=",files)
        print("withName:=",withName)
        print("withFileName:=",withFileName)
        print("mimeType:=",mimeType)
        WebService().postUsingMultiPartData(endpoint: POST_AddUser, parameter: parameter,files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success(result[APIKey.key_msg].stringValue)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
//            failed(ValidationTexts.SOMETHING_WENT_WRONG)
        }
    }
    
    func uploadEditUserAttachment(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(_ msg:String, _ userlist:UserList) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        print("files:=",files)
        print("withName:=",withName)
        print("withFileName:=",withFileName)
        print("mimeType:=",mimeType)
        WebService().postUsingMultiPartData(endpoint: POST_EditUser, parameter: parameter,files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = UserList(JSON: data) {
                        success(result[APIKey.key_msg].stringValue,detail)
                        return
                    }
                }
                //success(result[APIKey.key_msg].stringValue,)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
            //            failed(ValidationTexts.SOMETHING_WENT_WRONG)
        }
    }
    
    func getUserDetail(parameter:[String:Any],success:@escaping(_ detail:UserList) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewUser, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = UserList(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getViewUserProfile(parameter:[String:Any],success:@escaping(_ user:User) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewProfile, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let user = User(JSON: data) {
                            if let rid = Int(user.roleId), let role = UserRole(rawValue: rid) {
                                UserDefault.shared.userRole = role
                            }
                            print("UserDefault.shared.userRole:=",UserDefault.shared.userRole)
                            user.companyId = Utility.shared.userData.companyId
                            user.companyName = Utility.shared.userData.companyName
                            user.accessToken = Utility.shared.userData.accessToken
                            Utility.shared.userData = user
                            var userData = value[APIKey.key_data]
                            userData["company_id"].stringValue = user.companyId
                            userData["company_name"].stringValue = user.companyName
                            userData["access_token"].stringValue = user.accessToken
//                            userData["access_token"].stringValue = parameter["access_token"] as! String
                            UserDefault.shared.saveUserData(user: userData)
                            success(user)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func uploadEditUserProfileAttachment(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(_ msg:String, _ user:User) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        print("files:=",files)
        print("withName:=",withName)
        print("withFileName:=",withFileName)
        print("mimeType:=",mimeType)
        WebService().postUsingMultiPartData(endpoint: POST_EditProfile, parameter: parameter,files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let user = User(JSON: data) {
                        if let rid = Int(user.roleId), let role = UserRole(rawValue: rid) {
                            UserDefault.shared.userRole = role
                        }
                        print("UserDefault.shared.userRole:=",UserDefault.shared.userRole)
                        user.companyId = Utility.shared.userData.companyId
                        user.companyName = Utility.shared.userData.companyName
                        user.accessToken = Utility.shared.userData.accessToken
                        Utility.shared.userData = user
                        var userData = result[APIKey.key_data]
                        userData["company_id"].stringValue = user.companyId
                        userData["company_name"].stringValue = user.companyName
//                        userData["access_token"].stringValue = parameter["access_token"] as! String
                        UserDefault.shared.saveUserData(user: userData)
                        success(result[APIKey.key_msg].stringValue,user)
                        return
                    }
                }
                //success(result[APIKey.key_msg].stringValue,)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
            //            failed(ValidationTexts.SOMETHING_WENT_WRONG)
        }
    }
   
}
