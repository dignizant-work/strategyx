//
//  VimeoConfigureService.swift
//  StrategyX
//
//  Created by iMac on 29/05/20.
//  Copyright © 2020 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON

enum VimeoAPIError: Error {
    case unknown
    case missingData
    case serialization
    case invalidData
}

class VimeoConfigureService: ConcurrentOperation<(String, String)> {

    private let vimeoConfigUrl:String
    private var task: DataRequest?
    
    init(vimeoUrl:String) {
        self.vimeoConfigUrl = vimeoUrl
    }
    // MARK: - Main

    override func main() {
        guard let url = URL.init(string: vimeoConfigUrl) else {
            cancel()
            return
        }
        
       task = Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseSwiftyJSON { (result) in
            print("url:=",url)
//            print("parameter:=",para)
        print("response:=",result.value ?? "nil")
            if let result = result.value, result["video"].exists(), result["video"]["thumbs"].exists()  {
                let videoUrl = result["video"]["thumbs"]["640"].exists() ? result["video"]["thumbs"]["640"].stringValue : result["video"]["thumbs"]["base"].stringValue + "_640.jpg"
                DispatchQueue.main.async {
                    self.complete(result: .success((self.vimeoConfigUrl, videoUrl)))
                }
            } else {
                DispatchQueue.main.async {
                    self.complete(result: .failure(VimeoAPIError.missingData))
                }
            }
        }
    }
    
    // MARK: - Cancel
    
    override func cancel() {
        task?.cancel()
        super.cancel()
    }
}
