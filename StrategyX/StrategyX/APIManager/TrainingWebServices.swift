//
//  TrainingWebServices.swift
//  StrategyX
//
//  Created by Haresh on 29/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingWebServices: NSObject {

    static let shared = TrainingWebServices()
    
    func getAllTrainingList(parameter:[String:Any],success:@escaping(_ list:[TrainingList], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_TrainingList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let trainingList = TrainingAllList(JSON: data) {
                            success(trainingList.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllTrainingVideoList(parameter:[String:Any],success:@escaping(_ list:[TrainingVideoList], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_TrainingVideoList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let trainingVideoList = TrainingAllVideoList(JSON: data) {
                            success(trainingVideoList.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
