//
//  SuccessFactorWebservice.swift
//  StrategyX
//
//  Created by Haresh on 16/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
class SuccessFactorWebservice: NSObject {
    static let shared = SuccessFactorWebservice()
    func addSuccessFactor(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddSuccessFactor, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func updateSuccessFactor(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(SuccessFactorDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_EditSuccessFactor, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = SuccessFactorDetail(JSON: data) {
                        success(detail)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getAllSuccessFactorList(parameter:[String:Any],success:@escaping(_ list:[SuccessFactorList], _ offset:Int,_ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_SuccessFactorList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let successFactorlist = SuccessFactorAllList(JSON: data) {
                            success(successFactorlist.list,value[APIKey.key_next_offset].intValue,value[APIKey.key_assign_user_flag].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getSuccessFactorDetail(parameter:[String:Any],success:@escaping(_ detail:SuccessFactorDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewSuccessFactor, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = SuccessFactorDetail(JSON: data) {
                            success(detail)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func updateSuccessFactorReport(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(SuccessFactorReports) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_ViewSuccessFactorReport, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = SuccessFactorReports(JSON: data) {
                        success(detail)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getAllSFReportChartListWebServiceList(parameter:[String:Any],success:@escaping(_ list:[SFReportChartList], _ recordName:String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_SXRReportChart, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        //                        print("data:=",data)
                        if let actionslist = SFReportChartAllList(JSON: data) {
                            success(actionslist.list,actionslist.record_for_name)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
//                            return
                        } else {
                            failed(ValidationTexts.SOMETHING_WENT_WRONG)
                        }
                    } else {
                        failed(ValidationTexts.SOMETHING_WENT_WRONG)
                    }
                    
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
//                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
