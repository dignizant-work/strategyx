//
//  WebService.swift
//  StrategyX
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON

//let BASEURL = "https://app.strategyexe.com/api-new/api/" // Production
let BASEURL = "https://test2.actionlogger.com.au/api-new/api/" // Production test 2

//let BASEURL = "https://test.actionlogger.com.au/api-new/api/" // Live test
//let BASEURL = "http://139.59.79.228/action_log/api-new/api/" // Local

let YTDBASEURL = "https://img.youtube.com/vi/"
let VIMEOBASEURL = "https://i.vimeocdn.com/video/"

let VIMEOCONFIGBASEURL = "https://player.vimeo.com/video/"


let auth = "root:123"
let authData = auth.data(using: String.Encoding.utf8)!
let base64EncodedCredential = authData.base64EncodedString()
//    .base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
let authString = "Basic \(base64EncodedCredential)"
let authorization = ["Authorization" : authString]

//Authenication
let Login = "user/login"
let Register = "user/register"
let ForgotPassword = "user/forgot_password"
let ChangePassword = "user/change_password"
let ResendEmail = "user/resendMail"
let Logout = "user/logout"
let kGetTermsCondition = "user/getStaticContent"
let POST_ResendVerificationCode = "user/resend_verification_code"
let POST_SendVerificationCode = "user/send_verification_code"
let POST_VerificationCode = "user/verify_code"

//Comman
let GET_AllCompanyList = "company/getAllCompanyList"
let kGetCategory = "tacticalproject/category"
let kGetAssignList = "user/assign_to_userlist"
let POST_StrategyAssignUserList = "strategy/StrategyAssignUserList"
let kGetDepartment = "user/user_department"
let kGetRelated = "actionlog/relatedToList"
let kGetActionRisk = "actionlog/relatedTorisk"
let kGetTacticalProject = "actionlog/relatedTotactical_project"
let kGetFocus = "actionlog/relatedToFocus"
let kGetRelatedToSuccessFactor = "actionlog/relatedToCriticalSuccessFactor"
let kGetRelatedToIdeaAndProblem = "actionlog/relatedToIdeaAndProblem"
let kGetPrimaryArea = "strategy/PrimaryAreaList"
let kGetSecondryArea = "strategy/SecondaryAreaList"
let kGetStrategyGoal = "strategy/StrategyGoalList"
let kGetTagList = "tag/tagList"
let POST_AddDepartment = "department/addDepartment"
let POST_DeleteDepartment = "department/deleteDepartment"
let POST_EditTag = "tag/editTag"
let POST_CountryCode = "user/getcountryCode"
let POST_HomeForPushData = "user/home"
let POST_CheckVersion = "user/checkVersion"


//Action
let GET_ActionsList = "actionlog/getActionList"
let POST_AddAction = "actionlog/addAction"
let POST_EditAction = "actionlog/updateAction"
let POST_ViewAction = "actionlog/viewAction"
let POST_GetActionHistory = "actionlog/getActionHistory"
let POST_ChangeActionStatus = "actionlog/changeActionStatus"
let POST_CloseHistorySave = "actionlog/CloseHistory_Save"
let POST_ChangeOrderSubTaks = "actionlog/changeOrder"
let POST_AddSubTask = "actionlog/addSubTask"
let POST_ChangeDate = "actionlog/changeDate"
let POST_CompletedPercentage = "actionlog/completePercentage"

//Risk
let kPostAddRisk = "risk/addRisk"
let kPostEditRisk = "risk/updateRisk"

//Focus
let POST_AddFocus = "focus/addFocus"
let POST_UpdateFocus = "focus/updateFocus"
let POST_FocusList = "focus/getFocusList"
let POST_FocusDetail = "focus/viewFocus"

// View Risk
let kGetViewRisk = "risk/getRiskList"
let kViewRiskDetail = "risk/viewRisk"
let kPostAddNotes = "actionlog/postNotes"
let kPostRemoveNotes = "/actionlog/removeNotes"
let kPostEditNotes = "/actionlog/editNotes"
let POST_RiskCategory = "risk/riskCategory"
let POST_RiskSubCategory = "risk/riskSubCategory"
let POST_RiskConsequenceCategory = "risk/riskConsequenceCategory"

//Strategy
let POST_AddPrimaryArea = "strategy/addPrimaryArea"
let POST_ViewPrimaryArea = "strategy/PrimaryAreaView"
let POST_PrimaryAreaList = "strategy/PrimaryAreaList"
let POST_AddSecondaryArea = "strategy/addSecondaryArea"
let POST_ViewSecondaryArea = "strategy/SecondaryAreaView"
let POST_SecondaryAreaList = "strategy/SecondaryAreaList"
let POST_AddStrategyGoal = "strategy/addStrategyGoal"
let POST_StrategyGoalList = "strategy/StrategyGoalList"
let POST_StrategyAction = "strategy/strategyAction"
let POST_ViewStrategyGoal = "strategy/StrategyGoalView"
let POST_EditStrategy = "strategy/editStrategy"
let POST_DeleteStrategy = "strategy/deleteStrategy"
let POST_ApproveStrategy = "strategy/approveStrategy"

// Tactical Project
let POST_AddTacticalProject = "tacticalproject/addTacticalproject"
let POST_GetTacticalProject = "tacticalproject/getTacticalprojectList"
let POST_ViewTacticalProject = "tacticalproject/viewTacticalproject"
let POST_UpdateTacticalProject = "tacticalproject/updateTacticalproject"

//MyRole
let POST_MyRoleList = "role/RoleList"
let POST_AddMyRole = "role/addRole"
let POST_EditMyRole = "role/editRole"
let POST_RoleAttachment = "role/RoleAttachment"
let POST_DeleteRoleAttachment = "role/deleteRoleAttachment"
let POST_AddRoleProcedure = "role/addRoleProcudere"
let POST_RoleProcedureAction = "role/RoleProcudereAction"
let POST_RoleStatus = "role/RoleStatus"

// Settings
let POST_FeatureUpdatesList = "featureupdates/FeatureUpdatesList"
let POST_UserFeedback = "user/user_feedback"
let POST_UserRating = "user/user_rating"
let POST_AllUserList = "user/getAllUserList"
let POST_SatisfactionList = "satisfaction/satisfactionList"
let POST_AddSatisfaction = "satisfaction/addSatisfaction"

// Company
let POST_StreetList = "user/getStreets"
let POST_TimeZone = "user/getTimezone"
let POST_Suburbs = "user/getstate_suburbs"
let POST_States = "user/getState"
let POST_Register = "user/register"
let POST_ViewCompany = "company/viewCompany"
let POST_EditCompany = "company/editCompany"

// Add user
let POST_CompanyDepartment = "department/get_company_department"
let POST_UserManagers = "user/managers"
let POST_UserType = "user/UserType"
let POST_AddUser = "user/addUser"
let POST_EditUser = "user/edit_user"
let POST_ViewUser = "user/viewUser"
let POST_EditProfile = "user/edit_profile"
let POST_ViewProfile = "user/viewProfile"

// Notification
let POST_Notification = "user/notification_list"

// Add Tag
let POST_AddTag = "tag/addTag"

// Chart
let POST_Chart = "actionlog/chart"

//Notes
let POST_AddNotes = "notes/addNotes"
let POST_NotesList = "notes/NoteList"
let POST_EditNotes = "notes/editNotes"

// Training
let POST_TrainingList = "training/training_section"
let POST_TrainingVideoList = "training/Training_videoList"

// Idea and Problem
let POST_ADDIdeaAndProblem = "IdeaAndProblem/add_Idea_and_problem"
let POST_IdeaAndProblemList = "IdeaAndProblem/Idea_and_problem_List"
let POST_IdeaAndProblemUpdate = "IdeaAndProblem/Idea_and_problem_update"
let POST_IdeaAndProblemLikeList = "IdeaAndProblem/likeList"
let POST_IdeaAndProblemCommentList = "IdeaAndProblem/CommentList"
let POST_IdeaAndProblemAddComment = "IdeaAndProblem/addComment"
let POST_ViewIdeaAndProblem = "IdeaAndProblem/View_idea_and_problem"
let POST_ViewIdeaAndProblemLikeUnlike = "IdeaAndProblem/Like_Unlike"
let POST_IdeaAndProblemCreatedByList = "IdeaAndProblem/created_by_List"
let POST_IdeaAndProblemAssign_List = "IdeaAndProblem/assign_List"


//Success Factor
let POST_AddSuccessFactor = "CriticalSuccessFactor/addCriticalFactor"
let POST_SuccessFactorList = "CriticalSuccessFactor/CriticalFactorList"
let POST_ViewSuccessFactor = "CriticalSuccessFactor/ViewCriticalFactor"
let POST_EditSuccessFactor = "CriticalSuccessFactor/editCriticalFactor"
let POST_ViewSuccessFactorReport = "CriticalSuccessFactor/CriticalFactorReport"


// Reports
let POST_ReportDepartmentList = "report/getReportDepartmentList"
let POST_ReportUserList = "report/getReportUserList"
let POST_NonCompliance = "report/non_compliance"
let POST_NonComplianceDepartment = "report/non_compliance_department"
let POST_DueDateAdjusted = "report/dueDateAdjusted"
let POST_DueDateAdjustedBydepartment = "report/dueDateAdjustedBydepartment"
let POST_CreatedVSCompleted = "report/created_vs_completed"
let POST_PriorityOverview = "report/priority_overview"
let POST_Report = "report/Report"
let POST_SXRReportChart = "CriticalSuccessFactor/SXR_Report_chart"

struct AlamofireManager {
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        let sessionManager = Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: nil)
        return sessionManager
    }()
}

struct WebService {
    func cancelledAllRequest() {
        Alamofire.SessionManager.default.session.getAllTasks { (urlSessionTasks) in
            urlSessionTasks.forEach({$0.cancel()})
        }
    }
    func cancelledCurrentRequestifExist(url:String) {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadDataTask, downloadDataTask) in
            if let index = sessionDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                sessionDataTask[index].cancel()
            }
            if let index = uploadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                uploadDataTask[index].cancel()
            }
            if let index = downloadDataTask.firstIndex(where: { ($0.currentRequest?.url?.absoluteString ==  url) }) {
                downloadDataTask[index].cancel()
            }
        }
    }
    func get(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASEURL + endpoint
        
        var para = parameter
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_push_token] = push_token
        para[APIKey.key_lang] = lang
        
        Alamofire.request(url, method: .get, parameters: para, encoding: URLEncoding.default, headers: authorization).responseSwiftyJSON { (result) in
            print("url:=",url)
            print("parameter:=",para)
            print("response:=",result.value ?? "nil")
            result.result.value
            completion(result.result)
        }
    }

    func post(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASEURL + endpoint
        
        var para = parameter
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_push_token] = push_token
        para[APIKey.key_lang] = lang
        print(para)
        Alamofire.request(url, method: .post, parameters: para, encoding: URLEncoding.httpBody, headers: authorization).responseSwiftyJSON { (result) in
            print("statusCode:=",result.response?.statusCode ?? "nil")
            print("authorization:=",authorization)
            print("url:=",url)
            print("parameter:=",para)
            print("response:=",result.value ?? "nil")
            completion(result.result)
        }
    }
    
    func postUsingMultiPartData(endpoint:String ,parameter:[String:Any],files:[Data],withName:[String],withFileName:[String],mimeType:[String],completion:@escaping(JSON) -> Void) {
        let url = BASEURL + endpoint
        
        var para = parameter
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_push_token] = push_token
        para[APIKey.key_lang] = lang
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            var index = 0
            for data in files {
                multipartFormData.append(data, withName: withName[index], fileName: withFileName[index] , mimeType: mimeType[index])
                index += 1
            }
//            print("multipartFormData:=",multipartFormData.)
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: authorization) { (result) in
            print("authorization:=",authorization)
            print("url:=",url)
            print("parameter:=",para)

            switch result {
            case .success(let upload, _, _):
                print("statusCode:=",upload.response?.statusCode ?? "nil")
                upload.responseSwiftyJSON(completionHandler: { (result) in
                    if let json = result.result.value {
                        print("json:=",json)
                        completion(json)
                    } else {
                        completion(JSON(["flag":"0","msg":ValidationTexts.SOMETHING_WENT_WRONG]))
                    }
                })
                break
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                var msg = ValidationTexts.SOMETHING_WENT_WRONG
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    msg = ValidationTexts.NO_INTERNET_CONNECTION
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    msg = ValidationTexts.REQUEST_TIME_OUT
                }
                let result = JSON(["flag":"0","msg":msg])
                completion(result)
                break
            }
        }
    }
    
    func put(endpoint:String ,parameter:[String:Any],completion:@escaping(Result<JSON>) -> Void) {
        let url = BASEURL + endpoint
        
        var para = parameter
        para[APIKey.key_device_type] = deviceType
        para[APIKey.key_device_id] = deviceID
        para[APIKey.key_push_token] = push_token
        para[APIKey.key_lang] = lang
        
        Alamofire.request(url, method: .put, parameters: para, encoding: URLEncoding.default, headers: authorization).responseSwiftyJSON { (result) in
            print("url:=",url)
            print("parameter:=",para)
            print("response:=",result.value ?? "nil")
            completion(result.result)
        }
    }
    func downloadFileatDestination(audioUrl:String,completion:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        let documentsUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // your destination file url
        let endurl = URL.init(string: audioUrl)!
        let destination = documentsUrl.appendingPathComponent(endurl.lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3"))
        print(destination)
        // check if it exists before downloading it
        if FileManager().fileExists(atPath: destination.path) {
            print("The file already exists at path")
            completion(destination.path)
            return
        } else {
            //  if the file doesn't exist
            //  just download the data from your url
            URLSession.shared.downloadTask(with: endurl, completionHandler: { (location, response, error) in
                // after downloading your data you need to save it to your destination url
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let location = location, error == nil
                    else {
                        var msg = ValidationTexts.SOMETHING_WENT_WRONG
                        if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                            // no internet connection
                            msg = ValidationTexts.NO_INTERNET_CONNECTION
                            
                        } else if let err = error as? URLError, err.code == URLError.timedOut {
                            msg = ValidationTexts.REQUEST_TIME_OUT
                        }
                        //                let result = JSON(["flag":"0","msg":msg])
                        failed(msg)
                        return
                }
                
                do {
                    try FileManager.default.moveItem(at: location, to: destination)
                    print("file saved")
                    completion(destination.absoluteString)
                    return
                } catch {
                    print(error)
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
            }).resume()
        }
    }
    func downloadData(url:String,completion:@escaping(Data?) -> Void,failed:@escaping(String) -> Void)  {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: nil).responseData { (response) in
            switch response.result {
            case .success(let fileData):
                completion(fileData)
                break
            case .failure(let error):
                print("error:=",error)
                var msg = ValidationTexts.SOMETHING_WENT_WRONG
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    msg = ValidationTexts.NO_INTERNET_CONNECTION
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    msg = ValidationTexts.REQUEST_TIME_OUT
                }
                //                let result = JSON(["flag":"0","msg":msg])
                failed(msg)
                break
            }
        }
    }

}
