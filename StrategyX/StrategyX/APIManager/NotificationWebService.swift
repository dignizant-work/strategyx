//
//  NotificationWebService.swift
//  StrategyX
//
//  Created by Jaydeep on 06/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
class NotificationWebService: NSObject {
    
    static let shared = NotificationWebService()
    
    func getNotificationList(parameter:[String:Any],success:@escaping(_ list:[Notification], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_Notification, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        if let notificationList = NotificationList(JSON: data) {
                            success(notificationList.list,value[APIKey.key_next_offset].intValue)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
