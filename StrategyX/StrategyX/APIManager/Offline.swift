//
//  Offline.swift
//  StrategyX
//
//  Created by Haresh on 13/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

class OfflineManger: NSObject {
    
    static let shared = OfflineManger()
    
    func writeSingle<T:Object>(type:T) {
        do {
            try realm.write {
//                realm.add(type, update: true)
                realm.add(type, update: Realm.UpdatePolicy.modified)
            }
        } catch let error {
            print("error:=",error.localizedDescription)
        }
    }
    
    func writeMultiple<T:Object>(type:[T]) {
        for item in type {
            do {
                try realm.write {
                    realm.add(item, update: Realm.UpdatePolicy.modified)
                }
            } catch let error {
                print("error:=",error.localizedDescription)
            }
        }
        
       
    }
    
}
