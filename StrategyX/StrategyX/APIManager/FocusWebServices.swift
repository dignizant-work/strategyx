//
//  FocusWebServices.swift
//  StrategyX
//
//  Created by Haresh on 13/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class FocusWebServices: NSObject {
    static let shared = FocusWebServices()
    
    func addFocus(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddFocus, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }        
    }
    
    func updateFocus(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping(FocusDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_UpdateFocus, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = FocusDetail(JSON: data) {
                        success(detail)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getAllFocusList(parameter:[String:Any],success:@escaping(_ list:[Focus], _ offset:Int,_ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_FocusList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
//                        print("data:=",data)
                        if let focuslist = FocusList(JSON: data) {
                            success(focuslist.list,value[APIKey.key_next_offset].intValue,value[APIKey.key_assign_user_flag].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getFocusDetail(parameter:[String:Any],success:@escaping(_ detail:FocusDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_FocusDetail, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        
                        
                        if let detail = FocusDetail(JSON: data) {
                            success(detail)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
