//
//  StrategyWebService.swift
//  StrategyX
//
//  Created by Haresh on 20/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class StrategyWebService: NSObject {

    static let shared = StrategyWebService()
    
    func addPrimaryArea(parameter:[String:Any], files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddPrimaryArea, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getPrimaryAreaList(parameter:[String:Any],success:@escaping(_ list:[PrimaryArea], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_PrimaryAreaList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let primaryArealist = PrimaryAreaList(JSON: data) {
                            success(primaryArealist.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getPrimaryArea(parameter:[String:Any], success:@escaping(PrimaryArea?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewPrimaryArea, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let primaryArea = PrimaryArea(JSON: data) {
                            success(primaryArea)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    success(nil)
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func addSecondaryArea(parameter:[String:Any], files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddSecondaryArea, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getSecondaryAreaList(parameter:[String:Any],success:@escaping(_ list:[SecondaryArea], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_SecondaryAreaList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let secondaryArealist = SecondaryAreaList(JSON: data) {
                            success(secondaryArealist.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getSecondaryArea(parameter:[String:Any], success:@escaping(SecondaryArea?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewSecondaryArea, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let secondaryArea = SecondaryArea(JSON: data) {
                            success(secondaryArea)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    success(nil)
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func addStrategyGoal(parameter:[String:Any], files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddStrategyGoal, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getStrategyGoalList(parameter:[String:Any],success:@escaping(_ list:[StrategyGoalList], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_StrategyGoalList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let strategyGoalAllList = StrategyGoalAllList(JSON: data) {
                            success(strategyGoalAllList.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getStrategyActions(parameter:[String:Any],isEdit:Bool = false,success:@escaping(_ theModel:StrategyAction?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_StrategyAction, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if isEdit {
                        success(nil)
                        return
                    } else {
                        if let data = value[APIKey.key_data].dictionaryObject {
                            //                        print("data:=",data)
                            if let strategyAction = StrategyAction(JSON: data) {
                                success(strategyAction)
                                //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                                return
                            }
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    func getStrategyGoal(parameter:[String:Any], success:@escaping(StrategyGoalList?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewStrategyGoal, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let strategyGoal = StrategyGoalList(JSON: data) {
                            success(strategyGoal)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    success(nil)
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    func updateStrategy(isForStrategy:Bool = false,parameter:[String:Any], files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping(String,PrimaryArea?,SecondaryArea?,StrategyGoalList?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_EditStrategy, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if isForStrategy {
                        if let strategyGoal = StrategyGoalList(JSON: data) {
                            success(result[APIKey.key_msg].stringValue,nil,nil,strategyGoal)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    if let primaryArea = PrimaryArea(JSON: data) {
                        success(result[APIKey.key_msg].stringValue,primaryArea,nil,nil)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    } else if let secondaryArea = SecondaryArea(JSON: data) {
                        success(result[APIKey.key_msg].stringValue,nil,secondaryArea,nil)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    } else if let strategyGoal = StrategyGoalList(JSON: data) {
                        success(result[APIKey.key_msg].stringValue,nil,nil,strategyGoal)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                    success(result[APIKey.key_msg].stringValue, nil,nil,nil)
                    return
                }
                success(result[APIKey.key_msg].stringValue, nil,nil,nil)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    func updateStrategy(parameter:[String:Any],success:@escaping(String,StrategyGoalList?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_EditStrategy, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        //                        print("data:=",data)
                        if let strategyGoal = StrategyGoalList(JSON: data) {
                            success(value[APIKey.key_msg].stringValue, strategyGoal)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    success(value[APIKey.key_msg].stringValue, nil)
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                }
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func deleteStrategy(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_DeleteStrategy, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                }
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func approveStrategy(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ApproveStrategy, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                }
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
