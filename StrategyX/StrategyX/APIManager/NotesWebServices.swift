//
//  NotesWebServices.swift
//  StrategyX
//
//  Created by Haresh on 28/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotesWebServices: NSObject {

    static let shared = NotesWebServices()
    
    func addNote(parameter:[String:Any],success:@escaping(String, Notes) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_AddNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        
                        if let notesModel = Notes(JSON: data) {
                            success(value[APIKey.key_msg].stringValue,notesModel)
                            
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func updateNote(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_EditNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllNotesList(parameter:[String:Any],success:@escaping(_ list:[Notes], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_NotesList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let noteslist = NotesList(JSON: data) {
                            success(noteslist.list,value[APIKey.key_next_offset].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
