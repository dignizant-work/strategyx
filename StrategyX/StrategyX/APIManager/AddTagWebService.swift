//
//  NotificationWebService.swift
//  StrategyX
//
//  Created by Jaydeep on 06/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
class AddTagWebService: NSObject {
    
    static let shared = AddTagWebService()
    
    func postAddTag(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: POST_AddTag, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
