//
//  FocusWebServices.swift
//  StrategyX
//
//  Created by Haresh on 13/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class RiskWebservice: NSObject {
    static let shared = RiskWebservice()
    
    func getRiskList(parameter:[String:Any],success:@escaping([Risk], _ offset:Int,  _ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: kGetViewRisk, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let risklist = RiskList(JSON: data) {
                            success(risklist.risk,value[APIKey.key_next_offset].intValue, value[APIKey.key_assign_user_flag].intValue)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getRiskDetail(parameter:[String:Any],success:@escaping(_ detail:RiskDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: kViewRiskDetail, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        
                        if let detail = RiskDetail(JSON: data) {
                            success(detail)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func postAddRiskNotes(parameter:[String:Any], success:@escaping(_ notesData:NotesHistoryList) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostAddNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = NotesHistoryList(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func postDeleteNotesRisks(parameter:[String:Any], success:@escaping() -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostRemoveNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    success()
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func postEditRiskNotes(parameter:[String:Any], success:@escaping(_ notesData:NotesHistoryList) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostEditNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = NotesHistoryList(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func deleteRisk(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ChangeActionStatus, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)

                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
