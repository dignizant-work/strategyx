//
//  CommanListWebservice.swift
//  StrategyX
//
//  Created by Haresh on 12/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class CommanListWebservice: NSObject {

    static let shared = CommanListWebservice()

//    static func get<T:Object>(parameter:[String:Any], type:T.Type,success:()->Void,fail:(String)->Void) -> Void {
//        Alamofire.request(Login, method: .get, parameters:parameter , encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: <#T##(DataResponse<Any>) -> Void#>)
//    }
    
    func companyList(parameter:[String:Any],success:@escaping([Company]) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: GET_AllCompanyList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let companylist = CompanyList(JSON: data) {
                            success(companylist.company)
                            Utility.shared.getAllCompany = companylist.company
//                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getCategories(parameter:[String:Any], success:@escaping([Categories]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: kGetCategory, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let categoriesList = CategoriesList(JSON: data) {
                            success(categoriesList.categories)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getRiskAssign(parameter:[String:Any], success:@escaping([UserAssign]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: kGetAssignList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let assignList = userAssignList(JSON: data) {
                            success(assignList.userAssign)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getDepartment(parameter:[String:Any], success:@escaping([Department]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: kGetDepartment, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let departmentList = DepartmentList(JSON: data) {
                            success(departmentList.department)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllUserTypeList(parameter:[String:Any],success:@escaping(_ list:[UserType]) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_UserType, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        if let userTypeList = UserTypeList(JSON: data) {
                            success(userTypeList.list)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getAllUserList(parameter:[String:Any],success:@escaping(_ list:[UserList], _ offset:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_AllUserList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        if let userList = UserAllList(JSON: data) {
                            success(userList.list,value[APIKey.key_next_offset].intValue)
                            return
                        }
                    }
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func chartList(parameter:[String:Any], success:@escaping([ChartList]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_Chart, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ChartAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func chartActionList(parameter:[String:Any], success:@escaping([ChartActionList]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_Chart, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let chartList = ChartActionAllList(JSON: data) {
                            success(chartList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func phoneCodeList(parameter:[String:Any], success:@escaping([PhoneCode]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_CountryCode, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let phoneCodeList = PhoneCodeList(JSON: data) {
                            success(phoneCodeList.list)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    func getPushCount(parameter:[String:Any],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_HomeForPushData, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let pushData = PushCount(JSON: data) {
                            Utility.shared.pushData = pushData
                            success()
                            return
                        }
                    }
                    
                    failed(value[APIKey.key_msg].stringValue)
                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func downloadFile(strUrl:String,success:@escaping(Data?) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().downloadData(url: strUrl, completion: { (data) in
            success(data)
        }, failed: { (error) in
            print("File Error:=",error)
            failed(error)
        })
    }
    
    
    func downloadFileAtDestination(strUrl:String,success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().downloadFileatDestination(audioUrl: strUrl, completion: { (strDestinationURL) in
            success(strDestinationURL)
        }, failed: { (error) in
            print("File Error:=",error)
            failed(error)
        })
    }
    
    
    func checkVersion(parameter:[String:Any], success:@escaping(_ versionFlag:Int,_ latestVersion:String, _ lastUpdateDate:String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_CheckVersion, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        success(value[APIKey.key_data][APIKey.key_ios_version_flag].intValue,value[APIKey.key_data][APIKey.key_ios_current_version].stringValue,value[APIKey.key_data][APIKey.key_last_version_update_date].stringValue)
                        return
                        
//                        if let departmentList = DepartmentList(JSON: data) {
//                            success(departmentList.department)
//                            return
//                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getStrategyAssignUserList(parameter:[String:Any], success:@escaping([StrategyUserAssign]) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_StrategyAssignUserList, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        print("data:=",data)
                        if let assignList = StrategyUserAssignList(JSON: data) {
                            success(assignList.strategyUserAssign)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
}
