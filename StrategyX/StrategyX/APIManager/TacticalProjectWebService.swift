//
//  TacticalProjectWebService.swift
//  StrategyX
//
//  Created by Jaydeep on 21/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class TacticalProjectWebService: NSObject {
     static let shared = TacticalProjectWebService()
    
    func addTacticalProject(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping() -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_AddTacticalProject, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func getAllTacticalList(parameter:[String:Any],success:@escaping(_ list:[TacticalProjectList], _ offset:Int, _ assignUserFlag:Int) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_GetTacticalProject, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value.dictionaryObject {
                        //                        print("data:=",data)
                        if let tacticalProjectlist = TacticalProjecAllList(JSON: data) {
                            success(tacticalProjectlist.list,value[APIKey.key_next_offset].intValue,value[APIKey.key_assign_user_flag].intValue)
                            //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func getTacticalProjectDetail(parameter:[String:Any],success:@escaping(_ detail:TacticalProjectDetail) -> Void, failed:@escaping(String) -> Void) {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ViewTacticalProject, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    if let data = value[APIKey.key_data].dictionaryObject {
                        
                        if let detail = TacticalProjectDetail(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }
    
    func postAddTacticalProjectNotes(parameter:[String:Any], success:@escaping(_ notesData:NotesTacticalProjectList) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostAddNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = NotesTacticalProjectList(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure( _):
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            }
        }
    }
    
    func postEditTacticalProjectNotes(parameter:[String:Any], success:@escaping(_ notesData:NotesTacticalProjectList) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostEditNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    if let data = value[APIKey.key_data].dictionaryObject {
                        if let detail = NotesTacticalProjectList(JSON: data) {
                            success(detail)
                            return
                        }
                    }
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure( _):
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            }
        }
    }
    
    func postEditTacticalProject(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String], success:@escaping(TacticalProjectDetail) -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().postUsingMultiPartData(endpoint: POST_UpdateTacticalProject, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                if let data = result[APIKey.key_data].dictionaryObject {
                    if let detail = TacticalProjectDetail(JSON: data) {
                        success(detail)
                        //                            OfflineManger.shared.writeMultiple(type: companylist.company)
                        return
                    }
                }
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
        }
    }
    
    func postDeleteNotesTacticalProjectNotes(parameter:[String:Any], success:@escaping() -> Void, failed:@escaping(String) -> Void)  {
        
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().post(endpoint: kPostRemoveNotes, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    print("value \(value)")
                    success()
                    return
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    return
                }
            case .failure( _):
                failed(ValidationTexts.SOMETHING_WENT_WRONG)
                break
            }
        }
    }
    
    func uploadTacticalProjectAttachment(parameter:[String:Any],files: [Data], withName: [String], withFileName: [String], mimeType: [String],success:@escaping() -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        
        WebService().postUsingMultiPartData(endpoint: POST_AddTacticalProject, parameter: parameter, files: files, withName: withName, withFileName: withFileName, mimeType: mimeType) { (result) in
            print("result\(result)")
            if result[APIKey.key_flag].intValue == 1 {
                success()
                return
            } else {
                failed(result[APIKey.key_msg].stringValue)
                return
            }
//            failed(ValidationTexts.SOMETHING_WENT_WRONG)
        }
    }
    
    func deleteRisk(parameter:[String:Any],success:@escaping(String) -> Void, failed:@escaping(String) -> Void)  {
        if !NetworkConnectivity().isInternetConnection() {
            failed(ValidationTexts.NO_INTERNET_CONNECTION)
            return
        }
        WebService().post(endpoint: POST_ChangeActionStatus, parameter: parameter) { (result) in
            switch result {
            case .success(let value):
                if value[APIKey.key_flag].intValue == 1 {
                    success(value[APIKey.key_msg].stringValue)
                    //                    failed(value[APIKey.key_msg].stringValue)
                    //                    break
                } else {
                    failed(value[APIKey.key_msg].stringValue)
                    break
                }
                
             case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet  {
                    // no internet connection
                    failed(ValidationTexts.NO_INTERNET_CONNECTION)
                    
                } else if let err = error as? URLError, err.code == URLError.timedOut {
                    failed(ValidationTexts.REQUEST_TIME_OUT)
                } else {
                    // other failures
                    failed(ValidationTexts.SOMETHING_WENT_WRONG)
                }
                break
            }
        }
    }

}
