//
//  VimeoServiceManger.swift
//  StrategyX
//
//  Created by iMac on 29/05/20.
//  Copyright © 2020 Outsourced Global Limited. All rights reserved.
//

import Foundation

class VimeoServiceManger {
    private let queueManager: QueueManager
    
    // MARK: - Init
    
    init(withQueueManager queueManager: QueueManager = QueueManager.shared) {
        self.queueManager = queueManager
    }
    
    // MARK: - Avatar
    
    func retrieveVimeoImage(from vimeoUrl: String, completionHandler: @escaping (_ result: OperationResult<(String, String)>) -> Void) {
        let operation = VimeoConfigureService(vimeoUrl: vimeoUrl)
        operation.completionHandler = completionHandler
        queueManager.enqueue(operation)
    }

}
