//
//  APIKey.swift
//  StrategyX
//
//  Created by Haresh on 11/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation

struct APIKey {
    
    static let key_status = "status"
    static let key_error = "error"
    static let key_data = "data"
    static let key_msg = "msg"
    static let key_flag = "flag"
    
    static let key_device_id = "device_id"
    static let key_device_type = "device_type"
    static let key_push_token = "push_token"
    static let key_lang = "lang"
    
    static let key_email = "email"
    static let key_password = "password"
    
    static let key_access_token = "access_token"
    static let key_user_id = "user_id"
    
    static let key_category_for = "category_for"
   
    static let key_company_id = "company_id"
    static let key_company_name = "company_name"
    
    static let key_assign_to = "assign_to"
    
    static let key_category = "category"
    static let key_unwanted_event = "unwanted_event"
    static let key_description = "description"
    static let key_assigned = "assigned"
    static let key_department_id = "department_id"
    static let key_department_name = "department_name"
    static let key_level = "level"
    static let key_title = "title"
    static let key_is_specific = "is_specific"
    static let key_is_measure = "is_measure"
    static let key_is_attainable = "is_attainable"
    static let key_is_realistic = "is_realistic"
    static let key_is_timely = "is_timely"
    static let key_duedate = "duedate"
    static let key_revised_date = "revised_date"
    static let key_assigned_to = "assigned_to"
    static let key_assignto_id = "assignto_id"
    static let key_assignto_name = "assignto_name"
    static let key_category_id = "category_id"
    static let key_category_name = "category_name"
    // get risk data
    
    static let key_search = "search"
    static let key_grid_type = "grid_type"
    static let key_page_offset = "page_offset"
    
    static let key_note_list = "note_list"
    
    // add note List
    
    static let key_note_for_id = "note_for_id"
    static let key_notes = "notes"
    static let key_note_for = "note_for"
    static let key_risks = "risks"
    static let key_tactical_projects = "tactical_projects"
    
    static let key_notes_id = "notes_id"
    
    // tactical project
    
    static let key_project_name = "project_name"
    static let key_cost = "cost"
    static let key_startdate = "startdate"    
    static let key_tactical_project_id = "tactical_project_id"
    static let key_tactical_projects_id = "tactical_projects_id"
    
    // Action
    static let key_work_date = "work_date"
    static let key_approve_by = "approve_by"
    static let key_approved_by = "approved_by"
    static let key_related_to = "related_to"
    static let key_goal = "goal"
    static let key_department = "department"
    static let key_sub_task = "sub_task"
    static let key_tags = "tags"
    static let key_risk = "risk"
    static let key_action_logs = "action_logs"
    static let key_completed_percentage = "completed_percentage"
    static let key_remove_voice_notes = "remove_voice_notes"
    static let key_remove_attechment = "remove_attechment"
    static let key_duedatefilter = "duedatefilter"
    static let key_workdatefilter = "workdatefilter"
    
    // add user
    static let key_timezone_id = "timezone_id"
    static let key_firstname = "firstname"
    static let key_lastname = "lastname"
    static let key_mobile = "mobile"
    static let key_user_type = "user_type"
    static let key_user_image = "user_image"
    static let key_verification_code = "verification_code"
    static let key_manager_id = "manager_id"
    
    // add Comapny
    static let key_state_id = "state_id"
    static let key_company_abbr = "company_abbr"
    static let key_user_firstname = "user_firstname"
    static let key_user_email = "user_email"
    static let key_user_lastname = "user_lastname"
    static let key_website = "website"
    static let key_unit_number = "unit_number"
    static let key_street_name = "street_name"
    static let key_street_type = "street_type"
    static let key_state = "state"
    static let key_suburb = "suburb"
    static let key_post_code = "post_code"
    static let key_company_image = "company_image"
    
    // add Tag
    static let key_name = "name"
    static let key_Audio_name = "Audio"
    static let key_Extension = "mp4"
    
    // edit user
    static let key_edit_user_id = "edit_user_id"
    static let key_view_user_id = "view_user_id"
    static let key_exclude_user_id = "exclude_user_id"
    // notes history
    
    static let key_All = "All"
    static let key_Assigned_To = "Assigned"
    static let key_completed = "% Completed"
    static let key_posted_notes = "Posted Notes"
    static let key_revised_due_date = "Revised Due date"
    static let key_Work_Date = "Work date"
    static let key_Opened_an_action = "Opened"
    static let key_Closed_an_action = "Closed"
    
    // notes status
    
    static let key_Assigned = "assigned"
    static let key_Percentage = "percentage"
    static let key_Commented = "commented"
    static let key_Revised = "revised"
    static let key_Worked = "worked"
    static let key_Opened = "opened"
    static let key_Closed = "closed"
    
    // delete
    
    static let key_deleted = "deleted"
    
    // Chart
    
    static let key_chart_id = "chart_id"
    static let key_chart_for = "chart_for"
    static let key_primary = "primary"
    static let key_secondary = "secondary"
    
    // Authentication
    
    static let key_content_type = "content_type"
    static let key_license_agreement = "License_agreement"
    
    // Google PlacePicker key
    
    static let key_google_place_picker = "AIzaSyCaY3jO9atkfrlrk_4Skyw2P_MIemkXGko"
    
    
    // Tag
    
    static let key_is_add_form_tag = "is_add_form_tag"
    static let key_tag_active_status = "active"


    //History
    static let key_history_for = "history_for"
    static let key_history_id = "history_id"
    
}



