//
//  CardDetailsViewModel.swift
//  StrategyX
//
//  Created by Haresh on 20/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CardDetailsViewModel {

    //MARK:- Variable
    fileprivate let theController:CardDetailsVC!
    
    
    
    //MARK:- LifeCycle
    init(theController:CardDetailsVC) {
        self.theController = theController
    }
    
    func validateForm() -> Bool {
        guard let view = theController.view as? CardDetailsView else {return false}
        if view.txtCardHolderName.text!.trimmed().isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter cardholder name")
            return false
        } else if view.txtCardHolderName.text!.trimmed().isEmpty || view.txtCardnumber.cardNumber.trimmed().isEmpty || view.txtCardExpireMonth.text!.trimmed().isEmpty || view.txtCVC.text!.trimmed().isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter card details")
            return false
        } else if !view.btnTNC.isSelected {
            self.theController.showAlertAtBottom(message: "Please accept terms of use and privacy policy")
            return true
        }
        return true
    }
    
}
