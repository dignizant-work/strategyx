//
//  CardDetailsVC.swift
//  StrategyX
//
//  Created by Haresh on 20/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import Stripe

class CardDetailsVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CardDetailsView = { [unowned self] in
       return self.view as! CardDetailsView
    }()
    
    internal lazy var theCurrentViewModel: CardDetailsViewModel = {
       return CardDetailsViewModel(theController: self)
    }()
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func setupUI()  {
        theCurrentView.setupLayout()
        
    }
    
    //MARK:- Redirection
    func redirectToBilingInformationVC() {
        let vc = BillingInformationVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        backAction()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if theCurrentViewModel.validateForm() {
            self.view.allButtons(isEnable: false)
            sender.startLoadyIndicator()
           /* let cardParams = STPCardParams()
            cardParams.number = theCurrentView.txtCardnumber.cardNumber
            cardParams.expMonth = UInt(theCurrentView.txtCardExpireMonth.dateComponents.month ?? 0)
            cardParams.expYear = UInt(theCurrentView.txtCardExpireMonth.dateComponents.year ?? 0)
            cardParams.cvc = theCurrentView.txtCVC.text!
            
            STPAPIClient.shared().createToken(withCard: cardParams) { [weak self] (token, error) in
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
                guard let tokenClient = token, error == nil else {
                    // Present error to user...
                    

                    if let errorget = error?.localizedDescription {
                        self?.showAlertAtBottom(message: errorget)
                    }
                    return
                }
                Utility.shared.loginData?.stripeToken = tokenClient.tokenId
                self?.redirectToBilingInformationVC()
            }*/
        }
//        redirectToBilingInformationVC()
    }
    @IBAction func onBtnTermAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentView.btnTNC.isSelected = !theCurrentView.btnTNC.isSelected
    }
}
