//
//  CardDetailsView.swift
//  StrategyX
//
//  Created by Haresh on 20/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CardDetailsView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var txtCardnumber: BKCardNumberField!
    @IBOutlet weak var txtCardExpireMonth: BKCardExpiryField!
    @IBOutlet weak var txtCVC: BKCardNumberField!
    
    @IBOutlet weak var btnTNC: UIButton!
    
    //MARK:- Life Cycle
    func setupLayout() {
        txtCardnumber.showsCardLogo = true
        
    }
    
}
