//
//  VerifyEmailAddressView.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class VerifyEmailAddressView: ViewParentWithoutXIB {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK:- Variabel
    
    @IBOutlet weak var lblEmail: UILabel!
    
    
    //setup
    func setupLayout() {
        updateEmail(strEmail: "example@gmail.com")
    }
    
    func updateEmail(strEmail:String) {
        let textContent = "Check your email \(strEmail) and click the link to activate your account."
        let attriString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedString.Key.foregroundColor: appBgLightGrayColor,
            NSAttributedString.Key.font: FontStyle().setFontType(strType: "R15")!])
        let range = (textContent as NSString).range(of: strEmail)
        attriString.addAttributes([NSAttributedString.Key.foregroundColor: appGreen], range: range)
        lblEmail.attributedText = attriString
    }
    
}
