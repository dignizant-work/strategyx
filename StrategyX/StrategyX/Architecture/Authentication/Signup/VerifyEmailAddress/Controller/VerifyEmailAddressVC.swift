//
//  VerifyEmailAddressVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class VerifyEmailAddressVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:VerifyEmailAddressView = { [unowned self] in
        return self.view as! VerifyEmailAddressView
    }()
    
    var strEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        strEmail = (Utility.shared.loginData?.email ?? "").trimmed()
        theCurrentView.updateEmail(strEmail: strEmail)
        Utility.shared.loginData = nil
    }
    
    //MARK:- IBAction
    @IBAction func onBtnResendEmailAction(_ sender: UIButton) {
        self.view.allButtons(isEnable: false)
        sender.startLoadyIndicator()
        resendEmailWebservice(success: { [weak self] (strMsg) in
            sender.stopLoadyIndicator()
            self?.showAlertAtBottom(message: strMsg)
            self?.view.allButtons(isEnable: true)
            }, failed: { [weak self] (error) in
                sender.stopLoadyIndicator()
                self?.showAlertAtBottom(message: error)
                self?.view.allButtons(isEnable: true)
        })
    }
    
    @IBAction func onBtnGotoLoginAction(_ sender: Any) {
        AppDelegate.shared.redirectToRootLoginViewController()
    }
}
//MARK:- Api Call
extension VerifyEmailAddressVC {
    
    
    func resendEmailWebservice(success:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        
        let parameter:[String:Any] = [APIKey.key_user_email:strEmail]
        AuthenicationService.shared.resendEmail(parameter: parameter, success: { (strMsg) in
            success(strMsg)
        }, failed: { (error) in
            failed(error)
        })
    
    }
    
}
