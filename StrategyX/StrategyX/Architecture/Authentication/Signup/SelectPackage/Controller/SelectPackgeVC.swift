//
//  SelectPackgeVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SelectPackgeVC: ParentViewController {

    fileprivate lazy var theCurrentView:SelectPackageView = { [unowned self] in
       return self.view as! SelectPackageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
    }
    
    //MARK:- Member Function
    func redirectToBilingInformationVC() {
        let vc = BillingInformationVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToCardDetailVC() {
        let vc = CardDetailsVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        showAlertAction(msg: " Are you sure you want skip this registration process?") { [weak self] in
            self!.backToRootAction()
        }
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCardDetailVC()
    }
    
}
/*
//MARK:- UITableViewDataSource
extension SelectPackgeVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectPackageTVCell") as! SelectPackageTVCell
        cell.setTheData(isSelected: indexPath.row == 1)
        cell.selectionStyle = .none
        return cell
    }
    
}
//MARK:- UITableViewDelegate
extension SelectPackgeVC:UITableViewDelegate {
    
}
*/
