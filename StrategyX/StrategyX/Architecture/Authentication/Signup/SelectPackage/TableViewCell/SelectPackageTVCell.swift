//
//  SelectPackageTVCell.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SelectPackageTVCell: UITableViewCell {

    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var img_BGView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTheData(isSelected:Bool) {
        btnCheck.isSelected = isSelected
        img_BGView.image = btnCheck.isSelected ? UIImage(named: "ic_text_box_green_select_package_screen.png") : UIImage(named: "ic_text_box_grey_select_package_screen.png")
//        lblTitle.text = ""
//        lblDescription.text = ""
    }
    
}
