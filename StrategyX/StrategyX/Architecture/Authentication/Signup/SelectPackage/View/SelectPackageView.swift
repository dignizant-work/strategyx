//
//  SelectPackageView.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SelectPackageView: ViewParentWithoutXIB {

    //MARK:-  Variable
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblAdditionalUser: UILabel!
    @IBOutlet weak var lblPerMonth: UILabel!
    
    //
    func setupLayout() {
   
        let textContent = "You'll recieve a 30 Day Free Trial and you can cancel at any time."
        
        let attriString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedString.Key.foregroundColor: appTextLightGrayColor,
            NSAttributedString.Key.font: FontStyle().setFontType(strType: "R15")!])
        let range = (textContent as NSString).range(of: "30 Day Free Trial")
        attriString.addAttributes([NSAttributedString.Key.font: FontStyle().setFontType(strType: "B15")!,NSAttributedString.Key.foregroundColor:appBlack], range: range)
        lblDescription.attributedText = attriString
        lblPerMonth.text = "per month\n(20 Users Included)"
        lblAdditionalUser.text = "Each Additional User\n$7 Per Month"
    }
    /*
    func setTableView(theDelegate:SelectPackgeVC) {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150.0
        tableView.separatorStyle = .none
        
        tableView.register(UINib.init(nibName: "SelectPackageTVCell", bundle: nil), forCellReuseIdentifier: "SelectPackageTVCell")
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
    }
    
    
    func setTheDelegatesForTableView(theObejct: UITableView? = nil, theDelegate: SelectPackgeVC) {
        if theObejct != nil {
            theObejct?.register(UINib.init(nibName: "SelectPackageTVCell", bundle: nil), forCellReuseIdentifier: "SelectPackageTVCell")
            
            theObejct?.tableFooterView = UIView()
            theObejct?.separatorStyle = .none
            theObejct?.estimatedRowHeight = 400
            theObejct?.rowHeight = UITableView.automaticDimension
            
            theObejct?.delegate = theDelegate
            theObejct?.dataSource = theDelegate            
        }
    }*/
}
