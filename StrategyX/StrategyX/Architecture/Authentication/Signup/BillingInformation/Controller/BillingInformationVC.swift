//
//  BillingInformationVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import GooglePlaces

class BillingInformationVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:BillingInformationView = { [unowned self] in
        return self.view as! BillingInformationView
    }()
    
    var selectedData = ["country":"","state":"","city":"","timezoneId":"","phoneCodeId":""]

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
    }
    
    //MARK:- Member function
    func redirectToVerifyEmailVC() {
        let vc = VerifyEmailAddressVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        backAction()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateAddress() {
            return
        }
        
        let billingAddress = Utility.shared.loginData?.companyAddress ?? ""
        let billingcity = Utility.shared.loginData?.city ?? ""
        let billingstate = Utility.shared.loginData?.state ?? ""
        let billingcountry = Utility.shared.loginData?.country ?? ""

        if theCurrentView.btnCompanyAddress.isSelected {
            Utility.shared.loginData?.billingAddress = billingAddress
            Utility.shared.loginData?.billingCity = billingcity
            Utility.shared.loginData?.billingState = billingstate
            Utility.shared.loginData?.billingCountry = billingcountry
        } else {
            Utility.shared.loginData?.billingAddress = theCurrentView.txtAddress.text!.trimmed()
            Utility.shared.loginData?.billingCity = selectedData["city"] ?? ""
            Utility.shared.loginData?.billingState = selectedData["state"] ?? ""
            Utility.shared.loginData?.billingCountry = selectedData["country"] ?? ""
        }
        print("LoginData:=",Utility.shared.loginData!)
        self.view.allButtons(isEnable: false)
        sender.startLoadyIndicator()
        signupWebservice(success: { [weak self] (strMsg) in
            sender.stopLoadyIndicator()
            self?.showAlertAtBottom(message: strMsg)
            self?.view.allButtons(isEnable: true)
            self?.redirectToVerifyEmailVC()
            }, failed: { [weak self] (error) in
                sender.stopLoadyIndicator()
                self?.showAlertAtBottom(message: error)
                self?.view.allButtons(isEnable: true)
        })
    }
    
}
//MARK:- Api Calling
extension BillingInformationVC {
    func signupWebservice(success:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        guard let theModel = Utility.shared.loginData else { return failed(ValidationTexts.SOMETHING_WENT_WRONG) }

        let parameter:[String:Any] = [APIKey.key_user_email:theModel.email.trimmed(),
                                      APIKey.key_user_firstname:theModel.firstname.trimmed(),
                                      APIKey.key_user_lastname:theModel.lastname.trimmed(),
                                      APIKey.key_mobile:theModel.mobile.trimmed(),
                                      APIKey.key_country_code:theModel.phoneCodeID,
                                      APIKey.key_company_name:theModel.companyName.trimmed(),
                                      APIKey.key_map_address:theModel.companyAddress,
                                      APIKey.key_map_state:theModel.state,
                                      APIKey.key_map_city:theModel.city,
                                      APIKey.key_map_country:theModel.country,
                                      APIKey.key_timezone_id:theModel.timeZoneID,
                                      APIKey.key_website:theModel.website.trimmed(),
                                      APIKey.key_user_type:theModel.userType,
                                      APIKey.key_stripe_token:theModel.stripeToken,
                                      APIKey.key_billing_address:theModel.billingAddress,
                                      APIKey.key_billing_state:theModel.billingState,
                                      APIKey.key_billing_city:theModel.billingCity,
                                      APIKey.key_billing_country:theModel.billingCountry]
        
        AuthenicationService.shared.signup(parameter: parameter, success: { (strMsg) in
            success(strMsg)
        }, failed: { (error) in
            failed(error)
        })
    }
}
//MARK:- UITextfield Delegate
extension BillingInformationVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == theCurrentView.txtAddress{
            openGooglePlacePicker()
            return false
        }
        return true
    }
    
}

//MARK:- Google Place Picker

extension BillingInformationVC {
    func openGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
         filter.type = .address
         autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
}

//MARK:- Google Placepicker Delegate
extension BillingInformationVC:GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place ID: \(place.placeID ?? "")")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress ?? "")")
        theCurrentView.txtAddress.text = place.formattedAddress ??  ""
//        theCurrentView.lblCompanyAddress.text = theCurrentView.txtAddress.text
        getAddressFromLatLon(place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

//MARK:- get address
extension BillingInformationVC {
    func getAddressFromLatLon(_ coordinate:CLLocationCoordinate2D) {
        
        let ceo: CLGeocoder = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            { [weak self] (placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    self?.selectedData["country"] = pm.country ?? ""
                    self?.selectedData["state"] = pm.administrativeArea ?? ""
                    self?.selectedData["city"] = pm.locality ?? ""
                }
        })
        
    }
}
