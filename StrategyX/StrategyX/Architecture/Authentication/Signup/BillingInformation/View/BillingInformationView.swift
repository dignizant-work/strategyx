//
//  BillingInformationView.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class BillingInformationView: ViewParentWithoutXIB {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    
    //MARK:- Variable
    @IBOutlet weak var btnCompanyAddress: UIButton!
    @IBOutlet weak var lblErrorAddress: UILabel!
    @IBOutlet weak var txtAddress: kTextFiledPlaceHolder!
    
    let disposeBag = DisposeBag()
    
    
    //setup
    func setupLayout() {
        lblErrorAddress.text = ""
        
        //Rx Action
        setBtnSameAddressAction()
    }
    
    func validateAddress() -> Bool {
        lblErrorAddress.text = ""
        
        if txtAddress.text!.count == 0 {
            lblErrorAddress.text = "Please enter address"
            return false
        } else {
            return true
        }
    }
    
    //Rx Action
    func setBtnSameAddressAction() {
        btnCompanyAddress.rx.tap
            .subscribe(onNext:{ [weak self] in
                if let obj = self {
                    obj.txtAddress.isUserInteractionEnabled = true
                    obj.btnCompanyAddress.isSelected = !obj.btnCompanyAddress.isSelected
                    if obj.btnCompanyAddress.isSelected {
                        obj.txtAddress.text = Utility.shared.loginData?.companyAddress
                        obj.txtAddress.isUserInteractionEnabled = false
                    } else {
                        obj.txtAddress.text = ""
                    }
                }
            }).disposed(by: disposeBag)
    }
    
}
