//
//  CompanyDetailView.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyDetailView: ViewParentWithoutXIB {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var txtCompanyName: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorCompanyName: UILabel!
    @IBOutlet weak var txtAddress: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorAddress: UILabel!
    @IBOutlet weak var txtSelectTimeZone: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorSelectTimeZone: UILabel!
    @IBOutlet weak var txtWebsite: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorWebsite: UILabel!
    @IBOutlet weak var btnSelectTimeZone: UIButton!
    
    //Layout
    func setupLayout() {
        lblErrorCompanyName.text = ""
        lblErrorAddress.text = ""
        lblErrorSelectTimeZone.text = ""
        lblErrorWebsite.text = ""
        
//        _ = txtCompanyName.becomeFirstResponder()
    }
    
    func validateRegister() -> Bool {
        lblErrorCompanyName.text = ""
        lblErrorAddress.text = ""
        lblErrorSelectTimeZone.text = ""
        lblErrorWebsite.text = ""
        
        if txtCompanyName.text!.count == 0 {
            lblErrorCompanyName.text = "Please enter company name"
            return false
        } else if txtAddress.text!.count == 0 {
            lblErrorAddress.text = "Please enter address"
            return false
        }
        else if txtSelectTimeZone.text!.count == 0 {
            lblErrorSelectTimeZone.text = "Please select time zone"
            return false
        }
        else if txtWebsite.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            lblErrorWebsite.text = "Please enter website"
            return false
        } else if !txtWebsite.text!.trimmingCharacters(in: .whitespacesAndNewlines).validateURL() {
            lblErrorWebsite.text = "Please enter valid website"
            return false
        } else {
            return true
        }
    }

}
