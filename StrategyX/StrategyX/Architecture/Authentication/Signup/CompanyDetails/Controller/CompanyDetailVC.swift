//
//  CompanyDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CompanyDetailView = { [unowned self] in
       return self.view as! CompanyDetailView
    }()
    
    fileprivate lazy var theCurrentModel:CompanyDetailsModel = {
        return CompanyDetailsModel.init(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        Utility.shared.loginData?.companyName = theCurrentView.txtCompanyName.text ?? ""
        Utility.shared.loginData?.companyAddress = theCurrentView.txtAddress.text ?? ""
        Utility.shared.loginData?.website = theCurrentView.txtWebsite.text ?? ""
        super.viewWillDisappear(animated)
    }
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.txtCompanyName.text = Utility.shared.loginData?.companyName
        theCurrentView.txtAddress.text = Utility.shared.loginData?.companyAddress
        theCurrentView.txtSelectTimeZone.text = Utility.shared.loginData?.timeZone
        theCurrentView.txtWebsite.text = Utility.shared.loginData?.website

        timeZoneListService()
    }
    
    //MARK:- Member function
    func updateDropDownData(str:String, index:Int)  {
        theCurrentView.txtSelectTimeZone.text = str
        Utility.shared.loginData?.timeZone = str
        Utility.shared.loginData?.timeZoneID = theCurrentModel.arr_TimeZoneList[index].timezoneId
    }
    
    //MARK:- Redirection
    func presentDropDownListVC() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        let arr_TimeZone:[String] = theCurrentModel.arr_TimeZoneList.map({ $0.timezoneName })
        vc.setTheData(strTitle: "Time Zone",arr:arr_TimeZone)
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToSelectPackageVC() {
        let vc = SelectPackgeVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToVerifyEmailVC() {
        let vc = VerifyEmailAddressVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func btnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        backAction()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateRegister() {
            return
        }
        Utility.shared.loginData?.companyName = theCurrentView.txtCompanyName.text ?? ""
        Utility.shared.loginData?.companyAddress = theCurrentView.txtAddress.text ?? ""
        Utility.shared.loginData?.website = theCurrentView.txtWebsite.text ?? ""
        
        self.view.allButtons(isEnable: false)
        sender.startLoadyIndicator()
        signupWebservice(success: { [weak self] (strMsg) in
            sender.stopLoadyIndicator()
            self?.showAlertAtBottom(message: strMsg)
            self?.view.allButtons(isEnable: true)
            self?.redirectToVerifyEmailVC()
        }, failed: { [weak self] (error) in
            sender.stopLoadyIndicator()
            self?.showAlertAtBottom(message: error)
            self?.view.allButtons(isEnable: true)
        })
    }

    @IBAction func onBtnSelectTimeZoneAction(_ sender: Any) {
        presentDropDownListVC()
    }
}
//MARK:- Api Call
extension CompanyDetailVC {
    func timeZoneListService() {
        let parameter:[String:Any] = [:]
        theCurrentView.btnSelectTimeZone.loadingIndicator(true, allignment: .right)
        AddUserWebService.shared.getAllTimezoneList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_TimeZoneList = list
            self?.theCurrentView.btnSelectTimeZone.loadingIndicator(false)
            }, failed: { [weak self] (error) in
                self?.theCurrentView.btnSelectTimeZone.loadingIndicator(false)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func signupWebservice(success:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        guard let theModel = Utility.shared.loginData else { return failed(ValidationTexts.SOMETHING_WENT_WRONG) }
        
        let parameter:[String:Any] = [APIKey.key_user_email:theModel.email,APIKey.key_user_firstname:theModel.firstname,APIKey.key_user_lastname:theModel.lastname,APIKey.key_mobile:theModel.mobile,APIKey.key_country_code:theModel.phoneCodeID,APIKey.key_company_name:theModel.companyName,APIKey.key_map_address:theModel.companyAddress,APIKey.key_map_state:theModel.state,APIKey.key_map_city:theModel.city,APIKey.key_map_country:theModel.country,APIKey.key_timezone_id:theModel.timeZoneID,APIKey.key_website:theModel.website,APIKey.key_user_type:theModel.userType]
        
        AuthenicationService.shared.signup(parameter: parameter, success: { (strMsg) in
            success(strMsg)
        }, failed: { (error) in
            failed(error)
        })
    }
    
}
