//
//  PersonalDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class PersonalDetailModel {
    
    // MARK:- Variables | Properties
    private unowned var theController: PersonalDetailVC
    
    var arr_PhoneCodeList:[PhoneCode] = []
    var arr_TimeZoneList:[Timezone] = []
    var selectedData = ["country":"","state":"","city":"","timezoneId":"","phoneCodeId":""]
    
    // MARK:- View Lifecycle
    init(theController: PersonalDetailVC) {
        self.theController = theController
    }
    
}
