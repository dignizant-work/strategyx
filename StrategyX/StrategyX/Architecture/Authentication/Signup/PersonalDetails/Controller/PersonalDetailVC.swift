//
//  PersonalDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 11/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import GooglePlaces

class PersonalDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:PersonalDetailView = { [unowned self] in
        return self.view as! PersonalDetailView
    }()
    
    fileprivate lazy var theCurrentModel:PersonalDetailModel = {
       return PersonalDetailModel(theController: self)
    }()
    

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        Utility.shared.loginData?.companyName = theCurrentView.txtCompanyName.text ?? ""
        Utility.shared.loginData?.companyAddress = theCurrentView.txtAddress.text ?? ""
        Utility.shared.loginData?.website = theCurrentView.txtWebsite.text ?? ""
        super.viewWillDisappear(animated)
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        Utility.shared.loginData = LoginData()
        countryCodeListService()
        theCurrentView.txtCompanyName.text = Utility.shared.loginData?.companyName
        theCurrentView.txtAddress.text = Utility.shared.loginData?.companyAddress
        theCurrentView.txtSelectTimeZone.text = Utility.shared.loginData?.timeZone
        theCurrentView.txtWebsite.text = Utility.shared.loginData?.website
        theCurrentView.lblCountryCode.textColor = appPlaceHolder
        timeZoneListService()
    }
    
    func presentDropDownListVC(selectType:phoneCodeTimezone) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch selectType {
        case .phoneCode:
            let arr_country:[String] = theCurrentModel.arr_PhoneCodeList.map({ "+" + ($0.phonecode) + " (\($0.countryname))" })
            vc.setTheData(strTitle: "Phone Code",arr:arr_country,isSearchHidden: false)
            break
        case .timeZone:
             let arr_list:[String] = theCurrentModel.arr_TimeZoneList.map({"\($0.timezoneName) (\($0.timezoneCode))"})
            vc.setTheData(strTitle: "Time Zone",arr:arr_list)
            break
        }
      
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(str: strData, index: index, selectType: selectType)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }

    func updateDropDownData(str:String, index:Int,selectType:phoneCodeTimezone)  {
        switch selectType {
        case .phoneCode:
            theCurrentView.lblCountryCode.textColor = black
            theCurrentView.lblCountryCode.text = str
            Utility.shared.loginData?.phoneCode = str
            if let selectedIndex = theCurrentModel.arr_PhoneCodeList.firstIndex(where: {"+" + ($0.phonecode) + " (\($0.countryname))" == str}){
               theCurrentModel.selectedData["phoneCodeId"] = theCurrentModel.arr_PhoneCodeList[selectedIndex].id
                Utility.shared.loginData?.phoneCodeID = theCurrentModel.arr_PhoneCodeList[selectedIndex].id
                Utility.shared.loginData?.phoneCode = theCurrentModel.arr_PhoneCodeList[selectedIndex].phonecode
            }
           
            break
        case .timeZone:
            theCurrentView.txtSelectTimeZone.text = str
            Utility.shared.loginData?.timeZone = str
            theCurrentModel.selectedData["timezoneId"] = theCurrentModel.arr_TimeZoneList[index].timezoneId
            Utility.shared.loginData?.timeZoneID = theCurrentModel.arr_TimeZoneList[index].timezoneId
            break
        }
       
    }
    
    //MARK:- redirection
    func redirectToCompanyDetailVC() {
        let vc = CompanyDetailVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToVerifyEmailVC() {
        let vc = VerifyEmailAddressVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        vc.strEmail = self.theCurrentView.txtEmailAddress.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToSelectPackage() {
        let vc = SelectPackgeVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
//        vc.strEmail = self.theCurrentView.txtEmailAddress.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func redirectToVerificationCode(strToken:String) {
        let vc = VerificationCodeVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        vc.setToken(strToken:strToken)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnNumberAction(_ sender: Any) {
        if theCurrentModel.arr_PhoneCodeList.count == 0 {
            self.showAlertAtBottom(message: "Please wait, Until phone code is not downloaded")
            return
        }
        presentDropDownListVC(selectType: .phoneCode)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        Utility.shared.loginData = nil
        backAction()
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateRegister() {
            return
        }
//        Utility.shared.loginData = LoginData()
        Utility.shared.loginData?.firstname = theCurrentView.txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        Utility.shared.loginData?.lastname = theCurrentView.txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        Utility.shared.loginData?.email = theCurrentView.txtEmailAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        Utility.shared.loginData?.mobile = theCurrentView.txtPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
//        Utility.shared.
        Utility.shared.loginData?.companyName = theCurrentView.txtCompanyName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        Utility.shared.loginData?.companyAddress = theCurrentView.txtAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        Utility.shared.loginData?.city = (theCurrentModel.selectedData["city"] ?? "").trimmed()
        Utility.shared.loginData?.state = (theCurrentModel.selectedData["state"] ?? "").trimmed()
        Utility.shared.loginData?.country = (theCurrentModel.selectedData["country"] ?? "").trimmed()
        Utility.shared.loginData?.city = (theCurrentModel.selectedData["city"] ?? "").trimmed()
        Utility.shared.loginData?.website = theCurrentView.txtWebsite.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
//        self.redirectToSelectPackage()

        
        self.view.allButtons(isEnable: false)
        let parameter = [APIKey.key_mobile:Utility.shared.loginData!.mobile ,APIKey.key_country_code:Utility.shared.loginData?.phoneCode ?? "",APIKey.key_user_email:Utility.shared.loginData!.email]
        sender.startLoadyIndicator()
        verificationCodeService(parameter: parameter as [String : Any], success: { [weak self] (token) in
            self?.view.allButtons(isEnable: true)
            self?.redirectToVerificationCode(strToken: token)
        }, failed: { [weak self] (error) in
            sender.stopLoadyIndicator()
            self?.showAlertAtBottom(message: error)
            self?.view.allButtons(isEnable: true)
        })
        /*
        signupWebservice(success: { [weak self] (strMsg) in
            sender.stopLoadyIndicator()
//            self?.redirectToSelectPackage()
            self?.view.allButtons(isEnable: true)
            self?.redirectToVerifyEmailVC()
            }, failed: { [weak self] (error) in
                sender.stopLoadyIndicator()
                self?.showAlertAtBottom(message: error)
                self?.view.allButtons(isEnable: true)
        })*/
    }
    
    @IBAction func onBtnSelectTimeZoneAction(_ sender: Any) {
        presentDropDownListVC(selectType: .timeZone)
    }
    
}

//MARK:- Google Place Picker

extension PersonalDetailVC{
    func openGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
}

//MARK:- Google Placepicker Delegate
extension PersonalDetailVC:GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place ID: \(place.placeID ?? "")")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress ?? "")")
        theCurrentView.txtAddress.text = place.formattedAddress ??  ""
        theCurrentView.lblCompanyAddress.text = theCurrentView.txtAddress.text
        getAddressFromLatLon(place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

//MARK:- get address
extension PersonalDetailVC{
    func getAddressFromLatLon(_ coordinate:CLLocationCoordinate2D) {
        
        let ceo: CLGeocoder = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    self.theCurrentModel.selectedData["country"] = pm.country ?? ""
                    self.theCurrentModel.selectedData["state"] = pm.administrativeArea ?? ""
                    self.theCurrentModel.selectedData["city"] = pm.locality ?? ""
                }
        })
        
    }
}

//MARK:- UITextfield Delegate
extension PersonalDetailVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == theCurrentView.txtAddress{
            openGooglePlacePicker()
            return false
        }
        return true
    }
    
}

extension PersonalDetailVC {
    
    func verificationCodeService(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.verificationCode(parameter: parameter, success: { (otpToken,msg) in
            success(otpToken)
            }, failed: { (error) in
                failed(error)
        })
    }
    
    func countryCodeListService() {
        let parameter:[String:Any] = [:]
        theCurrentView.btnNumber.loadingIndicator(true, allignment: .right)
        CommanListWebservice.shared.phoneCodeList(parameter:parameter , success: { [weak self] (list) in
                self?.theCurrentModel.arr_PhoneCodeList = list
                self?.theCurrentView.btnNumber.loadingIndicator(false)
            }, failed: {[weak self] (error) in
                self?.theCurrentView.btnNumber.loadingIndicator(false)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func timeZoneListService() {
        let parameter:[String:Any] = [:]
        theCurrentView.btnSelectTimeZone.loadingIndicator(true, allignment: .right)
        AddUserWebService.shared.getAllTimezoneList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_TimeZoneList = list
            self?.theCurrentView.btnSelectTimeZone.loadingIndicator(false)
            }, failed: { [weak self] (error) in
                self?.theCurrentView.btnSelectTimeZone.loadingIndicator(false)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func signupWebservice(success:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        guard let theModel = Utility.shared.loginData else { return failed(ValidationTexts.SOMETHING_WENT_WRONG) }
        
        let parameter:[String:Any] = [APIKey.key_user_email:theModel.email.trimmed(),
                                      APIKey.key_user_firstname:theModel.firstname.trimmed(),
                                      APIKey.key_user_lastname:theModel.lastname.trimmed(),
                                      APIKey.key_mobile:theModel.mobile.trimmed(),
                                      APIKey.key_country_code:theCurrentModel.selectedData["phoneCodeId"] ?? "",
                                      APIKey.key_company_name:theModel.companyName.trimmed(),
                                      APIKey.key_map_address:(theCurrentView.txtAddress.text ?? "").trimmed(),
                                      APIKey.key_map_state:(theCurrentModel.selectedData["state"] ?? "").trimmed(),
                                      APIKey.key_map_city:(theCurrentModel.selectedData["city"] ?? "").trimmed(),
                                      APIKey.key_map_country:(theCurrentModel.selectedData["country"] ?? "").trimmed(),
                                      APIKey.key_timezone_id:(theCurrentModel.selectedData["timezoneId"] ?? "").trimmed(),
                                      APIKey.key_website:theModel.website.trimmed(),
                                      APIKey.key_user_type:theModel.userType]
        
        AuthenicationService.shared.signup(parameter: parameter, success: { (strMsg) in
            success(strMsg)
        }, failed: { (error) in
            failed(error)
        })
    }
    
}
