//
//  PersonalDetailView.swift
//  StrategyX
//
//  Created by Haresh on 11/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PersonalDetailView: ViewParentWithoutXIB {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var txtFirstName: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    @IBOutlet weak var txtLastName: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorLastName: UILabel!
    @IBOutlet weak var txtEmailAddress: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorEmailAddress: UILabel!
    @IBOutlet weak var txtPhone: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorPhone: UILabel!
    @IBOutlet weak var btnNumber: UIButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var txtCompanyName: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorCompanyName: UILabel!
    @IBOutlet weak var lblCompanyAddress: UILabel!
    @IBOutlet weak var txtAddress: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorAddress: UILabel!
    @IBOutlet weak var txtSelectTimeZone: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorSelectTimeZone: UILabel!
    @IBOutlet weak var txtWebsite: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorWebsite: UILabel!
    @IBOutlet weak var btnSelectTimeZone: UIButton!
    
    //Layout
    func setupLayout() {
        lblErrorFirstName.text = ""
        lblErrorPhone.text = ""
        lblErrorLastName.text = ""
        lblErrorFirstName.text = ""
        lblErrorEmailAddress.text = ""
        
        lblErrorCompanyName.text = ""
        lblErrorAddress.text = ""
        lblErrorSelectTimeZone.text = ""
        lblErrorWebsite.text = ""
        
//        _ = txtFirstName.becomeFirstResponder()
    }
    
    func validateRegister() -> Bool {
        lblErrorFirstName.text = ""
        lblErrorPhone.text = ""
        lblErrorLastName.text = ""
        lblErrorFirstName.text = ""
        lblErrorEmailAddress.text = ""
        
        lblErrorCompanyName.text = ""
        lblErrorAddress.text = ""
        lblErrorSelectTimeZone.text = ""
        lblErrorWebsite.text = ""
        
        if txtFirstName.text!.trimmed().count == 0 {
            lblErrorFirstName.text = "Please enter first name"
            return false
        } else if txtLastName.text!.trimmed().count == 0 {
            lblErrorLastName.text = "Please enter last name"
            return false
        } else if txtEmailAddress.text!.trimmed().count == 0 {
            lblErrorEmailAddress.text = "Please enter email address"
            return false
        } else if !txtEmailAddress.text!.trimmed().isValidEmail() {
            lblErrorEmailAddress.text = "Please enter valid email address"
            return false
        } else if txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            lblErrorPhone.text = "Please enter phone"
            return false
        } else if lblCountryCode.text!.trimmed().isEmpty {
            lblErrorPhone.text = "Please select phone code"
            return false
        } else if txtCompanyName.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            lblErrorCompanyName.text = "Please enter company name"
            return false
        } else if txtAddress.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            lblErrorAddress.text = "Please enter address"
            return false
        } else if txtSelectTimeZone.text!.count == 0 {
            lblErrorSelectTimeZone.text = "Please select time zone"
            return false
        } else if txtWebsite.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            lblErrorWebsite.text = "Please enter website"
            return false
        } else if !txtWebsite.text!.trimmingCharacters(in: .whitespacesAndNewlines).validateURL() {
            lblErrorWebsite.text = "Please enter valid website"
            return false
        } else {
            return true
        }
    }
    
    
    
}

