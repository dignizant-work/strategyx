//
//  VerificationCodeView.swift
//  StrategyX
//
//  Created by Haresh on 28/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class VerificationCodeView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var txtCode: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorCode: UILabel!

    //MARK:- LifeCycle
    func setupLayout() {
        lblErrorCode.text = ""
    }
    
    func validateCode() -> Bool {
        lblErrorCode.text = ""
        
        if txtCode.text!.count == 0 {
            lblErrorCode.text = "Please enter code"
            return false
        } else {
            return true
        }
    }
}
