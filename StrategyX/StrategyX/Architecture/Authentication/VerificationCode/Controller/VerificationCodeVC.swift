//
//  VerificationCodeVC.swift
//  StrategyX
//
//  Created by Haresh on 28/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class VerificationCodeVC: UIViewController {

    fileprivate lazy var theCurrentView: VerificationCodeView = { [unowned self] in
        return self.view as! VerificationCodeView
    }()
    
    internal lazy var theCurrentViewModel: VerificationCodeViewModel = {
        return VerificationCodeViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setToken(strToken:String) {
        theCurrentViewModel.otpToken = strToken
    }
    func setupUI() {
        theCurrentView.setupLayout()
        
    }
    
    //MARK:- redirection
    func redirectToSelectPackage() {
        let vc = SelectPackgeVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnVerifyCodeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateCode() {
            return
        }
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
//         let parameter = [APIKey.key_otp_token:theCurrentViewModel.otpToken,APIKey.key_mobile:Utility.shared.loginData?.mobile ?? "",APIKey.key_country_code:Utility.shared.loginData?.phoneCode ?? ""]
        
        let parameter = [APIKey.key_otp_token:theCurrentViewModel.otpToken,APIKey.key_verification_code:self.theCurrentView.txtCode.text!]

        theCurrentViewModel.verifyCodeService(parameter: parameter, success: { [weak self] (msg) in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.showAlertAtBottom(message: msg)
            self?.redirectToSelectPackage()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
        
    }

    @IBAction func onBtnResendCodeAction(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        let parameter = [APIKey.key_otp_token:theCurrentViewModel.otpToken,APIKey.key_mobile:Utility.shared.loginData?.mobile ?? "",APIKey.key_country_code:Utility.shared.loginData?.phoneCode ?? ""]
        
        theCurrentViewModel.resendVerificationCodeService(parameter: parameter, success: { [weak self] (msg) in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.showAlertAtBottom(message: msg)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
    }
    
    @IBAction func onBtnLoginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        showAlertAction(msg: "Are you sure you want skip this registration process?") { [weak self] in
            self?.backToRootAction()
        }
    }
    
}
