//
//  VerificationCodeViewModel.swift
//  StrategyX
//
//  Created by Haresh on 28/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class VerificationCodeViewModel {

    //MARK:- Variable
    fileprivate weak var theController:VerificationCodeVC!

    var otpToken = ""
    
    
    //MARK:- LifeCycle
    init(theController:VerificationCodeVC) {
        self.theController = theController
    }
}

//MARK:- Api Call
extension VerificationCodeViewModel {
    func resendVerificationCodeService(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.resendVerificationCode(parameter: parameter, success: { (msg) in
            success(msg)
        }, failed: { (error) in
            failed(error)
        })
    }
    
    func verifyCodeService(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.verifyCode(parameter: parameter, success: { (msg) in
            success(msg)
        }, failed: { (error) in
            failed(error)
        })
    }
}
