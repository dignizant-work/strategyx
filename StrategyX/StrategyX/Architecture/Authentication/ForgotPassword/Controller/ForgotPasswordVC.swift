//
//  ForgotPasswordVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ForgotPasswordView = { [unowned self] in
       return self.view as! ForgotPasswordView
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI() {
        theCurrentView.setupLayout()
    }
    
    //MARK:- IBAction
    @IBAction func onBtnRequestResetLinkAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateEmail() {
            return
        }
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
        let parameter = [APIKey.key_email:(theCurrentView.txtEmail.text ?? "").trimmed()]
        forgotPasswordService(parameter: parameter, success: { [weak self] (msg) in
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
                self?.showAlertAtBottom(message: msg)
                self?.onBtnBackToSigninAction("")
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
        
    }
    
    @IBAction func onBtnBackToSigninAction(_ sender: Any) {
        self.view.endEditing(true)
        backAction()
    }
}
//MARK:- Api Call
extension ForgotPasswordVC {
    func forgotPasswordService(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.forgotPassword(parameter: parameter, success: { (msg) in
            success(msg)
        }, failed: { (error) in
            failed(error)
        })
    }
}
