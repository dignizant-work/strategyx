//
//  ForgotPasswordView.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    //MARK:- Variable
    
    @IBOutlet weak var txtEmail: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorEmail: UILabel!
    
    func setupLayout() {
        lblErrorEmail.text = ""
    }
    
    func validateEmail() -> Bool {
        lblErrorEmail.text = ""
        
        if txtEmail.text!.count == 0 {
            lblErrorEmail.text = "Please enter email"
            return false
        } else if !txtEmail.text!.trimmed().isValidEmail() {
            lblErrorEmail.text = "Please enter valid email"
            return false
        } else {
            return true
        }
    }
}
