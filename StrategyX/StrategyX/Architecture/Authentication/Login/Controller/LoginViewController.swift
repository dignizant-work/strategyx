//
//  LoginViewController.swift
//  StrategyX
//
//  Created by Haresh on 11/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import IQKeyboardManagerSwift

class LoginViewController:ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:LoginView = { [unowned self] in
        return self.view as! LoginView
    }()
    
//    private lazy var theCurrentManager: StoreItemDetailManager = {
//        return StoreItemDetailManager(theController: self, theModel: self.theCurrentModel)
//    }()
    
    private lazy var theCurrentModel: LoginModel = {
        return LoginModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utility.shared.loginData = nil
    }
    func setupUI() {
        theCurrentView.setupLayout()
        
    }
    
    //MARK:- Member Funcation
    func redirectToSignupScreen() {
        let vc = PersonalDetailVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func redirectToTermsConditionScreen(_ user:User,_ data:JSON) {
        let vc = TermsConditionVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        vc.setUserData(user, data)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func redirectToForgotPasswordScreen() {
        let vc = ForgotPasswordVC.instantiateFromAppStoryboard(appStoryboard: .authentication)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    func presentToHomeScreen() {
        if Utility.shared.userData != nil {
            guard let sidemenucontroller = AppDelegate.shared.setupSideMenuController() else { return  }
            self.navigationController?.present(sidemenucontroller, animated: true, completion: nil)
        }
    }*/
    
    // MARK:- IBAction
    @IBAction func onBtnBiometricAction(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let _ = UserDefault.shared.getTouchUserData() else { return  }
        guard let lastUserLoginData = UserDefault.shared.getLastUserLoginData() else { return  }
        let isStayIn = theCurrentView.btnStaySignedin.isSelected

        BiometricIDAuth.shared.authenicateUser { [weak self] (strErrorMsg,isRedirectToSetting)  in
            if isRedirectToSetting {
                if let msg = strErrorMsg {
                    self?.showAlertAction(msg: msg, completion: {
                        guard let urlSetting = URL.init(string: UIApplication.openSettingsURLString) else { return }
                        UIApplication.shared.open(urlSetting, options: [:], completionHandler: nil)
                    })
                }
                return
            }
            if let msg = strErrorMsg,!msg.trimmed().isEmpty {
                if !msg.isEmpty {
                    self?.showAlert(msg: msg)
                } else {
                    
                }
            } else if strErrorMsg == nil && isRedirectToSetting == false {
                // successfully athenicate
                push_token = Messaging.messaging().fcmToken ?? ""
                sender.startLoadyIndicator()
                self?.view.allButtons(isEnable: false)
                self?.theCurrentView.btnStaySignedin.isEnabled = true
                
                let parameter = [APIKey.key_email:(lastUserLoginData.email).trimmed(),APIKey.key_password:(lastUserLoginData.password).trimmed()]
                self?.loginService(parameter: parameter, success: { [weak self] (user,data)in
                    if isStayIn {
                        let stayin = JSON(["email":(lastUserLoginData.email).trimmed(),"password":(lastUserLoginData.password).trimmed()])
                        UserDefault.shared.saveStayInUserData(user: stayin)
                    } else {
                        UserDefault.shared.removeStayInUserData()
                        self?.theCurrentView.txtUserName.text = ""
                        self?.theCurrentView.txtPassword.text = ""
                    }
                    sender.stopLoadyIndicator()
                    self?.view.allButtons(isEnable: true)
                    if user.isAgreementAcknowledge == "1"{
                        self?.setUserDataInDefault(user, data)
                        UserDefault.shared.getUserData()
                        AppDelegate.shared.setupSideMenuController()
                    } else {
                        self?.view.endEditing(true)
                        self?.redirectToTermsConditionScreen(user,data)
                    }
                    
                    }, failed: { [weak self] (error) in
                        self?.showAlertAtBottom(message: error)
                        sender.stopLoadyIndicator()
                        self?.view.allButtons(isEnable: true)
                })
            }
        }
        
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !theCurrentView.validateLogin() {
            return
        }
        
        let isStayIn = theCurrentView.btnStaySignedin.isSelected
        push_token = Messaging.messaging().fcmToken ?? ""
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        theCurrentView.btnStaySignedin.isEnabled = true
        
        let stayin = JSON(["email":(self.theCurrentView.txtUserName.text ?? "").trimmed(),"password":(self.theCurrentView.txtPassword.text ?? "").trimmed()])
        
        let parameter = [APIKey.key_email:(theCurrentView.txtUserName.text ?? "").trimmed(),APIKey.key_password:(theCurrentView.txtPassword.text ?? "").trimmed()]
        loginService(parameter: parameter, success: { [weak self] (user,data)in
            if isStayIn {
                UserDefault.shared.saveStayInUserData(user: stayin)
            } else {
                UserDefault.shared.removeStayInUserData()
                self?.theCurrentView.txtUserName.text = ""
                self?.theCurrentView.txtPassword.text = ""
            }
            if let touchData = UserDefault.shared.getTouchUserData(), touchData.email == stayin["email"].stringValue {
                
            } else {
                UserDefault.shared.removeTouchUserData()
            }
            UserDefault.shared.saveLastUserLoginData(user: stayin)
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            if user.isAgreementAcknowledge == "1"{
                self?.setUserDataInDefault(user, data)
                UserDefault.shared.getUserData()
                AppDelegate.shared.setupSideMenuController()
            } else {
                self?.view.endEditing(true)
                self?.redirectToTermsConditionScreen(user,data)
            }

        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
        })
    
    }
    @IBAction func btnCreateAccount(_ sender: Any) {
        self.view.endEditing(true)
        redirectToSignupScreen()
    }
    @IBAction func btnStaySignedInAction(_ sender: Any) {
        theCurrentView.updateBtnStaySignedIn()
    }
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToForgotPasswordScreen()
    }
    
    func setUserDataInDefault(_ user:User,_ data:JSON){
        if let rid = Int(user.roleId), let role = UserRole(rawValue: rid) {
            UserDefault.shared.userRole = role
        }
        print("UserDefault.shared.userRole:=",UserDefault.shared.userRole)
        //                            OfflineManger.shared.writeSingle(type: user)
//        user.companyId = user.companyId == "0" ? "" : user.companyId
        Utility.shared.userData = user
        var userData = data
        userData["company_id"].stringValue = user.companyId
        UserDefault.shared.saveUserData(user: userData)
    }
}

//MARK:- Api Call
extension LoginViewController {
    func loginService(parameter:[String:Any], success:@escaping(_ user:User,_ data:JSON) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.login(parameter: parameter, success: { (user,data) in
            /*if user.isAgreementAcknowledge == "1"{
                self?.setUserDataInDefault(user, data)
            }else{
                 UserDefault.shared.getUserData()
            }*/
            success(user,data)
        }, failed: { (error) in
            failed(error)
        })
    }
}
