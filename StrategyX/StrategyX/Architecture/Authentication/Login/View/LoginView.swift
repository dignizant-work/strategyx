//
//  LoginView.swift
//  StrategyX
//
//  Created by Haresh on 10/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginView: ViewParentWithoutXIB {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    // MARK:- IBOutlet
    
    @IBOutlet weak var btnTouchLogin: UIButton!
    @IBOutlet weak var txtUserName: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorUsername: UILabel!
    @IBOutlet weak var txtPassword: kTextFiledPlaceHolder!
    @IBOutlet weak var lblErrorPassword: UILabel!
    @IBOutlet weak var btnStaySignedin: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    
    let disposeBag = DisposeBag()
    
    
    func setupLayout() {
        btnTouchLogin.isHidden = true
        lblErrorUsername.text = ""
        lblErrorPassword.text = ""
        //Rx Action
        setBtnPasswordAction()
        
        if let stayInUser = UserDefault.shared.getStayInUserData() {
            txtUserName.text = stayInUser.email
            txtPassword.text = stayInUser.password
            updateBtnStaySignedIn()
        }
        
        if let _ = UserDefault.shared.getTouchUserData() {
            btnTouchLogin.isHidden = false
        }
        
        switch BiometricIDAuth.shared.biometricType() {
        case .faceID:
            btnTouchLogin.setTitle("Face ID Login", for: .normal)
            break
        case  .touchID:
            btnTouchLogin.setTitle("Touch ID Login", for: .normal)
            break
        default:
            break
            
        }
    }
    
    func validateLogin() -> Bool {        
        lblErrorUsername.text = ""
        lblErrorPassword.text = ""
        
        if txtUserName.text!.count == 0 {
            lblErrorUsername.text = "Please enter email"
            return false
        } else if !txtUserName.text!.trimmed().isValidEmail() {
            lblErrorUsername.text = "Please enter vaild email"
            return false
        } else if txtPassword.text!.count == 0 {
            lblErrorPassword.text = "Please enter password"
            return false
        } else {
            return true
        }
    }
    
    func updateBtnStaySignedIn() {
        btnStaySignedin.isSelected = !btnStaySignedin.isSelected
    }
    
    func updateBtnPasswordShow() {
        btnPassword.isSelected = !btnPassword.isSelected
        txtPassword.isSecureTextEntry = btnPassword.isSelected ? false : true
    }
    
    //Rx Action
    func setBtnPasswordAction() {
        btnPassword.rx.tap
            .subscribe(onNext:{ [weak self] in
                self?.updateBtnPasswordShow()
            }).disposed(by: disposeBag)
    }
}
