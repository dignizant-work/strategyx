//
//  TermsConditionModel.swift
//  StrategyX
//
//  Created by Jaydeep on 29/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class TermsConditionModel {
    
    //MARK:- Variable
    fileprivate weak var theController:TermsConditionVC!
    var theUserModel:User?
    var dictUser:JSON?
    
    //MARK:- LifeCycle
    init(theController:TermsConditionVC) {
        self.theController = theController
    }

}
