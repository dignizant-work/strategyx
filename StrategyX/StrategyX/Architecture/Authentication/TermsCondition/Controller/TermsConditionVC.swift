//
//  TermsConditionVC.swift
//  StrategyX
//
//  Created by Jaydeep on 29/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import loady
import SwiftyJSON

class TermsConditionVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TermsConditionView = { [unowned self] in
        return self.view as! TermsConditionView
        }()
    
    fileprivate lazy var theCurrentModel:TermsConditionModel = {
        return TermsConditionModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        getTermsCondition()
    }
    
    //MARK:- Action
    
    @IBAction func onBtnAgreeAction(_ sender: UIButton) {
        guard let theModel = theCurrentModel.theUserModel else {
            return
        }
        guard let userDict = theCurrentModel.dictUser else {
            return
        }
        setUserDataInDefault(theModel, userDict)
        UserDefault.shared.getUserData()
        AppDelegate.shared.setupSideMenuController()
    }
    
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction()
    }
    
    func setupData(_ detail:TermsCondition){
        self.theCurrentView.lblTermsCondition.text = detail.contentText
    }
    
    func setUserData(_ user:User,_ data:JSON){
        theCurrentModel.theUserModel = user
        theCurrentModel.dictUser = data
    }
    
    func setUserDataInDefault(_ user:User,_ data:JSON){
        if let rid = Int(user.roleId), let role = UserRole(rawValue: rid) {
            UserDefault.shared.userRole = role
        }
        print("UserDefault.shared.userRole:=",UserDefault.shared.userRole)
//        user.companyId = user.companyId == "0" ? "" : user.companyId
        user.isAgreementAcknowledge = "1"
        Utility.shared.userData = user
        var userData = data
//        userData["company_id"].stringValue = user.companyId
        userData["is_agreement_acknowledge"].stringValue = user.isAgreementAcknowledge
        UserDefault.shared.saveUserData(user: userData)
    }
}

//MARK:- Api Call
extension TermsConditionVC {
    
    func getTermsCondition()
    {
        let parameter = [APIKey.key_content_type:APIKey.key_license_agreement
                         ]
        
        AuthenicationService.shared.getTermsConditions(parameter: parameter, success: { [weak self] (detail) in
            self?.setupData(detail)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
}
