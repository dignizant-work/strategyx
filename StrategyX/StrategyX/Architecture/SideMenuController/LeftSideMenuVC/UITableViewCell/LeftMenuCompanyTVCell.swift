//
//  LeftMenuCompanyTVCell.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class LeftMenuCompanyTVCell: UITableViewCell {

    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,isSelected:Bool) {
        lblTitle.text = strTitle
        lblTitle.textColor = ColorType().setColor(index: isSelected ? 4 : 8)
        btnSelection.isSelected = isSelected
    }
    
}
