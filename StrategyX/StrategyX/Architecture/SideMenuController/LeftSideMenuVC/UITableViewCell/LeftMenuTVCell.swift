//
//  LeftMenuTVCell.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class LeftMenuTVCell: UITableViewCell {

    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strImg:String,strTitle:String) {
        img_icon.image = UIImage(named: strImg)
        lblTitle.text = strTitle
    }
    
}
