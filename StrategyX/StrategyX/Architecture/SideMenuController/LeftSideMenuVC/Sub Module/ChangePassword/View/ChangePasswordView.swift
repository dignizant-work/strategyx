//
//  ChangePasswordView.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChangePasswordView: ViewParentWithoutXIB {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    //MARK:- Variable
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var lblErrorCurrentPassword: UILabel!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var lblErrorNewPassword: UILabel!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var lblErrorConfirmPassword: UILabel!
    
    let disposeBag = DisposeBag()
    
    func setupLayout() {
        lblErrorNewPassword.text = ""
        lblErrorCurrentPassword.text = ""
        lblErrorConfirmPassword.text = ""
        
        //Rx Action
        rxTextCurrentPassword()
        rxTextNewPassword()
        rxTextConfirmPassword()
    }
    
    func validateNewPassword(strPassword:String) -> String {
        
        //--------REQUIREMENTS--------
        let REQUIRED_LENGTH = 8
        let MAXIMUM_LENGTH = 15
        let REQUIRE_SPECIAL_CHARACTERS = true
        let REQUIRE_DIGITS = true
        let REQUIRE_LOWER_CASE = true
        let REQUIRE_UPPER_CASE = false
        
        var currentScore = 0
        var sawUpper = false
        var sawLower = false
        var sawDigit = false
        var sawSpecial = false
        
        for i in 0..<strPassword.count {
            let char = strPassword[i]
            if (!sawSpecial && !(char.isContainsLetters || char.isContainsDigit)) {
                currentScore += 1
                sawSpecial = true
            } else {
                if (!sawDigit && char.isContainsDigit) {
                    currentScore += 1
                    sawDigit = true
                } else {
                    if (!sawUpper || !sawLower) {
                        if char.isContainsUpperCase {
                            sawUpper = true
                        } else {
                            sawLower = true
                        }
                        if sawLower && sawUpper {
                            currentScore += 1
                        }
                    }
                }
            }
        }
        
        if (strPassword.length > REQUIRED_LENGTH) {
            if ((REQUIRE_SPECIAL_CHARACTERS && !sawSpecial)
                || (REQUIRE_UPPER_CASE && !sawUpper)
                || (REQUIRE_LOWER_CASE && !sawLower)
                || (REQUIRE_DIGITS && !sawDigit)) {
                currentScore = 1
            } else {
                currentScore = 2
                if (strPassword.length > MAXIMUM_LENGTH) {
                    currentScore = 3
                }
            }
        } else {
            currentScore = 0
        }
        
        switch (currentScore) {
        case 0:
            return "Weak"
        case 1:
            return "Medium"
        case 2:
            return "Strong"
        case 3:
            return "Very Strong"
        default:
            return "Very Strong"
        }
    }
    func validatePassword() -> Bool {
         if txtCurrentPassword.text!.count == 0 {
            lblErrorCurrentPassword.text = "Please enter current password"
            return false
        } else if txtNewPassword.text!.count == 0 {
            lblErrorNewPassword.attributedText = nil
            lblErrorNewPassword.textColor = red
            lblErrorNewPassword.text = "Please enter new password"
            return false
         } else if txtConfirmPassword.text!.count == 0 {
            lblErrorConfirmPassword.textColor = red
            lblErrorConfirmPassword.text = "Password not match"
            return false
         }  else {
            return true
         }
    }
    
    func setAttributeStringToErrorNewPassword(strPassword:String) {
        let textContent = "Password strength: " + strPassword
        let attriString = NSMutableAttributedString(string: textContent, attributes: [
            NSAttributedString.Key.foregroundColor: appBgLightGrayColor,
            NSAttributedString.Key.font: FontStyle().setFontType(strType: "R15")!])
        
        let range = (textContent as NSString).range(of: strPassword)
        switch (strPassword.localizedLowercase) {
        case "Weak".localizedLowercase:
             attriString.addAttributes([NSAttributedString.Key.foregroundColor: appPink], range: range)
        case "Medium".localizedLowercase:
            attriString.addAttributes([NSAttributedString.Key.foregroundColor: appYellow], range: range)
        case "Strong":
            attriString.addAttributes([NSAttributedString.Key.foregroundColor: appGreen], range: range)
        case "Very Strong":
            attriString.addAttributes([NSAttributedString.Key.foregroundColor: blue], range: range)
        default:
//            "Very Strong"
            attriString.addAttributes([NSAttributedString.Key.foregroundColor: blue], range: range)
        }
       
        lblErrorNewPassword.attributedText = attriString
    }
    
    //MARK:- RX Action
    func rxTextCurrentPassword() {
        txtCurrentPassword.rx.controlEvent(UIControl.Event.editingChanged)
            .subscribe(onNext: { [weak self]  in
                if let obj = self {
                    obj.lblErrorCurrentPassword.text = obj.txtCurrentPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 ? "" : "Please enter new password"
                }
            }).disposed(by: disposeBag)
    }
    func rxTextNewPassword() {
        txtNewPassword.rx.controlEvent(UIControl.Event.editingChanged)
            .subscribe(onNext: { [weak self]  in
                if let obj = self {
                    if obj.txtNewPassword.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                        obj.setAttributeStringToErrorNewPassword(strPassword: obj.validateNewPassword(strPassword: obj.txtNewPassword.text!))
                    } else {
                        obj.lblErrorNewPassword.attributedText = nil
                        obj.lblErrorNewPassword.text = "Please enter new password"
                        obj.lblErrorNewPassword.textColor = red
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    func rxTextConfirmPassword() {
        txtConfirmPassword.rx.controlEvent(UIControl.Event.editingChanged)
            .subscribe(onNext: { [weak self]  in
                if let obj = self {
                    obj.lblErrorConfirmPassword.text = obj.txtConfirmPassword.text! == obj.txtNewPassword.text! ? "Password match" : "Password not match"
                    obj.lblErrorConfirmPassword.textColor = obj.txtConfirmPassword.text! == obj.txtNewPassword.text! ? appGreen : red
                }
            }).disposed(by: disposeBag)
    }
}
