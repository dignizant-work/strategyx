//
//  ChangePasswordVC.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ChangePasswordVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:ChangePasswordView = { [unowned self] in
       return self.view as! ChangePasswordView
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
    }
    
    func emptyAllTextField() {
        theCurrentView.txtCurrentPassword.text = ""
        theCurrentView.txtNewPassword.text = ""
        theCurrentView.txtConfirmPassword.text = ""
    }
    func updateUserData(strNewPassword:String) {
        Utility.shared.userData.password = strNewPassword
        UserDefault.shared.updateUserData()
    }
    
    //MARK:- IBAction
    @IBAction func onBtnSaveChangesAction(_ sender: UIButton) {
        if !theCurrentView.validatePassword() {
            return
        }
        self.view.endEditing(true)
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        let newPassword = theCurrentView.txtConfirmPassword.text ?? ""
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_current_password:theCurrentView.txtCurrentPassword.text ?? "",APIKey.key_new_password:theCurrentView.txtConfirmPassword.text ?? ""]

        changePasswordService(parameter: parameter, success: { [weak self] (msg) in
            self?.updateUserData(strNewPassword: newPassword)
            self?.emptyAllTextField()
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.showAlertAtBottom(message: msg)
        
        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
        })
    }
    
}
//MARK:- Api Call
extension ChangePasswordVC {
    func changePasswordService(parameter:[String:Any], success:@escaping(String) -> Void, failed:@escaping(String) -> Void) {
        AuthenicationService.shared.changePassword(parameter: parameter, success: { (msg) in
            success(msg)
        }, failed: { (error) in
            failed(error)
        })
    }
}
