//
//  TacticalProjectModel.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TacticalProjectModel {

    //MARK:- Variable
    fileprivate weak var thecontroller:TacticalProjectVC!
    var arr_TacticalProjectList:[TacticalProjectList]?
    var nextOffset = 0
    var filterData = FilterData.init()
    var apiLoadingAtTacticalProjectIndex:[String] = []
    var assignToUserFlag = -1

    init(theController:TacticalProjectVC) {
        self.thecontroller = theController
    }
    
    func updateList(list:[TacticalProjectList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_TacticalProjectList?.removeAll()
        }
        nextOffset = offset
        if arr_TacticalProjectList == nil {
            arr_TacticalProjectList = []
        }
        list.forEach({ arr_TacticalProjectList?.append($0) })
        print(arr_TacticalProjectList!)
    }
    
}
