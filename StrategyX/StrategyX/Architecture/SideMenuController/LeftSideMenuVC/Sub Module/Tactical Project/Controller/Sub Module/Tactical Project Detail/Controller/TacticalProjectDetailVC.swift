//
//  TacticalProjectDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TacticalProjectDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:TacticalProjectDetailView = { [unowned self] in
       return self.view as! TacticalProjectDetailView
    }()
    
    fileprivate lazy var theCurrentModel:TacticalProjectDetailModel = {
       return TacticalProjectDetailModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getTacticalProjectDetailWebService()
    }
    
    func setTheData(theTacticalProjectListModel:TacticalProjectList?, isForArchive:Bool = false) {
        theCurrentModel.theTacticalProjectListModel = theTacticalProjectListModel
        theCurrentModel.isForArchive = isForArchive
    }
    
    func checkValidForm()
    {
        self.view.endEditing(true)
        if theCurrentModel.strAddPostText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter post name")
            return
        }
        else
        {
            if theCurrentModel.theTacticalProjectDetailModel  == nil{
                return
            }
            let postText = theCurrentModel.strAddPostText
            theCurrentModel.strAddPostText = ""
            if theCurrentModel.isPostEdit == true{
                let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                     
                                     APIKey.key_user_id: Utility.shared.userData.id,
                                     APIKey.key_notes_id: theCurrentModel.notesId,
                                     APIKey.key_notes: postText
                                     
                ]
                editNotes(parameter: parameter, tag:theCurrentModel.editNotesTag)
                return
            }
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentModel.theTacticalProjectDetailModel!.tactical_projects_id,
                                 APIKey.key_notes: postText,
                                 APIKey.key_note_for: APIKey.key_tactical_projects
            ]
            addNotes(parameter: parameter)
        }
    }
    
    func updateBottomCell() {
        let index = IndexPath(row: 9, section: 0)
        theCurrentModel.isAddPostApiLoading = !theCurrentModel.isAddPostApiLoading
        UIView.performWithoutAnimation {
            let contentOffset = theCurrentView.tableView.contentOffset
            theCurrentView.tableView.reloadRows(at: [index], with: .none)
            theCurrentView.tableView.contentOffset = contentOffset
        }
    }
    
    func update(text:String,dropDown:DropDown,index:Int) {
        switch dropDown {
        case theCurrentModel.moreDD:
            print("text \(text)")
            print("index \(index)")
            theCurrentModel.editNotesTag = dropDown.tag
            let note = theCurrentModel.arr_noteList[dropDown.tag]
            switch index
            {
                case 0:
                    self.theCurrentModel.strAddPostText = note.notes_description
                    self.theCurrentModel.isPostEdit = true
                    self.theCurrentModel.notesId = note.id
                    self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: theCurrentModel.tableviewCount - 1, section: 0)], with: .none)
                    break
                case 1:
                    
                    showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
                        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                                         APIKey.key_notes_id:note.id] as [String : Any]
                        self?.deleteNotes(parameter: parameter, tag: dropDown.tag)
                    }                   
                    break
                default:
                    break
            }
            break
        default:
            break
        }
    }
    
    func updateTableviewData(){
        self.theCurrentModel.strAddPostText = ""
        self.theCurrentModel.editNotesTag = 0
        self.theCurrentModel.notesId = ""
        self.theCurrentModel.isPostEdit = false
        self.theCurrentModel.noteCurrentIndex = 0
        if (theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status.lowercased() == APIKey.key_archived.lowercased()) {
            self.theCurrentModel.tableviewCount = 10
        } else {
            self.theCurrentModel.tableviewCount = 11
        }
//        self.theCurrentModel.tableviewCount = 9
        if self.theCurrentModel.arr_noteList.count != 0
        {
            self.theCurrentModel.tableviewCount += (self.theCurrentModel.arr_noteList.count)
        }
        /*else if (theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status != APIKey.key_archived.lowercased()) && theCurrentModel.arr_noteList.count == 0 {
            self.theCurrentModel.tableviewCount += 1
        } else if (theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status == APIKey.key_archived.lowercased()) && theCurrentModel.arr_noteList.count == 0 {
            self.theCurrentModel.tableviewCount -= 1
        }*/
        self.theCurrentView.tableView.reloadData()
    }
    
}

//MARK:- UITableViewDataSource
extension TacticalProjectDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! TacticalProjectDetailTVCell
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! TacticalProjectDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
//            cell.configureCell1(strTitle: "Reduce visit cycle of machines")
            if theCurrentModel.theTacticalProjectDetailModel != nil {
                cell.configureCell1(strTitle: theCurrentModel.theTacticalProjectDetailModel!.project_name, strCategoryName: theCurrentModel.theTacticalProjectDetailModel!.category_name, isSkeltonAnimation: false)
            } else {
                cell.configureCell1(strTitle: " ", strCategoryName: " ", isSkeltonAnimation: true)
            }
            
        } else if indexPath.row == 1 { //Created By
           // cell.configureCell2(strTypeName: "Created By", strTypeDetail: "Clara Logan")
            if theCurrentModel.theTacticalProjectDetailModel != nil{
                cell.configureCell2(strTypeName: "Created By", strTypeDetail: theCurrentModel.theTacticalProjectDetailModel!.created_by,isSkeltonAnimation:false)
            }
            else{
                cell.configureCell2(strTypeName: "Created By", strTypeDetail: "",isSkeltonAnimation:true)
            }
            
        } else if indexPath.row == 2 { // Discription Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! TacticalProjectDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theTacticalProjectDetailModel != nil{
                cell.configureCell3(strDescription: theCurrentModel.theTacticalProjectDetailModel!.tactical_description,isSkeltonAnimation:false)
            }
            else{
                cell.configureCell3(strDescription: "",isSkeltonAnimation:true)
            }
           // cell.configureCell3(strDescription: "Lorem ipsum dolor sit amet, consectetur elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc.")
        } else if indexPath.row >= 3 && indexPath.row <= 7 {
            if indexPath.row == 7 {
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell5") as! TacticalProjectDetailTVCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                if theCurrentModel.theTacticalProjectDetailModel != nil{
                    cell.configureCell5(strCost: theCurrentModel.theTacticalProjectDetailModel!.cost, strStartDate: theCurrentModel.theTacticalProjectDetailModel!.startdate, strDueDate: theCurrentModel.theTacticalProjectDetailModel!.duedate, strReviseDueDate: theCurrentModel.theTacticalProjectDetailModel!.revised_date,isSkeltonAnimation: false)
                }
                else{
                    cell.configureCell5(strCost: "", strStartDate: "", strDueDate: "", strReviseDueDate: "",isSkeltonAnimation: true)
                }
                
            } else {
                if indexPath.row == 3 {
                    if theCurrentModel.theTacticalProjectDetailModel != nil{
                        cell.configureCell2(strTypeName: "Category", strTypeDetail: theCurrentModel.theTacticalProjectDetailModel!.category_name,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Category", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                    
                } else if indexPath.row == 4 {
                    if theCurrentModel.theTacticalProjectDetailModel != nil{
                        cell.configureCell2(strTypeName: "Company", strTypeDetail: theCurrentModel.theTacticalProjectDetailModel!.company_name,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Company", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                } else if indexPath.row == 5 {
                    if theCurrentModel.theTacticalProjectDetailModel != nil{
                        cell.configureCell2(strTypeName: "Assign To", strTypeDetail: theCurrentModel.theTacticalProjectDetailModel!.assigned_to,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Assign To", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                } else if indexPath.row == 6 {
                    if theCurrentModel.theTacticalProjectDetailModel != nil{
                        cell.configureCell2(strTypeName: "Department", strTypeDetail: theCurrentModel.theTacticalProjectDetailModel!.department_name,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Department", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                }
            }
        } else if indexPath.row == 8 {
            var dash = ""
            if theCurrentModel.arr_noteList.count <= 0 {
                dash = "-"
            }
            cell.configureCell2(strTypeName: "Notes", strTypeDetail: dash,isForNotes: true,isSkeltonAnimation:false)
        } else if indexPath.row == 9 && (theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status.lowercased() != APIKey.key_archived.lowercased()) { // Add notes
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell6") as! TacticalProjectDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configureCell6(isLoading: theCurrentModel.isAddPostApiLoading)
            
            cell.handlerText = {[weak self] (strText) in
//                cell.txtAddNote.text = strText
                self?.theCurrentModel.strAddPostText = strText
            }
            cell.handlerOnBtnAddPostClick = {[weak self] in
                self?.checkValidForm()
                cell.txtAddNote.text = ""
            }
            
        } else if indexPath.row == theCurrentModel.tableviewCount - 1 { // Last Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! TacticalProjectDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
//            cell.configureCell4(isLoading: theCurrentModel.isAddPostApiLoading)
//            cell.txtAddNote.text = self.theCurrentModel.strAddPostText
        
            if theCurrentModel.theTacticalProjectDetailModel != nil {
                if !UserDefault.shared.isCanEditForm(strOppoisteID: theCurrentModel.theTacticalProjectDetailModel?.created_by_id ?? "") || UserDefault.shared.isCanArchiveForm(strOppoisteID: theCurrentModel.theTacticalProjectDetailModel?.created_by_id ?? "", completionFlag: theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status == APIKey.key_archived.lowercased() ? 1 : 0) {
                    cell.btnSave.isHidden = true
                } else {
                    cell.btnSave.isHidden = false
                }
            }
            
            cell.handlerOnBtnCloseClick = { [weak self] in
                self?.backAction(isAnimation: false)
            }
        /*            if theCurrentModel.theTacticalProjectDetailModel != nil{
                if theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status.lowercased() == APIKey.key_archived.lowercased(){
                    cell.btnSave.isHidden = true
                }
            }
            */
            cell.handlerOnBtnSaveClick = {[weak self] in
                if self?.theCurrentModel.theTacticalProjectDetailModel == nil{
                    return
                }
                
                let vc = AddTacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .main)
//                vc.setUpdateScreen(editType: .edit, tacticalProjectDetail: (self?.theCurrentModel.theTacticalProjectDetailModel)!)
//                vc.handlerUpdateModel = { [weak self] in
//                    self?.getTacticalProjectDetailWebService()
//                }
                self?.push(vc: vc)
            }
        } else { // Notes
            let noteCell = tableView.dequeueReusableCell(withIdentifier: "RisksNoteTVCell") as! RisksNoteTVCell
            noteCell.contentView.backgroundColor = .clear
            noteCell.backgroundColor = .clear
            noteCell.selectionStyle = .none
            if theCurrentModel.arr_noteList.count <= 0{
                noteCell.lblNorecordfound.isHidden = false
                return noteCell
            }
            noteCell.lblNorecordfound.isHidden = true
            noteCell.btnMoreOutlet.isHidden = true
            /*if theCurrentModel.theTacticalProjectDetailModel?.tacticalProject_status.lowercased() == APIKey.key_archived.lowercased(){
                noteCell.btnMoreOutlet.isHidden = true
            }
            noteCell.btnMoreOutlet.tag = theCurrentModel.noteCurrentIndex
            theCurrentModel.arr_MoreList = ["Edit","Delete"]
            noteCell.onHandlerBtnMore = {[weak self] (tag) in
                self?.view.endEditing(true)
                self?.theCurrentModel.moreDD.tag = tag
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: noteCell.btnMoreOutlet)
                }
                self?.theCurrentModel.moreDD.show()
            }
            */
            let note = theCurrentModel.arr_noteList[theCurrentModel.noteCurrentIndex]
            var dateDue = ""
            let date = note.created_date
            let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType5)
            dateDue = dueDateformate!
            noteCell.configure(strDescription: note.notes_description, name: note.from_user_name, date: "Posted on \(dateDue)", img: note.assigned_to_user_image)
            if theCurrentModel.noteCurrentIndex < theCurrentModel.arr_noteList.count - 1
            {
                theCurrentModel.noteCurrentIndex += 1
            }
            return noteCell
            
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension TacticalProjectDetailVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4 && UserDefault.shared.userRole != .superAdminUser {
            return CGFloat.leastNormalMagnitude
        }
        return UITableView.automaticDimension
    }
}


//MARK:- Api Call
extension TacticalProjectDetailVC {
    func getTacticalProjectDetailWebService() {
        guard let tacticalProjectsId = theCurrentModel.theTacticalProjectListModel?.tacticalProjectsId else { return }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_tactical_project_id:tacticalProjectsId] as [String : Any]
        
        TacticalProjectWebService.shared.getTacticalProjectDetail(parameter: parameter, success: { [weak self] (detail) in
            
            self?.theCurrentModel.theTacticalProjectDetailModel = detail
            self?.theCurrentModel.arr_noteList = []
//            self?.theCurrentModel.arr_noteList = Array(detail.notesList)
            self?.updateTableviewData()
            }, failed: { [weak self] (error) in
                self?.view.allButtons(isEnable: true)
                //                self?.setBackground(strMsg: error)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func editNotes(parameter:[String:Any],tag:Int){
        updateBottomCell()
        TacticalProjectWebService.shared.postEditTacticalProjectNotes(parameter: parameter, success: { [weak self] (data) in
                self?.updateBottomCell()
                self?.theCurrentModel.arr_noteList[tag] = data
                self?.updateTableviewData()
            }, failed: { (error) in
                self.updateBottomCell()
                self.showAlertAtBottom(message: error)
        })
    }
    
    func addNotes(parameter:[String:Any])
    {
        updateBottomCell()
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.updateBottomCell()
            self?.theCurrentModel.arr_noteList.append(data)
            self?.updateTableviewData()
            }, failed: { (error) in
                self.updateBottomCell()
                self.showAlertAtBottom(message: error)
        })
        
        
    }
    
    func deleteNotes(parameter:[String:Any],tag:Int)
    {
       TacticalProjectWebService.shared.postDeleteNotesTacticalProjectNotes(parameter: parameter, success: { [weak self] in
                self?.theCurrentModel.arr_noteList.remove(at: tag)
                self?.updateTableviewData()
            }, failed: { (error) in
                self.showAlertAtBottom(message: error)
        })
        
    }
}
