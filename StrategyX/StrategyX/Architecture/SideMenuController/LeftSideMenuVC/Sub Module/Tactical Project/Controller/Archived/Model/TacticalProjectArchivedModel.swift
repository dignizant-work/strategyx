//
//  TacticalProjectArchivedModel.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

struct SelectedFile {
    var selectedFileName = ""
    var selectedFileData:Data?
    var selectedFiletype = SelectedFileType.file
    
    
    init(selectedFileName:String = "" ,selectedFileData:Data = Data()) {
        self.selectedFileName = selectedFileName
        self.selectedFileData = selectedFileData
        self.selectedFiletype = SelectedFileType.file
    }
    
    init(selectedAudioFileName:String = "" ,selectedAudioFileData:Data = Data()) {
        self.selectedFileName = selectedAudioFileName
        self.selectedFileData = selectedAudioFileData
        self.selectedFiletype = SelectedFileType.audio

    }
}



class TacticalProjectArchivedModel {

    //MARK:- Variable
    weak var theController:TacticalProjectArchivedVC!
    var pageOffset:Int = 0
    var filterData = FilterData.init()
    var apiLoadingAtTacticalProjectIndex:[String] = []

    init(theController:TacticalProjectArchivedVC) {
        self.theController = theController
    }
    
    // get category
    
    var arr_archivedList:[TacticalProjectList]?
    
    func updateList(list:[TacticalProjectList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_archivedList?.removeAll()
        }
        pageOffset = offset
        if arr_archivedList == nil {
            arr_archivedList = []
        }
        list.forEach({ arr_archivedList?.append($0) })
        print(arr_archivedList!)
    }
    
    
}
