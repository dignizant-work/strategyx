//
//  TacticalProjectArchivedVC.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class TacticalProjectArchivedVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:TacticalProjectArchivedView = { [unowned self] in
       return self.view as! TacticalProjectArchivedView
    }()
    
    fileprivate lazy var theCurrentModel:TacticalProjectArchivedModel = {
       return TacticalProjectArchivedModel(theController: self)
    }()
    let moreDD = DropDown()

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        archivedService()
        //        displayContentController(content: subListViewController(step: .step0, title: "Unwanted Events"))
    }
    
    func setCompanyData()
    {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func configureDropDown(dropDown:DropDown, view:UIView,at indexRow:Int) {
        let cellView = theCurrentView.tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        let tacticalProjectModel = self.theCurrentModel.arr_archivedList![view.tag]
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(cellView.contentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: cellView.contentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            
            // Setup your custom UI components
            switch item {
            case MoreAction.chart.rawValue:
                cell.configure(iconName: "ic_report_profile_screen", title: item)
                break
            case MoreAction.action.rawValue:
                cell.configure(iconName: "ic_plus_icon_popup", title: item)
                break
            case MoreAction.edit.rawValue:
                cell.configure(iconName: "ic_edit_icon_popup", title: item)
                break
            case MoreAction.delete.rawValue:
                cell.configure(iconName: "ic_delete_icon_popup", title: item)
                break
            case MoreAction.archived.rawValue:
                cell.configure(iconName: "ic_achive_icon", title: item)
                break
            default:
                cell.configure(iconName: "ic_delete_notes", title: item)
                break
            }
        }
        
        switch dropDown {
        case moreDD:
            
            var strArrayMore:[String] = []
            strArrayMore = [MoreAction.chart.rawValue,MoreAction.delete.rawValue]
            if !UserDefault.shared.isCanEditForm(strOppoisteID: tacticalProjectModel.createdById)  {
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                    strArrayMore.remove(at: index) // edit
                }
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                    strArrayMore.remove(at: index) // delete
                }
            }
            
        
          
            dropDown.dataSource = strArrayMore
            break
            
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index),dropdown tag \(view.tag)")
            switch dropDown {
            case self.moreDD:
                switch item {
                case MoreAction.chart.rawValue:
                    self.presentGraphAction(strChartId: tacticalProjectModel.tacticalProjectsId)
                    break
                case MoreAction.action.rawValue:
//                    self.redirectToAddActionScreen(theTacticalProjectModel: tacticalProjectModel)
                    break
                case MoreAction.edit.rawValue:
//                    self.theCurrentModel.isEditTacticalProject = false
//                    self.getTacticalProjectDetailWebService(tacticalProjectsId: tacticalProjectModel.tacticalProjectsId)
                    break
                case MoreAction.delete.rawValue:
                    self.deleteTacticalProject(strTacticalProjectID: tacticalProjectModel.tacticalProjectsId)
                    break
                case MoreAction.archived.rawValue:
                    self.archiveTacticalProject(strTacticalProjectID: tacticalProjectModel.tacticalProjectsId)
                    break
                default:
                    break
                }
                break
                
            default:
                break
            }
            
        }
    }
    func archiveTacticalProject(strTacticalProjectID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtTacticalProjectIndex.append(strTacticalProjectID)
            self?.updateBottomCell(loadingID: strTacticalProjectID, isLoadingApi: true)
            self?.changeTacticalProjectStatusWebService(strTacticalProjectID: strTacticalProjectID, isDelete: false,isArchive: true)
        }
    }
    
    func deleteTacticalProject(strTacticalProjectID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtTacticalProjectIndex.append(strTacticalProjectID)
            self?.updateBottomCell(loadingID: strTacticalProjectID, isLoadingApi: true)
            self?.changeTacticalProjectStatusWebService(strTacticalProjectID: strTacticalProjectID, isDelete: true)
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_archivedList else { return  }
        var indexRow = -1
        if let index = model.firstIndex(where: {$0.tacticalProjectsId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtTacticalProjectIndex.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtTacticalProjectIndex.remove(at: index)
            }
            theCurrentModel.arr_archivedList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_archivedList?.count == 0 {
                setBackground(strMsg: "Tactical Project not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtTacticalProjectIndex.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtTacticalProjectIndex.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    //MARK:- Redirect To
    func redirectToTacticalProjectSublist1VC(theTacticalProjectID:String,theTacticalProjectDetailModel:TacticalProjectDetail) {
        guard let theModel = theCurrentModel.arr_archivedList?.first(where: {$0.tacticalProjectsId == theTacticalProjectID }) else {
            return
        }
        let vc = TacticalProjectSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theTacticalProjectListModel: theModel, theTacticalProjectDetailModel: theTacticalProjectDetailModel, isForArchive: true)
        self.push(vc: vc)
    }
    func updateTacticalProjectListModelFromActionSubList(theTacticalProjectModel:TacticalProjectList) {
        if let index = theCurrentModel.arr_archivedList?.firstIndex(where: {$0.tacticalProjectsId == theTacticalProjectModel.tacticalProjectsId}) {
            theCurrentModel.arr_archivedList?.remove(at: index)
            theCurrentModel.arr_archivedList?.insert(theTacticalProjectModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateTacticalProjectListModel(theTacticalProjectDetailModel:TacticalProjectDetail) {
        if let index = theCurrentModel.arr_archivedList?.firstIndex(where: {$0.tacticalProjectsId == theTacticalProjectDetailModel.tactical_projects_id}) {
            theCurrentModel.arr_archivedList![index].projectName = theTacticalProjectDetailModel.project_name
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    /*
     func subListViewController(step:RisksSubControllerType,title:String) -> RisksSubListVC {
     let vc = RisksSubListVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
     vc.setTheData(type: step, title: title)
     //        vc.delegate = self
     theCurrentModel.subViewControllers.append(vc)
     return vc
     }*/
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .tacticalProject)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            self?.theCurrentModel.pageOffset = 0
            if filterData.searchText != ""{
                totalFilterCount += 1
            }
            if filterData.categoryID != ""{
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_archivedList?.removeAll()
            self?.theCurrentModel.arr_archivedList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.archivedService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .tacticalProject)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnTacticalProjectGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension TacticalProjectArchivedVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_archivedList == nil || theCurrentModel.arr_archivedList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_archivedList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RisksSubListTVCell") as! RisksSubListTVCell
        //cell.configure(strTitle: "License to oprate business is", strSubTitle: "W: 21 Oct 1:30pm. D: 30 Oct 5:30pm")
        if theCurrentModel.arr_archivedList != nil
        {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtTacticalProjectIndex.contains(theCurrentModel.arr_archivedList![indexPath.row].tacticalProjectsId) {
                isApiLoading = true
            }
            cell.configureTacticalProject(theModel: theCurrentModel.arr_archivedList![indexPath.row],isApiLoading:isApiLoading)
//            cell.updateBackGroundColor(isCompleted: theCurrentModel.arr_archivedList![indexPath.row].completeActionFlag == 1)
            cell.btnMore.tag = indexPath.row
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore,at: indexPath.row)
                    obj.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.pageOffset != -1 {
                archivedService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension TacticalProjectArchivedVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_archivedList != nil {
            getTacticalProjectDetailWebService(tacticalProjectsId: theCurrentModel.arr_archivedList![indexPath.row].tacticalProjectsId, isEdit: false)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.pageOffset != -1 && theCurrentModel.arr_archivedList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
}

//MARK:- call API

extension TacticalProjectArchivedVC
{
    func getTacticalProjectDetailWebService(tacticalProjectsId:String,isEdit:Bool) {
        
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_tactical_project_id:tacticalProjectsId] as [String : Any]
        
        TacticalProjectWebService.shared.getTacticalProjectDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            self?.redirectToTacticalProjectSublist1VC(theTacticalProjectID: tacticalProjectsId, theTacticalProjectDetailModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func archivedService(isRefreshing:Bool = false, isFilter:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.pageOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_category:theCurrentModel.filterData.categoryID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_grid_type:APIKey.key_archived,
                         APIKey.key_page_offset:"\(theCurrentModel.pageOffset)"]
        ArchivedWebservice.shared.getTacticalProjectArchivedList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func changeTacticalProjectStatusWebService(strTacticalProjectID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strTacticalProjectID]]).rawString() else {
            updateBottomCell(loadingID: strTacticalProjectID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_tactical_projects,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        TacticalProjectWebService.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strTacticalProjectID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strTacticalProjectID)
        })
    }
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_archivedList == nil || theCurrentModel.arr_archivedList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
}
