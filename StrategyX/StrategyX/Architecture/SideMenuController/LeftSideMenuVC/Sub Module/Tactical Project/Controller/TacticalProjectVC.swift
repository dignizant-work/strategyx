//
//  TacticalProjectVC.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class TacticalProjectVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TacticalProjectView = { [unowned self] in
       return self.view as! TacticalProjectView
    }()
    
    fileprivate lazy var theCurrentModel:TacticalProjectModel = {
       return TacticalProjectModel(theController: self)
    }()
    
    //MARK:- Variabled Declaration
    
    let moreDD = DropDown()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getTacticalProjectWebService()
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func updateTacticalProjectListModelFromActionSubList(theTacticalProjectModel:TacticalProjectList) {
        if let index = theCurrentModel.arr_TacticalProjectList?.firstIndex(where: {$0.tacticalProjectsId == theTacticalProjectModel.tacticalProjectsId}) {
            theCurrentModel.arr_TacticalProjectList?.remove(at: index)
            theCurrentModel.arr_TacticalProjectList?.insert(theTacticalProjectModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateTacticalProjectListModel(theTacticalProjectDetailModel:TacticalProjectDetail?) {
        guard let theDetailModel = theTacticalProjectDetailModel else { return  }
        if let index = theCurrentModel.arr_TacticalProjectList?.firstIndex(where: {$0.tacticalProjectsId == theDetailModel.tactical_projects_id}) {
            theCurrentModel.arr_TacticalProjectList![index].projectName = theDetailModel.project_name
            theCurrentModel.arr_TacticalProjectList![index].duedate = theDetailModel.duedate
            theCurrentModel.arr_TacticalProjectList![index].revisedDate = theDetailModel.revised_date
            theCurrentModel.arr_TacticalProjectList![index].duedateColor = theDetailModel.duedateColor
            theCurrentModel.arr_TacticalProjectList![index].revisedColor = theDetailModel.revisedColor
            theCurrentModel.arr_TacticalProjectList![index].assigneeId = theDetailModel.assignee_id
            theCurrentModel.arr_TacticalProjectList![index].assignedTo = theDetailModel.assigned_to
            theCurrentModel.arr_TacticalProjectList![index].assignedToUserImage = theDetailModel.assignedToUserImage

            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
//            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_TacticalProjectList else { return  }
        var indexRow = -1
        if let index = model.firstIndex(where: {$0.tacticalProjectsId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtTacticalProjectIndex.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtTacticalProjectIndex.remove(at: index)
            }
            theCurrentModel.arr_TacticalProjectList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_TacticalProjectList?.count == 0 {
                setBackground(strMsg: "Tactical Project not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtTacticalProjectIndex.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtTacticalProjectIndex.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_TacticalProjectList?.firstIndex(where: {$0.tacticalProjectsId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtTacticalProjectIndex.append(strActionLogID)
            } else {
                if let index = theCurrentModel.apiLoadingAtTacticalProjectIndex.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtTacticalProjectIndex.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_TacticalProjectList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_TacticalProjectList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_TacticalProjectList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_TacticalProjectList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_TacticalProjectList![index].workdateColor = model.workdateColor
                        //                        theCurrentModel.arr_TacticalProjectList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func redirectToTacticalProjectArchivedVC() {
        let vc = TacticalProjectArchivedVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        self.push(vc: vc)
    }
    
    func redirectToTacticalProjectSublist1VC(theTacticalProjectID:String,theTacticalProjectDetailModel:TacticalProjectDetail) {
        guard let theModel = theCurrentModel.arr_TacticalProjectList?.first(where: {$0.tacticalProjectsId == theTacticalProjectID }) else {
            return
        }
        let vc = TacticalProjectSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theTacticalProjectListModel: theModel, theTacticalProjectDetailModel: theTacticalProjectDetailModel)
        self.push(vc: vc)
    }
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .tacticalProject)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        var filterData = theCurrentModel.filterData
        if theCurrentModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != ""{
                totalFilterCount += 1
            }
            if filterData.categoryID != ""{
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentModel.filterData.assigntoID = "0"
            }
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_TacticalProjectList?.removeAll()
            self?.theCurrentModel.arr_TacticalProjectList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getTacticalProjectWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphAction(strChartId:String) {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .tacticalProject)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToEditTacticalProjectVC(theModel:TacticalProjectDetail) {
        let vc = AddTacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, tacticalProjectDetail: theModel)
        vc.handlerUpdateModel = { [weak self] (theDetailModel) in
            self?.updateTacticalProjectListModel(theTacticalProjectDetailModel: theDetailModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToAddActionScreen(theTacticalProjectModel:TacticalProjectList) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddTacticalProjectData(theTacticalProjectId: theTacticalProjectModel.tacticalProjectsId, theTacticalProjectName: theTacticalProjectModel.projectName, theDueDate: theTacticalProjectModel.duedate, theDueDateColor: theTacticalProjectModel.duedateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theTacticalProjectModel.tacticalProjectStatus))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentModel.arr_TacticalProjectList?.firstIndex(where: {$0.tacticalProjectsId == theTacticalProjectModel.tacticalProjectsId}) {
                let model = self!.theCurrentModel.arr_TacticalProjectList![index]
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.theCurrentModel.arr_TacticalProjectList?.remove(at: index)
                self?.theCurrentModel.arr_TacticalProjectList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.tacticalProjectsId)
            }
        }
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theTacticalProjectModel
            
            if let index = self?.theCurrentModel.arr_TacticalProjectList?.firstIndex(where: {$0.tacticalProjectsId == theTacticalProjectModel.tacticalProjectsId}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentModel.arr_TacticalProjectList?.remove(at: index)
                self?.theCurrentModel.arr_TacticalProjectList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.tacticalProjectsId)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnTacticalProjectGraphAction(_ sender: Any) {
        presentGraphAction(strChartId: "")
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        /*var title = ""
         if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
         title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
         }
         
         if let vc = theCurrentModel.subViewControllers.last {
         removeContentController(content: vc)
         theCurrentView.updateTitleText(strTitle: title)
         theCurrentModel.subViewControllers.removeLast()
         if theCurrentModel.subViewControllers.count == 1 {
         theCurrentView.backButtonRisk(isHidden: true)
         }
         print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
         }*/
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToTacticalProjectArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_TacticalProjectList == nil || theCurrentModel.arr_TacticalProjectList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView,at indexRow:Int) {
        let cellView = theCurrentView.tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        let tacticalProjectModel = self.theCurrentModel.arr_TacticalProjectList![view.tag]
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(cellView.contentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: cellView.contentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            
            // Setup your custom UI components
            switch item {
            case MoreAction.chart.rawValue:
                cell.configure(iconName: "ic_report_profile_screen", title: item)
                break
            case MoreAction.action.rawValue:
                cell.configure(iconName: "ic_plus_icon_popup", title: item)
                break
            case MoreAction.edit.rawValue:
                cell.configure(iconName: "ic_edit_icon_popup", title: item)
                break
            case MoreAction.delete.rawValue:
                cell.configure(iconName: "ic_delete_icon_popup", title: item)
                break
            case MoreAction.archived.rawValue:
                cell.configure(iconName: "ic_achive_icon", title: item)
                break
            default:
                cell.configure(iconName: "ic_delete_notes", title: item)
                break
            }
        }
        
        switch dropDown {
        case moreDD:
            
            var strArrayMore:[String] = []
            strArrayMore = [MoreAction.chart.rawValue,MoreAction.action.rawValue,MoreAction.edit.rawValue,MoreAction.delete.rawValue,MoreAction.archived.rawValue]
            if !UserDefault.shared.isCanEditForm(strOppoisteID: tacticalProjectModel.createdById)  {
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                    strArrayMore.remove(at: index) // edit
                }
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                    strArrayMore.remove(at: index) // delete
                }
            }
            
            if !UserDefault.shared.isCanArchiveForm(strOppoisteID: tacticalProjectModel.createdById, completionFlag: tacticalProjectModel.completeActionFlag) {
                strArrayMore.removeLast() // archive
            }
            /*
            if tacticalProjectModel.createdById == Utility.shared.userData.id{
                strArrayMore.append(MoreAction.delete.rawValue)
            }
            if tacticalProjectModel.completeActionFlag == 1 && tacticalProjectModel.createdById == Utility.shared.userData.id{
                strArrayMore.append(MoreAction.archived.rawValue)
            }*/
            dropDown.dataSource = strArrayMore
            break
            
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index),dropdown tag \(view.tag)")
            switch dropDown {
            case self.moreDD:
                switch item {
                case MoreAction.chart.rawValue:
                    self.presentGraphAction(strChartId: tacticalProjectModel.tacticalProjectsId)
                    break
                case MoreAction.action.rawValue:
                    self.redirectToAddActionScreen(theTacticalProjectModel: tacticalProjectModel)
                    break
                case MoreAction.edit.rawValue:
                    self.getTacticalProjectDetailWebService(tacticalProjectsId: tacticalProjectModel.tacticalProjectsId, isEdit: true)
                    break
                case MoreAction.delete.rawValue:
                    self.deleteTacticalProject(strTacticalProjectID: tacticalProjectModel.tacticalProjectsId)
                    break
                case MoreAction.archived.rawValue:
                    self.archiveTacticalProject(strTacticalProjectID: tacticalProjectModel.tacticalProjectsId)
                    break
                default:
                    break
                }
                break
                
            default:
                break
            }
            
        }
    }
    
    
    func archiveTacticalProject(strTacticalProjectID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtTacticalProjectIndex.append(strTacticalProjectID)
            self?.updateBottomCell(loadingID: strTacticalProjectID, isLoadingApi: true)
            self?.changeTacticalProjectStatusWebService(strTacticalProjectID: strTacticalProjectID, isDelete: false,isArchive: true)
        }
    }
    
    func deleteTacticalProject(strTacticalProjectID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtTacticalProjectIndex.append(strTacticalProjectID)
            self?.updateBottomCell(loadingID: strTacticalProjectID, isLoadingApi: true)
            self?.changeTacticalProjectStatusWebService(strTacticalProjectID: strTacticalProjectID, isDelete: true)
        }
    }
    
    
}
//MARK:-UITableViewDataSource
extension TacticalProjectVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_TacticalProjectList == nil || theCurrentModel.arr_TacticalProjectList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_TacticalProjectList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RisksSubListTVCell") as! RisksSubListTVCell       
        if theCurrentModel.arr_TacticalProjectList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtTacticalProjectIndex.contains(theCurrentModel.arr_TacticalProjectList![indexPath.row].tacticalProjectsId) {
                isApiLoading = true
            }
            cell.configureTacticalProject(theModel: theCurrentModel.arr_TacticalProjectList![indexPath.row],isApiLoading:isApiLoading)
            cell.updateBackGroundColor(isCompleted: theCurrentModel.arr_TacticalProjectList![indexPath.row].completeActionFlag == 1)
            cell.btnMore.tag = indexPath.row
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_TacticalProjectList?[indexPath.row].tacticalProjectStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_TacticalProjectList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_TacticalProjectList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_TacticalProjectList![indexPath.row].tacticalProjectsId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore,at: indexPath.row)
                    obj.moreDD.show()
                }
            }

            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getTacticalProjectWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension TacticalProjectVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_TacticalProjectList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr_model = theCurrentModel.arr_TacticalProjectList, arr_model.count > 0, !theCurrentModel.apiLoadingAtTacticalProjectIndex.contains(arr_model[indexPath.row].tacticalProjectsId) else { return  }
        getTacticalProjectDetailWebService(tacticalProjectsId: arr_model[indexPath.row].tacticalProjectsId, isEdit: false)
//        redirectToTacticalProjectSublist1VC(theModel: arr_model[indexPath.row])
    }
}

//MARK:- Api Call
extension TacticalProjectVC {
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_tactical_projects,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
    func getTacticalProjectDetailWebService(tacticalProjectsId:String,isEdit:Bool) {
        
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_tactical_project_id:tacticalProjectsId] as [String : Any]
        
        TacticalProjectWebService.shared.getTacticalProjectDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            if isEdit {
                self?.redirectToEditTacticalProjectVC(theModel: detail)
            } else {
                self?.redirectToTacticalProjectSublist1VC(theTacticalProjectID: tacticalProjectsId, theTacticalProjectDetailModel: detail)
            }

            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func getTacticalProjectWebService(isRefreshing:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_category:theCurrentModel.filterData.categoryID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        TacticalProjectWebService.shared.getAllTacticalList(parameter: parameter, success: { [weak self] (list, nextOffset,assignToUserFlag) in
            if assignToUserFlag == 1 && (self?.theCurrentModel.nextOffset ?? -1) == 0 { // Need to set filter count
                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.theCurrentModel.filterData.assigntoID = Utility.shared.userData.id
                self?.theCurrentModel.assignToUserFlag = assignToUserFlag
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    /*func deleteTacticalProjectWebService() {
        let index = theCurrentModel.apiLoadingAtIndex
        
        let tacticalProjectID = theCurrentModel.arr_TacticalProjectList![index].tacticalProjectsId
        guard let jsonString = JSON([[APIKey.key_status_id:tacticalProjectID]]).rawString() else {
            updateBottomCell(atRowIndex: index)
            return
        }
        //        status_for_id
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_tactical_projects,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        
        TacticalProjectWebService.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(atRowIndex: index, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(atRowIndex: index)
        })
    }*/
    
    func changeTacticalProjectStatusWebService(strTacticalProjectID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strTacticalProjectID]]).rawString() else {
            updateBottomCell(loadingID: strTacticalProjectID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_tactical_projects,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        TacticalProjectWebService.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strTacticalProjectID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strTacticalProjectID)
        })
    }
}
