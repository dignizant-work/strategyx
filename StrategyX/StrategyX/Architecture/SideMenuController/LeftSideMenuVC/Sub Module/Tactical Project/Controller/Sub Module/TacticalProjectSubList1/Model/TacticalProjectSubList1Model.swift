//
//  TacticalProjectSubList1Model.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TacticalProjectSubList1Model: NSObject {

    //MARK:- Variable
    weak var theController:TacticalProjectSubList1VC!
    var theTacticalProjectListModel:TacticalProjectList? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers {
                if let index = controller.firstIndex(where: {($0 as? TacticalProjectVC) != nil}) {
                    (controller[index] as? TacticalProjectVC)?.updateTacticalProjectListModelFromActionSubList(theTacticalProjectModel: theTacticalProjectListModel!)
                }
                if let index = controller.firstIndex(where: {($0 as? TacticalProjectArchivedVC) != nil}) {
                    (controller[index] as? TacticalProjectArchivedVC)?.updateTacticalProjectListModelFromActionSubList(theTacticalProjectModel: theTacticalProjectListModel!)
                }
            }
        }
    }
    var theTacticalProjectDetailModel:TacticalProjectDetail?

    var isForArchive = false
    var isForActionArchive = false
    
    var arr_TacticalProjectListActionsList:[ActionsList]?
    var nextOffset = 0
    var apiLoadingAtActionID:[String] = []

    var filterData = FilterData.init()
    let moreDD = DropDown()

    //MARK:- LifeCycle
    init(theController:TacticalProjectSubList1VC) {
        self.theController = theController
    }
    
    func updateList(list:[ActionsList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_TacticalProjectListActionsList?.removeAll()
        }
        nextOffset = offset
        if arr_TacticalProjectListActionsList == nil {
            arr_TacticalProjectListActionsList = []
        }
        list.forEach({ arr_TacticalProjectListActionsList?.append($0) })
        print(arr_TacticalProjectListActionsList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:ActionsList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]
        if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
            dataSource.remove(at: index) // edit
            dataSourceIcon.remove(at: index)
        }
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdBy) {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
}
