//
//  TacticalProjectDetailTVCell.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SkeletonView
import Loady

class TacticalProjectDetailTVCell: UITableViewCell {

    //Cell1
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    //Cell2
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblTypeDetail: UILabel!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var viewLevel: UIView!
    
    //Cell3
    @IBOutlet weak var lblDescription: UILabel!
    
    //Cell4
    @IBOutlet weak var txtAddNote: UITextField!
    @IBOutlet weak var btnPost: LoadyButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewTxtAddNote: UIView!
//    @IBOutlet weak var constrainViewTxtAddNoteHeight: NSLayoutConstraint! // 42
    
    //Cell5
    @IBOutlet weak var lblProjectCost: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblRevisedDueDate: UILabel!
    
    
    var handlerOnBtnCloseClick:() -> Void = {}
    var handlerText:(String) -> Void = {_ in}
    let disposeBag = DisposeBag()
    var handlerOnBtnAddPostClick:() -> Void = {}
    var handlerOnBtnSaveClick:() -> Void = { }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell1(strTitle:String,strCategoryName:String,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation{
             [lblTitle,lblCategoryName].forEach({ $0?.showAnimatedSkeleton() })
        }
        else{
            [lblTitle,lblCategoryName].forEach({ $0?.hideSkeleton() })
        }
        lblTitle.text = strTitle
        lblCategoryName.text = strCategoryName
    }
    
    /*func configureCell2(strTypeName:String,strTypeDetail:String,isForLevel:Bool = false,isForNotes:Bool = false,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation {
            lblTypeName.text = ""
            lblTypeDetail.text = ""
            [lblTypeName,lblTypeDetail].forEach({ $0?.showAnimatedSkeleton() })
        }
        else{
            
        }
        
        viewLevel.isHidden = !isForLevel
        if isForLevel {
            lblTypeName.text = strTypeName
        } else if isForNotes {
            lblTypeName.text = strTypeName
        } else {
            lblTypeName.text = strTypeName
            lblTypeDetail.text = strTypeDetail
        }
    }*/
    
    func configureCell2(strTypeName:String,strTypeDetail:String,isForLevel:Bool = false,isForNotes:Bool = false,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation {
            lblTypeName.text = strTypeName
            lblTypeDetail.text = " "
            viewLevel.isHidden = !isForLevel
            [lblTypeDetail].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            lblTypeName.text = strTypeName
            lblTypeDetail.text = ""
            viewLevel.isHidden = !isForLevel
            if isForLevel {
                lblTypeName.text = strTypeName
            } else if isForNotes {
                lblTypeName.text = strTypeName
            } else {
                lblTypeName.text = strTypeName
                lblTypeDetail.text = strTypeDetail
            }
            [lblTypeDetail].forEach({ $0?.hideSkeleton() })
        }
        
    }
    
    func configureCell3(strDescription:String,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation {
            lblDescription.text = " "
            [lblDescription].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            [lblDescription].forEach({ $0?.hideSkeleton() })
            lblDescription.text = strDescription
        }
    }
    
    func configureCell4(isLoading:Bool = false) {
        rxTextFieldDidChange()
        if isLoading {
            btnPost.startLoadyIndicator()
            self.contentView.allButtons(isEnable: false)
        } else {
            btnPost.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }
    }
    
    func configureCell6(isLoading:Bool = false) {
        rxTextFieldDidChange()
        if isLoading {
            btnPost.startLoadyIndicator()
        } else {
            btnPost.stopLoadyIndicator()
        }
    }
    
    func rxTextFieldDidChange() {
        txtAddNote.rx.controlEvent([.editingDidBegin, .editingDidEnd,.editingChanged])
            .asObservable()
            .subscribe(onNext: { _ in
                // print("editing state changed")
                self.handlerText(self.txtAddNote.text ?? "")
            })
            .disposed(by: disposeBag)
    }
    
    func configureCell5(strCost:String,strStartDate:String,strDueDate:String,strReviseDueDate:String,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation{
            [lblProjectCost,lblStartDate,lblRevisedDueDate,lblDueDate].forEach({ $0?.showAnimatedSkeleton()
                $0?.text = ""})
        }
        else{
            [lblProjectCost,lblStartDate,lblRevisedDueDate,lblDueDate].forEach({ $0?.hideSkeleton()
            })
            lblProjectCost.text = "\(strCost)"
            
            if strStartDate != ""{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DateFormatterInputType.inputType1
                if let finalDate = dateFormatter.date(from: strStartDate){
                    dateFormatter.dateFormat = DateFormatterOutputType.outputType2
                    let strFinalDate = dateFormatter.string(from: finalDate)
                    lblStartDate.text = strFinalDate
                }
            }else{
                lblStartDate.text = "-"
            }
            
            if strDueDate != ""{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DateFormatterInputType.inputType1
                if let finalDate = dateFormatter.date(from: strDueDate){
                    dateFormatter.dateFormat = DateFormatterOutputType.outputType2
                    let strFinalDate = dateFormatter.string(from: finalDate)
                    lblDueDate.text = strFinalDate
                }
            }else{
                lblDueDate.text = "-"
            }
            
            if strReviseDueDate != ""{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DateFormatterInputType.inputType1
                if let finalDate = dateFormatter.date(from: strReviseDueDate){
                    dateFormatter.dateFormat = DateFormatterOutputType.outputType2
                    let strFinalDate = dateFormatter.string(from: finalDate)
                    lblRevisedDueDate.text = strFinalDate
                }
            }else{
                lblRevisedDueDate.text = "-"
            }
        }
    }
    
    
    //MARK:-IBAction
    @IBAction func onBtnPostAction(_ sender: Any) {
         handlerOnBtnAddPostClick()
    }
    
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        handlerOnBtnSaveClick()
    }
    
    
    @IBAction func onBtnClose(_ sender: Any) {
        handlerOnBtnCloseClick()
    }

}
