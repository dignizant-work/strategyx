//
//  TacticalProjectDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TacticalProjectDetailModel: NSObject {
    //MARK:- Variable
    fileprivate weak var theController:TacticalProjectDetailVC!
    var tableviewCount = 11
    var isForArchive = false
    var theTacticalProjectListModel:TacticalProjectList?
    var arr_noteList:[NotesTacticalProjectList] = []
    var noteCurrentIndex = 0
    var strAddPostText = String()
    let moreDD = DropDown()
    var arr_MoreList:[String] = []
    var isAddPostApiLoading = false
    var isPostEdit = Bool()
    var notesId = String()
    var editNotesTag = Int()
    
    var theTacticalProjectDetailModel:TacticalProjectDetail? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
                    (vc as? TacticalProjectVC)?.updateTacticalProjectListModel(theTacticalProjectDetailModel: theTacticalProjectDetailModel!)
                    (vc as? TacticalProjectArchivedVC)?.updateTacticalProjectListModel(theTacticalProjectDetailModel: theTacticalProjectDetailModel!)
                    (vc as? TacticalProjectSubList1VC)?.updateTacticalProjectListModel(theTacticalProjectDetailModel: theTacticalProjectDetailModel!)
                }
            }
        }
    }
    
    init(theController:TacticalProjectDetailVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: -85, y: 30)
//        dropDown.width = 100
        switch dropDown {
        case moreDD:
            let arr_MoreList:[String] = self.arr_MoreList.map({$0 })
            dropDown.dataSource = arr_MoreList
            break
            
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.update(text: item, dropDown: dropDown,index:index)
                break
           
            default:
                break
            }
            
        }
    }
    
}
