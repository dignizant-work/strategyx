//
//  StrategyActionsModel.swift
//  StrategyX
//
//  Created by Haresh on 04/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class StrategyActionsModel {
    //MARK:-Variable
    fileprivate weak var theController:StrategyActionsVC!
    var strategyID = ""
    var strategyTitle = ""

    var stpe: StrategySubControllerType = .step0
    var theStrategyModel:Any = PrimaryArea()
    var arr_ActionsList:[ActionsList]?
    var nextOffset = 0
    var isForArchive = false
    var apiLoadingAtActionID:[String] = []
    let moreDD = DropDown()
    var theStrategyActionModel:StrategyAction?

    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)
    var isForActionArchive = false
    
    init(theController:StrategyActionsVC) {
        self.theController = theController
    }
    func updateStratgyActionModel(actions:[String]) {
        theStrategyActionModel?.visionText = actions[0]
        theStrategyActionModel?.missionText = actions[1]
        theStrategyActionModel?.valuesText = actions[2]
        theStrategyActionModel?.cultureText = actions[3]
        theStrategyActionModel?.coreBusinessText = actions[4]
        if actions.indices.contains(5) {
            let json = JSON(parseJSON: actions[5])
            print("json:=",json)
            if json.count > 0 {
                theStrategyActionModel?.coreCompetencText.removeAll()
                for item in json.arrayValue {
                    theStrategyActionModel?.coreCompetencText.append(item.stringValue)
                }
            }
        }
        
        theStrategyActionModel?.strengthsText = actions[6]
        theStrategyActionModel?.weaknessesText = actions[7]
        theStrategyActionModel?.opportunitiesText = actions[8]
        theStrategyActionModel?.threatsText = actions[9]

        theController.updateHeaderList()
        theController.handlerUpdateStrategyActionModel(theStrategyActionModel)
        theController.updateEditStrategyDetailView()
    }
    func updateList(list:[ActionsList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_ActionsList?.removeAll()
        }
        nextOffset = offset
        if arr_ActionsList == nil {
            arr_ActionsList = []
        }
        list.forEach({ arr_ActionsList?.append($0) })
        print(arr_ActionsList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:ActionsList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]
        if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
            dataSource.remove(at: index) // edit
            dataSourceIcon.remove(at: index)
        }
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdBy) {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        /*
        if Utility.shared.userData.id != theModel.createdBy {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        if isForArchive {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }*/
        
        
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }

}
//MARK:- Api Call
extension StrategyActionsModel {
    func strategyActionService(isView:Bool = false,strStrategyActions:[String]) {
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_company_id:Utility.shared.userData.companyId]
        if isView {
            parameter[APIKey.key_is_view] = "yes"
        } else {
            parameter[APIKey.key_vision_text] = strStrategyActions[0]
            parameter[APIKey.key_mission_text] = strStrategyActions[1]
            parameter[APIKey.key_values_text] = strStrategyActions[2]
            parameter[APIKey.key_culture_text] = strStrategyActions[3]
            parameter[APIKey.key_core_business_text] = strStrategyActions[4]
            parameter[APIKey.key_core_competenc_text] = strStrategyActions[5]
            parameter[APIKey.key_strengths_text] = strStrategyActions[6]
            parameter[APIKey.key_weaknesses_text] = strStrategyActions[7]
            parameter[APIKey.key_opportunities_text] = strStrategyActions[8]
            parameter[APIKey.key_threats_text] = strStrategyActions[9]
        }
        
        StrategyWebService.shared.getStrategyActions(parameter: parameter, isEdit: !isView, success: { [weak self] (theModel) in
            if isView {
                self?.theStrategyActionModel = theModel
                self?.theController.updateHeaderList()
            } else {
                self?.updateStratgyActionModel(actions: strStrategyActions)
            }
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
}
