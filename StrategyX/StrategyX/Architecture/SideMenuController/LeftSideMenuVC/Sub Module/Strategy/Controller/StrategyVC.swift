//
//  StrategyVC.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class StrategyVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:StrategyView = { [unowned self] in
       return self.view as! StrategyView
    }()
    
    fileprivate lazy var theCurrentModel:StrategyModel = {
       return StrategyModel(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setTheDelegateOfCollectionView(theDelegate: self)
        theCurrentModel.strategyActionService(isView: true, strStrategyActions: [])
        theCurrentModel.strategyAssignUserListService()
        let model = SubListSelectedDataModel.init(title: "Primary Result", strSelectedPrimaryID: "", strSelectedSecondaryID: "")
        theCurrentModel.selectedSubListData.append(model)
        displayContentController(content: subListViewController(step: .step0, title: "Primary Result", theModel: model))
    }
    func updateHeaderList() {
        theCurrentView.collectionView.reloadData()
    }
    func updateNotes(index:Int, description:String, coreCompetence:[String], isUpdateCoreCompetence:Bool) {
        let coreCompetenceJsonString = JSON(coreCompetence).rawString() ?? ""
        let strengthsText = theCurrentModel.theStrategyActionModel?.strengthsText ?? ""
        let weaknessesText = theCurrentModel.theStrategyActionModel?.weaknessesText ?? ""
        let opportunitiesText = theCurrentModel.theStrategyActionModel?.opportunitiesText ?? ""
        let threatsText = theCurrentModel.theStrategyActionModel?.threatsText ?? ""
        
        var notes = [theCurrentModel.theStrategyActionModel?.visionText ?? "",
                     theCurrentModel.theStrategyActionModel?.missionText ?? "",
                     theCurrentModel.theStrategyActionModel?.valuesText ?? "",
                     theCurrentModel.theStrategyActionModel?.cultureText ?? "",
                     theCurrentModel.theStrategyActionModel?.coreBusinessText ?? "",
                     coreCompetenceJsonString,
                     strengthsText,
                     weaknessesText,
                     opportunitiesText,
                     threatsText]
        
        if !isUpdateCoreCompetence {
            notes[index] = description
        }
        theCurrentModel.strategyActionService(isView: false, strStrategyActions: notes)

//        theCurrentView.collectionView.reloadItems(at: [IndexPath.init(row: index, section: 0)])
    }
    func setTheDataInSubListViewController() {
        if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.currentIndexOfSubViewController) {
//            let vc = theCurrentModel.subViewControllers[theCurrentModel.currentIndexOfSubViewController].
        }
    }
    func updateEditStrategyDetailView() {
        if let viewEditStrategyDetail = self.view.viewWithTag(2134565) as? EditStrategyDetailView {
            viewEditStrategyDetail.animateAddButton(isAnimate: false)
            removeViewFromSuperView(view: viewEditStrategyDetail)
        }
    }
    
    func strategyActionReloadAllSection() {
        /*var primaryAreaData:(String,String) = ("", "")
        var secondaryAreaData:(String,String) = ("", "")
        
        if theCurrentModel.selectedSubListData.indices.contains(1) {
            primaryAreaData.1 = theCurrentModel.selectedSubListData[1].title
            primaryAreaData.0 = theCurrentModel.selectedSubListData[1].strSelectedPrimaryID
        }
        
        if theCurrentModel.selectedSubListData.indices.contains(2) {
            secondaryAreaData.1 = theCurrentModel.selectedSubListData[2].title
            secondaryAreaData.0 = theCurrentModel.selectedSubListData[2].strSelectedSecondaryID
        }*/
        
        
        if theCurrentModel.subViewControllers.indices.contains(0) { // primary area
            theCurrentModel.subViewControllers[0].refreshPrimaryArea(strAssignedToUserID:theCurrentModel.strSelectedUserAssignID)
        }
        if theCurrentModel.subViewControllers.indices.contains(1) { // secondary area
            theCurrentModel.subViewControllers[1].refreshSecondaryArea(strAssignedToUserID:theCurrentModel.strSelectedUserAssignID)
        }
        if theCurrentModel.subViewControllers.indices.contains(2) { // strategy goal
            theCurrentModel.subViewControllers[2].refreshStrategyGoal(strAssignedToUserID:theCurrentModel.strSelectedUserAssignID)
        }
        
    }
    
    func updateTotalActionInAllStrategy(isForAdd:Bool,theStrategyGoalModel:StrategyGoalList?, lastCompleteActionCount:Int = 0,lastActionCount:Int = 0) {
        var primaryAreaData:(String,String) = ("", "")
        var secondaryAreaData:(String,String) = ("", "")
        
        if theCurrentModel.selectedSubListData.indices.contains(1) {
            primaryAreaData.1 = theCurrentModel.selectedSubListData[1].title
            primaryAreaData.0 = theCurrentModel.selectedSubListData[1].strSelectedPrimaryID
        }
        
        if theCurrentModel.selectedSubListData.indices.contains(2) {
            secondaryAreaData.1 = theCurrentModel.selectedSubListData[2].title
            secondaryAreaData.0 = theCurrentModel.selectedSubListData[2].strSelectedSecondaryID
        }
        
        if theCurrentModel.subViewControllers.indices.contains(0) { // primary area
            if let index = theCurrentModel.subViewControllers[0].arr_PrimaryResult?.firstIndex(where: { $0.id ==  primaryAreaData.0}) {
                let primaryModel = theCurrentModel.subViewControllers[0].arr_PrimaryResult![index]
                if isForAdd {
                    primaryModel.actionCount = primaryModel.actionCount + 1
                    primaryModel.archiveGoalFlag = 0
                    if primaryModel.actionCount == primaryModel.completeActionCount && (primaryModel.actionCount != 0 &&  primaryModel.completeActionCount != 0) {
                        primaryModel.completeActionFlag = 1
                    } else {
                        primaryModel.completeActionFlag = 0
                    }
                } else {
                    primaryModel.archiveGoalFlag = 0
                    primaryModel.actionCount = primaryModel.actionCount - lastActionCount
                    primaryModel.completeActionCount = primaryModel.completeActionCount - lastCompleteActionCount
                    if primaryModel.actionCount == primaryModel.completeActionCount && (primaryModel.actionCount != 0 &&  primaryModel.completeActionCount != 0) {
                        primaryModel.completeActionFlag = 1
                    } else {
                        primaryModel.completeActionFlag = 0
                    }
                }
                theCurrentModel.subViewControllers[0].updatePrimaryAreaData(theModel: primaryModel)
            }
        }
        
        if theCurrentModel.subViewControllers.indices.contains(1) { // secondary area
            if let index = theCurrentModel.subViewControllers[1].arr_SecondaryResult?.firstIndex(where: { $0.id ==  secondaryAreaData.0}) {
                let secondaryModel = theCurrentModel.subViewControllers[1].arr_SecondaryResult![index]
                if isForAdd {
                    secondaryModel.archiveGoalFlag = 0
                    secondaryModel.actionCount = secondaryModel.actionCount + 1
                    if secondaryModel.actionCount == secondaryModel.completeActionCount && (secondaryModel.actionCount != 0 &&  secondaryModel.completeActionCount != 0) {
                        secondaryModel.completeActionFlag = 1
                    } else {
                        secondaryModel.completeActionFlag = 0
                    }
                } else {
                    secondaryModel.actionCount = secondaryModel.actionCount - lastActionCount
                    secondaryModel.completeActionCount = secondaryModel.completeActionCount - lastCompleteActionCount
                    if secondaryModel.actionCount == secondaryModel.completeActionCount && (secondaryModel.actionCount != 0 &&  secondaryModel.completeActionCount != 0) {
                        secondaryModel.completeActionFlag = 1
                    } else {
                        secondaryModel.completeActionFlag = 0
                    }
                }
                theCurrentModel.subViewControllers[1].updateSecondaryAreaData(theModel: secondaryModel)
            }
        }
        
        if theCurrentModel.subViewControllers.indices.contains(2), let theModel = theStrategyGoalModel { // Strategy Goal
            if let index = theCurrentModel.subViewControllers[2].arr_StrategyGoalResult?.firstIndex(where: { $0.id ==  theModel.id}) {
                let goalModel = theCurrentModel.subViewControllers[2].arr_StrategyGoalResult![index]
                if isForAdd {
                    theCurrentModel.subViewControllers[2].refreshStrategyGoal(strAssignedToUserID:theCurrentModel.strSelectedUserAssignID)

                    /*
                    goalModel.archiveGoalFlag = 0
                    goalModel.actionCount = goalModel.actionCount + 1
                    if goalModel.actionCount == goalModel.completeActionCount && (goalModel.actionCount != 0 &&  goalModel.completeActionCount != 0) {
                        goalModel.completeActionFlag = 1
                    } else {
                        goalModel.completeActionFlag = 0
                    }
                    theCurrentModel.subViewControllers[2].updateStrategyGoalData(theModel: goalModel)
                    */
                } else {
                    goalModel.actionCount = goalModel.actionCount - lastActionCount
                    goalModel.completeActionCount = goalModel.completeActionCount - lastCompleteActionCount
                    if goalModel.actionCount == goalModel.completeActionCount && (goalModel.actionCount != 0 &&  goalModel.completeActionCount != 0) {
                        goalModel.completeActionFlag = 1
                    } else {
                        goalModel.completeActionFlag = 0
                    }
                }
            }
        }
        
    }
    
    func updateStrategyListModelFromActionSubList(theModel:Any, oldCompletedActionCount:Int, isDeleteItem:Bool = false) {
        var primaryAreaData:(String,String) = ("", "")
        var secondaryAreaData:(String,String) = ("", "")
//        var isAdd = false
//        var isDelete = false
        var totalCount = -1
        var completedCount = -1
        
        if theCurrentModel.selectedSubListData.indices.contains(1) {
            primaryAreaData.1 = theCurrentModel.selectedSubListData[1].title
            primaryAreaData.0 = theCurrentModel.selectedSubListData[1].strSelectedPrimaryID
        }
        
        if theCurrentModel.selectedSubListData.indices.contains(2) {
            secondaryAreaData.1 = theCurrentModel.selectedSubListData[2].title
            secondaryAreaData.0 = theCurrentModel.selectedSubListData[2].strSelectedSecondaryID
        }
        
        if theCurrentModel.subViewControllers.indices.contains(2), let theGoalModel = theModel as? StrategyGoalList  { // Strategy Goal
            if let index = theCurrentModel.subViewControllers[2].arr_StrategyGoalResult?.firstIndex(where: { $0.id ==  theGoalModel.id}) {
                let goalModel = theCurrentModel.subViewControllers[2].arr_StrategyGoalResult![index]
//                if theGoalModel.actionCount > goalModel.actionCount {
//                    isAdd = true
//                } else {
//                    isDelete = true
//                }
                totalCount = theGoalModel.actionCount
                completedCount = theGoalModel.completeActionCount
                
                if totalCount == completedCount && (totalCount != 0 &&  completedCount != 0) {
                    goalModel.completeActionFlag = 1
                } else {
                    goalModel.completeActionFlag = 0
                }
                theCurrentModel.subViewControllers[2].updateStrategyGoalData(theModel: theGoalModel)
            }
        }
        
        if theCurrentModel.subViewControllers.indices.contains(1) { // secondary area
            if let theSecondary = theModel as? SecondaryArea {
                secondaryAreaData.0 = theSecondary.id
            }
            if let index = theCurrentModel.subViewControllers[1].arr_SecondaryResult?.firstIndex(where: { $0.id ==  secondaryAreaData.0}) {
                let secondaryModel = theCurrentModel.subViewControllers[1].arr_SecondaryResult![index]
                if let theSecondary = theModel as? SecondaryArea {
                    totalCount = theSecondary.actionCount
                    completedCount = theSecondary.completeActionCount
                    secondaryModel.actionCount = totalCount
                    secondaryModel.completeActionCount = completedCount
                } else {
                    secondaryModel.actionCount = totalCount
                    secondaryModel.completeActionCount = completedCount
                }
                /*
                if !isAdd && !isDelete {
                    
                    if secondaryModel.completeActionCount > oldCompletedActionCount {
                        isAdd = true
                    } else {
                        isDelete = true
                    }
                } else {
                    
                    if isAdd && !isDelete {
                        secondaryModel.completeActionCount += 1
                    } else if !isAdd && isDelete && !isDeleteItem {
                        secondaryModel.completeActionCount -= 1
                    }
                }
                if isDeleteItem {
                    secondaryModel.actionCount -= 1
                    secondaryModel.actionCount = secondaryModel.actionCount < 0 ? 0 : secondaryModel.actionCount
                }*/
                if secondaryModel.completeActionCount == -1 {
                    secondaryModel.completeActionCount = 0
                }
                if secondaryModel.actionCount == secondaryModel.completeActionCount && (secondaryModel.actionCount != 0 &&  secondaryModel.completeActionCount != 0) {
                    secondaryModel.completeActionFlag = 1
                } else {
                    secondaryModel.completeActionFlag = 0
                }
                theCurrentModel.subViewControllers[1].updateSecondaryAreaData(theModel: secondaryModel)
            }
        }
        
        if theCurrentModel.subViewControllers.indices.contains(0) { // primary area
            if let thePrimary = theModel as? PrimaryArea {
                primaryAreaData.0 = thePrimary.id
            }
            if let index = theCurrentModel.subViewControllers[0].arr_PrimaryResult?.firstIndex(where: { $0.id ==  primaryAreaData.0}) {
                let primaryModel = theCurrentModel.subViewControllers[0].arr_PrimaryResult![index]
                if let thePrimary = theModel as? PrimaryArea {
                    totalCount = thePrimary.actionCount
                    completedCount = thePrimary.completeActionCount
                    primaryModel.actionCount = totalCount
                    primaryModel.completeActionCount = completedCount
                } else {
                    primaryModel.actionCount = totalCount
                    primaryModel.completeActionCount = completedCount
                }
                /*
                if !isAdd && !isDelete {
                    totalCount = thePrimary.actionCount
                    completedCount = thePrimary.completeActionCount

                    if primaryModel.completeActionCount > oldCompletedActionCount {
                        isAdd = true
                    } else {
                        isDelete = true
                    }
                } else {
                    if isAdd && !isDelete {
                        primaryModel.completeActionCount += 1
                    } else if !isAdd && isDelete && !isDeleteItem {
                        primaryModel.completeActionCount -= 1
                    }
                }
                if isDeleteItem {
                    primaryModel.actionCount -= 1
                    primaryModel.actionCount = primaryModel.actionCount < 0 ? 0 : primaryModel.actionCount
                }*/
                if primaryModel.completeActionCount == -1 {
                    primaryModel.completeActionCount = 0
                }
                if primaryModel.actionCount == primaryModel.completeActionCount && (primaryModel.actionCount != 0 &&  primaryModel.completeActionCount != 0) {
                    primaryModel.completeActionFlag = 1
                } else {
                    primaryModel.completeActionFlag = 0
                }
                theCurrentModel.subViewControllers[0].updatePrimaryAreaData(theModel: primaryModel)
            }
        }
    }
    
    func updateStrategyListModelFromEditScreen(theModel:Any) {
        if theCurrentModel.subViewControllers.indices.contains(2), let theGoalModel = theModel as? StrategyGoalList  { // Strategy Goal
            theCurrentModel.subViewControllers[2].updateStrategyGoalData(theModel: theGoalModel)
        }
        
        if theCurrentModel.subViewControllers.indices.contains(1), let theSecondaryModel = theModel as? SecondaryArea { // secondary area
            theCurrentModel.subViewControllers[1].updateSecondaryAreaData(theModel: theSecondaryModel)
        }
        
        if theCurrentModel.subViewControllers.indices.contains(0), let thePrimaryModel = theModel as? PrimaryArea { // primary area
            theCurrentModel.subViewControllers[0].updatePrimaryAreaData(theModel: thePrimaryModel)
        }
    }
    
    func updateDropDownData(categorySelection:AddFocusViewModel.categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].assigneeId
            theCurrentView.lblFilterText.text = str
            if theCurrentModel.subViewControllers.indices.contains(0) { // primary area
                theCurrentModel.subViewControllers[0].refreshPrimaryArea(strAssignedToUserID:theCurrentModel.strSelectedUserAssignID)
            }
            break
        case .department:
            break
        }
    }
    
    //MARK:- Redirection
    func presentDropDownListVC(categorySelection:AddFocusViewModel.categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            break
        case .assignTo:
            let arr_assignTo:[String] = theCurrentModel.arr_assignList.map({$0.assignedTo})
            //          let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func presentGraphAction(strChartId:String = "", chartFlagViewType:ChartFlagView = ChartFlagView.strategy) {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.btnGraph.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)

        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 10.0, bottom: tabbarheight, chartFlagViewType: chartFlagViewType)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func removeViewFromSuperView(view:UIView)  {
        (view as? EditStrategyDetailView)?.animateAddButton(isAnimate: false)
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 0
        }, completion: { (finished) in
            view.removeFromSuperview()
        })
    }
    func showEditStrategyDetailView(title:String,description:String,defaultText:String,index:Int) {
        let view = EditStrategyDetailView.loadFromXib() as! EditStrategyDetailView
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.setupLayout()
        view.tag = 2134565
        view.setTheData(title: title, description: description, defaultText: defaultText)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        var list:[String] = []
        if let coreCompetence = theCurrentModel.theStrategyActionModel?.coreCompetencText {
            list = Array(coreCompetence)
        }
        
        view.handlerOnTextUpdate = { [weak self] (text) in
            self?.updateNotes(index: index, description: text, coreCompetence: list, isUpdateCoreCompetence: false)
//            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    
    func showEditCoreCompetenciesView(title:String,arrPoints:[String], index:Int) {
        let view = EditCoreCompetenciesView.loadFromXib() as! EditCoreCompetenciesView
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.setupLayout()
        view.tag = 213489
        view.setTheData(title: title, arrPoints: arrPoints)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        view.handlerOnTextUpdate = { [weak self] (item) in
            self?.updateNotes(index: index, description: "", coreCompetence: item, isUpdateCoreCompetence: true)
            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    
    func showSeeMoreStrategyDetailView(title:String,description:String, index:Int) {
        let view = SeeMoreStrategyDetailView.loadFromXib() as! SeeMoreStrategyDetailView
        let height = self.view.frame.size.height
        
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height)
        view.setupLayout()
        view.setTheData(title: title, description: description)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    func subListViewController(step:StrategySubControllerType,title:String, theModel:SubListSelectedDataModel) -> SubListViewController {
        let vc = SubListViewController.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(type: step, selectedData: theModel)
        vc.delegate = self
        theCurrentModel.subViewControllers.append(vc)
        return vc
    }
    func redirectToAddPrimaryAreaScreen(theModel:PrimaryArea?,theOldModel:PrimaryArea) {
        let model = theModel
        model?.notesCount = 0
        model?.actionCount = theOldModel.actionCount
        model?.completeActionFlag = theOldModel.completeActionFlag
        model?.completeActionCount = theOldModel.completeActionCount
        
        theCurrentModel.subViewControllers[theCurrentModel.currentIndexOfSubViewController].updatePrimaryAreaData(theModel: model!)
        let vc = AddPrimaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerEditData = { [weak self] (theModelPrimary) in
//            let model = theModel
//            model.title = strTitle
//            model.descriptionArea = strDiscription
            if let obj = self {
                theModelPrimary.notesCount = 0
                theModelPrimary.completeActionCount = theOldModel.completeActionCount
                theModelPrimary.completeActionFlag = theOldModel.completeActionFlag
                theModelPrimary.actionCount = theOldModel.actionCount
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updatePrimaryAreaData(theModel: theModelPrimary)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddSecondaryAreaScreen(theModel:SecondaryArea? = nil,theOldModel:SecondaryArea? = nil,thePrimaryModel:PrimaryArea? = nil,isForAddFromStrategyList:Bool = false) {
        let vc = AddSecondaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        if isForAddFromStrategyList {
            vc.setTheAddData(theModel: thePrimaryModel, isForAddFromStrategyList: isForAddFromStrategyList)
        } else {
            if let model = theModel {
                model.notesCount = 0
                model.actionCount = theOldModel?.actionCount ?? 0
                model.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                model.completeActionCount = theOldModel?.completeActionCount ?? 0
                theCurrentModel.subViewControllers[theCurrentModel.currentIndexOfSubViewController].updateSecondaryAreaData(theModel: model)
            }
            
            vc.setTheEditData(theModel: theModel, isForEdit: true)
        }
        vc.handlerEditData = { [weak self] (theModelSecondary) in
//            let model = theModel!
//            model.title = strTitle
//            model.descriptionArea = strDiscription

            if let obj = self {
                theModelSecondary.notesCount = 0
                theModelSecondary.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelSecondary.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelSecondary.actionCount = theOldModel?.actionCount ?? 0
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updateSecondaryAreaData(theModel: theModelSecondary)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddGoalScreen(theModel:StrategyGoalList?,theOldModel:StrategyGoalList? = nil,theSecondaryyModel:SecondaryArea? = nil,isForAddFromStrategyList:Bool = false) {
        let vc = AddGoalVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        if isForAddFromStrategyList {
            let secondaryModel = theSecondaryyModel
            if theCurrentModel.selectedSubListData.indices.contains(1) {
                secondaryModel?.descriptionArea = theCurrentModel.selectedSubListData[1].title
            }
            vc.setTheAddData(theModel: theSecondaryyModel, isForAddFromStrategyList: isForAddFromStrategyList)
        } else {
            if let model = theModel {
                model.notesCount = 0
                model.actionCount = theOldModel?.actionCount ?? 0
                model.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                model.completeActionCount = theOldModel?.completeActionCount ?? 0
                theCurrentModel.subViewControllers[theCurrentModel.currentIndexOfSubViewController].updateStrategyGoalData(theModel: model)
            }
            vc.setTheEditData(theModel: theModel, isForEdit: true)
        }
        vc.handlerEditData = { [weak self] (theModelGoal) in
//            let model = theModel!
//            model.title = strTitle
//            model.descriptionArea = strDiscription

            if let obj = self {
                theModelGoal.notesCount = 0
                theModelGoal.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelGoal.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelGoal.actionCount = theOldModel?.actionCount ?? 0

                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updateStrategyGoalData(theModel: theModelGoal)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToStrategyActionScreen(strStrategyID:String,strategyTitle:String,stpe: StrategySubControllerType, theModel:Any) {
        let vc = StrategyActionsVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(strStrategyID: strStrategyID, strategyTitle: strategyTitle, step: stpe, theModel: theModel)
        vc.setTheStrategyActionModel(theModel: theCurrentModel.theStrategyActionModel)
        
        var oldCompleteActionCount = 0
        if let goal = theModel as? StrategyGoalList {
            oldCompleteActionCount = goal.completeActionCount
            //            lastActionCount = goal.actionCount
        } else if let secondary = theModel as? SecondaryArea {
            oldCompleteActionCount = secondary.completeActionCount
            //            lastActionCount = secondary.actionCount
        } else if let primary = theModel as? PrimaryArea {
            oldCompleteActionCount = primary.completeActionCount
            //            lastActionCount = primary.actionCount
        }
        vc.handlerUpdateStrategyActionModel = { [weak self] (theStrategyActionModel) in
            self?.theCurrentModel.theStrategyActionModel = theStrategyActionModel
            self?.updateHeaderList()
        }
        vc.handlerUpdateModel = { [weak self] (theStrategyModel, isDeleted) in
            if isDeleted {
                self?.strategyActionReloadAllSection()
            } else {
//                self?.updateStrategyListModelFromActionSubList(theModel: theStrategyModel, oldCompletedActionCount: oldCompleteActionCount, isDeleteItem: isDeleted)
            }
        }
        vc.handlerUpdateEditStrategy = { [weak self] (theStrategyModel) in
            self?.updateStrategyListModelFromEditScreen(theModel: theStrategyModel)
        }
        self.push(vc: vc)
    }
    func redirectToAddActionScreen(theModel:StrategyGoalList) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        print("theCurrentModel.selectedSubListData:=",theCurrentModel.selectedSubListData)

        var primaryAreaData:(String,String) = ("", "")
        var secondaryAreaData:(String,String) = ("", "")
        var isContainPrimaryAndSecondaryData = (false,false)
        
        if theCurrentModel.selectedSubListData.indices.contains(1) {
            primaryAreaData.1 = theCurrentModel.selectedSubListData[1].title
            primaryAreaData.0 = theCurrentModel.selectedSubListData[1].strSelectedPrimaryID
            isContainPrimaryAndSecondaryData.0 = true
        }
        
        if theCurrentModel.selectedSubListData.indices.contains(2) {
            secondaryAreaData.1 = theCurrentModel.selectedSubListData[2].title
            secondaryAreaData.0 = theCurrentModel.selectedSubListData[2].strSelectedSecondaryID
            isContainPrimaryAndSecondaryData.1 = true
        }
        
        if !isContainPrimaryAndSecondaryData.0 || !isContainPrimaryAndSecondaryData.1 {
            return
        }
        print("primaryAreaData:=",primaryAreaData)
        print("secondaryAreaData:=",secondaryAreaData)
        vc.setAddStrategyData(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId: theModel.id, theGoalName: theModel.title, theDueDate: theModel.duedate, theDueDateColor: theModel.duedateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel.status))
        
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            let model = theModel
            model.duedate = strDueDate
            model.duedateColor = strDueDateColor
            self?.updateStrategyListModelFromEditScreen(theModel: model)
        }
        
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theModel
            model.completeActionFlag = 0
//            model.actionCount = model.actionCount + 1
            self?.updateTotalActionInAllStrategy(isForAdd: true, theStrategyGoalModel: theModel, lastCompleteActionCount: model.completeActionCount, lastActionCount: model.actionCount)
            /*
            if let obj = self {
                if obj.theCurrentModel.subViewControllers.indices.contains(0) {
                    if let index = obj.theCurrentModel.subViewControllers[0].arr_PrimaryResult?.firstIndex(where: { $0.id ==  primaryAreaData.0}) {
                        let primaryModel = obj.theCurrentModel.subViewControllers[0].arr_PrimaryResult![index]
                        primaryModel.actionCount = primaryModel.actionCount + 1
                        obj.theCurrentModel.subViewControllers[0].updatePrimaryAreaData(theModel: primaryModel)
                    }
                }
                if obj.theCurrentModel.subViewControllers.indices.contains(1) {
                    if let index = obj.theCurrentModel.subViewControllers[1].arr_SecondaryResult?.firstIndex(where: { $0.id ==  secondaryAreaData.0}) {
                        let secondaryModel = obj.theCurrentModel.subViewControllers[1].arr_SecondaryResult![index]
                        secondaryModel.actionCount = secondaryModel.actionCount + 1
                        obj.theCurrentModel.subViewControllers[1].updateSecondaryAreaData(theModel: secondaryModel)
                    }
                }
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updateStrategyGoalData(theModel: model)
            }*/
        }
        self.push(vc: vc)
    }
    
    
    func redirectToAddCriticalSucessFactorVC(thePrimaryAreaId: String, thePrimaryAreaName: String, theSecondaryAreaId: String, theSecondaryAreaName: String, theGoalId: String, theGoalName: String, theDueDate: String, theDueDateColor: String,theModel:StrategyGoalList?) {
        
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setAddStrategyData(thePrimaryAreaId: thePrimaryAreaId, thePrimaryAreaName: thePrimaryAreaName, theSecondaryAreaId: theSecondaryAreaId, theSecondaryAreaName: theSecondaryAreaName, theGoalId: theGoalId, theGoalName: theGoalName, theDueDate: theDueDate, theDueDateColor: theDueDateColor)
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let model = theModel {
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.updateStrategyListModelFromEditScreen(theModel: model)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToAddRiskVC(thePrimaryAreaId: String, thePrimaryAreaName: String, theSecondaryAreaId: String, theSecondaryAreaName: String, theGoalId: String, theGoalName: String, theDueDate: String, theDueDateColor: String, theModel:StrategyGoalList?) {
        
        let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddStrategyData(thePrimaryAreaId: thePrimaryAreaId, thePrimaryAreaName: thePrimaryAreaName, theSecondaryAreaId: theSecondaryAreaId, theSecondaryAreaName: theSecondaryAreaName, theGoalId: theGoalId, theGoalName: theGoalName, theDueDate: theDueDate, theDueDateColor: theDueDateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel?.status ?? ""))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let model = theModel {
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.updateStrategyListModelFromEditScreen(theModel: model)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToAddTacticalProjectVC(thePrimaryAreaId: String, thePrimaryAreaName: String, theSecondaryAreaId: String, theSecondaryAreaName: String, theGoalId: String, theGoalName: String, theDueDate: String, theDueDateColor: String, theModel:StrategyGoalList?) {
        
        let vc = AddTacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setAddStrategyData(thePrimaryAreaId: thePrimaryAreaId, thePrimaryAreaName: thePrimaryAreaName, theSecondaryAreaId: theSecondaryAreaId, theSecondaryAreaName: theSecondaryAreaName, theGoalId: theGoalId, theGoalName: theGoalName, theDueDate: theDueDate, theDueDateColor: theDueDateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel?.status ?? ""))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let model = theModel {
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.updateStrategyListModelFromEditScreen(theModel: model)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToAddFocusVC(thePrimaryAreaId: String, thePrimaryAreaName: String, theSecondaryAreaId: String, theSecondaryAreaName: String, theGoalId: String, theGoalName: String, theDueDate: String, theDueDateColor: String, theModel:StrategyGoalList?) {
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setAddStrategyData(thePrimaryAreaId: thePrimaryAreaId, thePrimaryAreaName: thePrimaryAreaName, theSecondaryAreaId: theSecondaryAreaId, theSecondaryAreaName: theSecondaryAreaName, theGoalId: theGoalId, theGoalName: theGoalName, theDueDate: theDueDate, theDueDateColor: theDueDateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel?.status ?? ""))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let model = theModel {
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.updateStrategyListModelFromEditScreen(theModel: model)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToAddIdeaAndProblemVC(thePrimaryAreaId: String, thePrimaryAreaName: String, theSecondaryAreaId: String, theSecondaryAreaName: String, theGoalId: String, theGoalName: String, theDueDate: String, theDueDateColor: String, theModel:StrategyGoalList?, type:IdeaAndProblemType) {
        let vc = AddIdeaNProblemVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG, type: type)
        vc.setAddStrategyData(thePrimaryAreaId: thePrimaryAreaId, thePrimaryAreaName: thePrimaryAreaName, theSecondaryAreaId: theSecondaryAreaId, theSecondaryAreaName: theSecondaryAreaName, theGoalId: theGoalId, theGoalName: theGoalName, theDueDate: theDueDate, theDueDateColor: theDueDateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel?.status ?? ""))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let model = theModel {
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.updateStrategyListModelFromEditScreen(theModel: model)
            }
        }
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        var title = ""
        if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
            title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
        }
 
        if let vc = theCurrentModel.subViewControllers.last {
            removeContentController(content: vc)
            theCurrentView.updateTitleText(strTitle: title)
            theCurrentModel.subViewControllers.removeLast()
            theCurrentModel.selectedSubListData.removeLast()
            theCurrentModel.currentIndexOfSubViewController = theCurrentModel.subViewControllers.count - 1
            if theCurrentModel.subViewControllers.count == 1 {
                theCurrentView.backButtonStrategy(isHidden: true)
                theCurrentModel.strSelectedPrimaryID = ""
                theCurrentModel.strSelectedPrimaryID = ""
                theCurrentView.visibilityoFViewFilter(isHidden: false)
            }
            print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
        }
    }
    
    @IBAction func onBtnStrategyGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentModel.arr_assignList.count == 0 {
            theCurrentModel.strategyAssignUserListService()
            return
        }
        presentDropDownListVC(categorySelection: .assignTo)
    }
    
}
//MARK:- Container View
extension StrategyVC {
    func displayContentController(content: UIViewController) {
        content.view.frame = theCurrentView.containerView.bounds
        theCurrentView.containerView.addSubview(content.view)
        addChild(content)
        content.didMove(toParent: self)
        content.view.layoutIfNeeded()
        
        let size = theCurrentView.containerView.bounds
        content.view.frame = CGRect(x: size.width, y: 0, width: size.width, height: size.height)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            content.view.frame=CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }, completion:nil)
        UIView.animate(withDuration: 0.3) {
            content.view.alpha = 1
        }
    }
    
    func removeContentController(content: UIViewController) {
        let size = theCurrentView.containerView.bounds
        UIView.animate(withDuration: 0.4, animations: {
            content.view.frame = CGRect(x: size.width, y: 0, width: size.width, height: size.height)
        }, completion: { (competed) in
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
            print("content remove")
        })
    }
}
//MARK:- UICollectionViewDataSource
extension StrategyVC:UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StrategyCardCollectionViewCell", for: indexPath) as! StrategyCardCollectionViewCell
        cell.setTheEditAction(id: theCurrentModel.theStrategyActionModel?.id ?? "")
        switch indexPath.row {
        case 0:
            cell.configure(type: .vision, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.visionTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.visionText
                cell.configure(type: .vision, description: theCurrentModel.theStrategyActionModel!.visionText, isSkeltonAnimation: false)
            }
            break
        case 1:
//            cell.configure(type: .mission, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .mission, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.missionTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.missionText
                cell.configure(type: .mission, description: theCurrentModel.theStrategyActionModel!.missionText, isSkeltonAnimation: false)
            }
            break
        case 2:
//            cell.configure(type: .value, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .value, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.valuesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.valuesText
                cell.configure(type: .value, description: theCurrentModel.theStrategyActionModel!.valuesText, isSkeltonAnimation: false)
            }
            break
        case 3:
//            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .culture, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.cultureTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.cultureText
                cell.configure(type: .culture, description: theCurrentModel.theStrategyActionModel!.cultureText, isSkeltonAnimation: false)
            }
            break
        case 4:
            //            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .coreBussiness, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.coreBusinessTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.coreBusinessText
                cell.configure(type: .coreBussiness, description: theCurrentModel.theStrategyActionModel!.coreBusinessText, isSkeltonAnimation: false)
            }
            break
        case 5:
            //            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .coreCompetencies, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault
                cell.coreCompetenices = Array(theCurrentModel.theStrategyActionModel!.coreCompetencText)
                cell.configure(type: .coreCompetencies, description: theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault, isSkeltonAnimation: false)
            }
            break
        case 6:
            cell.configure(type: .strengths, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.strengthsTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.strengthsText
                cell.configure(type: .strengths, description: theCurrentModel.theStrategyActionModel!.strengthsText, isSkeltonAnimation: false)
            }
            break
        case 7:
            cell.configure(type: .weaknesses, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.weaknessesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.weaknessesText
                cell.configure(type: .weaknesses, description: theCurrentModel.theStrategyActionModel!.weaknessesText, isSkeltonAnimation: false)
            }
            break
        case 8:
            cell.configure(type: .opportunities, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.opportunitiesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.opportunitiesText
                cell.configure(type: .opportunities, description: theCurrentModel.theStrategyActionModel!.opportunitiesText, isSkeltonAnimation: false)
            }
            break
        case 9:
            cell.configure(type: .threats, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.threatsTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.threatsText
                cell.configure(type: .threats, description: theCurrentModel.theStrategyActionModel!.threatsText, isSkeltonAnimation: false)
            }
            break
            
        default:
            break
        }
        
        cell.handlerBtnEditAction = { [weak self] in
            if self?.theCurrentModel.theStrategyActionModel != nil {
                if indexPath.row == 5 {
                    if let arr_CoreBusiness = self?.theCurrentModel.theStrategyActionModel!.coreCompetencText {
                        self?.showEditCoreCompetenciesView(title: cell.lblTitle.text ?? "", arrPoints: Array(arr_CoreBusiness), index: indexPath.row)
                    }
                } else {
                    self?.showEditStrategyDetailView(title: cell.lblTitle.text ?? "", description: cell.orignalText, defaultText: cell.defaultText, index: indexPath.row)
                }
            }
        }
        cell.handlerBtnSeeMoreAction = {[weak self] in
            if self?.theCurrentModel.theStrategyActionModel != nil {
                self?.showSeeMoreStrategyDetailView(title: cell.lblTitle.text ?? "", description: cell.lblDescription.text ?? "", index: indexPath.row)
            }
        }
        
        return cell
    }
    
}
//MARK:- UICollectionViewDelegate
extension StrategyVC:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentModel.theStrategyActionModel != nil {
            let cell = collectionView.cellForItem(at: indexPath) as! StrategyCardCollectionViewCell
            var strDescription = cell.orignalText.isEmpty ? cell.defaultText : cell.orignalText
            if indexPath.row == 5 {
                let arr_CoreBusiness = theCurrentModel.theStrategyActionModel!.coreCompetencText
                if Array(arr_CoreBusiness).count != 0 {
                    strDescription = "".createBulletString(arr: Array(arr_CoreBusiness))
                }
            }
            showSeeMoreStrategyDetailView(title: cell.lblTitle.text ?? "", description: strDescription, index: indexPath.row)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == theCurrentView.collectionView {
            let pageWidth = Float(theCurrentView.itemWidth + theCurrentView.itemSpacing)
            let targetXContentOffset = Float(targetContentOffset.pointee.x)
            let contentWidth = Float(theCurrentView.collectionView!.contentSize.width  )
            var newPage = Float(theCurrentView.currentItem)
            
            if velocity.x == 0 {
                newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
            } else {
                newPage = Float(velocity.x > 0 ? theCurrentView.currentItem + 1 : theCurrentView.currentItem - 1)
                if newPage < 0 {
                    newPage = 0
                }
                if (newPage > contentWidth / pageWidth) {
                    newPage = ceil(contentWidth / pageWidth) - 1.0
                }
            }
            
            theCurrentView.currentItem = Int(newPage)
            let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
        }
    }
}
/*
//MARK:- UIScrollViewDelegate
extension StrategyVC:UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == theCurrentView.collectionView {
            let pageWidth = Float(theCurrentView.itemWidth + theCurrentView.itemSpacing)
            let targetXContentOffset = Float(targetContentOffset.pointee.x)
            let contentWidth = Float(theCurrentView.collectionView!.contentSize.width  )
            var newPage = Float(theCurrentView.currentItem)
            
            if velocity.x == 0 {
                newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
            } else {
                newPage = Float(velocity.x > 0 ? theCurrentView.currentItem + 1 : theCurrentView.currentItem - 1)
                if newPage < 0 {
                    newPage = 0
                }
                if (newPage > contentWidth / pageWidth) {
                    newPage = ceil(contentWidth / pageWidth) - 1.0
                }
            }
            
            theCurrentView.currentItem = Int(newPage)
            let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
        }
    }
}
*/

//MARK:- StrategySubControllerDelegate
extension StrategyVC:StrategySubControllerDelegate {
    func moreActionTablviewCell(step: StrategySubControllerType, theModel: Any, at indexRow: Int, itemName: String) {
        var primaryAreaData:(String,String) = ("", "")
        var secondaryAreaData:(String,String) = ("", "")
        
        if theCurrentModel.selectedSubListData.indices.contains(1) {
            primaryAreaData.1 = theCurrentModel.selectedSubListData[1].title
            primaryAreaData.0 = theCurrentModel.selectedSubListData[1].strSelectedPrimaryID
        }
        
        if theCurrentModel.selectedSubListData.indices.contains(2) {
            secondaryAreaData.1 = theCurrentModel.selectedSubListData[2].title
            secondaryAreaData.0 = theCurrentModel.selectedSubListData[2].strSelectedSecondaryID
        }
        
        switch itemName {
        case MoreAction.edit.rawValue:
            switch step {
            case .step0:
                guard let theOldModel = theModel as? PrimaryArea else { return }
                theCurrentModel.getPrimaryAreaDetailWebService(strPrimaryID: theOldModel.id, theOldModel: theOldModel)
//                redirectToAddPrimaryAreaScreen(theModel: theModel as? PrimaryArea)
                return
            case .step1:
                guard let theOldModel = theModel as? SecondaryArea else { return }
                theCurrentModel.getSecondaryAreaDetailWebService(strSecondaryID: theOldModel.id, theOldModel: theOldModel)
//                redirectToAddSecondaryAreaScreen(theModel: theModel as? SecondaryArea)
                return
            case .step2:
                guard let theOldModel = theModel as? StrategyGoalList else { return }
                theCurrentModel.getStrategyGoalDetailWebService(strStrategyGoalID: theOldModel.id, theOldModel: theOldModel)
//                redirectToAddGoalScreen(theModel: theModel as? StrategyGoalList)
                return
            }
        case MoreAction.delete.rawValue:
            var completeActionCount = 0
            var actionCount = 0
            if let model = theModel as? StrategyGoalList {
                completeActionCount = model.completeActionCount
                actionCount = model.actionCount
            } else if let model = theModel as? SecondaryArea {
                completeActionCount = model.completeActionCount
                actionCount = model.actionCount

            } else if let model = theModel as? PrimaryArea {
                completeActionCount = model.completeActionCount
                actionCount = model.actionCount
            }
            strategyActionReloadAllSection()
//            updateTotalActionInAllStrategy(isForAdd: false, theStrategyGoalModel: theModel as? StrategyGoalList, lastCompleteActionCount: completeActionCount, lastActionCount: actionCount)
            break
        case MoreAction.chart.rawValue:
            var chartID = ""
            var chartFlagView = ChartFlagView.strategy
            
            if let model = theModel as? PrimaryArea {
                chartID = model.id
                chartFlagView = .primary
            } else if let model = theModel as? SecondaryArea {
                chartID = model.id
                chartFlagView = .secondary
            } else if let model = theModel as? StrategyGoalList {
                chartID = model.id
                chartFlagView = .strategyGoal
            }
            presentGraphAction(strChartId: chartID, chartFlagViewType: chartFlagView)
            break
        case MoreAction.goal.rawValue:
            redirectToAddGoalScreen(theModel: nil, theSecondaryyModel: theModel as? SecondaryArea, isForAddFromStrategyList: true)
            return
        case MoreAction.secondary.rawValue:
            redirectToAddSecondaryAreaScreen(theModel: nil, thePrimaryModel: theModel as? PrimaryArea, isForAddFromStrategyList: true)
            return
        case MoreAction.action.rawValue:
            if let model = theModel as? StrategyGoalList {
                redirectToAddActionScreen(theModel: model)
            }
            break
        case MoreAction.approve.rawValue:
            strategyActionReloadAllSection()
            break
//           ["Success Factor","Risk",,"Idea","Problem","Focus","Action"]
           
        case "Success Factor":
            redirectToAddCriticalSucessFactorVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList))
            break
        case "Risk":
            redirectToAddRiskVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList))
            break
        case "Tactical Project":
            redirectToAddTacticalProjectVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList))
            break
        case "Focus":
            redirectToAddFocusVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList))
            break
        case "Idea":
            redirectToAddIdeaAndProblemVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList), type: .idea)
            break
        case "Problem":
            redirectToAddIdeaAndProblemVC(thePrimaryAreaId: primaryAreaData.0, thePrimaryAreaName: primaryAreaData.1, theSecondaryAreaId: secondaryAreaData.0, theSecondaryAreaName: secondaryAreaData.1, theGoalId:(theModel as? StrategyGoalList)?.id ?? "" , theGoalName: (theModel as? StrategyGoalList)?.title ?? "", theDueDate: (theModel as? StrategyGoalList)?.duedate ?? "", theDueDateColor: (theModel as? StrategyGoalList)?.duedateColor ?? "", theModel: (theModel as? StrategyGoalList), type: .problem)
            break
        
        default:
            break
        }
    }
    
    func actionsSelectTableViewCell(row: Int, stpe: StrategySubControllerType, theModel: Any) {
        
        switch stpe {
        case .step0:
            theCurrentModel.getPrimaryAreaDetailWebService(strPrimaryID: (theModel as! PrimaryArea).id, theOldModel: (theModel as! PrimaryArea), isForAction: true, step: stpe)

//            redirectToStrategyActionScreen(strStrategyID: (theModel as! PrimaryArea).id, strategyTitle: (theModel as! PrimaryArea).title, stpe: stpe, theModel: theModel)
            break
        case .step1:
            theCurrentModel.getSecondaryAreaDetailWebService(strSecondaryID: (theModel as! SecondaryArea).id, theOldModel: (theModel as! SecondaryArea), isForAction: true, step: stpe)

//            redirectToStrategyActionScreen(strStrategyID: (theModel as! SecondaryArea).id, strategyTitle: (theModel as! SecondaryArea).title, stpe: stpe, theModel: theModel)
            break
        case .step2:
            theCurrentModel.getStrategyGoalDetailWebService(strStrategyGoalID: (theModel as! StrategyGoalList).id, theOldModel: (theModel as! StrategyGoalList), isForAction: true, step: stpe)

//            redirectToStrategyActionScreen(strStrategyID: (theModel as! StrategyGoalList).id, strategyTitle: (theModel as! StrategyGoalList).title, stpe: stpe, theModel: theModel)
            break
        }
    }
    
    func didSelectTableViewCell(row: Int, stpe: StrategySubControllerType, selectedData: SubListSelectedDataModel) {
        print("row:=\(row), stpe:=\(stpe)")
        switch stpe {
        case .step0:
            theCurrentView.visibilityoFViewFilter(isHidden: true)
            theCurrentModel.selectedSubListData.append(selectedData)
            theCurrentModel.currentIndexOfSubViewController = 1
            theCurrentView.updateTitleText(strTitle: selectedData.title)
            displayContentController(content: subListViewController(step: .step1, title: "Financial Performance", theModel: selectedData))
            theCurrentView.backButtonStrategy(isHidden: false)
        case .step1:
            theCurrentModel.selectedSubListData.append(selectedData)
            theCurrentModel.currentIndexOfSubViewController = 2
            theCurrentView.updateTitleText(strTitle: selectedData.title + " - Goals")
            displayContentController(content: subListViewController(step: .step2, title: "Financial Performance > Expenses", theModel: selectedData))
        case .step2:
            
            break
        }
        print("selectedSubListData:=",theCurrentModel.selectedSubListData)
    }
}
