//
//  SubListViewController.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

struct SubListSelectedDataModel {
    var title = ""
    var strSelectedPrimaryID = ""
    var strSelectedSecondaryID = ""
    var strSelectedSecondaryAsssignedID = ""
    
    init(title:String = "" ,strSelectedPrimaryID:String = "", strSelectedSecondaryID:String = "", strSelectedSecondaryAsssignedID:String = "") {
        self.title = title
        self.strSelectedPrimaryID = strSelectedPrimaryID
        self.strSelectedSecondaryID = strSelectedSecondaryID
        self.strSelectedSecondaryAsssignedID = strSelectedSecondaryAsssignedID
    }
}

class SubListViewController: ParentViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variable
    var subControllerStpes = StrategySubControllerType.step0
    weak var delegate:StrategySubControllerDelegate!
    var strTitle = ""
    var arrResult:[Any]? = []

    var selectedData = SubListSelectedDataModel()
    var nextOffset = 0
    var apiLoadingAtStrategyID:[String] = []
    let addDD = DropDown()
    let moreDD = DropDown()
    var strSelectedStrategyUserAssignedID = ""
    
    var strSelectedPrimaryID = ""
    var arr_PrimaryResult:[PrimaryArea]?
    
    var strSelectedSecondaryID = ""
    var arr_SecondaryResult:[SecondaryArea]?
    
    var strSelectedStrategyGoalID = ""
    var arr_StrategyGoalResult:[StrategyGoalList]?
    var strSelectedSecondaryAsssignedID = ""
    
    
    lazy var refreshControl1:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = appGreen
        refreshControl.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    
    //MARK:- Life cylce
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        
    }
    
    func setupLayout() {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl1)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(RisksSubListTVCell.self)
        tableView.registerCellNib(StrategySubListTVCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        switch subControllerStpes {
        case .step0:
            primaryAreaListService()
            break
        case .step1:
            secondaryAreaListService()
            break
        case .step2:
            strategyGoalListService()
            break
        }
    }
    
    func setTheData(type:StrategySubControllerType,selectedData:SubListSelectedDataModel) {
        self.subControllerStpes = type
        self.selectedData = selectedData
        self.strTitle = selectedData.title
        self.strSelectedPrimaryID = selectedData.strSelectedPrimaryID
        self.strSelectedSecondaryID = selectedData.strSelectedSecondaryID
        self.strSelectedSecondaryAsssignedID = selectedData.strSelectedSecondaryAsssignedID
    }
    
    
    @objc func refresh() {
        self.refreshControl1.beginRefreshing()
        switch subControllerStpes {
        case .step0:
            primaryAreaListService(isRefreshing: true)
            return
        case .step1:
            secondaryAreaListService(isRefreshing: true)
            return
        case .step2:
            strategyGoalListService(isRefreshing: true)
            return
        }
    }
    
    func refreshPrimaryArea(strAssignedToUserID:String) {
        strSelectedStrategyUserAssignedID = strAssignedToUserID
        arr_PrimaryResult?.removeAll()
        arr_PrimaryResult = nil
        tableView.backgroundView = nil
        tableView.reloadData()
        primaryAreaListService(isRefreshing: true)
    }
    
    func refreshSecondaryArea(strAssignedToUserID:String) {
        strSelectedStrategyUserAssignedID = strAssignedToUserID
        arr_SecondaryResult?.removeAll()
        arr_SecondaryResult = nil
        tableView.backgroundView = nil
        tableView.reloadData()
        secondaryAreaListService(isRefreshing: true)
    }
    
    func refreshStrategyGoal(strAssignedToUserID:String) {
        strSelectedStrategyUserAssignedID = strAssignedToUserID
        arr_StrategyGoalResult?.removeAll()
        arr_StrategyGoalResult = nil
        tableView.backgroundView = nil
        tableView.reloadData()
        strategyGoalListService(isRefreshing: true)
    }
    
    
    
    func configureDropDown(dropDown:DropDown, view:UIView, at indexRow:Int,createdID:String) {
        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
//        print("cellView.contentView.frame:=",cellView.contentView.frame)
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(cellView.contentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: cellView.contentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        
        var dataSource = [MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]

        if !UserDefault.shared.isCanEditForm(strOppoisteID: createdID) {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
            
        }
        /*
        if Utility.shared.userData.id != createdID {
            dataSource.removeLast() // delete
            dataSourceIcon.removeLast()
//            dataSource.removeLast() // edit
//            dataSourceIcon.removeLast()
        }*/
        
        switch subControllerStpes {
        case .step0:
            if UserDefault.shared.isCanEditForm(strOppoisteID: createdID) {
                dataSource.insert(MoreAction.secondary.rawValue, at: 0)
                dataSourceIcon.insert("ic_plus_icon_popup", at: 0)
            }
            break
        case .step1:
            if UserDefault.shared.isCanEditForm(strOppoisteID: createdID) {
                dataSource.insert(MoreAction.goal.rawValue, at: 0)
                dataSourceIcon.insert("ic_plus_icon_popup", at: 0)
            }
            break
        case .step2:
            dataSource.insert("Add", at: 0)
            dataSourceIcon.insert("ic_plus_icon_popup", at: 0)
            if let totalAction = arr_StrategyGoalResult?[indexRow].actionCount, let completedAction = arr_StrategyGoalResult?[indexRow].completeActionCount, let archiveGoalFlag = arr_StrategyGoalResult?[indexRow].archiveGoalFlag, (totalAction == completedAction) && (totalAction != 0 && completedAction != 0) &&  UserDefault.shared.isCanApproveGoal(strAssignedID: strSelectedSecondaryAsssignedID) && archiveGoalFlag == 0 {
                dataSource.append(MoreAction.approve.rawValue)
                dataSourceIcon.append("ic_actons_menu_screen")
            }
            break
        }
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                if  self.subControllerStpes == .step2 && index == 0 {
                    self.configureDropDownForAdd(dropDown: self.addDD, view: view, at: indexRow, createdID: createdID)
                    return
                }
                self.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
    
    func configureDropDownForAdd(dropDown:DropDown, view:UIView,  at indexRow:Int,createdID:String) {
        var arr_DataSource = ["Success Factor","Risk","Tactical Project","Idea","Problem","Focus","Action"]
        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        dropDown.anchorView = view
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(cellView.contentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: cellView.contentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            arr_DataSource.remove(at: 0)
        }
        if UserRole.staffUser == UserDefault.shared.userRole {
            arr_DataSource = ["Idea","Problem","Action"]
        }
        dropDown.dataSource = arr_DataSource
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.performDropDownAction(index: indexRow, item: item)
        }
        dropDown.cancelAction = {
            self.configureDropDown(dropDown: self.moreDD, view: view, at: indexRow, createdID: createdID)
            self.moreDD.show()
        }
        dropDown.show()
    
    }
    
    func setBackground(strMsg:String) {
        switch subControllerStpes {
        case .step0:
            if arr_PrimaryResult == nil || arr_PrimaryResult?.count == 0 {
                tableView.backgroundView = tableView.backGroundMessageView(strMsg: strMsg)
            }
            break
        case .step1:
            if arr_SecondaryResult == nil || arr_SecondaryResult?.count == 0 {
                tableView.backgroundView = tableView.backGroundMessageView(strMsg: strMsg)
            }
            break
        case .step2:
            if arr_StrategyGoalResult == nil || arr_StrategyGoalResult?.count == 0 {
                tableView.backgroundView = tableView.backGroundMessageView(strMsg: strMsg)
            }
            break
        }
        tableView.reloadData()

    }
    
    func updatePrimaryAreaData(theModel:PrimaryArea) {
        if let index = arr_PrimaryResult?.firstIndex(where: {$0.id == theModel.id}) {
            arr_PrimaryResult?.remove(at: index)
            arr_PrimaryResult?.insert(theModel, at: index)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    func updateSecondaryAreaData(theModel:SecondaryArea) {
        if let index = arr_SecondaryResult?.firstIndex(where: {$0.id == theModel.id}) {
            arr_SecondaryResult?.remove(at: index)
            arr_SecondaryResult?.insert(theModel, at: index)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    func updateStrategyGoalData(theModel:StrategyGoalList) {
        if let index = arr_StrategyGoalResult?.firstIndex(where: {$0.id == theModel.id}) {
            arr_StrategyGoalResult?.remove(at: index)
            arr_StrategyGoalResult?.insert(theModel, at: index)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    func updatePrimaryList(list:[PrimaryArea], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_PrimaryResult?.removeAll()
        }
        nextOffset = offset
        if arr_PrimaryResult == nil {
            arr_PrimaryResult = []
        }
        list.forEach({ arr_PrimaryResult?.append($0) })
        print(arr_PrimaryResult!)
        tableView.reloadData()
    }
    func updateSecondaryList(list:[SecondaryArea], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_SecondaryResult?.removeAll()
        }
        nextOffset = offset
        if arr_SecondaryResult == nil {
            arr_SecondaryResult = []
        }
        list.forEach({ arr_SecondaryResult?.append($0) })
        print(arr_SecondaryResult!)
        tableView.reloadData()
    }
    func updateStrategyGoalList(list:[StrategyGoalList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_StrategyGoalResult?.removeAll()
        }
        nextOffset = offset
        if arr_StrategyGoalResult == nil {
            arr_StrategyGoalResult = []
        }
        list.forEach({ arr_StrategyGoalResult?.append($0) })
        print(arr_StrategyGoalResult!)
        tableView.reloadData()
    }
    func performDropDownAction(index:Int,item:String) {
        var theModel:Any?
        var theModelID = ""
        
        switch subControllerStpes {
        case .step0:
            theModel = arr_PrimaryResult![index]
            theModelID = arr_PrimaryResult![index].id
            break
        case .step1:
            theModel = arr_SecondaryResult![index]
            theModelID = arr_SecondaryResult![index].id
            break
        case .step2:
            theModel = arr_StrategyGoalResult![index]
            theModelID = arr_StrategyGoalResult![index].id
            break
        }
        guard let model = theModel else { return }
        var rediretToMoreAction = false
        
        switch item {
        case MoreAction.edit.rawValue:
            rediretToMoreAction = true
            break
        case MoreAction.delete.rawValue:
            deleteStrategy(index: index, theModelID: theModelID, theModel: model)
            return
        case MoreAction.chart.rawValue:
            rediretToMoreAction = true
            break
        case MoreAction.action.rawValue:
            rediretToMoreAction = true
            break
        case MoreAction.secondary.rawValue:
            rediretToMoreAction = true
            break
        case MoreAction.goal.rawValue:
            rediretToMoreAction = true
            break
        case MoreAction.approve.rawValue:
            approveStrategy(index: index, theModelID: theModelID, theModel: model)
//            rediretToMoreAction = true
            break
//            ["Success Factor","Risk","Tactical Project","Idea","Problem","Focus","Action"]
        case "Success Factor","Risk","Tactical Project","Idea","Problem","Focus":
            rediretToMoreAction = true
            break
        default:
            break
        }
        if rediretToMoreAction,delegate != nil {
            delegate.moreActionTablviewCell(step: subControllerStpes, theModel: model, at: index, itemName: item)
            return
        }
    }
    
    func deleteStrategy(index:Int, theModelID: String, theModel:Any) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.apiLoadingAtStrategyID.append(theModelID)
            self?.updateBottomCell(loadingID: theModelID, at: index,isLoadingApi: true)
            self?.deleteStrategyService(at: index, theModelID: theModelID, theModel: theModel)
        }
    }
    
    func approveStrategy(index:Int, theModelID: String, theModel:Any) {
        showAlertAction(msg: "Are you sure want to approve this?") { [weak self] in
            self?.apiLoadingAtStrategyID.append(theModelID)
            self?.updateBottomCell(loadingID: theModelID, at: index,isLoadingApi: true)
            self?.approveStrategyService(at: index, theModelID: theModelID, theModel: theModel)
        }
    }
    
    func updateBottomCell(loadingID:String,at Index:Int, isRemove:Bool = false, isLoadingApi:Bool = false) {
        
//        apiLoadingAtStrategyID = isLoadingApi ? Index : -1
        let contentOffset = tableView.contentOffset
        var indexRow = -1
        
        switch subControllerStpes {
        case .step0:
            if let index = arr_PrimaryResult?.firstIndex(where: {$0.id == loadingID}) {
                indexRow = index
            }
            break
        case .step1:
            if let index = arr_SecondaryResult?.firstIndex(where: {$0.id == loadingID}) {
                indexRow = index
            }
            //                arr_SecondaryResult?.remove(at: Index)
            break
        case .step2:
            if let index = arr_StrategyGoalResult?.firstIndex(where: {$0.id == loadingID}) {
                indexRow = index
            }
            //                arr_StrategyGoalResult?.remove(at: Index)
            break
        }
        if isRemove && indexRow != -1 {
            if let index = apiLoadingAtStrategyID.firstIndex(where: {$0 == loadingID}) {
                apiLoadingAtStrategyID.remove(at: index)
            }
            switch subControllerStpes {
            case .step0:
                if indexRow != -1 {
                    arr_PrimaryResult?.remove(at: indexRow)
                }
                if arr_PrimaryResult!.count == 0 {
                    setBackground(strMsg: "Primary area not available")
                }
                break
            case .step1:
                if indexRow != -1 {
                    arr_SecondaryResult?.remove(at: indexRow)
                }
                if arr_SecondaryResult!.count == 0 {
                    setBackground(strMsg: "Secondary area not available")
                }
//                arr_SecondaryResult?.remove(at: Index)
                break
            case .step2:
                if indexRow != -1 {
                    arr_StrategyGoalResult?.remove(at: indexRow)
                }
                if arr_StrategyGoalResult!.count == 0 {
                    setBackground(strMsg: "Strategy goal not available")
                }
//                arr_StrategyGoalResult?.remove(at: Index)
                break
            }
            UIView.performWithoutAnimation {
                tableView.reloadData()
                tableView.contentOffset = contentOffset
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = apiLoadingAtStrategyID.firstIndex(where: {$0 == loadingID}) {
                    apiLoadingAtStrategyID.remove(at: index)
                }
            }
            if indexRow == -1 {
                return
            }
            let index = IndexPath(row: Index, section: 0)
            UIView.performWithoutAnimation {
                tableView.reloadRows(at: [index], with: .none)
                tableView.contentOffset = contentOffset
            }
        }
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = arr_StrategyGoalResult?.firstIndex(where: {$0.id == strActionLogID }) {
            if isLoadingApi {
                apiLoadingAtStrategyID.append(strActionLogID)
            } else {
                if let index = apiLoadingAtStrategyID.firstIndex(where: {$0 == strActionLogID}) {
                    apiLoadingAtStrategyID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        arr_StrategyGoalResult![index].revisedColor = model.revisedColor
                        arr_StrategyGoalResult![index].duedateColor = model.revisedColor
                        arr_StrategyGoalResult![index].revisedDate = model.revisedDate
                        arr_StrategyGoalResult![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        arr_StrategyGoalResult![index].workdateColor = model.workdateColor
//                        arr_StrategyGoalResult![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = tableView.contentOffset
                tableView.reloadRows(at: [indexPath], with: .none)
                tableView.contentOffset = contentOffset
            }
        }
    }
}

//MARK:-UITableViewDataSource
extension SubListViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch subControllerStpes {
        case .step0:
            if let _ = tableView.backgroundView {
                return 0
            }
            tableView.backgroundView = nil
            return arr_PrimaryResult?.count ?? 10
        case .step1:
            if let _ = tableView.backgroundView , (arr_SecondaryResult == nil || arr_SecondaryResult?.count == 0) {
                return 0
            }
            tableView.backgroundView = nil
            return arr_SecondaryResult?.count ?? 10
        case .step2:
            if let _ = tableView.backgroundView , (arr_StrategyGoalResult == nil || arr_StrategyGoalResult?.count == 0) {
                return 0
            }
            tableView.backgroundView = nil
            return arr_StrategyGoalResult?.count ?? 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StrategySubListTVCell") as! StrategySubListTVCell
        switch subControllerStpes {
        case .step0:
            if arr_PrimaryResult != nil && arr_PrimaryResult?.count != 0 {
                cell.setLineOfTitle()
                cell.updateBackGroundColor(isCompleted: arr_PrimaryResult![indexPath.row].archiveGoalFlag == 1)
//                cell.setEditButtonUpdate(createdID: arr_PrimaryResult![indexPath.row].createdBy)
                let total = "\(arr_PrimaryResult![indexPath.row].completeActionCount)/\(arr_PrimaryResult![indexPath.row].actionCount)"
                var isApiLoading = false
                if apiLoadingAtStrategyID.contains(arr_PrimaryResult![indexPath.row].id) {
                    isApiLoading = true
                }
                cell.configure(img: arr_PrimaryResult![indexPath.row].userImage, strTitle: arr_PrimaryResult![indexPath.row].title,strAssignTo:arr_PrimaryResult![indexPath.row].assignedTo, strTotal: total, strNotesCount: "\(arr_PrimaryResult![indexPath.row].notesCount)", isApiLoading: isApiLoading)
                cell.updateProfileView(strAssignedID: arr_PrimaryResult![indexPath.row].assigneeId)
                /*
                cell.handlerEditAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.delegate.editSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_PrimaryResult![indexPath.row])
                    }
                }*/
                cell.handlerActions = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.delegate.actionsSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_PrimaryResult![indexPath.row])
                    }
                }
                cell.handlerMoreAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore, at: indexPath.row, createdID: obj.arr_PrimaryResult![indexPath.row].createdBy)
                        obj.moreDD.show()
                    }
                }
    
                let lastSectionIndex = tableView.numberOfSections - 1
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && nextOffset != -1 {
                    primaryAreaListService()
                }
            } else {
                cell.showAnimation()
            }
            break
        case .step1:
            if arr_SecondaryResult != nil && arr_SecondaryResult?.count != 0 {
                cell.setLineOfTitle()
                cell.updateBackGroundColor(isCompleted: arr_SecondaryResult![indexPath.row].archiveGoalFlag == 1)
//                cell.setEditButtonUpdate(createdID: arr_SecondaryResult![indexPath.row].createdBy)
                let total = "\(arr_SecondaryResult![indexPath.row].completeActionCount)/\(arr_SecondaryResult![indexPath.row].actionCount)"
                var isApiLoading = false
                if apiLoadingAtStrategyID.contains(arr_SecondaryResult![indexPath.row].id) {
                    isApiLoading = true
                }
                cell.configure(img: arr_SecondaryResult![indexPath.row].userImage, strTitle: arr_SecondaryResult![indexPath.row].title,strAssignTo:arr_SecondaryResult![indexPath.row].assignedTo, strTotal: total, strNotesCount: "\(arr_SecondaryResult![indexPath.row].notesCount)", isApiLoading: isApiLoading)
                cell.updateProfileView(strAssignedID: arr_SecondaryResult![indexPath.row].assigneeId)

                /*cell.handlerEditAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.delegate.editSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_SecondaryResult![indexPath.row])
                    }
                }*/
                cell.handlerActions = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.delegate.actionsSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_SecondaryResult![indexPath.row])
                    }
                }
                cell.handlerMoreAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore, at: indexPath.row, createdID: obj.arr_SecondaryResult![indexPath.row].createdBy)
                        obj.moreDD.show()
                    }
                }
                let lastSectionIndex = tableView.numberOfSections - 1
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && nextOffset != -1 {
                    secondaryAreaListService()
                }
            } else {
                cell.showAnimation()
            }
            break
        case .step2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RisksSubListTVCell") as! RisksSubListTVCell

            if arr_StrategyGoalResult != nil && arr_StrategyGoalResult?.count != 0 {
                var isApiLoading = false
                if apiLoadingAtStrategyID.contains(arr_StrategyGoalResult![indexPath.row].id) {
                    isApiLoading = true
                }
                cell.configureStrategyGoal(theModel: arr_StrategyGoalResult![indexPath.row], isApiLoading: isApiLoading)
                cell.updateProfileView(strAssignedID: arr_StrategyGoalResult![indexPath.row].assigneeId)

                cell.handleDueDateAction = { [weak self] in
                    if !UserDefault.shared.isArchiveAction(strStatus: self?.arr_StrategyGoalResult![indexPath.row].status ?? "") {
                        self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.arr_StrategyGoalResult![indexPath.row].duedate ?? "", dueDate: self?.arr_StrategyGoalResult![indexPath.row].duedate ?? "", strActionID: self?.arr_StrategyGoalResult![indexPath.row].id ?? "")
                    }
                }
                cell.updateBackGroundColor(isCompleted: arr_StrategyGoalResult![indexPath.row].archiveGoalFlag == 1)
                cell.btnMore.tag = indexPath.row
                cell.handlerActions = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        
                        obj.delegate.actionsSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_StrategyGoalResult![indexPath.row])
                    }
                }
                cell.handlerMoreAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        
                        obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore, at: indexPath.row, createdID: obj.arr_StrategyGoalResult![indexPath.row].createdBy)
                        obj.moreDD.show()
                    }
                }
                
                let lastSectionIndex = tableView.numberOfSections - 1
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && nextOffset != -1 {
                    strategyGoalListService()
                }
                
                
                
                /*
//                ====== Old =====
                cell.setLineOfTitle(noOfLine: 0)
                cell.updateBackGroundColor(isCompleted: arr_StrategyGoalResult![indexPath.row].completeActionFlag == 1)
//                cell.setEditButtonUpdate(createdID: arr_StrategyGoalResult![indexPath.row].createdBy)
                let total = "\(arr_StrategyGoalResult![indexPath.row].completeActionCount)/\(arr_StrategyGoalResult![indexPath.row].actionCount)"
                var isApiLoading = false
                if apiLoadingAtStrategyID.contains(arr_StrategyGoalResult![indexPath.row].id) {
                    isApiLoading = true
                }
                cell.configure(img: arr_StrategyGoalResult![indexPath.row].userImage, strTitle: arr_StrategyGoalResult![indexPath.row].title,strAssignTo:arr_StrategyGoalResult![indexPath.row].assignedTo, strTotal: total, strNotesCount: "\(arr_StrategyGoalResult![indexPath.row].notesCount)", isApiLoading: isApiLoading)
                /*cell.handlerEditAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        obj.delegate.editSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_StrategyGoalResult![indexPath.row])
                    }
                }*/
                cell.handlerActions = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                        
                        obj.delegate.actionsSelectTableViewCell(row: indexPath.row, stpe: obj.subControllerStpes, theModel: obj.arr_StrategyGoalResult![indexPath.row])
                    }
                }
                cell.handlerMoreAction = { [weak self] in
                    if let obj = self, obj.delegate != nil {
                       
                        obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore, at: indexPath.row, createdID: obj.arr_StrategyGoalResult![indexPath.row].createdBy)
                        obj.moreDD.show()
                    }
                }
                let lastSectionIndex = tableView.numberOfSections - 1
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && nextOffset != -1 {
                    strategyGoalListService()
                }*/
            } else {
                cell.startAnimation()
            }
            return cell
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension SubListViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && nextOffset != -1 {
            var isShowError = false
            switch subControllerStpes {
            case .step0:
                if arr_PrimaryResult != nil {
                    isShowError = true
                }
                break
            case .step1:
                if arr_SecondaryResult != nil {
                    isShowError = true
                }
                break
            case .step2:
                if arr_StrategyGoalResult != nil {
                    isShowError = true
                }
                 break
            }
            
            if isShowError {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = appGreen
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
            }
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var selectedID = ""
        var selectedData = SubListSelectedDataModel(title: strTitle , strSelectedPrimaryID: selectedID, strSelectedSecondaryID: selectedID)
        switch subControllerStpes {
        case .step0:
            if arr_PrimaryResult == nil {
                return
            }
            selectedID = arr_PrimaryResult![indexPath.row].id
            selectedData = SubListSelectedDataModel(title: arr_PrimaryResult![indexPath.row].title, strSelectedPrimaryID: selectedID, strSelectedSecondaryID: "")
            break
        case .step1:
            if arr_SecondaryResult == nil {
                return
            }
            selectedID = arr_SecondaryResult![indexPath.row].id
            selectedData = SubListSelectedDataModel(title: arr_SecondaryResult![indexPath.row].title, strSelectedPrimaryID: self.selectedData.strSelectedPrimaryID, strSelectedSecondaryID: selectedID, strSelectedSecondaryAsssignedID:arr_SecondaryResult![indexPath.row].assigneeId)
            break
        case .step2:
            /*if arr_StrategyGoalResult == nil {
                return
            }
            selectedID = arr_StrategyGoalResult![indexPath.row].id
            selectedData = SubListSelectedDataModel(title: arr_StrategyGoalResult![indexPath.row].title, strSelectedPrimaryID: self.selectedData.strSelectedPrimaryID, strSelectedSecondaryID: selectedID)*/
            
            if arr_StrategyGoalResult != nil && delegate != nil {
                delegate.actionsSelectTableViewCell(row: indexPath.row, stpe: subControllerStpes, theModel: arr_StrategyGoalResult![indexPath.row])
            }
            return
        }
        
        if apiLoadingAtStrategyID.contains(selectedID) {
            return
        }
        if delegate != nil {
            delegate.didSelectTableViewCell(row: indexPath.row, stpe: subControllerStpes, selectedData: selectedData)
//            delegate.didSelectTableViewCell(row: indexPath.row, stpe: subControllerStpes, strSelectedID: selectedID)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -45 { //change 40 to whatever you want
            if  !self.refreshControl1.isRefreshing{
                refresh()
            }
        }
    }
}

//MARK:-UITableViewDelegate
extension SubListViewController {
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_strategy_goals,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
    func primaryAreaListService(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
        }
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset,APIKey.key_list_for:APIKey.key_all,APIKey.key_company_id:Utility.shared.userData.companyId,APIKey.key_assign_to:strSelectedStrategyUserAssignedID] as [String : Any]
        StrategyWebService.shared.getPrimaryAreaList(parameter: parameter, success: { [weak self] (list, offset) in
                self?.updatePrimaryList(list: list, offset: offset, isRefreshing: isRefreshing)
                self?.refreshControl1.endRefreshing()
            }, failed: { [weak self] (error) in
                self?.updatePrimaryList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.refreshControl1.endRefreshing()
        })
    }
    
    func secondaryAreaListService(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_primary_area_id:strSelectedPrimaryID,APIKey.key_page_offset:nextOffset] as [String : Any]
        StrategyWebService.shared.getSecondaryAreaList(parameter: parameter, success: { [weak self] (list, offset) in
                self?.refreshControl1.endRefreshing()
                self?.updateSecondaryList(list: list, offset: offset, isRefreshing: isRefreshing)
            }, failed: { [weak self] (error) in
                self?.updateSecondaryList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.refreshControl1.endRefreshing()
        })
    }
    
    func strategyGoalListService(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_primary_area_id:strSelectedPrimaryID,APIKey.key_secondary_area_id:strSelectedSecondaryID,APIKey.key_page_offset:nextOffset] as [String : Any]
        StrategyWebService.shared.getStrategyGoalList(parameter: parameter, success: { [weak self] (list, offset) in
                self?.refreshControl1.endRefreshing()
                self?.updateStrategyGoalList(list: list, offset: offset, isRefreshing: isRefreshing)
            }, failed: { [weak self] (error) in
                self?.updateStrategyGoalList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.refreshControl1.endRefreshing()
        })
        
    }
    
    func deleteStrategyService(at Index:Int, theModelID: String, theModel:Any) {
        var delete_table = ""
//        var strategyID = ""
        
        switch subControllerStpes {
        case .step0:
            delete_table = APIKey.key_strategy_primary_areas
//            strategyID = (theModel as? PrimaryArea)?.id ?? ""
            break
        case .step1:
            delete_table = APIKey.key_strategy_secondary_areas
//            strategyID = (theModel as? SecondaryArea)?.id ?? ""

            break
        case .step2:
            delete_table = APIKey.key_strategy_goals
//            strategyID = (theModel as? StrategyGoalList)?.id ?? ""
            break
        }
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_delete_id:theModelID,APIKey.key_delete_table:delete_table] as [String : Any]
        StrategyWebService.shared.deleteStrategy(parameter: parameter, success: { [weak self] (msg) in
                if let obj = self, obj.delegate != nil {
                    obj.delegate.moreActionTablviewCell(step: obj.subControllerStpes, theModel: theModel, at: Index, itemName: MoreAction.delete.rawValue)
                }
            self?.updateBottomCell(loadingID: theModelID, at: Index, isRemove: true)
            /*if let step = self?.subControllerStpes {
                switch step {
                case .step0:
                    self?.refreshPrimaryArea(strAssignedToUserID: self?.strSelectedStrategyUserAssignedID ?? "")
                    break
                case .step1:
                    self?.refreshSecondaryArea(strAssignedToUserID: self?.strSelectedStrategyUserAssignedID ?? "")
                    break
                case .step2:
                    self?.refreshStrategyGoal(strAssignedToUserID: self?.strSelectedStrategyUserAssignedID ?? "")
                    break
                }
            }*/
//                self?.showAlertAtBottom(message: msg)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell(loadingID: theModelID, at: Index)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func approveStrategyService(at Index:Int,theModelID: String, theModel:Any) {
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_status_for_id:theModelID,APIKey.key_status_for:APIKey.key_strategy_goals] as [String : Any]
        StrategyWebService.shared.approveStrategy(parameter: parameter, success: { [weak self] (msg) in
            if let obj = self, obj.delegate != nil {
                obj.delegate.moreActionTablviewCell(step: obj.subControllerStpes, theModel: theModel, at: Index, itemName: MoreAction.approve.rawValue)
            }
            (theModel as? StrategyGoalList)?.archiveGoalFlag = 1
            self?.arr_StrategyGoalResult?[Index].archiveGoalFlag = 1
            self?.updateBottomCell(loadingID: theModelID, at: Index, isRemove: false)
            
            //                self?.showAlertAtBottom(message: msg)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell(loadingID: theModelID, at: Index)
                self?.showAlertAtBottom(message: error)
        })
    }
}
