//
//  StrategyActionsVC.swift
//  StrategyX
//
//  Created by Haresh on 04/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class StrategyActionsVC: ParentViewController {

    fileprivate lazy var theCurrentView:StrategyActionsView = { [unowned self] in
        return self.view as! StrategyActionsView
    }()
    
    fileprivate lazy var theCurrentModel:StrategyActionsModel = {
       return StrategyActionsModel.init(theController: self)
    }()
    var handlerUpdateModel:(Any, Bool) -> Void = { _,_ in }
    var handlerUpdateStrategyActionModel:(StrategyAction?) -> Void = { _ in }
    var handlerUpdateEditStrategy:(Any) -> Void = { _ in }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    func setTheData(strStrategyID:String,strategyTitle:String,step:StrategySubControllerType, theModel:Any) {
        theCurrentModel.strategyID = strStrategyID
        theCurrentModel.strategyTitle = strategyTitle
        theCurrentModel.stpe = step
        theCurrentModel.theStrategyModel = theModel
    }
    func setTheStrategyActionModel(theModel:StrategyAction?) {
        theCurrentModel.theStrategyActionModel = theModel
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.updateTitle(strTitle: theCurrentModel.strategyTitle)
        theCurrentView.setTheDelegateOfCollectionView(theDelegate: self)
        theCurrentView.setupTableView(theDelegate: self)
        updateHeaderList()
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        }
        /*if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            theCurrentView.updateFilterCountText(strCount: count)
        }*/
        getActionListWebService()
    }
    func updateHeaderList() {
        theCurrentView.collectionView.reloadData()
    }
    func updateEditStrategyDetailView() {
        if let viewEditStrategyDetail = self.view.viewWithTag(2134565) as? EditStrategyDetailView {
            viewEditStrategyDetail.animateAddButton(isAnimate: false)
            removeViewFromSuperView(view: viewEditStrategyDetail)
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_ActionsList == nil || theCurrentModel.arr_ActionsList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
   
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_ActionsList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            break
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            break
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            presentGraphActionList(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            break
        default:
            break
        }
        
    }
    
    func deleteActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    func archiveActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: false,isArchive: true)
        }
    }
    
    func updateNotes(index:Int, description:String, coreCompetence:[String], isUpdateCoreCompetence:Bool) {
        let coreCompetenceJsonString = JSON(coreCompetence).rawString() ?? ""
        let strengthsText = theCurrentModel.theStrategyActionModel?.strengthsText ?? ""
        let weaknessesText = theCurrentModel.theStrategyActionModel?.weaknessesText ?? ""
        let opportunitiesText = theCurrentModel.theStrategyActionModel?.opportunitiesText ?? ""
        let threatsText = theCurrentModel.theStrategyActionModel?.threatsText ?? ""
        
        var notes = [theCurrentModel.theStrategyActionModel?.visionText ?? "",
                     theCurrentModel.theStrategyActionModel?.missionText ?? "",
                     theCurrentModel.theStrategyActionModel?.valuesText ?? "",
                     theCurrentModel.theStrategyActionModel?.cultureText ?? "",
                     theCurrentModel.theStrategyActionModel?.coreBusinessText ?? "",
                     coreCompetenceJsonString,
                     strengthsText,
                     weaknessesText,
                     opportunitiesText,
                     threatsText]
        if !isUpdateCoreCompetence {
            notes[index] = description
        }
        theCurrentModel.strategyActionService(isView: false, strStrategyActions: notes)        
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_ActionsList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            let deletedModel = model[indexRow]
            if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtActionID.remove(at: index)
            }
            //            theCurrentModel.apiLoadingAtActionID = "-1"
            theCurrentModel.arr_ActionsList?.remove(at: indexRow)
            
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            updateTheStrategyModelFromActionList(isForUpdate: false, theActionListModel: deletedModel, isDeleteItem: true)
            if theCurrentModel.arr_ActionsList?.count == 0 {
                setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
            }
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [index], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateTheStrategyModelFromActionList(theActionDetailModel:ActionDetail? = nil,isForUpdate:Bool = false, theActionListModel:ActionsList? = nil, isDeleteItem:Bool = false) {
        guard var arrModels = theCurrentModel.arr_ActionsList else { return }
        var theModel = theActionListModel
        var previousModelPercentageCompeleted = "-1"
        
        if let model = theActionListModel {
            if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                let updatedModel = theCurrentModel.arr_ActionsList![index]
                previousModelPercentageCompeleted = updatedModel.percentageCompeleted
                updatedModel.actionlogTitle = model.actionlogTitle
                updatedModel.revisedDate = model.revisedDate
                updatedModel.duedate = model.duedate
                updatedModel.workDate = model.workDate
                updatedModel.percentageCompeleted = model.percentageCompeleted
                updatedModel.assigneeId = model.assigneeId
                updatedModel.assignedTo = model.assignedTo
                updatedModel.assignedToUserImage = model.assignedToUserImage
                theCurrentModel.arr_ActionsList![index] = model
                theModel = model
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
        
        
//        guard let deletedActionModel = theModel else { return }
        
        //        theCurrentModel.theFocusModel = model
        arrModels = theCurrentModel.arr_ActionsList!
        var totalActions = arrModels.count
        var completedActions =  0
        var completedActionFlag = 0
        
        switch theCurrentModel.stpe {
        case .step0:
            totalActions = (theCurrentModel.theStrategyModel as? PrimaryArea)?.actionCount ?? 0
            completedActions = (theCurrentModel.theStrategyModel as? PrimaryArea)?.completeActionCount ?? 0
            completedActionFlag = (theCurrentModel.theStrategyModel as? PrimaryArea)?.completeActionFlag ?? 0
            break
        case .step1:
            totalActions = (theCurrentModel.theStrategyModel as? SecondaryArea)?.actionCount ?? 0
            completedActions = (theCurrentModel.theStrategyModel as? SecondaryArea)?.completeActionCount ?? 0
            completedActionFlag = (theCurrentModel.theStrategyModel as? SecondaryArea)?.completeActionFlag ?? 0
            break
        case .step2:
            totalActions = (theCurrentModel.theStrategyModel as? StrategyGoalList)?.actionCount ?? 0
            completedActions = (theCurrentModel.theStrategyModel as? StrategyGoalList)?.completeActionCount ?? 0
            completedActionFlag = (theCurrentModel.theStrategyModel as? StrategyGoalList)?.completeActionFlag ?? 0
            break
        }
        
        
        //        let filterCompletedActions = arrModels.filter({ $0.percentageCompeleted == "100" })
        /*
        if isForUpdate {
            if deletedActionModel.percentageCompeleted != "100" && previousModelPercentageCompeleted == "100" {
                completedActions -= 1
            } else if deletedActionModel.percentageCompeleted == "100" && previousModelPercentageCompeleted != "100" {
                completedActions += 1
            }
        } else {
            if deletedActionModel.percentageCompeleted == "100" {
                completedActions -= 1
            }
        }*/
        
        if isDeleteItem {
            totalActions -= 1
            totalActions = totalActions < 0 ? 0 : totalActions
        }
        
        if completedActions == -1 {
            completedActions = 0
        }
        completedActionFlag = (completedActions != 0 && totalActions != 0) && (completedActions == totalActions) ? 1 : 0
        switch theCurrentModel.stpe {
        case .step0:
            (theCurrentModel.theStrategyModel as? PrimaryArea)?.actionCount = totalActions
            (theCurrentModel.theStrategyModel as? PrimaryArea)?.completeActionCount = completedActions
            (theCurrentModel.theStrategyModel as? PrimaryArea)?.completeActionFlag = completedActionFlag
            break
        case .step1:
            (theCurrentModel.theStrategyModel as? SecondaryArea)?.actionCount = totalActions
            (theCurrentModel.theStrategyModel as? SecondaryArea)?.completeActionCount = completedActions
            (theCurrentModel.theStrategyModel as? SecondaryArea)?.completeActionFlag = completedActionFlag

            break
        case .step2:
            (theCurrentModel.theStrategyModel as? StrategyGoalList)?.actionCount = totalActions
            (theCurrentModel.theStrategyModel as? StrategyGoalList)?.completeActionCount = completedActions
            (theCurrentModel.theStrategyModel as? StrategyGoalList)?.completeActionFlag = completedActionFlag
            break
        }
        print("theCurrentModel.theStrategyModel:=",theCurrentModel.theStrategyModel)
        handlerUpdateModel(theCurrentModel.theStrategyModel, isDeleteItem)
//        model.totalAction = totalActions
//        model.complateAction = completedActions
//        model.completeActionFlag = completedActionFlag
//        print("theCurrentModel.theFocusModel:=",model)
//        theCurrentModel.theFocusModel = model
        
    }
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_ActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            } else {
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
                if let model = theModel {
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_ActionsList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_ActionsList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_ActionsList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_ActionsList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_ActionsList![index].workdateColor = model.workdateColor
                        theCurrentModel.arr_ActionsList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId }) {
            theCurrentModel.arr_ActionsList![index] = model
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentModel.arr_ActionsList![index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentModel.arr_ActionsList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_ActionsList![index].revisedColor = model.revisedColor
            theCurrentModel.arr_ActionsList![index].duedate = model.revisedDate
            theCurrentModel.arr_ActionsList![index].duedateColor = model.duedateColor
            theCurrentModel.arr_ActionsList![index].requestFlag = model.requestFlag
            theCurrentModel.arr_ActionsList![index].approveFlag = model.approveFlag

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentModel.arr_ActionsList?[index].percentageCompeleted = "\(percentage)"
            theCurrentModel.arr_ActionsList?[index].approveFlag = percentage == 100 ? 1 : 0

            updateTheStrategyModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theCurrentModel.arr_ActionsList?[index])
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel)  in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            if let model = theActionModel {
                theModel?.duedateColor = model.duedateColor
            }
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_ActionsList?.removeAll()
            self?.theCurrentModel.arr_ActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getActionListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphAction(strChartId:String = "", chartFlagViewType:ChartFlagView = ChartFlagView.strategy) {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.btnGraph.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 10.0, bottom: tabbarheight, chartFlagViewType: chartFlagViewType)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToAddActionVC(theModel:ActionDetail) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
            self?.updateTheStrategyModelFromActionList(theActionDetailModel: theActionDetailModel, isForUpdate: true, theActionListModel: nil)
        }
        self.push(vc: vc)
    }
    func redirectToActionDetailScreen(theModel:ActionsList) {
        let vc = ActionDetailVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setupData(theActionId: theModel.actionlogId, isCheckEditScreen: false)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionListModel) in
            self?.updateTheStrategyModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionListModel)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theModel) in
            self?.updateRequestDueDateActionListModelAndUI(theModel: theModel)
        }
        self.push(vc: vc)
    }
    
    func removeViewFromSuperView(view:UIView)  {
        (view as? EditStrategyDetailView)?.animateAddButton(isAnimate: false)
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 0
        }, completion: { (finished) in
            view.removeFromSuperview()
        })
    }
    func showEditStrategyDetailView(title:String,description:String,defaultText:String,index:Int) {
        let view = EditStrategyDetailView.loadFromXib() as! EditStrategyDetailView
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.setupLayout()
        view.tag = 2134565
        view.setTheData(title: title, description: description, defaultText: defaultText)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        var list:[String] = []
        if let coreCompetence = theCurrentModel.theStrategyActionModel?.coreCompetencText {
            list = Array(coreCompetence)
        }
        
        view.handlerOnTextUpdate = { [weak self] (text) in
            self?.updateNotes(index: index, description: text, coreCompetence: list, isUpdateCoreCompetence: false)
            //            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    
    func showEditCoreCompetenciesView(title:String,arrPoints:[String], index:Int) {
        let view = EditCoreCompetenciesView.loadFromXib() as! EditCoreCompetenciesView
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.setupLayout()
        view.tag = 213489
        view.setTheData(title: title, arrPoints: arrPoints)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        view.handlerOnTextUpdate = { [weak self] (item) in
            self?.updateNotes(index: index, description: "", coreCompetence: item, isUpdateCoreCompetence: true)
            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    func showSeeMoreStrategyDetailView(title:String,description:String, index:Int) {
        let view = SeeMoreStrategyDetailView.loadFromXib() as! SeeMoreStrategyDetailView
        let height = self.view.frame.size.height
        
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: height)
        view.setupLayout()
        view.setTheData(title: title, description: description)
        view.handlerOnCancelAction = { [weak self] in
            self?.removeViewFromSuperView(view: view)
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            view.alpha = 1
        }, completion: { (finished) in
            view.alpha = 1
        })
        self.view.addSubview(view)
    }
    
    func redirectToAddPrimaryAreaScreen(theOldModel:PrimaryArea) {
        let vc = AddPrimaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theOldModel, isForEdit: true)
        vc.handlerEditData = { [weak self] (theModelPrimary) in
            if let obj = self {
                theModelPrimary.completeActionCount = theOldModel.completeActionCount
                theModelPrimary.completeActionFlag = theOldModel.completeActionFlag
                theModelPrimary.actionCount = theOldModel.actionCount
                theModelPrimary.notesCount = 0
                obj.theCurrentModel.theStrategyModel = theModelPrimary
                obj.theCurrentView.updateTitle(strTitle: theModelPrimary.title)
                obj.handlerUpdateEditStrategy(theModelPrimary)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddSecondaryAreaScreen(theOldModel:SecondaryArea? = nil,isForAddFromStrategyList:Bool = false) {
        let vc = AddSecondaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theOldModel, isForEdit: true)
        
        vc.handlerEditData = { [weak self] (theModelSecondary) in
            if let obj = self {
                theModelSecondary.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelSecondary.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelSecondary.actionCount = theOldModel?.actionCount ?? 0
                theModelSecondary.notesCount = 0
                obj.theCurrentModel.theStrategyModel = theModelSecondary
                obj.theCurrentView.updateTitle(strTitle: theModelSecondary.title)
                obj.handlerUpdateEditStrategy(theModelSecondary)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddGoalScreen(theOldModel:StrategyGoalList? = nil,isForAddFromStrategyList:Bool = false) {
        let vc = AddGoalVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theOldModel, isForEdit: true)
        vc.handlerEditData = { [weak self] (theModelGoal) in
            if let obj = self {
                theModelGoal.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelGoal.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelGoal.actionCount = theOldModel?.actionCount ?? 0
                theModelGoal.notesCount = 0
                obj.theCurrentModel.theStrategyModel = theModelGoal
                obj.theCurrentView.updateTitle(strTitle: theModelGoal.title)
                obj.handlerUpdateEditStrategy(theModelGoal)
            }
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnArchiveAction(_ sender: Any) {
//        theCurrentModel.filterData = FilterData.init()
//        setCompanyData()
        theCurrentModel.isForActionArchive = true
        theCurrentView.viewArchive.isHidden = true
        theCurrentView.lblTitle.text = "\(theCurrentModel.strategyTitle) > Archived Actions"
//        theCurrentView.updateSubTitleViewHiddenForArchiveAction(true)
        theCurrentModel.arr_ActionsList?.removeAll()
        theCurrentModel.arr_ActionsList = nil
        theCurrentView.tableView.reloadData()
        getActionListWebService(isRefreshing: true)
    }
    @IBAction func onBtnEditScreenAction(_ sender: Any) {
        if theCurrentModel.stpe == .step0 {
            if let model = theCurrentModel.theStrategyModel as? PrimaryArea {
                model.notesCount = 0
                handlerUpdateEditStrategy(model)
                redirectToAddPrimaryAreaScreen(theOldModel: model)
            }
        } else if theCurrentModel.stpe == .step1 {
            if let model = theCurrentModel.theStrategyModel as? SecondaryArea {
                model.notesCount = 0
                handlerUpdateEditStrategy(model)
                redirectToAddSecondaryAreaScreen(theOldModel: model)
            }
        } else {
            if let model = theCurrentModel.theStrategyModel as? StrategyGoalList {
                model.notesCount = 0
                handlerUpdateEditStrategy(model)
                redirectToAddGoalScreen(theOldModel: model)
            }
        }
    }
    
    @IBAction func onBtnFocusGraphAction(_ sender: Any) {
        if theCurrentModel.stpe == .step0 {
            presentGraphAction(strChartId: theCurrentModel.strategyID, chartFlagViewType: ChartFlagView.primary)
        } else if theCurrentModel.stpe == .step1 {
            presentGraphAction(strChartId: theCurrentModel.strategyID, chartFlagViewType: ChartFlagView.secondary)
        } else {
            presentGraphAction(strChartId: theCurrentModel.strategyID, chartFlagViewType: ChartFlagView.strategyGoal)
        }
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionFilter()
    }
    
}
//MARK:- UICollectionViewDataSource
extension StrategyActionsVC:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StrategyCardCollectionViewCell", for: indexPath) as! StrategyCardCollectionViewCell
        cell.setTheEditAction(id: theCurrentModel.theStrategyActionModel?.id ?? "")
        switch indexPath.row {
        case 0:
            cell.configure(type: .vision, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.visionTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.visionText
                cell.configure(type: .vision, description: theCurrentModel.theStrategyActionModel!.visionText, isSkeltonAnimation: false)
            }
            break
        case 1:
            //            cell.configure(type: .mission, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .mission, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.missionTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.missionText
                cell.configure(type: .mission, description: theCurrentModel.theStrategyActionModel!.missionText, isSkeltonAnimation: false)
            }
            break
        case 2:
            //            cell.configure(type: .value, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .value, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.valuesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.valuesText
                cell.configure(type: .value, description: theCurrentModel.theStrategyActionModel!.valuesText, isSkeltonAnimation: false)
            }
            break
        case 3:
            //            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .culture, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.cultureTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.cultureText
                cell.configure(type: .culture, description: theCurrentModel.theStrategyActionModel!.cultureText, isSkeltonAnimation: false)
            }
            break
        case 4:
            //            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .coreBussiness, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.coreBusinessTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.coreBusinessText
                cell.configure(type: .coreBussiness, description: theCurrentModel.theStrategyActionModel!.coreBusinessText, isSkeltonAnimation: false)
            }
            break
        case 5:
            //            cell.configure(type: .culture, description: theCurrentModel.arr_Notes[indexPath.row])
            cell.configure(type: .coreCompetencies, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault
                cell.coreCompetenices = Array(theCurrentModel.theStrategyActionModel!.coreCompetencText)
                cell.configure(type: .coreCompetencies, description: theCurrentModel.theStrategyActionModel!.coreCompetencTextDefault, isSkeltonAnimation: false)
            }
            break
        case 6:
            cell.configure(type: .strengths, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.strengthsTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.strengthsText
                cell.configure(type: .strengths, description: theCurrentModel.theStrategyActionModel!.strengthsText, isSkeltonAnimation: false)
            }
            break
        case 7:
            cell.configure(type: .weaknesses, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.weaknessesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.weaknessesText
                cell.configure(type: .weaknesses, description: theCurrentModel.theStrategyActionModel!.weaknessesText, isSkeltonAnimation: false)
            }
            break
        case 8:
            cell.configure(type: .opportunities, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.opportunitiesTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.opportunitiesText
                cell.configure(type: .opportunities, description: theCurrentModel.theStrategyActionModel!.opportunitiesText, isSkeltonAnimation: false)
            }
            break
        case 9:
            cell.configure(type: .threats, description: " ", isSkeltonAnimation: true)
            if theCurrentModel.theStrategyActionModel != nil {
                cell.defaultText = theCurrentModel.theStrategyActionModel!.threatsTextDefault
                cell.orignalText = theCurrentModel.theStrategyActionModel!.threatsText
                cell.configure(type: .threats, description: theCurrentModel.theStrategyActionModel!.threatsText, isSkeltonAnimation: false)
            }
            break
        default:
            break
        }
        
        cell.handlerBtnEditAction = { [weak self] in
            if self?.theCurrentModel.theStrategyActionModel != nil {
                if indexPath.row == 5 {
                    if let arr_CoreBusiness = self?.theCurrentModel.theStrategyActionModel!.coreCompetencText {
                        self?.showEditCoreCompetenciesView(title: cell.lblTitle.text ?? "", arrPoints: Array(arr_CoreBusiness), index: indexPath.row)
                    }
                } else {
                    self?.showEditStrategyDetailView(title: cell.lblTitle.text ?? "", description: cell.orignalText, defaultText: cell.defaultText, index: indexPath.row)
                }
            }
        }
        cell.handlerBtnSeeMoreAction = {[weak self] in
            if self?.theCurrentModel.theStrategyActionModel != nil {
                self?.showSeeMoreStrategyDetailView(title: cell.lblTitle.text ?? "", description: cell.lblDescription.text ?? "", index: indexPath.row)
            }
        }
        
        return cell
    }
    
}
//MARK:- UICollectionViewDelegate
extension StrategyActionsVC:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentModel.theStrategyActionModel != nil {
            let cell = collectionView.cellForItem(at: indexPath) as! StrategyCardCollectionViewCell
            var strDescription = cell.orignalText.isEmpty ? cell.defaultText : cell.orignalText
            if indexPath.row == 5 {
                let arr_CoreBusiness = theCurrentModel.theStrategyActionModel!.coreCompetencText
                if Array(arr_CoreBusiness).count != 0 {
                    strDescription = "".createBulletString(arr: Array(arr_CoreBusiness))
                }
            }
            showSeeMoreStrategyDetailView(title: cell.lblTitle.text ?? "", description: strDescription, index: indexPath.row)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == theCurrentView.collectionView {
            let pageWidth = Float(theCurrentView.itemWidth + theCurrentView.itemSpacing)
            let targetXContentOffset = Float(targetContentOffset.pointee.x)
            let contentWidth = Float(theCurrentView.collectionView!.contentSize.width  )
            var newPage = Float(theCurrentView.currentItem)
            
            if velocity.x == 0 {
                newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
            } else {
                newPage = Float(velocity.x > 0 ? theCurrentView.currentItem + 1 : theCurrentView.currentItem - 1)
                if newPage < 0 {
                    newPage = 0
                }
                if (newPage > contentWidth / pageWidth) {
                    newPage = ceil(contentWidth / pageWidth) - 1.0
                }
            }
            
            theCurrentView.currentItem = Int(newPage)
            let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
        }
    }
}
/*
//MARK:- UIScrollViewDelegate
extension StrategyActionsVC:UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == theCurrentView.collectionView {
            let pageWidth = Float(theCurrentView.itemWidth + theCurrentView.itemSpacing)
            let targetXContentOffset = Float(targetContentOffset.pointee.x)
            let contentWidth = Float(theCurrentView.collectionView!.contentSize.width  )
            var newPage = Float(theCurrentView.currentItem)
            
            if velocity.x == 0 {
                newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
            } else {
                newPage = Float(velocity.x > 0 ? theCurrentView.currentItem + 1 : theCurrentView.currentItem - 1)
                if newPage < 0 {
                    newPage = 0
                }
                if (newPage > contentWidth / pageWidth) {
                    newPage = ceil(contentWidth / pageWidth) - 1.0
                }
            }
            
            theCurrentView.currentItem = Int(newPage)
            let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
            targetContentOffset.pointee = point
        }
    }
}
*/
//MARK:-UITableViewDataSource
extension StrategyActionsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_ActionsList == nil || theCurrentModel.arr_ActionsList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_ActionsList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActionCell") as! ActionCell
        cell.showMoreButtonForAction(isHidden: true)
        if theCurrentModel.arr_ActionsList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtActionID.contains(theCurrentModel.arr_ActionsList![indexPath.row].actionlogId) {
                isApiLoading = true
            }
            cell.configureActions(theModel: theCurrentModel.arr_ActionsList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentModel.arr_ActionsList![indexPath.row])
//                self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_ActionsList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_ActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_ActionsList![indexPath.row].actionlogId ?? "")
            }
            cell.handleWorkDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentModel.arr_ActionsList![indexPath.row].workDate ?? "", dueDate: self?.theCurrentModel.arr_ActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_ActionsList![indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerPercentCompletedAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentModel.arr_ActionsList?[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_ActionsList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getActionListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
    
}
//MARK:-UITableViewDelegate
extension StrategyActionsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.section].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("Chart")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_ActionsList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_ActionsList != nil {
            performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            /*
            if theCurrentModel.arr_ActionsList![indexPath.row].actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
                redirectToActionDetailScreen(theModel: theCurrentModel.arr_ActionsList![indexPath.row])
            } else {
                performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            }*/
        }
    }
}

//MARK:- Api Call
extension StrategyActionsVC {
    func getActionListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = theCurrentModel.filterData.workDate, !workDateFormate.isEmpty {
            //            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = theCurrentModel.filterData.dueDate, !dueDateFormate.isEmpty {
            //            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_tags:theCurrentModel.filterData.searchTag,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_workdatefilter:workDate,
                         APIKey.key_duedatefilter:dueDate,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]

        switch theCurrentModel.stpe {
        case .step0:
            parameter[APIKey.key_primary_area] = theCurrentModel.strategyID
            break
        case .step1:
            parameter[APIKey.key_secondary_area] = theCurrentModel.strategyID

            break
        case .step2:
            parameter[APIKey.key_strategy_goal] = theCurrentModel.strategyID
            break
        }
        if theCurrentModel.isForActionArchive {
            parameter[APIKey.key_grid_type] = APIKey.key_archived
        }
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        ActionsWebService.shared.getAllActionsList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()

        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
//            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}
