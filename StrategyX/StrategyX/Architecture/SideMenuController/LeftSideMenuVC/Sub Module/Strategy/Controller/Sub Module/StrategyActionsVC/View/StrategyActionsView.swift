//
//  StrategyActionsView.swift
//  StrategyX
//
//  Created by Haresh on 04/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class StrategyActionsView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var viewArchive: UIView!
    @IBOutlet weak var btnBackFocus: UIButton!
    @IBOutlet weak var constrainBtnBackFocusWidth: NSLayoutConstraint!// 35.0
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    let collectionMargin = CGFloat(24)
    let itemSpacing = CGFloat(10)
    let itemHeight = CGFloat(106)
    
    var itemWidth = CGFloat(0)
    var currentItem = 0
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButton(isHidden: false)
        updateFilterCountText(strCount: "")
        setupCollectionView()
    }
    /*func setupCollectionView() {
        collectionView.registerCellNib(StrategyCardCollectionViewCell.self)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.itemSize = CGSize(width: self.frame.width - 45, height: 106)
        collectionView.collectionViewLayout = flowLayout
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 15.0, bottom: 0, right: 15.0)
        collectionView.showsHorizontalScrollIndicator = false
    }*/
    
    func setupCollectionView() {
        collectionView.registerCellNib(StrategyCardCollectionViewCell.self)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        collectionView!.collectionViewLayout = layout
        collectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    func setTheDelegateOfCollectionView(theDelegate:StrategyActionsVC) {
        collectionView.delegate = theDelegate
        collectionView.dataSource = theDelegate
    }
    func setupTableView(theDelegate:StrategyActionsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(ActionCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? StrategyActionsVC)?.getActionListWebService(isRefreshing: true)        
    }
    
    func backButton(isHidden:Bool) {
        constrainBtnBackFocusWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
    func updateTitle(strTitle:String) {
        lblTitle.text = strTitle
    }

}
