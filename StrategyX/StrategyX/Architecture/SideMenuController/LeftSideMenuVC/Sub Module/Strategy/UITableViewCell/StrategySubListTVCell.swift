//
//  StrategySubListTVCell.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class StrategySubListTVCell: UITableViewCell {
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30

    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var btnTotal: UIButton!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!

    
    var handlerEditAction:() -> Void = {}
    var handlerActions:() -> Void = {}
    var handlerMoreAction:() -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        showAnimation()
//        [img_Profile,lblTitle,btnTotal,btnEdit,btnMore].forEach({
//            $0.showAnimatedSkeleton()
//        })
//        lblTotal.text = " "

    }
    func showAnimation() {
        updateBackGroundColor(isCompleted: false)
        img_Profile.isHidden = false
        img_Profile.showAnimatedSkeleton()
        lblTitle.showAnimatedSkeleton()
        btnTotal.showAnimatedSkeleton()
        btnEdit.showAnimatedSkeleton()
        btnMore.showAnimatedSkeleton()
        viewNotesCount.showSkeleton()
        lblTitle.text = " "
    }
    func hideAnimation() {
        img_Profile.hideSkeleton()
        lblTitle.hideSkeleton()
        btnTotal.hideSkeleton()
        btnEdit.hideSkeleton()
        btnMore.hideSkeleton()
        viewNotesCount.hideSkeleton()
//        [img_Profile,lblTitle,btnTotal,btnEdit,btnMore].forEach({ $0.hideSkeleton() })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateBackGroundColor(isCompleted:Bool = false) {
        viewBg.backgroundColor = isCompleted ? appGreen.withAlphaComponent(0.19) : nil
    }
    func setLineOfTitle(noOfLine:Int = 2)  {
        lblTitle.numberOfLines = noOfLine
    }

    func configure(img:String,strTitle:String,strAssignTo:String, strTotal:String,strNotesCount:String,isApiLoading:Bool) {
        hideAnimation()
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblNotesCount.text = strNotesCount
        viewNotesCount.setBackGroundColor = (Int(strNotesCount) ?? 0) > 0 ? 9 : 3
        btnMore.loadingIndicator(isApiLoading)
        lblProfile.text = strAssignTo.acronym()
        if img.isEmpty && strAssignTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !img.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: img, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if strAssignTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if strAssignTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        /*if !img.isEmpty {
            img_Profile.sd_setImageLoadMultiTypeURLForList(url: img, completion: { [weak self] (isHidden) in
                self?.lblProfile.isHidden = !isHidden
                if strAssignTo.isEmpty {
                    self?.img_Profile.isHidden = false
                }
            })
        } 
        
        if strAssignTo.isEmpty {
            img_Profile.isHidden = false
        }*/
//        img_Profile.sd_setImageLoadMultiTypeURL(url: img, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with:  URL(string: img), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))

        lblTitle.text = strTitle
        btnTotal.setTitle(strTotal, for: .normal)
//        lblTotal.text = strTotal
    }
    
    func setEditButtonUpdate(createdID:String) {
        if UserDefault.shared.userRole == UserRole.staffUser && Utility.shared.userData.id != createdID {
            btnEdit.isHidden = true
        } else {
            btnEdit.isHidden = false
        }
    }
    func hideViewNotes() {
        viewNotesCount.isHidden = true
//        constrainViewNotesWidth.constant = 0
//        constrainViewNotesLeading.constant = 0
    }
    func updateProfileView(strAssignedID:String) {
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID:strAssignedID) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
    }
    //MARK:-IBAction
    @IBAction func onBtnEditAction(_ sender: Any) {
        handlerEditAction()
    }
    
    @IBAction func onBtnActions(_ sender: Any) {
        handlerActions()
    }
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
    
    
}
