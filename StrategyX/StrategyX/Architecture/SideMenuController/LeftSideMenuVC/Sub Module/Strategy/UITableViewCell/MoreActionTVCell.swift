//
//  MoreActionTVCell.swift
//  StrategyX
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class MoreActionTVCell: DropDownCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(iconName:String, title:String) {
        imgItem.image = UIImage(named: iconName)
        optionLabel.text = title
    }
    
}
