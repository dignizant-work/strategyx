//
//  CoreCompetenciesCollectionViewCell.swift
//  StrategyX
//
//  Created by Haresh on 22/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CoreCompetenciesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
}
