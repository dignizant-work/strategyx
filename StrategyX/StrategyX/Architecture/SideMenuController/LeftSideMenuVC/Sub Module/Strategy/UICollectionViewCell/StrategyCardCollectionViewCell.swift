//
//  StrategyCardCollectionViewCell.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class StrategyCardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var coreCompetenices:[String] = []
    var defaultText = ""
    var orignalText = ""
    
    
    var handlerBtnEditAction:() -> Void = {}
    var handlerBtnSeeMoreAction:() -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        viewBG.showAnimatedSkeleton()
       showAnimation()
    }
    func showAnimation() {
        [lblDescription,btnEdit].forEach({ $0?.showAnimatedSkeleton() })
    }
    func hideAnimation() {
        [lblDescription,btnEdit].forEach({ $0?.hideSkeleton() })
//        viewBG.hideSkeleton()
    }
    
    func configure(type:StrategyCardType,description:String,isSkeltonAnimation:Bool = false) {
        
        /*switch type {
        case .vision:
            viewBG.backgroundColor = appStrategyBlue
            break
        case .mission:
            viewBG.backgroundColor = appStrategyYellow
            break
        case .value:
            viewBG.backgroundColor = appStrategyLightSkyBlue
            break
        case .culture:
            viewBG.backgroundColor = appStrategyPurple
            break
        case .coreBussiness:
            viewBG.backgroundColor = addLightBlueAddSubTaskColor
            lblTitle.setFontColor = 10
            lblDescription.setFontColor = 10
            btnMore.setFontColor = 10

            break
        case .coreCompetencies:
            viewBG.backgroundColor = addLightBlueAddSubTaskColor
            lblTitle.setFontColor = 10
            lblDescription.setFontColor = 10
            btnMore.setFontColor = 10
            break
        }*/
        if isSkeltonAnimation {
            //            lblTypeName.text = " "
            lblDescription.text = " "
//            viewBG.showAnimatedSkeleton()
//            [lblDescription].forEach({ $0.showAnimatedSkeleton() })
            showAnimation()
        } else {
            hideAnimation()
            lblDescription.text = orignalText.isEmpty ? defaultText : orignalText
            lblDescription.isHidden = false
            collectionView.isHidden = true
            if type == .coreCompetencies {
                lblDescription.isHidden = coreCompetenices.count != 0
                collectionView.isHidden = coreCompetenices.count == 0
                if !collectionView.isHidden {
                    var reOrder:[String] = []
                    
                    for (index, element) in coreCompetenices.enumerated() {
                        if index % 2 == 0 {
                            if index != 0 {
                                if let last = reOrder.last {
                                    reOrder[index - 1] = element
                                    reOrder.append(last)
                                }
                            } else {
                                reOrder.append(element)
                            }
                        } else {
                            reOrder.append(element)
                        }
                    }
                    coreCompetenices = reOrder
//                    print("reOrder:=",reOrder)
                }
                setupCollectionView()
            }
        }
        lblTitle.text = type.rawValue
//        lblDescription.text = description
    }
    func setupCollectionView() {
        collectionView.registerCellNib(CoreCompetenciesCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.isUserInteractionEnabled = false
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.itemSize = CGSize(width: (collectionView.frame.width - 10)/2, height: 20)
        collectionView.collectionViewLayout = flowLayout
//        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.reloadData()
        
    }
    func setTheEditAction(id:String) {
        btnEdit.isHidden = true
        btnEdit.tintColor = appTextBlackShedColor
        if UserDefault.shared.userRole != .staffUser {
            btnEdit.isHidden = false
        }
    }
    
    //MARK:- IBAction
    @IBAction func onBtnMoreAction(_ sender: Any) {
//        (cellPresentingViewController() as? StrategyVC)
        handlerBtnSeeMoreAction()
    }
    
    @IBAction func onBtnEditAction(_ sender: Any) {
//        (cellPresentingViewController() as? StrategyVC).
        handlerBtnEditAction()
        
    }
}

extension StrategyCardCollectionViewCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coreCompetenices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoreCompetenciesCollectionViewCell", for: indexPath) as! CoreCompetenciesCollectionViewCell
        cell.configure(strTitle: coreCompetenices[indexPath.row])
        return cell
    }
    
    
}
