//
//  StrategyModel.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class StrategyModel {

    fileprivate weak var theController:StrategyVC!
    var subControllerStpes = StrategySubControllerType.step0
    var subViewControllers:[SubListViewController] = []
    var currentIndexOfSubViewController = 0
    var strSelectedCompanyID = Utility.shared.userData.companyId
    
    var strSelectedPrimaryID = ""
    var strSelectedSecondaryID = ""
    var selectedSubListData:[SubListSelectedDataModel] = []
    
    var theStrategyActionModel:StrategyAction?

    var arr_assignList:[StrategyUserAssign] = []
    var strSelectedUserAssignID:String = ""

//    var arr_Notes:[String] = ["","","",""]
    
    
    //MARK:- Variable
    init(theController:StrategyVC) {
        self.theController = theController
    }
    
    func updateStratgyActionModel(actions:[String]) {
        theStrategyActionModel?.visionText = actions[0]
        theStrategyActionModel?.missionText = actions[1]
        theStrategyActionModel?.valuesText = actions[2]
        theStrategyActionModel?.cultureText = actions[3]
        theStrategyActionModel?.coreBusinessText = actions[4]
        if actions.indices.contains(5) {
            let json = JSON(parseJSON: actions[5])
            print("json:=",json)
            if json.count > 0 {
                theStrategyActionModel?.coreCompetencText.removeAll()
                for item in json.arrayValue {
                    theStrategyActionModel?.coreCompetencText.append(item.stringValue)
                }
            }
        }
        theStrategyActionModel?.strengthsText = actions[6]
        theStrategyActionModel?.weaknessesText = actions[7]
        theStrategyActionModel?.opportunitiesText = actions[8]
        theStrategyActionModel?.threatsText = actions[9]
        
        theController.updateHeaderList()
        theController.updateEditStrategyDetailView()
    }
}

//MARK:- Api Call
extension StrategyModel {
    func strategyActionService(isView:Bool = false,strStrategyActions:[String]) {
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_company_id:Utility.shared.userData.companyId]
        if isView {
            parameter[APIKey.key_is_view] = "yes"
        } else {
            parameter[APIKey.key_vision_text] = strStrategyActions[0]
            parameter[APIKey.key_mission_text] = strStrategyActions[1]
            parameter[APIKey.key_values_text] = strStrategyActions[2]
            parameter[APIKey.key_culture_text] = strStrategyActions[3]
            parameter[APIKey.key_core_business_text] = strStrategyActions[4]
            parameter[APIKey.key_core_competenc_text] = strStrategyActions[5]
            parameter[APIKey.key_strengths_text] = strStrategyActions[6]
            parameter[APIKey.key_weaknesses_text] = strStrategyActions[7]
            parameter[APIKey.key_opportunities_text] = strStrategyActions[8]
            parameter[APIKey.key_threats_text] = strStrategyActions[9]
        }
        
        StrategyWebService.shared.getStrategyActions(parameter: parameter, isEdit: !isView, success: { [weak self] (theModel) in
            if isView {
                self?.theStrategyActionModel = theModel
                self?.theController.updateHeaderList()
            } else {
                self?.updateStratgyActionModel(actions: strStrategyActions)
            }
        }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func strategyAssignUserListService() {
        (theController.view as? StrategyView)?.loadFilterToActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getStrategyAssignUserList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
            //            self?.theController.updateTableForDropDownStopLoading()
            (self?.theController.view as? StrategyView)?.loadFilterToActivity(isStart: false)
            
            }, failed: { [weak self] (error) in
                (self?.theController.view as? StrategyView)?.loadFilterToActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    
    func getPrimaryAreaDetailWebService(strPrimaryID:String, theOldModel:PrimaryArea,isForAction:Bool = false,step:StrategySubControllerType = StrategySubControllerType.step0) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_primary_area_id:strPrimaryID] as [String : Any]
        StrategyWebService.shared.getPrimaryArea(parameter: parameter, success: { [weak self] (theDetailModel) in
                Utility.shared.stopActivityIndicator()
            if isForAction {
                theDetailModel?.notesCount = theOldModel.notesCount
                theDetailModel?.actionCount = theOldModel.actionCount
                theDetailModel?.completeActionFlag = theOldModel.completeActionFlag
                theDetailModel?.completeActionCount = theOldModel.completeActionCount
                self?.theController.redirectToStrategyActionScreen(strStrategyID: theOldModel.id, strategyTitle: theOldModel.title, stpe: step, theModel: theDetailModel!)
            } else {
                self?.theController.redirectToAddPrimaryAreaScreen(theModel: theDetailModel, theOldModel: theOldModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })

    }
    
    func getSecondaryAreaDetailWebService(strSecondaryID:String, theOldModel:SecondaryArea,isForAction:Bool = false, step:StrategySubControllerType = StrategySubControllerType.step1) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_secondary_area_id:strSecondaryID] as [String : Any]
        StrategyWebService.shared.getSecondaryArea(parameter: parameter, success: { [weak self] (theDetailModel) in
            Utility.shared.stopActivityIndicator()
            if isForAction {
                theDetailModel?.notesCount = theOldModel.notesCount
                theDetailModel?.actionCount = theOldModel.actionCount
                theDetailModel?.completeActionFlag = theOldModel.completeActionFlag
                theDetailModel?.completeActionCount = theOldModel.completeActionCount
                self?.theController.redirectToStrategyActionScreen(strStrategyID: theOldModel.id, strategyTitle: theOldModel.title, stpe: step, theModel: theDetailModel!)
            } else {
                self?.theController.redirectToAddSecondaryAreaScreen(theModel: theDetailModel, theOldModel: theOldModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    
    func getStrategyGoalDetailWebService(strStrategyGoalID:String, theOldModel:StrategyGoalList,isForAction:Bool = false,step:StrategySubControllerType = StrategySubControllerType.step2) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_strategy_goal_id:strStrategyGoalID] as [String : Any]
        StrategyWebService.shared.getStrategyGoal(parameter: parameter, success: { [weak self] (theDetailModel) in
            Utility.shared.stopActivityIndicator()
            if isForAction {
                theDetailModel?.notesCount = theOldModel.notesCount
                theDetailModel?.actionCount = theOldModel.actionCount
                theDetailModel?.completeActionFlag = theOldModel.completeActionFlag
                theDetailModel?.completeActionCount = theOldModel.completeActionCount
                self?.theController.redirectToStrategyActionScreen(strStrategyID: theOldModel.id, strategyTitle: theOldModel.title, stpe: step, theModel: theDetailModel!)
            } else {
                self?.theController.redirectToAddGoalScreen(theModel: theDetailModel, theOldModel: theOldModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    
}

