//
//  StrategyView.swift
//  StrategyX
//
//  Created by Haresh on 17/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class StrategyView: ViewParentWithoutXIB {

    //MARK:- Variabel
    @IBOutlet weak var btnBackStrategy: UIButton!
    @IBOutlet weak var constrainBtnBackStrategyWidth: NSLayoutConstraint!// 35.0
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitlePrimary: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var constrainViewFilterHeight: NSLayoutConstraint! // 30
    @IBOutlet weak var lblFilterText: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var constrainViewFilterWidth: NSLayoutConstraint! // 165.0
    
    let collectionMargin = CGFloat(24)
    let itemSpacing = CGFloat(10)
    let itemHeight = CGFloat(106)
    
    var itemWidth = CGFloat(0)
    var currentItem = 0

    //MARK:- Life Cycle
    func setupLayout() {
        backButtonStrategy(isHidden: true)
        updateTitleText(strTitle: "Primary Result")
        setupCollectionView()
    }
    /*
    func setupCollectionView() {
        collectionView.registerCellNib(StrategyCardCollectionViewCell.self)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.itemSize = CGSize(width: self.frame.width - 45, height: 106)
        collectionView.collectionViewLayout = flowLayout
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 15.0, bottom: 0, right: 15.0)
        collectionView.showsHorizontalScrollIndicator = false
    }*/
    
    func setupCollectionView() {
        collectionView.registerCellNib(StrategyCardCollectionViewCell.self)

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        collectionView!.collectionViewLayout = layout
        collectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    func setTheDelegateOfCollectionView(theDelegate:StrategyVC) {
        collectionView.delegate = theDelegate
        collectionView.dataSource = theDelegate
    }
    
    func backButtonStrategy(isHidden:Bool) {
        constrainBtnBackStrategyWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateTitleText(strTitle:String) {
        lblTitlePrimary.text = strTitle
    }

    func loadFilterToActivity(isStart:Bool) {
        btnFilter.loadingIndicator(isStart, allignment: .right)
    }
    
    func visibilityoFViewFilter(isHidden:Bool) {
        viewFilter.isHidden = isHidden
        lblTitlePrimary.numberOfLines = isHidden ? 2 : 1
        constrainViewFilterWidth.constant = isHidden ? 0.0 : 165.0
        constrainViewFilterHeight.constant = isHidden ? 0.0 : 30.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
}

