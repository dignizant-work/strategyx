//
//  SeeMoreStrategyDetailView.swift
//  StrategyX
//
//  Created by Haresh on 08/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SeeMoreStrategyDetailView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDummy: UILabel!
    
    
    var handlerOnCancelAction:() -> Void = { }
    
    
    func setupLayout() {

    }
    func setTheData(title:String, description:String) {
        lblTitle.text = title
        lblDummy.text = description
        lblDescription.text = lblDummy.text
    }
        
    
    //MARK:- IBAction
    @IBAction func onAddCancelAction(_ sender: Any) {
        handlerOnCancelAction()
    }

}
