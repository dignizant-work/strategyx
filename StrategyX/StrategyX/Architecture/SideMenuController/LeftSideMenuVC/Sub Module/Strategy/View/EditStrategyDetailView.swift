//
//  EditStrategyDetailView.swift
//  StrategyX
//
//  Created by Haresh on 08/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EditStrategyDetailView: UIView {
    
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblErrorDescriptionMsg: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    let disposeBag = DisposeBag()
    
    var handlerOnTextUpdate:(String) -> Void = {_ in }
    var handlerOnCancelAction:() -> Void = { }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setupLayout() {
        onTextDescription()
    }
    func setTheData(title:String, description:String, defaultText:String) {
        txtDescription.isScrollEnabled = false
        lblErrorDescriptionMsg.text = defaultText
        lblTitle.text = title
        txtDescription.text = description
        lblErrorDescriptionMsg.isHidden = description.count > 0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) { [weak self] in
            self?.txtDescription.isScrollEnabled = true
        }
    }
    func animateAddButton(isAnimate:Bool = false) {
        if isAnimate {
            btnSave.startLoadyIndicator()
            self.allButtons(isEnable: false)
        } else {
            btnSave.stopLoadyIndicator()
            self.allButtons(isEnable: true)
        }
    }
    
    //MARK:- Rx
    func onTextDescription() {
        txtDescription.rx.text.orEmpty
            .subscribe(onNext:{ [weak self] (text) in
                self?.lblErrorDescriptionMsg.isHidden = text.count > 0
            }).disposed(by: disposeBag)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnAddAction(_ sender: UIButton) {
        self.endEditing(true)
        if txtDescription.text.trimmed().isEmpty {
            Utility.shared.showAlertAtBottom(strMessage: "Please enter description")
            return
        }
        animateAddButton(isAnimate: true)
        handlerOnTextUpdate(txtDescription.text ?? "")
    }
    
    @IBAction func onAddCancelAction(_ sender: Any) {
        self.endEditing(true)
        handlerOnCancelAction()
    }
    
}
