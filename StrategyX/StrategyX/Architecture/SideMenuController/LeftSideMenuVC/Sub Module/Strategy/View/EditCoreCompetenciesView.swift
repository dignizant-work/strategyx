//
//  EditCoreCompetenciesView.swift
//  StrategyX
//
//  Created by Haresh on 23/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EditCoreCompetenciesView: UIView {

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDescription:UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint! // 0.0

    var arrPoints:[String] = []
    
    let disposeBag = DisposeBag()

    
    var handlerOnTextUpdate:([String]) -> Void = {_ in }
    var handlerOnCancelAction:() -> Void = { }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    func setupLayout() {
//        onTextDescription()
        onTableView()
        setupTableView()
       
//        tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    func setupTableView() {
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50.0
        tableView.registerCellNib(AddActionSubTaskListTVCell.self)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            self.constrainTableViewHeight.constant = tabl.contentSize.height + 250 > self.frame.size.height ?  self.frame.size.height - 250 : tabl.contentSize.height
            self.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
    }
    
    func setTheData(title:String, arrPoints:[String]) {
        lblTitle.text = title
        self.arrPoints = arrPoints
        tableView.reloadData()
//        onTableViewData()
    }
    func updateSubTaskList(strTask:String, index:Int)  {
        if strTask.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.endEditing(true)
            Utility.shared.showAlertAtBottom(strMessage: "Please enter core competencies")
            return
        }
        arrPoints.insert(strTask, at: 0)
        tableView.reloadData()
    }
    func animateAddButton(isAnimate:Bool = false) {
        if isAnimate {
            btnSave.startLoadyIndicator()
            self.allButtons(isEnable: false)
        } else {
            btnSave.stopLoadyIndicator()
            self.allButtons(isEnable: true)
        }
    }
    
    //MARK:- Rx
    func onTextDescription() {
        txtDescription.rx.text.orEmpty
            .subscribe(onNext:{ [weak self] (text) in
                if !text.trimmed().isEmpty {
                    self?.txtDescription.text = nil
                    self?.updateSubTaskList(strTask: text, index: 0)

                } else {
                    Utility.shared.showAlertAtBottom(strMessage: "Please enter core competencies")
                }
            }).disposed(by: disposeBag)
    }
   
    func onTableView() {
        tableView.rx.observe(CGSize.self, "contentSize").observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext:{ [weak self] (contentSize) in
                if let size = contentSize {
                    let sizeTabl = size.height + 250 > (self?.frame.size.height ?? 0) ?  (self?.frame.size.height ?? 0) - 250 : size.height
                    self?.constrainTableViewHeight.constant = sizeTabl
//                    self?.constrainTableViewHeight.constant = size.height
                    self?.layoutIfNeeded()
                }
            }).disposed(by: disposeBag)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnAddTaskAction(_ sender: UIButton) {
//        self.endEditing(true)
        let text = txtDescription.text ?? ""
        txtDescription.text = nil
        updateSubTaskList(strTask: text, index: 0)
    }
    @IBAction func onBtnAddAction(_ sender: UIButton) {
        self.endEditing(true)
        if arrPoints.count == 0 {
            Utility.shared.showAlertAtBottom(strMessage: "Please enter at least one core competencies")
            return
        }
        animateAddButton(isAnimate: true)
        handlerOnTextUpdate(arrPoints)
    }
    
    @IBAction func onAddCancelAction(_ sender: Any) {
        self.endEditing(true)
        handlerOnCancelAction()
    }
    deinit {
//        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
}
//MARK:- UITableViewDataSource
extension EditCoreCompetenciesView:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.configure(strTitle: arrPoints[indexPath.row])
        cell.handlerOnDeleteAction = { [weak self] (tag) in
            self?.arrPoints.remove(at: indexPath.row)
            UIView.performWithoutAnimation {
//                tableView.deleteRows(at: [indexPath], with: .none)
                tableView.reloadData()
            }
        }
        return cell
    }
    
    
}
