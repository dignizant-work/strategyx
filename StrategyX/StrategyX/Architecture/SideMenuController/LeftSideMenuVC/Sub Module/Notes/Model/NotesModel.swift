//
//  NotesModel.swift
//  StrategyX
//
//  Created by Haresh on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class NotesModel {

    //MARK:- Variable
    fileprivate weak var theController:NotesVC!
//    var arr_List = ["Headset No Longer Wired For Sound","No Longer Wired For Sound","Longer Wired For Sound","Wired For Sound","Longer Wired For Sound","No Longer Wired For Sound","Headset No Longer Wired For Sound","Wired For Sound"]

//    var isSearching = false
    var arr_NoteList:[Notes]?
    var nextOffset = 0
    var apiLoadingAtNoteID:[String] = []
    var filterData = FilterNotesData.init()
    let moreDD = DropDown()

    
    //MARK:- Init
    init(theController:NotesVC) {
        self.theController = theController
    }
    
    func updateList(list:[Notes], offset:Int, isRefreshing:Bool) {
        
        nextOffset = offset
        if isRefreshing {
            arr_NoteList?.removeAll()
        }
        if arr_NoteList == nil {
            arr_NoteList = []
        }
        
        list.forEach({ arr_NoteList?.append($0)})
        print(arr_NoteList!)
    }

    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:Notes) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        let dataSource = [MoreAction.convert.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        let dataSourceIcon = ["ic_actons_menu_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]
        
       /* if Utility.shared.userData.id != theModel.created {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }*/
        
        /*if theModel.completeActionFlag != 1 {
         dataSource.removeLast() // archive
         dataSourceIcon.removeLast()
         }*/
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
}

//MARK:- Api Call
extension NotesModel {
    func notesListService(isRefreshing:Bool) {
        if isRefreshing {
            nextOffset = 0
        }
        var createdDate = ""
        
        if let workDateFormate = filterData.createdDate {
            createdDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType2)
        }
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset, APIKey.key_search:filterData.searchText,APIKey.key_date:createdDate] as [String : Any]
        NotesWebServices.shared.getAllNotesList(parameter: parameter, success: { [weak self] (list, nextOffset)  in
                self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
                self?.theController.updateTableViewData()
            }, failed: {[weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
        
    }
    func changeNoteStatusWebService(strNoteID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strNoteID]]).rawString() else {
            theController.updateBottomCell(loadingID: strNoteID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_my_notes,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.theController.updateBottomCell(loadingID: strNoteID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.theController.updateBottomCell(loadingID: strNoteID)
        })
    }
}
