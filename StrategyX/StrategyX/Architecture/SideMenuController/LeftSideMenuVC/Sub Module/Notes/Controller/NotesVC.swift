//
//  NotesVC.swift
//  StrategyX
//
//  Created by Haresh on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotesVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:NotesView = { [unowned self] in
       return self.view as! NotesView
    }()
    fileprivate lazy var theCurrentModel:NotesModel = {
       return NotesModel(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    func setupUI() {
        theCurrentView.setupLayout()
        setLogoOnNavigationHeader()
        theCurrentView.setupTableView(theDelegate: self)
        refreshApiCall()
    }
    
    func refreshApiCall() {
        
//        theCurrentModel.isSearching = false
//        theCurrentView.txtSearch.text = ""
//        theCurrentView.txtSearch.resignFirstResponder()
        
        theCurrentModel.arr_NoteList?.removeAll()
        theCurrentModel.arr_NoteList = nil
        theCurrentModel.nextOffset = 0
        theCurrentView.tableView.backgroundView = nil
        theCurrentView.tableView.reloadData()
        theCurrentModel.notesListService(isRefreshing: true)
    }
    /*
    func searchText(txt:String, endEditing:Bool = false) {
        theCurrentModel.isSearching = !txt.trimmed().isEmpty
//        theCurrentModel.arr_Search.removeAll()
//        theCurrentModel.arr_Search = theCurrentModel.arr_List.filter({ $0.lowercased().contains(txt.lowercased()) })
//        theCurrentView.tableView.reloadData()
        
        if endEditing {
            theCurrentModel.arr_NoteList?.removeAll()
            theCurrentModel.arr_NoteList = nil
            theCurrentModel.nextOffset = 0
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.notesListService(isRefreshing: true, textSearch: txt)
        }
    }*/
    func updateTableViewData() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    func addNotesModelInList(theModel:Notes) {
        if theCurrentModel.arr_NoteList == nil {
            theCurrentModel.arr_NoteList = [theModel]
        } else {
            theCurrentModel.arr_NoteList?.insert(theModel, at: 0)
        }
        UIView.performWithoutAnimation {
            theCurrentView.tableView.reloadData()
        }
    }
    func setBackground(strMsg:String) {
        theCurrentView.refreshControl.endRefreshing()
        if theCurrentModel.arr_NoteList == nil || theCurrentModel.arr_NoteList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_NoteList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtNoteID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtNoteID.remove(at: index)
            }
            theCurrentModel.arr_NoteList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteSections(IndexSet.init(integer: indexSection), with: .fade)
            if theCurrentModel.arr_NoteList?.count == 0 {
                setBackground(strMsg: "Note List Not Available.")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtNoteID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtNoteID.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_NoteList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
//            archiveFocus(strFocusID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteNote(strNoteID: model.id)
            return
        case MoreAction.edit.rawValue:
            redirectToAddNotesAction(theModel: model)
            break
        case MoreAction.chart.rawValue:
//            presentGraphAction(strChartId: model.id)
            break
        case MoreAction.convert.rawValue:
            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    
    func deleteNote(strNoteID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtNoteID .append(strNoteID)
            self?.updateBottomCell(loadingID: strNoteID, isLoadingApi: true)
            self?.theCurrentModel.changeNoteStatusWebService(strNoteID: strNoteID, isDelete: true)
        }
    }
    
    func updateNotesModelAndUI(theModel:Notes?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_NoteList?.firstIndex(where: {$0.id == model.id }) {
            theCurrentModel.arr_NoteList![index].notes = model.notes
            let indexPath = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func presentNotesFilter() {
        let vc = NotesFilterVC.init(nibName: "NotesFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            
            if filterData.searchText != ""{
                totalFilterCount += 1
            }
            
            if filterData.createdDate != nil{
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_NoteList?.removeAll()
            self?.theCurrentModel.arr_NoteList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.theCurrentModel.notesListService(isRefreshing: true)

        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    func redirectToAddNotesAction(theModel:Notes?) {
        let vc = AddNoteVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG, isForEdit:true)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theModel) in
            self?.updateNotesModelAndUI(theModel: theModel)
        }
        self.push(vc: vc)
    }
    func redirectToAddActionScreen(theModel:Notes) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddNotesData(theNoteId:theModel.id,theNoteName: theModel.notes)
        /*vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theModel
            if let index = self?.theCurrentModel.arr_NoteList?.firstIndex(where: {$0.id == theModel.id}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentModel.arr_NoteList?.remove(at: index)
                self?.theCurrentModel.arr_NoteList?.insert(model, at: index)
                self?.theCurrentModel.apiLoadingAtFocusID = model.id
                self?.updateBottomCell()
            }
        }*/
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentNotesFilter()
    }

}

//MARK:-UITableViewDataSource
extension NotesVC:UITableViewDataSource {
    /*func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_NoteList == nil || theCurrentModel.arr_NoteList?.count == 0) {
            return 0
        } else {
            return theCurrentModel.arr_NoteList?.count ?? 10
        }
    }*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
        if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_NoteList == nil || theCurrentModel.arr_NoteList?.count == 0) {
            return 0
        } else {
            return theCurrentModel.arr_NoteList?.count ?? 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesTVCell") as! NotesTVCell
        if theCurrentModel.arr_NoteList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtNoteID.contains(theCurrentModel.arr_NoteList![indexPath.section].id) {
                isApiLoading = true
            }
            cell.configure(theModel: theCurrentModel.arr_NoteList![indexPath.row], isApiLoading: isApiLoading)
            cell.handlerMoreAction = { [weak self, unowned cell] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnArrow, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_NoteList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
        } else {
            cell.showAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr_model = theCurrentModel.arr_NoteList, arr_model.count > 0, !theCurrentModel.apiLoadingAtNoteID.contains(arr_model[indexPath.row].id) else { return }
        redirectToAddNotesAction(theModel: arr_model[indexPath.row])
    }
    
    /*func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_NoteList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtNoteID.contains(arr_model[indexPath.section].id) {
            return false
        }
        return true
    }*/
    
}
//MARK:-UITableViewDelegate
extension NotesVC:UITableViewDelegate {
    /*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_NoteList, arr_model.count > 0, !theCurrentModel.apiLoadingAtNoteID.contains(arr_model[indexPath.section].id) else { return nil }
        
        let frame = tableView.rectForRow(at: indexPath)
        let editView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: tableView.frame.size.height))
        editView.backgroundColor = appGreen
        let editImage = UIImageView(frame: CGRect(x: 20, y: frame.size.height/2 - 10, width: 18, height: 18))
        editImage.image = UIImage(named: "ic_edit_notes")!
        editView.addSubview(editImage)
        
        let editAction = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "      ") { [weak self] (action, index) in
            print("edit")
            self?.redirectToAddNotesAction(theModel: arr_model[indexPath.section])

        }
        editAction.backgroundColor = UIColor(patternImage: editView.asImage())
        
        let actionView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: tableView.frame.size.height))
        actionView.backgroundColor = appGreen
        let actionImage = UIImageView(frame: CGRect(x: 20, y: frame.size.height/2 - 10, width: 18, height: 18))
        actionImage.image = UIImage(named: "ic_actons_menu_screen")!.withRenderingMode(.alwaysTemplate)
        actionImage.tintColor = white
        actionView.addSubview(actionImage)
        
        let actionAction = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "      ") { [weak self] (action, index) in
            print("action")
            self?.redirectToAddActionScreen(theModel: arr_model[indexPath.section])
        }
        actionAction.backgroundColor = UIColor(patternImage: actionView.asImage())
        
        let deleteView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: tableView.frame.size.height))
        deleteView.backgroundColor = appGreen
        let deleteImage = UIImageView(frame: CGRect(x: 20, y: frame.size.height/2 - 9, width: 12, height: 16))
        deleteImage.image = UIImage(named: "ic_delete_notes")!
        deleteView.addSubview(deleteImage)
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "      ") { [weak self] (action, index) in
            print("delete")
            self?.deleteNote(strNoteID: arr_model[indexPath.section].id)
        }
        deleteAction.backgroundColor = UIColor(patternImage: deleteView.asImage())
        
//        let imgSize: CGSize = tableView.frame.size
//        UIGraphicsBeginImageContextWithOptions(imgSize, false, UIScreen.main.scale)
//        let context = UIGraphicsGetCurrentContext()
//        backView.layer.render(in: context!)
//        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
        
        
        
        return [deleteAction,actionAction,editAction]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let arr_model = theCurrentModel.arr_NoteList, arr_model.count > 0, !theCurrentModel.apiLoadingAtNoteID.contains(arr_model[indexPath.section].id) else { return nil }

        let edit = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completionHandler) in
            completionHandler(true)
            print("edit")
            self?.redirectToAddNotesAction(theModel: arr_model[indexPath.section])
        }
        edit.image = UIImage(named: "ic_edit_notes")
        edit.backgroundColor = appGreen
        
        let notesAction = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completionHandler) in
            completionHandler(true)
            print("action")
            self?.redirectToAddActionScreen(theModel: arr_model[indexPath.section])
        }
        notesAction.image = UIImage(named: "ic_actons_manu_notes_screen")
        notesAction.backgroundColor = appGreen
        
        let delete = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completionHandler) in
            completionHandler(true)
            print("delete")
            self?.deleteNote(strNoteID: arr_model[indexPath.section].id)
        }
        delete.image = UIImage(named: "ic_delete_notes")
        delete.backgroundColor = appGreen
        
        let configure = UISwipeActionsConfiguration(actions: [delete,notesAction,edit])
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }*/
   
}

