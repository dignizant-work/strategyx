//
//  NotesView.swift
//  StrategyX
//
//  Created by Haresh on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class NotesView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!


//    let refreshControl = UIRefreshControl()
    let disposeBag = DisposeBag()


    //MAK:- LifeCycle
    func setupLayout() {
//        onTextFieldSearchAction()
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:NotesVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(NotesTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? NotesVC)?.refreshApiCall()
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }

    
    /*//RX Action
    func onTextFieldSearchAction() {
        txtSearch.rx.text.orEmpty
            .subscribe(onNext:{ (text) in
                (self.parentContainerViewController() as? NotesVC)?.searchText(txt: text)
            }).disposed(by: disposeBag)
        
        txtSearch.rx.controlEvent(UIControlEvents.editingDidEnd)
            .subscribe(onNext: { [weak self] _ in
                let text = self?.txtSearch.text ?? ""
                (self?.parentContainerViewController() as? NotesVC)?.searchText(txt: text, endEditing:true)
            }).disposed(by: disposeBag)
        
    }*/
}
