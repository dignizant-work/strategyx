//
//  NotesTVCell.swift
//  StrategyX
//
//  Created by Haresh on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotesTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var constrainBtnArrowWidth: NSLayoutConstraint! // 0.0
    
    var handlerMoreAction:() -> Void = {}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.text = " "
        lblSubTitle.text = " "
//        btnArrow.isHidden = true
//        constrainBtnArrowWidth.constant = 0.0
        showAnimation()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func showAnimation() {
        btnArrow.isHidden = true
        [lblTitle,lblSubTitle].forEach({ $0.showAnimatedSkeleton() })
    }
    func hideAnimation() {
        btnArrow.isHidden = false
        [lblTitle,lblSubTitle].forEach({ $0?.hideSkeleton() })
    }
    
    func configure(strTitle:String,strSubtitle:String) {
        hideAnimation()
        lblTitle.text = strTitle
        lblSubTitle.text = strSubtitle
    }
    
    func configure(theModel:Notes, isApiLoading:Bool = false) {
        hideAnimation()
//        btnArrow.isHidden = !isApiLoading
//        constrainBtnArrowWidth.constant = isApiLoading ? 30.0 : 0.0
        btnArrow.loadingIndicator(isApiLoading)
        self.contentView.isUserInteractionEnabled = !isApiLoading

        
        lblTitle.text = theModel.notes
        if let created = theModel.created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType11),!created.isEmpty {
            lblSubTitle.text = "Created: " + created
        }
    }
    
    //MARK:- Action
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
}
