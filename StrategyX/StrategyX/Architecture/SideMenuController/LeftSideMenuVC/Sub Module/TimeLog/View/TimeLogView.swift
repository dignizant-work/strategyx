//
//  TimeLogView.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TimeLogView: ViewParentWithoutXIB {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblTimeLog: UITableView!
    @IBOutlet weak var viewFilter: UIView!
    
    //MARK:- Life Cycle
    
    func setupTableView(theDelegate:TimeLogVC) {
        
        [tblTimeLog].forEach { (tableView) in
            tableView?.estimatedRowHeight = 30
            tableView?.rowHeight = UITableView.automaticDimension
            tableView?.delegate = theDelegate
            tableView?.dataSource = theDelegate
            tableView?.separatorStyle = .none
        }
        
        tblTimeLog.registerCellNib(TimeLogCell.self)
        
        
    }
}
