//
//  TimeLogCell.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TimeLogCell: UITableViewCell {
    
    
    @IBOutlet weak var viewProfile: UIView!
    
    @IBOutlet weak var lblProfile: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK:- View Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strShortName:String,strTitle:String,strSubTitle:String) {
        viewProfile.backgroundColor = UIColor.random()
        viewProfile.cornerRadius = self.viewProfile.bounds.width / 2
        viewProfile.clipsToBounds = true
        lblProfile.text = strShortName
        lblTitle.text = strTitle
        lblSubTitle.text = strSubTitle
       
    }
    
}
