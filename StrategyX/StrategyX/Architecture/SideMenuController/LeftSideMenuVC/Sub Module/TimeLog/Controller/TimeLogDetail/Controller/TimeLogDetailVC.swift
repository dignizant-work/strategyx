//
//  TimeLogDetailVC.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TimeLogDetailVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TimeLogDetailView =  { [unowned self] in
        return self.view as! TimeLogDetailView
        }()
    
    fileprivate lazy var theCurrentModel:TimeLogDetailModel = {
        return TimeLogDetailModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
    }
    
}


//MARK:- UITableViewDataSource
extension TimeLogDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! FocusDetailTVCell
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configureCell1(strTitle: "Work Compensation")
            
        } else if indexPath.row == 1 { //Created By
            cell.configureCell2(strTypeName: "Date", strTypeDetail: "21 Oct 18")
            
        } /*else if indexPath.row == 2 { // Discription Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configureCell3(strDescription: "Lorem ipsum dolor sit amet, consectetur elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc.")
        }*/ else if indexPath.row >= 2 && indexPath.row <= 5 {
            if indexPath.row == 2 {
                cell.configureCell2(strTypeName: "Start Time", strTypeDetail: "01:45")
            } else if indexPath.row == 3 {
                cell.configureCell2(strTypeName: "End Time", strTypeDetail: "9:35")
            } else if indexPath.row == 4 {
                cell.configureCell2(strTypeName: "Duration", strTypeDetail: "3:45")
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
               /* cell.btnSave.isHidden = true
                cell.handlerOnBtnCloseClick = { [weak self] in
                    self?.backAction()
                }*/
                return cell
            }
        } else  { // Last Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.btnSave.isHidden = true
            cell.btnClose.backgroundColor = appGreen
            cell.handlerOnBtnCloseClick = { [weak self] in
                self?.backAction(isAnimation: false)
            }
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension TimeLogDetailVC:UITableViewDelegate {
    
}
