//
//  TimeLogVC.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TimeLogVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TimeLogView = { [unowned self] in
        return self.view as! TimeLogView
        }()
    
    fileprivate lazy var theCurrentModel:TimeLogModel = {
        return TimeLogModel(theController: self)
    }()
    
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

         setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setSideMenuLeftNavigationBarItem()
      
    }
    

    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupTableView(theDelegate: self)
    }
    
    //MARK:- Action Zone
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionFilter()
    }
    
    //MARK:- Private Zone
    
    func presentActionFilter() {
        let vc = TimeLogFilterVC.init(nibName: "TimeLogFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .timelog)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
//        if filterData.assigntoID == "0" {
//            totalFilterCount -= 1
//            totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
//        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToTimeLogDetailVC() {
          let vc = TimeLogDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
          self.navigationController?.pushViewController(vc, animated: false)
    }

}

//MARK:-UITableViewDataSource
extension TimeLogVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeLogCell") as! TimeLogCell
        cell.configure(strShortName: "AB", strTitle: "Liecence to operate business", strSubTitle: "From 9:30pm To 10:30am")
        return cell
        
    }
    
}
//MARK:-UITableViewDelegate
extension TimeLogVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        redirectToTimeLogDetailVC()
    }
}

