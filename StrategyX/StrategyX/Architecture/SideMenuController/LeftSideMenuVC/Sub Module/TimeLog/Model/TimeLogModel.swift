//
//  TimeLogModel.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TimeLogModel {
    
    //MARK:- Variable
    weak var theController:TimeLogVC!
    
    //MARK:- LifeCycle
    init(theController:TimeLogVC) {
        self.theController = theController
    }
}
