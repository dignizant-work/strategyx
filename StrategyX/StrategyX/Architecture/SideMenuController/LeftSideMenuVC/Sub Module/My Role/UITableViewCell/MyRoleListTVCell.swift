//
//  MyRoleListTVCell.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleListTVCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var viewBgType: UIView!
    @IBOutlet weak var lblType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.text = " "
        lblSubTitle.text = " "
        showAnimation()
        viewBgType.rotate(degrees: -42.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showAnimation() {
        viewBgType.isHidden = true
        [lblTitle,lblSubTitle].forEach({ $0.showAnimatedSkeleton() })
    }
    func hideAnimation() {
        viewBgType.isHidden = false
        [lblTitle,lblSubTitle].forEach({ $0?.hideSkeleton() })
    }
    
    func configure(strTitle:String,strSubtitle:String,reoccuring:String, customFrequency:String) {
        hideAnimation()
        lblTitle.text = strTitle
        lblSubTitle.text = strSubtitle
        
        switch reoccuring {
        case "1":
            lblType.text = "Daily"
            viewBgType.setBackGroundColor = 3
            break
        case "5","7":
            lblType.text = "Weekly"
            viewBgType.setBackGroundColor = 15
            break
        case "30":
            lblType.text = "Monthly"
            viewBgType.setBackGroundColor = 27
            break
        case "365":
            lblType.text = "Annual"
            viewBgType.setBackGroundColor = 24
            break
        case "0":
            switch customFrequency {
            case "1":
                lblType.text = "Daily"
                viewBgType.setBackGroundColor = 3
                break
            case "7":
                lblType.text = "Weekly"
                viewBgType.setBackGroundColor = 15
                break
            case "30":
                lblType.text = "Monthly"
                viewBgType.setBackGroundColor = 27
                break
            case "365":
                lblType.text = "Annual"
                viewBgType.setBackGroundColor = 24
                break
            default:
                lblType.text = "Annual"
                viewBgType.setBackGroundColor = 24
                break
            }
        default:
            lblType.text = "Annual"
            viewBgType.setBackGroundColor = 24
            break
        }
    }
    
}
