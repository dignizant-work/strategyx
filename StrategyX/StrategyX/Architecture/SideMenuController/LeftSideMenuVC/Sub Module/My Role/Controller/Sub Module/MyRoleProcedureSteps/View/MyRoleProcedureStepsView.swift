//
//  MyRoleProcedureStepsView.swift
//  StrategyX
//
//  Created by Haresh on 20/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleProcedureStepsView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var img_Bg: UIImageView!

    
    //MARK:- LifeCycle
    
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
    }
    
    func setupTableView(theDelegate:MyRoleProcedureStepsVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        //        tableView.registerCellNib(RisksNoteTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
}
