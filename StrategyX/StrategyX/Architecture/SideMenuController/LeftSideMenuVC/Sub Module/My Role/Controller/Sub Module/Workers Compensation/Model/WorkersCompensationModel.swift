//
//  WorkersCompensationModel.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class WorkersCompensationModel {
    
    //MARK:- Variable
    fileprivate weak var theController:WorkersCompensationVC!
    var imageBG:UIImage?
    var loadingApiForDeleteProcedureAtIndex = -1
    var loadingApiForDeletAttachmentAtIndex = -1
    
    var theMyRoleModel:MyRole? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
                    (vc as? MyRoleVC)?.updateMyRoleListModel(theModel: theMyRoleModel!)
//                    (vc as? FocusArchivedVC)?.updateFocusListModel(theFocusDetailModel: theFocusDetailModel!)
//                    (vc as? FocusSubList1VC)?.updateFocusListModel(theFocusDetailModel: theFocusDetailModel!)
                }
            }
        }
    }
    
//    var tableviewCount = 7
    
    // Selecte File Data
    
    var arr_AttachmentList:[Attachment] = []
    var arr_ProcedureList:[Procedure] = []

    
    //MARK:- LifeCycle
    init(theController:WorkersCompensationVC) {
        self.theController = theController
    }
    
}
