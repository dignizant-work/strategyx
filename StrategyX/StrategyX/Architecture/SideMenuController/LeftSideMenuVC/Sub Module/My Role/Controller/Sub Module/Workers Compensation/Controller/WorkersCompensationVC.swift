//
//  WorkersCompensationVC.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices
import ObjectMapper

class WorkersCompensationVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:WorkersCompensationView =  { [unowned self] in
        return self.view as! WorkersCompensationView
        }()
    
    fileprivate lazy var theCurrentModel:WorkersCompensationModel = {
        return WorkersCompensationModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
//        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: [.old,.new], context: nil)
//        theCurrentView.collectionSelectedFile.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
//        theCurrentView.collectionSelectedFile.removeObserver(self, forKeyPath: "contentSize")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        if keyPath == "contentSize", let coll = object as? UICollectionView {
            self.theCurrentView.heightOfCollectionSelectedFile.constant = coll.contentSize.height
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
            print("tabl.contentSize.height:=",coll.contentSize.height)
        }
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.setupCollectionView(theDelegate: self)
        
        updateViewAndModel()
    }
    func setTheData(imgBG:UIImage?,theMyRoleModel:MyRole?) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theMyRoleModel = theMyRoleModel
    }
    func updateViewAndModel() {
        guard let model = theCurrentModel.theMyRoleModel else { return }
        if model.status.lowercased() == APIKey.key_archived.lowercased() {
            theCurrentView.viewPostProcedure.isHidden = true
            theCurrentView.constrainHeightOfViewProcedure.constant = 0.0
            
            theCurrentView.viewFileName.isHidden = true
            theCurrentView.constrainheightOfViewFileName.constant = 0.0
            theCurrentView.btnPublish.isHidden = true
            theCurrentView.btnSave.isHidden = true
            theCurrentView.btnEdit.setTitle("Exit", for: .normal)
        }
        theCurrentView.lblRoleTitle.text = model.title
        theCurrentView.lblCreatedBy.text =  model.createdByName.isEmpty ? "-" : model.createdByName
        theCurrentView.lblReoccurs.text =  model.reoccursString.isEmpty ? "-" : model.reoccursString
        if let duration = Int(model.duration) {
            let hours = duration / 60
            let minute = duration % 60
            theCurrentView.lblDuration.text = "\(hours)h:\(minute)m"
        }
        
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            theCurrentView.viewCompany.isHidden = false
        } else {
            theCurrentView.viewCompany.isHidden = true
        }
        /*if UserDefault.shared.userRole != .superAdminUser {
            theCurrentView.viewCompany.isHidden = true
        }*/
    
        theCurrentView.lblCompany.text = model.companyName.isEmpty ? "-" : model.companyName
        theCurrentView.lblAssignedTo.text = model.assignedTo.isEmpty ? "-" : model.assignedTo
        theCurrentView.lblDepartment.text = model.department.isEmpty ? "-" : model.department
        theCurrentView.lblStartDate.text = "-"
        theCurrentView.lblCreatedDate.text = "-"
        theCurrentView.lblModifiedDate.text = "-"
        theCurrentView.lblStatus.text = model.status
        theCurrentModel.arr_AttachmentList = Array(model.attachment)
        theCurrentModel.arr_ProcedureList = Array(model.procedure)

        if let startDateformate = model.startDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType4),!startDateformate.isEmpty {
            theCurrentView.lblStartDate.text =  startDateformate
        }
        if let createdDateformate = model.created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType4),!createdDateformate.isEmpty {
            theCurrentView.lblCreatedDate.text =  createdDateformate
        }
        if let modifiedDateformate = model.modified.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType4),!modifiedDateformate.isEmpty {
            theCurrentView.lblModifiedDate.text =  modifiedDateformate
        }
        theCurrentView.tableView.reloadData()
        theCurrentView.collectionSelectedFile.reloadData()
    }
    func updateMyRoleModel(theModel:MyRole) {
        let model = theModel
        model.attachment.removeAll()
        model.procedure.removeAll()
        theCurrentModel.arr_AttachmentList.forEach({ model.attachment.append($0) })
        theCurrentModel.arr_ProcedureList.forEach({ model.procedure.append($0) })
        theCurrentModel.theMyRoleModel = model
    }
    func updateProcedureList(theModel:Procedure) {
        theCurrentModel.arr_ProcedureList.append(theModel)
        theCurrentView.tableView.reloadData()
        updateMyRoleModel(theModel: theCurrentModel.theMyRoleModel!)
    }
    func deleteProcedureStep(theModel:Procedure, index:Int) {
        theCurrentModel.loadingApiForDeleteProcedureAtIndex = index
        theCurrentView.tableView.isUserInteractionEnabled = false
        UIView.performWithoutAnimation {
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action:APIKey.key_remove,
                         APIKey.key_procedure_id:theModel.procedureId]

        MyRoleWebServices.shared.deleteMyRoleProcedure(parameter: parameter, success: { [weak self] in
            if let obj = self {
                obj.theCurrentModel.arr_ProcedureList.remove(at: index)
                obj.theCurrentModel.loadingApiForDeletAttachmentAtIndex = -1
                obj.theCurrentView.tableView.isUserInteractionEnabled = true
                obj.theCurrentView.tableView.reloadData()
                obj.updateMyRoleModel(theModel: obj.theCurrentModel.theMyRoleModel!)
            }

            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.theCurrentModel.loadingApiForDeletAttachmentAtIndex = -1
                self?.theCurrentView.tableView.isUserInteractionEnabled = true
                self?.theCurrentView.tableView.reloadData()
        })
    }
    
    func uploadAttachment(files:[URL]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        for url in files {
            let path = url.absoluteString
            let mimeType = MimeType(path: path).value
            _ = mimeType.components(separatedBy: "/")
//            let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
            let fileName = url.lastPathComponent
            let data = try! Data(contentsOf: url)
            fileData.append(data)
            fileMimeType.append(mimeType)
            withName.append("files[\(index)]")
            withFileName.append("\(fileName)")
            index += 1
        }
        guard let model = theCurrentModel.theMyRoleModel else { return }
        
        theCurrentView.btnAttachment.startLoadyIndicator()

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_role_id:model.id]

        
        MyRoleWebServices.shared.uploadMyRoleAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theModels,msg) in
            if let obj = self {
                
                obj.theCurrentModel.arr_AttachmentList.removeAll()
                obj.theCurrentModel.arr_AttachmentList = theModels

                obj.theCurrentView.collectionSelectedFile.reloadData()
                obj.theCurrentView.btnAttachment.stopLoadyIndicator()
                obj.updateMyRoleModel(theModel: obj.theCurrentModel.theMyRoleModel!)
                self?.showAlertAtBottom(message: msg)

            }

        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            self?.theCurrentView.btnAttachment.stopLoadyIndicator()
        })
        
    }
    
    func deleteAttachment(theModel:Attachment, index:Int) {
        
        theCurrentModel.loadingApiForDeletAttachmentAtIndex = index
        theCurrentView.collectionSelectedFile.isUserInteractionEnabled = false
        theCurrentView.collectionSelectedFile.reloadData()

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_file_id:theModel.fileId]
        
        MyRoleWebServices.shared.deleteMyRoleAttachment(parameter: parameter, success: { [weak self] in
            if let obj = self {
                obj.theCurrentModel.arr_AttachmentList.remove(at: index)
                obj.theCurrentModel.loadingApiForDeletAttachmentAtIndex = -1
                obj.theCurrentView.collectionSelectedFile.isUserInteractionEnabled = true
                obj.theCurrentView.collectionSelectedFile.reloadData()
                obj.updateMyRoleModel(theModel: obj.theCurrentModel.theMyRoleModel!)
            }
            
        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            self?.theCurrentModel.loadingApiForDeletAttachmentAtIndex = -1
            self?.theCurrentView.collectionSelectedFile.isUserInteractionEnabled = true
            self?.theCurrentView.collectionSelectedFile.reloadData()
        })
        
    }
    
    func presentFileManger() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    //MARK:- Redirection
    func redirectToMyRoleCalenderScreen() {
        let imgBG = self.view.asImage()
        
        let vc = MyRoleCalendarVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG, isForEdit: true, theModel: theCurrentModel.theMyRoleModel)
        vc.handlerClose = { 
            
        }
        vc.handlerEditData = { [weak self] (theModel) in
            self?.theCurrentModel.theMyRoleModel = theModel
            self?.updateViewAndModel()
        }
        self.push(vc: vc)
    }
    func redirectToMyRoleProcedureStepsVC(theModel:MyRole?) {
        let img = self.view.asImage()
        let vc = MyRoleProcedureStepsVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(imgBG: img, theMyRoleModel: theModel)
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnPostProcedureAction(_ sender: UIButton) {
        let text = theCurrentView.txtProcedure.text ?? ""
        
        if text.trimmed().isEmpty {
            showAlertAtBottom(message: "Please enter procedure description")
            return
        }
        theCurrentView.txtProcedure.text = nil

        guard let model = theCurrentModel.theMyRoleModel else { return }
        theCurrentView.btnPostProcedure.startLoadyIndicator()

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_my_role_id:model.id,
                         APIKey.key_procedure_text:text]
        
        MyRoleWebServices.shared.AddMyRoleProcedure(parameter: parameter, success: { [weak self] (theModel) in
            if let obj = self {
                obj.updateProcedureList(theModel:theModel)
                obj.theCurrentView.btnPostProcedure.stopLoadyIndicator()
            }
        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            self?.theCurrentView.btnPostProcedure.stopLoadyIndicator()
        })
    }
    
    @IBAction func onBtnAttachmentFileAction(_ sender: UIButton) {
        self.view.endEditing(true)
        presentFileManger()
    }
    
    @IBAction func onBtnSaveNExitAction(_ sender: UIButton) {
        guard let model = theCurrentModel.theMyRoleModel else { return }
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_myRole_id:model.id,
                         APIKey.key_status:APIKey.key_myRoleStatus_Draft]
        
        MyRoleWebServices.shared.UpdateMyRoleStatus(parameter: parameter, success: { [weak self] (msg) in
            if let obj = self {
                obj.theCurrentModel.theMyRoleModel?.status = APIKey.key_myRoleStatus_Draft
                obj.updateMyRoleModel(theModel: obj.theCurrentModel.theMyRoleModel!)
            }
            self?.showAlertAtBottom(message: msg)
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.backAction(isAnimation: false)

        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
        })
        
    }
    
    @IBAction func onBtnPublishAction(_ sender: UIButton) {
        guard let model = theCurrentModel.theMyRoleModel else { return }
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_myRole_id:model.id,
                         APIKey.key_status:APIKey.key_myRoleStatus_Active]
        
        MyRoleWebServices.shared.UpdateMyRoleStatus(parameter: parameter, success: { [weak self] (msg) in
                if let obj = self {
                    obj.theCurrentModel.theMyRoleModel?.status = APIKey.key_myRoleStatus_Active
                    obj.updateMyRoleModel(theModel: obj.theCurrentModel.theMyRoleModel!)
                }
                self?.showAlertAtBottom(message: msg)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
                self?.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
                
        })
    }
    
    @IBAction func onBtnEditAction(_ sender: Any) {
        guard let model = theCurrentModel.theMyRoleModel else { return }
        if model.status.lowercased() == APIKey.key_archived.lowercased() {
            self.backAction(isAnimation: false)
        } else {
            redirectToMyRoleCalenderScreen()
        }
    }
    
    @IBAction func onBtnProcedureAction(_ sender: Any) {
        guard theCurrentModel.theMyRoleModel != nil else {
            return
        }
        redirectToMyRoleProcedureStepsVC(theModel: theCurrentModel.theMyRoleModel)
    }
    
}

//MARK:- UITableViewDataSource
extension WorkersCompensationVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.arr_ProcedureList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProcedureStepsTVCell") as! ProcedureStepsTVCell
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        let model = theCurrentModel.arr_ProcedureList[indexPath.row]
        
        cell.configure(strStep: "Step : \(indexPath.row + 1)", strTitle:model.procedureText.html2String, isLoading: theCurrentModel.loadingApiForDeleteProcedureAtIndex == indexPath.row)
        cell.handlerOnDeleteAction = { [weak self] in
            self?.deleteProcedureStep(theModel: model, index: indexPath.row)
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension WorkersCompensationVC:UITableViewDelegate {
    
}

//MARK:- UIDocumentPickerDelegate
extension WorkersCompensationVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arr_validUrls : [URL] = []
        
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension)) {
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    arr_validUrls.append(urls[i])
                }
            } else {
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        
        if (arrayInvalidFilesFormate.count > 0) {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        } else {
            if arr_validUrls.count > 0 {
                uploadAttachment(files: arr_validUrls)
            }
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url:=",url)
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension)) {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                return
            }
            
            uploadAttachment(files: [url])
        } else {
             self.showAlertAtBottom(message: "This file is not supported \(fileName)")
        }
        controller.dismiss(animated: true, completion: nil)
        

    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}
//MARK:- UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension WorkersCompensationVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentModel.arr_AttachmentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
//        let selectFile = theCurrentModel.arr_AttachmentList[indexPath.row]
        cell.lblFileName.text = theCurrentModel.arr_AttachmentList[indexPath.row].file
        cell.btnCloseOutlet.tag = indexPath.row
        if indexPath.row == theCurrentModel.loadingApiForDeletAttachmentAtIndex {
            cell.btnCloseOutlet.loadingIndicator(true)
        } else {
            cell.btnCloseOutlet.loadingIndicator(false)
        }
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveFileAction(_:)), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentModel.arr_AttachmentList[indexPath.row].file.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentModel.arr_AttachmentList[indexPath.row].file)
        } else {
            self.presentWebViewVC(url: theCurrentModel.arr_AttachmentList[indexPath.row].file, isLocal: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width - 20
        return CGSize(width: width/2, height: 50)
    }

    @IBAction func onBtnRemoveFileAction(_ sender: UIButton) {
        deleteAttachment(theModel:theCurrentModel.arr_AttachmentList[sender.tag] , index: sender.tag)
    }
}
