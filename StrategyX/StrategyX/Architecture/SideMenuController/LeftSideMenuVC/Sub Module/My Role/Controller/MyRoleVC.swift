//
//  MyRoleVC.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:MyRoleView = { [unowned self] in
       return self.view as! MyRoleView
    }()
    
    fileprivate lazy var theCurrentModel:MyRoleModel = {
       return MyRoleModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getMyRoleListWebService()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_MyRoleList == nil || theCurrentModel.arr_MyRoleList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateMyRoleListModel(theModel:MyRole) {
        if let index = theCurrentModel.arr_MyRoleList?.firstIndex(where: {$0.id == theModel.id}) {
            theCurrentModel.arr_MyRoleList?.remove(at: index)
            theCurrentModel.arr_MyRoleList?.insert(theModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    //MARK:- Redirection
    func redirectToMyRoleArchivedVC() {
        let vc = MyRoleArchivedVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        self.push(vc: vc)
    }
    
    func redirectToMyRoleCalendarVC() {
        let vc = MyRoleCalendarVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        self.push(vc: vc)
    }
    
    func redirectToWorkersCompensationVC(theModel:MyRole) {
        let img = self.view.asImage()
        let vc = WorkersCompensationVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(imgBG: img, theMyRoleModel: theModel)
        self.push(vc: vc)
    }
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .myRole)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                // self?.theCurrentView.lblFilterCount.text = "\(totalFilterCount)"
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_MyRoleList?.removeAll()
            self?.theCurrentModel.arr_MyRoleList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getMyRoleListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        /*
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .myRole)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)*/
    }
    
    func presentGraphAction() {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnMyRoleGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        /*var title = ""
         if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
         title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
         }
         
         if let vc = theCurrentModel.subViewControllers.last {
         removeContentController(content: vc)
         theCurrentView.updateTitleText(strTitle: title)
         theCurrentModel.subViewControllers.removeLast()
         if theCurrentModel.subViewControllers.count == 1 {
         theCurrentView.backButtonRisk(isHidden: true)
         }
         print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
         }*/
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToMyRoleArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension MyRoleVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_MyRoleList == nil || theCurrentModel.arr_MyRoleList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_MyRoleList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRoleListTVCell") as! MyRoleListTVCell
        if theCurrentModel.arr_MyRoleList != nil {
            cell.configure(strTitle: theCurrentModel.arr_MyRoleList![indexPath.row].title, strSubtitle: theCurrentModel.arr_MyRoleList![indexPath.row].reoccursString, reoccuring: theCurrentModel.arr_MyRoleList![indexPath.row].reoccuring, customFrequency: theCurrentModel.arr_MyRoleList![indexPath.row].customFrequency)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getMyRoleListWebService()
            }
        } else {
            cell.showAnimation()
        }
        return cell
    }
}
//MARK:-UITableViewDelegate
extension MyRoleVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_MyRoleList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = theCurrentModel.arr_MyRoleList?[indexPath.row] else { return }
        redirectToWorkersCompensationVC(theModel: model)
    }
}

//MARK:- Api Call
extension MyRoleVC {
    func getMyRoleListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_company_id:theCurrentModel.filterData.companyID,APIKey.key_page_offset:theCurrentModel.nextOffset,APIKey.key_search:theCurrentModel.filterData.searchText] as [String : Any]
        MyRoleWebServices.shared.getAllMyRoleList(parameter: parameter, success: { [weak self] (list, nextOffset) in
                self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
                self?.theCurrentView.refreshControl.endRefreshing()
                self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
        
    }
}
