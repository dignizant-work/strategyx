//
//  WorkersCompensationTVCell.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class WorkersCompensationTVCell: UITableViewCell {
    
    //Cell1
    @IBOutlet weak var lblTitle: UILabel!
    
    //Cell2
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblTypeDetail: UILabel!
    
    //Cell3
    @IBOutlet weak var lblDescription: UILabel!
    
    //Cell4
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    var handlerOnBtnCloseClick:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell1(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configureCell2(strTypeName:String,strTypeDetail:String) {
        lblTypeName.text = ""
        lblTypeDetail.text = ""
        lblTypeName.text = strTypeName
        lblTypeDetail.text = strTypeDetail
    }
    
    func configureCell3(strDescription:String) {
        lblDescription.text = strDescription
    }
    
    func configureCell4() {
        
    }
    
    
    //MARK:-IBAction
    @IBAction func onBtnPostAction(_ sender: Any) {
    }
    
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
    }
    
    
    @IBAction func onBtnClose(_ sender: Any) {
        handlerOnBtnCloseClick()
    }
    
}
