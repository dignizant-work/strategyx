//
//  MyRoleProcedureStepsVC.swift
//  StrategyX
//
//  Created by Haresh on 20/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleProcedureStepsVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:MyRoleProcedureStepsView = { [unowned self] in
       return self.view as! MyRoleProcedureStepsView
    }()
    
    fileprivate lazy var theCurrentModel:MyRoleProcedureStepsModel = {
       return MyRoleProcedureStepsModel(theController: self)
    }()
    
    //MARK:-LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
    }
    
    func setTheData(imgBG:UIImage?,theMyRoleModel:MyRole?) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theMyRoleDetailModel = theMyRoleModel
        guard let model = theMyRoleModel else { return }
        theCurrentModel.arr_AttachmentList = Array(model.attachment)
        theCurrentModel.arr_ProcedureList = Array(model.procedure)
    }
    

}

//MARK:- UITableViewDataSource
extension MyRoleProcedureStepsVC:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return theCurrentModel.arr_ProcedureList.count == 0 ? 2 : theCurrentModel.arr_ProcedureList.count + 1
        } else {
            return theCurrentModel.arr_AttachmentList.count == 0 ? 2 : theCurrentModel.arr_AttachmentList.count + 1
        }
//        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellMsg = tableView.dequeueReusableCell(withIdentifier: "Cell0")
        if cellMsg == nil {
            cellMsg = UITableViewCell.init(style: .default, reuseIdentifier: "Cell0")
        }
        cellMsg!.contentView.backgroundColor = .clear
        cellMsg!.backgroundColor = .clear
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! FocusDetailTVCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                cell.configureCell1(strTitle: theCurrentModel.theMyRoleDetailModel?.title ?? "", isSkeltonAnimation: false)
                return cell
                
            } else if theCurrentModel.arr_ProcedureList.count == 0 {
                if let viewBG = cellMsg!.contentView.viewWithTag(100), let lbl = viewBG.viewWithTag(101) as? UILabel {
                    lbl.text = "No procedure found"
                }
                return cellMsg!
            } else  { // Discription Cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                
                cell.configureCell3(strDescriptionTitle: "Stpe \(indexPath.row)", strDescription: theCurrentModel.arr_ProcedureList[indexPath.row - 1].procedureText)
                return cell
            }
        } else {
            if theCurrentModel.arr_AttachmentList.count == 0 && indexPath.row == 0 {
                if let viewBG = cellMsg!.contentView.viewWithTag(100), let lbl = viewBG.viewWithTag(101) as? UILabel {
                    lbl.text = "No attachment found"
                }
                return cellMsg!
            } else if indexPath.row < theCurrentModel.arr_AttachmentList.count  {
                var cell = tableView.dequeueReusableCell(withIdentifier: "Cell2")
                if cell == nil {
                    cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell2")
                }
                cell!.contentView.backgroundColor = .clear
                cell!.backgroundColor = .clear
                if let viewBG = cell!.contentView.viewWithTag(100), let lbl = viewBG.viewWithTag(101) as? UILabel {
                    lbl.text = (theCurrentModel.arr_AttachmentList[indexPath.row].file as NSString).lastPathComponent
                }
                return cell!
                
            } else  { // Last Cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! FocusDetailTVCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                /*if theCurrentModel.theFocusDetailModel != nil && !theCurrentModel.isForArchive {
                 cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
                 } else if theCurrentModel.theFocusDetailModel != nil && theCurrentModel.isForArchive {
                 cell.configureCell4(btnSaveHidden: true)
                 } else {
                 cell.configureCell4(isSkeltonAnimation: true)
                 }*/
                cell.handlerOnBtnSaveClick = { [weak self] in
                    //                self?.redirectToAddFocusVC()
                    self?.backAction(isAnimation: false)
                }
                cell.handlerOnBtnCloseClick = { [weak self] in
                    self?.backAction(isAnimation: false)
                }
                return cell
            }
        }
    }
    
    
}
//MARK:-UITableViewDelegate
extension MyRoleProcedureStepsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
        
        } else {
            if theCurrentModel.arr_AttachmentList.count != 0, indexPath.row < theCurrentModel.arr_AttachmentList.count  {
                if theCurrentModel.arr_AttachmentList[indexPath.row].file.isContainAudioOrVideoFile() {
                    self.presentAudioPlayer(url: theCurrentModel.arr_AttachmentList[indexPath.row].file)
                } else {
                    presentWebViewVC(url: theCurrentModel.arr_AttachmentList[indexPath.row].file, isLocal: false)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let viewBG = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
            viewBG.backgroundColor = clear
            let viewSub = UIView(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width - 30.0, height: 20))
            viewSub.backgroundColor = white
            viewBG.addSubview(viewSub)
            return viewBG
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 20.0
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}
