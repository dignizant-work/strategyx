//
//  ProcedureStepsTVCell.swift
//  StrategyX
//
//  Created by Haresh on 28/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ProcedureStepsTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSteps: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    var handlerOnDeleteAction:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strStep:String,strTitle:String, isLoading:Bool) {
        lblSteps.text = strStep
        lblTitle.text = strTitle
        btnCancel.loadingIndicator(isLoading)
    }
    
    @IBAction func onBtnDeleteAction(_ sender: Any) {
        handlerOnDeleteAction()
    }
}
