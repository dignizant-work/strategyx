//
//  MyRoleArchivedModel.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleArchivedModel {

    // MARK:- Variable
    fileprivate weak var theController:MyRoleArchivedVC!
    var arr_MyRoleList:[MyRole]?
    var nextOffset = 0
    
    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)

    //MARK:- LifeCycle
    init(theController:MyRoleArchivedVC) {
        self.theController = theController
    }
    
    func updateList(list:[MyRole], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_MyRoleList?.removeAll()
        }
        nextOffset = offset
        if arr_MyRoleList == nil {
            arr_MyRoleList = []
        }
        list.forEach({ arr_MyRoleList?.append($0) })
        print(arr_MyRoleList!)
    }
}
