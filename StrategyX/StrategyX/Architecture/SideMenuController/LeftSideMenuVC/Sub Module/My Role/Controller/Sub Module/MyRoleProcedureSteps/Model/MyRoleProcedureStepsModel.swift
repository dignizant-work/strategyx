//
//  MyRoleProcedureStepsModel.swift
//  StrategyX
//
//  Created by Haresh on 20/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MyRoleProcedureStepsModel {

    //MARK:- Variable
    fileprivate weak var theController:MyRoleProcedureStepsVC!
    var imageBG:UIImage?

    var tableviewCount = 3
    
    var theMyRoleDetailModel:MyRole?
    var arr_AttachmentList:[Attachment] = []
    var arr_ProcedureList:[Procedure] = []
    
    
    //MARK:- LifeCycle
    init(theController:MyRoleProcedureStepsVC) {
        self.theController = theController
    }

}
