//
//  WorkersCompensationView.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class WorkersCompensationView: ViewParentWithoutXIB {
    
    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblRoleTitle: UILabel!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var lblCompany: UILabel!
    
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblAssignedTo: UILabel!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblReoccurs: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblModifiedDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var txtProcedure: UITextField!
    @IBOutlet weak var lblAttachmentFile: UILabel!

    @IBOutlet weak var btnPostProcedure: UIButton!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
    
    @IBOutlet weak var btnPublish: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewPostProcedure: UIView!
    @IBOutlet weak var constrainHeightOfViewProcedure: NSLayoutConstraint! // 30.0
    
    @IBOutlet weak var constrainheightOfViewFileName: NSLayoutConstraint! // 30.0
    //MARK:- LifeCycle
    
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
            self.viewFileName.addDashedBorder(color: appGreen)
        })
    }
    
    
    
    func setupTableView(theDelegate:WorkersCompensationVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(ProcedureStepsTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    func setupCollectionView(theDelegate:WorkersCompensationVC)  {
        collectionSelectedFile.delegate = theDelegate
        collectionSelectedFile.dataSource = theDelegate
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.reloadData()
    }
    
    
}
