//
//  SuccessFactorReportTVCell.swift
//  StrategyX
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SuccessFactorReportTVCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    var handlerDelete:() -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String) {
       lblTitle.text = strTitle

        if let date = strTitle.convert(fromformate: DateFormatterInputType.inputType2, toFormate: DateFormatterOutputType.outputType2) {
            lblTitle.text = date
        }
    }
    
    
    @IBAction func onBtnDeleteAction(_ sender: Any) {
        handlerDelete()
    }
    
}
