//
//  CriticalFcatorsTVCell.swift
//  StrategyX
//
//  Created by Haresh on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CriticalFcatorsTVCell: UITableViewCell {
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30

    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var widthBtnMore: NSLayoutConstraint!
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!
    @IBOutlet weak var constrainViewNotesWidth: NSLayoutConstraint! // 22.0
    @IBOutlet weak var constrainViewNotesLeading: NSLayoutConstraint! // 5.0
    
    var handlerMoreAction:() -> Void = {}
    var handlerActions:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [img_Profile,lblTitle,lblTotalCount,viewNotesCount].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateBackGroundColor(isCompleted:Bool = false) {
        viewBg.backgroundColor = isCompleted ? appGreen.withAlphaComponent(0.19) : nil
    }
    func hideAnimation() {
        [img_Profile,lblTitle,lblTotalCount,viewNotesCount].forEach({ $0?.hideSkeleton() })
        btnMore.isHidden = false
    }
    
    func startAnimation() {
        img_Profile.isHidden = false
        [img_Profile,lblTitle,lblTotalCount,viewNotesCount].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
        btnMore.isHidden = true
    }
    
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configure(theModel:SuccessFactorList,isApiLoading:Bool = false) {
        hideAnimation()
        btnMore.loadingIndicator(isApiLoading)
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblProfile.text = theModel.assignedTo.acronym()
        if theModel.assignedToUserImage.isEmpty && theModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID: theModel.assigneeId) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
        
        lblNotesCount.text = "\(theModel.notesCount)"
        viewNotesCount.setBackGroundColor = theModel.notesCount > 0 ? 9 : 3
        
        lblTitle.text = theModel.criticalSuccessFactorName
        lblTotalCount.text = "\(theModel.complateAction)/\(theModel.totalAction)"
    }
    
    //MARK:- IBAction
    func hideMoreButton(){
        widthBtnMore.constant = 0
        btnMore.isHidden = true
    }
    
    func hideViewNotes() {
        viewNotesCount.isHidden = true
        constrainViewNotesWidth.constant = 0
        constrainViewNotesLeading.constant = 0
    }
    
    //MARK:- IBAction
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
    
    @IBAction func onBtnActions(_ sender: Any) {
        handlerActions()
    }
   
    
}
