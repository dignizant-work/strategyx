//
//  CriticalSucessFactorsView.swift
//  StrategyX
//
//  Created by Haresh on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CriticalSucessFactorsView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var btnBackFocus: UIButton!
    @IBOutlet weak var constrainBtnBackWidth: NSLayoutConstraint!// 35.0
    
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButton(isHidden: true)
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:CriticalSucessFactorsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(CriticalFcatorsTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? CriticalSucessFactorsVC)?.getCriticalSuccessFactorsListWebService(isRefreshing: true)
    }
    
    func backButton(isHidden:Bool) {
        constrainBtnBackWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
}
