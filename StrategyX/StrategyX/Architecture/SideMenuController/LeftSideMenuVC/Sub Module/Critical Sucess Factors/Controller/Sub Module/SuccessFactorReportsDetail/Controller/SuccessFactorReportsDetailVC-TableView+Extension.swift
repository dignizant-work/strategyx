//
//  SuccessFactorReportsDetailVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:-UITableViewDataSource
extension SuccessFactorReportsDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return theCurrentViewModel.arr_ActionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TacticalProjectSubListTVCell") as! TacticalProjectSubListTVCell
        //        cell.hideViewNotes()
        cell.showMoreButtonForAction(isHidden: true)
        
        var isApiLoading = false
        if theCurrentViewModel.apiLoadingAtActionID.contains(theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId) {
            isApiLoading = true
        }
        cell.configureActionsList(theModel: theCurrentViewModel.arr_ActionsList[indexPath.row], isApiLoading: isApiLoading)
        cell.handleDueDateAction = { [weak self] in
            if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogStatus ?? "") {
                self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].duedate ?? "", dueDate: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId ?? "")
            }
        }
        cell.handlerPercentCompletedAction = { [weak self] in
            if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogStatus ?? "") {
                self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentViewModel.arr_ActionsList[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId ?? "")
            }
        }
        cell.handleWorkDateAction = { [weak self] in
            if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogStatus ?? "") {
                self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].workDate ?? "", dueDate: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId ?? "")
            }
        }
        /*cell.handlerMoreAction = { [weak self] in
            if let obj = self {
                obj.theCurrentViewModel.configureDropDown(dropDown: obj.theCurrentViewModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentViewModel.arr_ActionsList[indexPath.row])
                obj.theCurrentViewModel.moreDD.show()
            }
        }*/
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if theCurrentView.tableViewAction != tableView {
            return false
        }
        if theCurrentViewModel.apiLoadingAtActionID.contains(theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
}
//MARK:-UITableViewDelegate
extension SuccessFactorReportsDetailVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard theCurrentView.tableViewAction == tableView else { return nil }
        let arr_model = theCurrentViewModel.arr_ActionsList
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.theCurrentViewModel.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        
        guard theCurrentView.tableViewAction == tableView else { return nil }
        
        let arr_model = theCurrentViewModel.arr_ActionsList
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("delete")
                self.theCurrentViewModel.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentView.tableViewAction == tableView {
            if !theCurrentViewModel.apiLoadingAtActionID.contains(theCurrentViewModel.arr_ActionsList[indexPath.row].actionlogId) {
                theCurrentViewModel.performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            }
        } else {
            
        }
        
    }
}
