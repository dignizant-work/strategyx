//
//  SuccessFactorReportsDetailViewModel.swift
//  StrategyX
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class SuccessFactorReportsDetailViewModel {
    
    //MARK:- Variable
    fileprivate weak var theController:SuccessFactorReportsDetailVC!
    
    var imageBG:UIImage?
    var theSuccessFactorReportModel:SuccessFactorReports?
    var arr_ActionsList:[ActionsList] = []
    var arr_ReportList:[Report] = []
    var theSuccessFactorModel:SuccessFactorList? = nil {
        didSet{
            if let controller = theController.navigationController?.viewControllers {
                if let index = controller.firstIndex(where: {($0 as? CriticalSucessFactorsVC) != nil}) {
                    (controller[index] as? CriticalSucessFactorsVC)?.updateSuccessFactorListModelFromActionSubList(theSuccessFactorModel: theSuccessFactorModel!)
                }
            }
        }
    }
    var theSuccessFactorDetailModel:SuccessFactorDetail?
    var theModel:Report?
    
    var isPresentDocumentPicker = false
    
    let moreDD = DropDown()
    var apiLoadingAtActionID:[String] = []

    let dateFormate = DateFormatter()
    var selectedReportDate = Date()
    var arr_SFReportChartData:[SFReportChartList] = []
    var handlerSFreportChartWebservice:(String) -> Void = { _ in}

    var arr_selectedFile:[AttachmentFiles] = []

    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","focusname":"","duedate":""])

    //MARK:- LifeCycle
    init(theController:SuccessFactorReportsDetailVC) {
        self.theController = theController
        dateFormate.dateFormat = DateFormatterOutputType.outputType2
        (theController.view as? SuccessFactorReportsDetailView)?.viewReportDate(strDate: dateFormate.string(from: selectedReportDate))
    }
    
    func isValidFormate() -> Bool {
        dateFormate.dateFormat = DateFormatterInputType.inputType2
        let date = dateFormate.string(from: selectedReportDate)
        let dateArray = arr_ReportList.filter({$0.date == date})
        if dateArray.count == 0 {
            return true
        }
        return false
    }
    
    func updateSuccessFactorSubListUpdate() {
        if let controller = theController.navigationController?.viewControllers {
            if let index = controller.firstIndex(where: {($0 as? CriticalSuccessFactorsSubList1VC) != nil}) {
                (controller[index] as? CriticalSuccessFactorsSubList1VC)?.getAllSuccessFactorActionsListWebService(isRefreshing:true)
            }
        }
    }
    
    func updateModelAndViews(theModel:SuccessFactorReports?)  {
        guard let model = self.theModel else {
            return
        }
//        theSuccessFactorReportModel = model
        arr_ActionsList = Array(model.actionsList)
//        arr_ReportList = Array(model.report)
//        updateSuccessFactorSubListUpdate()
        selectedData = JSON()
        if let date = model.date.convert(fromformate: DateFormatterInputType.inputType2, toFormate: DateFormatterOutputType.outputType2) {
            (theController.view as? SuccessFactorReportsDetailView)?.lblReportDate.text = date
        }
        if !model.reportFile.isEmpty {
            (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.isHidden = false
            if model.reportFile.contains(".jpg") || model.reportFile.contains(".png") || model.reportFile.contains(".jpeg") || model.reportFile.contains(".bmp") {
                
                (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage.init(named: "ic_bg_plash_holder_list")
                 (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.sd_setImageLoadMultiTypeURL(url: model.reportFile, placeholder: "ic_bg_plash_holder_list")
                (theController.view as? SuccessFactorReportsDetailView)?.viewUploadReportGraph.isHidden = true
            } else {
                (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage.init(named: "ic_document_upload_report")

                (theController.view as? SuccessFactorReportsDetailView)?.viewUploadReportGraph.isHidden = true
            }
        } else {
            (theController.view as? SuccessFactorReportsDetailView)?.viewUploadReportGraph.isHidden = false
        }
        (theController.view as? SuccessFactorReportsDetailView)?.txtGoal.text = model.goal
        (theController.view as? SuccessFactorReportsDetailView)?.txtDrivers.text = model.drivers
        (theController.view as? SuccessFactorReportsDetailView)?.txtViewObjective.text = model.objective
        (theController.view as? SuccessFactorReportsDetailView)?.txrViewComment.attributedText = model.comments.html2AttributedString
        (theController.view as? SuccessFactorReportsDetailView)?.btnDeleteUploadReportGraph.isHidden = true
        selectedData["focusgoal"].stringValue = model.focusTitle
        
        (theController.view as? SuccessFactorReportsDetailView)?.focusGoalText(selectedData: selectedData)
//        (theController.view as? SuccessFactorReportsDetailView)?.clearTextAllData()
        (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadData()
//        (theController.view as? SuccessFactorReportsDetailView)?.tableViewReportTitle.reloadData()
        
        if model.sxrReportAvailable  == 1 {
            (theController.view as? SuccessFactorReportsDetailView)?.updateViewlineChartVisiblity(isHidden: false)
            (theController.view as? SuccessFactorReportsDetailView)?.isChartActivityStart(isAnimating: true)
            getReportChartData()
        }
    }
    
    
    func performDropDownAction(index:Int,item:String) {
        guard arr_ActionsList.count != 0 else { return }
        let model = arr_ActionsList[index]
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            theController.presentGraphActionList(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            //            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    
    func deleteActionLog(strActionLogID: String) {
        theController.showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        let model = arr_ActionsList
        
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            let deletedModel = model[indexRow]
            if let index = apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                apiLoadingAtActionID.remove(at: index)
            }
            arr_ActionsList.remove(at: indexRow)
            updateSuccessFactorSubListUpdate()
            UIView.performWithoutAnimation {
                (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadData()
            }
            //            (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            updateTheSuccessFactorReportFromActionList(isForUpdate: false, theActionListModel: deletedModel)
            if arr_ActionsList.count == 0 {
                theController.setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    apiLoadingAtActionID.remove(at: index)
                }
            }
            
            let contentOffset = (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset
            
            UIView.performWithoutAnimation {
                (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadRows(at: [index], with: .none)
                if let contentPoint = contentOffset {
                    (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset = contentPoint
                }
            }
        }
    }
    
    func updateTheSuccessFactorReportFromActionList(theActionDetailModel:ActionDetail? = nil,isForUpdate:Bool = false, theActionListModel:ActionsList? = nil) {
        guard arr_ActionsList.count != 0 else { return }
        
        if let model = theActionListModel {
            if let index = arr_ActionsList.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                let updatedModel = arr_ActionsList[index]
                updatedModel.actionlogTitle = model.actionlogTitle
                updatedModel.revisedDate = model.revisedDate
                updatedModel.duedate = model.duedate
                updatedModel.workDate = model.workDate
                updatedModel.assigneeId = model.assigneeId
                updatedModel.assignedTo = model.assignedTo
                updatedModel.assignedToUserImage = model.assignedToUserImage
                updatedModel.percentageCompeleted = model.percentageCompeleted
                arr_ActionsList[index] = model
                UIView.performWithoutAnimation {
                    (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
    }
    
    
    func updateNotesCount(strActionLogID:String) {
        if let index = arr_ActionsList.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            arr_ActionsList[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadRows(at: [indexRow], with: .none)
                if let offset = contentOffset {
                    (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset = offset
                }
            }
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = arr_ActionsList.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                apiLoadingAtActionID.append(strActionLogID)
            } else {
                //                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    apiLoadingAtActionID.remove(at: index)
                }
                
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        arr_ActionsList[index].revisedColor = model.revisedColor
                        arr_ActionsList[index].duedateColor = model.revisedColor
                        arr_ActionsList[index].revisedDate = model.revisedDate
                        arr_ActionsList[index].duedate = model.revisedDate
                        break
                    case .workDate:
                        arr_ActionsList[index].workdateColor = model.workdateColor
                        arr_ActionsList[index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                if let contentOffset = (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset {
                    (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.reloadRows(at: [indexPath], with: .none)
                    (theController.view as? SuccessFactorReportsDetailView)?.tableViewAction.contentOffset = contentOffset
                }
            }
        }
    }

    func updateReportAttchment(isForVideo:Bool, videoImage:UIImage?) {
        if arr_selectedFile.count != 0 {
            (theController.view as? SuccessFactorReportsDetailView)?.btnDeleteUploadReportGraph.isHidden = false

            if isForVideo, let img = videoImage {
                (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = img
            } else {
                if arr_selectedFile[0].fileName.contains(".jpg") || arr_selectedFile[0].fileName.contains(".png") || arr_selectedFile[0].fileName.contains(".jpeg") || arr_selectedFile[0].fileName.contains(".bmp") {
                    if let data = arr_selectedFile[0].fileData {
                        (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage(data: data)
                    } else {
                        (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage.init(named: "ic_document_upload_report")
                    }
                } else {
                    (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage.init(named: "ic_document_upload_report")
                }
            }
        } else {
//            (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.image = UIImage.init(named: "ic_document_upload_report")
        }
        (theController.view as? SuccessFactorReportsDetailView)?.viewUploadReportGraph.isHidden = arr_selectedFile.count != 0
        (theController.view as? SuccessFactorReportsDetailView)?.imgUploadReport.isHidden = arr_selectedFile.count == 0
    }

}

//MARK:- Api Call
extension SuccessFactorReportsDetailViewModel {

    func successFactorResportWebservice(isForUpdate:Bool, isForRemoveReport:Bool = false, reportsID:String = "", moveNextReport:Int = 0, markAsComplete:Int = 0,complete:Int = 0,add_flag:Int)  {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
//        var voiceCount = 0
//        var fileCount = 0
       
        
        
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_success_factor_id:theSuccessFactorDetailModel?.id ?? "",
                         APIKey.key_add_flag : add_flag == 0 ? "0" : "1"]
        
        
        
        if isForRemoveReport {
            parameter[APIKey.key_remove_report] = reportsID
        } else {
            if isForUpdate {
                for i in 0..<arr_selectedFile.count {
                    if arr_selectedFile[i].isDeleteAttachment {
                        removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
                    } else {
                        
                        if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                            let path = arr_selectedFile[i].fileName
                            let mimeType = MimeType(path: path).value
                            //                    let mimeExtension = mimeType.components(separatedBy: "/")
                            //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                            
                            fileData.append(arr_selectedFile[i].fileData!)
                            fileMimeType.append(mimeType)
                            withName.append("files")
                            withFileName.append(path)
                            //                    fileCount += 1
                            
                            /*if arr_selectedFile[i].fileType == .audio {
                             withName.append("voice_notes[\(voiceCount)]")
                             withFileName.append(path)
                             voiceCount += 1
                             } else {
                             
                             }*/
                            index += 1
                        }
                    }
                }
                dateFormate.dateFormat = DateFormatterInputType.inputType2
                parameter[APIKey.key_date] = dateFormate.string(from: selectedReportDate)
                parameter[APIKey.key_drivers] = selectedData["drivers"].stringValue
                parameter[APIKey.key_goal] = selectedData["goal"].stringValue
                parameter[APIKey.key_objective] = selectedData["objective"].stringValue
                parameter[APIKey.key_comments] = selectedData["comment"].stringValue
                
//                parameter[APIKey.key_remove_file] = JSON(removeFile).rawString() ?? ""
                if complete == 1 {
                    parameter[APIKey.key_complete] = "1"
                } else {
                    if moveNextReport == 1 {
                       parameter[APIKey.key_move_next_report] = "1"
                    } else {
                       parameter[APIKey.key_mark_as_complete] = "1"
                    }
                }
            }
        }
        
        Utility.shared.showActivityIndicator()
        SuccessFactorWebservice.shared.updateSuccessFactorReport(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { (theModel) in
            Utility.shared.stopActivityIndicator()
            self.updateModelAndViews(theModel: theModel)
        }, failed: { [weak self] (error) in
            Utility.shared.stopActivityIndicator()
            self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
         alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
         let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
         loadingIndicator.hidesWhenStopped = true
         loadingIndicator.style = UIActivityIndicatorView.Style.gray
         loadingIndicator.tintColor = appGreen
         loadingIndicator.color = appGreen
         loadingIndicator.startAnimating()
         
         alert.view.addSubview(loadingIndicator)
         present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            //            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.theController.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                //                alert.dismiss(animated: false, completion: nil)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
    
    func getReportChartData() {
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_success_factor_id:theSuccessFactorDetailModel?.id ?? "",
                         APIKey.key_company_id:Utility.shared.userData.companyId,
                         APIKey.key_for_completed_report:"1",
                         APIKey.key_report_id:theModel?.id ?? ""]
        
        SuccessFactorWebservice.shared.getAllSFReportChartListWebServiceList(parameter: parameter, success: { [weak self]  (list,recordName) in
            self?.arr_SFReportChartData = list
            //            self?.handlerSFreportChartWebservice("")
            (self?.theController.view as? SuccessFactorReportsDetailView)?.lblLineChartTitle.text = recordName
            (self?.theController.view as? SuccessFactorReportsDetailView)?.isChartActivityStart(isAnimating: false)
            (self?.theController.view as? SuccessFactorReportsDetailView)?.setDataCountForLinearLineChart(theModel: self?.arr_SFReportChartData ?? [])
            
            }, failed: { [weak self] (error) in
                (self?.theController.view as? SuccessFactorReportsDetailView)?.isChartActivityStart(isAnimating: false, error: error)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.handlerSFreportChartWebservice(error)
        })
        
    }
}
