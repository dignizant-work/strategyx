//
//  CriticalSuccessFactorsSubList1View.swift
//  StrategyX
//
//  Created by Haresh on 16/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CriticalSuccessFactorsSubList1View: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var btnBackSuccessFactor: UIButton!
    @IBOutlet weak var constrainBtnBackSuccessFactorWidth: NSLayoutConstraint!// 35.0
    
    @IBOutlet weak var btnGraph: UIButton!
    
    @IBOutlet weak var lblTitlePrimary: UILabel!
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    
    @IBOutlet weak var btnDetailArrow: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    //    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButtonSuccessFactor(isHidden: false)
        updateTitleText(strTitle: "Workers Compensation")
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:CriticalSuccessFactorsSubList1VC) {
        //        refreshControl.tintColor = appGreen
        //        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(TacticalProjectSubListTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? CriticalSuccessFactorsSubList1VC)?.getAllSuccessFactorActionsListWebService(isRefreshing: true)
    }
    
    func backButtonSuccessFactor(isHidden:Bool) {
        constrainBtnBackSuccessFactorWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateTitleText(strTitle:String) {
        lblTitlePrimary.text = strTitle
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }

}
