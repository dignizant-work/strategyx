//
//  CriticalSuccessFactorsSubList1VC.swift
//  StrategyX
//
//  Created by Haresh on 16/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class CriticalSuccessFactorsSubList1VC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CriticalSuccessFactorsSubList1View = { [unowned self] in
        return self.view as! CriticalSuccessFactorsSubList1View
        }()
    
    fileprivate lazy var theCurrentModel:CriticalSuccessFactorsSubList1Model = {
        return CriticalSuccessFactorsSubList1Model(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.updateTitleText(strTitle: theCurrentModel.theSuccessFactorModel?.criticalSuccessFactorName ?? "")
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getAllSuccessFactorActionsListWebService()
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func setTheData(theSuccessFactorModel:SuccessFactorList?,theSuccessFactorDetailModel:SuccessFactorDetail?, isForArchive:Bool = false) {
        theCurrentModel.theSuccessFactorModel = theSuccessFactorModel
        theCurrentModel.isForArchive = isForArchive
        theCurrentModel.theSuccessFactorDetailModel = theSuccessFactorDetailModel
    }
    
    func redirectToActionDetailScreen(theModel:ActionsList) {
        let vc = ActionDetailVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setupData(theActionId: theModel.actionlogId, isCheckEditScreen: false)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_SuccessFactorActionsList == nil || theCurrentModel.arr_SuccessFactorActionsList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_SuccessFactorActionsList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            presentGraphActionList(strChartId: model.actionlogId)
            //            presentGraphAction(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            //            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    
    func deleteActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    func archiveActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: false,isArchive: true)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_SuccessFactorActionsList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtActionID.remove(at: index)
            }
            //            theCurrentModel.apiLoadingAtActionID = "-1"
            theCurrentModel.arr_SuccessFactorActionsList?.remove(at: indexRow)
            if let model =  theCurrentModel.theSuccessFactorModel {
                model.totalAction = theCurrentModel.arr_SuccessFactorActionsList?.count ?? 0
                model.completeActionFlag = model.totalAction == model.complateAction ? (model.totalAction == 0 && model.complateAction == 0) ? 0 : 1 : 0
                theCurrentModel.theSuccessFactorModel = model
            }
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
            if theCurrentModel.arr_SuccessFactorActionsList?.count == 0 {
                setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
            }
            /*
             if !isLoadingApi || isRemove {
             theCurrentModel.apiLoadingAtActionID = "-1"
             }*/
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [index], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateSuccessFactorListModel(theSuccessFactorDetailModel:SuccessFactorDetail) {
        if theCurrentModel.theSuccessFactorModel?.id == theSuccessFactorDetailModel.id {
            let model = theCurrentModel.theSuccessFactorModel!
            model.criticalSuccessFactorName = theSuccessFactorDetailModel.criticalSuccessFactorName
//            model.revisedDate = theSuccessFactorDetailModel.revisedDate
            model.assignedTo = theSuccessFactorDetailModel.assignedTo
            model.assigneeId = theSuccessFactorDetailModel.assigneeId
            model.assignedToUserImage = theSuccessFactorDetailModel.assignedToUserImage
//            model.duedateColor = theSuccessFactorDetailModel.duedateColor
//            model.revisedColor = theSuccessFactorDetailModel.revisedColor
            theCurrentModel.theSuccessFactorModel = model
            theCurrentModel.theSuccessFactorDetailModel = theSuccessFactorDetailModel
            theCurrentView.updateTitleText(strTitle: theCurrentModel.theSuccessFactorModel?.criticalSuccessFactorName ?? "")
        }
    }
    func updateTheSuccessFactorModelFromActionList(theActionDetailModel:ActionDetail? = nil,isForUpdate:Bool = false, theActionListModel:ActionsList? = nil) {
        guard var arrModels = theCurrentModel.arr_SuccessFactorActionsList else { return }
        var theModel = theActionListModel
        var previousModelPercentageCompeleted = "-1"
        
        if let model = theActionListModel {
            if let index = theCurrentModel.arr_SuccessFactorActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                let updatedModel = theCurrentModel.arr_SuccessFactorActionsList![index]
                previousModelPercentageCompeleted = updatedModel.percentageCompeleted
                updatedModel.actionlogTitle = model.actionlogTitle
                updatedModel.revisedDate = model.revisedDate
                updatedModel.duedate = model.duedate
                updatedModel.workDate = model.workDate
                updatedModel.assigneeId = model.assigneeId
                updatedModel.assignedTo = model.assignedTo
                updatedModel.assignedToUserImage = model.assignedToUserImage
                updatedModel.percentageCompeleted = model.percentageCompeleted
                theCurrentModel.arr_SuccessFactorActionsList![index] = model
                theModel = model
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
        
        guard let model = theCurrentModel.theSuccessFactorModel else { return }
        guard let deletedActionModel = theModel else { return }
        
        //        theCurrentModel.theSuccessFactorModel = model
        arrModels = theCurrentModel.arr_SuccessFactorActionsList!
        let totalActions = arrModels.count
        var completedActions = theCurrentModel.theSuccessFactorModel?.complateAction ?? 0
        var completedActionFlag = theCurrentModel.theSuccessFactorModel?.completeActionFlag ?? 0
        
        //        let filterCompletedActions = arrModels.filter({ $0.percentageCompeleted == "100" })
        
        if isForUpdate {
            if deletedActionModel.percentageCompeleted != "100" && previousModelPercentageCompeleted == "100" {
                completedActions -= 1
            } else if deletedActionModel.percentageCompeleted == "100" && previousModelPercentageCompeleted != "100" {
                completedActions += 1
            }
        } else {
            if deletedActionModel.percentageCompeleted == "100" {
                completedActions -= 1
            }
        }
        
        completedActionFlag = (completedActions != 0 && totalActions != 0) && (completedActions == totalActions) ? 1 : 0
        
        model.totalAction = totalActions
        model.complateAction = completedActions
        model.completeActionFlag = completedActionFlag
        print("theCurrentModel.theSuccessFactorModel:=",model)
        theCurrentModel.theSuccessFactorModel = model
        
    }
    func updateActionListModel(theActionDetailModel:ActionDetail?) {
        guard let model = theActionDetailModel else { return }
        
        if let index = theCurrentModel.arr_SuccessFactorActionsList?.firstIndex(where: {$0.actionlogId == model.actionLogid}) {
            theCurrentModel.arr_SuccessFactorActionsList![index].actionlogTitle = model.actionTitle
            theCurrentModel.arr_SuccessFactorActionsList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_SuccessFactorActionsList![index].duedate = model.duedate
            theCurrentModel.arr_SuccessFactorActionsList![index].workDate = model.workDate
            theCurrentModel.arr_SuccessFactorActionsList![index].assigneeId = model.assigneeId
            theCurrentModel.arr_SuccessFactorActionsList![index].assignedTo = model.assignedTo
            theCurrentModel.arr_SuccessFactorActionsList![index].assignedToUserImage = model.assignedToUserImage

            theCurrentModel.arr_SuccessFactorActionsList![index].percentageCompeleted = model.percentageCompeleted
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_SuccessFactorActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_SuccessFactorActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_SuccessFactorActionsList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            } else {
                //                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_SuccessFactorActionsList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_SuccessFactorActionsList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_SuccessFactorActionsList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_SuccessFactorActionsList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_SuccessFactorActionsList![index].workdateColor = model.workdateColor
                        theCurrentModel.arr_SuccessFactorActionsList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_SuccessFactorActionsList?.removeAll()
            self?.theCurrentModel.arr_SuccessFactorActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getAllSuccessFactorActionsListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToAddActionVC(theModel:ActionDetail) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
            self?.updateTheSuccessFactorModelFromActionList(theActionDetailModel: theActionDetailModel, isForUpdate: true, theActionListModel: nil)
        }
        self.push(vc: vc)
    }
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_SuccessFactorActionsList?.removeAll()
            self?.theCurrentModel.arr_SuccessFactorActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getAllSuccessFactorActionsListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToSuccessFactorDetailVC() {
        let model = theCurrentModel.theSuccessFactorModel
        model?.notesCount = 0
        theCurrentModel.theSuccessFactorModel = model
        let vc = CriticalSucessFactorDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
//        vc.setTheData(theSuccessFactorModel: theCurrentModel.theSuccessFactorModel, isForArchive: theCurrentModel.isForArchive)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddSuccessFactorVC(theModel:SuccessFactorDetail?) {
        let model = theCurrentModel.theSuccessFactorModel
        model?.notesCount = 0
        theCurrentModel.theSuccessFactorModel = model
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theSuccessFactorDetailModel) in
            self?.updateSuccessFactorListModel(theSuccessFactorDetailModel: theSuccessFactorDetailModel!)
        }
        self.push(vc: vc)
    }
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionList) in
            self?.updateTheSuccessFactorModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionList)
            
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
        }
        self.push(vc: vc)
    }
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnSuccessFactorGraphAction(_ sender: Any) {
        presentGraphAction(strChartId: theCurrentModel.theSuccessFactorModel?.id ?? "")
    }
    
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        redirectToAddSuccessFactorVC(theModel: theCurrentModel.theSuccessFactorDetailModel)
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        //        presentRiskFilter()
        presentActionFilter()
    }

}
//MARK:-UITableViewDataSource
extension CriticalSuccessFactorsSubList1VC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_SuccessFactorActionsList == nil || theCurrentModel.arr_SuccessFactorActionsList?.count == 0) {
            return 0
        }
        return theCurrentModel.arr_SuccessFactorActionsList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TacticalProjectSubListTVCell") as! TacticalProjectSubListTVCell
        cell.showMoreButtonForAction(isHidden: true)
        if theCurrentModel.arr_SuccessFactorActionsList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtActionID.contains(theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].actionlogId) {
                isApiLoading = true
            }
            cell.configureActionsList(theModel: theCurrentModel.arr_SuccessFactorActionsList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_SuccessFactorActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].actionlogId ?? "")
                }
            }
            cell.handleWorkDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_SuccessFactorActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].workDate ?? "", dueDate: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_SuccessFactorActionsList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getAllSuccessFactorActionsListWebService()
            }
            
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_SuccessFactorActionsList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
    
}
//MARK:-UITableViewDelegate
extension CriticalSuccessFactorsSubList1VC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_SuccessFactorActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_SuccessFactorActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_SuccessFactorActionsList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let arr_model = theCurrentModel.arr_SuccessFactorActionsList, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            
            /*if theCurrentModel.theSuccessFactorModel?.SuccessFactorStatus.lowercased() == APIKey.key_archived.lowercased() {
             redirectToActionDetailScreen(theModel: theCurrentModel.arr_SuccessFactorActionsList![indexPath.row])
             } else {
             performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
             }*/
        }
    }
}
//MARK:- Api Call
extension CriticalSuccessFactorsSubList1VC {
    func getAllSuccessFactorActionsListWebService(isRefreshing:Bool = false) {
        guard let successFactor_id = theCurrentModel.theSuccessFactorModel?.id else {
            theCurrentView.refreshControl.endRefreshing()
            theCurrentView.tableView.reloadData()
            return
        }
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        
        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = theCurrentModel.filterData.workDate, !workDateFormate.isEmpty {
            //            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = theCurrentModel.filterData.dueDate, !dueDateFormate.isEmpty {
            //            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        /*let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
         APIKey.key_access_token:Utility.shared.userData.accessToken,
         APIKey.key_page_offset:theCurrentModel.nextOffset,
         APIKey.key_SuccessFactor_id: SuccessFactor_id,
         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,
         APIKey.key_company_id:theCurrentModel.filterData.companyID] as [String : Any]*/
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_tags:theCurrentModel.filterData.searchTag,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_workdatefilter:workDate,
                         APIKey.key_duedatefilter:dueDate,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)",
            APIKey.key_success_factor_id: successFactor_id]
        
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        
        ActionsWebService.shared.getAllActionsList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            },  failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                //                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
        
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
         alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
         let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
         loadingIndicator.hidesWhenStopped = true
         loadingIndicator.style = UIActivityIndicatorView.Style.gray
         loadingIndicator.tintColor = appGreen
         loadingIndicator.color = appGreen
         loadingIndicator.startAnimating()
         
         alert.view.addSubview(loadingIndicator)
         present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            //            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                //                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}
