//
//  CriticalSucessFactorDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CriticalSucessFactorDetailVC: ParentViewController {
    //MARK:- Variable
    fileprivate lazy var theCurrentView:CriticalSucessFactorDetailView =  { [unowned self] in
        return self.view as! CriticalSucessFactorDetailView
    }()
    
    fileprivate lazy var theCurrentModel:CriticalSucessFactorDetailModel = {
        return CriticalSucessFactorDetailModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
//        getFocusDetailWebService()
    }
    
    func setTheData(theCriticalFactorModel:Focus?,isForArchive:Bool = false) {
        theCurrentModel.theCriticalFactorModel = theCriticalFactorModel
        theCurrentModel.isForArchive = isForArchive
    }
    
    //MARK:- Redirection
    func redirectToAddCriticalSucessFactorVC() {
//        guard let theModel = theCurrentModel.theCriticalFactorDetailModel else { return }
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
//        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theFocusDetailModel) in
            self?.getFocusDetailWebService()
        }
        self.push(vc: vc)
    }
    
}

//MARK:- UITableViewDataSource
extension CriticalSucessFactorDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! FocusDetailTVCell
        cell.configureCell2(strTypeName: " ", strTypeDetail: " ", isSkeltonAnimation: true)
        cell.lblTypeDetail.setFontColor = 5
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell1(strTitle: theCurrentModel.theCriticalFactorDetailModel!.title, isSkeltonAnimation: false)
            } else {
//                cell.configureCell1(strTitle: " ", isSkeltonAnimation: true)
                cell.configureCell1(strTitle: "Critical Success Factor", isSkeltonAnimation: false)

            }
        } else if indexPath.row == 1 {
            cell.configureCell2(strTypeName: "Created date", strTypeDetail: "30 Mar 18, 2:53pm", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Created date", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                var dateDue = "-"
                if let date = theCurrentModel.theCriticalFactorDetailModel?.duedate, let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!dueDateformate.isEmpty {
                    dateDue = dueDateformate
                }
                cell.configureCell2(strTypeName: "Created date", strTypeDetail: dateDue, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Due date", strTypeDetail: "04 Jul 18")
        } else if indexPath.row == 2 { //Created By
            cell.configureCell2(strTypeName: "Created By", strTypeDetail: "Michael Johnson", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Created By", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Created By", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.createdBy.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.createdBy, isSkeltonAnimation: false)
            }
            //            cell.configureCell2(strTypeName: "Created By", strTypeDetail: "Clara Logan")
            
        } else if indexPath.row == 3 { //Company
            cell.configureCell2(strTypeName: "Company", strTypeDetail: "Outsourced Global", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Company", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Company", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.companyName.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.companyName, isSkeltonAnimation: false)
            }
            //            cell.configureCell2(strTypeName: "Created By", strTypeDetail: "Clara Logan")
            
        } else if indexPath.row == 4 {
            cell.configureCell2(strTypeName: "Assigned to", strTypeDetail: "Annie", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Assigned To", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Assigned to", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.assignedTo.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.assignedTo, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Assign To", strTypeDetail: "Julian")
        } else if indexPath.row == 5 {
            cell.configureCell2(strTypeName: "Department", strTypeDetail: "Corporate", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Department", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Department", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.department.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.department, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Department", strTypeDetail: "Operations")
        } else if indexPath.row == 6 {
            cell.configureCell2(strTypeName: "Focus Gols", strTypeDetail: "0", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Focus Gols", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Focus Gols", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.department.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.department, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Department", strTypeDetail: "Operations")
        } else if indexPath.row == 7 {
            cell.configureCell2(strTypeName: "Report Title", strTypeDetail: "report title", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Report Title", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Report Title", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.department.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.department, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Department", strTypeDetail: "Operations")
        } else if indexPath.row == 8 {
            cell.lblTypeDetail.setFontColor = 3
            cell.configureCell2(strTypeName: "Report URL", strTypeDetail: "www.reporturl.com", isSkeltonAnimation: false)
//            cell.configureCell2(strTypeName: "Report URL", strTypeDetail: " ", isSkeltonAnimation: true)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Report URL", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.department.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.department, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Department", strTypeDetail: "Operations")
        } else if indexPath.row == 9 {
//            cell.configureCell2(strTypeName: "Report Frequency", strTypeDetail: " ", isSkeltonAnimation: true)
            cell.configureCell2(strTypeName: "Report Frequency", strTypeDetail: "Weekly", isSkeltonAnimation: false)

            if theCurrentModel.theCriticalFactorDetailModel != nil {
                cell.configureCell2(strTypeName: "Report Frequency", strTypeDetail: theCurrentModel.theCriticalFactorDetailModel!.department.isEmpty ? "-" : theCurrentModel.theCriticalFactorDetailModel!.department, isSkeltonAnimation: false)
            }
//            cell.configureCell2(strTypeName: "Report Frequency", strTypeDetail: "Weekly")
        } else  { // Last Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
//            cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
            cell.configureCell4(btnSaveHidden: true)

            /* Note:- Please follow focus condition for this one
             if theCurrentModel.theFocusDetailModel != nil {
                if !UserDefault.shared.isCanEditForm(strOppoisteID: theCurrentModel.theFocusDetailModel?.createdById ?? "") || UserDefault.shared.isCanArchiveForm(strOppoisteID: theCurrentModel.theFocusDetailModel?.createdById ?? "", completionFlag: theCurrentModel.theFocusDetailModel?.focusStatus == APIKey.key_archived.lowercased() ? 1 : 0) {
                    cell.configureCell4(btnSaveHidden: true)
                } else {
                    cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
                }
            } else {
                cell.configureCell4(isSkeltonAnimation: true)
            }*/
            cell.handlerOnBtnSaveClick = { [weak self] in
                self?.redirectToAddCriticalSucessFactorVC()
            }
            cell.handlerOnBtnCloseClick = { [weak self] in
                self?.backAction(isAnimation: false)
            }
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension CriticalSucessFactorDetailVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 && UserDefault.shared.userRole != .superAdminUser {
            return CGFloat.leastNormalMagnitude
        }
        return UITableView.automaticDimension
    }
}

//MARK:- Api Call
extension CriticalSucessFactorDetailVC {
    func getFocusDetailWebService() {
        guard let focus_id = theCurrentModel.theCriticalFactorModel?.id else { return }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_focus_id:focus_id] as [String : Any]
        
        FocusWebServices.shared.getFocusDetail(parameter: parameter, success: { [weak self] (detail) in
            self?.theCurrentModel.theCriticalFactorDetailModel = detail
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                //                self?.setBackground(strMsg: error)
                self?.showAlertAtBottom(message: error)
        })
    }
}
