//
//  SuccessFactorReportsVC.swift
//  StrategyX
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices

class SuccessFactorReportsVC: ParentViewController {

    internal lazy var theCurrentView: SuccessFactorReportsView = { [unowned self] in
        return self.view as! SuccessFactorReportsView
    }()
    
    internal lazy var theCurrentViewModel: SuccessFactorReportsViewModel = {
        return SuccessFactorReportsViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        theCurrentViewModel.handlerSFreportChartWebservice = { (error) in
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.lblTitlePrimary.text = theCurrentViewModel.theSuccessFactorDetailModel?.criticalSuccessFactorName
        theCurrentView.tableViewAction.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        if let url = theCurrentViewModel.theSuccessFactorDetailModel?.reportUrl {
            theCurrentView.viewGoToReportURL.isHidden = url.trimmed().isEmpty
            theCurrentView.viewLineChartGoToReportUrl.isHidden = url.trimmed().isEmpty
        }
        if !theCurrentViewModel.isPresentDocumentPicker {
            theCurrentViewModel.successFactorResportWebservice(isForUpdate: false, add_flag: 0)
        }
        theCurrentViewModel.isPresentDocumentPicker = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tableViewAction.removeObserver(self, forKeyPath: "contentSize")
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentViewModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        print(theCurrentViewModel.theSuccessFactorDetailModel ?? "object is nil")
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentViewModel.imageBG = imgBG
    }
    
    func setBackground(strMsg:String) {
        if theCurrentViewModel.arr_ActionsList.count == 0 {
            theCurrentView.tableViewAction.backgroundView = theCurrentView.tableViewAction.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableViewAction.reloadData()
        }
    }
    func setTheData(theSuccessFactorModel:SuccessFactorList?,theSuccessFactorDetailModel:SuccessFactorDetail?) {
        theCurrentViewModel.theSuccessFactorDetailModel = theSuccessFactorDetailModel
        theCurrentViewModel.theSuccessFactorModel = theSuccessFactorModel
//        print("theSuccessFactorDetailModel:=",theSuccessFactorDetailModel ?? nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            if tabl == theCurrentView.tableViewAction  {
                self.theCurrentView.constrainTableViewActionHeight.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height + 8
            }
            self.view.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        
    }
    
    
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentViewModel.arr_ActionsList.firstIndex(where: {$0.actionlogId == model.actionlogId }) {
            theCurrentViewModel.arr_ActionsList[index] = model
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableViewAction.contentOffset
                theCurrentView.tableViewAction.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableViewAction.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentViewModel.arr_ActionsList.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentViewModel.arr_ActionsList[index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentViewModel.arr_ActionsList[index].revisedDate = model.revisedDate
            theCurrentViewModel.arr_ActionsList[index].revisedColor = model.revisedColor
            theCurrentViewModel.arr_ActionsList[index].duedate = model.revisedDate
            theCurrentViewModel.arr_ActionsList[index].duedateColor = model.duedateColor
            theCurrentViewModel.arr_ActionsList[index].requestFlag = model.requestFlag
            theCurrentViewModel.arr_ActionsList[index].approveFlag = model.approveFlag

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableViewAction.contentOffset
                theCurrentView.tableViewAction.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableViewAction.contentOffset = contentOffset
            }
        }
    }
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentViewModel.arr_ActionsList.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentViewModel.arr_ActionsList[index].percentageCompeleted = "\(percentage)"
            theCurrentViewModel.arr_ActionsList[index].approveFlag = percentage == 100 ? 1 : 0

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableViewAction.contentOffset
                theCurrentView.tableViewAction.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableViewAction.contentOffset = contentOffset
            }
        }
    }
    
    
    
    //MARK:- Redirection
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel)  in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            if let model = theActionModel {
                theModel?.duedateColor = model.duedateColor
            }
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentFileManger() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = false
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.lblTitlePrimary.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let y = UIApplication.shared.statusBarFrame.size.height + 44
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: y, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentReportSelectionList(completed:Int,notCompleted:Int) {
        let vc = ReportActionSelectionVC.init(nibName: "ReportActionSelectionVC", bundle: nil)
        vc.setTheData(comletedAction: completed, notCompletedAction: notCompleted)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handleSelection = { [weak self] (selectedIndex) in
            self?.theCurrentViewModel.successFactorResportWebservice(isForUpdate: true, moveNextReport: selectedIndex == 0 ? 1 : 0, markAsComplete: selectedIndex == 1 ? 1 : 0, complete: 0, add_flag: 1)

        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToActionEditVC(theModel:ActionDetail) {
        theCurrentViewModel.updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionList) in
            self?.theCurrentViewModel.updateTheSuccessFactorReportFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionList)
            
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theActionList) in
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionList)
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToCustomDatePicker() {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        vc.setTheDate(selectedDate: theCurrentViewModel.selectedReportDate)
        vc.handlerDate = { [weak self] (date) in
            self?.theCurrentViewModel.selectedReportDate = date
            self?.theCurrentViewModel.dateFormate.dateFormat = DateFormatterOutputType.outputType2
            self?.theCurrentView.viewReportDate(strDate: self?.theCurrentViewModel.dateFormate.string(from: date) ?? "")
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToSuccessFactorReportDetailScreen(theModel:Report) {
        let vc = SuccessFactorReportsDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheDataReport(theModel: theModel)
        vc.reportID = theCurrentViewModel.theSuccessFactorReportModel?.id ?? ""
        vc.setTheData(theSuccessFactorModel: nil, theSuccessFactorDetailModel: theCurrentViewModel.theSuccessFactorDetailModel)
        vc.setTheData(imgBG: self.view.asImage())
        self.push(vc: vc)
    }
    func redirectToAddFocusScreen() {
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setAddSuccessFactorData(isFoReportFocus: true, strReportFocusID: theCurrentViewModel.theSuccessFactorReportModel?.id ?? "", strSuccessFactorTitle:theCurrentViewModel.theSuccessFactorDetailModel?.criticalSuccessFactorName ?? "", strSuccessFactorID:theCurrentViewModel.theSuccessFactorDetailModel?.id ?? "")
        vc.setTheData(imgBG: self.view.asImage())
        self.push(vc: vc)
    }
    func redirectToFocusScreen(strReportFocusID:String) {
        let vc = FocusVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheDataForReport(isFoReportFocus: true, strReportFocusID: strReportFocusID)
        self.push(vc: vc)
    }
    func redirectToAddActionScreen(theModel:SuccessFactorDetail?) {
        theCurrentViewModel.isForAddAction = true
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddSuccessFactorData(theSuccessFactorId: theModel?.id ?? "", theSuccessFactorName: theModel?.criticalSuccessFactorName ?? "", theDueDate: "", theDueDateColor: "")
        vc.handlerEditAction = { [weak self] (action) in
            self?.theCurrentViewModel.successFactorResportWebservice(isForUpdate: false, add_flag: 0)
        }
        /*vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentViewModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id}) {
                let model = self!.theCurrentModel.arr_SuccessFactorList![index]
                //                model.duedate = strDueDate
                //                model.duedateColor = strDueDateColor
                self?.theCurrentViewModel.arr_SuccessFactorList?.remove(at: index)
                self?.theCurrentModel.arr_SuccessFactorList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }*/
        
        /*vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theModel
            if let index = self?.theCurrentViewModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentModel.arr_SuccessFactorList?.remove(at: index)
                self?.theCurrentModel.arr_SuccessFactorList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }*/
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFocusGoalAction(_ sender: Any) {
        if (theCurrentViewModel.theSuccessFactorReportModel?.focusId ?? "").isEmpty {
            redirectToAddFocusScreen()
        } else {
            if let reportFocusID = theCurrentViewModel.theSuccessFactorReportModel?.id, !reportFocusID.isEmpty {
                redirectToFocusScreen(strReportFocusID: reportFocusID)
            }
        }
    }
    @IBAction func onBtnRemoveAttachmentAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentView.viewUploadReportGraph.isHidden = false
        theCurrentView.imgUploadReport.image = nil
        theCurrentView.imgUploadReport.isHidden = true
        theCurrentView.btnDeleteUploadReportGraph.isHidden = true
        theCurrentViewModel.arr_selectedFile.removeAll()
    }
    @IBAction func onBtnBackAction(_ sender: Any) {
        theCurrentViewModel.isBackClick = true
        theCurrentViewModel.successFactorResportWebservice(isForUpdate: true, add_flag: 1)
//        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnReportDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker()
    }
    
    @IBAction func onBtnUploadReportGraphAction(_ sender: Any) {
        self.view.endEditing(true)
        
        theCurrentViewModel.isPresentDocumentPicker = true
        presentFileManger()
    }
    
    @IBAction func onBtnGoToReportURLAction(_ sender: Any) {
        self.view.endEditing(true)
        guard let strUrl = theCurrentViewModel.theSuccessFactorDetailModel?.reportUrl else {
            self.showAlertAtBottom(message: "URL is not valid")
            return
        }
        if strUrl.contains("http://") || strUrl.contains("https://") || strUrl.contains("www.") {
            UIApplication.shared.open(URL.init(string: strUrl)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.open(URL.init(string: "http://" + strUrl)!, options: [:], completionHandler: nil)
        }
    }
    
    
    @IBAction func onBtnAddAction(_ sender: Any) {
        guard let theModel = theCurrentViewModel.theSuccessFactorDetailModel else { return }
        theCurrentViewModel.isPresentDocumentPicker = true
        redirectToAddActionScreen(theModel: theModel)
    }
    
    @IBAction func onBtnCompleteAction(_ sender: Any) {
        self.view.endEditing(true)
        if !theCurrentViewModel.isValidFormate() {
            self.showAlertAtBottom(message: "Report for this date is already created. Please choose other date.")
            return
        }
        let completedCount = theCurrentViewModel.arr_ActionsList.filter({$0.percentageCompeleted == "100" })
        let notcompletedCount = theCurrentViewModel.arr_ActionsList.count - completedCount.count
        if theCurrentViewModel.arr_ActionsList.count == completedCount.count {
            theCurrentViewModel.successFactorResportWebservice(isForUpdate: true, moveNextReport: 0, markAsComplete: 0, complete: 1, add_flag: 1)
        } else {
            presentReportSelectionList(completed: completedCount.count, notCompleted: notcompletedCount)
        }
    }
    
}

//MARK:- UIDocumentPickerDelegate
extension SuccessFactorReportsVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var videoImage:UIImage?
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension)) {
                theCurrentViewModel.arr_selectedFile.removeAll()
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    }) {
                        
                    } else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                            if localFile.contains(".mp4") {
                                videoImage = self.generateThumbnail(path: URL.init(fileURLWithPath: localFile))
                            }
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            } else {
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        theCurrentViewModel.updateReportAttchment(isForVideo:videoImage != nil , videoImage: videoImage)
//        self.theCurrentView.collectionSelectedFile.reloadData()
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var videoImage:UIImage?

        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension)) {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                }) {
                    
                } else {
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                        if localFile.contains(".mp4") {
                            videoImage = self.generateThumbnail(path: URL.init(fileURLWithPath: localFile))
                        }
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        } else {
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported, \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
//        self.theCurrentView.collectionSelectedFile.reloadData()
        theCurrentViewModel.updateReportAttchment(isForVideo:videoImage != nil , videoImage: videoImage)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
