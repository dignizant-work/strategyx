//
//  SuccessFactorReportsView.swift
//  StrategyX
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SwiftyJSON
import Charts

class SuccessFactorReportsView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitlePrimary: UILabel!
    @IBOutlet weak var txtDrivers: UITextField!
    @IBOutlet weak var viewUploadReportGraph: UIView!
    @IBOutlet weak var viewGoToReportURL: UIView!
    @IBOutlet weak var imgUploadReport: UIImageView!
    @IBOutlet weak var txtGoal: UITextField!
    @IBOutlet weak var tableViewAction: UITableView!
    @IBOutlet weak var constrainTableViewActionHeight: NSLayoutConstraint! // 0.0
    @IBOutlet weak var tableViewReportTitle: UITableView!
    @IBOutlet weak var constrainTableViewReportTitleHeight: NSLayoutConstraint! // 0.0
    @IBOutlet weak var lblReportDate: UILabel!
    @IBOutlet weak var txrViewComment: UITextView!
    @IBOutlet weak var lblPlaceholderComment: UILabel!
    @IBOutlet weak var txtViewObjective: UITextView!
    @IBOutlet weak var lblPlaceholderObjective: UILabel!
    
    @IBOutlet weak var lblFocusTitle: UILabel!
    @IBOutlet weak var btnAddFocusGoal: UIButton!
    @IBOutlet weak var btnDeleteUploadReportGraph: UIButton!
    @IBOutlet weak var viewLineChart: LineChartView!

    @IBOutlet weak var chartActivity: UIActivityIndicatorView!
    @IBOutlet weak var constrainViewLineChartHeight: NSLayoutConstraint! // 290
    @IBOutlet weak var viewBGLineChart: UIView!
    @IBOutlet weak var viewUploadReport: UIView!
    @IBOutlet weak var constrainViewUploadReportHeight: NSLayoutConstraint! // 126
    @IBOutlet weak var lblLineChartError: UILabel!
    @IBOutlet weak var lblXAxisLineChart: UILabel!
    @IBOutlet weak var lblLineChartTitle: UILabel!
    @IBOutlet weak var viewLineChartGoToReportUrl: UIView!
    
    let disposeBag = DisposeBag()

    
    //MARK:- Life Cycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        viewGoToReportURL.isHidden = true
        viewLineChartGoToReportUrl.isHidden = true
        btnDeleteUploadReportGraph.isHidden = true
        updateViewlineChartVisiblity(isHidden: true)

        lblXAxisLineChart.rotate(degrees: 270)
        lblXAxisLineChart.frame = CGRect(x:5, y:viewLineChart.center.y - 180, width:20, height:50)


        setupLinearLineChart()
        //Rx Actions
        rxDriversTextFieldAction()
        rxGoalTextFieldAction()
        rxCommentTextViewDidChange()
        rxObjectiveTextViewDidChange()
    }
    
    func setupTableView(theDelegate:SuccessFactorReportsVC) {
        tableViewAction.estimatedRowHeight = 50.0
        tableViewAction.rowHeight = UITableView.automaticDimension
        tableViewAction.registerCellNib(TacticalProjectSubListTVCell.self)
        tableViewAction.delegate = theDelegate
        tableViewAction.dataSource = theDelegate
        tableViewAction.separatorStyle = .none
        
        
        tableViewReportTitle.estimatedRowHeight = 50.0
        tableViewReportTitle.rowHeight = 50.0
        tableViewReportTitle.registerCellNib(SuccessFactorReportTVCell.self)
        tableViewReportTitle.delegate = theDelegate
        tableViewReportTitle.dataSource = theDelegate
        tableViewReportTitle.separatorStyle = .none
    }
    
    func viewReportDate(strDate:String) {
        lblReportDate.text = "  " + strDate + "  "
    }
    func clearTextAllData() {
        txtDrivers.text = ""
        txtGoal.text = ""
        txrViewComment.text = ""
        txtViewObjective.text = ""
        imgUploadReport.image = nil
        btnDeleteUploadReportGraph.isHidden = true
        viewUploadReportGraph.isHidden = false
    }
    func focusGoalText(selectedData:JSON)  {
        lblFocusTitle.text = selectedData["focusgoal"].stringValue
        if !selectedData["focusgoal"].stringValue.isEmpty {
            btnAddFocusGoal.setTitle("View", for: .normal)
        } else {
            btnAddFocusGoal.setTitle("Add", for: .normal)
        }
        txtDrivers.text = selectedData["drivers"].stringValue
        txrViewComment.text = selectedData["comment"].stringValue
        txtViewObjective.text = selectedData["objective"].stringValue
    }
    
    func setupLinearLineChart() {
        viewLineChart.delegate = self
        
        viewLineChart.chartDescription?.enabled = false
        viewLineChart.setScaleEnabled(false)
        viewLineChart.pinchZoomEnabled = true
        viewLineChart.scaleXEnabled = true
        viewLineChart.doubleTapToZoomEnabled = true
        viewLineChart.autoScaleMinMaxEnabled = false
        viewLineChart.clipsToBounds = false
        viewLineChart.clipValuesToContentEnabled = false
        //viewLineChart.setVisibleXRangeMaximum(3)
        viewLineChart.extraRightOffset = 5
        viewLineChart.extraLeftOffset = 0
        viewLineChart.legend.enabled = true
        viewLineChart.extraTopOffset = 20
        viewLineChart.extraBottomOffset = 18
        // viewLineChart.drawBordersEnabled = false
        
        let l = viewLineChart.legend
        l.form = .line
        l.font = R12
        l.textColor = appPlaceHolder
        l.horizontalAlignment = .center
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.formLineWidth  = 10
        l.drawInside = true
        l.yOffset = 0
//        l.xEntrySpace = 10
        
        let xAxis = viewLineChart.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelRotationAngle = 280
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.yOffset = 20
        xAxis.axisLineColor = appSepratorColor
        xAxis.labelCount = 5
        
        
        let yAxis = viewLineChart.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = true
        yAxis.labelTextColor = appPlaceHolder
        yAxis.drawAxisLineEnabled = false
        yAxis.xOffset = 20
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        
        let rightAxis = viewLineChart.rightAxis
        rightAxis.enabled = false
    }
    
    func setDataCountForLinearLineChart(theModel:[SFReportChartList]) {
        
        //        var arrChart = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        
        /*let chartData = ChartList()
         chartData.date = ""
         chartData.totalAction = 0
         chartData.totalCompleteAction = 0
         var arrChartList = arrChart
         arrChartList.insert(chartData, at: 0)*/
        let arrChartValueData = theModel
        var yTargetVals:[ChartDataEntry] = []
        var yActualVals:[ChartDataEntry] = []
        var str : [String] = []
        for i in 0..<arrChartValueData.count{
            yTargetVals.append(ChartDataEntry(x: Double(i), y: Double(arrChartValueData[i].target) ?? 0.0))
            yActualVals.append(ChartDataEntry(x: Double(i), y: Double(arrChartValueData[i].actual) ?? 0.0))
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd MMM yy"
//            let dateChart = dateFormatter.date(from: arrChartValueData[i].date)
//
//            dateFormatter.dateFormat = "dd MMM"
            str.append(arrChartValueData[i].date)
        }
        
        
        //        var yVals2:[ChartDataEntry] = []
        //        for i in 0..<arrChart.count{
        //            yVals2.append(ChartDataEntry()
        //        }
        
        //        let set1 = LineChartDataSet(values: yVals1, label: "DataSet 1")
        //        set1.axisDependency = .left
        //        set1.setColor(appGraphCyanColor)
        //        set1.lineWidth = 2
        //        set1.circleRadius = 0
        //        set1.fillAlpha = 65/255
        //        set1.fillColor = UIColor.blue
        //        set1.highlightEnabled = false
        //        set1.mode = .linear
        
        let set2 = LineChartDataSet(values: yTargetVals, label: "Target")
        set2.axisDependency = .left
        set2.setColor(appColorSuccessFactorBlue)
        set2.lineWidth = 2
        set2.circleRadius = 0
        set2.fillAlpha = 65/255
        set2.fillColor = .red
        set2.highlightEnabled = false
        set2.mode = .cubicBezier
        
        let set3 = LineChartDataSet(values: yActualVals, label: "Actual")
        set3.axisDependency = .left
        set3.setColor(appGreen)
        set3.lineWidth = 2
        set3.circleRadius = 0
        set3.fillAlpha = 65/255
        set3.fillColor = .red
        set3.highlightEnabled = false
        set3.mode = .cubicBezier
        
        let data = LineChartData(dataSets: [set2,set3])
        data.setValueTextColor(.clear)
        data.setValueFont(R14)
        
        print(str)
        viewLineChart.xAxis.valueFormatter = DayAxisValueFormatter(chart: viewLineChart, array: str)
        viewLineChart.data = data
    }
    
    func updateViewlineChartVisiblity(isHidden:Bool) {
        viewBGLineChart.isHidden = isHidden
        constrainViewLineChartHeight.constant = isHidden ? 0 : viewBGLineChart.frame.size.width + 30
        viewUploadReport.isHidden = !isHidden
        constrainViewUploadReportHeight.constant = !isHidden ? 0 : 126
    }
    
    func isChartActivityStart(isAnimating:Bool, error:String = "") {
        lblLineChartError.isHidden = true
        if isAnimating {
            lblXAxisLineChart.isHidden = true
            lblLineChartTitle.isHidden = true
            chartActivity.startAnimating()
            viewLineChart.isHidden = true
        } else {
            chartActivity.stopAnimating()
            lblLineChartTitle.isHidden = false
            viewLineChart.isHidden = false
            lblXAxisLineChart.isHidden = false
        }
        if !error.trimmed().isEmpty {
            viewLineChart.isHidden = true
            lblLineChartError.isHidden = false
            lblXAxisLineChart.isHidden = true
            lblLineChartTitle.isHidden = true
            lblLineChartError.text = error
        }
       
    }
    
    
    //Rx
    func rxDriversTextFieldAction() {
        txtDrivers.rx.text.orEmpty
            .subscribe({ [weak self] text in
                (self?.viewNextPresentingViewController() as? SuccessFactorReportsVC)?.theCurrentViewModel.selectedData["drivers"].stringValue = text.element ?? ""
            }).disposed(by:disposeBag)
    }
    func rxGoalTextFieldAction() {
        txtGoal.rx.text.orEmpty
            .subscribe({ [weak self] text in
                (self?.viewNextPresentingViewController() as? SuccessFactorReportsVC)?.theCurrentViewModel.selectedData["goal"].stringValue = text.element ?? ""
            }).disposed(by:disposeBag)
    }
    func rxCommentTextViewDidChange() {
        txrViewComment.rx.text.orEmpty
            .subscribe({ [weak self] event in
                (self?.viewNextPresentingViewController() as? SuccessFactorReportsVC)?.theCurrentViewModel.selectedData["comment"].stringValue = event.element ?? ""
                if let obj = self {
                    obj.lblPlaceholderComment.isHidden = obj.txrViewComment.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    func rxObjectiveTextViewDidChange() {
        txtViewObjective.rx.text.orEmpty
            .subscribe({ [weak self] event in
                (self?.viewNextPresentingViewController() as? SuccessFactorReportsVC)?.theCurrentViewModel.selectedData["objective"].stringValue = event.element ?? ""
                if let obj = self {
                    obj.lblPlaceholderObjective.isHidden = obj.txtViewObjective.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
}
//MARK:- ChartViewDelegate
extension SuccessFactorReportsView : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected")
    }
}
