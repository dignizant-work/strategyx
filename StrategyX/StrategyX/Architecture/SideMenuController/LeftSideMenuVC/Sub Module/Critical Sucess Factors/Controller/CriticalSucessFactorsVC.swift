//
//  CriticalSucessFactorsVC.swift
//  StrategyX
//
//  Created by Haresh on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class CriticalSucessFactorsVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CriticalSucessFactorsView = { [unowned self] in
       return self.view as! CriticalSucessFactorsView
    }()
    fileprivate lazy var theCurrentModel:CriticalSucessFactorsModel = {
       return CriticalSucessFactorsModel.init(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        
        getCriticalSuccessFactorsListWebService()
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_SuccessFactorList == nil || theCurrentModel.arr_SuccessFactorList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateSuccessFactorListModelFromReportDetail(theSuccessFactorID:String, isDelete:Bool) {
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theSuccessFactorID}) {
            if isDelete, let totalCount = theCurrentModel.arr_SuccessFactorList?[index].totalAction {
                let newTotalCount = (totalCount - 1) < 0 ? 0 : (totalCount - 1)
                theCurrentModel.arr_SuccessFactorList?[index].totalAction = newTotalCount
                if let completedCount = theCurrentModel.arr_SuccessFactorList?[index].complateAction {
                    theCurrentModel.arr_SuccessFactorList?[index].completeActionFlag = (completedCount == newTotalCount && (completedCount != 0 && newTotalCount != 0)) ? 1 : 0
                }
            }
//            theCurrentModel.arr_SuccessFactorList?.remove(at: index)
//            theCurrentModel.arr_SuccessFactorList?.insert(theSuccessFactorModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateSuccessFactorListModelFromActionSubList(theSuccessFactorModel:SuccessFactorList) {
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theSuccessFactorModel.id}) {
            theCurrentModel.arr_SuccessFactorList?.remove(at: index)
            theCurrentModel.arr_SuccessFactorList?.insert(theSuccessFactorModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateSucessFactorListModel(theSuccessFactorDetailModel:SuccessFactorDetail) {
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theSuccessFactorDetailModel.id}) {
            theCurrentModel.arr_SuccessFactorList![index].criticalSuccessFactorName = theSuccessFactorDetailModel.criticalSuccessFactorName
//            theCurrentModel.arr_SuccessFactorList![index].revisedDate = theSuccessFactorDetailModel.revisedDate
            theCurrentModel.arr_SuccessFactorList![index].assignedTo = theSuccessFactorDetailModel.assignedTo
            theCurrentModel.arr_SuccessFactorList![index].assigneeId = theSuccessFactorDetailModel.assigneeId
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_SuccessFactorList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
//            archiveSuccessFactor(strSuccessFactorID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteSuccessFactor(strSuccessFactorID: model.id)
            return
        case MoreAction.edit.rawValue:
            getSuccessFactorDetailWebService(strSuccessFactorID: model.id, isEdit: true)
            break
        case MoreAction.chart.rawValue:
            presentGraphAction(strChartId: model.id)
            break
        case MoreAction.action.rawValue:
            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    func deleteSuccessFactor(strSuccessFactorID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtSuccessFactorID.append(strSuccessFactorID)
            self?.updateBottomCell(loadingID: strSuccessFactorID, isLoadingApi: true)
            self?.changeSuccessFactorStatusWebService(strSuccessFactorID: strSuccessFactorID, isDelete: true)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_SuccessFactorList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtSuccessFactorID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtSuccessFactorID.remove(at: index)
            }
            theCurrentModel.arr_SuccessFactorList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
            //            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_SuccessFactorList?.count == 0 {
                setBackground(strMsg: "Success factor list not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtSuccessFactorID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtSuccessFactorID.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateSuccessFactorModelAndUI(theModel:SuccessFactorDetail?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == model.id }) {
//            theCurrentModel.arr_SuccessFactorList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_SuccessFactorList![index].criticalSuccessFactorName = model.criticalSuccessFactorName
            theCurrentModel.arr_SuccessFactorList![index].assigneeId = model.assigneeId
            theCurrentModel.arr_SuccessFactorList![index].assignedTo = model.assignedTo
            theCurrentModel.arr_SuccessFactorList![index].assignedToUserImage = model.assignedToUserImage
            
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func redirectToCriticalSuccessFectorDetailVC() {
        let vc = CriticalSucessFactorDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
//        vc.setTheData(theSuccessFactorModel: theCurrentModel.theSuccessFactorModel)
        self.navigationController?.pushViewController(vc, animated: false)
    }
   
    func redirectToCriticalSuccessFactorSubList1VC(theSuccessFactorID:String,theSucessFactorsDetailModel:SuccessFactorDetail?) {
        guard let theModel = theCurrentModel.arr_SuccessFactorList?.first(where: {$0.id == theSuccessFactorID }) else {
            return
        }
        let vc = CriticalSuccessFactorsSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theSuccessFactorModel: theModel, theSuccessFactorDetailModel: theSucessFactorsDetailModel)
        self.push(vc: vc)
    }
    
    func presentCriticalSuccessFilter() {
        let vc = CriticalSuccessFilterVC.init(nibName: "CriticalSuccessFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .successFactor)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        var filterData = theCurrentModel.filterData
        if theCurrentModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
//            self?.theCurrentModel.nextOffset = 0
            if filterData.createdByID != ""{
                totalFilterCount += 1
            }
            if filterData.searchByCSFText.trimmed() != ""{
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentModel.filterData.assigntoID = "0"
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.searchByReportTitleText.trimmed() != ""{
                totalFilterCount += 1
            }
            if filterData.reportFrequencyID != ""{
                totalFilterCount += 1
            }
            if filterData.metricID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_SuccessFactorList?.removeAll()
            self?.theCurrentModel.arr_SuccessFactorList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getCriticalSuccessFactorsListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToAddCriticalSucessFactorVC(theModel:SuccessFactorDetail) {
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id }) {
            let model = theCurrentModel.arr_SuccessFactorList![index]
            model.notesCount = 0
            updateSuccessFactorListModelFromActionSubList(theSuccessFactorModel: model)
        }
        //        guard let theModel = theCurrentModel.theSuccessFactorDetailModel else { return }
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theSuccessFactorDetailModel) in
            self?.updateSuccessFactorModelAndUI(theModel: theSuccessFactorDetailModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToSucessFactorDetailVC(theModel:SuccessFactorDetail) {
        if let index = theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id }) {
            let model = theCurrentModel.arr_SuccessFactorList![index]
            model.notesCount = 0
            updateSuccessFactorListModelFromActionSubList(theSuccessFactorModel: model)
        }
        //        guard let theModel = theCurrentModel.theSuccessFactorDetailModel else { return }
        let vc = SuccessFactorDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theSuccessFactorDetailModel) in
            self?.updateSuccessFactorModelAndUI(theModel: theSuccessFactorDetailModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToAddActionScreen(theModel:SuccessFactorList) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddSuccessFactorData(theSuccessFactorId: theModel.id, theSuccessFactorName: theModel.criticalSuccessFactorName, theDueDate: "", theDueDateColor: "")
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id}) {
                let model = self!.theCurrentModel.arr_SuccessFactorList![index]
//                model.duedate = strDueDate
//                model.duedateColor = strDueDateColor
                self?.theCurrentModel.arr_SuccessFactorList?.remove(at: index)
                self?.theCurrentModel.arr_SuccessFactorList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theModel
            if let index = self?.theCurrentModel.arr_SuccessFactorList?.firstIndex(where: {$0.id == theModel.id}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentModel.arr_SuccessFactorList?.remove(at: index)
                self?.theCurrentModel.arr_SuccessFactorList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }
        self.push(vc: vc)
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
//        presentRiskFilter()
        presentCriticalSuccessFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension CriticalSucessFactorsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_SuccessFactorList == nil || theCurrentModel.arr_SuccessFactorList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_SuccessFactorList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CriticalFcatorsTVCell") as! CriticalFcatorsTVCell
//        cell.configure(strTitle: "Managed Services Subscription Revenue")
        if theCurrentModel.arr_SuccessFactorList != nil {
            cell.updateBackGroundColor(isCompleted: theCurrentModel.arr_SuccessFactorList![indexPath.row].completeActionFlag == 1)
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtSuccessFactorID.contains(theCurrentModel.arr_SuccessFactorList![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configure(theModel: theCurrentModel.arr_SuccessFactorList![indexPath.row], isApiLoading: isApiLoading)
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_SuccessFactorList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getCriticalSuccessFactorsListWebService()
            }
        } else {
            cell.startAnimation()
        }

        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension CriticalSucessFactorsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_SuccessFactorList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let arr_model = theCurrentModel.arr_SuccessFactorList, arr_model.count > 0, !theCurrentModel.apiLoadingAtSuccessFactorID.contains(arr_model[indexPath.row].id) else { return  }
        getSuccessFactorDetailWebService(strSuccessFactorID: arr_model[indexPath.row].id, isEdit: true)
    }
}

//MARK:- Api Call
extension CriticalSucessFactorsVC {
    
    func getCriticalSuccessFactorsListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:theCurrentModel.nextOffset,APIKey.key_critical_success_factor:theCurrentModel.filterData.searchByCSFText, APIKey.key_department_id:theCurrentModel.filterData.departmentID,APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,APIKey.key_company_id:theCurrentModel.filterData.companyID] as [String : Any]
        
        SuccessFactorWebservice.shared.getAllSuccessFactorList(parameter: parameter, success: { [weak self] (list, nextOffset,assignToUserFlag) in
            if assignToUserFlag == 1 && (self?.theCurrentModel.nextOffset ?? -1) == 0 { // Need to set filter count
                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.theCurrentModel.filterData.assigntoID = Utility.shared.userData.id
                self?.theCurrentModel.assignToUserFlag = assignToUserFlag
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                //            self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    func getSuccessFactorDetailWebService(strSuccessFactorID:String, isEdit:Bool) {
        
        Utility.shared.showActivityIndicator()
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_success_factor_id:strSuccessFactorID] as [String : Any]
        
        SuccessFactorWebservice.shared.getSuccessFactorDetail(parameter: parameter, success: { [weak self] (detail) in
            //                alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            if isEdit {
                self?.redirectToSucessFactorDetailVC(theModel: detail)
            } else {
                self?.redirectToCriticalSuccessFactorSubList1VC(theSuccessFactorID: strSuccessFactorID, theSucessFactorsDetailModel: detail)
            }
            }, failed: { [weak self] (error) in
                //                alert.dismiss(animated: false, completion: nil)
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    func changeSuccessFactorStatusWebService(strSuccessFactorID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strSuccessFactorID]]).rawString() else {
            updateBottomCell(loadingID: strSuccessFactorID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_critical_success_factors,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strSuccessFactorID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strSuccessFactorID)
        })
    }
    
}
