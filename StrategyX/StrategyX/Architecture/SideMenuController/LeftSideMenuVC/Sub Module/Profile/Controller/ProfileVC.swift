//
//  ProfileVC.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ProfileVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:ProfileView = { [unowned self] in
       return self.view as! ProfileView
    }()
    
    fileprivate lazy var theCurrentModel:ProfileModel = {
        return ProfileModel(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }

    
    
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        getUserDetailView()
//        setUserProfileData()
    }
    
    func setUserProfileData() {
        theCurrentView.img_Profile.sd_setImageLoadMultiTypeURL(url: Utility.shared.userData.image, placeholder: "ic_big_plash_holder")
//        sd_setImage(with:  URL(string: Utility.shared.userData.image), placeholderImage: UIImage.init(named: "ic_big_plash_holder"))

        theCurrentView.lblUserName.text = Utility.shared.userData.firstname + " " + Utility.shared.userData.lastname
        theCurrentView.lblEmail.text = Utility.shared.userData.email
        theCurrentView.lblMobile.text = Utility.shared.userData.mobile
        theCurrentView.lblDepartMent.text = Utility.shared.userData.departmentId
        
    }
    
    func updateViewDetail() {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        theCurrentView.img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.image, placeholder: "ic_big_plash_holder")
        //        sd_setImage(with: URL.init(string: theModel.userImage), placeholderImage: UIImage.init(named: "ic_big_plash_holder"))
        theCurrentView.lblUserName.text = theModel.username
        theCurrentView.lblDepartMent.text = theModel.departmentName
        theCurrentView.lblEmail.text = theModel.email
        theCurrentView.lblMobile.text = theModel.mobile
        
    }
    
    //MARK:- IBAction
    @IBAction func onBtnEditProfileAction(_ sender: Any) {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        let vc = AddUserVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setupEditProfileData(type: .editProfile, theModel: theModel)
        vc.handlerEditUserProfileData = {[weak self]  in
            self?.getUserDetailView()
        }
        self.push(vc: vc)
    }
  
}

//MARK:- Call API
extension ProfileVC{
    
    func getUserDetailView()
    {
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_view_user_id: Utility.shared.userData.id,
                         ]
        AddUserWebService.shared.getViewUserProfile(parameter: parameter, success: { [weak self] (detail) in
            self?.theCurrentModel.theUserDetailModel = detail
            self?.updateViewDetail()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
}


