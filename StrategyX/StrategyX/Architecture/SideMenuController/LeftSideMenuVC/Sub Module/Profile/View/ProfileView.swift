//
//  ProfileView.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ProfileView: ViewParentWithoutXIB {

    //Variable

    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var lblDepartMent: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnEditProfileOutlet: UIButton!
    
    func setupLayout() {
        
    }
    
}
