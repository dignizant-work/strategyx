//
//  FocusView.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FocusView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var btnBackFocus: UIButton!
    @IBOutlet weak var constrainBtnBackFocusWidth: NSLayoutConstraint!// 35.0
    
    @IBOutlet weak var btnGraph: UIButton!
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var viewArchived: UIView!
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButtonFocus(isHidden: true)
//        updateTitleText(strTitle: "Unwanted Events")
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:FocusVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(RisksSubListTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? FocusVC)?.getFocusListWebService(isRefreshing: true)
    }
    
    func backButtonFocus(isHidden:Bool) {
        constrainBtnBackFocusWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)

        }
        self.layoutIfNeeded()
    }

}
