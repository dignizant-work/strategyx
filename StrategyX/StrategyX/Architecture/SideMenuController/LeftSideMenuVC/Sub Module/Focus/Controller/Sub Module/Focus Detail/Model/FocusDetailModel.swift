//
//  FocusDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FocusDetailModel {

    //MARK:- Variable
    fileprivate weak var theController:FocusDetailVC!
    
//    var tableviewCount = 9
    var theFocusModel:Focus?
    var isForArchive = false
//    var tableviewCellType:[AddFocusModel.celltype] = [.header,.createdBy,.description,.company,.assignTo,.department,.dueDate,.reviseDate,.bottom]

    var theFocusDetailModel:FocusDetail? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
//                    (vc as? FocusVC)?.updateFocusListModel(theFocusDetailModel: theFocusDetailModel!)
//                    (vc as? FocusArchivedVC)?.updateFocusListModel(theFocusDetailModel: theFocusDetailModel!)
                    (vc as? FocusSubList1VC)?.updateFocusListModel(theFocusDetailModel: theFocusDetailModel!)
                }
            }
        }
    }
    
    
    
    //MARK:- LifeCycle
    init(theController:FocusDetailVC) {
        self.theController = theController
    }

}
