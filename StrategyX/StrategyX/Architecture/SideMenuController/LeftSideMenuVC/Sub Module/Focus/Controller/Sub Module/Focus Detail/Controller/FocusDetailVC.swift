//
//  FocusDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FocusDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FocusDetailView =  { [unowned self] in
        return self.view as! FocusDetailView
    }()
    
    fileprivate lazy var theCurrentModel:FocusDetailModel = {
       return FocusDetailModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getFocusDetailWebService()
    }
    
    func setTheData(theFocusModel:Focus?,isForArchive:Bool = false) {
        theCurrentModel.theFocusModel = theFocusModel
        theCurrentModel.isForArchive = isForArchive
    }
    
    func updateUI() {
       /* if (theCurrentModel.theFocusDetailModel == nil || theCurrentModel.theFocusDetailModel!.description_focus.isEmpty) {
            if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == .description}) {
                theCurrentModel.tableviewCellType.remove(at: index)
            }
        }*/
        if UserDefault.shared.userRole != .superAdminUser {
//            if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == .company}) {
//                theCurrentModel.tableviewCellType.remove(at: index)
//            }
        }
        
    }
    
    //MARK:- Redirection
    func redirectToAddFocusVC() {
        guard let theModel = theCurrentModel.theFocusDetailModel else { return }
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theFocusDetailModel) in
            self?.getFocusDetailWebService()
        }
        self.push(vc: vc)
    }
    
}

//MARK:- UITableViewDataSource
extension FocusDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
//        return theCurrentModel.tableviewCellType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! FocusDetailTVCell
        cell.configureCell2(strTypeName: " ", strTypeDetail: " ", isSkeltonAnimation: true)
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
       /* if AddFocusModel.celltype.header == theCurrentModel.tableviewCellType[indexPath.row] {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell1(strTitle: theCurrentModel.theFocusDetailModel!.title, isSkeltonAnimation: false)
            } else {
                cell.configureCell1(strTitle: " ", isSkeltonAnimation: true)
            }
        } else if AddFocusModel.celltype.createdBy == theCurrentModel.tableviewCellType[indexPath.row] { //Created By
            cell.configureCell2(strTypeName: "Created By", strTypeDetail: " ", isSkeltonAnimation: true)
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell2(strTypeName: "Created By", strTypeDetail: theCurrentModel.theFocusDetailModel!.createdBy, isSkeltonAnimation: false)
            }
//            cell.configureCell2(strTypeName: "Created By", strTypeDetail: "Clara Logan")
            
        } else if AddFocusModel.celltype.description == theCurrentModel.tableviewCellType[indexPath.row] { // Discription Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell3(strDescription: theCurrentModel.theFocusDetailModel!.description_focus, isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescription: " ", isSkeltonAnimation: true)
            }
        } else if AddFocusModel.celltype.company == theCurrentModel.tableviewCellType[indexPath.row] {
            cell.configureCell2(strTypeName: "Company", strTypeDetail: " ", isSkeltonAnimation: true)
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell2(strTypeName: "Company", strTypeDetail: theCurrentModel.theFocusDetailModel!.companyName.isEmpty ? "-" : theCurrentModel.theFocusDetailModel!.companyName , isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Company", strTypeDetail: "Huawei")
        } else if AddFocusModel.celltype.assignTo == theCurrentModel.tableviewCellType[indexPath.row] {
            cell.configureCell2(strTypeName: "Assign To", strTypeDetail: " ", isSkeltonAnimation: true)
            
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell2(strTypeName: "Assign To", strTypeDetail: theCurrentModel.theFocusDetailModel!.assignedTo.isEmpty ? "-" : theCurrentModel.theFocusDetailModel!.assignedTo, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Assign To", strTypeDetail: "Julian")
        } else if AddFocusModel.celltype.department == theCurrentModel.tableviewCellType[indexPath.row] {
            cell.configureCell2(strTypeName: "Department", strTypeDetail: " ", isSkeltonAnimation: true)
            
            if theCurrentModel.theFocusDetailModel != nil {
                cell.configureCell2(strTypeName: "Department", strTypeDetail: theCurrentModel.theFocusDetailModel!.department.isEmpty ? "-" : theCurrentModel.theFocusDetailModel!.department, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Department", strTypeDetail: "Operations")
        } else if AddFocusModel.celltype.dueDate == theCurrentModel.tableviewCellType[indexPath.row] {
            cell.configureCell2(strTypeName: "Due date", strTypeDetail: " ", isSkeltonAnimation: true)
            
            if theCurrentModel.theFocusDetailModel != nil {
                var dateDue = "-"
                if let date = theCurrentModel.theFocusDetailModel?.duedate, let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!dueDateformate.isEmpty {
                    dateDue = dueDateformate
                }
                cell.configureCell2(strTypeName: "Due date", strTypeDetail: dateDue, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Due date", strTypeDetail: "04 Jul 18")
        } else if AddFocusModel.celltype.reviseDate == theCurrentModel.tableviewCellType[indexPath.row] {
            cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: " ", isSkeltonAnimation: true)
            
            if theCurrentModel.theFocusDetailModel != nil {
                var dateDue = "-"
                if let date = theCurrentModel.theFocusDetailModel?.revisedDate, let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!dueDateformate.isEmpty {
                    dateDue = dueDateformate
                }
                cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: dateDue, isSkeltonAnimation: false)
            }
            //                cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: "05 Oct 18")
        } else  { // Last Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theFocusDetailModel != nil {
                if !UserDefault.shared.isCanEditForm(strOppoisteID: theCurrentModel.theFocusDetailModel?.createdById ?? "") || UserDefault.shared.isCanArchiveForm(strOppoisteID: theCurrentModel.theFocusDetailModel?.createdById ?? "", completionFlag: theCurrentModel.theFocusDetailModel?.focusStatus == APIKey.key_archived.lowercased() ? 1 : 0) {
                    cell.configureCell4(btnSaveHidden: true)
                    cell.configureCell4(strBtnCloseTitle: "Exit",btnSaveHidden: true)
                } else {
                    cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
                }
            } else {
                cell.configureCell4(isSkeltonAnimation: true)
            }
            
            /*if theCurrentModel.theFocusDetailModel != nil && !theCurrentModel.isForArchive {
                cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
            } else if theCurrentModel.theFocusDetailModel != nil && theCurrentModel.isForArchive {
                cell.configureCell4(btnSaveHidden: true)
            } else {
                cell.configureCell4(isSkeltonAnimation: true)
            }*/
            cell.handlerOnBtnSaveClick = { [weak self] in
                self?.redirectToAddFocusVC()
            }
            cell.handlerOnBtnCloseClick = { [weak self] in
                 self?.backAction(isAnimation: false)
            }
        }*/
        
        return cell
    }
}
//MARK:-UITableViewDelegate
extension FocusDetailVC:UITableViewDelegate {
    
}

//MARK:- Api Call
extension FocusDetailVC {
    func getFocusDetailWebService() {
        guard let focus_id = theCurrentModel.theFocusModel?.id else { return }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_focus_id:focus_id] as [String : Any]
        
        FocusWebServices.shared.getFocusDetail(parameter: parameter, success: { [weak self] (detail) in
                self?.theCurrentModel.theFocusDetailModel = detail
                self?.updateUI()
                self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
//                self?.setBackground(strMsg: error)
                self?.showAlertAtBottom(message: error)
        })
    }
}

