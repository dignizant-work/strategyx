//
//  FocusVC.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class FocusVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FocusView = { [unowned self] in
       return self.view as! FocusView
    }()
    
    fileprivate lazy var theCurrentModel:FocusModel = {
       return FocusModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        if theCurrentModel.isForReportFocus {
            theCurrentView.btnGraph.isHidden = true
            theCurrentView.viewArchived.isHidden = true
            theCurrentView.backButtonFocus(isHidden: false)
        }
        getFocusListWebService()
    }
    func setTheDataForReport(isFoReportFocus:Bool,strReportFocusID:String) {
//        theCurrentViewModel.imageBG = imgBG
        theCurrentModel.isForReportFocus = isFoReportFocus
        theCurrentModel.reportFocousID = strReportFocusID
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_FocusList == nil || theCurrentModel.arr_FocusList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    func updateFocusListModelFromActionSubList(theFocusModel:Focus) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theFocusModel.id}) {
            theCurrentModel.arr_FocusList?.remove(at: index)
            theCurrentModel.arr_FocusList?.insert(theFocusModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateFocusListModel(theFocusDetailModel:FocusDetail) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theFocusDetailModel.id}) {
            theCurrentModel.arr_FocusList![index].focusTitle = theFocusDetailModel.title
            theCurrentModel.arr_FocusList![index].revisedDate = theFocusDetailModel.revisedDate
            theCurrentModel.arr_FocusList![index].assignedTo = theFocusDetailModel.assignedTo
            theCurrentModel.arr_FocusList![index].assigneeId = theFocusDetailModel.assigneeId
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_FocusList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            archiveFocus(strFocusID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteFocus(strFocusID: model.id)
            return
        case MoreAction.edit.rawValue:
            getFocusDetailWebService(strFocusID: model.id, isEdit: true)
            break
        case MoreAction.chart.rawValue:
            presentGraphAction(strChartId: model.id)
            break
        case MoreAction.action.rawValue:
            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    func deleteFocus(strFocusID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtFocusID.append(strFocusID)
            self?.updateBottomCell(loadingID: strFocusID, isLoadingApi: true)
            self?.changeFocusStatusWebService(strFocusID: strFocusID, isDelete: true)
        }
    }
    func archiveFocus(strFocusID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtFocusID.append(strFocusID)
            self?.updateBottomCell(loadingID: strFocusID, isLoadingApi: true)
            self?.changeFocusStatusWebService(strFocusID: strFocusID, isDelete: false,isArchive: true)
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_FocusList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtFocusID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtFocusID.remove(at: index)
            }
            theCurrentModel.arr_FocusList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_FocusList?.count == 0 {
                setBackground(strMsg: "Focus not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtFocusID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtFocusID.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateFocusModelAndUI(theModel:FocusDetail?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == model.id }) {
            theCurrentModel.arr_FocusList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_FocusList![index].focusTitle = model.title
            theCurrentModel.arr_FocusList![index].duedate = model.duedate
            theCurrentModel.arr_FocusList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_FocusList![index].duedateColor = model.duedateColor
            theCurrentModel.arr_FocusList![index].revisedColor = model.revisedColor
            theCurrentModel.arr_FocusList![index].assigneeId = model.assigneeId
            theCurrentModel.arr_FocusList![index].assignedTo = model.assignedTo
            theCurrentModel.arr_FocusList![index].assignedToUserImage = model.assignedToUserImage

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtFocusID.append(strActionLogID)
            } else {
                if let index = theCurrentModel.apiLoadingAtFocusID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtFocusID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_FocusList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_FocusList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_FocusList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_FocusList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_FocusList![index].workdateColor = model.workdateColor
//                        theCurrentModel.arr_FocusList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func redirectToFocusArchivedVC() {
        let vc = FocusArchivedVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        self.push(vc: vc)
    }
    
    func redirectToFocusSubList1VC(theFocusID:String,theFocusDetailModel:FocusDetail?) {
        guard let theModel = theCurrentModel.arr_FocusList?.first(where: {$0.id == theFocusID }) else {
            return
        }
        let vc = FocusSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theFocusModel: theModel, theFocusDetailModel: theFocusDetailModel)
        self.push(vc: vc)
    }
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        var filterData = theCurrentModel.filterData
        if theCurrentModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentModel.filterData.assigntoID = "0"
            }
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_FocusList?.removeAll()
            self?.theCurrentModel.arr_FocusList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getFocusListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToAddFocusVC(theModel:FocusDetail) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theModel.id }) {
            let model = theCurrentModel.arr_FocusList![index]
            model.notesCount = 0
            updateFocusListModelFromActionSubList(theFocusModel: model)
        }
//        guard let theModel = theCurrentModel.theFocusDetailModel else { return }
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theFocusDetailModel) in
            self?.updateFocusModelAndUI(theModel: theFocusDetailModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToAddActionScreen(theModel:Focus) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddFocusData(theFocusId: theModel.id, theFocusName: theModel.focusTitle, theDueDate: theModel.duedate, theDueDateColor: theModel.duedateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theModel.focusStatus))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theModel.id}) {
                let model = self!.theCurrentModel.arr_FocusList![index]
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.theCurrentModel.arr_FocusList?.remove(at: index)
                self?.theCurrentModel.arr_FocusList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theModel
            if let index = self?.theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theModel.id}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentModel.arr_FocusList?.remove(at: index)
                self?.theCurrentModel.arr_FocusList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.id)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFocusGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToFocusArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension FocusVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_FocusList == nil || theCurrentModel.arr_FocusList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_FocusList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RisksSubListTVCell") as! RisksSubListTVCell
        if theCurrentModel.arr_FocusList != nil {
            cell.updateBackGroundColor(isCompleted: theCurrentModel.arr_FocusList![indexPath.row].completeActionFlag == 1)
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtFocusID.contains(theCurrentModel.arr_FocusList![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configureFocus(theModel: theCurrentModel.arr_FocusList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_FocusList?[indexPath.row].focusStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_FocusList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_FocusList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_FocusList![indexPath.row].id ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_FocusList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getFocusListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension FocusVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_FocusList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard let arr_model = theCurrentModel.arr_FocusList, arr_model.count > 0, !theCurrentModel.apiLoadingAtFocusID.contains(arr_model[indexPath.row].id) else { return  }
        getFocusDetailWebService(strFocusID: arr_model[indexPath.row].id, isEdit: false)

//        redirectToFocusSubList1VC(theModel: arr_model[indexPath.row])

        /*
        if let arr_model = theCurrentModel.arr_FocusList, arr_model.count > 0,, !theCurrentModel.apiLoadingAtNoteID.contains(arr_model[indexPath.section].id) {
        }*/
        
    }
}

//MARK:- Api Call
extension FocusVC {
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_focuses,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
        }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
    func getFocusListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:theCurrentModel.nextOffset,APIKey.key_department_id:theCurrentModel.filterData.departmentID,APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,APIKey.key_company_id:theCurrentModel.filterData.companyID] as [String : Any]
        if theCurrentModel.isForReportFocus {
            parameter[APIKey.key_success_factor_report] = theCurrentModel.reportFocousID
        }
        FocusWebServices.shared.getAllFocusList(parameter: parameter, success: { [weak self] (list, nextOffset,assignToUserFlag) in
            if assignToUserFlag == 1 && (self?.theCurrentModel.nextOffset ?? -1) == 0 { // Need to set filter count
                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.theCurrentModel.filterData.assigntoID = Utility.shared.userData.id
                self?.theCurrentModel.assignToUserFlag = assignToUserFlag
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
        }, failed: { [weak self] (error) in
            self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
            self?.setBackground(strMsg: error)
//            self?.showAlertAtBottom(message: error)
            self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    func getFocusDetailWebService(strFocusID:String, isEdit:Bool) {
        
//        var alert = UIAlertController()
//        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.gray
//        loadingIndicator.tintColor = appGreen
//        loadingIndicator.color = appGreen
//        loadingIndicator.startAnimating()
//
//        alert.view.addSubview(loadingIndicator)
//        present(alert, animated: false, completion: nil)
        
        Utility.shared.showActivityIndicator()
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_focus_id:strFocusID] as [String : Any]
        
        FocusWebServices.shared.getFocusDetail(parameter: parameter, success: { [weak self] (detail) in
//                alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            if isEdit {
                self?.redirectToAddFocusVC(theModel: detail)
            } else {
                self?.redirectToFocusSubList1VC(theFocusID: strFocusID, theFocusDetailModel: detail)
            }
            }, failed: { [weak self] (error) in
//                alert.dismiss(animated: false, completion: nil)
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    func changeFocusStatusWebService(strFocusID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strFocusID]]).rawString() else {
            updateBottomCell(loadingID: strFocusID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_focuses,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strFocusID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strFocusID)
        })
    }
}
