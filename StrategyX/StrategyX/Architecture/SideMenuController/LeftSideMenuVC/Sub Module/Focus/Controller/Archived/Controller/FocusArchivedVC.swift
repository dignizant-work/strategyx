//
//  FocusArchivedVC.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class FocusArchivedVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FocusArchivedView = { [unowned self] in
       return self.view as! FocusArchivedView
    }()
    
    fileprivate lazy var theCurrentModel:FocusArchivedModel = {
       return FocusArchivedModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getFocusListWebService()
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_FocusList == nil || theCurrentModel.arr_FocusList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    func updateFocusListModelFromActionSubList(theFocusModel:Focus) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theFocusModel.id}) {
            theCurrentModel.arr_FocusList?.remove(at: index)
            theCurrentModel.arr_FocusList?.insert(theFocusModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func updateFocusListModel(theFocusDetailModel:FocusDetail) {
        if let index = theCurrentModel.arr_FocusList?.firstIndex(where: {$0.id == theFocusDetailModel.id}) {
            theCurrentModel.arr_FocusList![index].focusTitle = theFocusDetailModel.title
            theCurrentModel.arr_FocusList![index].revisedDate = theFocusDetailModel.revisedDate
            theCurrentModel.arr_FocusList![index].assignedTo = theFocusDetailModel.assignedTo
            theCurrentModel.arr_FocusList![index].assigneeId = theFocusDetailModel.assigneeId
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_FocusList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            archiveFocus(strFocusID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteFocus(strFocusID: model.id)
            return
        case MoreAction.edit.rawValue:
//            getFocusDetailWebService(strFocusID: model.id)
            break
        case MoreAction.chart.rawValue:
            presentGraphAction(strChartId: model.id)
            break
        case MoreAction.action.rawValue:
//            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    func deleteFocus(strFocusID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtFocusID.append(strFocusID)
            self?.updateBottomCell(loadingID: strFocusID, isLoadingApi: true)
            self?.changeFocusStatusWebService(strFocusID: strFocusID, isDelete: true)
        }
    }
    func archiveFocus(strFocusID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtFocusID.append(strFocusID)
            self?.updateBottomCell(loadingID: strFocusID, isLoadingApi: true)
            self?.changeFocusStatusWebService(strFocusID: strFocusID, isDelete: false,isArchive: true)
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_FocusList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtFocusID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtFocusID.remove(at: index)
            }
            theCurrentModel.arr_FocusList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_FocusList?.count == 0 {
                setBackground(strMsg: "Focus not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtFocusID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtFocusID.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    //MARK:- Redirection
    func redirectToFocusSubList1VC(theFocusID:String,theFocusDetailModel:FocusDetail?) {
        guard let theModel = theCurrentModel.arr_FocusList?.first(where: {$0.id == theFocusID }) else {
            return
        }
        let vc = FocusSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theFocusModel: theModel, theFocusDetailModel: theFocusDetailModel, isForArchive: true)
        self.push(vc: vc)
    }
   
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                // self?.theCurrentView.lblFilterCount.text = "\(totalFilterCount)"
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_FocusList?.removeAll()
            self?.theCurrentModel.arr_FocusList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getFocusListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .focus)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnFocusArchivedGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension FocusArchivedVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_FocusList == nil || theCurrentModel.arr_FocusList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_FocusList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RisksSubListTVCell") as! RisksSubListTVCell
        if theCurrentModel.arr_FocusList != nil {
//            cell.configureFocus(theModel: theCurrentModel.arr_FocusList![indexPath.row])
//            cell.hideMoreButton()
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtFocusID.contains(theCurrentModel.arr_FocusList![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configureFocus(theModel: theCurrentModel.arr_FocusList![indexPath.row], isApiLoading: isApiLoading)
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_FocusList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getFocusListWebService()
            }
            
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension FocusArchivedVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_FocusList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_FocusList != nil {
            getFocusDetailWebService(strFocusID: theCurrentModel.arr_FocusList![indexPath.row].id, isEdit: false)

//            redirectToFocusSubList1VC(theModel: theCurrentModel.arr_FocusList![indexPath.row])
        }
    }
}

//MARK:- Api Call
extension FocusArchivedVC {
    func getFocusListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:theCurrentModel.nextOffset,APIKey.key_grid_type:APIKey.key_archived,APIKey.key_company_id:theCurrentModel.filterData.companyID,APIKey.key_department_id:theCurrentModel.filterData.departmentID,APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID] as [String : Any]
        
        FocusWebServices.shared.getAllFocusList(parameter: parameter, success: { [weak self] (list, nextOffset,assignToUserFlag)  in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getFocusDetailWebService(strFocusID:String, isEdit:Bool) {
        
//        var alert = UIAlertController()
//        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.gray
//        loadingIndicator.tintColor = appGreen
//        loadingIndicator.color = appGreen
//        loadingIndicator.startAnimating()
//
//        alert.view.addSubview(loadingIndicator)
//        present(alert, animated: false, completion: nil)
        Utility.shared.showActivityIndicator()
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_focus_id:strFocusID] as [String : Any]
        
        FocusWebServices.shared.getFocusDetail(parameter: parameter, success: { [weak self] (detail) in
//            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
                self?.redirectToFocusSubList1VC(theFocusID: strFocusID, theFocusDetailModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeFocusStatusWebService(strFocusID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strFocusID]]).rawString() else {
            updateBottomCell(loadingID: strFocusID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_focuses,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strFocusID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strFocusID)
        })
    }
}
