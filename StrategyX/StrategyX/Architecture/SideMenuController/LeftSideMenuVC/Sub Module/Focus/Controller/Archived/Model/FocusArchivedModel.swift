//
//  FocusArchivedModel.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class FocusArchivedModel {

    //MARK:- Variable
    fileprivate weak var theController:FocusArchivedVC!
    
    var arr_FocusList:[Focus]?
    var nextOffset = 0
    let moreDD = DropDown()
    var apiLoadingAtFocusID:[String] = []

    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)

    
    //MARK:- LifeCycle
    init(theController:FocusArchivedVC) {
        self.theController = theController
    }
    
    func updateList(list:[Focus], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_FocusList?.removeAll()
        }
        nextOffset = offset
        if arr_FocusList == nil {
            arr_FocusList = []
        }
        list.forEach({ arr_FocusList?.append($0) })
        print(arr_FocusList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:Focus) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_delete_icon_popup"]
        
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById)  {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
}
