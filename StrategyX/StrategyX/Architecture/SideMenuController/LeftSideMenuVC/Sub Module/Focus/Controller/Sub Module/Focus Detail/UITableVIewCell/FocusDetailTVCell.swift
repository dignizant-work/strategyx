//
//  FocusDetailTVCell.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FocusDetailTVCell: UITableViewCell {

    //Cell1
    @IBOutlet weak var lblTitle: UILabel!
    
    //Cell2
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblTypeDetail: UILabel!
    
    //Cell3
    
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgBottomLine: UIImageView!
    
    //Cell4
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    var handlerOnBtnSaveClick:() -> Void = {}
    var handlerOnBtnCloseClick:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func configureCell1(strTitle:String, isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation {
            lblTitle.text = " "
            lblTitle.showAnimatedSkeleton()
        } else {
            lblTitle.text = strTitle
            lblTitle.hideSkeleton()
        }
    }
    
    func configureCell2(strTypeName:String,strTypeDetail:String,isSkeltonAnimation:Bool = false) {
        lblTypeName.text = strTypeName

        if isSkeltonAnimation {
//            lblTypeName.text = " "
            lblTypeDetail.text = " "
            [lblTypeDetail].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            lblTypeDetail.text = strTypeDetail
            [lblTypeDetail].forEach({ $0?.hideSkeleton() })
        }
    }
    
    func configureCell3(strDescriptionTitle:String = "Description",isHideBottomLine:Bool = false, strDescription:String,isSkeltonAnimation:Bool = false) {
        lblDescriptionTitle.text = strDescriptionTitle
        imgBottomLine.isHidden = isHideBottomLine
        if isSkeltonAnimation {
            lblDescription.text = " "
//            lblDescriptionTitle.text = " "
            [lblDescription].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            [lblDescription].forEach({ $0?.hideSkeleton() })
            lblDescription.text = strDescription.isEmpty ? "-" : strDescription
        }
        
    }
    
    func configureCell4(strBtnSaveTitle:String = "Save",strBtnCloseTitle:String = "Close",isSkeltonAnimation:Bool = false, btnSaveHidden:Bool = false,btnCloseHidden:Bool = false) {
        btnSave.isHidden = true // for temperory
        btnClose.isHidden = btnCloseHidden

        btnSave.setTitle(strBtnSaveTitle, for: .normal)
        btnClose.setTitle(strBtnCloseTitle, for: .normal)
        if isSkeltonAnimation {
            [btnSave,btnClose].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            [btnSave,btnClose].forEach({ $0?.hideSkeleton() })
        }
    }
    
    
    //MARK:-IBAction
    @IBAction func onBtnPostAction(_ sender: Any) {
        
    }
    
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        handlerOnBtnSaveClick()
    }
    
    
    @IBAction func onBtnClose(_ sender: Any) {
        handlerOnBtnCloseClick()
    }

}
