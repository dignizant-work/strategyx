//
//  FocusDetailView.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FocusDetailView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- LifeCycle
    
    func setupLayout() {
        
        
    }
    
    func setupTableView(theDelegate:FocusDetailVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.registerCellNib(RisksNoteTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }

}
