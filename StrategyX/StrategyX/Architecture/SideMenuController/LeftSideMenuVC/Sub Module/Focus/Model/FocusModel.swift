//
//  FocusModel.swift
//  StrategyX
//
//  Created by Haresh on 22/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class FocusModel {

    //MARK:-Variable
    fileprivate weak var theController:FocusVC!
    
    var arr_FocusList:[Focus]?
    var nextOffset = 0

    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)
    let moreDD = DropDown()
    var apiLoadingAtFocusID:[String] = []
    var assignToUserFlag = -1
    var isForReportFocus = false
    var reportFocousID = ""
    
    
    
    
    init(theController:FocusVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:Focus) {
//        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue,MoreAction.action.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue, MoreAction.archived.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_plus_icon_popup", "ic_edit_icon_popup", "ic_delete_icon_popup", "ic_achive_icon"]
        
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById)  {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        if !UserDefault.shared.isCanArchiveForm(strOppoisteID: theModel.createdById, completionFlag: theModel.completeActionFlag) {
            dataSource.removeLast() // archive
            dataSourceIcon.removeLast()
        }

        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
    
    func updateList(list:[Focus], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_FocusList?.removeAll()
        }
        nextOffset = offset
        if arr_FocusList == nil {
            arr_FocusList = []
        }
        list.forEach({ arr_FocusList?.append($0) })
        print(arr_FocusList!)
    }

}
