//
//  ArchivedView.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RisksArchivedView:ViewParentWithoutXIB {

    //MARK:- Variabel

    @IBOutlet weak var btnBackArchived: UIButton!
    @IBOutlet weak var constrainBtnBackArchivedWidth: NSLayoutConstraint!// 35.0
    
    @IBOutlet weak var btnGraph: UIButton!
    
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButtonArchived(isHidden: false)
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:RisksArchivedVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(RisksListTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
       (self.parentContainerViewController() as? RisksArchivedVC)?.archivedService(isRefreshing: true)
    }
    
    func backButtonArchived(isHidden:Bool) {
        constrainBtnBackArchivedWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
        self.layoutIfNeeded()
    }
}
