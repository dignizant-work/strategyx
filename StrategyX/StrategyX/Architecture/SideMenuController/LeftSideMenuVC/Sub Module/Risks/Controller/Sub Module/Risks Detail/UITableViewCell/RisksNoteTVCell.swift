//
//  RisksNoteTVCell.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage

class RisksNoteTVCell: UITableViewCell {

    @IBOutlet weak var btnMoreOutlet: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNorecordfound: UILabel!
    @IBOutlet weak var imgProfileLeading: NSLayoutConstraint! //20
    
    @IBOutlet weak var constrainViewTrailing: NSLayoutConstraint! // 15.0
    @IBOutlet weak var constrainViewLeading: NSLayoutConstraint! // 15.0
    var onHandlerBtnMore:(Int) -> Void = {_ in}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strDescription:String,name:String,date:String,img:String) {
        lblDate.text = date
        lblName.text = name
        lblDescription.text = strDescription
        imgProfile.sd_setImageLoadMultiTypeURL(url: img, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with: URL(string: img), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"), options: .lowPriority, progress:nil, completed: nil)
        
    }
    
    //MARK:-IBAction
    @IBAction func onBtnMoreAction(_ sender: UIButton) {
        onHandlerBtnMore(sender.tag)
    }
    
}
