//
//  ArchivedModel.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RisksArchivedModel {

    //MARK:- Variable
    fileprivate weak var theController:RisksArchivedVC!
    var pageOffset:Int = 0
    
    var filterData = FilterData.init()
    var apiLoadingAtRiskIndex:[String] = []

    init(theController:RisksArchivedVC) {
        self.theController = theController
    }
    
    // get category
    
    var arr_archivedList:[Risk]?
    
    func updateList(list:[Risk], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_archivedList?.removeAll()
        }
        pageOffset = offset
        if arr_archivedList == nil {
            arr_archivedList = []
        }
        list.forEach({ arr_archivedList?.append($0) })
        print(arr_archivedList!)
    }
}
