//
//  RisksDetailTVCell.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SkeletonView
import Loady

class RisksDetailTVCell: UITableViewCell {

    //Cell1
    @IBOutlet weak var lblTitle: UILabel!
    
    //Cell2
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblTypeDetail: UILabel!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var viewLevel: UIView!
    @IBOutlet weak var btnEditOutlet: UIButton!
    
    //Cell3
    @IBOutlet weak var lblDescription: UILabel!
    
    //Cell4
    @IBOutlet weak var txtAddNote: UITextField!
    @IBOutlet weak var btnPost: LoadyButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var viewTxtAddNote: UIView!
//    @IBOutlet weak var constrainViewTxtAddNoteHeight: NSLayoutConstraint! // 42

    
    //MARK:- Variable declaration
    
    var handlerOnBtnCloseClick:() -> Void = {}
    var handlerOnBtnSaveClick:() -> Void = { }
    var handlerText:(String) -> Void = {_ in}
    let disposeBag = DisposeBag()
    var handlerOnBtnAddPostClick:() -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell1(strTitle:String,isSkeltonAnimation:Bool = false) {
        if isSkeltonAnimation {
            lblTitle.text = " "
            lblTitle.showAnimatedSkeleton()
        } else {
            lblTitle.text = strTitle
            lblTitle.hideSkeleton()
        }
    }
    
    func configureCell2(strTypeName:String,strTypeDetail:String,isForLevel:Bool = false,isForNotes:Bool = false,isSkeltonAnimation:Bool = false,levelBgColor:UIColor = .clear) {
        if isSkeltonAnimation {
           // lblTypeName.text = " "
            lblTypeName.text = strTypeName
            lblTypeDetail.text = " "
            viewLevel.isHidden = !isForLevel
            [lblTypeDetail].forEach({ $0?.showAnimatedSkeleton() })
        } else {
           // lblTypeName.text = ""
            lblTypeName.text = strTypeName
            lblTypeDetail.text = ""
            viewLevel.isHidden = !isForLevel
            if isForLevel {
                viewLevel.backgroundColor = levelBgColor
                lblLevel.text = strTypeDetail
            } else if isForNotes {
                lblTypeName.text = strTypeName
                lblTypeDetail.text = strTypeDetail
            } else {
                lblTypeName.text = strTypeName
                lblTypeDetail.text = strTypeDetail
            }
            [lblTypeDetail].forEach({ $0?.hideSkeleton() })
        }
        
    }
    
    func configureCell3(strDescription:String,isSkeltonAnimation:Bool = false) {        
        if isSkeltonAnimation {
            lblDescription.text = " "
            [lblDescription].forEach({ $0?.showAnimatedSkeleton() })
        } else {
            [lblDescription].forEach({ $0?.hideSkeleton() })
            lblDescription.text = strDescription
        }
    }
    
    func configureCell4(isLoading:Bool = false) {
        rxTextFieldDidChange()
        if isLoading {
            btnPost.startLoadyIndicator()
            self.contentView.allButtons(isEnable: false)
        } else {
            btnPost.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }
    }
    
    func configureCell5(isLoading:Bool = false) {
        rxTextFieldDidChange()
        if isLoading {
            btnPost.startLoadyIndicator()
//            self.contentView.allButtons(isEnable: false)
        } else {
            btnPost.stopLoadyIndicator()
//            self.contentView.allButtons(isEnable: true)
        }
    }
    
    func rxTextFieldDidChange() {
        txtAddNote.rx.controlEvent([.editingDidBegin, .editingDidEnd,.editingChanged])
            .asObservable()
            .subscribe(onNext: { _ in
               // print("editing state changed")
                self.handlerText(self.txtAddNote.text ?? "")
            })
            .disposed(by: disposeBag)
    }
    
    
    
    //MARK:-IBAction
    @IBAction func onBtnPostAction(_ sender: Any) {
        handlerOnBtnAddPostClick()
    }
    

    @IBAction func onBtnSaveAction(_ sender: Any) {
        handlerOnBtnSaveClick()
    }
    
    
    @IBAction func onBtnClose(_ sender: Any) {
        handlerOnBtnCloseClick()
    }
    
    @IBAction func onBtnEditAction(_ sender:UIButton)
    {
        
    }
    
}
