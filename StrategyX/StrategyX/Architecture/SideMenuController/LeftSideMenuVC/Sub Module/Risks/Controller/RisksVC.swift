//
//  RisksVC.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class RisksVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:RisksView = { [unowned self] in
       return self.view as! RisksView
    }()
    
    fileprivate lazy var theCurrentModel:RisksModel = {
        return RisksModel(theController: self)
    }()
    
    //MARK:- Variabled Declaration
    
    let moreDD = DropDown()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        riskService(isRefreshing: true)
//        displayContentController(content: subListViewController(step: .step0, title: "Unwanted Events"))
    }
    
    func updateRiskListModel(theRiskDetailModel:RiskDetail) {
        if let index = theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == theRiskDetailModel.risk_id}) {
            theCurrentModel.arr_riskList![index].unwanted_event = theRiskDetailModel.unwanted_event
            theCurrentModel.arr_riskList![index].assignee_id = theRiskDetailModel.assignee_id
            theCurrentModel.arr_riskList![index].assigned_to = theRiskDetailModel.assigned_to
            theCurrentModel.arr_riskList![index].assigned_to_user_image = theRiskDetailModel.assigned_to_user_image
            theCurrentModel.arr_riskList![index].duedateColor = theRiskDetailModel.duedateColor
            theCurrentModel.arr_riskList![index].revisedColor = theRiskDetailModel.revisedColor
            theCurrentModel.arr_riskList![index].revisedDate = theRiskDetailModel.revisedDate
            
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
//            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_riskList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.risk_id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtRiskIndex.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtRiskIndex.remove(at: index)
            }
            theCurrentModel.arr_riskList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_riskList?.count == 0 {
                setBackground(strMsg: "Risk not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtRiskIndex.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtRiskIndex.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateRiskListModelFromActionSubList(theRiskModel:Risk) {
        if let index = theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == theRiskModel.risk_id}) {
            theCurrentModel.arr_riskList?.remove(at: index)
            theCurrentModel.arr_riskList?.insert(theRiskModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtRiskIndex.append(strActionLogID)
            } else {
                if let index = theCurrentModel.apiLoadingAtRiskIndex.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtRiskIndex.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_riskList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_riskList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_riskList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_riskList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_riskList![index].workdateColor = model.workdateColor
                        //                        theCurrentModel.arr_riskList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirect To
    func redirectToArchivedVC() {
        let vc = RisksArchivedVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        self.push(vc: vc)
    }
    
    func redirectToRisksSublist1VC(theRiskID:String, theRiskDetail:RiskDetail?) {
        guard let theModel = theCurrentModel.arr_riskList?.first(where: {$0.risk_id == theRiskID }) else {
            return
        }
        let vc = RisksSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theRiskModel: theModel, theRiskDetailModel: theRiskDetail)
        self.push(vc: vc)
    }
   
    
    func redirectToAddRiskVC(theModel:RiskDetail) {
        if let index = theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == theModel.risk_id }) {
            let model = theCurrentModel.arr_riskList![index]
            model.notesCount = 0
            updateRiskListModelFromActionSubList(theRiskModel: model)
        }
        
        let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theDetailModel) in
//            self?.theCurrentModel.isEditRisk = true
            if let theModel = theDetailModel {
                self?.updateRiskListModel(theRiskDetailModel: theModel)
            }
//            self?.getRiskDetailView(risk_id: theModel.risk_id)
        }
        self.push(vc: vc)
    }
    func redirectToAddActionScreen(theRiskModel:Risk) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setAddRiskData(theRiskId: theRiskModel.risk_id, theRiskName: theRiskModel.unwanted_event, theDueDate: theRiskModel.duedate, theDueDateColor: theRiskModel.duedateColor, isStatusArchive: UserDefault.shared.isArchiveAction(strStatus: theRiskModel.risk_status))
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == theRiskModel.risk_id}) {
                let model = self!.theCurrentModel.arr_riskList![index]
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.theCurrentModel.arr_riskList?.remove(at: index)
                self?.theCurrentModel.arr_riskList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.risk_id)
            }
        }
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            let model = theRiskModel
            
            if let index = self?.theCurrentModel.arr_riskList?.firstIndex(where: {$0.risk_id == theRiskModel.risk_id}) {
                model.completeActionFlag = 0
                model.total_action = model.total_action + 1
                self?.theCurrentModel.arr_riskList?.remove(at: index)
                self?.theCurrentModel.arr_riskList?.insert(model, at: index)
                self?.updateBottomCell(loadingID: model.risk_id)
            }
        }
        self.push(vc: vc)
    }
    
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .risks)       
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        var filterData = theCurrentModel.filterData
        if theCurrentModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            self?.theCurrentModel.pageOffset = 0
            if filterData.searchText != ""{
                totalFilterCount += 1
            }
            if filterData.categoryID != ""{
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentModel.filterData.assigntoID = "0"
            }
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
               self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_riskList?.removeAll()
            self?.theCurrentModel.arr_riskList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.riskService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphAction(strChartId:String) {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .risk)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnRisksGraphAction(_ sender: Any) {
        presentGraphAction(strChartId: "")
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        /*var title = ""
        if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
            title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
        }
        
        if let vc = theCurrentModel.subViewControllers.last {
            removeContentController(content: vc)
            theCurrentView.updateTitleText(strTitle: title)
            theCurrentModel.subViewControllers.removeLast()
            if theCurrentModel.subViewControllers.count == 1 {
                theCurrentView.backButtonRisk(isHidden: true)
            }
            print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
        }*/
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentRiskFilter()
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView,at indexRow:Int) {
        let cellView = theCurrentView.tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        let risk = self.theCurrentModel.arr_riskList![view.tag]
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(cellView.contentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: cellView.contentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            
            // Setup your custom UI components
            switch item {
            case MoreAction.chart.rawValue:
                cell.configure(iconName: "ic_report_profile_screen", title: item)
                break
            case MoreAction.action.rawValue:
                cell.configure(iconName: "ic_plus_icon_popup", title: item)
                break
            case MoreAction.edit.rawValue:
                cell.configure(iconName: "ic_edit_icon_popup", title: item)
                break
            case MoreAction.delete.rawValue:
                cell.configure(iconName: "ic_delete_icon_popup", title: item)
                break
            case MoreAction.archived.rawValue:
                cell.configure(iconName: "ic_achive_icon", title: item)
                break
            default:
                cell.configure(iconName: "ic_delete_notes", title: item)
                break
            }
        }
        
        switch dropDown {
        case moreDD:
            var strArrayMore:[String] = []
            strArrayMore = [MoreAction.chart.rawValue,MoreAction.action.rawValue,MoreAction.edit.rawValue,MoreAction.delete.rawValue,MoreAction.archived.rawValue]
            if !UserDefault.shared.isCanEditForm(strOppoisteID: risk.created_by_id)  {
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                    strArrayMore.remove(at: index) // edit
                }
                if let index = strArrayMore.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                    strArrayMore.remove(at: index) // delete
                }
            }
            
            if !UserDefault.shared.isCanArchiveForm(strOppoisteID: risk.created_by_id, completionFlag: risk.completeActionFlag) {
                strArrayMore.removeLast() // archive
            }
            /*
            strArrayMore = [MoreAction.action.rawValue,MoreAction.chart.rawValue,MoreAction.edit.rawValue]
            if risk.created_by_id == Utility.shared.userData.id{
                strArrayMore.append(MoreAction.delete.rawValue)
            }
            if risk.completeActionFlag == 1 && risk.created_by_id == Utility.shared.userData.id{
                strArrayMore.append(MoreAction.archived.rawValue)
            }*/
            dropDown.dataSource = strArrayMore
            break
            
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index),dropdown tag \(view.tag)")
            switch dropDown {
            case self.moreDD:
                switch item {
                case MoreAction.chart.rawValue:
                    self.presentGraphAction(strChartId: risk.risk_id)
                    break
                case MoreAction.action.rawValue:
                    self.redirectToAddActionScreen(theRiskModel: risk)
                    break
                case MoreAction.edit.rawValue:
//                    self.theCurrentModel.isEditRisk = false
                    let risk = self.theCurrentModel.arr_riskList![view.tag]
                    self.getRiskDetailView(risk_id: risk.risk_id, isEdit: true)
                    break
                case MoreAction.delete.rawValue:
                    self.deleteRisk(strRiskID:  risk.risk_id)
                    break
                case MoreAction.archived.rawValue:
                    self.archiveRisk(strRiskID:  risk.risk_id)
                    break
                default:
                    break
                }
                break
                
            default:
                break
            }
            
        }
    }
    
}

//MARK:- Call API

extension RisksVC
{
    
    func tblReloadData()
    {
        self.theCurrentView.refreshControl.endRefreshing()
        self.theCurrentView.tableView.reloadData()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_riskList == nil || theCurrentModel.arr_riskList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func archiveRisk(strRiskID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtRiskIndex.append(strRiskID)
            self?.updateBottomCell(loadingID: strRiskID, isLoadingApi: true)
            self?.changeRiskStatusWebService(strRiskID: strRiskID, isDelete: false,isArchive: true)
        }
    }
    
    func deleteRisk(strRiskID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtRiskIndex.append(strRiskID)
            self?.updateBottomCell(loadingID: strRiskID, isLoadingApi: true)
            self?.changeRiskStatusWebService(strRiskID: strRiskID, isDelete: true)
        }
    }
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_risks,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}

//MARK:- call API

extension RisksVC
{
    func riskService(isRefreshing:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.pageOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_category:theCurrentModel.filterData.categoryID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_page_offset:"\(theCurrentModel.pageOffset)"]
        
        RiskWebservice.shared.getRiskList(parameter: parameter, success: { [weak self] (list, nextOffset,assignToUserFlag)  in
            if assignToUserFlag == 1 && (self?.theCurrentModel.pageOffset ?? -1) == 0 { // Need to set filter count
                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.theCurrentModel.filterData.assigntoID = Utility.shared.userData.id
                self?.theCurrentModel.assignToUserFlag = assignToUserFlag
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getRiskDetailView(risk_id:String,isEdit:Bool) {
        Utility.shared.showActivityIndicator()
        
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_risk_id: risk_id,
                         ]
        
        RiskWebservice.shared.getRiskDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            if isEdit {
                self?.redirectToAddRiskVC(theModel: detail)
            } else {
                self?.redirectToRisksSublist1VC(theRiskID: risk_id, theRiskDetail: detail)

            }
//            self?.updateRiskListModel(theRiskDetailModel: detail)
            
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeRiskStatusWebService(strRiskID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strRiskID]]).rawString() else {
            updateBottomCell(loadingID: strRiskID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_risks,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strRiskID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strRiskID)
        })
    }
    
    
}

//MARK:-UITableViewDataSource
extension RisksVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_riskList == nil || theCurrentModel.arr_riskList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_riskList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RisksListTVCell") as! RisksListTVCell
        if theCurrentModel.arr_riskList != nil
        {
            cell.hideSkeleton()
            let risk = theCurrentModel.arr_riskList![indexPath.row]
            cell.updateBackGroundColor(isCompleted: theCurrentModel.arr_riskList![indexPath.row].completeActionFlag == 1)
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtRiskIndex.contains(theCurrentModel.arr_riskList![indexPath.row].risk_id) {
                isApiLoading = true
            }
            cell.configureRiskCell(risk: risk,isApiLoading:isApiLoading)
            cell.btnMore.tag = indexPath.row
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_riskList?[indexPath.row].risk_status ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_riskList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_riskList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_riskList![indexPath.row].risk_id ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.configureDropDown(dropDown: obj.moreDD, view: cell.btnMore,at: indexPath.row)
                    obj.moreDD.show()
                }
            }
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.pageOffset != -1 && theCurrentModel.pageOffset != 0 {
                riskService()
            }
        } else {
            cell.showSkeleton()
        }
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension RisksVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.pageOffset != -1 && theCurrentModel.arr_riskList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr_model = theCurrentModel.arr_riskList, arr_model.count > 0, !theCurrentModel.apiLoadingAtRiskIndex.contains(arr_model[indexPath.row].risk_id) else { return  }
        getRiskDetailView(risk_id: arr_model[indexPath.row].risk_id, isEdit: false)
//        redirectToRisksSublist1VC(theModel: arr_model[indexPath.row], theRiskDetail: <#RiskDetail?#>)
        /*
        if theCurrentModel.arr_riskList != nil, theCurrentModel.apiLoadingAtRiskIndex != theCurrentModel.arr_riskList![indexPath.row].risk_id {
        }*/
    }
}


/*
//MARK:- Container View
extension RisksVC {
    func displayContentController(content: UIViewController) {
        content.view.frame = theCurrentView.containerView.bounds
        theCurrentView.containerView.addSubview(content.view)
        addChild(content)
        content.didMove(toParent: self)
        content.view.layoutIfNeeded()
        
        let size = theCurrentView.containerView.bounds
        content.view.frame = CGRect(x: size.width, y: 0, width: size.width, height: size.height)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            content.view.frame=CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }, completion:nil)
        UIView.animate(withDuration: 0.3) {
            content.view.alpha = 1
        }
    }
    
    func removeContentController(content: UIViewController) {
        let size = theCurrentView.containerView.bounds
        UIView.animate(withDuration: 0.4, animations: {
            content.view.frame = CGRect(x: size.width, y: 0, width: size.width, height: size.height)
        }, completion: { (competed) in
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
            print("content remove")
        })
    }
}*/

//MARK:- StrategySubControllerDelegate
extension RisksVC:RisksSubControllerDelegate {
    
    func didSelectTableViewCell(row: Int, stpe: RisksSubControllerType) {
        /*print("row:=\(row), stpe:=\(stpe)")
        switch stpe {
        case .step0:
            theCurrentView.updateTitleText(strTitle: "License to Operate business is removed")
            displayContentController(content: subListViewController(step: .step1, title: "License to Operate business is removed"))
            theCurrentView.backButtonStrategy(isHidden: false)
        case .step1:
            theCurrentView.updateTitleText(strTitle: "License to Operate business is removed")
//            displayContentController(content: subListViewController(step: .step2, title: "Financial Performance > Expenses"))
        }*/
    }
}

