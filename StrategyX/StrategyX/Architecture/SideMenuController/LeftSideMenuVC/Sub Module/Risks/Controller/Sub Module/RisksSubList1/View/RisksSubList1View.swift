//
//  RisksSubList1View.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RisksSubList1View: ViewParentWithoutXIB {

    //MARK:- Variabel
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var viewArchive: UIView!
    @IBOutlet weak var viewSubTitleView: UIView!
    @IBOutlet weak var btnBackStrategy: UIButton!
    @IBOutlet weak var constrainBtnBackRiskWidth: NSLayoutConstraint!// 35.0
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var lblTitlePrimary: UILabel!
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButtonRisk(isHidden: false)
        updateTitleText(strTitle: "License to Operate business is removed")
        updateFilterCountText(strCount: "")
        updateSubTitleViewHiddenForArchiveAction(false)
    }
    
    func setupTableView(theDelegate:RisksSubList1VC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(TacticalProjectSubListTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
 
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? RisksSubList1VC)?.getAllRiskListWebService(isRefreshing: true)
    }
    
    func backButtonRisk(isHidden:Bool) {
        constrainBtnBackRiskWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateTitleText(strTitle:String) {
        lblTitlePrimary.text = strTitle
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }

    func updateSubTitleViewHiddenForArchiveAction( _ isHidden:Bool) {
        viewSubTitleView.isHidden = isHidden
        if isHidden {
            let layout = NSLayoutConstraint.init(item: viewSubTitleView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0)
            viewSubTitleView.addConstraint(layout)
        }
        btnGraph.isHidden = isHidden
    }
}
