//
//  RisksDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class RisksDetailModel {

    //MARK:- Variable
    weak var theController:RisksDetailVC!
    var tableviewCount = 13
    var theRiskModel:Risk?
    var arr_noteList:[NotesHistoryList] = []
    var isAddNotesApiLoading = false
    var strAddPostText = String()
    var noteCurrentIndex = 0
    var isPostEdit = Bool()
    var notesId = String()
    var editNotesTag = Int()
    let moreDD = DropDown()
    var arr_MoreList:[String] = []
    var arr_AllLevelType:[JSON] = []
    
    var theRiskDetailModel:RiskDetail? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
                    (vc as? RisksVC)?.updateRiskListModel(theRiskDetailModel: theRiskDetailModel!)
                    (vc as? RisksArchivedVC)?.updateRiskListModel(theRiskDetailModel: theRiskDetailModel!)
                    (vc as? RisksSubList1VC)?.updateRiskListModel(theRiskDetailModel: theRiskDetailModel!)
                }
            }
        }
    }
    
    //MARK:- LifeCycle
    init(theController:RisksDetailVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: -85, y: 30)
        //        dropDown.width = 100
        switch dropDown {
        case moreDD:
            let arr_MoreList:[String] = self.arr_MoreList.map({$0 })
            dropDown.dataSource = arr_MoreList
            break
            
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.update(text: item, dropDown: dropDown,index:index)
                break
                
            default:
                break
            }
            
        }
    }
    
    
}
