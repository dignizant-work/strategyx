//
//  RisksDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 21/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class RisksDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:RisksDetailView = { [unowned self] in
       return self.view as! RisksDetailView
    }()
    
    fileprivate lazy var theCurrentModel:RisksDetailModel = { [unowned self] in
       return RisksDetailModel(theController: self)
    }()
    
    var handlerUpdateRiskSublistModel:() -> Void = {}
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        let arr_red = setupLevelRedArray()
        for i in 0..<arr_red.count{
            theCurrentModel.arr_AllLevelType.append(arr_red[i])
        }
        let arr_yellow = setupLevelYellowArray()
        for i in 0..<arr_yellow.count{
            theCurrentModel.arr_AllLevelType.append(arr_yellow[i])
        }
        let arr_Green = setupLevelGreenArray()
        for i in 0..<arr_Green.count{
            theCurrentModel.arr_AllLevelType.append(arr_Green[i])
        }
        getRiskDetailView()
        
    }
    
    func setTheData(theRiskModel:Risk?) {
        theCurrentModel.theRiskModel = theRiskModel
    }
    
    func updateBottomCell() {
        let index = IndexPath(row: 10, section: 0)
        theCurrentModel.isAddNotesApiLoading = !theCurrentModel.isAddNotesApiLoading
        UIView.performWithoutAnimation {
            let contentOffset = theCurrentView.tableView.contentOffset
            theCurrentView.tableView.reloadRows(at: [index], with: .none)
            theCurrentView.tableView.contentOffset = contentOffset
        }
    }
    
    func checkValidForm()
    {
        self.view.endEditing(true)
        if theCurrentModel.strAddPostText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter post name")
            return
        }
        else
        {
            if theCurrentModel.theRiskModel  == nil{
                return
            }
            let postText = theCurrentModel.strAddPostText
            theCurrentModel.strAddPostText = ""

            if theCurrentModel.isPostEdit == true{
                let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                     
                                     APIKey.key_user_id: Utility.shared.userData.id,
                                     APIKey.key_notes_id: theCurrentModel.notesId,
                                     APIKey.key_notes: postText
                    
                ]
                self.editNotes(parameter: parameter, tag:theCurrentModel.editNotesTag)
                return
            }
          
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                             
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentModel.theRiskModel!.risk_id,
                                 APIKey.key_notes: postText,
                                 APIKey.key_note_for: APIKey.key_risks
                             ]
            self.addNotes(parameter: parameter)
        }
    }
    
    func update(text:String,dropDown:DropDown,index:Int) {
        switch dropDown {
        case theCurrentModel.moreDD:
            print("text \(text)")
            print("index \(index)")
            theCurrentModel.editNotesTag = dropDown.tag
            let note = theCurrentModel.arr_noteList[dropDown.tag]
            switch index
            {
            case 0:
                self.theCurrentModel.strAddPostText = note.notes_description
                self.theCurrentModel.isPostEdit = true
                self.theCurrentModel.notesId = note.id
                self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: theCurrentModel.tableviewCount - 1, section: 0)], with: .none)
                break
            case 1:
                
                showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
                    let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                                     APIKey.key_access_token:Utility.shared.userData.accessToken,
                                     APIKey.key_notes_id:note.id] as [String : Any]
                    self?.deleteNotes(parameter: parameter, tag: dropDown.tag)
                }
               
                break
            default:
                break
            }
            break
        default:
            break
        }
    }
    
    func updateTableviewData(){
        self.theCurrentModel.strAddPostText = ""
        self.theCurrentModel.editNotesTag = 0
        self.theCurrentModel.notesId = ""
        self.theCurrentModel.isPostEdit = false
        self.theCurrentModel.noteCurrentIndex = 0
        if (theCurrentModel.theRiskDetailModel?.risk_status == APIKey.key_archived.lowercased()) {
            self.theCurrentModel.tableviewCount = 12
        } else {
            self.theCurrentModel.tableviewCount = 13
        }
        if self.theCurrentModel.arr_noteList.count != 0 {
            self.theCurrentModel.tableviewCount += (self.theCurrentModel.arr_noteList.count)
        }
        self.theCurrentView.tableView.reloadData()
    }
    
    func setupLevelYellowArray() -> [JSON]{
        var arr_Level:[JSON] = []
        
        var dict = JSON()
        dict["value"].stringValue = "E1"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D1"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D2"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D2"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "C2"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "C3"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "B4"
        dict["color"].stringValue = "Yellow"
        arr_Level.append(dict)
        
        return arr_Level
    }
    
    func setupLevelRedArray() -> [JSON]{
        var arr_Level:[JSON] = []
        
        var dict = JSON()
        dict["value"].stringValue = "E2"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "E3"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "E4"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "E5"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D3"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D4"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "D5"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "C4"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "C5"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "B5"
        dict["color"].stringValue = "Red"
        arr_Level.append(dict)
        
        return arr_Level
    }
    
    func setupLevelGreenArray() -> [JSON]{
        var arr_Level:[JSON] = []
        
        var dict = JSON()
        dict["value"].stringValue = "C1"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "B1"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "B2"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "B3"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "A1"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "A2"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "A3"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "A4"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
        
        dict = JSON()
        dict["value"].stringValue = "A5"
        dict["color"].stringValue = "Green"
        arr_Level.append(dict)
      
        return arr_Level
    }
    
}

//MARK:- UITableViewDataSource
extension RisksDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! RisksDetailTVCell
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! RisksDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theRiskDetailModel != nil{
                cell.configureCell1(strTitle: theCurrentModel.theRiskDetailModel!.unwanted_event,isSkeltonAnimation:false)
            }
            else {
                cell.configureCell1(strTitle: theCurrentModel.theRiskModel!.unwanted_event,isSkeltonAnimation:true)
            }
            
        } else if indexPath.row == 1 { // Discription Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! RisksDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theRiskDetailModel != nil{
                cell.configureCell3(strDescription: theCurrentModel.theRiskDetailModel!.risk_description,isSkeltonAnimation:false)
            }
            else{
                cell.configureCell3(strDescription: "",isSkeltonAnimation:true)
            }
            
        } else if indexPath.row >= 2 && indexPath.row <= 9 {
            if indexPath.row == 10 {
                var dash = ""
                if theCurrentModel.arr_noteList.count <= 0 {
                    dash = "-"
                }
                cell.configureCell2(strTypeName: "Notes", strTypeDetail: dash,isForNotes: true,isSkeltonAnimation:false)
            } else if indexPath.row == 9 {
                if theCurrentModel.theRiskDetailModel != nil{
                    if let selectedIndex = theCurrentModel.arr_AllLevelType.firstIndex(where: {$0["value"].stringValue == theCurrentModel.theRiskDetailModel!.level}) {
                        let bgColor = theCurrentModel.arr_AllLevelType[selectedIndex]["color"].stringValue
                        if bgColor == "Yellow"{
                            cell.configureCell2(strTypeName: "Level", strTypeDetail: theCurrentModel.theRiskDetailModel!.level, isForLevel: true,isSkeltonAnimation:false,levelBgColor:appDarkYellow)
                        } else if bgColor == "Green"{
                            cell.configureCell2(strTypeName: "Level", strTypeDetail: theCurrentModel.theRiskDetailModel!.level, isForLevel: true,isSkeltonAnimation:false,levelBgColor:appGreen)
                        }  else if bgColor == "Red"{
                            cell.configureCell2(strTypeName: "Level", strTypeDetail: theCurrentModel.theRiskDetailModel!.level, isForLevel: true,isSkeltonAnimation:false,levelBgColor:appPink)
                        }
                    }
                }
                else{
                    cell.configureCell2(strTypeName: "Level", strTypeDetail: "", isForLevel: false,isSkeltonAnimation:true)
                }
                
            } else {
                if indexPath.row == 2 {
                    if theCurrentModel.theRiskDetailModel != nil{
                         cell.configureCell2(strTypeName: "Category", strTypeDetail: theCurrentModel.theRiskDetailModel!.category_name,isSkeltonAnimation:false)
                    }
                    else{
                       cell.configureCell2(strTypeName: "Category", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                   
                } else if indexPath.row == 4 { //Created By
                    if theCurrentModel.theRiskDetailModel != nil{
                        cell.configureCell2(strTypeName: "Created By", strTypeDetail: theCurrentModel.theRiskDetailModel!.created_by,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Created By", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                    
                }else if indexPath.row == 3 {
                    if theCurrentModel.theRiskDetailModel != nil{
                        cell.configureCell2(strTypeName: "Company", strTypeDetail: theCurrentModel.theRiskDetailModel!.company_name,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Company", strTypeDetail:
                        "",isSkeltonAnimation:true)
                    }
                    
                } else if indexPath.row == 5 {
                    if theCurrentModel.theRiskDetailModel != nil{
                         cell.configureCell2(strTypeName: "Assigned to", strTypeDetail: theCurrentModel.theRiskDetailModel!.assigned_to,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Assigned to", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                   
                } else if indexPath.row == 6 {
                    if theCurrentModel.theRiskDetailModel != nil{
                        cell.configureCell2(strTypeName: "Department", strTypeDetail: theCurrentModel.theRiskDetailModel!.department_name,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Department", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                } else if indexPath.row == 7 {
                    if theCurrentModel.theRiskDetailModel != nil{
                        var dateDue = "-"
                        if let date = theCurrentModel.theRiskDetailModel?.duedate, let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!dueDateformate.isEmpty {
                            dateDue = dueDateformate
                        }
                        cell.configureCell2(strTypeName: "Due date", strTypeDetail: dateDue,isSkeltonAnimation:false)
                    }
                    else{
                        cell.configureCell2(strTypeName: "Due date", strTypeDetail: "",isSkeltonAnimation:true)
                    }
                } else if indexPath.row == 8 {
                    cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: " ", isSkeltonAnimation: true)
                    
                    if theCurrentModel.theRiskDetailModel != nil {
                        var dateDue = "-"
                        if let date = theCurrentModel.theRiskDetailModel?.revisedDate, let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!dueDateformate.isEmpty {
                            dateDue = dueDateformate
                        }
                        cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: dateDue, isSkeltonAnimation: false)
                    }
                    //                cell.configureCell2(strTypeName: "Revised due date", strTypeDetail: "05 Oct 18")
                }
            }
        } else if indexPath.row == 10 {
            var dash = ""
            if theCurrentModel.arr_noteList.count <= 0 {
                dash = "-"
            }
            cell.configureCell2(strTypeName: "Notes", strTypeDetail: dash,isForNotes: true,isSkeltonAnimation:false)
        } else if indexPath.row == 11 && (theCurrentModel.theRiskDetailModel?.risk_status.lowercased() != APIKey.key_archived.lowercased()) { // Add notes
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell5") as! RisksDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configureCell5(isLoading: theCurrentModel.isAddNotesApiLoading)

            cell.handlerText = {[weak self] (strText) in
//                cell.txtAddNote.text = strText
                self?.theCurrentModel.strAddPostText = strText
            }
            cell.handlerOnBtnAddPostClick = {[weak self] in
                self?.checkValidForm()
                cell.txtAddNote.text = ""
            }
            
        } else if indexPath.row == theCurrentModel.tableviewCount - 1 { // Last Cell
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! RisksDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
//            cell.configureCell4(isLoading: theCurrentModel.isAddNotesApiLoading)
            
            if theCurrentModel.theRiskDetailModel != nil {
                if !UserDefault.shared.isCanEditForm(strOppoisteID: theCurrentModel.theRiskDetailModel?.created_by_id ?? "") || UserDefault.shared.isCanArchiveForm(strOppoisteID: theCurrentModel.theRiskDetailModel?.created_by_id ?? "", completionFlag: theCurrentModel.theRiskDetailModel?.risk_status == APIKey.key_archived.lowercased() ? 1 : 0) {
                    cell.btnSave.isHidden = true
                } else {
                    cell.btnSave.isHidden = false
                }
            }
            /*if theCurrentModel.theRiskDetailModel != nil {
                if theCurrentModel.theRiskDetailModel?.risk_status == APIKey.key_archived.lowercased(){
                    cell.btnSave.isHidden = true
                }
            }*/
            
            cell.handlerOnBtnCloseClick = { [weak self] in
                self?.backAction(isAnimation: false)
            }
            
            cell.handlerOnBtnSaveClick = {[weak self] in
                if self?.theCurrentModel.theRiskDetailModel == nil{
                    return
                }
                let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
                vc.setTheEditData(theModel: self?.theCurrentModel.theRiskDetailModel, isForEdit: true)
                vc.handlerUpdateModel = { [weak self] (theDetailModel) in
                    self?.getRiskDetailView()
                }
                self?.push(vc: vc)
            }
        } else { // Notes
             let noteCell = tableView.dequeueReusableCell(withIdentifier: "RisksNoteTVCell") as! RisksNoteTVCell
            noteCell.contentView.backgroundColor = .clear
            noteCell.backgroundColor = .clear
            noteCell.selectionStyle = .none
            if theCurrentModel.arr_noteList.count <= 0 {
                noteCell.lblNorecordfound.isHidden = false
                return noteCell
            }
            noteCell.lblNorecordfound.isHidden = true
            noteCell.btnMoreOutlet.isHidden = true
            /*if theCurrentModel.theRiskDetailModel?.risk_status == APIKey.key_archived.lowercased(){
                noteCell.btnMoreOutlet.isHidden = true
            }
            noteCell.btnMoreOutlet.tag = theCurrentModel.noteCurrentIndex
            theCurrentModel.arr_MoreList = ["Edit","Delete"]
            noteCell.onHandlerBtnMore = {[weak self] (tag) in
                self?.view.endEditing(true)
                self?.theCurrentModel.moreDD.tag = tag
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: noteCell.btnMoreOutlet)
                }
                self?.theCurrentModel.moreDD.show()
            }*/
            let note = theCurrentModel.arr_noteList[theCurrentModel.noteCurrentIndex]
            var dateDue = ""
            let date = note.created_date
            let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType5)
            dateDue = dueDateformate!
            noteCell.configure(strDescription: note.notes_description, name: note.from_user_name, date: "Posted on \(dateDue)", img: note.assigned_to_user_image)
            if theCurrentModel.noteCurrentIndex < theCurrentModel.arr_noteList.count - 1{
                 theCurrentModel.noteCurrentIndex += 1
            }
            return noteCell
        
        }
        
        return cell
    }
    
    
}
//MARK:-UITableViewDelegate
extension RisksDetailVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 && UserDefault.shared.userRole != .superAdminUser {
            return CGFloat.leastNormalMagnitude
        }
        return UITableView.automaticDimension
    }
}

//MARK:- Call API

extension RisksDetailVC
{
    func getRiskDetailView()
    {
        guard let risk_id = theCurrentModel.theRiskModel?.risk_id else {
            return
        }
       
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_risk_id: risk_id,
                         ]
        
        RiskWebservice.shared.getRiskDetail(parameter: parameter, success: { [weak self] (detail) in
          
            self?.theCurrentModel.theRiskDetailModel = detail
//            self?.theCurrentModel.arr_noteList = Array(detail.notesList)
            self?.updateTableviewData()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func editNotes(parameter:[String:Any],tag:Int){
        updateBottomCell()
        RiskWebservice.shared.postEditRiskNotes(parameter: parameter, success: { [weak self] (data) in
                self?.updateBottomCell()
                self?.theCurrentModel.arr_noteList[tag] = data
                self?.updateTableviewData()
            }, failed: { (error) in
                self.updateBottomCell()
                self.showAlertAtBottom(message: error)
        })
    }
    
    func addNotes(parameter:[String:Any])
    {
        updateBottomCell()
        RiskWebservice.shared.postAddRiskNotes(parameter: parameter, success: { [weak self] (data)in
            self?.updateBottomCell()
            self?.theCurrentModel.arr_noteList.append(data)
            self?.updateTableviewData()
            }, failed: { (error) in
                self.updateBottomCell()
                self.showAlertAtBottom(message: error)
        })
    }
    
    func deleteNotes(parameter:[String:Any],tag:Int)
    {
        RiskWebservice.shared.postDeleteNotesRisks(parameter: parameter, success: { [weak self] in
            self?.theCurrentModel.arr_noteList.remove(at: tag)
            self?.updateTableviewData()
            }, failed: { (error) in
                self.showAlertAtBottom(message: error)
        })
        
    }
}
