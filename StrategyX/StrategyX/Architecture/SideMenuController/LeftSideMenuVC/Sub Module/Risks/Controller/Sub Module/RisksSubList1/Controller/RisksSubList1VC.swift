//
//  RisksSubList1VC.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class RisksSubList1VC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:RisksSubList1View = { [unowned self] in
        return self.view as! RisksSubList1View
        }()
    
    fileprivate lazy var theCurrentModel:RisksSubList1Model = {
        return RisksSubList1Model(theController: self)
    }()
    
    var handlerUpdateRiskModel:() -> Void = {}
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.viewArchive.isHidden = theCurrentModel.isForArchive
        theCurrentView.updateTitleText(strTitle: theCurrentModel.theRiskModel?.unwanted_event ?? "")
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getAllRiskListWebService()
    }
    
    /*func setTheData(theRiskModel:Risk?) {
        theCurrentModel.theRiskModel = theRiskModel
    }*/
    func setTheData(theRiskModel:Risk?,theRiskDetailModel:RiskDetail?, isForArchive:Bool = false) {
        theCurrentModel.theRiskModel = theRiskModel
        theCurrentModel.isForArchive = isForArchive
        theCurrentModel.theRiskDetailModel = theRiskDetailModel
    }
    
    func setCompanyData()
    {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    
    func updateRiskListModel(theRiskDetailModel:RiskDetail) {
        if theCurrentModel.theRiskModel?.risk_id == theRiskDetailModel.risk_id {
            let model = theCurrentModel.theRiskModel!
            model.unwanted_event = theRiskDetailModel.unwanted_event
            model.revisedDate = theRiskDetailModel.revisedDate
            model.assigned_to = theRiskDetailModel.assigned_to
            model.assignee_id = theRiskDetailModel.assignee_id
            model.duedate = theRiskDetailModel.duedate
            model.revisedDate = theRiskDetailModel.revisedDate
            model.duedateColor = theRiskDetailModel.duedateColor
            model.revisedColor = theRiskDetailModel.revisedColor
            model.level = theRiskDetailModel.level
            model.assigned_to_user_image = theRiskDetailModel.assigned_to_user_image
            theCurrentModel.theRiskModel = model
            theCurrentModel.theRiskDetailModel = theRiskDetailModel
        }
        theCurrentView.updateTitleText(strTitle: theRiskDetailModel.unwanted_event)
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_RiskActionsList == nil || theCurrentModel.arr_RiskActionsList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_RiskActionsList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            presentGraphActionList(strChartId: model.actionlogId)
            //            presentGraphAction(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            //            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    
    func deleteActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    func archiveActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: false,isArchive: true)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_RiskActionsList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtActionID.remove(at: index)
            }
            //            theCurrentModel.apiLoadingAtActionID = "-1"
            theCurrentModel.arr_RiskActionsList?.remove(at: indexRow)
            if let model =  theCurrentModel.theRiskModel {
                model.total_action -= 1
                model.total_action = model.total_action < 0 ? 0 : model.total_action
                if theCurrentModel.isForActionArchive {
                    model.complate_action -= 1
                    model.complate_action = model.complate_action < 0 ? 0 : model.complate_action
                }
                model.completeActionFlag = model.total_action == model.complate_action ? (model.total_action == 0 && model.complate_action == 0) ? 0 : 1 : 0

//                model.completeActionFlag = model.total_action == model.complate_action ? 1 : 0
                theCurrentModel.theRiskModel = model
            }
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
            if theCurrentModel.arr_RiskActionsList?.count == 0 {
                setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
            }
            /*
             if !isLoadingApi || isRemove {
             theCurrentModel.apiLoadingAtActionID = "-1"
             }*/
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [index], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateTheRiskModelFromActionList(theActionDetailModel:ActionDetail? = nil,isForUpdate:Bool = false, theActionListModel:ActionsList? = nil) {
        guard var arrModels = theCurrentModel.arr_RiskActionsList else { return }
        var theModel = theActionListModel
        var previousModelPercentageCompeleted = "-1"
        
        if let model = theActionListModel {
            if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                let updatedModel = theCurrentModel.arr_RiskActionsList![index]
                previousModelPercentageCompeleted = updatedModel.percentageCompeleted
                updatedModel.actionlogTitle = model.actionlogTitle
                updatedModel.revisedDate = model.revisedDate
                updatedModel.duedate = model.duedate
                updatedModel.workDate = model.workDate
                updatedModel.percentageCompeleted = model.percentageCompeleted
                updatedModel.assigneeId = model.assigneeId
                updatedModel.assignedTo = model.assignedTo
                updatedModel.assignedToUserImage = model.assignedToUserImage
                theCurrentModel.arr_RiskActionsList![index] = model
                theModel = model
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
        
        guard let model = theCurrentModel.theRiskModel else { return }
        guard let deletedActionModel = theModel else { return }
        
        //        theCurrentModel.theRiskModel = model
        arrModels = theCurrentModel.arr_RiskActionsList!
        let totalActions = arrModels.count
        var completedActions = theCurrentModel.theRiskModel?.complate_action ?? 0
        var completedActionFlag = theCurrentModel.theRiskModel?.completeActionFlag ?? 0
        
        //        let filterCompletedActions = arrModels.filter({ $0.percentageCompeleted == "100" })
        
        if isForUpdate {
            if deletedActionModel.percentageCompeleted != "100" && previousModelPercentageCompeleted == "100" {
                completedActions -= 1
            } else if deletedActionModel.percentageCompeleted == "100" && previousModelPercentageCompeleted != "100" {
                completedActions += 1
            }
        } else {
            if deletedActionModel.percentageCompeleted == "100" {
                completedActions -= 1
            }
        }
        
        completedActionFlag = (completedActions != 0 && totalActions != 0) && (completedActions == totalActions) ? 1 : 0
        
        model.total_action = totalActions
        model.complate_action = completedActions
        model.completeActionFlag = completedActionFlag
        print("theCurrentModel.theRiskModel:=",model)
        theCurrentModel.theRiskModel = model
        
    }
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_RiskActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            } else {
                //                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
                
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_RiskActionsList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_RiskActionsList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_RiskActionsList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_RiskActionsList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_RiskActionsList![index].workdateColor = model.workdateColor
                        theCurrentModel.arr_RiskActionsList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId }) {
            theCurrentModel.arr_RiskActionsList![index] = model
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentModel.arr_RiskActionsList![index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentModel.arr_RiskActionsList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_RiskActionsList![index].revisedColor = model.revisedColor
            theCurrentModel.arr_RiskActionsList![index].duedate = model.revisedDate
            theCurrentModel.arr_RiskActionsList![index].duedateColor = model.duedateColor
            theCurrentModel.arr_RiskActionsList![index].requestFlag = model.requestFlag
            theCurrentModel.arr_RiskActionsList![index].approveFlag = model.approveFlag

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentModel.arr_RiskActionsList?.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentModel.arr_RiskActionsList?[index].percentageCompeleted = "\(percentage)"
            theCurrentModel.arr_RiskActionsList?[index].approveFlag = percentage == 100 ? 1 : 0

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    //MARK:- Redirection
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel)  in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            if let model = theActionModel {
                theModel?.duedateColor = model.duedateColor
            }
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_RiskActionsList?.removeAll()
            self?.theCurrentModel.arr_RiskActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getAllRiskListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .risks)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            var totalFilterCount = 0
            if filterData.searchText != ""{
                totalFilterCount += 1
            }
            if filterData.categoryID != ""{
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.departmentID != "" {
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_RiskActionsList?.removeAll()
            self?.theCurrentModel.arr_RiskActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getAllRiskListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphAction(strChartId:String) {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .risk)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToAddActionVC(theModel:ActionDetail) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
            self?.updateTheRiskModelFromActionList(theActionDetailModel: theActionDetailModel, isForUpdate: true, theActionListModel: nil)
        }
        self.push(vc: vc)
    }
    func redirectToRiskDetailVC() {
        let model = theCurrentModel.theRiskModel
        model?.notesCount = 0
        theCurrentModel.theRiskModel = model
        let vc = RisksDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        vc.setTheData(theRiskModel: theCurrentModel.theRiskModel)
        self.push(vc: vc)
    }
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionListModel) in
            self?.updateTheRiskModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionListModel)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theModel) in
            self?.updateRequestDueDateActionListModelAndUI(theModel: theModel)
        }
        self.push(vc: vc)
    }
    func redirectToAddRiskVC(theModel:RiskDetail?) {
        let model = theCurrentModel.theRiskModel
        model?.notesCount = 0
        theCurrentModel.theRiskModel = model
        
        let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theDetailModel) in
            //            self?.theCurrentModel.isEditRisk = true
            if let theModelDetail = theDetailModel {
                self?.updateRiskListModel(theRiskDetailModel: theModelDetail)
            }
            //            self?.getRiskDetailView(risk_id: theModel.risk_id)
        }
        self.push(vc: vc)
    }
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnArchiveAction(_ sender: Any) {
        theCurrentModel.filterData = FilterData.init()
        setCompanyData()
        theCurrentModel.isForActionArchive = true
        theCurrentView.viewArchive.isHidden = true
        theCurrentView.lblHeaderTitle.text = "Risks > Archived Actions"
        theCurrentView.updateSubTitleViewHiddenForArchiveAction(true)
        theCurrentModel.arr_RiskActionsList?.removeAll()
        theCurrentModel.arr_RiskActionsList = nil
        theCurrentView.tableView.reloadData()
        getAllRiskListWebService(isRefreshing: true)
    }
    @IBAction func onBtnRisksGraphAction(_ sender: Any) {
        presentGraphAction(strChartId: theCurrentModel.theRiskModel?.risk_id ?? "")
    }
    
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
//        redirectToRiskDetailVC()
        redirectToAddRiskVC(theModel: theCurrentModel.theRiskDetailModel)
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        //presentRiskFilter()
        presentActionFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension RisksSubList1VC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_RiskActionsList == nil || theCurrentModel.arr_RiskActionsList?.count == 0) {
            return 0
        }
        return theCurrentModel.arr_RiskActionsList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TacticalProjectSubListTVCell") as! TacticalProjectSubListTVCell
        cell.showMoreButtonForAction(isHidden: true)
        if theCurrentModel.arr_RiskActionsList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtActionID.contains(theCurrentModel.arr_RiskActionsList![indexPath.row].actionlogId) {
                isApiLoading = true
            }
            cell.configureActionsList(theModel: theCurrentModel.arr_RiskActionsList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentModel.arr_RiskActionsList![indexPath.row])
//                self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].actionlogId ?? "")
            }
            cell.handleWorkDateAction = { [weak self] in
                self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentModel.arr_RiskActionsList![indexPath.row])
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_RiskActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].workDate ?? "", dueDate: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_RiskActionsList![indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerPercentCompletedAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_RiskActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentModel.arr_RiskActionsList?[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentModel.arr_RiskActionsList?[indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_RiskActionsList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getAllRiskListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_RiskActionsList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
    
}
//MARK:-UITableViewDelegate
extension RisksSubList1VC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_RiskActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_RiskActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let arr_model = theCurrentModel.arr_RiskActionsList,!theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
/*
            if theCurrentModel.theRiskModel?.risk_status.lowercased() == APIKey.key_archived.lowercased() {
//                redirectToActionDetailScreen(theModel: theCurrentModel.arr_RiskActionsList![indexPath.row])
            } else {
            }*/
        }
    }
}

//MARK:- Api Call
extension RisksSubList1VC {
    func getAllRiskListWebService(isRefreshing:Bool = false) {
        guard let risk_id = theCurrentModel.theRiskModel?.risk_id else {
            theCurrentView.refreshControl.endRefreshing()
            theCurrentView.tableView.reloadData()
            return
        }
        
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        
        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = theCurrentModel.filterData.workDate, !workDateFormate.isEmpty {
            //            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = theCurrentModel.filterData.dueDate, !dueDateFormate.isEmpty {
            //            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_tags:theCurrentModel.filterData.searchTag,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_workdatefilter:workDate,
                         APIKey.key_duedatefilter:dueDate,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)",
                         APIKey.key_risk_id: risk_id]
        if theCurrentModel.isForArchive || theCurrentModel.isForActionArchive {
            parameter[APIKey.key_grid_type] = APIKey.key_archived
        }
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        ActionsWebService.shared.getAllActionsList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            },  failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
//                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()

        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
//            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}
