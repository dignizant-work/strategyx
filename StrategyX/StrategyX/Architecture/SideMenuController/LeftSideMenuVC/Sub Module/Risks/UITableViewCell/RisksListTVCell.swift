//
//  RisksListTVCell.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SkeletonView
import SDWebImage

class RisksListTVCell: UITableViewCell {
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30

    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var widthBtnMore: NSLayoutConstraint! //30
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!
    @IBOutlet weak var constrainViewNotesWidth: NSLayoutConstraint! // 22.0
    @IBOutlet weak var constrainViewNotesLeading: NSLayoutConstraint! // 0.0

    var handlerMoreAction:() -> Void = {}
    var handleDueDateAction:() -> Void = {}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        showSkeleton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateBackGroundColor(isCompleted:Bool = false) {
        viewBg.backgroundColor = isCompleted ? appGreen.withAlphaComponent(0.19) : nil
    }
    func configureRiskCell(risk:Risk, isApiLoading:Bool) {
        btnMore.loadingIndicator(isApiLoading)
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblNotesCount.text = "\(risk.notesCount)"
        viewNotesCount.setBackGroundColor = risk.notesCount > 0 ? 9 : 3
        lblTitle.text = risk.unwanted_event
        lblTotal.text = "\(risk.complate_action)/\(risk.total_action)"
        lblLevel.text = risk.level
//        img_Profile.sd_setImageLoadMultiTypeURL(url: risk.assigned_to_user_image, placeholder: "ic_mini_plash_holder")
        lblProfile.text = risk.assigned_to.acronym()
        if risk.assigned_to_user_image.isEmpty && risk.assigned_to.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !risk.assigned_to_user_image.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: risk.assigned_to_user_image, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if risk.assigned_to.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if risk.assigned_to.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        updateViewProfile(strAssignedID: risk.assignee_id)
        var Subtitle = ""
        let attributeString = NSMutableAttributedString()
        
        
        if let dueDate = risk.duedate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType1),!dueDate.isEmpty {
            Subtitle = "D: " + dueDate
        }else{
            Subtitle = "D: No Due Date"
        }
        let attributeStringWork =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: risk.duedateColor)])
        attributeString.append(attributeStringWork)
        
        lblSubTitle.attributedText = attributeString
        
//        (lblSubTitle as? LabelButton)?.onClick = { [weak self] in
//            self?.handleDueDateAction()
//        }
    }
    
    func configureRiskCell(archived:Risk) {
        lblNotesCount.text = "\(archived.notesCount)"
        viewNotesCount.setBackGroundColor = archived.notesCount > 0 ? 9 : 3

        lblTitle.text = archived.unwanted_event
        lblTotal.text = "\(archived.complate_action)/\(archived.total_action)"
        lblLevel.text = archived.level
//        img_Profile.sd_setImageLoadMultiTypeURL(url: archived.assigned_to_user_image, placeholder: "ic_mini_plash_holder")
        lblProfile.text = archived.assigned_to.acronym()
        if archived.assigned_to_user_image.isEmpty && archived.assigned_to.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !archived.assigned_to_user_image.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: archived.assigned_to_user_image, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if archived.assigned_to.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if archived.assigned_to.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        updateViewProfile(strAssignedID:archived.assignee_id)
       /* if !archived.assigned_to_user_image.isEmpty {
            img_Profile.sd_setImageLoadMultiTypeURLForList(url: archived.assigned_to_user_image, completion: { [weak self] (isHidden) in
                self?.lblProfile.isHidden = !isHidden
                if archived.assigned_to.isEmpty {
                    self?.img_Profile.isHidden = false
                }
            })
        }
        
        if archived.assigned_to.isEmpty {
            img_Profile.isHidden = false
        }*/
        var Subtitle = ""
        let attributeString = NSMutableAttributedString()
        
        
        if let dueDate = archived.duedate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType1),!dueDate.isEmpty {
            Subtitle = "D: " + dueDate
        }else{
            Subtitle = "D: No Due Date"
        }
        let attributeStringWork =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: archived.duedateColor)])
        attributeString.append(attributeStringWork)
        
        lblSubTitle.attributedText = attributeString
    }
    
    func hideSkeleton() {
        lblTitle.hideSkeleton()
        lblSubTitle.hideSkeleton()
        lblTotal.hideSkeleton()
        lblLevel.hideSkeleton()
        img_Profile.hideSkeleton()
        viewNotesCount.hideSkeleton()
        btnMore.isHidden = false
    }
    
    func showSkeleton() {
        img_Profile.isHidden = false
        [lblTitle,lblSubTitle,lblTotal,lblLevel,viewNotesCount].forEach({$0.showAnimatedSkeleton()})
        self.img_Profile.showSkeleton()
        self.btnMore.isHidden = true
    }
    
    func hideMoreButton(){
        widthBtnMore.constant = 0
        btnMore.isHidden = true
    }
    
    func hideViewNotes() {
        viewNotesCount.isHidden = true
        constrainViewNotesWidth.constant = 0
        constrainViewNotesLeading.constant = 0
    }
    
    func updateViewProfile(strAssignedID:String) {
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID: strAssignedID) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
    }
    
    //MARK:- IBAction
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
    
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        handleDueDateAction()
    }
    
}
