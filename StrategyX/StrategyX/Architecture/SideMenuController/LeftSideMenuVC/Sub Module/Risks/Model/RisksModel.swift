//
//  RisksModel.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RisksModel {

    fileprivate weak var theController:RisksVC!
    var pageOffset:Int = 0
    var isEditRisk = Bool()
//    var subControllerStpes = RisksSubControllerType.step0
//    var subViewControllers:[RisksSubListVC] = []
    var apiLoadingAtRiskIndex:[String] = []
    var filterData = FilterData.init()
    var assignToUserFlag = -1

    //MARK:- Variable
    init(theController:RisksVC) {
        self.theController = theController
    }
    
    // get category
    
    var arr_riskList:[Risk]?    
    
    
    func updateList(list:[Risk], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_riskList?.removeAll()
        }
        pageOffset = offset
        if arr_riskList == nil {
            arr_riskList = []
        }
        list.forEach({ arr_riskList?.append($0) })
        print(arr_riskList!)
    }
    
}

