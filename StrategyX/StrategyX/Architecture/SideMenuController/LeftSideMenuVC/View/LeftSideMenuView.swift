//
//  LeftSideMenuView.swift
//  StrategyX
//
//  Created by Haresh on 15/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LeftSideMenuView: ViewParentWithoutXIB {

    //MARK:- Variable
    
    @IBOutlet weak var img_downSelectCompanyArrow: UIImageView!
    @IBOutlet weak var img_downUserArrow: UIImageView!
    @IBOutlet weak var btnSelectCompanyArrow: UIButton!
    @IBOutlet weak var btnUserArrow: UIButton!
    @IBOutlet weak var viewSelectCompany: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblSelectedCompanyName: UILabel!
    
    @IBOutlet weak var img_UserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserRole: UILabel!
    @IBOutlet weak var constrain_ViewSelectCompany_height: NSLayoutConstraint! // 55.0
    
    func setupLayout() {
        img_UserProfile.sd_setImageLoadMultiTypeURL(url: Utility.shared.userData.image, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with:  URL(string: Utility.shared.userData.image), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))
        lblUserName.text = Utility.shared.userData.firstname + " " + Utility.shared.userData.lastname
        constrain_ViewSelectCompany_height.constant = 0.0
        viewSelectCompany.isHidden = true
        lblSelectedCompanyName.text = "All"
        
        switch UserDefault.shared.userRole {
        case .superAdminUser:
            lblUserRole.text = "Supper Admin"
            constrain_ViewSelectCompany_height.constant = 55.0
            viewSelectCompany.isHidden = false
            break
        case .companyAdminUser:
            lblUserRole.text = "Admin"
            break
        case .staffUser:
            lblUserRole.text = "Staff"
            break
        case .manager:
            lblUserRole.text = "Manager"
            break
        case .staffLite:
            lblUserRole.text = "Staff Lite"
            break
        }
        updateUserProfileView()
    }
    
    func updateUserProfileView() {
        img_UserProfile.sd_setImageLoadMultiTypeURL(url: Utility.shared.userData.image, placeholder: "ic_mini_plash_holder")
        lblUserName.text = Utility.shared.userData.firstname + " " + Utility.shared.userData.lastname
    }
    
    //setupTableview
    func setupTableView(theDelegate:LeftSideMenuVC) {
        tableView.rowHeight = 65.0
        tableView.estimatedRowHeight = 65.0
        tableView.separatorStyle = .none
        tableView.registerCellNib(LeftMenuTVCell.self)
        tableView.registerCellNib(LeftMenuCompanyTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
    }
    
    func updateBtnUserArrow() {
        btnUserArrow.isSelected = !btnUserArrow.isSelected
        if btnUserArrow.isSelected {
            img_downUserArrow.rotateArrow(at:CGFloat.pi)
        } else {
            img_downUserArrow.rotateArrow(at: 0, transorm: CGAffineTransform.identity)
        }
        if UserDefault.shared.userRole == UserRole.superAdminUser {
            viewSelectCompany.isHidden = btnUserArrow.isSelected
        }
    }
    func updateBtnSelectCompanyArrow() {
        self.btnSelectCompanyArrow.isSelected = !self.btnSelectCompanyArrow.isSelected
        if btnSelectCompanyArrow.isSelected {
            img_downSelectCompanyArrow.rotateArrow(at:CGFloat.pi)
        } else {
            img_downSelectCompanyArrow.rotateArrow(at: 0, transorm: CGAffineTransform.identity)
        }
        
    }
    func getBtnUserAndComanySelectionAction() -> (Bool,Bool) {
        return (btnUserArrow.isSelected,btnSelectCompanyArrow.isSelected)
    }
    func reloadTableView() {
        tableView.reloadData()
    }
}
