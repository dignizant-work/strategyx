//
//  LeftSideMenuVC.swift
//  StrategyX
//
//  Created by Haresh on 15/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

class LeftSideMenuVC: ParentViewController {

    //Varibale
    fileprivate lazy var theCurrentView:LeftSideMenuView = { [unowned self] in
       return self.view as! LeftSideMenuView
    }()
    
    fileprivate lazy var theCurrentModel:LeftSideMenuModel = {
        return LeftSideMenuModel(theController: self, theCurrentView: theCurrentView)
    }()
    
    //    private lazy var theCurrentModel: StoreItemDetailModel = {
    //        return StoreItemDetailModel(theController: self)
    //    }()
    
    weak var delegate:LeftSideMenuDelegate!
    
    let disposeBag = DisposeBag()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
         setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserRole.superAdminUser == UserDefault.shared.userRole && theCurrentModel.arr_SelectCompany_LeftMenuTitle.count == 0 {
            theCurrentModel.companylistService()
        }
        theCurrentView.updateUserProfileView()
//        theCurrentView.reloadTableView()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        /*if UserRole.superAdminUser == UserDefault.shared.userRole {
            theCurrentModel.companylistService()
        } else {
            theCurrentView.lblSelectedCompanyName.text = Utility.shared.userData.companyName.isEmpty ? "Select company" : Utility.shared.userData.companyName
        }
        theCurrentView.reloadTableView()*/
        
        if UserRole.superAdminUser == UserDefault.shared.userRole {
            theCurrentModel.companylistService()
        } else {
            theCurrentView.lblSelectedCompanyName.text = Utility.shared.userData.companyName.isEmpty ? "Select company" : Utility.shared.userData.companyName
        }
        
        //Rx Action
        btnUserArrowAction()
        btnSelectcompanyArrowAction()
    }
    func updateCompanyName(strCompanyName:String) {
        theCurrentView.lblSelectedCompanyName.text = strCompanyName
    }
    func reinitViewController() {
        guard let mainTabbarVC = slideMenuController()?.mainViewController as? MainTabBarController else { return }
        mainTabbarVC.reInitViewController()
    }
    //RxAction
    func btnUserArrowAction() {
        theCurrentView.btnUserArrow.rx.tap
            .subscribe(onNext:{ [weak self] in
                if let obj = self {
                    obj.theCurrentView.updateBtnUserArrow()
                    obj.theCurrentView.reloadTableView()
                }
            }).disposed(by: disposeBag)
    }
    
    func btnSelectcompanyArrowAction() {
        theCurrentView.btnSelectCompanyArrow.rx.tap
            .subscribe(onNext:{ [weak self] in
                if let obj = self {
                    if UserRole.superAdminUser == UserDefault.shared.userRole {
                        obj.theCurrentView.updateBtnSelectCompanyArrow()
                        obj.theCurrentView.reloadTableView()
                    }
                }
            }).disposed(by: disposeBag)
    }
}

//MARK:- UITableViewDataSource
extension LeftSideMenuVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.getTableviewList().1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arr_Menu = theCurrentModel.getTableviewList()
        if arr_Menu.0.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCompanyTVCell") as! LeftMenuCompanyTVCell
            cell.configure(strTitle: arr_Menu.1[indexPath.row], isSelected: indexPath.row == theCurrentModel.selectedCompanyIndex)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTVCell") as! LeftMenuTVCell
            cell.configure(strImg: arr_Menu.0[indexPath.row], strTitle: arr_Menu.1[indexPath.row])
            return cell
        }
        
    }
}

//MARK:- UITableViewDelegate
extension LeftSideMenuVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arr_Menu = theCurrentModel.getTableviewList()
        if theCurrentView.btnSelectCompanyArrow.isSelected && !theCurrentView.btnUserArrow.isSelected {
            if indexPath.row != 0 {
                if UserRole.superAdminUser == UserDefault.shared.userRole {
                    Utility.shared.userData.companyId = theCurrentModel.arr_SelectCompany_LeftMenuTitle[indexPath.row - 1].id
                    Utility.shared.userData.companyName = theCurrentModel.arr_SelectCompany_LeftMenuTitle[indexPath.row - 1].name
                    UserDefault.shared.updateUserData()
                }
            } else if indexPath.row == 0 {
                if UserRole.superAdminUser == UserDefault.shared.userRole {
                    Utility.shared.userData.companyId = ""
                    Utility.shared.userData.companyName = ""
                    UserDefault.shared.updateUserData()
                }
            }
            
            theCurrentModel.selectedCompanyIndex = indexPath.row
            updateCompanyName(strCompanyName: arr_Menu.1[indexPath.row])
            reinitViewController()
            theCurrentView.updateBtnSelectCompanyArrow()
            tableView.reloadData()
            slideMenuController()?.closeLeft()
        } else {
            guard delegate != nil else {
                return
            }
            slideMenuController()?.closeLeft()
            delegate.redirectToLeftSideMenuViewController(strTitle: arr_Menu.1[indexPath.row])
        }
    }
}
