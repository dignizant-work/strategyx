//
//  LeftSideMenuModel.swift
//  StrategyX
//
//  Created by Haresh on 16/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class LeftSideMenuModel {
    //Variable
    weak var theController:LeftSideMenuVC!
    weak var theCurrentView:LeftSideMenuView!
    
    var arr_LeftMenuTitle:[String] = ["Strategy","Success Factors","Risks","Tactical Projects","Focus","Notes","Recurring Tasks","Reports"]
    var arr_LeftMenuIcon:[String] = ["ic_statregy_menu_screen","ic_critical_success_factors","ic_risks_menu_screen","ic_tactical_projects_menu_screen","ic_focus_menu_screen","ic_notes","ic_my_role_manu_screen","ic_report_profile_screen"]
    
    var arr_SelectCompany_LeftMenuTitle:[Company] = []
    
    
    var arr_SelectAdmin_LeftMenuTitle:[String] = ["Profile","Change Password","Logout"]
    var arr_SelectAdmin_LeftMenuIcon:[String] = ["ic_my_role_manu_screen","ic_change_password_manu_screen","ic_logout_menu_screen"]
    var selectedCompanyIndex = 0
    
    
    //LifeCycle
    init(theController:LeftSideMenuVC,theCurrentView:LeftSideMenuView) {
        self.theController = theController
        self.theCurrentView = theCurrentView
        
        arr_SelectAdmin_LeftMenuIcon.remove(at: 1)
        arr_SelectAdmin_LeftMenuTitle.remove(at: 1)
        
    }
    
//    MARK:- ViewModel
    func getTableviewList() -> ([String],[String])  {
        // icon, title
        
        if theCurrentView.btnUserArrow.isSelected {
             return (arr_SelectAdmin_LeftMenuIcon,arr_SelectAdmin_LeftMenuTitle)
        } else if theCurrentView.btnSelectCompanyArrow.isSelected {
            var title = arr_SelectCompany_LeftMenuTitle.map({$0.name})
            title.insert("All", at: 0)
            
             return ([],title)
        } else {
            return (arr_LeftMenuIcon,arr_LeftMenuTitle)
        }
    }
    
    
}
//MARK:- Api Call
extension LeftSideMenuModel {
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            if !Utility.shared.userData.companyId.isEmpty {
                self?.theController.updateCompanyName(strCompanyName: Utility.shared.userData.companyName.isEmpty ? "All" : Utility.shared.userData.companyName)
                if let index = list.firstIndex(where: {$0.id == Utility.shared.userData.companyId}) {
                    self?.selectedCompanyIndex = 1 + index
                }
            } else if Utility.shared.userData.companyId == "0" {
                self?.theController.updateCompanyName(strCompanyName: Utility.shared.userData.companyName.isEmpty ? "All" : Utility.shared.userData.companyName)
                if let index = list.firstIndex(where: {$0.id == Utility.shared.userData.companyId}) {
                    self?.selectedCompanyIndex = 1 + index
                }
            }
            self?.arr_SelectCompany_LeftMenuTitle = list
            self?.theCurrentView.reloadTableView()
            }, failed: { [weak self](error) in
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
