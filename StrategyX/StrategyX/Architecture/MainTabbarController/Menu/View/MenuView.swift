//
//  MenuView.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MenuView: ViewParentWithoutXIB {

    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint! // 325
    @IBOutlet weak var tableView: UITableView!
    
    func setupLayout() {
        
    }
    
    //setupTableview
    func setupTableView(theDelegate:MenuVC) {
        tableView.rowHeight = 65.0
        tableView.estimatedRowHeight = 65.0
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.registerCellNib(RightMenuTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }

}
