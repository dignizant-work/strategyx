//
//  RightMenuTVCell.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RightMenuTVCell: UITableViewCell {

    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewBadge: UIView!
    
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet weak var constrainWidthCount: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(strImg:String,strTitle:String, isForNotification:Bool = false) {
        img_icon.image = UIImage(named: strImg)
        lblTitle.text = strTitle
        viewBadge.isHidden = true
        
        var count = 0
        if strTitle.lowercased() == "Settings".lowercased() {
            if !Utility.shared.pushData.softwareUpdateCount.isEmpty {
                count = Int(Utility.shared.pushData.softwareUpdateCount) ?? 0
            }
        } else if strTitle.lowercased() == "Notifications".lowercased() {
            if !Utility.shared.pushData.notificationUnread.isEmpty {
                count = Int(Utility.shared.pushData.notificationUnread) ?? 0
            }
        } else if strTitle.lowercased() == "Whats New".lowercased() {
            if !Utility.shared.pushData.whatsnewUnread.isEmpty {
                count = Int(Utility.shared.pushData.whatsnewUnread) ?? 0
            }
        } else if strTitle.lowercased() == "Training".lowercased() {
            if !Utility.shared.pushData.trainningVideoCount.isEmpty {
                count = Int(Utility.shared.pushData.trainningVideoCount) ?? 0
            }
        }
        if count != 0 {
            viewBadge.isHidden = false
            lblBadgeCount.text = "\(count)"
        }

    }
    
}
