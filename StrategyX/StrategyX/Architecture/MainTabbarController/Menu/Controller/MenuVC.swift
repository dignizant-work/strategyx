//
//  MenuVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MenuVC: ParentViewController {

    //Varibale
    fileprivate lazy var theCurrentView:MenuView = { [unowned self] in
        return self.view as! MenuView
        }()
    
    fileprivate lazy var theCurrentModel:MenuModel = {
        return MenuModel(theController: self)
    }()
    
    //    private lazy var theCurrentModel: StoreItemDetailModel = {
    //        return StoreItemDetailModel(theController: self)
    //    }()
    
    weak var delegate:RightSideMenuDelegate!
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        theCurrentView.reloadTableView()
    }
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.reloadTableView()
        theCurrentView.constrainTableViewHeight.constant = CGFloat(theCurrentModel.arr_RightMenuTitle.count * 65)
        if theCurrentView.constrainTableViewHeight.constant > self.view.frame.size.height - 80 {
            theCurrentView.constrainTableViewHeight.constant = self.view.frame.size.height - 80
            theCurrentView.tableView.isScrollEnabled = true
        }
        //Rx Action
    }
    
    //RxAction
    
}

//MARK:- UITableViewDataSource
extension MenuVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.getTableviewList().1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arr_Menu = theCurrentModel.getTableviewList()
        let cell = tableView.dequeueReusableCell(withIdentifier: "RightMenuTVCell") as! RightMenuTVCell
        cell.configure(strImg: theCurrentModel.isSelectedIndex != indexPath.row ? arr_Menu.0[indexPath.row] : theCurrentModel.arr_RightMenuSelectedIcon[indexPath.row], strTitle: arr_Menu.1[indexPath.row], isForNotification: indexPath.row == 1)
        return cell
    }
}

//MARK:- UITableViewDelegate
extension MenuVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arr_Menu = theCurrentModel.getTableviewList()
        guard delegate != nil else {
            return
        }
        slideMenuController()?.closeRight()
        delegate.redirectToRightSideMenuViewController(strTitle: arr_Menu.1[indexPath.row])
    }
}

class NavigationMenuVC: UINavigationController {
    
}
