//
//  FeedbackView.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FeedbackView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnBug: UIButton!
    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var txtViewDescription: UITextView!
    
    @IBOutlet weak var lblPlaceHolderDescription: UILabel!
    
    let disposeBag = DisposeBag()
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        updateFeedbackTypebtn(tag: 1)
        onTextViewDescription()
    }
    
    func updateFeedbackTypebtn(tag:Int) {
        if btnRequest.tag == tag {
            btnRequest.backgroundColor = .clear
            btnRequest.isSelected = true
            btnRequest.setFontColor = 1
            
            btnBug.backgroundColor = .white
            btnBug.isSelected = false
            btnBug.setFontColor = 5
        } else {
            btnRequest.backgroundColor = .white
            btnRequest.isSelected = false
            btnRequest.setFontColor = 5
            
            btnBug.backgroundColor = .clear
            btnBug.isSelected = true
            btnBug.setFontColor = 1
        }
    }
    
    //RX Action
    func onTextViewDescription() {
        txtViewDescription.rx.text.orEmpty
            .subscribe(onNext:{ [weak self] (text) in
                self?.lblPlaceHolderDescription.isHidden = text.count > 0
            }).disposed(by: disposeBag)
    }
    
}
