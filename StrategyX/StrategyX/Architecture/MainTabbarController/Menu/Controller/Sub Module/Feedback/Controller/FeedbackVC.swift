//
//  FeedbackVC.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FeedbackView = { [unowned self] in
       return self.view as! FeedbackView
    }()
    
    fileprivate lazy var theCurrentModel:FeedbackModel = {
       return FeedbackModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        if Utility.shared.userData.userRating.isEmpty || Utility.shared.userData.userRating == "0" {
            presentRattingView()
        }
    }
    func reinitView() {
        theCurrentView.txtTitle.text = ""
        theCurrentView.txtViewDescription.text = ""
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    
    func validateForm() -> Bool {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter title")
            return false
        } else if theCurrentView.txtViewDescription.text.trimmed().isEmpty {
            self.showAlertAtBottom(message: "Please enter discription")
            return false
        }
        return true
    }
    
    //MARK:- Redirection
    func presentRattingView() {
        let vc = RattingVC.init(nibName: "RattingVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
//        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    
    //MARK:- IBAction
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnFeedbackTypeAction(_ sender: UIButton) {
        theCurrentView.updateFeedbackTypebtn(tag: sender.tag)
    }
    
    @IBAction func onBtnSendAction(_ sender: UIButton) {
        if !validateForm() {
            return
        }
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
//        feedback_type = 1(request) , feedback_type = 2(bug)
        let feedbackType = theCurrentView.btnRequest.isSelected ? "1" : "2"
        let title = theCurrentView.txtTitle.text ?? ""
        let description = theCurrentView.txtViewDescription.text ?? ""
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_feedback_type:feedbackType,
                         APIKey.key_title:title,
                         APIKey.key_description:description]
        
        SettingsWebService.shared.userFeedBack(parameter: parameter, success: { [weak self] (msg) in
                self?.showAlertAtBottom(message: msg)
                self?.reinitView()
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
                self?.backAction(isAnimation: false)
            
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
    }
    
    
}
