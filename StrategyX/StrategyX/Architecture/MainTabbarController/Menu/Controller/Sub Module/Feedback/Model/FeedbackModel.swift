//
//  FeedbackModel.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackModel {

    //MARK:- Variable
    fileprivate weak var theController:FeedbackVC!
    var imageBG:UIImage?
    
    //MARK:- LifeCycle
    init(theController:FeedbackVC) {
        self.theController = theController
    }
    
}
