//
//  SatisfactionVC.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionVC: ParentViewController {

    internal lazy var theCurrentView:SatisfactionView = { [unowned self] in
       return self.view as! SatisfactionView
    }()
    internal lazy var theCurrentViewModel:SatisfactionViewModel = {
        return SatisfactionViewModel.init(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentViewModel.getSatisfactionListWebservice(isRefreshing: true)
    }
    
    func setBackground(strMsg:String) {
        if theCurrentViewModel.arr_SatisfactionList == nil || theCurrentViewModel.arr_SatisfactionList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            UIView.performWithoutAnimation({
                theCurrentView.tableView.reloadData()
            })
        }
    }
    
    //MARK:- IBAction
    @objc func onBtnHeaderAction(_ sender: UIButton) {
        guard let arr_Satisfaction = theCurrentViewModel.arr_SatisfactionList else { return  }
        let section = sender.tag
        
        var indexPaths = [IndexPath]()
        indexPaths.append(IndexPath(row: 0, section: section))
        for row in arr_Satisfaction[sender.tag].1.responds.indices {
            let indxPath = IndexPath(row: row + 1, section: section)
            indexPaths.append(indxPath)
        }
        let isExpandable = !arr_Satisfaction[sender.tag].0
        theCurrentViewModel.arr_SatisfactionList![sender.tag].0 = isExpandable
        sender.setImage(UIImage.init(named: isExpandable ? "ic_up_arrow_menu_screen" : "ic_down_arrow_create account _screen"), for: .normal)
        if isExpandable {
            theCurrentView.tableView.insertRows(at: indexPaths, with: .fade)
        } else {
            theCurrentView.tableView.deleteRows(at: indexPaths, with: .fade)
        }
    }
    
    @objc func onBtnLoadMoreAction(_ sender:UIButton) {
        theCurrentViewModel.apiAtLoadMore = true
        sender.loadingIndicator(true, allignment: .right)
        theCurrentViewModel.getSatisfactionListWebservice()
    }
    
    @IBAction func onBtnHappyFaceAction(_ sender: UIButton) {
        theCurrentView.updateHappyFaceSelectionView(index: sender.tag)
        switch sender.tag {
        case 0:
            theCurrentViewModel.commentType = .veryHappy
            break
        case 1:
            theCurrentViewModel.commentType = .happy
            break
        case 2:
            theCurrentViewModel.commentType = .notSure
            break
        case 3:
            theCurrentViewModel.commentType = .notHappy
            break
        default:
            break
        }
        theCurrentViewModel.nextOffset = 0
        theCurrentViewModel.arr_SatisfactionList?.removeAll()
        theCurrentViewModel.arr_SatisfactionList = nil
        UIView.performWithoutAnimation {
            theCurrentView.tableView.reloadData()
        }
        theCurrentViewModel.getSatisfactionListWebservice(isRefreshing: true)
    }
    
}
