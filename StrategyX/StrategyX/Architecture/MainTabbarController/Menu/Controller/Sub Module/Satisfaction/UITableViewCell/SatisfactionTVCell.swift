//
//  SatisfactionTVCell.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(index:Int,strFirstName:String, strLastName:String, strDate:String,strDescription:String) {
        if index == 0 {
            lblTitle.attributedText = nil
            lblTitle.text = ""
            lblTitle.isHidden = true
        } else {
            lblTitle.isHidden = false
            var name = strFirstName + " " + strLastName
            var attributeString = name
            
            if UserDefault.shared.userRole != .superAdminUser {
                name = (strFirstName.trimmed().isEmpty ? "" : strFirstName.trimmed().substring(toIndex: 1)) + (strLastName.trimmed().isEmpty ? "" : strLastName.trimmed().substring(toIndex: 1))
                attributeString = name
            }
            attributeString += " On "
            if let date = strDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!date.isEmpty {
                attributeString += date
            }
            let attribute = NSMutableAttributedString(string: attributeString)
            attribute.addAttributes([NSAttributedString.Key.font : R15,NSAttributedString.Key.foregroundColor:appPlaceHolder], range: (attributeString as NSString).range(of: attributeString))
            attribute.addAttributes([NSAttributedString.Key.font : R15,NSAttributedString.Key.foregroundColor:appBlack], range: (attributeString as NSString).range(of: name))
            lblTitle.attributedText = attribute
        }
        
        lblDescription.text = strDescription
    }
}
