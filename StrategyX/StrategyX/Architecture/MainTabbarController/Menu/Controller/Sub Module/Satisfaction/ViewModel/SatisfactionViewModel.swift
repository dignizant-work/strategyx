//
//  SatisfactionViewModel.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionViewModel {

    //MARK:- Variable
    weak var theController:SatisfactionVC!
    
    var arr_SatisfactionList:[(Bool,Satisfaction)]?
    var nextOffset:Int = 0
    var commentType = CommentType.veryHappy
    var apiAtLoadMore = false
    
    
    //MARK:- Life Cycle
    init(theController:SatisfactionVC) {
        self.theController = theController
    }
    
    func updateList(list:[Satisfaction], feedbackCount:SatisfactionFeedBackCount, offset:Int, isRefreshing:Bool) {
        apiAtLoadMore = false
        if isRefreshing {
            arr_SatisfactionList?.removeAll()
        }
        nextOffset = offset
        if arr_SatisfactionList == nil {
            arr_SatisfactionList = []
        }
        list.forEach({ arr_SatisfactionList?.append((false, $0)) })
        print(arr_SatisfactionList!)
    }
    
}

//MARK:- Api Call
extension SatisfactionViewModel {
    func getSatisfactionListWebservice(isRefreshing:Bool = false)  {
        if isRefreshing {
            nextOffset = 0
            WebService().cancelledCurrentRequestifExist(url: BASEURL + POST_SatisfactionList)
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_page_offset:"\(nextOffset)",
                         APIKey.key_comment_type:commentType.rawValue]
        
        SettingsWebService.shared.getAllSatisfactionList(parameter: parameter, success: { [weak self] (list, feedbackCount, nextOffset) in
             self?.updateList(list: list, feedbackCount: feedbackCount, offset: nextOffset, isRefreshing: isRefreshing)
            (self?.theController.view as? SatisfactionView)?.updateHappyFaceSelectionTitle(theModel: feedbackCount)
            (self?.theController.view as? SatisfactionView)?.refreshControl.endRefreshing()
            UIView.performWithoutAnimation({
                (self?.theController.view as? SatisfactionView)?.tableView.reloadData()
            })
            }, failed: { [weak self] (error, feedbackCount) in
                (self?.theController.view as? SatisfactionView)?.refreshControl.endRefreshing()
                self?.updateList(list: [], feedbackCount: feedbackCount, offset: 0, isRefreshing: isRefreshing)
                (self?.theController.view as? SatisfactionView)?.updateHappyFaceSelectionTitle(theModel: feedbackCount)
                self?.theController.setBackground(strMsg: error)
        })
    }
}
