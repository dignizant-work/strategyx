//
//  SatisfactionView.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblVeryHappy: UILabel!
    @IBOutlet weak var lblHappy: UILabel!
    @IBOutlet weak var lblNotSure: UILabel!
    @IBOutlet weak var lblNotHappy: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_BottomLine: UIImageView!
    
    //    let refreshControl = UIRefreshControl()
    
    //MARK:- Life Cycle
    func setupLayout() {
        updateHappyFaceSelectionView(index: 0)
        updateHappyFaceSelectionTitle(theModel: SatisfactionFeedBackCount())
        DispatchQueue.main.async {
            self.img_BottomLine.frame.origin.x = 17.0
        }
        
    }
    
    func setupTableView(theDelegate:SatisfactionVC) {
        //        refreshControl.tintColor = appGreen
        //        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(SatisfactionTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
        //        tableView.contentInset = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: -15)
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? SatisfactionVC)?.theCurrentViewModel.getSatisfactionListWebservice(isRefreshing:true)
    }
    
    func updateHappyFaceSelectionView(index:Int) {
        [lblVeryHappy,lblHappy,lblNotSure,lblNotHappy].forEach({$0?.setFontColor = 4})
        var x:CGFloat = 17.0
        switch index {
        case 0:
            lblVeryHappy.setFontColor = 33
            x = lblVeryHappy.frame.origin.x
            break
        case 1:
            lblHappy.setFontColor = 33
            x = lblHappy.frame.origin.x
            break
        case 2:
            lblNotSure.setFontColor = 33
            x = lblNotSure.frame.origin.x
            break
        case 3:
            lblNotHappy.setFontColor = 33
            x = lblNotHappy.frame.origin.x
            break
        default:
            break
        }
        UIView.animate(withDuration: 0.2) {
            self.img_BottomLine.frame.origin.x = x
        }
    }
    func updateHappyFaceSelectionTitle(theModel:SatisfactionFeedBackCount) {
        var feedbackCount = 0
        lblVeryHappy.text = "Very Happy\n(0)"
        lblHappy.text = "Happy\n(0)"
        lblNotSure.text = "Not Sure\n(0)"
        lblNotHappy.text = "Not Happy\n(0)"

        if let count = Int(theModel.veryHappy) {
            feedbackCount += count
            lblVeryHappy.text = "Very Happy\n(\(count))"
        }
        if let count = Int(theModel.happy) {
            feedbackCount += count
            lblHappy.text = "Happy\n(\(count))"
        }
        if let count = Int(theModel.notSure) {
            feedbackCount += count
            lblNotSure.text = "Not Sure\n(\(count))"
        }
        if let count = Int(theModel.notHappy) {
            feedbackCount += count
            lblNotHappy.text = "Not Happy\n(\(count))"
        }
        lblTitle.text = "Customer Satisfaction (\(feedbackCount))"
    }
}
