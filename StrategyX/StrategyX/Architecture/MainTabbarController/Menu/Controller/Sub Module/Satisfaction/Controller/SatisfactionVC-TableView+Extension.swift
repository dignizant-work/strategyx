//
//  SatisfactionVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:- UITableViewDataSource
extension SatisfactionVC:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return theCurrentViewModel.arr_SatisfactionList?.count ?? 10
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let arr = theCurrentViewModel.arr_SatisfactionList else { return 0 }
        
        if !arr[section].0 {
            return 0
        }
        
        return arr[section].1.responds.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SatisfactionTVCell") as! SatisfactionTVCell
        if indexPath.row == 0 {
            cell.configureCell(index: 0, strFirstName: (theCurrentViewModel.arr_SatisfactionList?[indexPath.section].1.createdByFirstname ?? ""), strLastName: (theCurrentViewModel.arr_SatisfactionList?[indexPath.section].1.createdByLastname ?? ""), strDate: theCurrentViewModel.arr_SatisfactionList?[indexPath.section].1.createdDate ?? "", strDescription: theCurrentViewModel.arr_SatisfactionList?[indexPath.section].1.comment ?? "")
        } else {
            let index = indexPath.row - 1
            if let responds = theCurrentViewModel.arr_SatisfactionList?[indexPath.section].1.responds, responds.indices.contains(index) {
                cell.configureCell(index: indexPath.row, strFirstName: responds[index].createdByFirstname, strLastName: responds[index].createdByLastname , strDate: responds[index].createdDate, strDescription: responds[index].respondComment)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: view.center.y - 13, width: 26, height: 26))
        switch theCurrentViewModel.commentType {
        case .veryHappy:
            imageView.image = UIImage.init(named: "ic_very_happy_select_mini")
            break
        case .happy:
            imageView.image = UIImage.init(named: "ic_happy_select_mini")
            break
        case .notSure:
            imageView.image = UIImage.init(named: "ic_not_sare_select_mini")
            break
        case .notHappy:
            imageView.image = UIImage.init(named: "ic_not_happy_select_mini")
            break
        }
        let lblTitle = UILabel.init(frame: CGRect.init(x: imageView.frame.origin.x + imageView.frame.size.width + 8, y: 0, width: view.frame.size.width - (imageView.frame.origin.x + imageView.frame.size.width + 8 + 35) , height: 50))
        lblTitle.setFont = "R15"
        lblTitle.setFontColor = 4
        lblTitle.numberOfLines = 1
        lblTitle.attributedText = nil
        lblTitle.text = ""
        
        let btnArrow = UIButton.init(frame: view.frame)
        
        if let arr = theCurrentViewModel.arr_SatisfactionList, arr.count > 0 {
            btnArrow.hideSkeleton()
            var name = (theCurrentViewModel.arr_SatisfactionList?[section].1.createdByFirstname ?? "") + " " + (theCurrentViewModel.arr_SatisfactionList?[section].1.createdByLastname ?? "")
            var attributeString = name
            
            if UserDefault.shared.userRole != .superAdminUser {
                name = ""
                if !(theCurrentViewModel.arr_SatisfactionList?[section].1.createdByFirstname ?? "").trimmed().isEmpty {
                    name = (theCurrentViewModel.arr_SatisfactionList?[section].1.createdByFirstname ?? "").trimmed().substring(toIndex: 1)
                }
                if !(theCurrentViewModel.arr_SatisfactionList?[section].1.createdByLastname ?? "").trimmed().isEmpty {
                    name += (theCurrentViewModel.arr_SatisfactionList?[section].1.createdByLastname ?? "").trimmed().substring(toIndex: 1)
                }
//                name = (theCurrentViewModel.arr_SatisfactionList?[section].1.createdByName ?? "").acronym()
                attributeString = name
            }
            attributeString += " On "
            if let strDate = theCurrentViewModel.arr_SatisfactionList?[section].1.createdDate, let date = strDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!date.isEmpty {
                attributeString += date
            }
            let attribute = NSMutableAttributedString(string: attributeString)
            attribute.addAttributes([NSAttributedString.Key.font : R15,NSAttributedString.Key.foregroundColor:appPlaceHolder], range: (attributeString as NSString).range(of: attributeString))
            attribute.addAttributes([NSAttributedString.Key.font : R15,NSAttributedString.Key.foregroundColor:appBlack], range: (attributeString as NSString).range(of: name))
            lblTitle.attributedText = attribute
        } else {
            btnArrow.showSkeleton()
            btnArrow.showAnimatedSkeleton()
        }
        
        btnArrow.contentHorizontalAlignment = .right
        btnArrow.setImage(UIImage.init(named: (theCurrentViewModel.arr_SatisfactionList?[section].0 ?? false) ? "ic_up_arrow_menu_screen" : "ic_down_arrow_create account _screen"), for: .normal)
        //
        btnArrow.tag = section
        btnArrow.addTarget(self, action: #selector(onBtnHeaderAction(_:)), for: .touchUpInside)
        view.addSubview(imageView)
        view.addSubview(lblTitle)
        view.addSubview(btnArrow)
        return view
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        print("section:=",section)
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        if theCurrentViewModel.nextOffset != 0, let arr = theCurrentViewModel.arr_SatisfactionList, arr.count > 0, theCurrentViewModel.nextOffset != -1, section == arr.count - 1 {
            let viewLine = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
            viewLine.setBackGroundColor = 11
            view.setBackGroundColor = 1
            view.frame.size = CGSize(width: tableView.frame.size.width, height: 50)
            let btnTitle = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: 50))
            btnTitle.setFont = "R13"
            btnTitle.setFontColor = 3
            let textRange = NSMakeRange(0, "View More".count)
            let attribute = NSMutableAttributedString(string: "View More")
            attribute.addAttributes([NSAttributedString.Key.font : R15,NSAttributedString.Key.foregroundColor:appGreen, NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue,NSAttributedString.Key.underlineColor:appGreen], range:textRange)
            btnTitle.setAttributedTitle(attribute, for: .normal)
            btnTitle.addTarget(self, action: #selector(onBtnLoadMoreAction(_:)), for: .touchUpInside)
            btnTitle.loadingIndicator(theCurrentViewModel.apiAtLoadMore, allignment: .right)
            view.addSubview(viewLine)
            view.addSubview(btnTitle)
            return view
        }
        view.setBackGroundColor = 11
        return view
    }
}
//MARK:- UITableViewDelegate
extension SatisfactionVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let arr = theCurrentViewModel.arr_SatisfactionList else { return 1.0 }
        if theCurrentViewModel.nextOffset != 0, arr.count > 0, theCurrentViewModel.nextOffset != -1, section == arr.count - 1 {
           return 50.0
        }
        return 1.0
    }
    
}
