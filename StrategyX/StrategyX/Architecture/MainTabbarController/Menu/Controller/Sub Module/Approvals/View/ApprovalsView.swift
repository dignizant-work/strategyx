//
//  ApprovalsView.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ApprovalsView: ViewParentWithoutXIB {

    //MARK:- Variabel
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
//    @IBOutlet weak var btnSent: UIButton!
//    @IBOutlet weak var btnRequested: UIButton!
//    @IBOutlet weak var btnApproval: UIButton!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    @IBOutlet weak var img_DropDownArrow: UIImageView!

    
//    let refreshControl = UIRefreshControl()
    
    //MARK:- Life Cycle
    func setupLayout() {
        updateFilterCountText(strCount: "")
//        updateHeaderBtnSelection(index: 0)
//        updateHeaderBtnSelectionTitle(sentCount: 0, approvalCount: 0, requestedCount: 0)
    }
    
    func setupTableView(theDelegate:ApprovalsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(ApprovalsTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
       
        
//        tableView.contentInset = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: -15)
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? ApprovalsVC)?.refreshApiCall()

    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    func updateDropDownTitle(strTitle:String) {
        lblDropDownTitle.text = strTitle
    }
    /*
    func updateHeaderBtnSelection(index:Int) {
        [btnSent, btnApproval, btnRequested].forEach({
            $0?.setBackGroundColor = 1
            $0?.setFontColor = 3
        })
        switch index {
        case 0:
            btnSent.setBackGroundColor = 3
            btnSent.setFontColor = 1
            break
        case 1:
            btnApproval.setBackGroundColor = 3
            btnApproval.setFontColor = 1
            break
        case 2:
            btnRequested.setBackGroundColor = 3
            btnRequested.setFontColor = 1
            break
        default:
            btnSent.setBackGroundColor = 3
            btnSent.setFontColor = 1
            break
        }
    }
 
    func updateHeaderBtnSelectionTitle(sentCount:Int,approvalCount:Int,requestedCount:Int) {
        btnSent.setTitle(" Sent (\(sentCount)) ", for: .normal)
        btnApproval.setTitle(" Approvals (\(approvalCount)) ", for: .normal)
        btnRequested.setTitle(" Requested (\(requestedCount)) ", for: .normal)
    }
    
    func updateIndividualHeaderBtnSelectionTitle(sentCount:Int = -1,approvalCount:Int = -1,requestedCount:Int = -1) {
        if sentCount != -1 {
            btnSent.setTitle(" Sent (\(sentCount)) ", for: .normal)
        }
        if approvalCount != -1 {
            btnApproval.setTitle(" Approvals (\(approvalCount)) ", for: .normal)
        }
        if requestedCount != -1 {
            btnRequested.setTitle(" Requested (\(requestedCount)) ", for: .normal)
        }
    }*/
    
}
