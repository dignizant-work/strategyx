//
//  ApprovalsModel.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown

class ApprovalsModel {

    //MARK:- Variable
    fileprivate weak var theController:ApprovalsVC!
    
    enum titleDropDownType:String {
        case sentApproval                   = "Sent for Approval"
        case yourApproval                   = "For Your Approval"
        case dueDateRequest                 = "Due Date Request"
    }

    var titleTypeDD = DropDown()
    var arr_TitleType = [titleDropDownType.sentApproval.rawValue,titleDropDownType.yourApproval.rawValue,titleDropDownType.dueDateRequest.rawValue]
    var selectedTitleType = titleDropDownType.sentApproval

    
    let identifier = "ApprovalsTVCell"
//    var selectedTag = 0
    var arr_ApprovalList:[ActionsList]?
    var nextOffset = 0
    var titleDropDownTypeCount = (sent:0, your:0, dueDate:0)
    
    var approvalCount:Int = 0 {
        didSet {
            AppDelegate.shared.approvalNotificationCount = approvalCount
        }
    }
    
    var apiLoadingAtApprovalID:[String] = []
    
    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)


    //MARK:- LifeCycle
    init(theController:ApprovalsVC) {
        self.theController = theController
    }
    
    func updateList(list:[ActionsList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_ApprovalList?.removeAll()
        }
        nextOffset = offset
        if arr_ApprovalList == nil {
            arr_ApprovalList = []
        }
        list.forEach({ arr_ApprovalList?.append($0) })
        print(arr_ApprovalList!)
    }
    
    func configure(dropDown:DropDown,view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 35)
        
        switch dropDown {
        case titleTypeDD:
            dropDown.dataSource = arr_TitleType
            break
        default:
            break
        }
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.update(text: item, dropDown: dropDown)
        }
        dropDown.cancelAction = { [unowned self] in
            self.rotateDropdownArrow()
        }
    }
    
    func update(text:String,dropDown:DropDown) {
        rotateDropdownArrow()
        
        switch dropDown {
        case titleTypeDD:
            let arr_txt = text.components(separatedBy: " (")
            
            let textUpdate = arr_txt.count > 0 ? arr_txt[0]: text
            switch textUpdate.localizedLowercase {
            case titleDropDownType.sentApproval.rawValue.localizedLowercase:
                selectedTitleType = .sentApproval
                break
            case titleDropDownType.yourApproval.rawValue.localizedLowercase:
                selectedTitleType = .yourApproval
                break
            case titleDropDownType.dueDateRequest.rawValue.localizedLowercase:
                selectedTitleType = .dueDateRequest
                break
            default:
                break
            }
            (theController.view as? ApprovalsView)?.lblDropDownTitle.text = selectedTitleType.rawValue
            break
        default:
            break
        }
        nextOffset = 0
        arr_ApprovalList?.removeAll()
        arr_ApprovalList = nil
        (theController.view as? ApprovalsView)?.tableView.backgroundView = nil
        (theController.view as? ApprovalsView)?.tableView.reloadData()
        theController.refreshApiCall()
    }
    func rotateDropdownArrow(at rotate:CGFloat = 0) {
        (theController.view as? ApprovalsView)?.img_DropDownArrow.rotateArrow(at:rotate)
    }
}
