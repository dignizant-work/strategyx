//
//  ApprovalsTVCell.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ApprovalsTVCell: UITableViewCell {
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var progressBar: CircularProgressBar!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var constrainbtnArrowWidth: NSLayoutConstraint! // 0.0
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!
    @IBOutlet weak var constrainViewNotesWidth: NSLayoutConstraint! // 22.0
//    @IBOutlet weak var constrainViewNotesLeading: NSLayoutConstraint! // 5.0
    @IBOutlet weak var lblRequest: UILabel!
    @IBOutlet weak var lblApproved: UILabel!

    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var viewType: UIView!

    var handleDueDateAction:() -> Void = {}
    var handleWorkDateAction:() -> Void = {}
    var handlerPercentCompletedAction:() -> Void = {}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        [img_Profile,lblTitle,lblSubTitle,viewNotesCount,lblDueDate].forEach({ $0.showAnimatedSkeleton() })
        setProgressBar(progress: 0.0)
        lblTitle.text = " "
        lblSubTitle.text = " "
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func startAnimation() {
        lblDueDate.setBackGroundColor = 0
        img_Profile.isHidden = false
        lblRequest.isHidden = true
        lblApproved.isHidden = true
        lblTitle.text = " "
        lblSubTitle.text = " "
        setProgressBar(progress: 0.0)
        lblTypeTitle.text = ""
        viewType.backgroundColor = white
        [img_Profile,lblTitle,lblSubTitle,viewNotesCount,lblDueDate].forEach({ $0.showAnimatedSkeleton() })
    }
    func hideAnimation() {
        [img_Profile,lblTitle,lblSubTitle,viewNotesCount,lblDueDate].forEach({ $0?.hideSkeleton() })
    }
    
    func configure(strTitle:String,strSubTitle:String, progress:Double) {
        lblTitle.text = strTitle
        lblSubTitle.text = strSubTitle
        progressBar.isHidden = false
        setProgressBar(progress: progress)
    }
    func configureActions(theModel:ActionsList, isApiLoading:Bool = false) {
        hideAnimation()
        btnArrow.isHidden = !isApiLoading
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblApproved.isHidden = theModel.approveFlag == 1 ? false : true
        lblRequest.isHidden = theModel.requestFlag == 1 ? false : true
        constrainbtnArrowWidth.constant = isApiLoading ? 30.0 : 0.0
        btnArrow.loadingIndicator(isApiLoading)
        lblProfile.text = theModel.assignedTo.acronym()
        if theModel.assignedToUserImage.isEmpty && theModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID: theModel.assigneeId) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
        
//        img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.assignedToUserImage, placeholder: "ic_mini_plash_holder")
        //        sd_setImage(with:  URL(string: theModel.assignedToUserImage), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))
        
        lblNotesCount.text = "\(theModel.notesCount)"
        viewNotesCount.setBackGroundColor = theModel.notesCount > 0 ? 9 : 3
        
//        lblDueDate.setBackGroundColor = theModel.requestedRevisedDate.isEmpty ? 0 : 30
        
        lblTitle.text = theModel.actionlogTitle
        let percentage = (Double(theModel.percentageCompeleted) ?? 0.0) / 100
        setProgressBar(progress: percentage)
        
        let attributeString = NSMutableAttributedString()
        
        var Subtitle = ""
        if let workDate = theModel.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType10),!workDate.isEmpty {
            Subtitle = "W: " + workDate
        }else{
            Subtitle = "W: No Work Date"
        }
//        Subtitle += " "
        let attributeStringWork =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R12,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theModel.workdateColor)])
        attributeString.append(attributeStringWork)
        lblSubTitle.attributedText = attributeStringWork

        var dueDateString = ""
        
        if let dueDate = theModel.duedate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType15),!dueDate.isEmpty {
            if Subtitle.isEmpty {
                Subtitle += "D: " + dueDate
                dueDateString = "D: " + dueDate
            } else {
                Subtitle += "D: " + dueDate
                dueDateString = "D: " + dueDate
            }
            dueDateString = dueDate

        }else{
            Subtitle += "D: " + "No Due Date"
            dueDateString = "D: " + "No Due Date"
            dueDateString = "No Due Date"

        }
        let attributeStringDue =  NSAttributedString(string: dueDateString, attributes: [NSAttributedString.Key.font:R12,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theModel.duedateColor)])
        attributeString.append(attributeStringDue)

        lblDueDate.attributedText = attributeStringDue
        
        lblTypeTitle.text = Utility.shared.setColorBGAccordingToActions(type: "\(theModel.type)").1
        lblTypeTitle.rotate(degrees: 270)
        lblTypeTitle.frame = CGRect(x:0, y:0, width:15, height:viewType.frame.size.height)
        viewType.backgroundColor = ColorType().setColor(index: Utility.shared.setColorBGAccordingToActions(type: "\(theModel.type)").0)


    }
    
    func configureApproval(theModel:Approve, isApiLoading:Bool) {
        hideAnimation()
        btnArrow.isHidden = !isApiLoading
        self.contentView.isUserInteractionEnabled = !isApiLoading
        constrainbtnArrowWidth.constant = isApiLoading ? 30.0 : 0.0
        btnArrow.loadingIndicator(isApiLoading)
        // TODO:- Add assign to
        //  lblProfile.text = theModel.approvedBy.acronym()
       /* if theModel.assignedToUserImage.isEmpty && theModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
        } else {
            if !theModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                if theModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                }
            }
        }*/

//        img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.assignedToUserImage, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with:  URL(string: theModel.assignedToUserImage), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))
        lblTitle.text = theModel.actionlogTitle
        let percentage = (Double(theModel.percentageCompeleted) ?? 0.0) / 100
        setProgressBar(progress: percentage)
        
//        viewBG.setBackGroundColor = theModel.requestedRevisedDate.isEmpty ? 1 : 30

        var Subtitle = ""
        if let dueDate = theModel.workDate.convertToDate(formate: DateFormatterInputType.inputType1) {
            let local = dueDate.toLocalTime().string(withFormat: DateFormatterOutputType.outputType1)
            Subtitle = "W: " + local
        }
        
        if let revisedDate = theModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1) {
            let local = revisedDate.toLocalTime().string(withFormat: DateFormatterOutputType.outputType1)
            if Subtitle.isEmpty {
                Subtitle += "D: " + local
            } else {
                Subtitle += ", D: " + local
            }
        }
        
        lblSubTitle.text = Subtitle
    }
    
    func setProgressBar(progress:Double) {
        
        progressBar.foregroundStrokeColor = appCyanDarkColor
        progressBar.backgroundStrokeColor = appSepratorColor
        progressBar.LabelFontColor = appTextBlackShedColor
        progressBar.labelSize = 0
        progressBar.lineWidth = 4
        progressBar.safePercent = 100
        progressBar.setProgress(to: progress, withAnimation: true)
    }
    
    func hideViewNotes() {
        viewNotesCount.isHidden = true
        constrainViewNotesWidth.constant = 0
//        constrainViewNotesLeading.constant = 0
    }
    
    //MARK:- Action
    @IBAction func onBtnSubTitleAction(_ sender: Any) {
        handleWorkDateAction()
    }
    
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        handleDueDateAction()
    }
    
    @IBAction func onBtnPercentCompletedAction(_ sender: Any) {
        handlerPercentCompletedAction()
    }
}
