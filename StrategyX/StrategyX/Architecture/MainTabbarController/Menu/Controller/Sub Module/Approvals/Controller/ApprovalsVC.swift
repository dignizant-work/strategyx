//
//  ApprovalsVC.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class ApprovalsVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:ApprovalsView = { [unowned self] in
       return self.view as! ApprovalsView
    }()
    fileprivate lazy var theCurrentModel:ApprovalsModel = {
       return ApprovalsModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentModel.configure(dropDown: theCurrentModel.titleTypeDD, view: theCurrentView.viewTitle)
        theCurrentModel.selectedTitleType = .yourApproval
        theCurrentModel.update(text: theCurrentModel.selectedTitleType.rawValue, dropDown: theCurrentModel.titleTypeDD)
//        getApprovalListWebService()
        updateDropDownTitle()
    }
    
    func reInitData() {        
        theCurrentView.tableView.backgroundView = nil
        theCurrentModel.arr_ApprovalList?.removeAll()
        theCurrentModel.arr_ApprovalList = nil
        theCurrentView.tableView.reloadData()
        theCurrentModel.selectedTitleType = .yourApproval
        theCurrentModel.update(text: theCurrentModel.selectedTitleType.rawValue, dropDown: theCurrentModel.titleTypeDD)
        getApprovalListWebService(isRefreshing: true)
    }
    
    func refreshApiCall() {
//        theCurrentModel.nextOffset = 0
//        theCurrentModel.arr_ApprovalList?.removeAll()
//        theCurrentModel.arr_ApprovalList = nil
//        theCurrentView.tableView.backgroundView = nil
//        theCurrentView.tableView.reloadData()
        
        getApprovalListWebService(isRefreshing: true)
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_ApprovalList == nil || theCurrentModel.arr_ApprovalList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    func updateCountFromPush(){
        theCurrentModel.titleDropDownTypeCount.sent = Int(Utility.shared.pushData.sentApprovalCount) ?? 0
        theCurrentModel.titleDropDownTypeCount.your = Int(Utility.shared.pushData.forYourApprovalCount) ?? 0
        theCurrentModel.titleDropDownTypeCount.dueDate = Int(Utility.shared.pushData.dueDateRequestCount) ?? 0
        updateDropDownTitle()
    }
    func updateDropDownTitle() {
//        var title = theCurrentModel.selectedTitleType.rawValue + " (\(0))"
        
        switch theCurrentModel.selectedTitleType {
        case .sentApproval:
//            title = theCurrentModel.selectedTitleType.rawValue + " (\(theCurrentModel.titleDropDownTypeCount.sent))"
            Utility.shared.pushData.sentApprovalCount = "\(theCurrentModel.titleDropDownTypeCount.sent)"
            break
        case .yourApproval:
//            title = theCurrentModel.selectedTitleType.rawValue + " (\(theCurrentModel.titleDropDownTypeCount.your))"
            Utility.shared.pushData.forYourApprovalCount = "\(theCurrentModel.titleDropDownTypeCount.your)"
            break
        case .dueDateRequest:
//            title = theCurrentModel.selectedTitleType.rawValue + " (\(theCurrentModel.titleDropDownTypeCount.dueDate))"
            Utility.shared.pushData.dueDateRequestCount = "\(theCurrentModel.titleDropDownTypeCount.dueDate)"
            break
        }
        theCurrentModel.approvalCount = theCurrentModel.titleDropDownTypeCount.sent + theCurrentModel.titleDropDownTypeCount.your + theCurrentModel.titleDropDownTypeCount.dueDate
        theCurrentView.updateDropDownTitle(strTitle: theCurrentModel.selectedTitleType.rawValue)
    }
    
    func showConformation(strApprovalID: String) {
        theCurrentModel.apiLoadingAtApprovalID.append(strApprovalID)
        updateBottomCell(loadingID: strApprovalID, isLoadingApi: true)
        approveActionsWebService(strApprovalID: strApprovalID)
        /*
        self.showAlertAction(title: "StrategyX", msg: "Are you sure want to approve?") { [weak self] in
            
        }*/
    }
    
    func showConformationForDate(strApprovalID: String, strRevisedDate:String) {
        self.showAlertAction(title: "StrategyX", msg: "Are you sure you want to approve this revised date?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtApprovalID.append(strApprovalID)
            self?.updateBottomCell(loadingID: strApprovalID, isLoadingApi: true)
            self?.sendRevisedDueDateRequestWebservice(strActionID: strApprovalID, revisedDate: strRevisedDate, isForApprove: true)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
//        theCurrentModel.apiLoadingAtApprovalID = isLoadingApi ? atSectionIndex : -1
        guard let model = theCurrentModel.arr_ApprovalList else { return  }
        var indexRow = -1
        
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtApprovalID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtApprovalID.remove(at: index)
            }
            theCurrentModel.arr_ApprovalList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
            var count = 0
            switch theCurrentModel.selectedTitleType {
            case .sentApproval:
                count = theCurrentModel.arr_ApprovalList?.count ?? 0
                theCurrentModel.titleDropDownTypeCount.sent = count
//                theCurrentView.updateIndividualHeaderBtnSelectionTitle(sentCount: count)
                if theCurrentModel.arr_ApprovalList!.count == 0 {
                    let msg = "Sent Approval List Not Available"
                    setBackground(strMsg: msg)
                }
                break
            case .yourApproval:
                count = theCurrentModel.arr_ApprovalList?.count ?? 0
                theCurrentModel.titleDropDownTypeCount.your = count
//                theCurrentView.updateIndividualHeaderBtnSelectionTitle(approvalCount: count)
                if theCurrentModel.arr_ApprovalList!.count == 0 {
                    let msg = "Great Job!! \n\nMore Actions Completed!\n\nA few Steps closer to achieving \nyour goals.."
                    setBackground(strMsg: msg)
                }
                break
            case .dueDateRequest:
                count = theCurrentModel.arr_ApprovalList?.count ?? 0
                theCurrentModel.titleDropDownTypeCount.dueDate = count
//                theCurrentView.updateIndividualHeaderBtnSelectionTitle(requestedCount: count)
                if theCurrentModel.arr_ApprovalList!.count == 0 {
                    let msg = "No Due Date Requests"
                    setBackground(strMsg: msg)
                }
                break
            }
            updateDropDownTitle()
//            theCurrentModel.approvalCount = theCurrentModel.arr_ApprovalList?.count ?? 0
            
//            theCurrentView.tableView.deleteSections(IndexSet.init(integer: indexRow), with: .fade)
            /*if theCurrentModel.arr_ApprovalList?.count == 0 {
                let msg = "Great Job!! \n\nMore Actions Completed!\n\nA few Steps closer to achieving \nyour goals.."
                setBackground(strMsg: msg)
            }*/
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
        } else {
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtApprovalID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtApprovalID.remove(at: index)
                }
            }
            let contentOffset = theCurrentView.tableView.contentOffset
            let index = IndexSet.init(integer: indexRow)

            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadSections(index, with: .fade)
//                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
      
    }
    
    func updateActionListModel(theActionDetailModel:ActionsList?) {
        guard let model = theActionDetailModel else { return }
        
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentModel.arr_ApprovalList![index].actionlogTitle = model.actionlogTitle
            theCurrentModel.arr_ApprovalList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_ApprovalList![index].duedate = model.duedate
            theCurrentModel.arr_ApprovalList![index].workDate = model.workDate
            theCurrentModel.arr_ApprovalList![index].percentageCompeleted = model.percentageCompeleted
            theCurrentModel.arr_ApprovalList![index].assigneeId = model.assigneeId
            theCurrentModel.arr_ApprovalList![index].assignedTo = model.assignedTo
            theCurrentModel.arr_ApprovalList![index].assignedToUserImage = model.assignedToUserImage
            if model.percentageCompeleted != "100" {
                if theCurrentModel.selectedTitleType != .dueDateRequest {
                    theCurrentModel.arr_ApprovalList!.remove(at: index)
                    theCurrentView.tableView.deleteSections(IndexSet.init(integer: index), with: .fade)
                }

                var count = 0
                switch theCurrentModel.selectedTitleType {
                case .sentApproval:
                    count = theCurrentModel.arr_ApprovalList?.count ?? 0
                    theCurrentModel.titleDropDownTypeCount.sent = count
//                    theCurrentView.updateIndividualHeaderBtnSelectionTitle(sentCount: count)
                    if theCurrentModel.arr_ApprovalList!.count == 0 {
                        let msg = "Sent Approval List Not Available"
                        setBackground(strMsg: msg)
                    }
                    break
                case .yourApproval:
                    count = theCurrentModel.arr_ApprovalList?.count ?? 0
                    theCurrentModel.titleDropDownTypeCount.your = count
//                    theCurrentView.updateIndividualHeaderBtnSelectionTitle(approvalCount: count)
                    if theCurrentModel.arr_ApprovalList!.count == 0 {
                        let msg = "Great Job!! \n\nMore Actions Completed!\n\nA few Steps closer to achieving \nyour goals.."
                        setBackground(strMsg: msg)
                    }
                    break
                case .dueDateRequest:
                    count = theCurrentModel.arr_ApprovalList?.count ?? 0
                    theCurrentModel.titleDropDownTypeCount.dueDate = count
//                    theCurrentView.updateIndividualHeaderBtnSelectionTitle(requestedCount: count)
                    if theCurrentModel.arr_ApprovalList!.count == 0 {
                        let msg = "No Due Date Requests"
                        setBackground(strMsg: msg)
                    }
                    break
                }
                updateDropDownTitle()
//                theCurrentModel.approvalCount = theCurrentModel.arr_ApprovalList?.count ?? 0
                /*if theCurrentModel.arr_ApprovalList!.count == 0 {
                    let msg = "Great Job!! \n\nMore Actions Completed!\n\nA few Steps closer to achieving \nyour goals.."
                    setBackground(strMsg: msg)
                }*/
            } else {
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadSections(IndexSet.init(integer: index), with: .none)
//                    reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
    }
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_ApprovalList?[index].notesCount = 0
            
            let contentOffset = theCurrentView.tableView.contentOffset
            let index = IndexSet.init(integer: index)
            
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadSections(index, with: .fade)
                //                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: { $0.actionlogId == model.actionlogId }) {
            theCurrentModel.arr_ApprovalList?[index] = model
            
            let contentOffset = theCurrentView.tableView.contentOffset
            let index = IndexSet.init(integer: index)
            
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadSections(index, with: .fade)
                //                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtApprovalID.append(strActionLogID)
            } else {
                //                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = theCurrentModel.apiLoadingAtApprovalID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtApprovalID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_ApprovalList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_ApprovalList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_ApprovalList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_ApprovalList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_ApprovalList![index].workdateColor = model.workdateColor
                        theCurrentModel.arr_ApprovalList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexset = IndexSet(integer: index)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadSections(indexset, with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentModel.arr_ApprovalList![index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentModel.arr_ApprovalList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_ApprovalList![index].revisedColor = model.revisedColor
            theCurrentModel.arr_ApprovalList![index].duedate = model.revisedDate
            theCurrentModel.arr_ApprovalList![index].duedateColor = model.duedateColor
            theCurrentModel.arr_ApprovalList![index].requestFlag = model.requestFlag
            theCurrentModel.arr_ApprovalList![index].approveFlag = model.approveFlag

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentModel.arr_ApprovalList?[index].percentageCompeleted = "\(percentage)"
            theCurrentModel.arr_ApprovalList?[index].approveFlag = percentage == 100 ? 1 : 0
            updateActionListModel(theActionDetailModel: theCurrentModel.arr_ApprovalList?[index])
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel) in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            self?.updateBottomCell(loadingID: theModel?.actionlogId ?? "", isRemove: true)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .approvals)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_ApprovalList?.removeAll()
            self?.theCurrentModel.arr_ApprovalList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getApprovalListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    /*
    func presentRiskFilter() {
        let vc = RiskFilterVC.init(nibName: "RiskFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .approvals)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_ApprovalList?.removeAll()
            self?.theCurrentModel.arr_ApprovalList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getApprovalListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }*/
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionListModel) in
            if self?.theCurrentModel.selectedTitleType == .dueDateRequest ,let model = theActionListModel, model.requestedRevisedDate == "" ,let index = self?.theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                self?.theCurrentModel.arr_ApprovalList!.remove(at: index)
                self?.theCurrentView.tableView.deleteSections(IndexSet.init(integer: index), with: .fade)
            }
            self?.updateActionListModel(theActionDetailModel: theActionListModel)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theActionListModel) in
            if self?.theCurrentModel.selectedTitleType == .dueDateRequest ,let model = theActionListModel, model.requestedRevisedDate == "" ,let index = self?.theCurrentModel.arr_ApprovalList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                self?.theCurrentModel.arr_ApprovalList!.remove(at: index)
                self?.theCurrentView.tableView.deleteSections(IndexSet.init(integer: index), with: .fade)
            }
            self?.updateActionListModel(theActionDetailModel: theActionListModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionFilter()
    }
    @IBAction func onBtnDropDownAction(_ sender: Any) {
        let arr = [theCurrentModel.arr_TitleType[0] + " (\(theCurrentModel.titleDropDownTypeCount.sent))",theCurrentModel.arr_TitleType[1] + " (\(theCurrentModel.titleDropDownTypeCount.your))",theCurrentModel.arr_TitleType[2] + " (\(theCurrentModel.titleDropDownTypeCount.dueDate))"]
        theCurrentModel.titleTypeDD.dataSource = arr
        theCurrentModel.titleTypeDD.show()
        theCurrentModel.rotateDropdownArrow(at: CGFloat.pi)
    }
    /*
    @IBAction func onBtnHeaderAction(_ sender: UIButton) {
        theCurrentView.updateHeaderBtnSelection(index: sender.tag)
        theCurrentModel.selectedTitleType = sender.tag
        theCurrentModel.nextOffset = 0
        theCurrentModel.arr_ApprovalList?.removeAll()
        theCurrentModel.arr_ApprovalList = nil
        theCurrentView.tableView.backgroundView = nil
        theCurrentView.tableView.reloadData()
        getApprovalListWebService(isRefreshing: true)
    }*/
    
}

//MARK:-UITableViewDataSource
extension ApprovalsVC:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_ApprovalList == nil || theCurrentModel.arr_ApprovalList?.count == 0) {
            return 0
        }
        return theCurrentModel.arr_ApprovalList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_ApprovalList == nil || theCurrentModel.arr_ApprovalList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: theCurrentModel.identifier) as! ApprovalsTVCell
        if theCurrentModel.arr_ApprovalList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtApprovalID.contains(theCurrentModel.arr_ApprovalList![indexPath.section].actionlogId) {
                isApiLoading = true
            }
            cell.configureActions(theModel: theCurrentModel.arr_ApprovalList![indexPath.section], isApiLoading: isApiLoading)
            if theCurrentModel.selectedTitleType == .dueDateRequest {
                cell.lblDueDate.setBackGroundColor = 0
            }
            cell.handleDueDateAction = { [weak self] in
                self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentModel.arr_ApprovalList?[indexPath.section])
//                self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentModel.arr_ApprovalList![indexPath.section].duedate ?? "", dueDate: self?.theCurrentModel.arr_ApprovalList![indexPath.section].duedate ?? "", strActionID: self?.theCurrentModel.arr_ApprovalList![indexPath.section].actionlogId ?? "")
            }
            cell.handleWorkDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ApprovalList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentModel.arr_ApprovalList![indexPath.section].workDate ?? "", dueDate: self?.theCurrentModel.arr_ApprovalList![indexPath.section].duedate ?? "", strActionID: self?.theCurrentModel.arr_ApprovalList![indexPath.section].actionlogId ?? "")
                }
            }
            cell.handlerPercentCompletedAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ApprovalList?[indexPath.row].actionlogStatus ?? "") {
                    self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentModel.arr_ApprovalList?[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentModel.arr_ApprovalList?[indexPath.row].actionlogId ?? "")
                }
            }
//            cell.configureApproval(theModel: theCurrentModel.arr_ApprovalList![indexPath.section],isApiLoading:isApiLoading)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getApprovalListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr_model = theCurrentModel.arr_ApprovalList, arr_model.count > 0, !theCurrentModel.apiLoadingAtApprovalID.contains(arr_model[indexPath.section].actionlogId) else { return  }
        getActionDetailWebService(strActionLogID: arr_model[indexPath.section].actionlogId)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch theCurrentModel.selectedTitleType {
        case .sentApproval:
           return false
        case .yourApproval:
            break
        case .dueDateRequest:
//            return true
            break
//        default:
//            return false
//            break
        }
        guard let arr_model = theCurrentModel.arr_ApprovalList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtApprovalID.contains(arr_model[indexPath.section].actionlogId) {
            return false
        }
        return true
    }
    
    
}
//MARK:-UITableViewDelegate
extension ApprovalsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_ApprovalList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_ApprovalList, arr_model.count > 0, !theCurrentModel.apiLoadingAtApprovalID.contains(arr_model[indexPath.section].actionlogId) else { return nil }
        
        let approve = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Approve") { [weak self] (action, index) in
            print("approved")
            if self?.theCurrentModel.selectedTitleType == .dueDateRequest {
                self?.showConformationForDate(strApprovalID: arr_model[indexPath.section].actionlogId, strRevisedDate: arr_model[indexPath.section].requestedRevisedDate)
            } else {
                self?.showConformation(strApprovalID: arr_model[indexPath.section].actionlogId)
            }
        }
        let decline = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Decline") { [weak self] (action, index) in
            print("Decline")
            self?.redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: true, isForDisplayDeclineRequestFromList: false, theModel: arr_model[indexPath.section])
        }
        approve.backgroundColor = appGreen
        decline.backgroundColor = appPink
        return theCurrentModel.selectedTitleType == .dueDateRequest ? [decline,approve] : [approve]
    }

    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_ApprovalList, arr_model.count > 0, !theCurrentModel.apiLoadingAtApprovalID.contains(arr_model[indexPath.section].actionlogId) else { return nil }
        
        let approvals = UIContextualAction(style: UIContextualAction.Style.normal, title: "Approve") { [weak self] (action, view, completionHandler) in
            completionHandler(true)
            print("approve")
            if self?.theCurrentModel.selectedTitleType == .dueDateRequest {
                self?.showConformationForDate(strApprovalID: arr_model[indexPath.section].actionlogId, strRevisedDate: arr_model[indexPath.section].requestedRevisedDate)
            } else {
                self?.showConformation(strApprovalID: arr_model[indexPath.section].actionlogId)
            }
//            self?.showConformation(strApprovalID: arr_model[indexPath.section].actionlogId)
        }
        let decline = UIContextualAction(style: UIContextualAction.Style.normal, title: "Decline") { [weak self] (action, view, completionHandler) in
            completionHandler(true)
            self?.redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: true, isForDisplayDeclineRequestFromList: false, theModel: arr_model[indexPath.section])

        }
        decline.backgroundColor = appPink
        approvals.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [approvals])
        if theCurrentModel.selectedTitleType == .dueDateRequest {
            configure = UISwipeActionsConfiguration(actions: [decline,approvals])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    
    
}
//MARK:- Api Call
extension ApprovalsVC {
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()

        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
//            alert.dismiss(animated: false, completion: nil)
            //              self?.redirectToAddActionVC(theModel: detail)
            self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    func getApprovalListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
            WebService().cancelledCurrentRequestifExist(url: BASEURL + GET_ActionsList)
        }
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:theCurrentModel.nextOffset,APIKey.key_department_id:theCurrentModel.filterData.departmentID,APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,APIKey.key_company_id:theCurrentModel.filterData.companyID] as [String : Any]
        
        switch theCurrentModel.selectedTitleType {
        case .sentApproval:
            parameter[APIKey.key_grid_type] = APIKey.key_sent_for_approval
            break
        case .yourApproval:
            parameter[APIKey.key_grid_type] = APIKey.key_for_Approval
            break
        case .dueDateRequest:
            parameter[APIKey.key_grid_type] = APIKey.key_revise_duedate_requests
            break
        }
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        SettingsWebService.shared.getAllApprovalList(parameter: parameter, success: { [weak self] (list, nextOffset, approvalCount, sentCount, requestCount) in
                self?.theCurrentModel.titleDropDownTypeCount.sent = sentCount
                self?.theCurrentModel.titleDropDownTypeCount.your = approvalCount
                self?.theCurrentModel.titleDropDownTypeCount.dueDate = requestCount
                self?.theCurrentModel.approvalCount = sentCount + approvalCount + requestCount
//                Utility.shared.pushData.approvalCount = ""
                AppDelegate.shared.updateMenuNotificationCount()
                self?.updateDropDownTitle()
                self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
                self?.theCurrentView.refreshControl.endRefreshing()
                self?.theCurrentView.tableView.reloadData()
        }, failed: { [weak self] (error,approvalCount, sentCount, requestCount) in
                self?.theCurrentModel.titleDropDownTypeCount.sent = sentCount
                self?.theCurrentModel.titleDropDownTypeCount.your = approvalCount
                self?.theCurrentModel.titleDropDownTypeCount.dueDate = requestCount

                self?.theCurrentModel.approvalCount = sentCount + approvalCount + requestCount
                self?.updateDropDownTitle()

//                Utility.shared.pushData.approvalCount = ""
                AppDelegate.shared.updateMenuNotificationCount()
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func approveActionsWebService(strApprovalID:String) {
//        let index = theCurrentModel.apiLoadingAtApprovalID
        
//        let statusID = theCurrentModel.arr_ApprovalList![index].actionlogId
        guard let jsonString = JSON([[APIKey.key_status_id:strApprovalID]]).rawString() else {
            updateBottomCell(loadingID: strApprovalID)
            return
        }
//        status_for_id
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_status:APIKey.key_archived.localizedLowercase,APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,APIKey.key_status_for_id:jsonString] as [String : Any]
        
        SettingsWebService.shared.approveActionsApproval(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strApprovalID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strApprovalID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
    }
    
    func sendRevisedDueDateRequestWebservice(strActionID:String,revisedDate:String,isForApprove:Bool) {
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionID,
                         APIKey.key_revised_date:revisedDate,
                         APIKey.key_respond_val:"1"]

        ActionsWebService.shared.uploadEditActionAttachment(parameter: parameter, files: [], withName: [], withFileName: [], mimeType: [], success: { (actionList) in
            self.updateBottomCell(loadingID: strActionID, isRemove: true)

        }, failed: { [weak self] (error) in
            self?.showAlertAtBottom(message: error)
            self?.updateBottomCell(loadingID: strActionID)
        })
        
    }
}

//MARK:-  NavigationActionVC
class NavigationApprovalVC: UINavigationController {
    
}
