//
//  FeatureUpdateVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeatureUpdateVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FeatureUpdateView = { [unowned self] in
        return self.view as! FeatureUpdateView
    }()
    fileprivate lazy var theCurrentModel:FeatureUpdateModel = {
        return FeatureUpdateModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getFeatureListWebService()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_FeatureList == nil || theCurrentModel.arr_FeatureList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
}

//MARK:- Redirect
extension FeatureUpdateVC {
    func redirectToFeatureUpdateDetailScreen(theModel:FeaturesUpdate) {
        let vc = FeatureUpdateDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

//MARK:-UITableViewDataSource
extension FeatureUpdateVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_FeatureList == nil || theCurrentModel.arr_FeatureList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_FeatureList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureUpdateCell") as! FeatureUpdateCell
        
        if theCurrentModel.arr_FeatureList != nil {
            var date = ""
            if let createddDate = theCurrentModel.arr_FeatureList![indexPath.row].created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType6),!createddDate.isEmpty {
                date = createddDate
            }
            cell.configure(strTitle: theCurrentModel.arr_FeatureList![indexPath.row].title, strSubTitle: date)
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getFeatureListWebService()
            }
        }
        return cell
    }

}

//MARK:-UITableViewDelegate
extension FeatureUpdateVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_FeatureList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_FeatureList != nil {
            redirectToFeatureUpdateDetailScreen(theModel: theCurrentModel.arr_FeatureList![indexPath.row])
        }
    }
}


//MARK:- Api Call
extension FeatureUpdateVC {
    func getFeatureListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:theCurrentModel.nextOffset] as [String : Any]
        SettingsWebService.shared.getAllFeatureList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            if self?.theCurrentModel.nextOffset == 0 {
                Utility.shared.pushData.whatsnewUnread = ""
                AppDelegate.shared.updateMenuNotificationCount()                
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            
        }, failed: { [weak self] (error) in
            self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
            self?.setBackground(strMsg: error)
            self?.theCurrentView.refreshControl.endRefreshing()
        })

    }
}
