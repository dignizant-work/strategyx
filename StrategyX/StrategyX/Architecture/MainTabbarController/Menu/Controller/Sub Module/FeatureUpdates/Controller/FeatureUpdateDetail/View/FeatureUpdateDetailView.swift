//
//  FeatureUpdateDetailView.swift
//  StrategyX
//
//  Created by Jaydeep on 08/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeatureUpdateDetailView: ViewParentWithoutXIB {
    
    //MARK:- Outelt Zone
    
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var lblUpdateTitleName: UILabel!
    @IBOutlet weak var lblUpdateSubTitleName: UILabel!
    @IBOutlet weak var lblUpdateDate: UILabel!
    @IBOutlet weak var lblUpdateDescription: UILabel!
    
    @IBOutlet weak var btnPlay: UIButton!
    
   

}
