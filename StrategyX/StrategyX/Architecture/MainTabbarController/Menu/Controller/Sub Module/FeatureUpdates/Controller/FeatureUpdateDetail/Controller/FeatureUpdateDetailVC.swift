//
//  FeatureUpdateDetailVC.swift
//  StrategyX
//
//  Created by Jaydeep on 08/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage

class FeatureUpdateDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FeatureUpdateDetailView = { [unowned self] in
        return self.view as! FeatureUpdateDetailView
        }()
    
    fileprivate lazy var theCurrentModel:FeatureUpdateDetailModel = {
        return FeatureUpdateDetailModel(theController: self)
    }()
    
    private let vimeoServiceManager = VimeoServiceManger()

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        updateViewModel()
    }

    func setTheData(theModel:FeaturesUpdate) {
        theCurrentModel.theModleFeatureUpdate = theModel
    }
    
    func updateViewModel() {
        guard theCurrentModel.theModleFeatureUpdate != nil else { return }
        if !theCurrentModel.theModleFeatureUpdate.videoId.isEmpty {
            if VideoType.vimeo.rawValue.lowercased() == theCurrentModel.theModleFeatureUpdate.videoType {
                if  let imageVimeo = SDImageCache.shared.imageFromCache(forKey: theCurrentModel.theModleFeatureUpdate.featureImage) {
                    theCurrentView.imgHeader.image = imageVimeo
                    return
                }
                vimeoServiceManager.retrieveVimeoImage(from: VIMEOCONFIGBASEURL + theCurrentModel.theModleFeatureUpdate.videoId +  "/config") { [weak self] (result) in
                    guard let strongSelf = self else {
                        return
                    }
                    switch result {
                    case .failure(_):
                        strongSelf.theCurrentView.imgHeader.image = UIImage(named: "ic_bg_plash_holder_list")
                        break
                    case .success(let (networkUrl, imageUrl)):
                        if VIMEOCONFIGBASEURL + strongSelf.theCurrentModel.theModleFeatureUpdate.videoId +  "/config"
                            == networkUrl {
                            //                        strongSelf.avatarImageView.image = image
                            SDWebImageManager.shared.loadImage(with: URL.init(string: imageUrl), options: SDWebImageOptions.queryMemoryData, progress: nil) { (image, data, error, cacheType, finished, url) in
                                strongSelf.theCurrentView.imgHeader.image = image
                                SDImageCache.shared.store(image, forKey: strongSelf.theCurrentModel.theModleFeatureUpdate.featureImage, completion: nil)
                            }
                        }
                    }
                    
                }
            } else {
                theCurrentView.imgHeader.sd_setImageLoadMultiTypeURL(url: "\(YTDBASEURL)\(theCurrentModel.theModleFeatureUpdate.videoId)/0.jpg", placeholder: "ic_bg_plash_holder_list")

            }
        } else {
            theCurrentView.btnPlay.isHidden = true
            theCurrentView.imgHeader.sd_setImageLoadMultiTypeURL(url: theCurrentModel.theModleFeatureUpdate.featureImage, placeholder: "ic_bg_plash_holder_list")
        }
//        theCurrentView.imgHeader.sd_setImageLoadMultiTypeURL(url: theCurrentModel.theModleFeatureUpdate.featureImage, placeholder: "ic_bg_plash_holder_list")

        theCurrentView.lblUpdateTitleName.text = theCurrentModel.theModleFeatureUpdate.title
//        theCurrentView.lblUpdateSubTitleName.text = theCurrentModel.theModleFeatureUpdate.createdBy
        
        var date = ""
        if let createddDate = theCurrentModel.theModleFeatureUpdate.created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2),!createddDate.isEmpty {
            date = createddDate
        }
        theCurrentView.lblUpdateDate.text = date
        theCurrentView.lblUpdateDescription.attributedText = theCurrentModel.theModleFeatureUpdate.description_feature.html2AttributedString
        theCurrentView.lblUpdateDescription.font = R16

    }
    
    
}

//MARK:- Action Zone

extension FeatureUpdateDetailVC
{
    @IBAction func btnBackAction(_ sender:UIButton)
    {
        self.backAction(isAnimation: false)
    }
    @IBAction func btnPlayAction(_ sender:UIButton)
    {
        if !theCurrentModel.theModleFeatureUpdate.videoId.isEmpty {
            let theModel = TrainingVideoList()
            theModel.id = theCurrentModel.theModleFeatureUpdate.featureupdateId
            theModel.title = theCurrentModel.theModleFeatureUpdate.title
            theModel.videoId = theCurrentModel.theModleFeatureUpdate.videoId
            theModel.videoUrl = theCurrentModel.theModleFeatureUpdate.featureImage
            theModel.videoType = theCurrentModel.theModleFeatureUpdate.videoType
            self.extRedirectToTrainingVideoVC(theModel: theModel)
//            let vc = VimeoPlayerController.init(nibName: "VimeoPlayerController", bundle: nil)
//            self.present(vc, animated: true, completion: nil)
            
        }
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
