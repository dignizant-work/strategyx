//
//  FeatureUpdateCell.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeatureUpdateCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    

    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblTitleName,lblSubTitle].forEach({ $0.showAnimatedSkeleton() })
        lblTitleName.text = " "
        lblSubTitle.text = " "
    }
    
    func hideAnimation() {
        [lblTitleName,lblSubTitle].forEach({ $0?.hideSkeleton() })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,strSubTitle:String) {
        hideAnimation()
        lblTitleName.text = strTitle
        lblSubTitle.text = strSubTitle
    }
    
}
