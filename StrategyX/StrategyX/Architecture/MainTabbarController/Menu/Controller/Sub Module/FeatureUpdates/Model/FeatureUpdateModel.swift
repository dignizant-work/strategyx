//
//  FeatureUpdateModel.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeatureUpdateModel {
    
    //MARK:- Variable
    fileprivate weak var theController:FeatureUpdateVC!
    var arr_FeatureList:[FeaturesUpdate]?
    var nextOffset = 0

    
    //MARK:- LifeCycle
    init(theController:FeatureUpdateVC) {
        self.theController = theController
    }
    
    func updateList(list:[FeaturesUpdate], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_FeatureList?.removeAll()
        }
        nextOffset = offset
        if arr_FeatureList == nil {
            arr_FeatureList = []
        }
        list.forEach({ arr_FeatureList?.append($0) })
        print(arr_FeatureList!)
    }
}
