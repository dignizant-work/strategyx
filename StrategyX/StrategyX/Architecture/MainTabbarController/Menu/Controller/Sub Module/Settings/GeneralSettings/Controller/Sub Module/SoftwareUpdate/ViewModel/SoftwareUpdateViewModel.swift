//
//  SoftwareUpdateViewModel.swift
//  StrategyX
//
//  Created by Haresh on 12/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class SoftwareUpdateViewModel:NSObject  {

    //MARK:-Variable
    fileprivate weak var theController:SoftwareUpdateVC!
    
    
    //MARK:- LifeCycle
    init(theController:SoftwareUpdateVC) {
        self.theController = theController
    }

}

//MARK:- API Call
extension SoftwareUpdateViewModel {
    func checkVerionWebService(parameters:[String:Any], success:@escaping(Int,String) -> Void, failed:@escaping(String) -> Void )  {
        CommanListWebservice.shared.checkVersion(parameter: parameters, success: { [weak self] (versionFlag,latestVerion,lastupdatedDate) in
            var date = ""
            if let lastDate = lastupdatedDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType11) {
                date = lastDate
                Utility.shared.pushData.lastVersionUpdateDate = date
            }
            (self?.theController.view as? SoftwareUpdateView)?.updateTitle(version: Utility.shared.appCurrentVersion , time: date)
                success(versionFlag,latestVerion)
            }, failed: { [weak self] (error) in
                failed(error)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
