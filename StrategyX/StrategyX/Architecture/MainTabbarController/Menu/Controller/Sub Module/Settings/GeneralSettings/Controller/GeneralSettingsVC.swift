//
//  GeneralSettingsVC.swift
//  StrategyX
//
//  Created by Haresh on 20/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import SlideMenuControllerSwift

class GeneralSettingsVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView: GeneralSettingsView = { [unowned self] in
        return self.view as! GeneralSettingsView
    }()
    
    lazy var theCurrentViewModel: GeneralSettingsViewModel = {
        return GeneralSettingsViewModel(theController: self)
    }()

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        
        if let _ = UserDefault.shared.getTouchUserData() {
            theCurrentViewModel.isActiveToggle = true
        }
        updateSoftwareUpdateCount()
    }
    func updateSoftwareUpdateCount() {
        theCurrentView.updateSoftwareBadgeCount(count: Int(Utility.shared.pushData.softwareUpdateCount) ?? 0)
    }

    //MARK:- Redirect To
    func redirectToSettingsScreen(type:SettingsVC.titleSelectionType) {
        let vc = SettingsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setSelectionType(type: type)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func redirectToTouchLoginScreen() {
        let vc = TouchLoginVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func redirectToSoftwareUpdateScreen() {
        let vc = SoftwareUpdateVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func redirectToAddDepartmentVC() {
        let img = self.view.asImage()
        let vc = AddDepartmentVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG:img, strCompanyID: Utility.shared.userData.companyId, strCompanyName:Utility.shared.userData.companyName)
        vc.handlerDepartmentCount = { (count) in
//            self?.theCurrentModel.theCompanyModel?.department = count
//            self?.updateViewCompanyDetail(theModel: self?.theCurrentModel.theCompanyModel)
        }
        self.push(vc: vc)
    }

    //MARK:- IBAction
    @IBAction func onBtnSoftwareUpdate(_ sender: Any) {
        redirectToSoftwareUpdateScreen()
    }
    
    @IBAction func onBtnCompaniesAction(_ sender: Any) {
        redirectToSettingsScreen(type: SettingsVC.titleSelectionType.companies)
    }
    
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        redirectToAddDepartmentVC()
    }
    
    
    @IBAction func onBtnUserAction(_ sender: Any) {
        redirectToSettingsScreen(type: SettingsVC.titleSelectionType.users)
    }
    
    @IBAction func onBtnTags(_ sender: Any) {
        redirectToSettingsScreen(type: SettingsVC.titleSelectionType.tags)
    }
    
    @IBAction func onBtnToggleBiometric(_ sender: UIButton) {
        let isToggle = !sender.isSelected
        BiometricIDAuth.shared.authenicateUser { [weak self] (strErrorMsg,isRedirectToSetting)  in
            if isRedirectToSetting {
                if let msg = strErrorMsg {
                    self?.showAlertAction(msg: msg, completion: {
                        guard let urlSetting = URL.init(string: UIApplication.openSettingsURLString) else { return }
                        UIApplication.shared.open(urlSetting, options: [:], completionHandler: nil)
                    })
                }
                return
            }
            if let msg = strErrorMsg,!msg.trimmed().isEmpty {
                if !msg.isEmpty {
                    self?.showAlert(msg: msg)
                } else {
                    
                }
            } else if strErrorMsg == nil && isRedirectToSetting == false {
                // successfully athenicate
                if sender.isSelected {
                    UserDefault.shared.removeTouchUserData()
                } else {
                    if let lastUserLoginData = UserDefault.shared.getLastUserLoginData() {
                        let touch = JSON(["email":(lastUserLoginData.email).trimmed(),"password":(lastUserLoginData.password).trimmed()])
                        UserDefault.shared.saveTouchUserData(user: touch)
                    } else {
                        let touch = JSON(["email":(Utility.shared.userData.email).trimmed(),"password":(Utility.shared.userData.password).trimmed()])
                        UserDefault.shared.saveTouchUserData(user: touch)
                    }
                }
                self?.theCurrentViewModel.isActiveToggle = isToggle
            }
        }
    }
    
}
