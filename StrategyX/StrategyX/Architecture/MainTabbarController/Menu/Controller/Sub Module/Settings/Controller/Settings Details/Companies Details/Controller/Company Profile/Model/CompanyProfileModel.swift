//
//  CompanyProfileModel.swift
//  StrategyX
//
//  Created by Haresh on 12/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyProfileModel {

    //MARK:- Variable
    fileprivate var theController:CompanyProfileVC!
    var tableviewCount = 19
    var imageBG:UIImage?
    var theModelCompanyDetail:CompanyDetail?
    var theCompanyModel:Company? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                if let index = controller.firstIndex(where: {( $0 as? CompaniesDetailsVC) != nil }) {
                    (controller[index] as? CompaniesDetailsVC)?.updateViewCompanyDetail(theModel: theCompanyModel)
                }
            }
        }
    }
    
    //MARK:- LifeCycle
    init(theController:CompanyProfileVC) {
        self.theController = theController
    }
    
}
