//
//  CompaniesDetailsVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompaniesDetailsVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CompaniesDetailsView = { [unowned self] in
        return self.view as! CompaniesDetailsView
    }()
    fileprivate lazy var theCurrentModel:CompaniesDetailsModel = {
        return CompaniesDetailsModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.tblDepartment.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tblDepartment.removeObserver(self, forKeyPath: "contentSize")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            if tabl.contentSize.height + 297.0 < theCurrentView.viewAddDepartment.frame.size.height {
                self.theCurrentView.constrainTblDepartmentHeight.constant = tabl.contentSize.height
                self.view.layoutIfNeeded()
            }
            //            handlerUpdateTableView()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        updateViewCompanyDetail(theModel: theCurrentModel.theCompanyModel)
        theCurrentModel.userListService(isRefreshing: true)
    }
    
    func setTheData(theModel:Company?, isForArchive:Bool = false) {
        theCurrentModel.theCompanyModel = theModel
        theCurrentModel.isForArchive = isForArchive
    }
    func setBackground(strMsg:String) {
        theCurrentView.refreshControl.endRefreshing()
        if theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
        }
        theCurrentView.tableView.reloadData()
    }
    func updateViewCompanyDetail(theModel:Company?) {
        theCurrentModel.theCompanyModel = theModel
        theCurrentView.update(strCompanyName: theModel?.name ?? "", strDepartment: "\((theModel?.department ?? 0))", strStuff: "\((theModel?.user ?? 0))")
    }
    func refreshApiCall() {
        theCurrentModel.arr_userList?.removeAll()
        theCurrentModel.arr_userList = nil
        theCurrentModel.nextOffset = 0
        theCurrentView.tableView.backgroundView = nil
        theCurrentView.tableView.reloadData()
        theCurrentModel.userListService(isRefreshing: true)
    }
    func updateTableViewData() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    
    func updateDepartmentList(strDepartment:String, index:Int, isDelete:Bool = false) {
        
        if isDelete {
            theCurrentModel.arr_DepartmentList.remove(at: index)
        } else {
            if strDepartment.isEmpty {
                showAlertAtBottom(message: "Please add department")
                return
            }
            
            theCurrentModel.arr_DepartmentList.insert(strDepartment, at: 0)
        }
        
        self.theCurrentView.tblDepartment.reloadData()
    }
    
    func updateUserListModel(theUserListModel:UserList) {
        if let index = theCurrentModel.arr_userList?.firstIndex(where: {$0.userId == theUserListModel.userId}) {
            theCurrentModel.arr_userList?.remove(at: index)
            theCurrentModel.arr_userList?.insert(theUserListModel, at: index)
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    //MARK:- Redirect
    func redirectToCompanyStuffDetailVC(theModel:UserList) {
        let vc = CompanyStuffDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToCompanyProfileDetailVC() {
        let img = self.view.asImage()
        let vc = CompanyProfileVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(imgBG:img, theCompanyModel: theCurrentModel.theCompanyModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToAddDepartmentVC() {
        let img = self.view.asImage()
        let vc = AddDepartmentVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG:img, strCompanyID: theCurrentModel.theCompanyModel?.id ?? "", strCompanyName:theCurrentModel.theCompanyModel?.name ?? "")
        vc.handlerDepartmentCount = { [weak self] (count) in
            self?.theCurrentModel.theCompanyModel?.department = count
            self?.updateViewCompanyDetail(theModel: self?.theCurrentModel.theCompanyModel)
        }
        self.push(vc: vc)
    }
    //MARK:- IBAction
    @IBAction func onBtnCompanyProfileAction(_ sender: Any) {
        guard theCurrentModel.theCompanyModel != nil else { return }
        redirectToCompanyProfileDetailVC()
    }
    @IBAction func onBtnAddDepartmentPlushAction(_ sender: Any) {
        let text = theCurrentView.txtAddDepartment.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        theCurrentView.txtAddDepartment.text = nil
        updateDepartmentList(strDepartment: text, index: 0)
    }
    @IBAction func onBtnAddDepartmentSaveAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if theCurrentModel.arr_DepartmentList.count == 0 {
            self.showAlertAtBottom(message: "Please add deparment")
            return
        }
        sender.startLoadyIndicator()
        theCurrentView.viewAddDepartment.allButtons(isEnable: false)
        theCurrentModel.addDepartment(success: { [weak self] in
            self?.theCurrentView.viewAddDepartment.allButtons(isEnable: true)
            self?.theCurrentView.viewDepartment(isShow: false)
            sender.stopLoadyIndicator()
        }, failed: { [weak self] in
            self?.theCurrentView.viewAddDepartment.allButtons(isEnable: true)
            sender.stopLoadyIndicator()
        })
    }
    @IBAction func onBtnAddDepartmentCloseAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentView.viewDepartment(isShow: false)
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        if theCurrentModel.isForArchive {
            return
        }
        redirectToAddDepartmentVC()
//        theCurrentView.viewDepartment(isShow: true)
        
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        
    }
    
}
//MARK:-UITableViewDataSource
extension CompaniesDetailsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == theCurrentView.tblDepartment {
            return theCurrentModel.arr_DepartmentList.count
        }
        if let _ = tableView.backgroundView , (theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_userList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == theCurrentView.tblDepartment {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            cell.configure(strTitle: theCurrentModel.arr_DepartmentList[indexPath.row])
            cell.handlerOnDeleteAction = { [weak self] (tag) in
                self?.updateDepartmentList(strDepartment: "", index: indexPath.row, isDelete: true)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsUsersTVCell") as! SettingsUsersTVCell
            //        cell.configure(strTitle: "Gabriel Moriera")
            if theCurrentModel.arr_userList != nil {
                cell.configure(theModel: theCurrentModel.arr_userList![indexPath.row])
                let lastSectionIndex = tableView.numberOfSections - 1
                let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
                if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                    theCurrentModel.userListService(isRefreshing: false)
                }
            } else {
                cell.startAnimation()
            }
            return cell
        }
        
    }
    
}
//MARK:-UITableViewDelegate
extension CompaniesDetailsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_userList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = theCurrentModel.arr_userList else { return }
        redirectToCompanyStuffDetailVC(theModel: model[indexPath.row])
    }
}
