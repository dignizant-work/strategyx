//
//  CompaniesDetailsView.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompaniesDetailsView: ViewParentWithoutXIB {

    //MARK:- Variabel
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var constrainBtnBackWidth: NSLayoutConstraint!// 35.0
    
    @IBOutlet weak var lblcompanyTitle: UILabel!
    @IBOutlet weak var btnGraph: UIButton!
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblStuff: UILabel!
    
    @IBOutlet weak var tblDepartment: UITableView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewAddDepartment: UIView!
    @IBOutlet weak var lblAddCompanyTitle: UILabel!
    @IBOutlet weak var lblAddDepartmentTitle: UILabel!
    @IBOutlet weak var txtAddDepartment: UITextField!
    
    @IBOutlet weak var constrainTblDepartmentHeight: NSLayoutConstraint! // 0
    
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        viewDepartment(isShow: false)
        backButton(isHidden: false)
    }
    
    func setupTableView(theDelegate:CompaniesDetailsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(SettingsUsersTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
        tblDepartment.estimatedRowHeight = 50.0
        tblDepartment.rowHeight = UITableView.automaticDimension
        tblDepartment.delegate = theDelegate
        tblDepartment.dataSource = theDelegate
        tblDepartment.separatorStyle = .none
    }
    
    func update(strCompanyName:String,strDepartment:String,strStuff:String) {
        lblcompanyTitle.text = strCompanyName
        lblDepartment.text = strDepartment
        lblStuff.text = strStuff
        lblAddDepartmentTitle.text = strCompanyName
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? CompaniesDetailsVC)?.refreshApiCall()
    }
    
    func backButton(isHidden:Bool) {
        constrainBtnBackWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    //MARK:- Add Department View
    func viewDepartment(isShow:Bool) {
        if isShow {
            self.viewAddDepartment.alpha = 0.0
             self.viewAddDepartment.isHidden = false
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.viewAddDepartment.alpha = isShow ? 1.0 : 0.0
        }, completion: { (finished) in
            self.viewAddDepartment.isHidden = !isShow
        })
    }
    
}
