//
//  TagsDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TagsDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:TagsDetailView = { [unowned self] in
       return self.view as! TagsDetailView
    }()
    
    fileprivate lazy var theCurrentModel:TagsDetailModel = {
       return TagsDetailModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        updateViewDetail()
    }
    
    func setTheData(theModel:TagList?) {
        theCurrentModel.theTagDetailModel = theModel
    }
    func updateViewDetail() {
        guard let theModel = theCurrentModel.theTagDetailModel else { return }
        theCurrentView.lblTagName.text = theModel.tag_name
        if theModel.tagStatus.lowercased() == "active".lowercased() {
            theCurrentView.btnActive.isSelected = true
            theCurrentModel.lastStateOfSelectedTag = true
        } else {
            theCurrentView.btnActive.isSelected = false
            theCurrentModel.lastStateOfSelectedTag = false
        }
    }

    //MARK:- IBAction
    @IBAction func onBtnBackAction(_ sender: UIButton) {
        if theCurrentView.btnBack.isSelected {
            theCurrentView.btnActive.isSelected = theCurrentModel.lastStateOfSelectedTag
            theCurrentView.updateBtnStatus()
        } else {
            backAction(isAnimation: false)
        }
    }
    
    @IBAction func onBtnEditAction(_ sender: UIButton) {
        if theCurrentView.btnEdit.isSelected {
            let lastState = theCurrentView.btnActive.isSelected
             self.theCurrentView.allButtons(isEnable: false)
             editTagStatus(success: { [weak self] in
                self?.theCurrentView.allButtons(isEnable: true)
                self?.theCurrentModel.lastStateOfSelectedTag = lastState
//                self?.theCurrentView.updateBtnActiveToggle()
                self?.theCurrentView.updateBtnStatus()
             }, failed: { [weak self] in
                self?.theCurrentView.allButtons(isEnable: true)
             })
        } else {
            theCurrentView.updateBtnStatus()
        }
    }
    
    @IBAction func onBtnActiveAction(_ sender: UIButton) {
        theCurrentView.updateBtnActiveToggle()
        /*
        self.theCurrentView.allButtons(isEnable: false)
        editTagStatus(success: { [weak self] in
            self?.theCurrentView.allButtons(isEnable: true)
            self?.theCurrentView.updateBtnActiveToggle()
        }, failed: { [weak self] in
            self?.theCurrentView.allButtons(isEnable: true)
        })*/
    }
    
}
//MARK:- Api Call
extension TagsDetailVC {
    
    func editTagStatus(success:@escaping() -> Void,failed:@escaping() -> Void) {
        let status = theCurrentView.btnActive.isSelected ? APIKey.key_myRoleStatus_Active : APIKey.key_InActive
        let model = theCurrentModel.theTagDetailModel!
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_tag_name:theCurrentModel.theTagDetailModel?.tag_name ?? "",APIKey.key_tag_id:theCurrentModel.theTagDetailModel?.tag_id ?? "",APIKey.key_tag_status:status] as [String : Any]
        
        
        CompanyWebService.shared.editTag(parameter: parameter, success: { [weak self] (msg) in
                self?.showAlertAtBottom(message: msg)
                model.tagStatus = status
                self?.theCurrentModel.theTagDetailModel = model
                success()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                failed()
        })
    }
    
}
