//
//  TouchLoginView.swift
//  StrategyX
//
//  Created by Haresh on 20/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TouchLoginView: ViewParentWithoutXIB {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnToggel: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        switch BiometricIDAuth.shared.biometricType() {
        case .faceID:
            lblTitle.text = "Face ID Login"
            lblDescription.text = "Enable Face ID or Password based \n access to prevent enter credential \n to your StrategyX App"
            break
        case  .touchID:
            lblTitle.text = "Touch ID Login"
            lblDescription.text = "Enable Touch ID or Password based \n access to prevent enter credential \n to your StrategyX App"
            break
        default:
            break
            
        }
        
    }
    
    func updateBtnActiveToggle() {
        btnToggel.isSelected = !btnToggel.isSelected
    }
    
}
