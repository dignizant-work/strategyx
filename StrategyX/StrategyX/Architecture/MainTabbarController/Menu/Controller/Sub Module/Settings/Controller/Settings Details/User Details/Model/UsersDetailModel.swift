//
//  UsersDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class UsersDetailModel {

    //MARK:- Variable
    fileprivate weak var theController:UsersDetailVC!
//    var theUserDetailModel:UserList?
    
    var theUserDetailModel:UserList? = nil{
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
                    (vc as? SettingsVC)?.updateUserListModel(theUserListModel: theUserDetailModel!)
                }
            }
        }
    }

    //MARK:- LifeCycle
    init(theController:UsersDetailVC) {
        self.theController = theController
    }
    
}
