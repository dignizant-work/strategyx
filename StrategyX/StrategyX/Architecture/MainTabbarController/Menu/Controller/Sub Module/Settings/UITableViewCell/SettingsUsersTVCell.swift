//
//  SettingsUsersTVCell.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SettingsUsersTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configure(theModel:UserList) {
        hideAnimation()
        lblTitle.text = theModel.username
        img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.userImage, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with:  URL(string: theModel.userImage), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))
    }
    
    func hideAnimation() {
        [lblTitle,img_Profile].forEach({ $0?.hideSkeleton() })
    }
    
    func startAnimation() {
        [lblTitle,img_Profile].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
    }
}
