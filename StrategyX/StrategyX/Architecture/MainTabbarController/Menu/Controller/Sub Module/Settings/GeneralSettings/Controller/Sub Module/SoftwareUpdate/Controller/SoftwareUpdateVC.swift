//
//  SoftwareUpdateVC.swift
//  StrategyX
//
//  Created by Haresh on 12/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SoftwareUpdateVC: ParentViewController {

    //MARK:- Variable
    lazy var theCurrentView: SoftwareUpdateView = { [unowned self] in
        return self.view as! SoftwareUpdateView
    }()
    
    internal lazy var theCurrentViewModel: SoftwareUpdateViewModel = {
        return SoftwareUpdateViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        setSideMenuLeftNavigationBarItem()
    }
    
   
    func setupUI() {
//        setLogoOnNavigationHeader()
        setBackButtonWithLeftTitle(strTitle: "Software Update")
        theCurrentView.setupLayout()
        
        theCurrentView.updateTitle(version: Utility.shared.appCurrentVersion, time: Utility.shared.pushData.lastVersionUpdateDate)
    }

    //MARK:- IBAction
    @IBAction func onBtnCheckVersion(_ sender: Any) {
        theCurrentView.btnProgress.startLoadyIndicator(topLineColor: appGreen)
        theCurrentView.checkForUpdateIsHidden(true)
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id, APIKey.key_access_token:Utility.shared.userData.accessToken, APIKey.key_ios_current_version:Utility.shared.appCurrentVersion]
        theCurrentViewModel.checkVerionWebService(parameters: parameter, success: { [weak self] (versionFlag,latestVerion) in
            self?.theCurrentView.btnProgress.stopLoadyIndicator()
            self?.theCurrentView.checkForUpdateIsHidden(false)
//            Utility.shared.pushData.lastVersionUpdateDate = latestVerion
//            self?.theCurrentView.updateTitle(version: Utility.shared.appCurrentVersion, time: Utility.shared.pushData.lastVersionUpdateDate)

            if versionFlag == 1 {
                self?.showAlertAction(msg: "New version (\(latestVerion)) of this app is available on store. Would you like to update?", buttonNo: "CANCEL", buttonYes: "OK", completion: {
                    
//                    itms://itunes.apple.com/us/app/strategyx/id1438372835?ls=1
                    guard let url = URL.init(string: "https://apps.apple.com/us/app/strategyx/id1438372835?ls=1") else { return }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                })
            } else {
                
            }
        }, failed: { [weak self] (error) in
            self?.theCurrentView.btnProgress.stopLoadyIndicator()
            self?.theCurrentView.checkForUpdateIsHidden(false)
        })
        
    }
}
