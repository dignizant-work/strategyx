//
//  SettingsModel.swift
//  StrategyX
//
//  Created by Haresh on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class SettingsModel {

    //MARK:- Variable
    fileprivate weak var theController:SettingsVC!
    
    var titleTypeDD = DropDown()
    var arr_TitleType = [SettingsVC.titleSelectionType.companies.rawValue,SettingsVC.titleSelectionType.users.rawValue,SettingsVC.titleSelectionType.tags.rawValue]
    var isSearching = false
    var arr_Search:[Any] = []
    
    var selectedTitleType = SettingsVC.titleSelectionType.companies
    
    var arr_companyList:[Company]?

    var arr_userList:[UserList]?
    var nextOffset = 0
    
    var arr_TagList:[TagList]?
    
    //MARK:- LifeCycle
    init(theController:SettingsVC) {
        self.theController = theController
        if UserDefault.shared.userRole == .companyAdminUser {
            selectedTitleType = .users
            arr_TitleType.remove(at: 0)
        } else if UserDefault.shared.userRole == .staffUser {
            arr_TitleType.remove(at: 0)
            arr_TitleType.remove(at: 0)
            selectedTitleType = .tags
        }
    }
    
    
    func configure(dropDown:DropDown,view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 35)
        
        switch dropDown {
        case titleTypeDD:
            dropDown.dataSource = arr_TitleType
            break
        default:
            break
        }
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
        dropDown.cancelAction = { [unowned self] in
            self.theController.rotateDropdownArrow()
        }
    }
    
    func updateList(list:[Any], offset:Int, isRefreshing:Bool) {
        switch selectedTitleType {
        case .companies:
            if isRefreshing {
                arr_companyList?.removeAll()
            }
            if arr_companyList == nil {
                arr_companyList = []
            }
            list.forEach({ arr_companyList?.append($0 as! Company) })
            print(arr_companyList!)
            break
        case .users:
            nextOffset = offset
            if isRefreshing {
                arr_userList?.removeAll()
            }
            if arr_userList == nil {
                arr_userList = []
            }
            
            list.forEach({ arr_userList?.append($0 as! UserList) })
            print(arr_userList!)
            break
        case .tags:
            if isRefreshing {
                arr_TagList?.removeAll()
            }
            if arr_TagList == nil {
                arr_TagList = []
            }
            list.forEach({ arr_TagList?.append($0 as! TagList) })
            print(arr_TagList!)
            break
        }
    }
}
//MARk:- Api Call
extension SettingsModel {
    func companylistService(isRefreshing:Bool) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            if self?.selectedTitleType == .companies {
                self?.updateList(list: list, offset: 0, isRefreshing: isRefreshing)
            }
            self?.theController.updateTableViewData()
        }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    func userListService(isRefreshing:Bool, textSearch:String) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset,APIKey.key_search:textSearch,APIKey.key_company_id:Utility.shared.userData.companyId] as [String : Any]
        CommanListWebservice.shared.getAllUserList(parameter: parameter, success: { [weak self] (list, nextOffset)  in
            if self?.selectedTitleType == .users {
                self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            }
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    func tagListService(isRefreshing:Bool) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_is_add_form_tag:"0"]
        ActionsWebService.shared.getTagList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            if self?.selectedTitleType == .tags {
                self?.updateList(list: list, offset: 0, isRefreshing: isRefreshing)
            }
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    
}
