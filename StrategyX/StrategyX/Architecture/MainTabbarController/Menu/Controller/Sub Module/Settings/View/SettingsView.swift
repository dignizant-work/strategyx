//
//  SettingsView.swift
//  StrategyX
//
//  Created by Haresh on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsView: ViewParentWithoutXIB {

    //MARK:- Variable
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    @IBOutlet weak var img_DropDownArrow: UIImageView!
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var viewAddCompany: UIView!
    
    @IBOutlet weak var viewArchived: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewAddUser: UIView!
    @IBOutlet weak var viewAddTag: UIView!
    
//    let refreshControl = UIRefreshControl()
    let disposeBag = DisposeBag()
    
    
    //MARK:- Life Cycle
    func setupLayout() {        
        onTextFieldSearchAction()
    }
    
    func setupTableView(theDelegate:SettingsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(SettingsCompaniesTVCell.self)
        tableView.registerCellNib(SettingsUsersTVCell.self)
        tableView.registerCellNib(SettingsTagsTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? SettingsVC)?.refreshApiCall()
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
        self.layoutIfNeeded()
    }
    
    //RX Action
    func onTextFieldSearchAction() {
        txtSearch.rx.text.orEmpty
            .subscribe(onNext:{ (text) in
                (self.parentContainerViewController() as? SettingsVC)?.searchText(txt: text)
            }).disposed(by: disposeBag)
        
        txtSearch.rx.controlEvent(UIControl.Event.editingDidEnd)
            .subscribe(onNext: { [weak self] _ in
                let text = self?.txtSearch.text ?? ""
                (self?.parentContainerViewController() as? SettingsVC)?.searchText(txt: text, endEditing:true)
            }).disposed(by: disposeBag)
        
    }
    
    
    
}
