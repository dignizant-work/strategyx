//
//  SettingsArchivedModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SettingsArchivedModel {

    //MARK:- Variable
    fileprivate weak var theController:SettingsArchivedVC!
    
    var selectedTitleType = SettingsVC.titleSelectionType.companies
    
    var isSearching = false
    var arr_Search:[Any] = []

    var arr_companyList:[Company]?
    
    var arr_userList:[UserList]?
    var nextOffset = 0
    
    var arr_TagList:[TagList]?
    
    //MARK:- LifeCycle
    init(theController:SettingsArchivedVC) {
        self.theController = theController
    }

    func updateList(list:[Any], offset:Int, isRefreshing:Bool) {
        switch selectedTitleType {
        case .companies:
            if isRefreshing {
                arr_companyList?.removeAll()
            }
            if arr_companyList == nil {
                arr_companyList = []
            }
            list.forEach({ arr_companyList?.append($0 as! Company) })
            print(arr_companyList!)
            break
        case .users:
            nextOffset = offset
            if isRefreshing {
                arr_userList?.removeAll()
            }
            if arr_userList == nil {
                arr_userList = []
            }
            list.forEach({ arr_userList?.append($0 as! UserList) })
            print(arr_userList!)
            break
        case .tags:
            if isRefreshing {
                arr_TagList?.removeAll()
            }
            if arr_TagList == nil {
                arr_TagList = []
            }
            list.forEach({ arr_TagList?.append($0 as! TagList) })
            print(arr_TagList!)
            break
        }
    }
}
//MARk:- Api Call
extension SettingsArchivedModel {
    func companylistService(isRefreshing:Bool) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_grid_type:APIKey.key_archived]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.updateList(list: list, offset: 0, isRefreshing: isRefreshing)
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    func userListService(isRefreshing:Bool, textSearch:String) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset,APIKey.key_search:textSearch,APIKey.key_grid_type:APIKey.key_archived] as [String : Any]
        CommanListWebservice.shared.getAllUserList(parameter: parameter, success: { [weak self] (list, nextOffset)  in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    func tagListService(isRefreshing:Bool) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_grid_type:APIKey.key_archived]
        ActionsWebService.shared.getTagList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.updateList(list: list, offset: 0, isRefreshing: isRefreshing)
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    
}

    
