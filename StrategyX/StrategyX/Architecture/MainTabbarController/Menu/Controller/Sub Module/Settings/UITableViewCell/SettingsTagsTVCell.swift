//
//  SettingsTagsTVCell.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SettingsTagsTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configure(theModel:TagList) {
        hideAnimation()
        lblTitle.text = theModel.tag_name
    }
    
    func configure(theModel:TrainingList) {
        hideAnimation()
        lblTitle.text = theModel.title
    }
    
    func hideAnimation() {
        [lblTitle].forEach({ $0?.hideSkeleton() })
    }
    
    func startAnimation() {
        [lblTitle].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
    }
    
}
