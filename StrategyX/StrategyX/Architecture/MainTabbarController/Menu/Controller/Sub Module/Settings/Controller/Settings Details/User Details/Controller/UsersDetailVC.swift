//
//  UsersDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Photos

class UsersDetailVC: ParentViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:UsersDetailView = { [unowned self] in
      return self.view as! UsersDetailView
    }()
    fileprivate lazy var theCurrentModel:UsersDetailModel = {
       return UsersDetailModel(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        getUserDetailView()
    }
    
    func setTheData(theModel:UserList?) {
        theCurrentModel.theUserDetailModel = theModel
    }
    func updateViewDetail() {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        theCurrentView.img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.userImage, placeholder: "ic_big_plash_holder")
//        sd_setImage(with: URL.init(string: theModel.userImage), placeholderImage: UIImage.init(named: "ic_big_plash_holder"))
        theCurrentView.lblUserName.text = theModel.username
        theCurrentView.lblDepartMent.text = theModel.departmentsCount
        theCurrentView.lblEmail.text = theModel.userEmail
        theCurrentView.lblMobile.text = theModel.mobileNumber
        theCurrentView.lblRole.text = theModel.userType
        theCurrentView.viewManager.isHidden = theModel.managers.count == 0
        theCurrentView.lblManager.text = theModel.managers.map({$0.fullName}).joined(separator: ",")
    }
    
    //MARK:- IBAction
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnAddImageAction(_ sender: Any) {
        /*if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            print("Button capture")
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerController.SourceType.photoLibrary;
            //imag.mediaTypes = [kUTTypeImage];
            imag.allowsEditing = false
            self.present(imag, animated: true, completion: nil)
        }*/
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
      /*  if let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            print("assetPath.absoluteString:=",assetPath.absoluteString)
            if (assetPath.absoluteString.lowercased().contains("jpg".lowercased())) {
                sonu = "JPG"
                print("JPG")
            }
            else if (assetPath.absoluteString.hasSuffix("PNG")) {
                sonu = "PNG"
                print("PNG")
            }
            else if (assetPath.absoluteString.hasSuffix("GIF")) {
                sonu = "GIF"
                print("GIF")
            }
            else {
                sonu = "Unknown"
                print("Unknown")
            }
            /*if assetPath.absoluteString.lowercased().contains(".gif") {
                let data = UIImage.gifImageWithURL(assetPath.absoluteString)
                theCurrentView.img_Profile.image = data

            }*/
        }*/
        
        var phAsset: PHAsset?
        if #available(iOS 11.0, *) {
            phAsset = info[.phAsset] as? PHAsset
        } else {
            if let url = info[.referenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
                phAsset = result.lastObject
            }
        }
        
//        guard let imageURL = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset else { return }
        
        guard let asset = phAsset else { return }
        
        if picker.sourceType == .photoLibrary || picker.sourceType == .savedPhotosAlbum {
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            options.isNetworkAccessAllowed = false
//            options.deliveryMode = .highQualityFormat
            PHImageManager.default().requestImageData(for: asset, options: options) { data, uti, orientation, info in
                guard let info = info else { return }
                
                if let error = info[PHImageErrorKey] as? Error {
                    print("Cannot fetch data for GIF image: \(error)")
                    return
                }
                
                if let isInCould = info[PHImageResultIsInCloudKey] as? Bool, isInCould {
                    print("Cannot fetch data from cloud. Option for network access not set.")
                    return
                }
                
                // do something with data (it is a Data object)
                if let imageData = data {
                    let dataGIF = UIImage.animatedImage(data: imageData)
                    self.theCurrentView.img_Profile.image = dataGIF
                }
            }
        } else {
            // do something with media taken via camera
        }
        
        
        picker.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func onBtnEditProfileAction(_ sender: Any) {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        let vc = AddUserVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setupEditScreenData(type: .editUser,theModel: theModel)
        vc.handlerEditUserData = {[weak self] data in
            self?.theCurrentModel.theUserDetailModel = data
            self?.updateViewDetail()
        }
        self.push(vc: vc)
    }
    
}

//MARK:- Call API
extension UsersDetailVC{
    
    func getUserDetailView()
    {
        guard let userid = theCurrentModel.theUserDetailModel?.userId else {
            return
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_view_user_id: userid,
                         ]
        
        AddUserWebService.shared.getUserDetail(parameter: parameter, success: { [weak self] (detail) in
            
            self?.theCurrentModel.theUserDetailModel = detail
            self?.updateViewDetail()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
}
