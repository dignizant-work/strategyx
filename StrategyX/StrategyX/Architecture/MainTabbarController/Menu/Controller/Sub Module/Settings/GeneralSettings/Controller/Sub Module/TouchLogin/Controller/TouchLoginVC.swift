//
//  TouchLoginVC.swift
//  StrategyX
//
//  Created by Haresh on 20/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
class TouchLoginVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView: TouchLoginView = { [unowned self] in
        return self.view as! TouchLoginView
    }()
    
    lazy var theCurrentViewModel: TouchLoginViewModel = {
        return TouchLoginViewModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        if let _ = UserDefault.shared.getTouchUserData() {
            theCurrentView.updateBtnActiveToggle()
        }
    }
    
    
//    MARK:- IBAction
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnToggleAction(_ sender: UIButton) {
        BiometricIDAuth.shared.authenicateUser { [weak self] (strErrorMsg,isRedirectToSetting)  in
            if isRedirectToSetting {
                if let msg = strErrorMsg {
                    self?.showAlertAction(msg: msg, completion: {
                        guard let urlSetting = URL.init(string: UIApplication.openSettingsURLString) else { return }
                        UIApplication.shared.open(urlSetting, options: [:], completionHandler: nil)
                    })
                }
                return
            }
            if let msg = strErrorMsg,!msg.trimmed().isEmpty {
                if !msg.isEmpty {
                    self?.showAlert(msg: msg)
                } else {
                    
                }
            } else if strErrorMsg == nil && isRedirectToSetting == false {
                // successfully athenicate
                if sender.isSelected {
                    UserDefault.shared.removeTouchUserData()
                } else {
                    if let lastUserLoginData = UserDefault.shared.getLastUserLoginData() {
                        let touch = JSON(["email":(lastUserLoginData.email).trimmed(),"password":(lastUserLoginData.password).trimmed()])
                        UserDefault.shared.saveTouchUserData(user: touch)
                    } else {
                        let touch = JSON(["email":(Utility.shared.userData.email).trimmed(),"password":(Utility.shared.userData.password).trimmed()])
                        UserDefault.shared.saveTouchUserData(user: touch)
                    }
                }
                self?.theCurrentView.updateBtnActiveToggle()
            }
        }
    }
    
}
