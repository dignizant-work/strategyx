//
//  CompanyStuffDetailView.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyStuffDetailView: ViewParentWithoutXIB {

    //Variable
    
    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var lblDepartMent: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var viewManager: UIView!
    @IBOutlet weak var lblManager: UILabel!

    
    //MARK:- LifeCycle
    func setupLayout() {
        viewManager.isHidden = true

    }
    

}
