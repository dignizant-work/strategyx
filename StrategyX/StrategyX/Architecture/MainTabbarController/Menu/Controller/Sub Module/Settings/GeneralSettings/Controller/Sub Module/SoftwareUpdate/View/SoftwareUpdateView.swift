//
//  SoftwareUpdateView.swift
//  StrategyX
//
//  Created by Haresh on 12/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SoftwareUpdateView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var lblVersionNumber: UILabel!
    @IBOutlet weak var lblLastcheckedDate: UILabel!
    @IBOutlet weak var viewCheckforUpdate: UIView!
    
    @IBOutlet weak var btnProgress: UIButton!
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }
    
    func updateTitle(version:String,time:String) {
        lblVersionNumber.text = "App Version: " + version
        lblLastcheckedDate.text = "Last successful check for update at " + time
        lblLastcheckedDate.isHidden = (time.isEmpty || time == "0000-00-00 00:00:00")
    }
    
    func checkForUpdateIsHidden(_ isHidden:Bool) {
        lblVersionNumber.isHidden = isHidden
        lblLastcheckedDate.isHidden = isHidden
        viewCheckforUpdate.isHidden = isHidden
    }
    
}
