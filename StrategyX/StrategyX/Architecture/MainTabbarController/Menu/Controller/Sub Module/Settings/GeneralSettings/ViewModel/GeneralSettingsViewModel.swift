//
//  GeneralSettingsViewModel.swift
//  StrategyX
//
//  Created by Haresh on 20/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class GeneralSettingsViewModel {

    fileprivate weak var theController:GeneralSettingsVC!
    
    var isActiveToggle:Bool = false {
        didSet {
            (theController.view as? GeneralSettingsView)?.btnToggleBioMetric.isSelected = isActiveToggle
        }
    }
    
    //MARK:- LifeCycle
    init(theController:GeneralSettingsVC) {
        self.theController = theController
    }
}
