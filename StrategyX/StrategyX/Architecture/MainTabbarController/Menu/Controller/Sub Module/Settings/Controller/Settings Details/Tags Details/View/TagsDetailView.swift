//
//  TagsDetailView.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TagsDetailView: ViewParentWithoutXIB {
    
    //MARK:- IBOutlet
    @IBOutlet weak var lblTagName: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnActive: UIButton!
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        updateBtnStatus()
        updateBtnActiveToggle()
    }
    
    func updateBtnActiveToggle() {
        btnActive.isSelected = !btnActive.isSelected
    }
    
    func updateBtnStatus() {
        btnEdit.isSelected = !btnEdit.isSelected
        btnBack.isSelected = btnEdit.isSelected
        btnActive.isUserInteractionEnabled = btnEdit.isSelected
    }
    

}
