//
//  SettingsVC.swift
//  StrategyX
//
//  Created by Haresh on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class SettingsVC: ParentViewController {

    //MARK:- Variable
    enum titleSelectionType:String {
        case companies          = "Companies"
        case users              = "Users"
        case tags               = "Tags"
    }
    
    fileprivate lazy var theCurrentView:SettingsView = { [unowned self] in
       return self.view as! SettingsView
    }()
    
    fileprivate lazy var theCurrentModel:SettingsModel = {
       return SettingsModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentModel.configure(dropDown: theCurrentModel.titleTypeDD, view: theCurrentView.viewTitle)
        update(text: theCurrentModel.selectedTitleType.rawValue, dropDown: theCurrentModel.titleTypeDD)

    }
    func setSelectionType(type:SettingsVC.titleSelectionType) {
        theCurrentModel.selectedTitleType =  type
    }
    func refreshApiCall() {

        theCurrentModel.isSearching = false
        theCurrentView.txtSearch.text = ""
        theCurrentView.txtSearch.resignFirstResponder()
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            theCurrentModel.arr_companyList?.removeAll()
            theCurrentModel.arr_companyList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.companylistService(isRefreshing: true)
            break
        case .users:
            theCurrentModel.arr_userList?.removeAll()
            theCurrentModel.arr_userList = nil
            theCurrentModel.nextOffset = 0
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.userListService(isRefreshing: true, textSearch: "")
            break
        case .tags:
            theCurrentModel.arr_TagList?.removeAll()
            theCurrentModel.arr_TagList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.tagListService(isRefreshing: true)
            break
        }
    }
    func refreshTagListApi() {
        if theCurrentModel.selectedTitleType == .tags {
            theCurrentModel.arr_TagList?.removeAll()
            theCurrentModel.arr_TagList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.tagListService(isRefreshing: true)
        }
    }
    func update(text:String,dropDown:DropDown) {
        rotateDropdownArrow()
        theCurrentView.viewArchived.isHidden = true
        theCurrentView.viewAddUser.isHidden = true
        theCurrentView.viewAddTag.isHidden = true
        theCurrentView.viewAddCompany.isHidden = true

        switch dropDown {
        case theCurrentModel.titleTypeDD:
            switch text.localizedLowercase {
            case titleSelectionType.companies.rawValue.localizedLowercase:
                theCurrentModel.selectedTitleType = .companies
//                theCurrentView.viewAddCompany.isHidden = false
                theCurrentView.viewArchived.isHidden = false
                break
            case titleSelectionType.users.rawValue.localizedLowercase:
                theCurrentModel.selectedTitleType = .users
//                theCurrentView.viewAddUser.isHidden = false
                break
            case titleSelectionType.tags.rawValue.localizedLowercase:
                theCurrentModel.selectedTitleType = .tags
                theCurrentView.viewAddTag.isHidden = false
                break
            default:
                break
            }
            theCurrentView.lblDropDownTitle.text = text
            
            break
        default:
            break
        }
        refreshApiCall()
    }
    func searchText(txt:String, endEditing:Bool = false) {
        theCurrentModel.isSearching = !txt.trimmed().isEmpty
        theCurrentModel.arr_Search.removeAll()

        switch theCurrentModel.selectedTitleType {
        case .companies:
            if let filter = theCurrentModel.arr_companyList?.filter({ $0.name.lowercased().contains(txt.lowercased()) }) {
                theCurrentModel.arr_Search = filter
            }
            break
        case .users:
            if endEditing {
                theCurrentModel.arr_userList?.removeAll()
                theCurrentModel.arr_userList = nil
                theCurrentModel.nextOffset = 0
                theCurrentView.tableView.backgroundView = nil
                theCurrentView.tableView.reloadData()
                theCurrentModel.userListService(isRefreshing: true, textSearch: txt)
            }
            break
        case .tags:
            if let filter = theCurrentModel.arr_TagList?.filter({ $0.tag_name.lowercased().contains(txt.lowercased()) }) {
                theCurrentModel.arr_Search = filter
            }
            break
        }
        theCurrentView.tableView.reloadData()
    }
    func rotateDropdownArrow(at rotate:CGFloat = 0) {
        theCurrentView.img_DropDownArrow.rotateArrow(at:rotate)
    }
    func updateCompanyListModel(theCompanyModel:Company) {
        if theCurrentModel.isSearching {
            if let index = theCurrentModel.arr_Search.firstIndex(where: {($0 as? Company)?.id == theCompanyModel.id}) {
                theCurrentModel.arr_Search.remove(at: index)
                theCurrentModel.arr_Search.insert(theCompanyModel, at: index)
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        } else {
            if let index = theCurrentModel.arr_companyList?.firstIndex(where: {$0.id == theCompanyModel.id}) {
                theCurrentModel.arr_companyList?.remove(at: index)
                theCurrentModel.arr_companyList?.insert(theCompanyModel, at: index)
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
        
    }
    func updateTagListModel(theTagModel:TagList) {
        if let index = theCurrentModel.arr_TagList?.firstIndex(where: {$0.tag_id == theTagModel.tag_id}) {
            theCurrentModel.arr_TagList?.remove(at: index)
            theCurrentModel.arr_TagList?.insert(theTagModel, at: index)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    
    func updateUserListModel(theUserListModel:UserList) {
        if let index = theCurrentModel.arr_userList?.firstIndex(where: {$0.userId == theUserListModel.userId}) {
            theCurrentModel.arr_userList?.remove(at: index)
            theCurrentModel.arr_userList?.insert(theUserListModel, at: index)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    func updateTableViewData() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    func setBackground(strMsg:String) {
        var isShow = false
        theCurrentView.refreshControl.endRefreshing()

        switch theCurrentModel.selectedTitleType {
        case .companies:
            if theCurrentModel.arr_companyList == nil || theCurrentModel.arr_companyList?.count == 0 {
                isShow = true
            }
            break
        case .users:
            if theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0 {
                isShow = true
            }
            break
        case .tags:
            if theCurrentModel.arr_TagList == nil || theCurrentModel.arr_TagList?.count == 0 {
                isShow = true
            }
            break
        }
        
        if isShow {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    func setTableviewRowCount() -> Int {
        var count = 10
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_companyList == nil || theCurrentModel.arr_companyList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_companyList?.count ?? 10
            }
            break
        case .users:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_userList?.count ?? 10
            }
            break
        case .tags:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_TagList == nil || theCurrentModel.arr_TagList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_TagList?.count ?? 10
            }
            break
        }
        theCurrentView.tableView.backgroundView = nil
        
        return count
    }
    //MARK:- Redirection
    func redirectToArchivedVC() {
        let vc = SettingsArchivedVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(type: theCurrentModel.selectedTitleType)
        self.push(vc: vc)
    }
    func redirectToCompaniesDetailsVC(theModel:Company) {
        let vc = CompaniesDetailsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel, isForArchive: false)
        self.push(vc: vc)
    }
    func redirectToUsersDetailsVC(theModel:UserList) {
        let vc = UsersDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.push(vc: vc)
    }
    func redirectToTagsDetailsVC(theModel:TagList) {
        let vc = TagsDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.push(vc: vc)
    }
    func presentSearchFilter(type:titleSelectionType) {
        let vc = SearchFilterVC.init(nibName: "SearchFilterVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        switch type {
        case .companies:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.company)
            break
        case .users:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.users)
            break
        case .tags:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.tags)
            break
        }
/*        vc.handlerSearchText = { [weak self] (text) in
            
        } */
        
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToAddCompanyScreen() {
        let vc = AddCompanyVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        self.push(vc: vc)
    }
    func redirectToAddUserScreen() {
        let vc = AddUserVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
       /*vc.handlerAddUserData = { [weak self] in
            self?.refreshApiCall()
        }*/
        self.push(vc: vc)
    }
    func redirectToAddTagsScreen() {
        let vc = AddTagsVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        /*vc.handlerAddTagData = { [weak self] in
            self?.refreshApiCall()
        }*/
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnAddCompanyAction(_ sender: Any) {
        redirectToAddCompanyScreen()
    }
    
    @IBAction func onBtnAddTagAction(_ sender: Any) {
        redirectToAddTagsScreen()
    }
    @IBAction func onBtnAddUserAction(_ sender: Any) {
        redirectToAddUserScreen()
    }
    @IBAction func onBtnDropDownAction(_ sender: Any) {
        theCurrentModel.titleTypeDD.show()
        rotateDropdownArrow(at: CGFloat.pi)
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentSearchFilter(type: theCurrentModel.selectedTitleType)
    }
    
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    
}
//MARK:-UITableViewDataSource
extension SettingsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if theCurrentModel.isSearching && theCurrentModel.selectedTitleType != titleSelectionType.users {
            return theCurrentModel.arr_Search.count
        }
        return setTableviewRowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCompaniesTVCell")
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCompaniesTVCell") as! SettingsCompaniesTVCell
            if theCurrentModel.isSearching {
                (cell as? SettingsCompaniesTVCell)?.configure(theModel: theCurrentModel.arr_Search[indexPath.row] as! Company)
            } else {
                if theCurrentModel.arr_companyList != nil {
                    (cell as? SettingsCompaniesTVCell)?.configure(theModel: theCurrentModel.arr_companyList![indexPath.row])
                } else {
                    (cell as? SettingsCompaniesTVCell)?.startAnimation()
                }
            }
            
            break
        case .users:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsUsersTVCell") as! SettingsUsersTVCell
            if theCurrentModel.arr_userList != nil {
                (cell as? SettingsUsersTVCell)?.configure(theModel: theCurrentModel.arr_userList![indexPath.row])
            } else {
                (cell as? SettingsUsersTVCell)?.startAnimation()
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 && !theCurrentModel.isSearching {
                theCurrentModel.userListService(isRefreshing: false, textSearch: "")
            }
            break
        case .tags:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTagsTVCell") as! SettingsTagsTVCell
            if theCurrentModel.isSearching {
                (cell as? SettingsTagsTVCell)?.configure(theModel: theCurrentModel.arr_Search[indexPath.row] as! TagList)
            } else {
                if theCurrentModel.arr_TagList != nil {
                    (cell as? SettingsTagsTVCell)?.configure(theModel: theCurrentModel.arr_TagList![indexPath.row])
                } else {
                    (cell as? SettingsTagsTVCell)?.startAnimation()
                }
            }
            break
        }

        return cell!
    }
    
}
//MARK:-UITableViewDelegate
extension SettingsVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if theCurrentModel.selectedTitleType == titleSelectionType.users && !theCurrentModel.isSearching {
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_userList != nil {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = appGreen
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                theCurrentView.tableView.tableFooterView = spinner
                theCurrentView.tableView.tableFooterView?.isHidden = false
            } else {
                theCurrentView.tableView.tableFooterView?.isHidden = true
                theCurrentView.tableView.tableFooterView = nil
            }
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil

        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch theCurrentModel.selectedTitleType {
        case .companies:
            guard let arr_company = theCurrentModel.arr_companyList else { return }
            let theModel = theCurrentModel.isSearching ? theCurrentModel.arr_Search[indexPath.row] as! Company : arr_company[indexPath.row]
            redirectToCompaniesDetailsVC(theModel: theModel)
            break
        case .users:
            guard let arr_user = theCurrentModel.arr_userList else { return }
            redirectToUsersDetailsVC(theModel: arr_user[indexPath.row])
            break
        case .tags:
            guard let arr_tag = theCurrentModel.arr_TagList else { return }
            redirectToTagsDetailsVC(theModel: arr_tag[indexPath.row])
            break
        }
    }
}
