//
//  CompaniesDetailsModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class CompaniesDetailsModel {

    //MARK:- Variable
    fileprivate weak var theController:CompaniesDetailsVC!
    var theCompanyModel:Company? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                if let index = controller.firstIndex(where: {( $0 as? SettingsVC) != nil }), theCompanyModel != nil {
                    (controller[index] as? SettingsVC)?.updateCompanyListModel(theCompanyModel:theCompanyModel!)
                }
            }
        }
    }
    var isForArchive = false
    var arr_userList:[UserList]?
    var nextOffset = 0
    var arr_DepartmentList:[String] = []

    
    //MARK:- LifeCycle
    init(theController:CompaniesDetailsVC) {
        self.theController = theController
    }
    
    func updateList(list:[UserList], offset:Int, isRefreshing:Bool) {
        nextOffset = offset
        if isRefreshing {
            arr_userList?.removeAll()
        }
        if arr_userList == nil {
            arr_userList = []
        }
        list.forEach({ arr_userList?.append($0) })
        print(arr_userList!)
    }
    
    
}
//MARK:- Api call
extension CompaniesDetailsModel {
    func userListService(isRefreshing:Bool) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset,APIKey.key_search:"",APIKey.key_company_id:theCompanyModel?.id ?? ""] as [String : Any]
        CommanListWebservice.shared.getAllUserList(parameter: parameter, success: { [weak self] (list, nextOffset)  in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theController.updateTableViewData()
            }, failed: { [weak self] (error) in
                self?.theController.setBackground(strMsg: error)
        })
    }
    
    func addDepartment(success:@escaping() -> Void,failed:@escaping() -> Void) {
        var departmentName:[[String:String]] = []
        arr_DepartmentList.forEach({departmentName.append(["name":$0])})
        let department = JSON(departmentName).rawString() ?? ""
        let departmentModel = theCompanyModel!
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:nextOffset,APIKey.key_company_id:theCompanyModel?.id ?? "",APIKey.key_department_name:department] as [String : Any]
        
        CompanyWebService.shared.addDepartment(parameter: parameter, success: { [weak self] (msg, count, strDepartmentID,strDepartmentName) in
            departmentModel.department = count
//            self?.theCompanyModel = departmentModel
            self?.theController.updateViewCompanyDetail(theModel: departmentModel)
//            self?.theController.showAlertAtBottom(message: msg)
            success()
        }, failed: { [weak self] (error) in
            self?.theController.showAlertAtBottom(message: error)
            failed()
        })
        
    }
}
