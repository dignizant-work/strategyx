//
//  GeneralSettingsView.swift
//  StrategyX
//
//  Created by Haresh on 20/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class GeneralSettingsView: ViewParentWithoutXIB {

    
    @IBOutlet weak var lblSoftwareBadgeCount: UILabel!
    @IBOutlet weak var viewSoftwareCountBadge: UIView!
    @IBOutlet weak var viewSoftwareUpdate: UIView!
    @IBOutlet weak var btnToggleBioMetric: UIButton!
    @IBOutlet weak var lblBiometricTitle: UILabel!
    @IBOutlet weak var viewBiometric: UIView!
    @IBOutlet weak var viewTags: UIView!
    @IBOutlet weak var viewUsers: UIView!
    @IBOutlet weak var viewCompanies: UIView!
    @IBOutlet weak var viewDepartment: UIView!
    
    //MARK:- LifeCycle
    func setupLayout() {
        viewCompanies.isHidden = false
        viewUsers.isHidden = false
        viewTags.isHidden = true
        viewDepartment.isHidden = true
        
        switch BiometricIDAuth.shared.biometricType() {
        case .faceID:
            lblBiometricTitle.text = "Face ID Login"
            break
        case .touchID:
            lblBiometricTitle.text = "Touch ID Login"
            break
        default:
            viewBiometric.isHidden = true
            break
        }
        
        if UserDefault.shared.userRole == .companyAdminUser {
            viewCompanies.isHidden = true
            viewDepartment.isHidden = false
        } else if UserDefault.shared.userRole == .manager {
            viewCompanies.isHidden = true
        } else if UserDefault.shared.userRole == .staffUser || UserDefault.shared.userRole == .staffLite  {
            viewCompanies.isHidden = true
            viewUsers.isHidden = true
        }
       
    }
    
    func updateBtnActiveToggle() {
        btnToggleBioMetric.isSelected = !btnToggleBioMetric.isSelected
    }
    
    func updateSoftwareBadgeCount(count:Int) {
        viewSoftwareCountBadge.isHidden = count == 0
        lblSoftwareBadgeCount.text = "\(count)"
    }

}
