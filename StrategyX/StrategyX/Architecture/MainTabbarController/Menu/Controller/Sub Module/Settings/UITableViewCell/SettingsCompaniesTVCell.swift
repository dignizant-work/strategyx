//
//  SettingsCompaniesTVCell.swift
//  StrategyX
//
//  Created by Haresh on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SettingsCompaniesTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDepartMent: UILabel!
    @IBOutlet weak var lblUserCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(theModel:Company) {
        hideAnimation()
        lblTitle.text = theModel.name
        lblDepartMent.text = "\(theModel.department)"
        lblUserCount.text = "\(theModel.user)"
    }
    
    func hideAnimation() {
        [lblTitle,lblDepartMent,lblUserCount].forEach({ $0?.hideSkeleton() })
    }
    
    func startAnimation() {
        [lblTitle,lblDepartMent,lblUserCount].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
        lblDepartMent.text = " "
        lblUserCount.text = " "
    }
    
}
