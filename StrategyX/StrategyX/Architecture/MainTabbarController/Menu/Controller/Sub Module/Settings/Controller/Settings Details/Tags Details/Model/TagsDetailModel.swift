//
//  TagsDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TagsDetailModel {

    //MARK:- Variable
    fileprivate weak var theController:TagsDetailVC!
    var theTagDetailModel:TagList? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                if let index = controller.firstIndex(where: {( $0 as? SettingsVC) != nil }) {
                    (controller[index] as? SettingsVC)?.updateTagListModel(theTagModel:theTagDetailModel!)
                }
            }
        }
    }
    var lastStateOfSelectedTag = false
    
    //MARK:- LifeCycle
    init(theController:TagsDetailVC) {
        self.theController = theController
    }
    
}
