//
//  CompanyStuffDetailModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyStuffDetailModel {

    //MARK:- Variable
    fileprivate weak var theController:CompanyStuffDetailVC!
    
    
    var theUserDetailModel:UserList? = nil{
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
                    (vc as? CompaniesDetailsVC)?.updateUserListModel(theUserListModel: theUserDetailModel!)
                }
            }
        }
    }
    
    
    //MARK:- LifeCycle
    init(theController:CompanyStuffDetailVC) {
        self.theController = theController
    }
    

}
