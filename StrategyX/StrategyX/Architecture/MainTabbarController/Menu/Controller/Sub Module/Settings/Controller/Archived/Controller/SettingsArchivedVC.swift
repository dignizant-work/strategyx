//
//  SettingsArchivedVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SettingsArchivedVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView: SettingsArchivedView = { [unowned self] in
       return self.view as! SettingsArchivedView
    }()
    fileprivate lazy var theCurrentModel:SettingsArchivedModel = {
        return SettingsArchivedModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        refreshApiCall()

    }
    func setTheData(type:SettingsVC.titleSelectionType) {
        theCurrentModel.selectedTitleType = type
    }
    func refreshApiCall() {
        
        theCurrentModel.isSearching = false
        theCurrentView.txtSearch.text = ""
        theCurrentView.txtSearch.resignFirstResponder()
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            theCurrentModel.arr_companyList?.removeAll()
            theCurrentModel.arr_companyList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.companylistService(isRefreshing: true)
            break
        case .users:
            theCurrentModel.arr_userList?.removeAll()
            theCurrentModel.arr_userList = nil
            theCurrentModel.nextOffset = 0
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.userListService(isRefreshing: true, textSearch: "")
            break
        case .tags:
            theCurrentModel.arr_TagList?.removeAll()
            theCurrentModel.arr_TagList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentModel.tagListService(isRefreshing: true)
            break
        }
    }
    
    func searchText(txt:String, endEditing:Bool = false) {
        theCurrentModel.isSearching = !txt.trimmed().isEmpty
        theCurrentModel.arr_Search.removeAll()
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            if let filter = theCurrentModel.arr_companyList?.filter({ $0.name.lowercased().contains(txt.lowercased()) }) {
                theCurrentModel.arr_Search = filter
            }
            break
        case .users:
            if endEditing {
                theCurrentModel.arr_userList?.removeAll()
                theCurrentModel.arr_userList = nil
                theCurrentModel.nextOffset = 0
                theCurrentView.tableView.backgroundView = nil
                theCurrentView.tableView.reloadData()
                theCurrentModel.userListService(isRefreshing: true, textSearch: txt)
            }
            
            //            if let filter = theCurrentModel.arr_userList?.filter({ $0.username.lowercased().contains(txt.lowercased()) }) {
            //                theCurrentModel.arr_Search = filter
            //            }
            break
        case .tags:
            if let filter = theCurrentModel.arr_TagList?.filter({ $0.tag_name.lowercased().contains(txt.lowercased()) }) {
                theCurrentModel.arr_Search = filter
            }
            break
        }
        theCurrentView.tableView.reloadData()
    }
    func updateTableViewData() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    func setBackground(strMsg:String) {
        var isShow = false
        theCurrentView.refreshControl.endRefreshing()
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            if theCurrentModel.arr_companyList == nil || theCurrentModel.arr_companyList?.count == 0 {
                isShow = true
            }
            break
        case .users:
            if theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0 {
                isShow = true
            }
            break
        case .tags:
            if theCurrentModel.arr_TagList == nil || theCurrentModel.arr_TagList?.count == 0 {
                isShow = true
            }
            break
        }
        
        if isShow {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    func setTableviewRowCount() -> Int {
        var count = 10
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_companyList == nil || theCurrentModel.arr_companyList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_companyList?.count ?? 10
            }
            break
        case .users:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_userList == nil || theCurrentModel.arr_userList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_userList?.count ?? 10
            }
            break
        case .tags:
            if let _ = theCurrentView.tableView.backgroundView , (theCurrentModel.arr_TagList == nil || theCurrentModel.arr_TagList?.count == 0) {
                count = 0
                return count
            } else {
                count = theCurrentModel.arr_TagList?.count ?? 10
            }
            break
        }
        theCurrentView.tableView.backgroundView = nil
        
        return count
    }
    //MARK:- Redirect
    func redirectToCompaniesDetailsVC(theModel:Company) {
        let vc = CompaniesDetailsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel, isForArchive: true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func redirectToUsersDetailsVC(theModel:UserList) {
        let vc = UsersDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.push(vc: vc)
    }
    func redirectToTagsDetailsVC() {
        let vc = TagsDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.push(vc: vc)
    }
    func presentSearchFilter(type:SettingsVC.titleSelectionType) {
        let vc = SearchFilterVC.init(nibName: "SearchFilterVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        switch type {
        case .companies:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.company)
            break
        case .users:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.users)
            break
        case .tags:
            vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: SearchFilterModel.searchFilterType.tags)
            break
        }
        vc.handlerSearchText = { (text) in
            
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }

    
    //MARK:- IBAction
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentSearchFilter(type: theCurrentModel.selectedTitleType)
    }
    
}
//MARK:-UITableViewDataSource
extension SettingsArchivedVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if theCurrentModel.isSearching && theCurrentModel.selectedTitleType != SettingsVC.titleSelectionType.users {
            return theCurrentModel.arr_Search.count
        }
        return setTableviewRowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCompaniesTVCell")
        
        switch theCurrentModel.selectedTitleType {
        case .companies:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCompaniesTVCell") as! SettingsCompaniesTVCell
            if theCurrentModel.isSearching {
                (cell as? SettingsCompaniesTVCell)?.configure(theModel: theCurrentModel.arr_Search[indexPath.row] as! Company)
            } else {
                if theCurrentModel.arr_companyList != nil {
                    (cell as? SettingsCompaniesTVCell)?.configure(theModel: theCurrentModel.arr_companyList![indexPath.row])
                } else {
                    (cell as? SettingsCompaniesTVCell)?.startAnimation()
                }
            }
            
            break
        case .users:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsUsersTVCell") as! SettingsUsersTVCell
            if theCurrentModel.arr_userList != nil {
                (cell as? SettingsUsersTVCell)?.configure(theModel: theCurrentModel.arr_userList![indexPath.row])
            } else {
                (cell as? SettingsUsersTVCell)?.startAnimation()
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 && !theCurrentModel.isSearching {
                theCurrentModel.userListService(isRefreshing: false, textSearch: "")
            }
            
            //            (cell as? SettingsUsersTVCell)?.configure(strTitle: "Gabriel Moriera")
            break
        case .tags:
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTagsTVCell") as! SettingsTagsTVCell
            if theCurrentModel.isSearching {
                (cell as? SettingsTagsTVCell)?.configure(theModel: theCurrentModel.arr_Search[indexPath.row] as! TagList)
            } else {
                if theCurrentModel.arr_TagList != nil {
                    (cell as? SettingsTagsTVCell)?.configure(theModel: theCurrentModel.arr_TagList![indexPath.row])
                } else {
                    (cell as? SettingsTagsTVCell)?.startAnimation()
                }
            }
            
            //            (cell as? SettingsTagsTVCell)?.configure(strTitle: "Strategy tag")
            break
        }
        
        return cell!
    }
    
    
}
//MARK:-UITableViewDelegate
extension SettingsArchivedVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if theCurrentModel.selectedTitleType == SettingsVC.titleSelectionType.users && !theCurrentModel.isSearching {
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_userList != nil {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = appGreen
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                theCurrentView.tableView.tableFooterView = spinner
                theCurrentView.tableView.tableFooterView?.isHidden = false
            } else {
                theCurrentView.tableView.tableFooterView?.isHidden = true
                theCurrentView.tableView.tableFooterView = nil
            }
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch theCurrentModel.selectedTitleType {
        case .companies:
            var companyModel:Company?
            if theCurrentModel.arr_companyList == nil {
                return
            }
            if theCurrentModel.isSearching {
                companyModel = theCurrentModel.arr_Search[indexPath.row] as? Company
            } else {
                companyModel = theCurrentModel.arr_companyList?[indexPath.row]
            }
            if companyModel == nil {
                return
            }
            redirectToCompaniesDetailsVC(theModel: companyModel!)
            break
        case .users:
            var userModel:UserList?
            if theCurrentModel.arr_userList == nil {
                return
            }
            if theCurrentModel.isSearching {
                userModel = theCurrentModel.arr_Search[indexPath.row] as? UserList
            } else {
                userModel = theCurrentModel.arr_userList?[indexPath.row]
            }
            if userModel == nil {
                return
            }
            redirectToUsersDetailsVC(theModel: userModel!)
            break
        case .tags:
            redirectToTagsDetailsVC()
            break
        }
    }
}

