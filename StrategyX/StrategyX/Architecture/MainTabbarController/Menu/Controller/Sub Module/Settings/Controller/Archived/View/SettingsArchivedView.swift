//
//  SettingsArchivedView.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SettingsArchivedView: ViewParentWithoutXIB {

    //MARK:- Variabel
    
    @IBOutlet weak var btnBackArchived: UIButton!
    @IBOutlet weak var constrainBtnBackArchivedWidth: NSLayoutConstraint!// 35.0
    
    @IBOutlet weak var btnGraph: UIButton!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    let disposeBag = DisposeBag()
    
    //MARK:- Life Cycle
    func setupLayout() {
        backButtonArchived(isHidden: false)
//        updateFilterCountText(strCount: "2")
        onTextFieldSearchAction()
    }
    
    func setupTableView(theDelegate:SettingsArchivedVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(SettingsCompaniesTVCell.self)
        tableView.registerCellNib(SettingsUsersTVCell.self)
        tableView.registerCellNib(SettingsTagsTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? SettingsArchivedVC)?.refreshApiCall()
    }
    
    func backButtonArchived(isHidden:Bool) {
        constrainBtnBackArchivedWidth.constant = isHidden ? 0.0 : 35.0
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
        self.layoutIfNeeded()
    }
    
    //RX Action
    func onTextFieldSearchAction() {
        txtSearch.rx.text.orEmpty
            .subscribe(onNext:{ (text) in
                (self.parentContainerViewController() as? SettingsArchivedVC)?.searchText(txt: text)
            }).disposed(by: disposeBag)
        
        txtSearch.rx.controlEvent(UIControl.Event.editingDidEnd)
            .subscribe(onNext: { [weak self] _ in
                let text = self?.txtSearch.text ?? ""
                (self?.parentContainerViewController() as? SettingsArchivedVC)?.searchText(txt: text, endEditing:true)
            }).disposed(by: disposeBag)
        
    }

}
