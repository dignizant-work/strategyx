//
//  CompanyStuffDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyStuffDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CompanyStuffDetailView = { [unowned self] in
       return self.view as! CompanyStuffDetailView
    }()
    fileprivate lazy var theCurrentModel:CompanyStuffDetailModel = {
       return CompanyStuffDetailModel(theController: self)
    }()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        getUserDetailView()
    }
    func setTheData(theModel:UserList?) {
        theCurrentModel.theUserDetailModel = theModel
    }
    func updateViewDetail() {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        theCurrentView.img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.userImage, placeholder: "ic_big_plash_holder")
        theCurrentView.lblUserName.text = theModel.username
        theCurrentView.lblDepartMent.text = theModel.departmentsCount
        theCurrentView.lblEmail.text = theModel.userEmail
        theCurrentView.lblMobile.text = theModel.mobileNumber
        theCurrentView.lblRole.text = theModel.userType
        theCurrentView.viewManager.isHidden = theModel.managers.count == 0
        theCurrentView.lblManager.text = theModel.managers.map({$0.fullName}).joined(separator: ",")

    }
    
    //MARK:- IBAction
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnEditProfileAction(_ sender: Any) {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        let vc = AddUserVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setupEditScreenData(type: .editUser,theModel: theModel)
        vc.handlerEditUserData = {[weak self] data in
            self?.theCurrentModel.theUserDetailModel = data
            self?.updateViewDetail()
        }
        self.push(vc: vc)
    }
    
}

//MARK:- Call API
extension CompanyStuffDetailVC{
    
    func getUserDetailView()
    {
        guard let userid = theCurrentModel.theUserDetailModel?.userId else {
            return
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_view_user_id: userid,
                         ]
        
        AddUserWebService.shared.getUserDetail(parameter: parameter, success: { [weak self] (detail) in
            
            self?.theCurrentModel.theUserDetailModel = detail
            self?.updateViewDetail()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
}
