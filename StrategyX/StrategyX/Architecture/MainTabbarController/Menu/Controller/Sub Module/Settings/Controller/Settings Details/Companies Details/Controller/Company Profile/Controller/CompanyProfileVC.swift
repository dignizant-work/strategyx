//
//  CompanyProfileVC.swift
//  StrategyX
//
//  Created by Haresh on 12/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyProfileVC: ParentViewController {

    //MARK:- Variabel
    fileprivate lazy var theCurrentView:CompanyProfileView = { [unowned self] in
       return self.view as! CompanyProfileView
    }()
    fileprivate lazy var theCurrentModel:CompanyProfileModel = {
        return CompanyProfileModel(theController: self)
    }()
   
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func setTheData(imgBG:UIImage?,theCompanyModel:Company?) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theCompanyModel = theCompanyModel
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        getCompanyDetailWebService()
    }
    func updatetheModel(theModel:CompanyDetail) {
        theCurrentModel.theModelCompanyDetail = theModel
        let theCompanyModel = theCurrentModel.theCompanyModel
        theCompanyModel?.name = theModel.companieName
        theCurrentModel.theCompanyModel = theCompanyModel
        theCurrentView.tableView.reloadData()
    }
    
    //MARK:- Redirection
    func redirectToAddCompanyVC() {
        guard let theModel = theCurrentModel.theModelCompanyDetail else { return }
        let vc = AddCompanyVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG, theCompanyDetailModel: theModel, isForEdit: true)
        self.push(vc: vc)
    }
    
}
//MARK:- UITableViewDataSource
extension CompanyProfileVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableviewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewTVCell") as! HeaderViewTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Details", isForGoal: false)
            
            return cell
        } else if indexPath.row == 1 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Company Name", strDescription: theCurrentModel.theModelCompanyDetail?.companieName ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Company Name",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 2 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Company Abbreviation", strDescription: theCurrentModel.theModelCompanyDetail?.abbreviation ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Company Abbreviation",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 3 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Company Website", strDescription: theCurrentModel.theModelCompanyDetail?.website ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Company Website", strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyLogoTVCell") as! CompanyLogoTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configureLoadImage(strImg: theCurrentModel.theModelCompanyDetail?.profile ?? "")
//            cell.configure(img: theCurrentModel.img_Profile)

            return cell
        } else if indexPath.row == 5 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Company Profile",isHideBottomLine:true, strDescription: theCurrentModel.theModelCompanyDetail?.descriptionCompany ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Company Profile",isHideBottomLine:true, strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSubTitleTVCell") as! HeaderSubTitleTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "COMPANY ADDRESS INFORMATION")
            return cell
        } else if indexPath.row == 7 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Address", strDescription: theCurrentModel.theModelCompanyDetail?.mapAddress ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Address", strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 8 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Country", strDescription: theCurrentModel.theModelCompanyDetail?.country ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Country",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 9 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "State", strDescription: theCurrentModel.theModelCompanyDetail?.state ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "State", strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 10 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "City",isHideBottomLine:true,strDescription: theCurrentModel.theModelCompanyDetail?.city ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "City",isHideBottomLine:true,strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSubTitleTVCell") as! HeaderSubTitleTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "COMPANY ADMIN INFORMATION")
            return cell
        } else if indexPath.row == 12 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Admin Email", strDescription: theCurrentModel.theModelCompanyDetail?.userEmail ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Admin Email",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 13 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Admin Timezone", strDescription: theCurrentModel.theModelCompanyDetail?.timezoneName ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Admin Timezone",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 14 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Admin First Name", strDescription: theCurrentModel.theModelCompanyDetail?.userFirstname ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Admin First Name", strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 15 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Admin Last Name", strDescription: theCurrentModel.theModelCompanyDetail?.userLastname ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Admin Last Name",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 16 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Mobile", strDescription: theCurrentModel.theModelCompanyDetail?.userMobile ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Mobile",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else if indexPath.row == 17 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell3(strDescriptionTitle: "Department Name", strDescription: theCurrentModel.theModelCompanyDetail?.companyDepartment ?? "", isSkeltonAnimation: false)
            } else {
                cell.configureCell3(strDescriptionTitle: "Department Name",strDescription: " ", isSkeltonAnimation: true)
            }
            return cell
        } else { // Last Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4") as! FocusDetailTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.theModelCompanyDetail != nil {
                cell.configureCell4(strBtnSaveTitle:"Edit",isSkeltonAnimation: false)
            } else {
                cell.configureCell4(isSkeltonAnimation: true)
            }
            cell.handlerOnBtnSaveClick = { [weak self] in
                self?.redirectToAddCompanyVC()
            }
            cell.handlerOnBtnCloseClick = { [weak self] in
                self?.backAction(isAnimation: false)
            }
            return cell
        }
    }
}
//MARK:-UITableViewDelegate
extension CompanyProfileVC:UITableViewDelegate {
    
}

//MARK:- Api Call
extension CompanyProfileVC {
    func getCompanyDetailWebService() {
        guard let company_id = theCurrentModel.theCompanyModel?.id else { return }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_company_id:company_id] as [String : Any]
        CompanyWebService.shared.companyDetail(parameter: parameter, success: { [weak self] (theModel) in
                self?.theCurrentModel.theModelCompanyDetail = theModel
                self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
        
    }
}
