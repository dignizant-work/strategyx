//
//  TrainingVideoVC.swift
//  StrategyX
//
//  Created by Jaydeep on 25/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import XCDYouTubeKit
import AVKit
import HCVimeoVideoExtractor

class TrainingVideoVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:TrainingVideoView = { [unowned self] in
        return self.view as! TrainingVideoView
        }()
    
    fileprivate lazy var theCurrentModel:TrainingVideoModel = {
        return TrainingVideoModel(theController: self)
    }()
    
    weak var weakPlayerViewController: AVPlayerViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("TrainingVideoVC")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "videoBounds" {
            if let rect = change?[.newKey] as? NSValue {
                if let newrect = rect.cgRectValue as CGRect? {
                    // 200 is height of playerViewController in normal screen mode
                    print("newrect:=",newrect)
                    if newrect.origin.x < 0 && newrect.size.height > theCurrentView.viewVideoPlayer.frame.size.height {
                        print("full screen")
                        
                    } else {
                        print("normal screen")
                        
                    }
                }
            }
        }
    }
    func setupUI() {
        setLogoOnNavigationHeader()
//        theCurrentView.setupYoutubePlayerDelegate(theDelegate: self)
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: theCurrentView.activityIndicator)
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG, strVideoID: theCurrentModel.theTrainingVideoModel?.videoId ?? "", strTitle: theCurrentModel.theTrainingVideoModel?.title ?? "", strVideoUrl:theCurrentModel.theTrainingVideoModel?.videoUrl ?? "", videoType: theCurrentModel.theTrainingVideoModel?.videoType ?? "youtube")
        playVideoInView()
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }

    }
    
    func setTheData(imgBG:UIImage?, theModel:TrainingVideoList?) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theTrainingVideoModel = theModel
    }
    
    func playVideoInView() {
        let playerViewController = AVPlayerViewController()
//        present(playerViewController, animated: true)
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: theCurrentView.viewVideoPlayer.frame.width, height:theCurrentView.viewVideoPlayer.frame.height)
        theCurrentView.viewVideoPlayer.addSubview(playerViewController.view)
        self.addChild(playerViewController)
        weakPlayerViewController = playerViewController
        weakPlayerViewController?.addObserver(self, forKeyPath: "videoBounds", options: NSKeyValueObservingOptions.new, context: nil)
        
        var isVimeoVideo = false
        
        if let videoType = theCurrentModel.theTrainingVideoModel?.videoType, videoType.lowercased() == VideoType.vimeo.rawValue.lowercased() {
            isVimeoVideo = true
        }
        
        if isVimeoVideo {
            playVimeoVideo()
        } else {
            playYoutubeVideo()
        }
        
    }
    
    private func playYoutubeVideo() {
        XCDYouTubeClient.default().getVideoWithIdentifier(theCurrentModel.theTrainingVideoModel?.videoId ?? "") { [weak self] (video, error) in
            if let obj = self {
                obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
            }
            if video != nil {
                let streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    self?.weakPlayerViewController?.player = AVPlayer(url: streamURL)
                }
                self?.weakPlayerViewController?.player?.play()
                
            } else {
                //                self?.dismiss(animated: true)
            }
        }
    }
    
    private func playVimeoVideo() {
        HCVimeoVideoExtractor.fetchVideoURLFrom(id: theCurrentModel.theTrainingVideoModel?.videoId ?? "") { [weak self] (video, error) in
            DispatchQueue.main.async {
                if let obj = self {
                    obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
                }
                if let err = error {
                    print("Error = \(err.localizedDescription)")
                    self?.showAlertAtBottom(message: "Invalid video url")
                    return
                }
                
                guard let vid = video else {
                    print("Invalid video object")
                    self?.showAlertAtBottom(message: "Invalid video url")
                    return
                }
                let streamURL = vid.videoURL[.Quality1080p] ?? vid.videoURL[.Quality960p] ?? vid.videoURL[.Quality720p] ?? vid.videoURL[.Quality640p] ?? vid.videoURL[.Quality540p] ?? vid.videoURL[.Quality360p]
                if let streamURL = streamURL {
                    self?.weakPlayerViewController?.player = AVPlayer(url: streamURL)
                }
                self?.weakPlayerViewController?.player?.play()
            }
            
        }
    }
  
    //MARK:- Action
    
    @IBAction func onBtnCloseAction(_ sender: UIButton) {
        self.backAction(isAnimation: false)
    }

    deinit {
        weakPlayerViewController?.removeObserver(self, forKeyPath: "videoBounds")
    }
}
/*
//MARK:- Delegate

extension TrainingVideoVC:WKYTPlayerViewDelegate{
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: theCurrentView.activityIndicator)
    }
    
    func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
        print("error:=",error)
    }
}
*/
