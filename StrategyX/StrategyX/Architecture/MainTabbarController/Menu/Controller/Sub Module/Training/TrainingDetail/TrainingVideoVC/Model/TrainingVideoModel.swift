//
//  TrainingVideoModel.swift
//  StrategyX
//
//  Created by Jaydeep on 25/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingVideoModel {
    //MARK:- Variable
    fileprivate weak var theController:TrainingVideoVC!
    
    var imageBG:UIImage?
    var theTrainingVideoModel:TrainingVideoList?
    
    //MARK:- LifeCycle
    init(theController:TrainingVideoVC) {
        self.theController = theController
    }
    
}
