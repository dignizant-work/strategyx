//
//  TrainingVideoView.swift
//  StrategyX
//
//  Created by Jaydeep on 25/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import XCDYouTubeKit
import SDWebImage

class TrainingVideoView:ViewParentWithoutXIB {

    //MARK:- Outlet
    @IBOutlet weak var lblTrainingVideoTitle: UILabel!
    @IBOutlet weak var viewVideoPlayer: UIView!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:- Life Cycle
    
    func setupLayout(imgBG:UIImage?,strVideoID:String, strTitle:String, strVideoUrl:String,videoType:String) {
        img_Bg.image = imgBG
        lblTrainingVideoTitle.text = strTitle
        if strVideoID.isEmpty {
            return
        }
        if videoType.contains("vimeo.com") {
            if  let imageVimeo = SDImageCache.shared.imageFromCache(forKey: strVideoUrl) {
                img.image = imageVimeo
                return
            } else {
                img.image = UIImage(named: "ic_mini_plash_holder")
            }
//            img.sd_setImageLoadMultiTypeURL(url: "\(VIMEOBASEURL)\(strVideoID)_640.jpg", placeholder: "ic_mini_plash_holder")
        } else {
            img.sd_setImageLoadMultiTypeURL(url: "\(YTDBASEURL)\(strVideoID)/0.jpg", placeholder: "ic_mini_plash_holder")
        }
//        img.sd_setImageLoadMultiTypeURL(url: "\(YTDBASEURL)\(strVideoID)/0.jpg", placeholder: "ic_mini_plash_holder")
//        viewVideoPlayer.load(withVideoId: strVideoID)
        configureActivityIndicator(activityIndicator: activityIndicator)
    }
    
//    func setupYoutubePlayerDelegate(theDelegate:TrainingVideoVC) {
//        viewVideoPlayer.delegate = theDelegate
//    }
    
    func configureActivityIndicator(activityIndicator:UIActivityIndicatorView) {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
    }
    
    func statusActivityIndicator(isLoading:Bool=false,activityIndicator:UIActivityIndicatorView){
        if isLoading == true{
            activityIndicator.color = appGreen
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            viewVideoPlayer.isHidden = true
        }else{
            activityIndicator.stopAnimating()
            viewVideoPlayer.isHidden = false
        }
    }
    
}
