//
//  TrainingView.swift
//  StrategyX
//
//  Created by Jaydeep on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingView: ViewParentWithoutXIB {
    
     //MARK:- Variable
    
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()

    
    //MARK:- Life Cycle
    func setupLayout() {
        
       
    }
    
    func setupTableView(theDelegate:TrainingVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.registerCellNib(SettingsTagsTVCell.self)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }

    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? TrainingVC)?.getTrainingListWebService(isRefreshing: true)
    }
    
}

