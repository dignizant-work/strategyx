//
//  TrainingDetailCell.swift
//  StrategyX
//
//  Created by Jaydeep on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage

class TrainingDetailCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_Play: UIImageView!
    @IBOutlet weak var img_Video: UIImageView!
    
    private let vimeoServiceManager = VimeoServiceManger()
    private var trainingVideoModel:TrainingVideoList!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hideAnimation() {
        img_Play.isHidden = false
        [lblTitle, img_Video].forEach({ $0?.hideSkeleton() })
    }
    func startAnimation() {
        img_Play.isHidden = true
        [lblTitle,img_Video].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
    }
    func configure(theModel:TrainingVideoList) {
        hideAnimation()
        trainingVideoModel = theModel
        lblTitle.text = theModel.title
        if VideoType.vimeo.rawValue.lowercased() == theModel.videoType {
            if  let imageVimeo = SDImageCache.shared.imageFromCache(forKey: trainingVideoModel.videoUrl) {
                img_Video.image = imageVimeo
                return
            }
            vimeoServiceManager.retrieveVimeoImage(from: VIMEOCONFIGBASEURL + theModel.videoId +  "/config") { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .failure(_):
                    strongSelf.img_Video.image = UIImage(named: "ic_bg_plash_holder_list")
                    break
                case .success(let (networkUrl, imageUrl)):
                    if VIMEOCONFIGBASEURL + strongSelf.trainingVideoModel.videoId +  "/config" == networkUrl {
//                        strongSelf.avatarImageView.image = image
//                        strongSelf.img_Video.sd_setImageLoadMultiTypeURL(url: <#T##String#>, placeholder: <#T##String#>)
                        SDWebImageManager.shared.loadImage(with: URL.init(string: imageUrl), options: SDWebImageOptions.queryMemoryData, progress: nil) { (image, data, error, cacheType, finished, url) in
                            strongSelf.img_Video.image = image
                            SDImageCache.shared.store(image, forKey: strongSelf.trainingVideoModel.videoUrl, completion: nil)
                        }
                    }
                }
                
            }
        } else {
            img_Video.sd_setImageLoadMultiTypeURL(url: "\(YTDBASEURL)\(theModel.videoId)/0.jpg", placeholder: "ic_bg_plash_holder_list")

        }
    }
    
    
    
    
}
