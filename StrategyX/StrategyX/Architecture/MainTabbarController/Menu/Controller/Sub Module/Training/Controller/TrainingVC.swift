//
//  TrainingVC.swift
//  StrategyX
//
//  Created by Jaydeep on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TrainingView = { [unowned self] in
        return self.view as! TrainingView
    }()
    
    fileprivate lazy var theCurrentModel:TrainingModel = {
        return TrainingModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }

    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getTrainingListWebService()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_TrainingList == nil || theCurrentModel.arr_TrainingList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateTableView() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    
    func redirectToTrainingDetailVC(theModel:TrainingList?) {
        let vc = TrainingDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.push(vc: vc)
    }
}

//MARK:-UITableViewDataSource
extension TrainingVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_TrainingList == nil || theCurrentModel.arr_TrainingList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_TrainingList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTagsTVCell") as! SettingsTagsTVCell
        if theCurrentModel.arr_TrainingList != nil {
            cell.configure(theModel: theCurrentModel.arr_TrainingList![indexPath.row])
        } else {
            cell.startAnimation()
        }
        
        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension TrainingVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let modelList = theCurrentModel.arr_TrainingList else { return }
        redirectToTrainingDetailVC(theModel: modelList[indexPath.row])
    }
}

//MARK:- Api Call
extension TrainingVC {
    
    func getTrainingListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            //            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:""] as [String : Any]
        
        TrainingWebServices.shared.getAllTrainingList(parameter: parameter, success: { [weak self] (list, nextOffset) in
                Utility.shared.pushData.trainningVideoCount = ""
                AppDelegate.shared.updateMenuNotificationCount()
                self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
                self?.theCurrentView.refreshControl.endRefreshing()
                self?.theCurrentView.tableView.reloadData()
        }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
}
