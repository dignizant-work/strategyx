//
//  TrainingDetailView.swift
//  StrategyX
//
//  Created by Jaydeep on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingDetailView: ViewParentWithoutXIB {

    //MARK:- Variable
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitleName: UILabel!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout(strTitle:String) {
        //        updateFilterCountText(strCount: "2")
        lblTitleName.text = strTitle
    }
    
    func setupTableView(theDelegate:TrainingDetailVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.registerCellNib(TrainingDetailCell.self)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? TrainingDetailVC)?.getTrainingVideoListWebService(isRefreshing: true)
    }

}
