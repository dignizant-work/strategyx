//
//  TrainingDetailVC.swift
//  StrategyX
//
//  Created by Jaydeep on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingDetailVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TrainingDetailView = { [unowned self] in
        return self.view as! TrainingDetailView
        }()
    
    fileprivate lazy var theCurrentModel:TrainingDetailModel = {
        return TrainingDetailModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(strTitle: theCurrentModel.theTrainingListModel?.title ?? "")
        theCurrentView.setupTableView(theDelegate: self)
        getTrainingVideoListWebService()
    }
    
    func setTheData(theModel:TrainingList?)  {
        theCurrentModel.theTrainingListModel = theModel
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_TrainingVideoList == nil || theCurrentModel.arr_TrainingVideoList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg, textColor: appGreen)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateTableView() {
        theCurrentView.refreshControl.endRefreshing()
        theCurrentView.tableView.reloadData()
    }
    
    @IBAction func onBtnBackAction(_ sender:UIButton) {
        backAction(isAnimation: false)
    }
    
    func redirectToTrainingVideoVC(theModel:TrainingVideoList?) {
        let vc = TrainingVideoVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG, theModel: theModel)
        self.push(vc: vc)
    }

}

//MARK:-UITableViewDataSource
extension TrainingDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_TrainingVideoList == nil || theCurrentModel.arr_TrainingVideoList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_TrainingVideoList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingDetailCell") as! TrainingDetailCell
        if theCurrentModel.arr_TrainingVideoList != nil {
            cell.configure(theModel: theCurrentModel.arr_TrainingVideoList![indexPath.row])
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension TrainingDetailVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let theModels = theCurrentModel.arr_TrainingVideoList else { return }
        redirectToTrainingVideoVC(theModel: theModels[indexPath.row])
    }
}

//MARK:- Api Call
extension TrainingDetailVC {
    
    func getTrainingVideoListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            //            theCurrentModel.nextOffset = 0
        }
        guard let theModel = theCurrentModel.theTrainingListModel else {
            updateTableView()
            return
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:"",APIKey.key_video_section_id:theModel.id] as [String : Any]
        
        TrainingWebServices.shared.getAllTrainingVideoList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.updateTableView()
            }, failed: { [weak self] (error) in
                self?.setBackground(strMsg: error)
                self?.updateTableView()
        })
    }
    
}
