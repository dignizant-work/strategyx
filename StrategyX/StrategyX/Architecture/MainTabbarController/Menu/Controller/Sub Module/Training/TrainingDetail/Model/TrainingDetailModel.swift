//
//  TrainingDetailModel.swift
//  StrategyX
//
//  Created by Jaydeep on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingDetailModel {
    //MARK:- Variable
    fileprivate weak var theController:TrainingDetailVC!
    var theTrainingListModel:TrainingList?
    var arr_TrainingVideoList:[TrainingVideoList]?

    
    
    //MARK:- LifeCycle
    init(theController:TrainingDetailVC) {
        self.theController = theController
    }

    
    func updateList(list:[TrainingVideoList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_TrainingVideoList?.removeAll()
        }
        //        nextOffset = offset
        if arr_TrainingVideoList == nil {
            arr_TrainingVideoList = []
        }
        list.forEach({ arr_TrainingVideoList?.append($0) })
        print(arr_TrainingVideoList!)
    }
}
