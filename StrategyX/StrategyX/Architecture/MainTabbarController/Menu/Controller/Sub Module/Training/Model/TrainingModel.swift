//
//  TrainingModel.swift
//  StrategyX
//
//  Created by Jaydeep on 15/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TrainingModel {
    
    //MARK:- Varibale
    fileprivate weak var theController:TrainingVC!
    var arr_TrainingList:[TrainingList]?
    
    
    //MARK:- LifeCycle
    init(theController:TrainingVC) {
        self.theController = theController
    }
    
    func updateList(list:[TrainingList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_TrainingList?.removeAll()
        }
//        nextOffset = offset
        if arr_TrainingList == nil {
            arr_TrainingList = []
        }
        list.forEach({ arr_TrainingList?.append($0) })
        print(arr_TrainingList!)
    }

}

