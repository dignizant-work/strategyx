//
//  FeatureUpdateCell.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SkeletonView

class NotificationCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    

    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(theModel:Notification){
        lblTitleName.text = theModel.notification
        lblSubTitle.text = theModel.timeName
    }
    
    func hideSkeleton() {
        [lblTitleName,lblSubTitle].forEach({$0?.hideSkeleton()})
    }
    
    func showSkeleton() {
        [lblTitleName,lblSubTitle].forEach({$0.showAnimatedSkeleton()})
    }
    
}
