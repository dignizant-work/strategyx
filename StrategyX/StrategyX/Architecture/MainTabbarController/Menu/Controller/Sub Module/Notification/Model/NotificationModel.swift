//
//  FeatureUpdateModel.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotificationModel {
    
    //MARK:- Variable
    fileprivate weak var theController:NotificationVC!
    var nextOffset:Int = 0
    
    // get category
    
    var arr_NotificationList:[Notification]?
    
    
    func updateList(list:[Notification], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_NotificationList?.removeAll()
        }
        nextOffset = offset
        if arr_NotificationList == nil {
            arr_NotificationList = []
        }
        list.forEach({ arr_NotificationList?.append($0) })
        print(arr_NotificationList!)
    }
    
    //MARK:- LifeCycle
    init(theController:NotificationVC) {
        self.theController = theController
    }
}
