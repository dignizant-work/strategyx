//
//  FeatureUpdateVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotificationVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:NotificationView = { [unowned self] in
        return self.view as! NotificationView
    }()
    fileprivate lazy var theCurrentModel:NotificationModel = {
        return NotificationModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        getNotificationService()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_NotificationList == nil || theCurrentModel.arr_NotificationList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
}

//MARK:- Redirect
extension NotificationVC
{
    
}

//MARK:-UITableViewDataSource
extension NotificationVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_NotificationList == nil || theCurrentModel.arr_NotificationList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_NotificationList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        if theCurrentModel.arr_NotificationList != nil{
            cell.hideSkeleton()
            cell.configure(theModel: self.theCurrentModel.arr_NotificationList![indexPath.row])
        }else{
            cell.showSkeleton()
        }
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
            getNotificationService()
        }
        return cell
    }
}

//MARK:-UITableViewDelegate
extension NotificationVC {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_NotificationList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let arr_model = theCurrentModel.arr_NotificationList, arr_model.count > 0 else { return }
        self.redirectToModule(theModel: arr_model[indexPath.row])
    }
}


//MARK:- call API

extension NotificationVC
{
    func getNotificationService(isRefreshing:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        
        NotificationWebService.shared.getNotificationList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            if self?.theCurrentModel.nextOffset == 0 {
                Utility.shared.pushData.notificationUnread = ""
                AppDelegate.shared.updateMenuNotificationCount()
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
}
