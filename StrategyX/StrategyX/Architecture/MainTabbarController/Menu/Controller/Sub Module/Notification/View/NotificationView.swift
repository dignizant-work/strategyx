//
//  FeatureUpdateView.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotificationView: ViewParentWithoutXIB {
    
    //MARK:- Variabel
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    //MARK:- Life Cycle
    func setupLayout() {
        
    }
    
    func setupTableView(theDelegate:NotificationVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(NotificationCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
        
        //        tableView.contentInset = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: -15)
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? NotificationVC)?.getNotificationService(isRefreshing: true)
    }
}
