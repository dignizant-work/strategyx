//
//  FeedbackListVC.swift
//  StrategyX
//
//  Created by Jaydeep on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackListVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:FeedbackListView = {
        return self.view as! FeedbackListView
    }()
    fileprivate lazy var theCurrentModel:FeedbackListModel = {
        return FeedbackListModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
//        getNotificationService()
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_FeedbackList == nil || theCurrentModel.arr_FeedbackList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func redirectToFeedbackdetailVC(){
        let vc = FeedbackDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.push(vc: vc)
    }

    func redirectToAddFeedbackVC(){
        let vc = FeedbackVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnAddFeedBackAction(_ sender: Any) {
        redirectToAddFeedbackVC()
    }
}

//MARK:-UITableViewDataSource
extension FeedbackListVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if let _ = tableView.backgroundView , (theCurrentModel.arr_NotificationList == nil || theCurrentModel.arr_NotificationList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_NotificationList?.count ?? 10*/
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackListCell") as! FeedbackListCell
        
        cell.imgStatus.backgroundColor = appGreen
        cell.imgStatus.text = "F"
        if indexPath.row%2 == 0{
            cell.imgStatus.backgroundColor = red
            cell.imgStatus.text = "B"
        }
        cell.imgStatus.layer.cornerRadius = cell.imgStatus.frame.height/2
        cell.imgStatus.clipsToBounds = true
        
        if indexPath.row%3 == 0{
            cell.lblStatus.text = "Resolved"
            cell.lblStatus.textColor = appGreen
        } else if indexPath.row%3 == 1 {
            cell.lblStatus.text = "Submitted"
            cell.lblStatus.textColor = appStrategyBlue
        } else if indexPath.row%3 == 2{
            cell.lblStatus.text = "Replied"
            cell.lblStatus.textColor = appStrategyYellow
        }
        /*if theCurrentModel.arr_NotificationList != nil{
            cell.hideSkeleton()
            cell.configure(theModel: self.theCurrentModel.arr_NotificationList![indexPath.row])
        }else{
            cell.showSkeleton()
        }*/
        
        return cell
    }
}

//MARK:-UITableViewDelegate
extension FeedbackListVC {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_FeedbackList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        redirectToFeedbackdetailVC()
    }
}


//MARK:- call API

extension FeedbackListVC
{
    func getFeedbackListService(isRefreshing:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        
        NotificationWebService.shared.getNotificationList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
}

