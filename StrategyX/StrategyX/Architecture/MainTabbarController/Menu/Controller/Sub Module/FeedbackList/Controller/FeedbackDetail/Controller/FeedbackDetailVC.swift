//
//  FeedbackDetailVC.swift
//  StrategyX
//
//  Created by Jaydeep on 28/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackDetailVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:FeedbackDetailView = {
        return self.view as! FeedbackDetailView
    }()
    fileprivate lazy var theCurrentModel:FeedbackDetailModel = {
        return FeedbackDetailModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        //        getNotificationService()
    }
    
   /* func setBackground(strMsg:String) {
        if theCurrentModel.arr_FeedbackList == nil || theCurrentModel.arr_FeedbackList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }*/
    
    @IBAction func onBtnBackAction(_ sender:UIButton){
        backAction(isAnimation: false)
    }
    
    func onBtnLikeAction(){
        redirectToFeedbackVC()
    }

    func onBtnCommentAction(){
        redirectToFeedbackVC()
    }
    
    func redirectToFeedbackVC()  {
        let vc = FeedbackVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        self.push(vc: vc)
    }
}

//MARK:-UITableViewDataSource
extension FeedbackDetailVC:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if let _ = tableView.backgroundView , (theCurrentModel.arr_NotificationList == nil || theCurrentModel.arr_NotificationList?.count == 0) {
         return 0
         }
         tableView.backgroundView = nil
         return theCurrentModel.arr_NotificationList?.count ?? 10*/
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackDetailCell") as! FeedbackDetailCell
            cell.lblFeedbackDetail.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            return cell
        }
        
        let noteCell = tableView.dequeueReusableCell(withIdentifier: "RisksNoteTVCell") as! RisksNoteTVCell
        noteCell.imgProfileLeading.constant = 10
        noteCell.contentView.backgroundColor = .clear
        noteCell.backgroundColor = .clear
        noteCell.selectionStyle = .none
        noteCell.btnMoreOutlet.isHidden = true
        noteCell.lblNorecordfound.isHidden = true
        return noteCell
    }
}

//MARK:-UITableViewDelegate
extension FeedbackDetailVC {
    
    /*func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_FeedbackList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }*/
}

