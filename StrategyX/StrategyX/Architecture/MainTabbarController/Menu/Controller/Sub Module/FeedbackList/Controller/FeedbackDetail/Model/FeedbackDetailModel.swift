//
//  FeedbackDetailModel.swift
//  StrategyX
//
//  Created by Jaydeep on 28/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackDetailModel {

    //MARK:- Variable
    fileprivate weak var theController:FeedbackDetailVC!
    var nextOffset:Int = 0
    
    // get category
    
    var arr_FeedbackList:[Notification]?
    
    
    func updateList(list:[Notification], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_FeedbackList?.removeAll()
        }
        nextOffset = offset
        if arr_FeedbackList == nil {
            arr_FeedbackList = []
        }
        list.forEach({ arr_FeedbackList?.append($0) })
        //        print(arr_NotificationList!)
    }
    
    //MARK:- LifeCycle
    init(theController:FeedbackDetailVC) {
        self.theController = theController
    }
}
