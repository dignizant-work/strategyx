//
//  FeedbackDetailView.swift
//  StrategyX
//
//  Created by Jaydeep on 28/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackDetailView: ViewParentWithoutXIB {

    //MARK:- Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
  
    @IBOutlet weak var tableView: UITableView!
    
      //MARK:- Variabel
    
//    let refreshControl = UIRefreshControl()
    
    //MARK:- Life Cycle
    func setupLayout() {
        
    }
    
    func setupTableView(theDelegate:FeedbackDetailVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(RisksNoteTVCell.self)
        tableView.registerCellNib(FeedbackDetailCell.self)
        tableView.registerCellNib(BottomButtonTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
    }
    
    @objc override func refresh() {
//        refreshControl.beginRefreshing()

        //        (self.parentContainerViewController() as? NotificationVC)?.getNotificationService(isRefreshing: true)
    }
}
