//
//  FeedbackListView.swift
//  StrategyX
//
//  Created by Jaydeep on 27/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackListView: ViewParentWithoutXIB {

    //MARK:- Variabel
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    //MARK:- Life Cycle
    func setupLayout() {
        
    }
    
    func setupTableView(theDelegate:FeedbackListVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(FeedbackListCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
//        (self.parentContainerViewController() as? NotificationVC)?.getNotificationService(isRefreshing: true)
    }

}
