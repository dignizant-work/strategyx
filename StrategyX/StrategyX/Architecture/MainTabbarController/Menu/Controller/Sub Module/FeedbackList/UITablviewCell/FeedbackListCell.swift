//
//  FeedbackListCell.swift
//  StrategyX
//
//  Created by Jaydeep on 28/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class FeedbackListCell: UITableViewCell {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblFeedbackTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgStatus: UILabel!
    
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
