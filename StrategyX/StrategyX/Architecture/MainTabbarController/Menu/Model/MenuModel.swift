//
//  MenuModel.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MenuModel {
    //Variable
    weak var theController:MenuVC!
    var isSelectedIndex = 1
    
//    var arr_RightMenuTitle:[String] = ["Settings","Notifications","Feedback","Feature Updates","Resources","Training"]
    var arr_RightMenuTitle:[String] = ["Settings","Notifications","Satisfaction","Whats New","Training","Resources","Time Log"]
    var arr_RightMenuIcon:[String] = ["ic_unselect_setting_butten_menu_screen","ic_unselect_notification_menu_screen","ic_unselect_feedback_menu_screen","ic_unselect_feature_update_menu_screen","ic_training","ic_recsources_profile_screen","ic_time_log_menu_screen"]
    var arr_RightMenuSelectedIcon:[String] = ["ic_select_setting_butten_menu_screen","ic_unselect_notification_menu_screen","ic_select_feedback_menu_screen","ic_select_feature_update_menu_screen","ic_training","ic_recsources_profile_screen","ic_time_log_menu_screen"]
    
//    ic_select_notification_menu_screen
   
    
    //LifeCycle
    init(theController:MenuVC) {
        self.theController = theController
        arr_RightMenuTitle.remove(at: arr_RightMenuTitle.count - 2)
        arr_RightMenuIcon.remove(at: arr_RightMenuIcon.count - 2)
        arr_RightMenuSelectedIcon.remove(at: arr_RightMenuSelectedIcon.count - 2)
        
        arr_RightMenuTitle.removeLast()
        arr_RightMenuIcon.removeLast()
        arr_RightMenuSelectedIcon.removeLast()
        
        if UserDefault.shared.userRole == .staffLite {
            arr_RightMenuTitle.removeLast()
            arr_RightMenuIcon.removeLast()
            arr_RightMenuSelectedIcon.removeLast()
        }
        
    }
    
    //    MARK:- ViewModel
    
    func getTableviewList() -> ([String],[String])  {
        // icon, title
        
        return (arr_RightMenuIcon,arr_RightMenuTitle)
    }
}
