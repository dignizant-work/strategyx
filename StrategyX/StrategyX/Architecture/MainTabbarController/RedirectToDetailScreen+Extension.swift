//
//  RedirectToDetailScreen+Extension.swift
//  StrategyX
//
//  Created by Haresh on 02/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

//MARK:- Redirection
extension ParentViewController {
    
    func redirectToModule(theModel:Notification) {
        guard !theModel.relatedId.isEmpty else {
            return
        }
        switch theModel.relatedTo {
        case RedirectToModule.actionLog.rawValue:
            self.extGetActionDetailWebService(strActionLogID: theModel.relatedId)
            break
        case RedirectToModule.primaryArea.rawValue:
            self.extGetPrimaryAreaDetailWebService(strPrimaryID: theModel.relatedId)
            break
        case RedirectToModule.secondaryArea.rawValue:
            self.extGetSecondaryAreaDetailWebService(strSecondaryID: theModel.relatedId)
            break
        case RedirectToModule.strategyGoal.rawValue:
            self.extGetStrategyGoalDetailWebService(strStrategyGoalID: theModel.relatedId)
            break
        case RedirectToModule.risk.rawValue:
            self.extGetRiskDetailView(risk_id: theModel.relatedId)
            break
        case RedirectToModule.tacticalProject.rawValue:
            self.extGetTacticalProjectDetailWebService(tacticalProjectsId: theModel.relatedId)
            break
        case RedirectToModule.focuses.rawValue:
            self.extGetFocusDetailWebService(strFocusID: theModel.relatedId)
            break
        case RedirectToModule.myRole.rawValue:
            self.extGetMyRoleListWebService(strRoleID: theModel.relatedId)
            break
        case RedirectToModule.successFactor.rawValue:
            self.extGetSuccessFactorDetailWebService(strSuccessFactorID: theModel.relatedId)
            break
        case RedirectToModule.ideaNproblem.rawValue:
            self.extGetIdeaAndProblemListWebService(strIdeaAndProblemID: theModel.relatedId)
            break
        case RedirectToModule.whatsNew.rawValue:
            self.extGetFeatureListWebService(whatsNewID: theModel.relatedId)
            break
        case RedirectToModule.trainingVideo.rawValue:
            self.extGetTrainingVideoListWebService(strVideoID: theModel.relatedId)
            break
        case RedirectToModule.softwareUpdate.rawValue:
            self.extRedirectToSoftwareUpdateScreen()
            break
        default:
            break
        }
    }
    
    func extRedirectToActionEditVC(theModel:ActionDetail) {
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
//        vc.handlerEditAction = { [weak self] (theActionListModel) in
//            self?.updateTheRiskModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionListModel)
//        }
        self.push(vc: vc)
    }
    func extRedirectToAddPrimaryAreaScreen(theModel:PrimaryArea?) {
        let vc = AddPrimaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        /*vc.handlerEditData = { [weak self] (theModelPrimary) in
            //            let model = theModel
            //            model.title = strTitle
            //            model.descriptionArea = strDiscription
            if let obj = self {
                theModelPrimary.notesCount = 0
                theModelPrimary.completeActionCount = theOldModel.completeActionCount
                theModelPrimary.completeActionFlag = theOldModel.completeActionFlag
                theModelPrimary.actionCount = theOldModel.actionCount
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updatePrimaryAreaData(theModel: theModelPrimary)
            }
        }*/
        self.push(vc: vc)
    }
    func extRedirectToAddSecondaryAreaScreen(theModel:SecondaryArea? = nil) {
        let vc = AddSecondaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)

        /*vc.handlerEditData = { [weak self] (theModelSecondary) in
            //            let model = theModel!
            //            model.title = strTitle
            //            model.descriptionArea = strDiscription
            
            if let obj = self {
                theModelSecondary.notesCount = 0
                theModelSecondary.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelSecondary.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelSecondary.actionCount = theOldModel?.actionCount ?? 0
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updateSecondaryAreaData(theModel: theModelSecondary)
            }
        }*/
        self.push(vc: vc)
    }
    func extRedirectToAddGoalScreen(theModel:StrategyGoalList?) {
        let vc = AddGoalVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        /*vc.handlerEditData = { [weak self] (theModelGoal) in
            //            let model = theModel!
            //            model.title = strTitle
            //            model.descriptionArea = strDiscription
            
            if let obj = self {
                theModelGoal.notesCount = 0
                theModelGoal.completeActionCount = theOldModel?.completeActionCount ?? 0
                theModelGoal.completeActionFlag = theOldModel?.completeActionFlag ?? 0
                theModelGoal.actionCount = theOldModel?.actionCount ?? 0
                
                obj.theCurrentModel.subViewControllers[obj.theCurrentModel.currentIndexOfSubViewController].updateStrategyGoalData(theModel: theModelGoal)
            }
        }*/
        self.push(vc: vc)
    }
    func extRedirectToAddRiskVC(theModel:RiskDetail) {
        let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        /*vc.handlerUpdateModel = { [weak self] (theDetailModel) in
         //            self?.theCurrentModel.isEditRisk = true
         if let theModel = theDetailModel {
         self?.updateRiskListModel(theRiskDetailModel: theModel)
         }
         //            self?.getRiskDetailView(risk_id: theModel.risk_id)
         }*/
        self.push(vc: vc)
    }
    func extRedirectToAddTacticalProjectVC(theTacticalDetailModel:TacticalProjectDetail?) {
        let vc = AddTacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setUpdateScreen(editType: .edit, tacticalProjectDetail: theTacticalDetailModel)
        /*vc.handlerUpdateModel = { [weak self] (theDetailModel) in
            self?.updateTacticalProjectListModel(theTacticalProjectDetailModel:theDetailModel)
        }*/
        self.push(vc: vc)
    }
    func extRedirectToAddFocusVC(theModel:FocusDetail) {
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
               /* vc.handlerUpdateModel = { [weak self] (theFocusDetailModel) in
                    self?.updateFocusModelAndUI(theModel: theFocusDetailModel)
                }*/
        self.push(vc: vc)
    }
    func extRedirectToMyRoleCalenderScreen(theModel:MyRole?) {
        let imgBG = self.view.asImage()
        let vc = MyRoleCalendarVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG, isForEdit: true, theModel: theModel)
        /*vc.handlerClose = {
            
        }
        vc.handlerEditData = { [weak self] (theModel) in
            self?.theCurrentModel.theMyRoleModel = theModel
            self?.updateViewAndModel()
        }*/
        self.push(vc: vc)
    }
    
    func extRedirectToIdeasNProblemsDetailsVC(theModel:IdeaAndProblemList) {
        let vc = IdeaNProblemDetailVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        vc.setTheData(theModel: theModel, isForArchive: false)
        self.push(vc: vc)
    }
    
    func extRedirectToFeatureUpdateDetailScreen(theModel:FeaturesUpdate) {
        let vc = FeatureUpdateDetailVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        vc.setTheData(theModel: theModel)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func extRedirectToTrainingVideoVC(theModel:TrainingVideoList?) {
        let vc = TrainingVideoVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG, theModel: theModel)
        self.push(vc: vc)
    }
    
    func extRedirectToSoftwareUpdateScreen() {
        let vc = SoftwareUpdateVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        self.push(vc: vc)
    }

    func extRedirectToAddCriticalSucessFactorVC(theModel:SuccessFactorDetail) {
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
//        vc.handlerUpdateModel = { [weak self] (theSuccessFactorDetailModel) in
//            self?.updateSuccessFactorModelAndUI(theModel: theSuccessFactorDetailModel)
//        }
        self.push(vc: vc)
    }
    
    func extRedirectToSucessFactorDetailVC(theModel:SuccessFactorDetail) {
        
        //        guard let theModel = theCurrentModel.theSuccessFactorDetailModel else { return }
        let vc = SuccessFactorDetailVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        let imgBG = self.view.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
//        vc.handlerUpdateModel = { [weak self] (theSuccessFactorDetailModel) in
//            self?.updateSuccessFactorModelAndUI(theModel: theSuccessFactorDetailModel)
//        }
        self.push(vc: vc)
    }
}

//MARK:- Detail Webservice
extension ParentViewController {
    func extGetActionDetailWebService(strActionLogID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            self?.extRedirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetPrimaryAreaDetailWebService(strPrimaryID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_primary_area_id:strPrimaryID] as [String : Any]
        StrategyWebService.shared.getPrimaryArea(parameter: parameter, success: { [weak self] (theDetailModel) in
            Utility.shared.stopActivityIndicator()
                self?.extRedirectToAddPrimaryAreaScreen(theModel: theDetailModel)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetSecondaryAreaDetailWebService(strSecondaryID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_secondary_area_id:strSecondaryID] as [String : Any]
        StrategyWebService.shared.getSecondaryArea(parameter: parameter, success: { [weak self] (theDetailModel) in
            Utility.shared.stopActivityIndicator()
                self?.extRedirectToAddSecondaryAreaScreen()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetStrategyGoalDetailWebService(strStrategyGoalID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_strategy_goal_id:strStrategyGoalID] as [String : Any]
        StrategyWebService.shared.getStrategyGoal(parameter: parameter, success: { [weak self] (theDetailModel) in
            Utility.shared.stopActivityIndicator()
                self?.extRedirectToAddGoalScreen(theModel: theDetailModel)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetRiskDetailView(risk_id:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_risk_id: risk_id,
                         ]
        
        RiskWebservice.shared.getRiskDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            self?.extRedirectToAddRiskVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetTacticalProjectDetailWebService(tacticalProjectsId:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_tactical_project_id:tacticalProjectsId] as [String : Any]
        
        TacticalProjectWebService.shared.getTacticalProjectDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            self?.extRedirectToAddTacticalProjectVC(theTacticalDetailModel: detail)
            
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetFocusDetailWebService(strFocusID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_focus_id:strFocusID] as [String : Any]
        
        FocusWebServices.shared.getFocusDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
            self?.extRedirectToAddFocusVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetMyRoleListWebService(strRoleID:String) {
        Utility.shared.showActivityIndicator()

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:"0",APIKey.key_role_id:strRoleID] as [String : Any]
        MyRoleWebServices.shared.getAllMyRoleList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            Utility.shared.stopActivityIndicator()
            if let theModel = list.first {
                self?.extRedirectToMyRoleCalenderScreen(theModel: theModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
    func extGetIdeaAndProblemListWebService(strIdeaAndProblemID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:"0",APIKey.key_idea_and_problem_id:strIdeaAndProblemID] as [String : Any]
        
        IdeaAndProblemsWebServices.shared.getIdeaAndProblemList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            Utility.shared.stopActivityIndicator()
            if let theModel = list.first {
                self?.extRedirectToIdeasNProblemsDetailsVC(theModel: theModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetFeatureListWebService(whatsNewID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:"0",APIKey.key_feature_update_id:whatsNewID] as [String : Any]
        SettingsWebService.shared.getAllFeatureList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            Utility.shared.stopActivityIndicator()
            if let theModel = list.first {
                self?.extRedirectToFeatureUpdateDetailScreen(theModel: theModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
    func extGetTrainingVideoListWebService(strVideoID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:"0",APIKey.key_video_id:strVideoID] as [String : Any]
        TrainingWebServices.shared.getAllTrainingVideoList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            Utility.shared.stopActivityIndicator()
            if let theModel = list.first {
                self?.extRedirectToTrainingVideoVC(theModel: theModel)
            }
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func extGetSuccessFactorDetailWebService(strSuccessFactorID:String) {
        
        Utility.shared.showActivityIndicator()
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_success_factor_id:strSuccessFactorID] as [String : Any]
        
        SuccessFactorWebservice.shared.getSuccessFactorDetail(parameter: parameter, success: { [weak self] (detail) in
            //                alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.extRedirectToSucessFactorDetailVC(theModel: detail)
           
            }, failed: { [weak self] (error) in
                //                alert.dismiss(animated: false, completion: nil)
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
    }
}
