//
//  ResourcesVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class ResourcesVC: ParentViewController {
    
    //MARK:- Variable
    enum categorySelectionType:Int {
        case company             = 1
        case department          = 2
    }
    fileprivate lazy var theCurrentView:ResourcesView = { [unowned self] in
        return self.view as! ResourcesView
        }()
    
    fileprivate lazy var theCurrentModel:ResourcesModel = {
        return ResourcesModel(theController: self)
    }()    
   
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.setupChartview(chartView: theCurrentView.HorizotalBarChart)
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
    }

    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            self.theCurrentModel.selectedData["company"].stringValue = str
            theCurrentView.txtCompany.text = str
            break
        case .department:
            self.theCurrentModel.selectedData["department"].stringValue = str
            theCurrentView.txtDepartment.text = str
            break
        }
    }
    
    //MARK:- Redirection
    func presentDropDownListVC(categorySelection:categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            vc.setTheData(strTitle: "Company",arr:theCurrentModel.arr_Company)
            break
        case .department:
            vc.setTheData(strTitle: "Depatment",arr:theCurrentModel.arr_Department)
            break
        
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }

}

//MARK:- Set drop down

extension ResourcesVC {
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.departmentDD:
            theCurrentView.txtDepartment.text = text
            break
        case theCurrentModel.companyDD:
            theCurrentView.txtCompany.text = text
            break
       
        default:
            break
        }        
    }
}

//MARK:- Action Zone

extension ResourcesVC {
    @IBAction func btnCompanyAction(_ sender:UIButton)
    {
        self.view.endEditing(true)
//        theCurrentModel.companyDD.show()
        presentDropDownListVC(categorySelection: ResourcesVC.categorySelectionType.company)
    }
    
    @IBAction func btnDepartmentAction(_ sender:UIButton)
    {
        self.view.endEditing(true)
//        theCurrentModel.departmentDD.show()
        presentDropDownListVC(categorySelection: ResourcesVC.categorySelectionType.department)
    }
    
    @IBAction func btnDailyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
    
    @IBAction func btnWeeklyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
        
    }
    
    @IBAction func btnMonthlyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
    
    @IBAction func btnCustomDateAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
        
        let vc = ResourceDetailVC.instantiateFromAppStoryboard(appStoryboard: .resources)
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

class NavigationResourcesVC: UINavigationController {
    
}


