//
//  ResourceDetailVCViewController.swift
//  StrategyX
//
//  Created by Jaydeep on 02/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import Charts

class ResourceDetailVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ResourceDetailView = { [unowned self] in
        return self.view as! ResourceDetailView
        }()
    
    fileprivate lazy var theCurrentModel:ResorceDetailModel = {
        return ResorceDetailModel(theController: self)
    }()
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.setupChartview(chartView: theCurrentView.HorizotalBarChart)
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
    }
    
}

//MARK:- Action Zone

extension ResourceDetailVC
{
    @IBAction func btnBackAction(_ sender:UIButton)
    {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func btnDailyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
    
    @IBAction func btnWeeklyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
    
    @IBAction func btnMonthlyAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
    
    @IBAction func btnCustomDateAction(_ sender:UIButton)
    {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
    }
}
