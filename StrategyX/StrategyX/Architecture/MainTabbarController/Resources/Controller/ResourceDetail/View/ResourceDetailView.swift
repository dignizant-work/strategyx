//
//  ResourceDetailView.swift
//  StrategyX
//
//  Created by Jaydeep on 02/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts

class ResourceDetailView: ViewParentWithoutXIB {

    //MARK:- Outlet Zone
    @IBOutlet weak var btnDailyOutlet: UIButton!
    @IBOutlet weak var btnMontlyOutlet: UIButton!
    @IBOutlet weak var btnWeeklyOutlet: UIButton!
    @IBOutlet weak var btnCustomDateOutlet: UIButton!
    @IBOutlet weak var HorizotalBarChart: HorizontalBarChartView!
    
    //MARK:- Setup View
    
    func setupChartview(chartView:HorizontalBarChartView)
    {
        //chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.legend.enabled = false
        
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.axisLineColor = appSepratorColor
        //xAxis.yOffset = 20
        // xAxis.axisMinimum = 0
        // xAxis.centerAxisLabelsEnabled = true
        
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = false
        yAxis.labelTextColor = appPlaceHolder
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        yAxis.xOffset = 20
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        
        //chartView.fitBars = false
        
        self.setDataCount()
    }
    
    func setDataCount() {
        
        var yVals = [BarChartDataEntry]()
        yVals.append(BarChartDataEntry(x: Double(0), yValues: [0,4,5,11], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(1), yValues: [0,5,5,10], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(2), yValues: [0,7,9,4], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(3), yValues: [0,2,3,15], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,4,14,2], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,8,8,4], icon: UIImage(named: "icon")))
        
        
        var set1: BarChartDataSet! = nil
        if let set = HorizotalBarChart.data?.dataSets.first as? BarChartDataSet {
            set1 = set
            set1.values = yVals
            set1.highlightEnabled = false
            
        } else {
            let greenColor = UIColor(red: 0/255, green: 200/255, blue: 132/255, alpha: 1)
            let redColor = UIColor(red: 255/255, green: 106/255, blue: 93/255, alpha: 1)
            set1 = BarChartDataSet(values: yVals, label: "")
            set1.colors = [greenColor,UIColor.clear,redColor,UIColor.clear]
            set1.drawValuesEnabled = false
            set1.highlightEnabled = false
        }
        
        var strName = [String]()
        strName.append("Brett")
        strName.append("John")
        strName.append("Sam")
        strName.append("Robert")
        strName.append("Sandra")
        strName.append("bandra")
        
        print("Array Name \(strName)")
        
        HorizotalBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: strName)
       
        
        let data = BarChartData(dataSet: set1)
        data.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
        data.barWidth = 0.6        
        HorizotalBarChart.data = data
        
    }
    
    func resetColor()
    {
        [btnDailyOutlet,btnMontlyOutlet,btnWeeklyOutlet,btnCustomDateOutlet].forEach { (btn) in
            btn?.layer.borderColor = appTextFieldBorder.cgColor
            btn?.setTitleColor(appPlaceHolder, for: .normal)
        }
    }
    
    func selectedState(_ sender:UIButton)
    {
        sender.layer.borderColor = appGreen.cgColor
        sender.setTitleColor(appGreen, for: .normal)
    }
}
