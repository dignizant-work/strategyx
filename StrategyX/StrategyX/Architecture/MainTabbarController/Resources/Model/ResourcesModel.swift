//
//  ResourcesModel.swift
//  StrategyX
//
//  Created by Jaydeep on 01/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

import SwiftyJSON

class ResourcesModel {
    
    //MARK:- Variable
    weak var theController:ResourcesVC!
    let departmentDD = DropDown()
    let companyDD = DropDown()
    
    var arr_Company:[String] = ["Company 1","Company 2","Company 3","Company 4"]
    var arr_Department:[String] = ["Department 1","Department 2","Department 3","Department 4"]
    var selectedData:JSON = JSON(["company":"","department":""])
    
    //MARK:- LifeCycle
    init(theController:ResourcesVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case departmentDD:
            dropDown.dataSource = ["Department1","Department2","Department3"]
            break
        case companyDD:
            dropDown.dataSource = ["Company1","Company2","Company3"]
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
}
