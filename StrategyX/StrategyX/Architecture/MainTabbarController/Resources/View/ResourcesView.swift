//
//  ResourcesView.swift
//  StrategyX
//
//  Created by Jaydeep on 01/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import Charts

class ResourcesView: ViewParentWithoutXIB {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var viewDepartment: UIView!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var HorizotalBarChart: HorizontalBarChartView!
    @IBOutlet weak var btnDailyOutlet: UIButton!
    @IBOutlet weak var btnMontlyOutlet: UIButton!
    @IBOutlet weak var btnWeeklyOutlet: UIButton!
    @IBOutlet weak var btnCustomDateOutlet: UIButton!
    
    //MARK:- Setup View
    func setupLayout() {
        if UserDefault.shared.userRole != UserRole.superAdminUser {
            viewCompany.isHidden = true
        }
    }
    func setupChartview(chartView:HorizontalBarChartView) {
        //chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        chartView.drawRoundedBarEnabled = true
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.legend.enabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.axisLineColor = appSepratorColor
        //xAxis.xOffset = 5
        
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = false
        yAxis.labelTextColor = appPlaceHolder
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        //yAxis.xOffset = 20
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        
        //chartView.fitBars = false
        
        self.setDataCount()
    }
    
    func setDataCount() {
        
        var yVals = [BarChartDataEntry]()
        yVals.append(BarChartDataEntry(x: Double(0), yValues: [0,4,5,11], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(1), yValues: [0,5,5,10], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(2), yValues: [0,7,9,4], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(3), yValues: [0,2,3,15], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,4,14,2], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,8,8,4], icon: UIImage(named: "icon")))
        
        
        var set1: BarChartDataSet! = nil
        if let set = HorizotalBarChart.data?.dataSets.first as? BarChartDataSet {
            set1 = set
            set1.values = yVals
           set1.highlightEnabled = false
            
        } else {
            
            set1 = BarChartDataSet(values: yVals, label: "")
            set1.barRoundingCorners = [.allCorners]
            set1.colors = [appGreen,UIColor.clear,appPink,UIColor.clear]
            set1.drawValuesEnabled = false
            set1.highlightEnabled = false
        }
        
        var strName = [String]()
        strName.append("Brett")
        strName.append("John")
        strName.append("Sam")
        strName.append("Robert")
        strName.append("Sandra")
        strName.append("bandra")
        
        print("Array Name \(strName)")        
        
        HorizotalBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: strName)
        
        let data = BarChartData(dataSet: set1)
        data.setValueFont(R10)
        data.barWidth = 0.6
        HorizotalBarChart.data = data
        
    }
    
    func resetColor()
    {
        [btnDailyOutlet,btnMontlyOutlet,btnWeeklyOutlet,btnCustomDateOutlet].forEach { (btn) in
            btn?.layer.borderColor = appTextFieldBorder.cgColor
            btn?.setTitleColor(appPlaceHolder, for: .normal)
        }
    }
    
    func selectedState(_ sender:UIButton)
    {
        sender.layer.borderColor = appGreen.cgColor
        sender.setTitleColor(appGreen, for: .normal)
    }

    

}
