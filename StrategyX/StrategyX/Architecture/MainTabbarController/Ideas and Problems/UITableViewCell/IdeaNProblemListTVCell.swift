//
//  IdeaNProblemListTVCell.swift
//  StrategyX
//
//  Created by Haresh on 04/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeaNProblemListTVCell: UITableViewCell {
    @IBOutlet weak var viewType: UIView!

    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30

    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var widthBtnMore: NSLayoutConstraint!
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!
    @IBOutlet weak var constrainViewNotesWidth: NSLayoutConstraint! // 22.0
    @IBOutlet weak var constrainViewNotesLeading: NSLayoutConstraint! // 5.0
    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var lblDueDate: UILabel!

    var handlerMoreAction:() -> Void = {}
    var handlerActions:() -> Void = {}
    var handleDueDateAction:() -> Void = {}
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [img_Profile,lblTitle,lblSubTitle,lblDueDate,lblTotalCount,viewNotesCount,viewType].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
        lblSubTitle.text = " "
        lblDueDate.text = " "
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateBackGroundColor(isCompleted:Bool = false) {
        viewBg.backgroundColor = isCompleted ? appGreen.withAlphaComponent(0.19) : nil
    }
    func hideAnimation() {
        [img_Profile,lblTitle,lblSubTitle,lblDueDate,lblTotalCount,viewNotesCount,viewType].forEach({ $0?.hideSkeleton() })
        btnMore.isHidden = false
    }
    
    func startAnimation() {
        viewType.setBackGroundColor = 1
        img_Profile.isHidden = false
        [img_Profile,lblTitle,lblSubTitle,lblDueDate,lblTotalCount,viewNotesCount,viewType].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
        lblSubTitle.text = " "
        lblDueDate.text = " "
        btnMore.isHidden = true
    }
    
    func configure(strTitle:String,strSubTitle:String) {
        lblTitle.text = strTitle
        lblSubTitle.text = strSubTitle
    }
    
    func configureIdeaAndProblem(theModel:IdeaAndProblemList,isApiLoading:Bool = false) {
        hideAnimation()
        btnMore.loadingIndicator(isApiLoading)
        self.contentView.isUserInteractionEnabled = !isApiLoading
        //        lblProfile.isHidden = true
        //        img_Profile.image = UIImage(named: theModel.type.lowercased() == IdeaAndProblemType.idea.rawValue.lowercased() ? "ic_Idea_icon" : "ic_problem_icon")
        //        img_Profile.contentMode = .scaleToFill
        lblProfile.text = theModel.assignedTo.acronym()
        lblSubTitle.text = ""
        lblDueDate.text = ""

        if theModel.assignedToUserImage.isEmpty && theModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
    
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID: theModel.assigneeId) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
        
        lblNotesCount.text = "\(theModel.notesCount)"
        viewNotesCount.setBackGroundColor = theModel.notesCount > 0 ? 9 : 3
        
        //        img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.assignedToUserImage, placeholder: "ic_mini_plash_holder")
        lblTitle.text = theModel.title
        lblTotalCount.text = "\(String(describing: theModel.complateAction))/\(String(describing: theModel.totalAction))"
        lblSubTitle.text = ""
        lblSubTitle.attributedText = nil
        btnDueDate.isUserInteractionEnabled = false
        
        //        ["submitted","accepted","rejected","on_hold","completed"]
        //        ["submitted","fixing","on_hold","fixed"]
        
        var statusForColor = appPlaceHolder
        switch theModel.status.lowercased() {
        case "submitted":
            statusForColor = appStrategyBlue
            break
        case "fixing","accepted":
            statusForColor = appGreen
            break
        case "on_hold":
            statusForColor = orange
            break
        case "completed","fixed":
            statusForColor = appPlaceHolder
            break
        case "rejected":
            statusForColor = red
            break
        default:
            break
        }
        
        lblDueDate.attributedText = nil
        lblSubTitle.attributedText = nil
        
        var status = theModel.status.capitalizedFirst()
        if theModel.status.lowercased() == "on_hold" {
            status = "On Hold"
        }
        lblSubTitle.text = status
        viewType.backgroundColor = statusForColor
        lblSubTitle.rotate(degrees: 270)
        lblSubTitle.frame = CGRect(x:0, y:0, width:15, height:viewType.frame.size.height)
        
//        let attributeStringWork =  NSAttributedString(string: status + " ", attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:statusForColor])
//        lblSubTitle.attributedText = attributeStringWork
        
        if let dueDate = theModel.duedate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType10),!dueDate.isEmpty {
            btnDueDate.isUserInteractionEnabled = true
            //            cell.configureCell6(strItemTitle: "Stage", strSelectedName: theCurrentViewModel.selectedData["stage"].stringValue, strDueDate: strDueDate, strDueDateColor: theCurrentViewModel.theIdeaAndProblemModel?.duedateColor ?? "")
            
//            let attributeString = NSMutableAttributedString()
//            let attributeStringWork =  NSAttributedString(string: theModel.status.capitalizedFirst() + " ", attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:statusForColor])
//            attributeString.append(attributeStringWork)
            
            let attributeStringDue =  NSAttributedString(string: dueDate, attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theModel.duedateColor)])
//            attributeString.append(attributeStringDue)
            
            lblDueDate.attributedText = attributeStringDue
        } else {
            
        }
        
        /*(lblSubTitle as? LabelButton)?.onClick = { [weak self] in
         if isDateAvailabel {
         self?.handleDueDateAction()
         }
         }*/
        
    }
    
    func hideMoreButton(){
        widthBtnMore.constant = 0
        btnMore.isHidden = true
    }
    
    func hideViewNotes() {
        viewNotesCount.isHidden = true
        constrainViewNotesWidth.constant = 0
        constrainViewNotesLeading.constant = 0
    }
    
    //MARK:- IBAction
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
    
    @IBAction func onBtnActions(_ sender: Any) {
        handlerActions()
    }
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        handleDueDateAction()
    }
    
}
