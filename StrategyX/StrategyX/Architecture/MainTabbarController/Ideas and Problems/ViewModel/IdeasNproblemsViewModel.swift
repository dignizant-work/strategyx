//
//  IdeasNproblemsViewModel.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class IdeasNproblemsViewModel {
    
    //MARK:-Variable
    fileprivate weak var theController:IdeasNproblemsVC!
    enum titleSelectionType:String {
        case ideaNProblems          = "Ideas and Problems"
        case ideas                  = "Ideas"
        case problems               = "Problems"
    }
    var filterData:FilterCreticalFectorsData = FilterCreticalFectorsData()
    var assignToUserFlag = -1

    var titleTypeDD = DropDown()
    var arr_TitleType = [IdeasNproblemsViewModel.titleSelectionType.ideaNProblems.rawValue,IdeasNproblemsViewModel.titleSelectionType.ideas.rawValue,IdeasNproblemsViewModel.titleSelectionType.problems.rawValue]
    var selectedTitleType = IdeasNproblemsViewModel.titleSelectionType.ideaNProblems

    var arr_IdeaNProblemList:[IdeaAndProblemList]?
    var nextOffset = 0

    var apiLoadingAtIdeaNProblemsID:[String] = []
    let moreDD = DropDown()

    //MARK:- LifeCycle
    init(theController:IdeasNproblemsVC) {
        self.theController = theController
    }
    
    func updateList(list:[IdeaAndProblemList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_IdeaNProblemList?.removeAll()
        }
        nextOffset = offset
        if arr_IdeaNProblemList == nil {
            arr_IdeaNProblemList = []
        }
        list.forEach({ arr_IdeaNProblemList?.append($0) })
        print(arr_IdeaNProblemList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:IdeaAndProblemList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.action.rawValue,MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue, MoreAction.archived.rawValue]
        var dataSourceIcon = ["ic_plus_icon_popup","ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup", "ic_achive_icon"]
       
        if theModel.assigneeId.isEmpty || theModel.assigneeId == "0" || theModel.duedate.isEmpty {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.action.rawValue}) {
                dataSource.remove(at: index) // action
                dataSourceIcon.remove(at: index)
            }
        }
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById)  {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        if !UserDefault.shared.isCanArchiveForm(strOppoisteID: theModel.createdById, completionFlag: theModel.completeActionFlag) {
            dataSource.removeLast() // archive
            dataSourceIcon.removeLast()
        }
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
    
    func configure(dropDown:DropDown,view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 35)
        
        switch dropDown {
        case titleTypeDD:
            dropDown.dataSource = arr_TitleType
            break
        default:
            break
        }
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.update(text: item, dropDown: dropDown)
        }
        dropDown.cancelAction = { [unowned self] in
            self.rotateDropdownArrow()
        }
    }
    func update(text:String,dropDown:DropDown) {
        rotateDropdownArrow()
        
        switch dropDown {
        case titleTypeDD:
            switch text.localizedLowercase {
            case titleSelectionType.ideaNProblems.rawValue.localizedLowercase:
                selectedTitleType = .ideaNProblems
                break
            case titleSelectionType.ideas.rawValue.localizedLowercase:
                selectedTitleType = .ideas
                break
            case titleSelectionType.problems.rawValue.localizedLowercase:
                selectedTitleType = .problems
                break
            default:
                break
            }
            (theController.view as? IdeasNproblemsView)?.lblDropDownTitle.text = text
            break
        default:
            break
        }
        theController.refreshApiCall()
    }
    func rotateDropdownArrow(at rotate:CGFloat = 0) {
        (theController.view as? IdeasNproblemsView)?.img_DropDownArrow.rotateArrow(at:rotate)
    }
    
    func setBackground(strMsg:String) {
        if arr_IdeaNProblemList == nil || arr_IdeaNProblemList?.count == 0 {
            (theController.view as? IdeasNproblemsView)?.tableView.backgroundView = (theController.view as? IdeasNproblemsView)?.tableView.backGroundMessageView(strMsg: strMsg)
            (theController.view as? IdeasNproblemsView)?.tableView.reloadData()
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = arr_IdeaNProblemList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            archiveIdeaAndProblem(strIdeaAndProblemID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteIdeaAndProblem(strIdeaAndProblemID: model.id)
            return
        case MoreAction.edit.rawValue:
            theController.redirectToIdeasNProblemsDetailsVC(theModel: model)
            /*
            if model.type.lowercased() == IdeaAndProblemType.idea.rawValue.lowercased() {
                theController.redirectToAddIdeaVC(theModel: model)
            } else {
                theController.redirectToAddProblemVC(theModel: model)
            }*/
            break
        case MoreAction.chart.rawValue:
            theController.presentGraphAction(strChartId: model.id)
            break
        case MoreAction.action.rawValue:
            theController.redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    
    func deleteIdeaAndProblem(strIdeaAndProblemID: String) {
        theController.showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.apiLoadingAtIdeaNProblemsID.append(strIdeaAndProblemID)
            self?.updateBottomCell(loadingID: strIdeaAndProblemID, isLoadingApi: true)
            self?.changeIdeaAndProblemStatusWebService(strIdeaAndProblemID: strIdeaAndProblemID, isDelete: true)
        }
    }
    func archiveIdeaAndProblem(strIdeaAndProblemID: String) {
        theController.showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.apiLoadingAtIdeaNProblemsID.append(strIdeaAndProblemID)
            self?.updateBottomCell(loadingID: strIdeaAndProblemID, isLoadingApi: true)
            self?.changeIdeaAndProblemStatusWebService(strIdeaAndProblemID: strIdeaAndProblemID, isDelete: false, isArchive: true)
        }
    }
    
    func updateIdeaAndProblemModelAndUI(theModel:IdeaAndProblemList?) {
        guard let model = theModel else { return }
        if let index = arr_IdeaNProblemList?.firstIndex(where: {$0.id == model.id }) {
            arr_IdeaNProblemList![index] = model
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = (theController.view as? IdeasNproblemsView)?.tableView.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsView)?.tableView.reloadRows(at: [indexRow], with: .none)
                if let contentPoint = contentOffset {
                    (theController.view as? IdeasNproblemsView)?.tableView.contentOffset = contentPoint
                }
            }
        }
    }
    func updateIdeaAndProblemListModelFromActionSubList(theModel:IdeaAndProblemList) {
        if let index = arr_IdeaNProblemList?.firstIndex(where: {$0.id == theModel.id}) {
            arr_IdeaNProblemList?.remove(at: index)
            arr_IdeaNProblemList?.insert(theModel, at: index)
            (theController.view as? IdeasNproblemsView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    func addIdeaAndProblemModelInList(theModel:IdeaAndProblemList) {
        if arr_IdeaNProblemList == nil {
            arr_IdeaNProblemList = [theModel]
        } else {
            arr_IdeaNProblemList?.insert(theModel, at: 0)
        }
        UIView.performWithoutAnimation {
            (theController.view as? IdeasNproblemsView)?.tableView.reloadData()
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = arr_IdeaNProblemList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = apiLoadingAtIdeaNProblemsID.firstIndex(where: {$0 == loadingID}) {
                apiLoadingAtIdeaNProblemsID.remove(at: index)
            }
            arr_IdeaNProblemList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsView)?.tableView.reloadData()
            }
//            (theController.view as? IdeasNproblemsView)?.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if arr_IdeaNProblemList?.count == 0 {
                setBackground(strMsg: "Idea and problem not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = apiLoadingAtIdeaNProblemsID.firstIndex(where: {$0 == loadingID}) {
                    apiLoadingAtIdeaNProblemsID.remove(at: index)
                }
            }
            let contentOffset = (theController.view as? IdeasNproblemsView)?.tableView.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsView)?.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                if let contentPoint = contentOffset {
                    (theController.view as? IdeasNproblemsView)?.tableView.contentOffset = contentPoint
                }
            }
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = arr_IdeaNProblemList?.firstIndex(where: {$0.id == strActionLogID }) {
            if isLoadingApi {
                apiLoadingAtIdeaNProblemsID.append(strActionLogID)
            } else {
                if let index = apiLoadingAtIdeaNProblemsID.firstIndex(where: {$0 == strActionLogID}) {
                    apiLoadingAtIdeaNProblemsID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        arr_IdeaNProblemList![index].revisedColor = model.revisedColor
                        arr_IdeaNProblemList![index].duedateColor = model.revisedColor
                        arr_IdeaNProblemList![index].revisedDate = model.revisedDate
                        arr_IdeaNProblemList![index].duedate = model.revisedDate
                        break
                    case .workDate:
//                        arr_IdeaNProblemList![index].workdateColor = model.workdateColor
                        //                        arr_IdeaNProblemList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            let contentOffset = (theController.view as? IdeasNproblemsView)?.tableView.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsView)?.tableView.reloadRows(at: [indexPath], with: .fade)
                if let contentPoint = contentOffset {
                    (theController.view as? IdeasNproblemsView)?.tableView.contentOffset = contentPoint
                }
            }
            
        }
    }

}

//MARK:- Api Call
extension IdeasNproblemsViewModel {
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_status_for_ideas_and_problems,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
    func ideaAndProblemListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
            WebService().cancelledCurrentRequestifExist(url: BASEURL + POST_IdeaAndProblemList)
        }
        var type = "Idea&Problem"
        switch selectedTitleType {
        case .ideas:
            type = "Idea"
            break
        case .problems:
            type = "Problem"
            break
        default:
            type = "Idea&Problem"
            break
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:nextOffset,APIKey.key_search:type,APIKey.key_department_id:filterData.departmentID,APIKey.key_assign_to:filterData.assigntoID,APIKey.key_company_id:filterData.companyID,APIKey.key_created_by:filterData.createdByID, APIKey.key_stage:filterData.stage.lowercased() == "all".lowercased() ? "" : filterData.stage] as [String : Any]
        
        IdeaAndProblemsWebServices.shared.getIdeaAndProblemList(parameter: parameter, success: { [weak self] (list, nextOffset) in
//            (self?.theController.view as? IdeasNproblemsView)?.updateFilterCountText(strCount: "1")
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            (self?.theController.view as? IdeasNproblemsView)?.refreshControl.endRefreshing()
            (self?.theController.view as? IdeasNproblemsView)?.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                (self?.theController.view as? IdeasNproblemsView)?.refreshControl.endRefreshing()
        })
    }
    
    func changeIdeaAndProblemStatusWebService(strIdeaAndProblemID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strIdeaAndProblemID]]).rawString() else {
            updateBottomCell(loadingID: strIdeaAndProblemID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_status_for_ideas_and_problems,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strIdeaAndProblemID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strIdeaAndProblemID)
        })
    }
}
