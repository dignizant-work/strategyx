//
//  IdeasNproblemsSubListVC.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeasNproblemsSubListVC: ParentViewController {
    //MARK:- Variable
    lazy var theCurrentView: IdeasNproblemsSubListView = { [unowned self] in
        return self.view as! IdeasNproblemsSubListView
    }()
    
    internal lazy var theCurrentViewModel: IdeasNproblemsSubListViewModel = {
        return IdeasNproblemsSubListViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setTheData(theModel:IdeaAndProblemList?, isForArchive:Bool = false) {
        theCurrentViewModel.theIdeaAndProblemModel = theModel
        theCurrentViewModel.isForArchive = isForArchive
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.viewArchive.isHidden = theCurrentViewModel.isForArchive
        theCurrentView.lblHeaderTitle.text = theCurrentViewModel.theIdeaAndProblemModel?.type ?? ""

//        theCurrentView.updateTitleText(strTitle: theCurrentViewModel.theFocusModel?.focusTitle ?? "")
        theCurrentView.setupTableView(theDelegate: self)
        
        theCurrentViewModel.strTitlePrimary = theCurrentViewModel.theIdeaAndProblemModel?.title ?? ""
        setCompanyData()
        theCurrentViewModel.actionsListWebService(isRefreshing: true)
        
        /*if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        }
        
        getAllFocusActionsListWebService()*/
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
//            theCurrentView.updateFilterCountText(strCount: "")
            theCurrentViewModel.strFilterCount = ""
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
//                theCurrentView.updateFilterCountText(strCount: "")
                theCurrentViewModel.strFilterCount = ""
            }
        }
    }
    
    func setBackground(strMsg:String) {
        if theCurrentViewModel.arr_ActionsList == nil || theCurrentViewModel.arr_ActionsList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    /*func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentViewModel.arr_ActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentViewModel.arr_ActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }*/
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentViewModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId }) {
            theCurrentViewModel.arr_ActionsList![index] = model
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentViewModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentViewModel.arr_ActionsList![index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentViewModel.arr_ActionsList![index].revisedDate = model.revisedDate
            theCurrentViewModel.arr_ActionsList![index].revisedColor = model.revisedColor
            theCurrentViewModel.arr_ActionsList![index].duedate = model.revisedDate
            theCurrentViewModel.arr_ActionsList![index].duedateColor = model.duedateColor
            theCurrentViewModel.arr_ActionsList![index].requestFlag = model.requestFlag
            theCurrentViewModel.arr_ActionsList![index].approveFlag = model.approveFlag

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentViewModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentViewModel.arr_ActionsList?[index].percentageCompeleted = "\(percentage)"
            theCurrentViewModel.arr_ActionsList?[index].approveFlag = percentage == 100 ? 1 : 0

            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    //MARK:- Redirection
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel)  in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentViewModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentViewModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentViewModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            if let model = theActionModel {
                theModel?.duedateColor = model.duedateColor
            }
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentViewModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentViewModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentViewModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentViewModel.arr_ActionsList?.removeAll()
            self?.theCurrentViewModel.arr_ActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.theCurrentViewModel.actionsListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToIdeasNProblemsDetailsVC() {
        let model = theCurrentViewModel.theIdeaAndProblemModel
        model?.notesCount = 0
        theCurrentViewModel.theIdeaAndProblemModel = model
        let vc = IdeaNProblemDetailVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        vc.setTheData(theModel: theCurrentViewModel.theIdeaAndProblemModel, isForArchive: theCurrentViewModel.isForArchive)
        self.push(vc: vc)
    }
    
    func redirectToActionEditVC(theModel:ActionDetail) {
        theCurrentViewModel.updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionList) in
            self?.theCurrentViewModel.updateTheIdeaAndProblemModelFromActionList(theActionDetailModel: nil, isForUpdate: true, theActionListModel: theActionList)
            
            //            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theModel) in
            self?.updateRequestDueDateActionListModelAndUI(theModel: theModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnArchiveAction(_ sender: Any) {
        theCurrentViewModel.filterData = FilterData.init()
        setCompanyData()
        theCurrentViewModel.isForActionArchive = true
        theCurrentView.viewArchive.isHidden = true
        theCurrentView.lblHeaderTitle.text = "\(theCurrentViewModel.theIdeaAndProblemModel?.type ?? "") > Archived Actions"
        theCurrentView.updateSubTitleViewHiddenForArchiveAction(true)
        theCurrentViewModel.arr_ActionsList?.removeAll()
        theCurrentViewModel.arr_ActionsList = nil
        theCurrentView.tableView.reloadData()
        theCurrentViewModel.actionsListWebService(isRefreshing: true)
    }
    @IBAction func onBtnFocusGraphAction(_ sender: Any) {
//        presentGraphAction()
    }
    
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnPrimaryTextDetailAction(_ sender: Any) {
        redirectToIdeasNProblemsDetailsVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        //        presentRiskFilter()
        presentActionFilter()
    }

}
