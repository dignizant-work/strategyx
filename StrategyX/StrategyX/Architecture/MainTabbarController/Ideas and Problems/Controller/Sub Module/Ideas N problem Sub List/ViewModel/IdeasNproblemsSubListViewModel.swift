//
//  IdeasNproblemsSubListViewModel.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class IdeasNproblemsSubListViewModel: NSObject {

    //MARK:-Variable
    fileprivate weak var theController:IdeasNproblemsSubListVC!
    
    var theIdeaAndProblemModel:IdeaAndProblemList? = nil {
        didSet{
            if let controller = theController.navigationController?.viewControllers {
                if let index = controller.firstIndex(where: {($0 as? IdeasNproblemsVC) != nil}) {
                    (controller[index] as? IdeasNproblemsVC)?.theCurrentViewModel.updateIdeaAndProblemListModelFromActionSubList(theModel: theIdeaAndProblemModel!)
                }
                if let index = controller.firstIndex(where: {($0 as? IdeasNProblemsArchivedVC) != nil}) {
                    (controller[index] as? IdeasNProblemsArchivedVC)?.theCurrentViewModel.updateIdeaAndProblemListModelFromActionSubList(theModel: theIdeaAndProblemModel!)
                }
            }
        }
    }
    var strTitlePrimary:String = "" {
        didSet{
            (theController.view as? IdeasNproblemsSubListView)?.lblTitlePrimary.text = strTitlePrimary
        }
    }
    var strFilterCount:String = "" {
        didSet{
            (theController.view as? IdeasNproblemsSubListView)?.updateFilterCountText(strCount: strFilterCount)
        }
    }
    
    var isForArchive = false
    var apiLoadingAtActionID:[String] = []
    var arr_ActionsList:[ActionsList]?
    var nextOffset = 0

    var filterData = FilterData.init()

    var apiLoadingAtIdeaNProblemsID:[String] = []
    let moreDD = DropDown()
    var isForActionArchive = false

    //MARK:- LifeCycle
    init(theController:IdeasNproblemsSubListVC) {
        self.theController = theController
    }
    
    func updateList(list:[ActionsList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_ActionsList?.removeAll()
        }
        nextOffset = offset
        if arr_ActionsList == nil {
            arr_ActionsList = []
        }
        list.forEach({ arr_ActionsList?.append($0) })
        print(arr_ActionsList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:ActionsList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]
        if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
            dataSource.remove(at: index) // edit
            dataSourceIcon.remove(at: index)
        }
        
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdBy) {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.performDropDownAction(index: indexRow, item: item)
                break
            default:
                break
            }
        }
    }

    func performDropDownAction(index:Int,item:String) {
        guard let model = arr_ActionsList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            theController.presentGraphActionList(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            //            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    
    func deleteActionLog(strActionLogID: String) {
        theController.showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = arr_ActionsList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            let deletedModel = model[indexRow]
            if let index = apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                apiLoadingAtActionID.remove(at: index)
            }
            arr_ActionsList?.remove(at: indexRow)
            if let model =  theIdeaAndProblemModel {
                model.totalAction -= 1
                model.totalAction = model.totalAction < 0 ? 0 : model.totalAction
                if isForActionArchive {
                    model.complateAction -= 1
                    model.complateAction = model.complateAction < 0 ? 0 : model.complateAction
                }
                //                model.completeActionFlag = model.totalAction == model.complateAction ? 1 : 0
                model.completeActionFlag = model.totalAction == model.complateAction ? (model.totalAction == 0 && model.complateAction == 0) ? 0 : 1 : 0

                theIdeaAndProblemModel = model
            }
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadData()
            }
//            (theController.view as? IdeasNproblemsSubListView)?.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            updateTheIdeaAndProblemModelFromActionList(isForUpdate: false, theActionListModel: deletedModel)
            if arr_ActionsList?.count == 0 {
                theController.setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    apiLoadingAtActionID.remove(at: index)
                }
            }
            
            let contentOffset = (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset

            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadRows(at: [index], with: .none)
                if let contentPoint = contentOffset {
                    (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset = contentPoint
                }
            }
        }
    }
    
    func updateIdeaAndProblemListModel(theModel:IdeaAndProblemList) {
        theIdeaAndProblemModel = theModel
        (theController.view as? IdeasNproblemsSubListView)?.updateTitleText(strTitle: theModel.title)
    }
    
    func updateTheIdeaAndProblemModelFromActionList(theActionDetailModel:ActionDetail? = nil,isForUpdate:Bool = false, theActionListModel:ActionsList? = nil) {
        guard var arrModels = arr_ActionsList else { return }
        var theModel = theActionListModel
        var previousModelPercentageCompeleted = "-1"
        
        if let model = theActionListModel {
            if let index = arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
                let updatedModel = arr_ActionsList![index]
                previousModelPercentageCompeleted = updatedModel.percentageCompeleted
                updatedModel.actionlogTitle = model.actionlogTitle
                updatedModel.revisedDate = model.revisedDate
                updatedModel.duedate = model.duedate
                updatedModel.workDate = model.workDate
                updatedModel.percentageCompeleted = model.percentageCompeleted
                updatedModel.assigneeId = model.assigneeId
                updatedModel.assignedTo = model.assignedTo
                updatedModel.assignedToUserImage = model.assignedToUserImage
                arr_ActionsList![index] = model
                theModel = model
                UIView.performWithoutAnimation {
                    (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
        
        guard let model = theIdeaAndProblemModel else { return }
        guard let deletedActionModel = theModel else { return }
        
        //        theIdeaAndProblemModel = model
        arrModels = arr_ActionsList!
        let totalActions = arrModels.count
        var completedActions = theIdeaAndProblemModel?.complateAction ?? 0
        var completedActionFlag = theIdeaAndProblemModel?.completeActionFlag ?? 0
        
        //        let filterCompletedActions = arrModels.filter({ $0.percentageCompeleted == "100" })
        
        if isForUpdate {
            if deletedActionModel.percentageCompeleted != "100" && previousModelPercentageCompeleted == "100" {
                completedActions -= 1
            } else if deletedActionModel.percentageCompeleted == "100" && previousModelPercentageCompeleted != "100" {
                completedActions += 1
            }
        } else {
            if deletedActionModel.percentageCompeleted == "100" {
                completedActions -= 1
            }
        }
        
        completedActionFlag = (completedActions != 0 && totalActions != 0) && (completedActions == totalActions) ? 1 : 0
        
        model.totalAction = totalActions
        model.complateAction = completedActions
        model.completeActionFlag = completedActionFlag
        print("theIdeaAndProblemModel:=",model)
        theIdeaAndProblemModel = model
        
    }
    func updateActionListModel(theActionDetailModel:ActionDetail?) {
        guard let model = theActionDetailModel else { return }
        
        if let index = arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionLogid}) {
            arr_ActionsList![index].actionlogTitle = model.actionTitle
            arr_ActionsList![index].revisedDate = model.revisedDate
            arr_ActionsList![index].duedate = model.duedate
            arr_ActionsList![index].workDate = model.workDate
            arr_ActionsList![index].assigneeId = model.assigneeId
            arr_ActionsList![index].assignedTo = model.assignedTo
            arr_ActionsList![index].assignedToUserImage = model.assignedToUserImage
            arr_ActionsList![index].percentageCompeleted = model.percentageCompeleted
            arr_ActionsList![index].approveFlag = model.approveFlag
            arr_ActionsList![index].requestFlag = model.requestFlag
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        }
    }
    func updateNotesCount(strActionLogID:String) {
        if let index = arr_ActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            arr_ActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadRows(at: [indexRow], with: .none)
                if let offset = contentOffset {
                    (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset = offset
                }
            }
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                apiLoadingAtActionID.append(strActionLogID)
            } else {
                //                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    apiLoadingAtActionID.remove(at: index)
                }
                
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        arr_ActionsList![index].revisedColor = model.revisedColor
                        arr_ActionsList![index].duedateColor = model.revisedColor
                        arr_ActionsList![index].revisedDate = model.revisedDate
                        arr_ActionsList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        arr_ActionsList![index].workdateColor = model.workdateColor
                        arr_ActionsList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                if let contentOffset = (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset {
                    (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadRows(at: [indexPath], with: .none)
                    (theController.view as? IdeasNproblemsSubListView)?.tableView.contentOffset = contentOffset
                }
            }
        }
    }
   
    
}

//MARK:- API Call
extension IdeasNproblemsSubListViewModel {
    func actionsListWebService(isRefreshing:Bool = false) {
        guard let ideaNProblemID = theIdeaAndProblemModel?.id else {
            (theController.view as? IdeasNproblemsSubListView)?.refreshControl.endRefreshing()
            (theController.view as? IdeasNproblemsSubListView)?.tableView.reloadData()
            return
        }
        
        if isRefreshing {
            nextOffset = 0
        }

        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = filterData.workDate, !workDateFormate.isEmpty {
            //            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = filterData.dueDate, !dueDateFormate.isEmpty {
            //            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_page_offset:"\(nextOffset)",
            APIKey.key_tags:filterData.searchTag,
            APIKey.key_search:filterData.searchText,
            APIKey.key_department_id:filterData.departmentID,
            APIKey.key_assign_to:filterData.assigntoID.isEmpty ? "0" : filterData.assigntoID,
            APIKey.key_company_id:filterData.companyID,
            APIKey.key_workdatefilter:workDate,
            APIKey.key_duedatefilter:dueDate]
        
        if theIdeaAndProblemModel?.type.lowercased() == APIKey.key_idea.lowercased() {
            parameter[APIKey.key_idea] = ideaNProblemID
        } else {
            parameter[APIKey.key_problem] = ideaNProblemID
        }
        if filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(filterData.relatedToValue)"
        }
        if isForActionArchive {
            parameter[APIKey.key_grid_type] = APIKey.key_archived
        }
        ActionsWebService.shared.getAllActionsList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            (self?.theController.view as? IdeasNproblemsSubListView)?.refreshControl.endRefreshing()
            (self?.theController.view as? IdeasNproblemsSubListView)?.tableView.reloadData()
            },  failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                self?.theController.setBackground(strMsg: error)
                (self?.theController.view as? IdeasNproblemsSubListView)?.refreshControl.endRefreshing()
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
         alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
         let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
         loadingIndicator.hidesWhenStopped = true
         loadingIndicator.style = UIActivityIndicatorView.Style.gray
         loadingIndicator.tintColor = appGreen
         loadingIndicator.color = appGreen
         loadingIndicator.startAnimating()
         
         alert.view.addSubview(loadingIndicator)
         present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            //            alert.dismiss(animated: false, completion: nil)
            Utility.shared.stopActivityIndicator()
            self?.theController.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                //                alert.dismiss(animated: false, completion: nil)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}
