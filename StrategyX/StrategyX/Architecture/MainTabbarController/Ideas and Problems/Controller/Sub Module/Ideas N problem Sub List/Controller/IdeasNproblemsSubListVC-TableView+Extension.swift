//
//  IdeasNproblemsSubListVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:-UITableViewDataSource
extension IdeasNproblemsSubListVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentViewModel.arr_ActionsList == nil || theCurrentViewModel.arr_ActionsList?.count == 0) {
            return 0
        }
        return theCurrentViewModel.arr_ActionsList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TacticalProjectSubListTVCell") as! TacticalProjectSubListTVCell
//        cell.hideViewNotes()
        cell.showMoreButtonForAction(isHidden: true)
        if theCurrentViewModel.arr_ActionsList != nil {
            var isApiLoading = false
            if theCurrentViewModel.apiLoadingAtActionID.contains(theCurrentViewModel.arr_ActionsList![indexPath.row].actionlogId) {
                isApiLoading = true
            }
            cell.configureActionsList(theModel: theCurrentViewModel.arr_ActionsList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentViewModel.arr_ActionsList![indexPath.row])
//                self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].actionlogId ?? "")
            }
            cell.handleWorkDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].workDate ?? "", dueDate: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_ActionsList![indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerPercentCompletedAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentViewModel.arr_ActionsList?[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentViewModel.arr_ActionsList?[indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentViewModel.configureDropDown(dropDown: obj.theCurrentViewModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentViewModel.arr_ActionsList![indexPath.row])
                    obj.theCurrentViewModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.nextOffset != 0 {
                theCurrentViewModel.actionsListWebService()
            }
            
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentViewModel.arr_ActionsList, arr_model.count > 0 else { return false }
        if theCurrentViewModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
}
//MARK:-UITableViewDelegate
extension IdeasNproblemsSubListVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentViewModel.arr_ActionsList, arr_model.count > 0, !theCurrentViewModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.theCurrentViewModel.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentViewModel.arr_ActionsList, arr_model.count > 0, !theCurrentViewModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("delete")
                self.theCurrentViewModel.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.arr_ActionsList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let arr_model = theCurrentViewModel.arr_ActionsList, !theCurrentViewModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            theCurrentViewModel.performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            
            /*if theCurrentModel.theFocusModel?.focusStatus.lowercased() == APIKey.key_archived.lowercased() {
             redirectToActionDetailScreen(theModel: theCurrentViewModel.arr_ActionsList![indexPath.row])
             } else {
             performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
             }*/
        }
    }
}
