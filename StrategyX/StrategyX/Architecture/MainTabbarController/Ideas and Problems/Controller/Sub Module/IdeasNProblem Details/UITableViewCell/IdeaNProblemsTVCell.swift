//
//  IdeaNProblemsTVCell.swift
//  StrategyX
//
//  Created by Haresh on 19/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class IdeaNProblemsTVCell: UITableViewCell {

    //headerCell
    @IBOutlet weak var btnCommentTouchup: UIButton!
    @IBOutlet weak var btnLikeTouchup: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var lblCommentCount: UILabel!
    
    //Cell 1
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var btnEdit: UIButton!
    let disposeBag = DisposeBag()
    var handlerTitleText:(String) -> Void = {_ in}
    var handlerText:(String) -> Void = {_ in}

    //Cell2
    
    
    //Cell3
    @IBOutlet weak var lblTypeName: UILabel!
    
    @IBOutlet weak var lblTypeDescription: UILabel!
    
    //Cell4
    @IBOutlet weak var lblItemTitle: UILabel!
    
    @IBOutlet weak var lblItemSelectedName: UILabel!
    @IBOutlet weak var viewDropDown: UIView!
    var handlerBtnClick:() -> Void = {}

    
    //Cell5
    @IBOutlet weak var lblInputDateTitle: UILabel!
    @IBOutlet weak var lblInputDate: UILabel!
    @IBOutlet weak var viewDate: UIView!

    //Cell6
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var constrainLblDueDateLeading: NSLayoutConstraint! // 5.0
    
    //Cell7
    @IBOutlet weak var viewVisibility: UIView!
    @IBOutlet weak var btnPublic: UIButton!
    @IBOutlet weak var btnPrivate: UIButton!
    var handlerVisibilityClick:(Int) -> Void = {_ in }
    
    //Cell8
    @IBOutlet weak var viewRelatedToGeneralAction: UIView!
    @IBOutlet weak var viewSelectionofRelatedToGeneralAction: UIView!
    @IBOutlet weak var lblRelatedToGeneralAction: UILabel!
    @IBOutlet weak var viewRelatedToOtherAction: UIView!
    @IBOutlet weak var lblTitleRelatedToOtherAction: UILabel!
    @IBOutlet weak var viewSelectionofRelatedToOtherAction: UIView!
    @IBOutlet weak var lblRelatedToOtherAction: UILabel!

    
    //LikeCell
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLikeDate: UILabel!
    
    //Bottom Cell
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnBottomComment: UIButton!
    @IBOutlet weak var btnBottomLike: UIButton!
    
    var handlerBottomComment:() -> Void = {}
    var handlerBottomLike:() -> Void = {}
    var handlerSave:() -> Void = { }

    
    
    var handlerLikeOrCommentedSelected:(Bool) -> Void = {_ in}
    var handlerAttachmentOrVoiceNote:(Bool) -> Void = {_ in}
    var handlerBackAction:() -> Void = {}
    var handlerEditAction:() -> Void = {}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureHeaderCell(strLikeCount:String,strCommentCount:String, isLikeSelected:Bool) {
        lblLikeCount.text = strLikeCount
        lblCommentCount.text = strCommentCount
        
        if isLikeSelected {
            btnComment.tintColor = appPlaceHolder
            btnLike.tintColor = appGreen
        } else {
            btnLike.tintColor = appPlaceHolder
            btnComment.tintColor = appGreen
        }
    }
    func configureHeaderCellLikeAndComment(isLoadingForLike:Bool, isLoadingForComment:Bool) {
        if isLoadingForLike {
            btnLikeTouchup.startLoadyIndicator()
        } else {
            btnLikeTouchup.stopLoadyIndicator()
        }
        if isLoadingForComment {
            btnCommentTouchup.startLoadyIndicator()
        } else {
            btnCommentTouchup.stopLoadyIndicator()
        }
        if isLoadingForLike || isLoadingForComment {
            self.contentView.allButtons(isEnable: false)
        } else {
            self.contentView.allButtons(isEnable: true)
        }
    }
    
    func configureLikeCell(theModel:INPLikeList) {
        lblUserName.text = theModel.userName
        imgProfile.sd_setImageLoadMultiTypeURL(url: theModel.userImage, placeholder: "ic_mini_plash_holder")
        lblLikeDate.text = ""
        if let dueDate = theModel.created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType11),!dueDate.isEmpty {
            lblLikeDate.text = dueDate
        }
    }
    
    func configureCell1(strTitle:String, type:String) {
        lblTitle.text = type
        txtTitle.text = strTitle
//        txtTitle.isUserInteractionEnabled = false
        rxTextFieldAction()
//        lblDescription.text = strDescription
    }
    func rxTextFieldAction() {
        txtTitle.rx.text.orEmpty
            .subscribe({ [weak self] _ in
                self?.handlerTitleText(self?.txtTitle.text ?? "")
            }).disposed(by:disposeBag)
    }
    func setTheDescription(inputText:String) {
        txtDescription.text = inputText
        rxTextViewDidChange()

        DispatchQueue.main.async { [weak self] in
            self?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        
    }
    //RX
    func rxTextViewDidChange() {
        txtDescription.rx.text.orEmpty
            .subscribe({ [weak self] event in
                self?.handlerText(self?.txtDescription.text ?? "")
                if let obj = self {
                    obj.lblPlaceHolder.isHidden = obj.txtDescription.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    func configureCell3(strTypeName:String,strTypeDescription:String) {
        lblTypeName.text = strTypeName
        lblTypeDescription.text = strTypeDescription
    }
    
    func configureCell4(strItemTitle:String,strSelectedName:String) {
        lblItemTitle.text = strItemTitle
        lblItemSelectedName.text = strSelectedName
    }
    
    func configureCell6(strItemTitle:String,strSelectedName:String,strDueDate:String, strDueDateColor:String) {
        lblItemTitle.text = strItemTitle
        lblItemSelectedName.text = strSelectedName
        constrainLblDueDateLeading.constant = strDueDate.isEmpty ? 0.0 : 5.0
        lblDueDate.attributedText = nil
        lblDueDate.text = ""
        if !strDueDate.isEmpty {
            let attributeStringDue =  NSAttributedString(string: strDueDate, attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: strDueDateColor)])
            lblDueDate.attributedText = attributeStringDue
        }
    }
    
    func configureCell7(strItemTitle:String,strSelectionType:Int,isCanEdit:Bool) {
        lblItemTitle.text = strItemTitle
        viewVisibility.isUserInteractionEnabled = isCanEdit
        updateVisibilityAction(sender: strSelectionType == 1 ? btnPrivate : btnPublic)
    }
    
    func configureCell8(strRelatedID:Int,strLabel:String,strRelatedTitle:String) {
        isEnableView(isEnable: false, view:viewSelectionofRelatedToOtherAction)
        isEnableView(isEnable: false, view:viewSelectionofRelatedToGeneralAction)
        lblTitleRelatedToOtherAction.text = strLabel
        lblRelatedToGeneralAction.text = strLabel
        lblRelatedToOtherAction.text = strRelatedTitle
        
        if strRelatedID == 0 {
            viewRelatedToOtherAction.isHidden = true
        }
    }
    
    func configureBottomCell(isLoadingForAdd:Bool = false, strTitleBtnAdd:String = "Save", isBtnAddHidden:Bool = false, strTitleBtnComment:String = "Comment", isBtnCommentHidden:Bool = false, isLoadingForLike:Bool = false, strTitleBtnLike:String = "Like", isBtnLikeHidden:Bool = false) {
        btnAdd.setTitle(strTitleBtnAdd, for: .normal)
        btnBottomComment.setTitle(strTitleBtnComment, for: .normal)
        btnBottomLike.setTitle(strTitleBtnLike, for: .normal)

        btnAdd.isHidden = isBtnAddHidden
        btnBottomComment.isHidden = isBtnCommentHidden
        btnBottomLike.isHidden = isBtnLikeHidden

        if isLoadingForAdd || isLoadingForLike {
            if isLoadingForAdd {
                btnAdd.startLoadyIndicator()
                self.contentView.allButtons(isEnable: false)
            }
            if isLoadingForLike {
                btnBottomLike.startLoadyIndicator()
                self.contentView.allButtons(isEnable: false)
            }
        } else {
            btnAdd.stopLoadyIndicator()
            btnBottomLike.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }
        /*
        if isLoadingForAdd {
            btnAdd.startLoadyIndicator()
            self.contentView.allButtons(isEnable: false)
        } else {
            btnAdd.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }
        
        if isLoadingForLike {
            btnBottomLike.startLoadyIndicator()
            self.contentView.allButtons(isEnable: false)
        } else {
            btnBottomLike.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }*/
    }
    func isLikedButton(isLike:Int) {
        btnBottomLike.setBackGroundColor = isLike == 1 ? 28 : 3
    }
    
    //MARK:-IBAction
    func isDisableCompanySelection() {
        if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
            viewDropDown.isUserInteractionEnabled = false
        }
    }
    
    func dropDown(isEnable:Bool) {
        viewDropDown.isUserInteractionEnabled = isEnable ? true : false
        viewDropDown.alpha = isEnable ? 1.0 : 0.6
    }
    
    func configureCell5(strInputDateTitle:String,strInputselectedDate:String) {
        lblInputDate.text = strInputselectedDate
        lblInputDateTitle.text = strInputDateTitle
    }
    func viewDate(isEnable:Bool) {
        viewDate.isUserInteractionEnabled = isEnable
        viewDate.alpha = isEnable ? 1.0 : 0.6
    }
    
    func isEnableView(isEnable:Bool,view:UIView) {
        view.isUserInteractionEnabled = isEnable ? true : false
        view.alpha = isEnable ? 1.0 : 0.6
    }
    
    func updateVisibilityAction(sender:UIButton?) {
        if let btn = sender {
            if btnPublic.isSelected == btn.isSelected {
                btnPublic.isSelected = true
                btnPrivate.isSelected = false
            } else {
                btnPublic.isSelected = false
                btnPrivate.isSelected = true
            }
        }
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnBackAction(_ sender: Any) {
        handlerBackAction()
    }
    
    @IBAction func onBtnEditAction(_ sender: Any) {
        handlerEditAction()
    }

    @IBAction func onBtnAddAttachmentAction(_ sender: Any) {
        handlerAttachmentOrVoiceNote(true)
    }
    
    @IBAction func onBtnAddVoiceAction(_ sender: Any) {
        handlerAttachmentOrVoiceNote(false)
    }
    
    @IBAction func onBtnDropDownOpenAction(_ sender: Any) {
       handlerBtnClick()
    }
    
    
    @IBAction func onBtnDateAction(_ sender: Any) {
        handlerBtnClick()
    }
    
    @IBAction func onBtnLikeAction(_ sender: UIButton) {
        handlerLikeOrCommentedSelected(true)
    }
    
    @IBAction func onBtnCommentAction(_ sender: UIButton) {
        handlerLikeOrCommentedSelected(false)
    }
    
    
    @IBAction func onBtnAddAction(_ sender: UIButton) {
        handlerSave()
    }
    
    @IBAction func onBtnBottomCommentAction(_ sender: Any) {
        handlerBottomComment()
    }
    
    @IBAction func onBtnBottomLikeAction(_ sender: Any) {
        handlerBottomLike()
    }
    
    @IBAction func onBtnVisiblityAction(_ sender: UIButton) {
        updateVisibilityAction(sender: sender)
        handlerVisibilityClick(btnPublic.isSelected ? 0 : 1)
    }
}
