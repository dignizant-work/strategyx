//
//  IdeaNProblemDetailVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 19/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:- UITableViewDataSource
extension IdeaNProblemDetailVC:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if let _ = tableView.backgroundView , (theCurrentViewModel.arr_FocusList == nil || theCurrentViewModel.arr_FocusList?.count == 0) {
         return 0
         }
         tableView.backgroundView = nil
         return theCurrentViewModel.arr_FocusList?.count ?? 10*/
        if section == 0 {
            return 1
        } else if section == 1 {
            return 10
        } else if section == 2 {
            return 1 + theCurrentViewModel.arr_selectedFile.count + 1
        } else {
            var count = 1
            if theCurrentViewModel.isLikeSelected {
                count = theCurrentViewModel.arr_Like.count == 0 ? 1 : theCurrentViewModel.arr_Like.count
            } else {
                count = theCurrentViewModel.arr_Comment.count == 0 ? 2 : theCurrentViewModel.arr_Comment.count + 1
            }
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! IdeaNProblemsTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            //                cell.configure(strTitle: "Description", strPlaceHolder: "Description",isMandatory:false)
            cell.setTheDescription(inputText: theCurrentViewModel.selectedData["description"].stringValue)
            cell.configureCell1(strTitle:theCurrentViewModel.selectedData["title"].stringValue, type: theCurrentViewModel.selectedData["type"].stringValue)
            cell.btnEdit.isHidden = true
            if UserDefault.shared.userRole == .staffUser {
                cell.txtDescription.isUserInteractionEnabled = false
                cell.txtTitle.isUserInteractionEnabled = false
            } else {
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    cell.btnEdit.isHidden = true
                    cell.txtDescription.isUserInteractionEnabled = false
                    cell.txtTitle.isUserInteractionEnabled = false
                }
            }
            
            
            cell.handlerEditAction = { [weak self] in
                self?.redirecToAddIdeaAndProblemVC()
            }
            cell.handlerTitleText = { [weak self] (text) in
                self?.theCurrentViewModel.selectedData["title"].stringValue = text
            }
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentViewModel.selectedData["description"].stringValue = text
            }
            cell.handlerBackAction = { [weak self] in
                self?.backAction(isAnimation: false)
            }
            
            return cell
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! IdeaNProblemsTVCell
                cell.configureCell3(strTypeName: "Created By", strTypeDescription: theCurrentViewModel.selectedData["createdby"].stringValue)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! IdeaNProblemsTVCell
                cell.configureCell3(strTypeName: "Created Date", strTypeDescription: theCurrentViewModel.selectedData["submitted"].stringValue.isEmpty ? "-" : theCurrentViewModel.selectedData["submitted"].stringValue)
                return cell
            } /*else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell6") as! IdeaNProblemsTVCell
                    
                if (theCurrentViewModel.selectedData["stage"].stringValue.lowercased() == "fixing".lowercased()) || (theCurrentViewModel.selectedData["stage"].stringValue.lowercased() == "accepted".lowercased()), let strDueDate = theCurrentViewModel.selectedData["duedate"].stringValue.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType2), !strDueDate.isEmpty {
                    cell.configureCell6(strItemTitle: "Stage", strSelectedName: theCurrentViewModel.selectedData["stage"].stringValue, strDueDate: strDueDate, strDueDateColor: theCurrentViewModel.theIdeaAndProblemModel?.duedateColor ?? "")
                } else {
                    cell.configureCell6(strItemTitle: "Stage", strSelectedName: theCurrentViewModel.selectedData["stage"].stringValue, strDueDate: "",strDueDateColor: theCurrentViewModel.theIdeaAndProblemModel?.duedateColor ?? "")
                }
                
//                cell.configureCell4(strItemTitle: "Stage", strSelectedName: theCurrentViewModel.selectedData["stage"].stringValue)
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    cell.dropDown(isEnable: false)
                } else {
                    if theCurrentViewModel.isEdit {
                        if UserRole.staffUser != UserDefault.shared.userRole {
                            cell.dropDown(isEnable: true)
                        } else {
                            cell.dropDown(isEnable: false)
                        }
                    } else {
                        cell.isDisableCompanySelection()
                    }
                }
                
                cell.handlerBtnClick = { [weak self] in
                    self?.presentDropDownListVC(categorySelection: IdeaNProblemDetailViewModel.categorySelectionType.stage)
                }
                return cell
            } */ else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! IdeaNProblemsTVCell
                cell.configureCell4(strItemTitle: "Company", strSelectedName: theCurrentViewModel.selectedData["company"].stringValue.isEmpty ? "Select company" : theCurrentViewModel.selectedData["company"].stringValue)
                
                if theCurrentViewModel.isEdit {
                    cell.dropDown(isEnable: false)
                } else {
                    cell.isDisableCompanySelection()
                }
                cell.handlerBtnClick = { [weak self] in
                    self?.presentDropDownListVC(categorySelection: IdeaNProblemDetailViewModel.categorySelectionType.company)
                }
                return cell
            } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell8") as! IdeaNProblemsTVCell
                cell.configureCell8(strRelatedID: theCurrentViewModel.theIdeaAndProblemModel?.relatedId ?? 0, strLabel: theCurrentViewModel.theIdeaAndProblemModel?.label ?? "", strRelatedTitle: theCurrentViewModel.theIdeaAndProblemModel?.related_title ?? "")
                return cell
            }  else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! IdeaNProblemsTVCell
                cell.configureCell4(strItemTitle: "Stage", strSelectedName: theCurrentViewModel.selectedData["stage"].stringValue.isEmpty ? "Select stage" : theCurrentViewModel.selectedData["stage"].stringValue)
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    cell.dropDown(isEnable: false)
                } else {
                    if theCurrentViewModel.isEdit {
                        if UserRole.staffUser != UserDefault.shared.userRole {
                            cell.dropDown(isEnable: true)
                        } else {
                            cell.dropDown(isEnable: false)
                        }
                    } else {
                        cell.isDisableCompanySelection()
                    }
                }
                
                cell.handlerBtnClick = { [weak self] in
                    self?.presentDropDownListVC(categorySelection: IdeaNProblemDetailViewModel.categorySelectionType.stage)
                }
                return cell
            } else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! IdeaNProblemsTVCell
                cell.configureCell4(strItemTitle: "Assigned to", strSelectedName: theCurrentViewModel.selectedData["assignto"].stringValue.isEmpty ? "Select assigned to" : theCurrentViewModel.selectedData["assignto"].stringValue)
                if theCurrentViewModel.isEdit {
                    cell.dropDown(isEnable: false)
                    if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                        cell.dropDown(isEnable: false)
                    } else {
                        if UserRole.staffUser != UserDefault.shared.userRole {
                            if let theModel = theCurrentViewModel.theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById){
                                cell.dropDown(isEnable: true)
                            }
                        }
                    }
                }
                cell.handlerBtnClick = { [weak self] in
                    self?.presentDropDownListVC(categorySelection: IdeaNProblemDetailViewModel.categorySelectionType.assignTo)
                }
                return cell
            } else if indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! IdeaNProblemsTVCell
                cell.configureCell4(strItemTitle: "Department", strSelectedName:theCurrentViewModel.selectedData["department"].stringValue.isEmpty ? "Select department" : theCurrentViewModel.selectedData["department"].stringValue)
                if theCurrentViewModel.isEdit {
                    cell.dropDown(isEnable: false)
                    if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                        cell.dropDown(isEnable: false)
                    } else {
                        if UserRole.staffUser != UserDefault.shared.userRole {
                            if let theModel = theCurrentViewModel.theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById){
                                cell.dropDown(isEnable: true)
                            }
                        }
                    }
                    
                }
                cell.handlerBtnClick = { [weak self] in
                    self?.presentDropDownListVC(categorySelection: IdeaNProblemDetailViewModel.categorySelectionType.department)
                }
                return cell
            } else if indexPath.row == 7 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! IdeaNProblemsTVCell
                cell.configureCell3(strTypeName: "Due Date", strTypeDescription: theCurrentViewModel.selectedData["duedate"].stringValue.isEmpty ? "-" : theCurrentViewModel.selectedData["duedate"].stringValue)
                return cell
                /*
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell5") as! IdeaNProblemsTVCell
                cell.configureCell5(strInputDateTitle: "Due Date", strInputselectedDate: theCurrentViewModel.selectedData["duedate"].stringValue)
                if theCurrentViewModel.isEdit {
                    cell.viewDate(isEnable: false)
                }
                cell.handlerBtnClick = { [weak self] in
                    //                self?.redirectToDatePicker(selectionType: .due)
                    self?.redirectToCustomDatePicker(selectionType: .due)
                }
                return cell*/
            } else if indexPath.row == 8 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell5") as! IdeaNProblemsTVCell
                cell.configureCell5(strInputDateTitle: "Revised Date", strInputselectedDate:theCurrentViewModel.selectedData["revisedduedate"].stringValue.isEmpty ? "Select revised date" : theCurrentViewModel.selectedData["revisedduedate"].stringValue)
                cell.viewDate(isEnable: false)
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    cell.viewDate(isEnable: false)
                } else {
                    if UserRole.staffUser != UserDefault.shared.userRole {
                        if let theModel = theCurrentViewModel.theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById){
                            cell.viewDate(isEnable: true)
                        }
                    }
                }
                

//                if theCurrentViewModel.isEdit {
//                    cell.viewDate(isEnable: false)
//                }
                cell.handlerBtnClick = { [weak self] in
                    //                self?.redirectToDatePicker(selectionType: .due)
                    self?.redirectToCustomDatePicker(selectionType: .resived)
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell7") as! IdeaNProblemsTVCell
                var isCanEdit = false
                if let theModel = theCurrentViewModel.theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById) {
                    isCanEdit = true
                }
                
                cell.configureCell7(strItemTitle: "Visibility", strSelectionType: theCurrentViewModel.selectedData["isPrivate"].intValue, isCanEdit: isCanEdit)
                
                
                cell.handlerVisibilityClick = { [weak self] (index) in
                    print(index)
                    self?.theCurrentViewModel.selectedData["isPrivate"].intValue = index
                }
                return cell
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! IdeaNProblemsTVCell
                cell.handlerAttachmentOrVoiceNote = { [weak self] isAttachment in
                    if isAttachment { // Add Attachment
                        self?.presentFileManger()
                    } else { // Add Voice
                        self?.presentVoiceNote()
                    }
                }
                return cell
            } else if indexPath.row > 0, theCurrentViewModel.arr_selectedFile.count != 0,  theCurrentViewModel.arr_selectedFile.count + 1 != indexPath.row {
//                1 + theCurrentViewModel.arr_selectedFile.count + 1
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
                cell.selectionStyle = .none
                cell.tag = indexPath.row
                let index = indexPath.row - 1
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    cell.btnClose.isHidden = true
                }
                //                cell.lblTitle.setFontColor = theCurrentViewModel.arr_selectedFile[index].fileType == .audio ? 1 : 5
                
                //                cell.bgView.setBackGroundColor = theCurrentViewModel.arr_selectedFile[index].fileType == .audio ? 3 : 26
                cell.configure(strTitle: (theCurrentViewModel.arr_selectedFile[index].fileName as NSString).lastPathComponent)
                cell.handlerOnDeleteAction = { [weak self] (tag) in
                    self?.theCurrentViewModel.deleteAttachment(index: index)
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BottomCell") as! IdeaNProblemsTVCell
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                cell.configureBottomCell(isLoadingForAdd: theCurrentViewModel.isEditIdeaAndProblemApiLoading, strTitleBtnAdd: "Save & Exit", isBtnAddHidden: false, strTitleBtnComment: "Comment", isBtnCommentHidden: false, isLoadingForLike: theCurrentViewModel.isEditLikeApiLoading, strTitleBtnLike: "Like", isBtnLikeHidden: false)
                //                cell.configure(isLoading: theCurrentViewModel.isEditIdeaAndProblemApiLoading)
                cell.isLikedButton(isLike: theCurrentViewModel.theIdeaAndProblemModel?.isLike ?? 0)
                if theCurrentViewModel.isEdit {
                   
                    //                    cell.configure(isLoading: theCurrentViewModel.isEditIdeaAndProblemApiLoading, strTitlebtnAdd: "Edit", strTitlebtnCancel: "Close")
                    //                    cell.configure(isLoading: theCurrentViewModel.isEditIdeaAndProblemApiLoading, strTitlebtnAdd: "Save", isBtnAddHidden: false, strTitlebtnCancel: "Like", isBtnCancelHidden: true)
                }
                cell.handlerSave = { [weak self] in
                    if ((self?.theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (self?.theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (self?.theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased())) {
                        
                        self?.backAction(isAnimation: false)
                    } else {
                        if UserDefault.shared.userRole == .staffUser {
                            self?.backAction(isAnimation: false)
                        } else {
                            self?.theCurrentViewModel.validateForm()
                        }
                    }
                }
                cell.handlerBottomComment = { [weak self] in
                    self?.theCurrentViewModel.isLikeSelected = false
                    if self?.theCurrentViewModel.arr_Comment.count == 0 {
                        self?.theCurrentViewModel.commentListService()
                    }
                }
                cell.handlerBottomLike = { [weak self] in
                    self?.theCurrentViewModel.likeUnlikeService()
                }
                /*cell.handlerAdd = { [weak self] in
                 self?.theCurrentViewModel.validateForm()
                 }
                 cell.handlerCancel = { [weak self] in
                 self?.backAction(isAnimation: false)
                 }*/
                return cell
            }
        } else {
            if theCurrentViewModel.isLikeSelected {
                if theCurrentViewModel.arr_Like.count == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataFoundTVCell") as! NoDataFoundTVCell
                    //                cell.configureLikeCell(strUserName: "Michael Johnson", strDate: "30 Mar 18, 2:53pm")
                    cell.setMessage(strMsg: theCurrentViewModel.likeErrorMsg)
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "likeCell") as! IdeaNProblemsTVCell
                    //                cell.configureLikeCell(strUserName: "Michael Johnson", strDate: "30 Mar 18, 2:53pm")
                    cell.configureLikeCell(theModel: theCurrentViewModel.arr_Like[indexPath.row])
                    return cell
                }
                
            } else {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5") as! RisksDetailTVCell
                    cell.contentView.backgroundColor = .clear
                    cell.backgroundColor = .clear
                    cell.configureCell5(isLoading: theCurrentViewModel.isCommentApiLoading)
//                    cell.configureCell5(isLoading: false)

                    
                    cell.handlerText = {[weak self] (strText) in
                        //                cell.txtAddNote.text = strText
                        self?.theCurrentViewModel.strAddCommentText = strText
                    }
                    cell.handlerOnBtnAddPostClick = {[weak self] in
                        self?.theCurrentViewModel.validComment()
                        cell.txtAddNote.text = ""
                    }
                    return cell
                } else if theCurrentViewModel.arr_Comment.count == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataFoundTVCell") as! NoDataFoundTVCell
                    //                cell.configureLikeCell(strUserName: "Michael Johnson", strDate: "30 Mar 18, 2:53pm")
                    cell.setMessage(strMsg: theCurrentViewModel.commentErrorMsg)
                    return cell
                }  else {
                    let noteCell = tableView.dequeueReusableCell(withIdentifier: "RisksNoteTVCell") as! RisksNoteTVCell
                    noteCell.contentView.backgroundColor = .clear
                    noteCell.backgroundColor = .clear
                    noteCell.selectionStyle = .none
                    noteCell.constrainViewTrailing.constant = 0.0
                    noteCell.constrainViewLeading.constant = 0.0
                    /*if theCurrentViewModel.arr_noteList.count <= 0{
                        noteCell.lblNorecordfound.isHidden = false
                        return noteCell
                    }*/
                    noteCell.lblNorecordfound.isHidden = true
                    noteCell.btnMoreOutlet.isHidden = true
                   
                    let note = theCurrentViewModel.arr_Comment[indexPath.row - 1]
                    var dateDue = ""
                    let date = note.created
                    let dueDateformate = date.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType5)
                    dateDue = dueDateformate!
                    noteCell.configure(strDescription: note.comment, name: note.userName, date: dateDue, img: note.userImage)
//                    noteCell.configure(strDescription: "In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu.", name:"Michael Johnson" , date: "30 Mar 18, 2:53pm", img: "")
                    return noteCell
                }
            }
        }
    }
}

//MARK:- UITableViewDelegate
extension IdeaNProblemDetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 || section == 1 || section == 2 {
            return nil
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! IdeaNProblemsTVCell
            cell.configureHeaderCell(strLikeCount: "(\(theCurrentViewModel.arr_Like.count))", strCommentCount: "(\(theCurrentViewModel.theIdeaAndProblemModel?.comment ?? 0))", isLikeSelected: theCurrentViewModel.isLikeSelected)
            cell.configureHeaderCellLikeAndComment(isLoadingForLike: theCurrentViewModel.isLikeListApiLoading, isLoadingForComment: theCurrentViewModel.isCommentListApiLoading)
            cell.handlerLikeOrCommentedSelected = { [weak self] (isLike) in
                self?.theCurrentViewModel.isLikeSelected = isLike
            }
            cell.btnLikeTouchup.addTarget(self, action: #selector(onheaderButtonLikeOrCommentAction(_:)), for: .touchUpInside)
            cell.btnCommentTouchup.addTarget(self, action: #selector(onheaderButtonLikeOrCommentAction(_:)), for: .touchUpInside)

            return cell.contentView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 || section == 2 {
            return CGFloat.leastNormalMagnitude
        } else {
            return 44.0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            let index = indexPath.row - 1

            if indexPath.row == 0 {
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    return CGFloat.leastNormalMagnitude
                }
            } else if indexPath.row > 0 && theCurrentViewModel.arr_selectedFile.count + 1 == indexPath.row {
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    return CGFloat.leastNormalMagnitude
                } else {
                    return 60.0
                }
            } else if indexPath.row > 0 && theCurrentViewModel.arr_selectedFile.count != 0, theCurrentViewModel.arr_selectedFile.indices.contains(index) {
                if theCurrentViewModel.arr_selectedFile[index].isDeleteAttachment {
                    return CGFloat.leastNormalMagnitude
                }
            }
            
            
        } else if indexPath.section == 1 {
            if indexPath.row == 2 {
                if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                    return CGFloat.leastNormalMagnitude
                }
            }
        } else if indexPath.section == 3 {
            if theCurrentViewModel.isLikeSelected && indexPath.row == 0 && theCurrentViewModel.arr_Like.count == 0 {
                return tableView.frame.height
            } else if !theCurrentViewModel.isLikeSelected && indexPath.row == 0 {
                if (theCurrentViewModel.theIdeaAndProblemModel?.status == APIKey.key_archived.lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "fixed".lowercased()) || (theCurrentViewModel.theIdeaAndProblemModel?.status == "completed".lowercased()) {
                    return CGFloat.leastNormalMagnitude
                }
            } else if !theCurrentViewModel.isLikeSelected && indexPath.row == 1 && theCurrentViewModel.arr_Comment.count == 0 {
                return tableView.frame.height
            }
        }
        return UITableView.automaticDimension
    }
    /*
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     let lastSectionIndex = tableView.numberOfSections - 1
     let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
     if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.arr_FocusList != nil {
     let spinner = UIActivityIndicatorView(style: .gray)
     spinner.color = appGreen
     spinner.startAnimating()
     spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
     
     theCurrentView.tableView.tableFooterView = spinner
     theCurrentView.tableView.tableFooterView?.isHidden = false
     } else {
     theCurrentView.tableView.tableFooterView?.isHidden = true
     theCurrentView.tableView.tableFooterView = nil
     }
     }*/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2, indexPath.row > 0 {
            let index = indexPath.row - 1
            
            if theCurrentViewModel.arr_selectedFile[index].fileName.isContainAudioOrVideoFile() {
                self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[index].fileName)
            } else {
                presentWebViewVC(url: theCurrentViewModel.arr_selectedFile[index].fileName, isLocal: !theCurrentViewModel.arr_selectedFile[index].isOldAttachment)
            }
            /*
            if theCurrentViewModel.arr_selectedFile[index].fileType == .audio {
                if !theCurrentViewModel.arr_selectedFile[index].isOldAttachment, let audioData = theCurrentViewModel.arr_selectedFile[index].fileData {
                    if let url = self.setAudioFileToDocumentDirectory(audioData: audioData, audioNameWithExtension: "\(APIKey.key_Audio_name).\(APIKey.key_Extension)") {
                        self.presentAudioPlayer(url: url,isLocalURl: true)
                    }
                } else {
                    if theCurrentViewModel.arr_selectedFile[indexPath.row].fileName.contains(".wav") {
                        let fileName = theCurrentViewModel.arr_selectedFile[indexPath.row].fileName
                        let lastFile = (fileName as NSString).lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3")
                        print("lastFile:=",lastFile)
                        self.downloadAudioFileWebService(strURl: fileName) { [weak self] (fileData) in
                            if let url = self?.setAudioFileToDocumentDirectory(audioData: fileData, audioNameWithExtension: lastFile) {
                                self?.presentAudioPlayer(url: url,isLocalURl: true)
                            }
                        }
                    } else {
                        self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName)
                    }
//                    self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName)
                }
            }*/
        }
    }
    
}
