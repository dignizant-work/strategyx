//
//  IdeaNProblemDetailVC.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import IQAudioRecorderController
import MobileCoreServices

class IdeaNProblemDetailVC: ParentViewController {

    fileprivate lazy var theCurrentView: IdeaNProblemDetailView = { [unowned self] in
        return self.view as! IdeaNProblemDetailView
    }()
    
    internal lazy var theCurrentViewModel: IdeaNProblemDetailViewModel = {
        return IdeaNProblemDetailViewModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentViewModel.showTheEditData()
        theCurrentView.tableView.reloadData()
        theCurrentViewModel.isLikeSelected = true
        theCurrentViewModel.likeListService()
    }
    
    func setTheData(theModel:IdeaAndProblemList?,isForArchive:Bool = false) {
        theCurrentViewModel.theIdeaAndProblemModel = theModel
        theCurrentViewModel.isForArchive = isForArchive
        theCurrentViewModel.isEdit = true
    }
    
    @objc func onheaderButtonLikeOrCommentAction(_ sender:UIButton) {
        print("buttonClick")
        
        if sender.tag == 1 && !theCurrentViewModel.isLikeSelected { // Like
            theCurrentViewModel.isLikeSelected = !theCurrentViewModel.isLikeSelected
            if theCurrentViewModel.arr_Like.count == 0 {
                theCurrentViewModel.likeListService()
            }
        } else if sender.tag == 2 && theCurrentViewModel.isLikeSelected {
            theCurrentViewModel.isLikeSelected = !theCurrentViewModel.isLikeSelected
            if theCurrentViewModel.arr_Comment.count == 0 {
                theCurrentViewModel.commentListService()
            }
        }
        
        /*if sender.tag == 1 && !theCurrentViewModel.isLikeSelected { // Like
            theCurrentViewModel.isLikeSelected = !theCurrentViewModel.isLikeSelected
            if theCurrentViewModel.arr_Like.count == 0 {
                theCurrentViewModel.likeListService()
            }
         }
        if sender.tag == 1 { // Like
//            theCurrentViewModel.likeUnlikeService()
            theCurrentViewModel.isLikeSelected = true
            /*if theCurrentViewModel.arr_Like.count == 0 {
                theCurrentViewModel.likeListService()
            }*/
         } else if sender.tag == 2 && theCurrentViewModel.isLikeSelected {
            theCurrentViewModel.isLikeSelected = false
            if theCurrentViewModel.arr_Comment.count == 0 {
                theCurrentViewModel.commentListService()
            }
        }*/
    }
    
    //MARK:- Redirect To
    func presentDropDownListVC(categorySelection:IdeaNProblemDetailViewModel.categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
//            let arr_company:[String] = theCurrentViewModel.arr_companyList.map({$0.name}).filter({!$0.isEmpty})
//            vc.setTheData(strTitle: "Company",arr:arr_company)
            break
        case .assignTo:
            let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assignee_name}).filter({!$0.isEmpty})
            //          let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            let arr_department:[String] = theCurrentViewModel.arr_departmentList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Depatment",arr:arr_department)
            break
        case .stage:
            vc.setTheData(strTitle: "Stage",arr:theCurrentViewModel.arr_Stage)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToCustomDatePicker(selectionType:IdeaNProblemDetailViewModel.dateSelctionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = theCurrentViewModel.selectedData["duedate"].string, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterOutputType.outPutType4) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .resived:
            if let reviseDate = theCurrentViewModel.selectedData["revisedduedate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outPutType4) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentViewModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirecToAddIdeaAndProblemVC() {
        guard let theModel = theCurrentViewModel.theIdeaAndProblemModel else { return }
        if theModel.type.lowercased() == IdeaAndProblemType.idea.rawValue.lowercased() {
            redirectToAddIdeaVC()
        } else {
            redirectToAddProblemVC()
        }
    }
    
    func redirectToAddIdeaVC() {
        let vc = AddIdeaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theCurrentViewModel.theIdeaAndProblemModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theIdeaModel) in
            self?.theCurrentViewModel.theIdeaAndProblemModel = theIdeaModel
            self?.theCurrentViewModel.updateTheIdeaAndProblemModel()
        }
        self.push(vc: vc)
    }
    
    func redirectToAddProblemVC() {
        let vc = AddProblemVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theCurrentViewModel.theIdeaAndProblemModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theProblemModel) in
            self?.theCurrentViewModel.theIdeaAndProblemModel = theProblemModel
            self?.theCurrentViewModel.updateTheIdeaAndProblemModel()
        }
        self.push(vc: vc)
    }
    
    func presentFileManger() {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        
    }
}

//MARK:- UIDocumentPickerDelegate
extension IdeaNProblemDetailVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    }) {
                        
                    } else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        theCurrentViewModel.reloadAttachmentSecton()
        
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                }) {
                    
                } else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        } else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        theCurrentViewModel.reloadAttachmentSecton()
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension IdeaNProblemDetailVC:IQAudioRecorderViewControllerDelegate {
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")

        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
//                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
//                let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                
                //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
//                self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                
                
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.theCurrentViewModel.reloadAttachmentSecton()
                    }
                    
                }
                
            }
            
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        

    }
}
