//
//  IdeaNProblemDetailView.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeaNProblemDetailView: ViewParentWithoutXIB {

    //MARK:- Variable
    
    @IBOutlet weak var tableView: UITableView!
//    let refreshControl = UIRefreshControl()

    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }
    
    func setupTableView(theDelegate:IdeaNProblemDetailVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
//        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(RisksNoteTVCell.self)
        tableView.registerCellNib(InputDescriptionTVCell.self)
//        tableView.registerCellNib(BottomButtonTVCell.self)
        tableView.registerCellNib(NoDataFoundTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        //        (self.parentContainerViewController() as? IdeasNproblemsVC)?.getFocusListWebService(isRefreshing: true)
        refreshControl.beginRefreshing()

        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }

}
