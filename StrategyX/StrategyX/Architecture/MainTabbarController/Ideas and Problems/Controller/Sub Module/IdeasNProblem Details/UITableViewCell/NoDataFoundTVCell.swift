//
//  NoDataFoundTVCell.swift
//  StrategyX
//
//  Created by Haresh on 14/05/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NoDataFoundTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMessage(strMsg:String) {
        lblTitle.text = strMsg
    }
    
}
