//
//  IdeaNProblemDetailViewModel.swift
//  StrategyX
//
//  Created by Haresh on 18/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class IdeaNProblemDetailViewModel {
    
    //MARK:- Variable
    fileprivate weak var theController:IdeaNProblemDetailVC!
    
    enum categorySelectionType:Int {
        case company             = 2
        case assignTo            = 3
        case department          = 4
        case stage               = 5
    }
    enum dateSelctionType:Int {
        case due                = 2
        case resived            = 3
    }
    
    var isForArchive = false

    var theIdeaAndProblemModel:IdeaAndProblemList? = nil {
        didSet {
            if let controller = theController.navigationController?.viewControllers {
                if let index = controller.firstIndex(where: {($0 as? IdeasNproblemsSubListVC) != nil}) {
                    (controller[index] as? IdeasNproblemsSubListVC)?.theCurrentViewModel.updateIdeaAndProblemListModel(theModel: theIdeaAndProblemModel!)
//                        .updateIdeaAndProblemListModelFromActionSubList(theModel: theIdeaAndProblemModel!)
                } else {
                    if let index = controller.firstIndex(where: {($0 as? IdeasNproblemsVC) != nil}) {
                        (controller[index] as? IdeasNproblemsVC)?.theCurrentViewModel.updateIdeaAndProblemListModelFromActionSubList(theModel: theIdeaAndProblemModel!)
                    }
                    if let index = controller.firstIndex(where: {($0 as? IdeasNProblemsArchivedVC) != nil}) {
                        (controller[index] as? IdeasNProblemsArchivedVC)?.theCurrentViewModel.updateIdeaAndProblemListModelFromActionSubList(theModel: theIdeaAndProblemModel!)
                    }
                }
            }
        }
    }
    
    var isLikeSelected:Bool = false {
        didSet {
//            let _ = theController.view
            if let tableView = (theController.view as? IdeaNProblemDetailView)?.tableView {
                let offset = tableView.contentOffset
                (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = false
                tableView.reloadSections(IndexSet.init(integer: 3), with: .none)
//                tableView.layoutIfNeeded()
                tableView.setContentOffset(offset, animated: false)
                (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
                /*UIView.performWithoutAnimation {
                    
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.02, execute: { [weak self] in
//
//                    })
                }*/
            }
//            (theController.view as? IdeaNProblemDetailView)?.tableView!.reloadSections(IndexSet.init(integer: 3), with: .none)
        }
    }
    
    var likeErrorMsg = ""
    var commentErrorMsg = ""
    
    
    var arr_Stage = ["submitted","accepted","rejected","on_hold","completed"] // idea

    /*Idea :
    submitted
    fixing
    on_hold
    fixed
    
    Problem :
    submitted
    accepted
    rejected
    on_hold
    completed*/
    
    var arr_selectedFile:[AttachmentFiles] = []
    var arr_Like:[INPLikeList] = []
    var arr_Comment:[INPCommentList] = []
    var strAddCommentText = ""
    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","focusname":"","duedate":""])
    var isEdit = false
    
    var isDropDownLoadingAtIndex:(Int,Int) = (-1,-1) // (Section, row)
    var isEditIdeaAndProblemApiLoading = false
    var isCommentApiLoading = false
    
    var selectedDueDate:Date?
    var selectedrevisedDate:Date?
    var isEditLikeApiLoading = false
    var isLikeListApiLoading = false
    var isCommentListApiLoading = false

    
    // getCompany
//    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    
    //MARK:- LifeCycle
    init(theController:IdeaNProblemDetailVC) {
        self.theController = theController
    }
    
    
    func showTheEditData() {
        
        guard let theModel = theIdeaAndProblemModel else { return }
        if theModel.type.lowercased() == IdeaAndProblemType.problem.rawValue.lowercased() {
          arr_Stage = ["submitted","fixing","on_hold","fixed"]
        }
        selectedData["type"].stringValue = theModel.type
        selectedData["title"].stringValue = theModel.title
        selectedData["description"].stringValue = theModel.description_ideaNproblem
        selectedData["createdby"].stringValue = theModel.createdBy
        selectedData["company"].stringValue = theModel.companyName
        selectedData["assignto"].stringValue = theModel.assignedTo
        selectedData["department"].stringValue = theModel.department
        selectedData["stage"].stringValue = theModel.status
        selectedData["submitted"].stringValue = theModel.submitted
        selectedData["duedate"].stringValue = theModel.duedate
        selectedData["revisedduedate"].stringValue = theModel.revisedDate
        selectedData["isPrivate"].intValue = Int(theModel.isPrivate) ?? 0
        selectedDueDate = theModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1)
        
        if let dueDate = selectedDueDate {
            selectedData["duedate"].stringValue = dueDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        selectedrevisedDate = theModel.revisedDate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let revisedDate = selectedrevisedDate {
            selectedData["revisedduedate"].stringValue = revisedDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        if let submitted = theModel.submitted.convertToDate(formate: DateFormatterInputType.inputType1) {
            selectedData["submitted"].stringValue = submitted.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        strSelectedCompanyID = theModel.companyId
        strSelectedUserAssignID = theModel.assigneeId
        strSelectedDepartmentID = theModel.departmentId
        
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById) {
            strSelectedCompanyID = theModel.companyId.isEmpty ? "\(Utility.shared.userData.companyId)" : theModel.companyId
            assignUserListService()
            if strSelectedUserAssignID != "" {
                departmentListService()
            }
        }

    }
    func updateTheIdeaAndProblemModel()  {
        guard let theModel = theIdeaAndProblemModel else { return }
        print("theModel:=",theModel)
        selectedData["title"].stringValue = theModel.title
        selectedData["description"].stringValue = ""
        selectedData["description"].stringValue = theModel.description_ideaNproblem
        selectedData["createdby"].stringValue = theModel.createdBy
        selectedData["company"].stringValue = theModel.companyName
        selectedData["assignto"].stringValue = theModel.assignedTo
        selectedData["department"].stringValue = theModel.department
        selectedData["stage"].stringValue = theModel.status
        selectedData["submitted"].stringValue = theModel.submitted
        selectedData["duedate"].stringValue = theModel.duedate
        selectedData["revisedduedate"].stringValue = theModel.revisedDate
        selectedData["isPrivate"].intValue = Int(theModel.isPrivate) ?? 0
        selectedDueDate = theModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1)
        
        if let dueDate = selectedDueDate {
            selectedData["duedate"].stringValue = dueDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        selectedrevisedDate = theModel.revisedDate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let revisedDate = selectedrevisedDate {
            selectedData["revisedduedate"].stringValue = revisedDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        if let submitted = theModel.submitted.convertToDate(formate: DateFormatterInputType.inputType1) {
            selectedData["submitted"].stringValue = submitted.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        strSelectedCompanyID = theModel.companyId
        strSelectedUserAssignID = theModel.assigneeId
        strSelectedDepartmentID = theModel.departmentId
        arr_selectedFile.removeAll()
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadData()
        }
    }
    func deleteAttachment(index:Int) {
        if arr_selectedFile[index].isOldAttachment {
            arr_selectedFile[index].isDeleteAttachment = true
        } else {
            arr_selectedFile.remove(at: index)
        }
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadSections(IndexSet.init(integer: 2), with: .none)
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
        }
        
//        tabelViewCount = 6 + arr_selectedFile.count
//        theCurrentView.tableView.reloadData()
    }
    
    func updateTableForDropDownStopLoading() {
        if isDropDownLoadingAtIndex.1 == -1 {
            return
        }
        let index = IndexPath(row: isDropDownLoadingAtIndex.1, section: isDropDownLoadingAtIndex.0)
        isDropDownLoadingAtIndex = (-1,-1)
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [index], with: .none)
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
        }
    }
    
    func updateBottomCell() {
//        let index = IndexPath(row: 8, section: 1)
        isEditIdeaAndProblemApiLoading = !isEditIdeaAndProblemApiLoading
        /*let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset

        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [index], with: .none)
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset = contentPoint
            }
        }*/
    }
    func reloadTableView() {
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
        
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadData()
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
        }
    }
    func reloadTableLikeAndCommentSection() {
        isCommentApiLoading = false
        (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = false
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
//        UIView.setAnimationsEnabled(false)
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.beginUpdates()
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadSections(IndexSet.init(integer: 3), with: .none)
            (theController.view as? IdeaNProblemDetailView)?.tableView.endUpdates()
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
            (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.02, execute: { [weak self] in
//
//            })
//            (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
        }
    }
    func reloadAddCommentCell() {
        let index = IndexPath(row: 0, section: 3)
        isCommentApiLoading = !isCommentApiLoading
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
        
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [index], with: .none)
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
        }
    }

    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            /*strSelectedCompanyID = arr_companyList[index].id
            assignUserListService()
            
            selectedData["company"].stringValue = str
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            isDropDownLoadingAtIndex = -1
            UIView.performWithoutAnimation {
                if let index = tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.company}) {
                    self.(theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                }
                if let index = tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.assignTo}) {
                    isDropDownLoadingAtIndex = index
                    self.(theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                }
                if let index = tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.department}) {
                    self.(theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                }
                //                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 4, section: 0),IndexPath.init(row: 5, section: 0),IndexPath.init(row: 6, section: 0)], with: UITableView.RowAnimation.none)
            }*/
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            strSelectedUserAssignID = arr_assignList[index].user_id
            selectedData["assignto"].stringValue = str
            departmentListService()
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            /*
            var isEdit = false
            if let theModel = theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById){
                
                isEdit = true
            }*/
            self.reloadSecton1()
            /*UIView.performWithoutAnimation {
                
                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 4, section: 1)], with: UITableView.RowAnimation.none)
                isDropDownLoadingAtIndex = (1,5)
                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 5, section: 1)], with: UITableView.RowAnimation.none)
//                if isEdit {
//
//                }
                
                //                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 5, section: 0),IndexPath.init(row: 6, section: 0)], with: UITableView.RowAnimation.none)
            }*/
            break
        case .department:
            strSelectedDepartmentID = arr_departmentList[index].id
            selectedData["department"].stringValue = str
            self.reloadSecton1()
           /* UIView.performWithoutAnimation {
                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 5, section: 1)], with: UITableView.RowAnimation.none)
            }*/
            break
        case .stage:
            selectedData["stage"].stringValue = str
            /*UIView.performWithoutAnimation {
                (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 3, section: 1)], with: UITableView.RowAnimation.none)
            }*/
            reloadSecton1()
            break
            
        }
    }
    
    func updateDate(strDate:String,type:dateSelctionType,date:Date) {
        switch type {
        case .due:
            selectedDueDate = date
            selectedData["duedate"].stringValue = strDate
            reloadSecton1()
//            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 6, section: 1)], with: UITableView.RowAnimation.none)
            
        //            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 7, section: 0)], with: UITableView.RowAnimation.none)
        case .resived:
            selectedrevisedDate = date
            selectedData["revisedduedate"].stringValue = strDate
            reloadSecton1()
//            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadRows(at: [IndexPath.init(row: 7, section: 1)], with: UITableView.RowAnimation.none)

            //            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 8, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    func reloadAttachmentSecton() {
        (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = false
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)!.tableView.reloadSections(IndexSet.init(integer: 2), with: .none)
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
            (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
        }
    }
    func reloadSecton1() {
        (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = false
        let contentOffset = (theController.view as? IdeaNProblemDetailView)?.tableView.contentOffset
//        UIView.setAnimationsEnabled(false)
        UIView.performWithoutAnimation {
            (theController.view as? IdeaNProblemDetailView)?.tableView.beginUpdates()
            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadSections(IndexSet.init(integer: 1), with: .none)
            (theController.view as? IdeaNProblemDetailView)?.tableView.endUpdates()
            if let contentPoint = contentOffset {
                (theController.view as? IdeaNProblemDetailView)?.tableView.setContentOffset(contentPoint, animated: false)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01, execute: { [weak self] in
                (self?.theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
            })
            //            (theController.view as? IdeaNProblemDetailView)?.tableView.isScrollEnabled = true
        }
    }
    func validateForm() {
        theController.view.endEditing(true)
        if selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            theController.showAlertAtBottom(message: "Please enter title")
            return
        }
        if strSelectedCompanyID.isEmpty {
            theController.showAlertAtBottom(message: "Please select company")
            return
        }
        
        if selectedData["company"].stringValue.isEmpty {
            theController.showAlertAtBottom(message: "Please select company")
            return
        } else if selectedData["assignto"].stringValue.isEmpty {
            theController.showAlertAtBottom(message: "Please select assign to")
            return
        } else if selectedData["department"].stringValue.isEmpty {
            theController.showAlertAtBottom(message: "Please select department")
            return
        } /*else if selectedData["duedate"].stringValue.isEmpty {
            theController.showAlertAtBottom(message: "Please select due date")
            return
        } */
        /*else if selectedData["revisedduedate"].stringValue.isEmpty {
            theController.showAlertAtBottom(message: "Please select revised date")
            return
        }*/
        
        else if !selectedData["revisedduedate"].stringValue.isEmpty {
            if let dueDate1 = selectedDueDate, let revisedDate1 = selectedrevisedDate,revisedDate1 < dueDate1 {
                theController.showAlertAtBottom(message: "Revised date should be grater then due date")
                return
            }
        }
//        var dueDate = ""
        var revisedDate = ""
        
//        if let dueDate1 = selectedDueDate {
//            dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
//        }
        
        if let revisedDate1 = selectedrevisedDate {
            revisedDate = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
//
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_type:IdeaAndProblemType.idea.rawValue,
                         APIKey.key_remove_file:"",
                         APIKey.key_idea_and_problem_id:theIdeaAndProblemModel?.id ?? "",
                         APIKey.key_title:selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_description:selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_assigned:strSelectedUserAssignID,
                         APIKey.key_department_id:strSelectedDepartmentID,
                         APIKey.key_status:selectedData["stage"].stringValue,
//                         APIKey.key_duedate:dueDate,
                         APIKey.key_is_private:selectedData["isPrivate"].stringValue,
                         APIKey.key_revised_date:revisedDate,
                         APIKey.key_company_id:strSelectedCompanyID] as [String : String]
        
        updateIdeaNProbemWebService(parameter: parameter)
    }
    func validComment() {
        theController.view.endEditing(true)
        if strAddCommentText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            theController.showAlertAtBottom(message: "Please enter comment")
            return
        } else {
           
            let postText = strAddCommentText
            strAddCommentText = ""
            reloadAddCommentCell()
            addCommentService(strComment: postText)
        }
    }
}
//MARK:- Api Call
extension IdeaNProblemDetailViewModel {
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { (list) in
//            self?.arr_companyList = list
            
            }, failed: { [weak self](error) in
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func assignUserListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
            self?.updateTableForDropDownStopLoading()
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateTableForDropDownStopLoading()
        })
    }
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            self?.updateTableForDropDownStopLoading()
            if let obj = self, !obj.isEdit, !obj.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            } else if let theModel = self?.theIdeaAndProblemModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById), list.count > 0 {
                self?.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            }
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateTableForDropDownStopLoading()
        })
    }
    func updateIdeaNProbemWebService(parameter:[String:String]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var voiceCount = 0
        var fileCount = 0

        for i in 0..<arr_selectedFile.count {
            if arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
            } else {

                if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
        var param = parameter
        param[APIKey.key_remove_file] = JSON(removeFile).rawString() ?? ""
//        APIKey.key_remove_file:JSON(removeFile).rawString() ?? "",
        
        self.updateBottomCell()
        self.reloadTableView()
        
        IdeaAndProblemsWebServices.shared.updateIdeaNProblemAttachment(parameter:param, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theModel) in
            self?.isEditIdeaAndProblemApiLoading = false
            self?.reloadTableView()
            self?.theIdeaAndProblemModel = theModel
            self?.updateTheIdeaAndProblemModel()
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell()
                self?.reloadTableView()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func likeListService() {
        isLikeListApiLoading = true
        reloadTableLikeAndCommentSection()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_idea_and_problem_id:theIdeaAndProblemModel?.id ?? ""]
        
        IdeaAndProblemsWebServices.shared.getIdeaAndProblemLikeList(parameter: parameter, success: { [weak self] (list) in
                self?.isLikeListApiLoading = false
                self?.arr_Like = list
                self?.reloadTableLikeAndCommentSection()
            }, failed: { [weak self] (error) in
//                self?.theController.showAlertAtBottom(message: error)
                self?.isLikeListApiLoading = false
                self?.likeErrorMsg = error
                self?.arr_Like = []
                self?.reloadTableLikeAndCommentSection()

//                self?.reloadTableLikeAndCommentSection()
        })
    }
    func commentListService() {
        isCommentListApiLoading = true
        reloadTableLikeAndCommentSection()

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_idea_and_problem_id:theIdeaAndProblemModel?.id ?? ""]
        
        IdeaAndProblemsWebServices.shared.getIdeaAndProblemCommentList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_Comment = list
            self?.isCommentListApiLoading = false
            self?.reloadTableLikeAndCommentSection()
            }, failed: { [weak self] (error) in
//                self?.theController.showAlertAtBottom(message: error)
                self?.isCommentListApiLoading = false
                self?.commentErrorMsg = error
                self?.arr_Comment = []
                self?.reloadTableLikeAndCommentSection()

        })
    }
    
    func addCommentService(strComment:String) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_idea_and_problem_id:theIdeaAndProblemModel?.id ?? "",
                         APIKey.key_comment:strComment]
        let commentCount = theIdeaAndProblemModel?.comment ?? 0
        IdeaAndProblemsWebServices.shared.addINPComment(parameter: parameter, success: { [weak self] (strMsg, list) in
            self?.reloadAddCommentCell()
            self?.arr_Comment.append(list)
            self?.theIdeaAndProblemModel?.comment = commentCount + 1
//
            self?.reloadTableLikeAndCommentSection()
            }, failed: { [weak self] (error) in
                self?.reloadAddCommentCell()
                self?.theController.showAlertAtBottom(message: error)
                //                self?.reloadTableLikeAndCommentSection()
        })
    }
    
    func likeUnlikeService() {
        isEditLikeApiLoading = true
        reloadAttachmentSecton()
//        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_idea_and_problem_id:theIdeaAndProblemModel?.id ?? ""
                         ]
        
        IdeaAndProblemsWebServices.shared.likeUnlike(parameter: parameter, success: { [weak self] (strMsg, list) in
//            Utility.shared.stopActivityIndicator()
            self?.isEditLikeApiLoading = false
            self?.theIdeaAndProblemModel?.isLike = (self?.theIdeaAndProblemModel?.isLike ?? 0) == 0 ? 1 : 0
            self?.reloadAttachmentSecton()
            self?.isLikeSelected = true
//            self?.theController.showAlertAtBottom(message: strMsg)
            self?.theIdeaAndProblemModel?.like = list.like
            self?.likeListService()
//            self?.reloadTableLikeAndCommentSection()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()

//                self?.reloadAddCommentCell()
                self?.isLikeSelected = true
                self?.theController.showAlertAtBottom(message: error)
                //                self?.reloadTableLikeAndCommentSection()
        })
    }
}
