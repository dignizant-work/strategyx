//
//  IdeasNproblemsVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

//MARK:- UITableViewDataSource
extension IdeasNproblemsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentViewModel.arr_IdeaNProblemList == nil || theCurrentViewModel.arr_IdeaNProblemList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentViewModel.arr_IdeaNProblemList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IdeaNProblemListTVCell") as! IdeaNProblemListTVCell
        
        if theCurrentViewModel.arr_IdeaNProblemList != nil {
            cell.updateBackGroundColor(isCompleted: theCurrentViewModel.arr_IdeaNProblemList![indexPath.row].completeActionFlag == 1)
            var isApiLoading = false
            if theCurrentViewModel.apiLoadingAtIdeaNProblemsID.contains(theCurrentViewModel.arr_IdeaNProblemList![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configureIdeaAndProblem(theModel: theCurrentViewModel.arr_IdeaNProblemList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isIdeaAndProblemArchive(strStatus: self?.theCurrentViewModel.arr_IdeaNProblemList?[indexPath.row].status ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentViewModel.arr_IdeaNProblemList![indexPath.row].duedate ?? "", dueDate: self?.theCurrentViewModel.arr_IdeaNProblemList![indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_IdeaNProblemList![indexPath.row].id ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentViewModel.configureDropDown(dropDown: obj.theCurrentViewModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentViewModel.arr_IdeaNProblemList![indexPath.row])
                    obj.theCurrentViewModel.moreDD.show()
                }
            }
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.nextOffset != 0 {
                theCurrentViewModel.ideaAndProblemListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
}

//MARK:- UITableViewDelegate
extension IdeasNproblemsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.arr_IdeaNProblemList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        redirectToIdeasNproblemsSubListVC(theModel:Focus())
        guard let arr_model = theCurrentViewModel.arr_IdeaNProblemList, arr_model.count > 0, !theCurrentViewModel.apiLoadingAtIdeaNProblemsID.contains(arr_model[indexPath.row].id) else { return  }
        redirectToIdeasNproblemsSubListVC(theModel: arr_model[indexPath.row])
    }
}
