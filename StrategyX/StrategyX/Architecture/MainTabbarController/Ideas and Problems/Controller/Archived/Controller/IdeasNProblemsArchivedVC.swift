//
//  IdeasNProblemsArchivedVC.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeasNProblemsArchivedVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView: IdeasNProblemsArchivedView = { [unowned self] in
        return self.view as! IdeasNProblemsArchivedView
    }()
    
    lazy var theCurrentViewModel: IdeasNProblemsArchivedViewModel = {
        return IdeasNProblemsArchivedViewModel.init(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)

    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    
    //MARK:- Redirect To
    func presentCriticalSuccessFilter() {
        let vc = CriticalSuccessFilterVC.init(nibName: "CriticalSuccessFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .ideaAndProblem, isForArchiveINP:true)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        /* var filterData = theCurrentViewModel.filterData
         if theCurrentViewModel.assignToUserFlag == 0 {
         filterData.assigntoID = ""
         filterData.assigntoName = ""
         }
         if filterData.assigntoID == "0" {
         filterData.assigntoID = ""
         filterData.assigntoName = ""
         }*/
        vc.setTheData(filterData: theCurrentViewModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentViewModel.filterData = filterData
            
            var totalFilterCount = 0
            
            //            self?.theCurrentViewModel.nextOffset = 0
            if !filterData.createdByID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.searchByCSFText.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.companyID.trimmed().isEmpty && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if !filterData.assigntoID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            /*if filterData.assigntoID == "0" {
             totalFilterCount -= 1
             totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
             } else if filterData.assigntoID.trimmed().isEmpty {
             self?.theCurrentViewModel.filterData.assigntoID = "0"
             }*/
            if !filterData.departmentID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.searchByReportTitleText.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.reportFrequencyID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.metricID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.stage.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentViewModel.nextOffset = 0
            self?.theCurrentViewModel.arr_IdeaNProblemList?.removeAll()
            self?.theCurrentViewModel.arr_IdeaNProblemList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .ideaAndProblem)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToIdeasNproblemsSubListVC(theModel:IdeaAndProblemList) {
        let vc = IdeasNproblemsSubListVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        vc.setTheData(theModel: theModel, isForArchive: true)
        self.push(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentCriticalSuccessFilter()
    }


}
