//
//  IdeasNProblemsArchivedViewModel.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class IdeasNProblemsArchivedViewModel {
    
    //MARK:-Variable
    fileprivate weak var theController:IdeasNProblemsArchivedVC!

    var arr_IdeaNProblemList:[IdeaAndProblemList]?
    var nextOffset = 0
    var filterData:FilterCreticalFectorsData = FilterCreticalFectorsData()

    var apiLoadingAtIdeaNProblemsID:[String] = []
    let moreDD = DropDown()
    
    //MARK:- LifeCycle
    init(theController:IdeasNProblemsArchivedVC) {
        self.theController = theController
    }
    
    func updateList(list:[IdeaAndProblemList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_IdeaNProblemList?.removeAll()
        }
        nextOffset = offset
        if arr_IdeaNProblemList == nil {
            arr_IdeaNProblemList = []
        }
        list.forEach({ arr_IdeaNProblemList?.append($0) })
        print(arr_IdeaNProblemList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:IdeaAndProblemList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_delete_icon_popup"]
        
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById)  {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.edit.rawValue}) {
                dataSource.remove(at: index) // edit
                dataSourceIcon.remove(at: index)
            }
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = arr_IdeaNProblemList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
//            archiveIdeaAndProblem(strIdeaAndProblemID: model.id)
            return
        case MoreAction.delete.rawValue:
            deleteIdeaAndProblem(strIdeaAndProblemID: model.id)
            return
        case MoreAction.edit.rawValue:
            
            break
        case MoreAction.chart.rawValue:
            theController.presentGraphAction(strChartId: model.id)
            break
        case MoreAction.action.rawValue:
//            theController.redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    
    func deleteIdeaAndProblem(strIdeaAndProblemID: String) {
        theController.showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.apiLoadingAtIdeaNProblemsID.append(strIdeaAndProblemID)
            self?.updateBottomCell(loadingID: strIdeaAndProblemID, isLoadingApi: true)
            self?.changeIdeaAndProblemStatusWebService(strIdeaAndProblemID: strIdeaAndProblemID, isDelete: true)
        }
    }
    
    func updateIdeaAndProblemListModelFromActionSubList(theModel:IdeaAndProblemList) {
        if let index = arr_IdeaNProblemList?.firstIndex(where: {$0.id == theModel.id}) {
            arr_IdeaNProblemList?.remove(at: index)
            arr_IdeaNProblemList?.insert(theModel, at: index)
            (theController.view as? IdeasNproblemsView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = arr_IdeaNProblemList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.id == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = apiLoadingAtIdeaNProblemsID.firstIndex(where: {$0 == loadingID}) {
                apiLoadingAtIdeaNProblemsID.remove(at: index)
            }
            arr_IdeaNProblemList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNProblemsArchivedView)?.tableView.reloadData()
            }
//            (theController.view as? IdeasNProblemsArchivedView)?.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if arr_IdeaNProblemList?.count == 0 {
                setBackground(strMsg: "Idea and problem not available")
            }
        } else {
            if !isLoadingApi && !isRemove {
                if let index = apiLoadingAtIdeaNProblemsID.firstIndex(where: {$0 == loadingID}) {
                    apiLoadingAtIdeaNProblemsID.remove(at: index)
                }
            }
            let contentOffset = (theController.view as? IdeasNProblemsArchivedView)?.tableView.contentOffset
            UIView.performWithoutAnimation {
                (theController.view as? IdeasNProblemsArchivedView)?.tableView.reloadRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
                if let contentPoint = contentOffset {
                    (theController.view as? IdeasNProblemsArchivedView)?.tableView.contentOffset = contentPoint
                }
            }
        }
    }
    
    func setBackground(strMsg:String) {
        if arr_IdeaNProblemList == nil || arr_IdeaNProblemList?.count == 0 {
            (theController.view as? IdeasNProblemsArchivedView)?.tableView.backgroundView = (theController.view as? IdeasNProblemsArchivedView)?.tableView.backGroundMessageView(strMsg: strMsg)
            (theController.view as? IdeasNProblemsArchivedView)?.tableView.reloadData()
        }
    }
}
//MARK:- Api Call
extension IdeasNProblemsArchivedViewModel {
    func ideaAndProblemListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:nextOffset,APIKey.key_grid_type:APIKey.key_archived,APIKey.key_department_id:filterData.departmentID,APIKey.key_assign_to:filterData.assigntoID,APIKey.key_company_id:filterData.companyID,APIKey.key_created_by:filterData.createdByID,APIKey.key_stage:filterData.stage.lowercased() == "all".lowercased() ? "" : filterData.stage] as [String : Any]
        
        IdeaAndProblemsWebServices.shared.getIdeaAndProblemList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            //            (self?.theController.view as? IdeasNproblemsView)?.updateFilterCountText(strCount: "1")
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            (self?.theController.view as? IdeasNProblemsArchivedView)?.refreshControl.endRefreshing()
            (self?.theController.view as? IdeasNProblemsArchivedView)?.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.setBackground(strMsg: error)
                (self?.theController.view as? IdeasNProblemsArchivedView)?.refreshControl.endRefreshing()
        })
    }
    
    func changeIdeaAndProblemStatusWebService(strIdeaAndProblemID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strIdeaAndProblemID]]).rawString() else {
            updateBottomCell(loadingID: strIdeaAndProblemID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_status_for_ideas_and_problems,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strIdeaAndProblemID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strIdeaAndProblemID)
        })
    }
}

