//
//  IdeasNproblemsVC.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeasNproblemsVC: ParentViewController {
    //MARK:- Variable
    fileprivate lazy var theCurrentView: IdeasNproblemsView = { [unowned self] in
        return self.view as! IdeasNproblemsView
    }()
    
    lazy var theCurrentViewModel: IdeasNproblemsViewModel = {
       return IdeasNproblemsViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentViewModel.configure(dropDown: theCurrentViewModel.titleTypeDD, view: theCurrentView.viewTitle)
        
        reInitData()
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    
    func reInitData() {
        setCompanyData()
        theCurrentViewModel.nextOffset = 0
        theCurrentView.tableView.backgroundView = nil
        theCurrentViewModel.arr_IdeaNProblemList?.removeAll()
        theCurrentViewModel.arr_IdeaNProblemList = nil
        theCurrentView.tableView.reloadData()
        theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)

    }
    
    func refreshApiCall() {
        theCurrentViewModel.nextOffset = 0
        theCurrentViewModel.arr_IdeaNProblemList?.removeAll()
        theCurrentViewModel.arr_IdeaNProblemList = nil
        theCurrentView.tableView.backgroundView = nil
        theCurrentView.tableView.reloadData()
        theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
        /*switch theCurrentViewModel.selectedTitleType {
        case .ideaNProblems:
            
            break
        case .ideas:
            theCurrentViewModel.arr_IdeaNProblemList?.removeAll()
            theCurrentViewModel.arr_IdeaNProblemList = nil
            theCurrentViewModel.nextOffset = 0
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
            break
        case .problems:
            theCurrentViewModel.arr_IdeaNProblemList?.removeAll()
            theCurrentViewModel.arr_IdeaNProblemList = nil
            theCurrentView.tableView.backgroundView = nil
            theCurrentView.tableView.reloadData()
            theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
            break
        }*/
    }
    
    //MARK:- Navigate to
    func presentCriticalSuccessFilter() {
        let vc = CriticalSuccessFilterVC.init(nibName: "CriticalSuccessFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .ideaAndProblem)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
       /* var filterData = theCurrentViewModel.filterData
        if theCurrentViewModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }*/
        vc.setTheData(filterData: theCurrentViewModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentViewModel.filterData = filterData
            
            var totalFilterCount = 0
            
            //            self?.theCurrentViewModel.nextOffset = 0
            if !filterData.createdByID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.searchByCSFText.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.companyID.trimmed().isEmpty && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if !filterData.assigntoID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            /*if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.trimmed().isEmpty {
                self?.theCurrentViewModel.filterData.assigntoID = "0"
            }*/
            if !filterData.departmentID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.searchByReportTitleText.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.reportFrequencyID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.metricID.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if !filterData.stage.trimmed().isEmpty {
                totalFilterCount += 1
            }
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.refreshApiCall()
        }
        
        AppDelegate.shared.presentOnWindow(vc: vc)
        
    }
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setUpChartId(strChartId: strChartId)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .ideaAndProblem)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToIdeasNProblemsArchivedVC() {
        let vc = IdeasNProblemsArchivedVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        self.push(vc: vc)
    }
    func redirectToIdeasNproblemsSubListVC(theModel:IdeaAndProblemList) {
        let vc = IdeasNproblemsSubListVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        vc.setTheData(theModel: theModel)
        self.push(vc: vc)
    }
    func redirectToIdeasNProblemsDetailsVC(theModel:IdeaAndProblemList) {
        theModel.notesCount = 0
        theCurrentViewModel.updateIdeaAndProblemListModelFromActionSubList(theModel: theModel)
        let vc = IdeaNProblemDetailVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        vc.setTheData(theModel: theModel, isForArchive: false)
        self.push(vc: vc)
    }
    func redirectToAddIdeaVC(theModel:IdeaAndProblemList) {
        let vc = AddIdeaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theIdeaModel) in
            self?.theCurrentViewModel.updateIdeaAndProblemModelAndUI(theModel: theIdeaModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToAddProblemVC(theModel:IdeaAndProblemList) {
        let vc = AddProblemVC.instantiateFromAppStoryboard(appStoryboard: .main)
        let imgBG = theCurrentView.asImage()
        vc.setTheData(imgBG: imgBG)
        vc.setTheEditData(theModel: theModel, isForEdit: true)
        vc.handlerUpdateModel = { [weak self] (theProblemModel) in
            self?.theCurrentViewModel.updateIdeaAndProblemModelAndUI(theModel: theProblemModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToAddActionScreen(theModel:IdeaAndProblemList) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
//        vc.setAddFocusData(theFocusId: theModel.id, theFocusName: theModel.focusTitle)
        if theModel.type.lowercased() == IdeaAndProblemType.idea.rawValue.lowercased() {
            vc.setAddIdeaData(theIdeaId: theModel.id, theIdeaName: theModel.title, theDueDate: theModel.duedate,theDueDateColor: theModel.duedateColor, isStatusArchive: UserDefault.shared.isIdeaAndProblemArchive(strStatus: theModel.status))
        } else {
            vc.setAddProblemData(theProblemId: theModel.id, theProblemName: theModel.title, theDueDate: theModel.duedate,theDueDateColor: theModel.duedateColor, isStatusArchive: UserDefault.shared.isIdeaAndProblemArchive(strStatus: theModel.status))
        }
        vc.handlerEditDueDateChanged = { [weak self] (strDueDate, strDueDateColor) in
            if let index = self?.theCurrentViewModel.arr_IdeaNProblemList?.firstIndex(where: {$0.id == theModel.id}) {
                let model = self!.theCurrentViewModel.arr_IdeaNProblemList![index]
                model.duedate = strDueDate
                model.duedateColor = strDueDateColor
                self?.theCurrentViewModel.arr_IdeaNProblemList?.remove(at: index)
                self?.theCurrentViewModel.arr_IdeaNProblemList?.insert(model, at: index)
                self?.theCurrentViewModel.updateBottomCell(loadingID: model.id)
            }
        }
        vc.handlerEditAction = { [weak self, unowned theModel] (theActionDetailModel) in
            let model = theModel
            if let index = self?.theCurrentViewModel.arr_IdeaNProblemList?.firstIndex(where: {$0.id == theModel.id}) {
                model.completeActionFlag = 0
                model.totalAction = model.totalAction + 1
                self?.theCurrentViewModel.arr_IdeaNProblemList?.remove(at: index)
                self?.theCurrentViewModel.arr_IdeaNProblemList?.insert(model, at: index)
                self?.theCurrentViewModel.updateBottomCell(loadingID: model.id)
            }
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnDropDownAction(_ sender: Any) {
        theCurrentViewModel.titleTypeDD.show()
        theCurrentViewModel.rotateDropdownArrow(at: CGFloat.pi)
    }
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToIdeasNProblemsArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentCriticalSuccessFilter()
    }
}

class NavigationIdeasNProblemsVC: UINavigationController {
    
}
