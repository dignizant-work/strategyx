//
//  IdeasNproblemsView.swift
//  StrategyX
//
//  Created by Haresh on 17/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class IdeasNproblemsView: ViewParentWithoutXIB {

    //MARK:- Variable
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var viewArchived: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    @IBOutlet weak var img_DropDownArrow: UIImageView!

    
//    let refreshControl = UIRefreshControl()
    
    //MARK:- LifeCycle
    func setupLayout() {
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:IdeasNproblemsVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.registerCellNib(RisksSubListTVCell.self)
        tableView.registerCellNib(IdeaNProblemListTVCell.self)

        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? IdeasNproblemsVC)?.theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
}
