//
//  ReportCategoriesVC-UITableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 19/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

//MARK:- UITableViewDataSource
extension ReportCategoriesVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_categoriesViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: theCurrentViewModel.cellID)
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: theCurrentViewModel.cellID)
        }
        if let lbl = cell?.viewWithTag(997) as? UILabel {
            lbl.text = theCurrentViewModel.arr_categoriesViewModel[indexPath.row].strItemName
        }
        return cell!
    }
    
}

//MARK:- UITableViewDelegate
extension ReportCategoriesVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 || indexPath.row == 1 {
            redirectToReportVC(selectionType: indexPath.row == 0 ? .overdueItems : .dueDateAdjusted)
            return
        }
        if theCurrentViewModel.arr_categoriesViewModel[indexPath.row].strItemName.localizedLowercase == "Created VS Completed".localizedLowercase {
            redirectToCreatedVSCompletedVC()
            return
        }
        if theCurrentViewModel.arr_categoriesViewModel[indexPath.row].strItemName.localizedLowercase == "Priority Overview".localizedLowercase {
            redirectToPriorityOverviewVC()
            return
        }
        
    }
}
