//
//  ReportCategoriesVC.swift
//  StrategyX
//
//  Created by Haresh on 19/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportCategoriesVC: ParentViewController {

    //MARK:- Variable
    internal lazy var theCurrentView:ReportCategoriesView = { [unowned self] in
       return self.view as! ReportCategoriesView
    }()
    
    internal lazy var theCurrentViewModel:ReportCategoriesViewModel = {
        return ReportCategoriesViewModel.init()
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
    }
    
    //MARK:- Redirection
    func redirectToReportVC(selectionType:ReportsSelectionSection) {
        let vc = ReportsVC.instantiateFromAppStoryboard(appStoryboard: .reports)
        vc.setTheData(selectionType:selectionType)
        self.push(vc: vc)
    }
    
    func redirectToCreatedVSCompletedVC() {
        let vc = CreatedVSCompletedVC.instantiateFromAppStoryboard(appStoryboard: .reports)
        self.push(vc: vc)
    }
    
    
    
    func redirectToPriorityOverviewVC() {
        let vc = PriorityOverviewVC.instantiateFromAppStoryboard(appStoryboard: .reports)
        self.push(vc: vc)
    }
}

