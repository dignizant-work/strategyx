//
//  ReportCategoriesViewModel.swift
//  StrategyX
//
//  Created by Haresh on 19/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportCategoriesViewModel {

    //MARK:- Variable
    var cellID = "cell"
    var arr_categoriesViewModel:[ReportCategoriesTVCellViewModel] = []
    
    
    //MARK:- LifeCycle
    init() {
        arr_categoriesViewModel = ["Overdue Items","Due Date Adjusted","Created VS Completed","Priority Overview"].map({ return ReportCategoriesTVCellViewModel.init(strItemName: $0)})
       
        
        
    }
    
}
