//
//  ReportCategoriesView.swift
//  StrategyX
//
//  Created by Haresh on 19/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportCategoriesView: ViewParentWithoutXIB {

     //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    

    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }
    func setupTableView(theDelegate:ReportCategoriesVC)  {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = 60
        tableView.separatorStyle = .none
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        
    }
    
    
}
