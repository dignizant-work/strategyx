//
//  ReportsView.swift
//  StrategyX
//
//  Created by Jaydeep on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts

class ReportsView: ViewParentWithoutXIB {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var activityForMainLineChatByPerson: UIActivityIndicatorView!
    @IBOutlet weak var viewMainLineChartByPerson: UIView!
    @IBOutlet weak var activityForMainHorizontalBarChart: UIActivityIndicatorView!
    @IBOutlet weak var viewMainHorizontalBarChart: UIView!
    @IBOutlet weak var activityForMainLineChart: UIActivityIndicatorView!
    @IBOutlet weak var viewMainLineChart: UIView!
    @IBOutlet weak var btnSelectionResources: UIButton!
    @IBOutlet weak var btnSelectionDepartment: UIButton!
    @IBOutlet weak var lblResources: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblYAxisLineChartByPerson: UILabel!
    @IBOutlet weak var lblLineChartByPersonHeaderTitle: UILabel!
    @IBOutlet weak var lblYAxisHorizontalBarChart: UILabel!
    @IBOutlet weak var lblHorizontalBarChartHeaderTitle: UILabel!
    @IBOutlet weak var lblSelectionDaysType: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblViewLineChartHeaderTitle: UILabel!
    @IBOutlet weak var lblYAxisLineChart: UILabel!
    @IBOutlet weak var viewLineChartByPerson: LineChartView!
    @IBOutlet weak var viewLineChart: LineChartView!
    @IBOutlet weak var lblXAxiaLineChartByPerson: UILabel!
    @IBOutlet weak var lblXAxisLineChart: UILabel!
    @IBOutlet weak var viewFilterFirst: UIView!
    @IBOutlet weak var viewFilterSecond: UIView!
    @IBOutlet weak var lblSecondFilterCount: UILabel!
    @IBOutlet weak var lblFirstFilterCount: UILabel!
    @IBOutlet weak var verticalBarChart: CombinedChartView!
    @IBOutlet weak var HorizotalBarChart: HorizontalBarChartView!
    @IBOutlet weak var btnFirstDailyOutlet: UIButton!
    @IBOutlet weak var btnFirstMontlyOutlet: UIButton!
    @IBOutlet weak var btnFirstWeeklyOutlet: UIButton!
    @IBOutlet weak var btnFirstCustomDateOutlet: UIButton!
    @IBOutlet weak var btnSecondDailyOutlet: UIButton!
    @IBOutlet weak var btnSecondMontlyOutlet: UIButton!
    @IBOutlet weak var btnSecondWeeklyOutlet: UIButton!
    @IBOutlet weak var btnSecondCustomDateOutlet: UIButton!
    @IBOutlet weak var constraintLblFirstFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var constraintLblSecondFilterCountWidth: NSLayoutConstraint! // 18.0

    
    func setupLayout() {
        updateFirstFilterCountText(strCount: "")
        updateSecondFilterCountText(strCount: "")
        lblXAxisLineChart.rotate(degrees: 270)
        lblXAxisLineChart.frame = CGRect(x:5, y:viewLineChart.center.y - 110, width:20, height:220)
        lblXAxiaLineChartByPerson.rotate(degrees: 270)
        lblXAxiaLineChartByPerson.frame = CGRect(x:5, y:viewLineChartByPerson.center.y - 110, width:20, height:220)
        
    }
    
    //MARK:- Setup Vertical Chart View
    
   /* func setupChartview(chartView:BarChartView)
    {
        //chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.drawRoundedBarEnabled = true
        chartView.chartDescription?.text = ""
        chartView.legend.enabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = R15
        xAxis.granularity = 1
       // xAxis.labelCount = 7
     //   xAxis.valueFormatter = DayAxisValueFormatter(chart: chartView, array: <#[String]#>)
        xAxis.labelRotationAngle = -45
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelTextColor = appPlaceHolder        
        
     /*   let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        leftAxisFormatter.negativeSuffix = " $"
        leftAxisFormatter.positiveSuffix = " $"*/
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = R15
        leftAxis.labelTextColor = appPlaceHolder
       // leftAxis.labelCount = 8
       // leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
       // leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = false
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        
        self.updateChartData()
    }
    
    func updateChartData() {
        
        self.setDataCount(20, range: 30)
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        let start = 1
        
        let yVals = (start..<start+count+1).map { (i) -> BarChartDataEntry in
            let mult = range + 1
            let val = Double(arc4random_uniform(mult))
           // if arc4random_uniform(100) < 25 {
                return BarChartDataEntry(x: Double(i), y: val)
           // }
        }
        
        var set1: BarChartDataSet! = nil
        if let set = verticalBarChart.data?.dataSets.first as? BarChartDataSet {
            set1 = set
            set1.values = yVals
            // set1.barRoundingCorners = [.topLeft,.topRight]
            
            verticalBarChart.data?.notifyDataChanged()
            verticalBarChart.notifyDataSetChanged()
        } else {
            set1 = BarChartDataSet(values: yVals, label: "")
            //set1.colors = ChartColorTemplates.material()
            set1.colors = [appGreen]
            set1.highlightEnabled = false
           // set1.drawValuesEnabled = true
            set1.barRoundingCorners = [.topLeft,.topRight]
            let data = BarChartData(dataSet: set1)
            data.setValueFont(R10)
            data.barWidth = 0.8
            
            data.setValueTextColor(.white)
            verticalBarChart.data = data
        }
        
        //        chartView.setNeedsDisplay()
    }*/
    
    func setupLinearLineChart(lineChartView:LineChartView,theDelegate:ReportsVC) {
        lineChartView.delegate = theDelegate
        
        lineChartView.chartDescription?.enabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.pinchZoomEnabled = true
        lineChartView.scaleXEnabled = true
        lineChartView.doubleTapToZoomEnabled = true
        lineChartView.autoScaleMinMaxEnabled = false
        lineChartView.clipsToBounds = false
        lineChartView.clipValuesToContentEnabled = false
        //viewLineChart.setVisibleXRangeMaximum(3)
        lineChartView.extraRightOffset = 0
        lineChartView.extraLeftOffset = 0
        lineChartView.legend.enabled = false
        lineChartView.extraTopOffset = 20
        
        // viewLineChart.drawBordersEnabled = false
        
        let l = lineChartView.legend
        l.form = .line
        l.font = R12
        l.textColor = appPlaceHolder
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.formLineWidth  = 10
        l.drawInside = false
        
        let xAxis = lineChartView.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelRotationAngle = 280
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.yOffset = 20
        xAxis.axisLineColor = appSepratorColor
        xAxis.labelCount = 5
        
        
        let yAxis = lineChartView.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = true
        yAxis.labelTextColor = appPlaceHolder
        yAxis.drawAxisLineEnabled = false
        yAxis.xOffset = 20
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        
        let rightAxis = lineChartView.rightAxis
        rightAxis.enabled = false
    }
    
    func setDataCountForLinearLineChart(lineChartView:LineChartView,arrChart:[ReportChart]) {
        
        /*let chartData = ChartList()
         chartData.date = ""
         chartData.totalAction = 0
         chartData.totalCompleteAction = 0
         var arrChartList = arrChart
         arrChartList.insert(chartData, at: 0)*/
        
        var yVals1:[ChartDataEntry] = []
        for i in 0..<arrChart.count{
            yVals1.append(ChartDataEntry(x: Double(i), y: Double(arrChart[i].totalAction)))
        }
        
//        var yVals2:[ChartDataEntry] = []
//        for i in 0..<arrChart.count{
//            yVals2.append(ChartDataEntry()
//        }
        
//        let set1 = LineChartDataSet(values: yVals1, label: "DataSet 1")
//        set1.axisDependency = .left
//        set1.setColor(appGraphCyanColor)
//        set1.lineWidth = 2
//        set1.circleRadius = 0
//        set1.fillAlpha = 65/255
//        set1.fillColor = UIColor.blue
//        set1.highlightEnabled = false
//        set1.mode = .linear
        
        let set2 = LineChartDataSet(values: yVals1, label: "Audit Dates")
        set2.axisDependency = .left
        set2.setColor(appGreen)
        set2.lineWidth = 2
        set2.circleRadius = 0
        set2.fillAlpha = 65/255
        set2.fillColor = .red
        set2.highlightEnabled = false
        set2.mode = .linear
        
        let data = LineChartData(dataSets: [set2])
        data.setValueTextColor(.clear)
        data.setValueFont(R14)
        
        var str : [String] = []
        for i in 0..<arrChart.count{
            str.append("  " + arrChart[i].date)
        }
        print(str)
        lineChartView.xAxis.valueFormatter = DayAxisValueFormatter(chart: lineChartView, array: str)
        lineChartView.data = data
    }
    /*
    func setChartData() {
        
        let data = CombinedChartData()
        data.lineData = generateLineData()
        data.barData = generateBarData()
        
        let xAxis = verticalBarChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = R15
        xAxis.granularity = 1
        xAxis.labelRotationAngle = 30
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelTextColor = appPlaceHolder
        
        let leftAxis = verticalBarChart.leftAxis
        leftAxis.labelFont = R15
        leftAxis.labelTextColor = appPlaceHolder
        leftAxis.labelPosition = .outsideChart
        leftAxis.axisMinimum = 0
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = false
        
        let rightAxis = verticalBarChart.rightAxis
        rightAxis.enabled = false
        
        
        verticalBarChart.highlightPerTapEnabled = false
        verticalBarChart.legend.enabled = false
        verticalBarChart.drawValueAboveBarEnabled = false
        verticalBarChart.drawRoundedBarEnabled = true
        
        verticalBarChart.data = data
    }
    
    func generateLineData() -> LineChartData {
        let entries = (0..<12).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i) + 0.5, y: Double(arc4random_uniform(15) + 5))
        }
        
        let set = LineChartDataSet(values: entries, label: "")
        set.setColor(appGraphCyanColor)
        set.lineWidth = 2
        set.drawCircleHoleEnabled = false
        set.drawCirclesEnabled = false
        set.fillColor = clear
        set.mode = .linear
        set.drawValuesEnabled = false
        set.valueFont = .systemFont(ofSize: 10)
        set.valueTextColor = .clear
        set.drawVerticalHighlightIndicatorEnabled = false
        set.drawHorizontalHighlightIndicatorEnabled = false       
        
        return LineChartData(dataSet: set)
    }
    
    func generateBarData() -> BarChartData {
        let start = 1
        
        let yVals = (start..<start+12+1).map { (i) -> BarChartDataEntry in
            let mult = 20 + 1
            let val = Double(arc4random_uniform(UInt32(mult)))
            return BarChartDataEntry(x: Double(i), y: val)
           
        }
        
        var set1: BarChartDataSet! = nil
        set1 = BarChartDataSet(values: yVals, label: "")
        set1.colors = [appGreen]
        set1.highlightEnabled = false
        set1.barRoundingCorners = [.topLeft,.topRight]
        set1.drawValuesEnabled = true
        
        let data = BarChartData(dataSet: set1)
        data.setValueFont(R10)
        data.barWidth = 0.8
        data.setValueTextColor(.white)
        
        return data
        
    }*/
    
    //MARK:- Setup Horizontal BarChart

    
    func setupChartview(chartView:HorizontalBarChartView)
    {
        //chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        chartView.drawRoundedBarEnabled = true
        chartView.scaleXEnabled = false
        chartView.scaleYEnabled = false
        chartView.legend.enabled = false
        chartView.extraBottomOffset = 10
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.axisLineColor = appSepratorColor
        //xAxis.xOffset = 5
        
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = false
        yAxis.labelTextColor = appPlaceHolder
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        //yAxis.xOffset = 20
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = true
        
        //chartView.fitBars = false
        
//        self.setDataCountForHorizontalBarChart()
    }
    
    func setDataCountForHorizontalBarChart(list:[ReportChartByDepartment]) {
        
        var yVals = [BarChartDataEntry]()
        var strName = [String]()
        var values = [String]()
        
        for index in 0..<list.count {
            yVals.append(BarChartDataEntry(x: Double(index), y: Double(list[index].totalActions)))
            strName.append(list[index].title)
            values.append("\(list[index].totalActions)")
        }
        /*
        yVals.append(BarChartDataEntry(x: Double(1), y: Double(20)))
        yVals.append(BarChartDataEntry(x: Double(2), y: Double(20)))
        yVals.append(BarChartDataEntry(x: Double(3), y: Double(20)))
        yVals.append(BarChartDataEntry(x: Double(4), y: Double(20)))
        yVals.append(BarChartDataEntry(x: Double(5), y: Double(20)))
         */
        
        /*yVals.append(BarChartDataEntry(x: Double(0), yValues: [0,0,0,20], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(1), yValues: [0,0,0,20], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(2), yValues: [0,0,0,20], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(3), yValues: [0,0,0,20], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,0,0,20], icon: UIImage(named: "icon")))
        yVals.append(BarChartDataEntry(x: Double(4), yValues: [0,0,0,20], icon: UIImage(named: "icon")))*/
        
        
        var set1: BarChartDataSet! = nil
        if let set = HorizotalBarChart.data?.dataSets.first as? BarChartDataSet {
            set1 = set
            set1.values = yVals
            set1.highlightEnabled = false
        } else {
            set1 = BarChartDataSet(values: yVals, label: "")
            set1.colors = [appGreen]
            set1.drawValuesEnabled = true
            set1.barRoundingCorners = [.topRight,.bottomRight]
            set1.highlightEnabled = false
        }
        
        
        /*strName.append("Corporate")
        strName.append("John")
        strName.append("Sam")
        strName.append("Robert")
        strName.append("Sandra")
        strName.append("bandra")*/
        print("Array Name \(strName)")
//        HorizotalBarChart.drawValueAboveBarEnabled = true
        HorizotalBarChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: strName)
//        HorizotalBarChart.leftAxis.valueFormatter = IndexAxisValueFormatter(values: values)
        HorizotalBarChart.xAxis.labelCount = strName.count
        let data = BarChartData(dataSet: set1)
        data.setValueFont(R10)
        data.barWidth = 0.7
        data.setValueTextColor(UIColor.black)
        HorizotalBarChart.data = data
        
    }
    
    func setTextAccordingtoType(type:ReportsSelectionSection) {
        switch type {
        case .overdueItems:
            lblHeaderTitle.text = "Overdue Items"
            lblViewLineChartHeaderTitle.text = "Overdue Items - By Department"
            lblXAxisLineChart.text = "No. of Overdue Items"
            lblYAxisLineChart.text = "Audit Dates"
            lblHorizontalBarChartHeaderTitle.text = lblViewLineChartHeaderTitle.text
            lblYAxisHorizontalBarChart.text = "No. of Overdue Items"
            lblLineChartByPersonHeaderTitle.text = "Overdue Items - By Person"
            lblXAxiaLineChartByPerson.text = "No. of Overdue Items"
            lblYAxisLineChartByPerson.text = "Audit Dates"
            break
        case .dueDateAdjusted:
            lblHeaderTitle.text = "Due Date Adjusted"
            lblViewLineChartHeaderTitle.text = "Due Date Adjusted - By Department"
            lblXAxisLineChart.text = "No. of times due date adjusted"
            lblYAxisLineChart.text = "Dates"
            lblHorizontalBarChartHeaderTitle.text = lblViewLineChartHeaderTitle.text
            lblYAxisHorizontalBarChart.text = "No. of times due date adjusted"
            lblLineChartByPersonHeaderTitle.text = "Due Date Adjusted - By Person"
            lblXAxiaLineChartByPerson.text = "No. of times due date adjusted"
            lblYAxisLineChartByPerson.text = "Dates"
            break
        }
        
    }
    //MARK:- SetColor
    func resetCustomDate(button:UIButton?) {
        button?.borderColor = 6
        button?.setFontColor = 5
        button?.setTitle("Custom Range", for: .normal)
    }
    func resetFirstColor() {
        [btnFirstDailyOutlet,btnFirstMontlyOutlet,btnFirstWeeklyOutlet].forEach { (btn) in
            btn?.borderColor = 6
            btn?.setFontColor = 5
        }
    }
    
    func resetSecondColor() {
        [btnSecondDailyOutlet,btnSecondMontlyOutlet,btnSecondWeeklyOutlet].forEach { (btn) in
            btn?.borderColor = 6
            btn?.setFontColor = 5
        }
    }
    
    func selectedState(_ sender:UIButton) {
        sender.borderColor = 3
        sender.setFontColor = 3
    }
    
    //MARK:- view Update
    func updateFirstFilterCountText(strCount:String) {
        lblFirstFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFirstFilterCountWidth.constant = 0.0
        } else {
            constraintLblFirstFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
    func updateSecondFilterCountText(strCount:String) {
        lblSecondFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblSecondFilterCountWidth.constant = 0.0
        } else {
            constraintLblSecondFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
    func viewMainLineChart(isHidden:Bool) {
        if isHidden {
            self.viewMainLineChart.alpha = 0
            self.activityForMainLineChart.startAnimating()
        } else {
            self.activityForMainLineChart.stopAnimating()
            UIView.animate(withDuration: 0.8) { [weak self] in
                self?.viewMainLineChart.alpha = 1
            }
        }
    }
    func viewMainHorizontalBarChart(isHidden:Bool) {
        if isHidden {
            self.viewMainHorizontalBarChart.alpha = 0
            self.activityForMainHorizontalBarChart.startAnimating()
        } else {
            self.activityForMainHorizontalBarChart.stopAnimating()
            UIView.animate(withDuration: 0.8) { [weak self] in
                self?.viewMainHorizontalBarChart.alpha = 1
            }
        }
    }
    func viewMainLineChartByPerson(isHidden:Bool) {
        if isHidden {
            self.viewMainLineChartByPerson.alpha = 0
            self.activityForMainLineChatByPerson.startAnimating()
        } else {
            self.activityForMainLineChatByPerson.stopAnimating()
            UIView.animate(withDuration: 0.8) { [weak self] in
                self?.viewMainLineChartByPerson.alpha = 1
            }
        }
    }
    
    func showLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(true, allignment: .right)
    }
    
    func stopLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(false)
    }
}
