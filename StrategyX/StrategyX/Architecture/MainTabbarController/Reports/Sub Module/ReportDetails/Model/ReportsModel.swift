//
//  ReportsModel.swift
//  StrategyX
//
//  Created by Jaydeep on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportsModel {
    
    //MARK:- Variable
    enum categorySelectionType:Int {
        case daysCategory        = 2
        case assignTo            = 3
        case department          = 4
    }
    
    weak var theController:ReportsVC!
    var selectionType = ReportsSelectionSection.dueDateAdjusted
    var reportTypeForLineChart = CalendarFrequencyType.daily
    var reportTypeForLineChartByPerson = CalendarFrequencyType.daily
    var periodType = ""
    
    var customDateRangeForHorizontalBarChart:(Date?,Date?) = (nil,nil)
    var customDateRangeForLineChartByPerson:(Date?,Date?) = (nil,nil)
    
    var strSelectedCompanyID:String = Utility.shared.userData.companyId.isEmpty ? "0" : Utility.shared.userData.companyId
    var arr_Days = ["All","Today","Yesterday","This Week","Last Week","This Month","Last Month","This Year"]
    
    // getAssign
    var arr_assignList:[ReportAssignUser] = []
    var strSelectedUserAssignID:String = ""
    
    // Department
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""

    //NonCompliance
    var arr_nonComplianceChartList = [ReportChart]()
    var arr_nonComplianceChartListByPerson = [ReportChart]()
    
    //NonComplianceByDepartment
    var arr_nonComplianceByDepartmentChartList = [ReportChartByDepartment]()
    
    var completionHandlerAssignUserApi:(String) -> Void = {_ in}
    var completionHandlerDepartmentApi:(String) -> Void = {_ in}
    var completionHandlerNonComplianceChartApi:(String) -> Void = {_ in}
    var completionHandlerNonComplianceChartByPersonApi:(String) -> Void = {_ in}
    var completionHandlerNonComplianceByDepartmentChartApi:(String) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    init(theController:ReportsVC) {
        self.theController = theController
        updateDepartmentList(list: [])
        updateAssignList(list: [])
    }
    
    func updateAssignList(list:[ReportAssignUser]) {
        let user = ReportAssignUser()
        user.userName = "All Resources"
        user.departmentId = ""
        user.id = ""
        arr_assignList = [user]
        list.forEach({arr_assignList.append($0)})
    }
    
    func updateDepartmentList(list:[Department]) {
        let department = Department()
        department.name = "All Departments"
        department.id = ""
        arr_departmentList = [department]
        list.forEach({arr_departmentList.append($0)})
    }
    
    func updateReportType(index:Int, isForLineChartByPerson:Bool) {
        var type:CalendarFrequencyType?
        
        switch index {
        case 0:
            type = CalendarFrequencyType.daily
            break
        case 1:
            type = CalendarFrequencyType.weekly
            break
        case 2:
            type = CalendarFrequencyType.monthly
            break
        case 3:
            break
        default:
            type = CalendarFrequencyType.daily
            break
        }
        
        if let reportType = type, index != 3 {
            if isForLineChartByPerson {
                reportTypeForLineChartByPerson = reportType
            } else {
                reportTypeForLineChart = reportType
            }
        }
        
    }

}
//MARK:- Api Call
extension ReportsModel {
    func assignUserListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:strSelectedCompanyID]
        if !strSelectedDepartmentID.isEmpty {
            parameter[APIKey.key_department] = strSelectedDepartmentID
        }
        ReportsWebServices.shared.getReportUserAssignList(parameter: parameter, success: { [weak self] (list) in
            self?.updateAssignList(list: list)

            self?.completionHandlerAssignUserApi("")
            }, failed: { [weak self] (error) in
                self?.completionHandlerAssignUserApi(error)
                self?.theController.showAlertAtBottom(message: error)
//                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
        
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        ReportsWebServices.shared.getReportsDepartment(parameter: parameter, success: { [weak self] (list) in
            self?.updateDepartmentList(list: list)
            self?.completionHandlerDepartmentApi("")

            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerDepartmentApi(error)
        })
    }
    
    func getNonComplianceChartList() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_report_type:reportTypeForLineChart.rawValue]
        ReportsWebServices.shared.getNonComplianceChartList(parameter: parameter, success: { [weak self] (list) in
                self?.arr_nonComplianceChartList = list
                self?.completionHandlerNonComplianceChartApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceChartList = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceChartApi(error)
        })
    }
    
    func getNonComplianceByDepartmentChartList() {
        var strStartDate = ""
        var strEndDate = ""
        if let startDate = customDateRangeForHorizontalBarChart.0, let endDate = customDateRangeForHorizontalBarChart.1 {
            strStartDate = startDate.string(withFormat: DateFormatterInputType.inputType2)
            strEndDate = endDate.string(withFormat: DateFormatterInputType.inputType2)
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_period:periodType,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_start_date:strStartDate,
                         APIKey.key_end_date:strEndDate]
        ReportsWebServices.shared.getNonComplianceByDepartmentChartList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_nonComplianceByDepartmentChartList = list.reversed()
            self?.completionHandlerNonComplianceByDepartmentChartApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceByDepartmentChartList = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceByDepartmentChartApi(error)
        })
    }
    
    
    func getNonComplianceChartListByPerson() {
        var strStartDate = ""
        var strEndDate = ""
        if let startDate = customDateRangeForLineChartByPerson.0, let endDate = customDateRangeForLineChartByPerson.1 {
            strStartDate = startDate.string(withFormat: DateFormatterInputType.inputType2)
            strEndDate = endDate.string(withFormat: DateFormatterInputType.inputType2)
        }
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_report_type:reportTypeForLineChartByPerson.rawValue,
                         APIKey.key_resource:strSelectedUserAssignID,
                         APIKey.key_start_date:strStartDate,
                         APIKey.key_end_date:strEndDate]
        ReportsWebServices.shared.getNonComplianceChartList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_nonComplianceChartListByPerson = list
            self?.completionHandlerNonComplianceChartByPersonApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceChartListByPerson = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceChartByPersonApi(error)
        })
    }
    
    func getDueDateAdjustedChartList() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_report_type:reportTypeForLineChart.rawValue]
        ReportsWebServices.shared.getDueDateAdjustedChartList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_nonComplianceChartList = list
            self?.completionHandlerNonComplianceChartApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceChartList = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceChartApi(error)
        })
    }
    
    func getDueDateAdjustedByDepartmentChartList() {
        var strStartDate = ""
        var strEndDate = ""
        if let startDate = customDateRangeForHorizontalBarChart.0, let endDate = customDateRangeForHorizontalBarChart.1 {
            strStartDate = startDate.string(withFormat: DateFormatterInputType.inputType2)
            strEndDate = endDate.string(withFormat: DateFormatterInputType.inputType2)
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_period:periodType,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_start_date:strStartDate,
                         APIKey.key_end_date:strEndDate]
        ReportsWebServices.shared.getDueDateAdjustedBydepartmentChartList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_nonComplianceByDepartmentChartList = list.reversed()
            self?.completionHandlerNonComplianceByDepartmentChartApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceByDepartmentChartList = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceByDepartmentChartApi(error)
        })
    }
    
    
    func getDueDateAdjustedChartListByPerson() {
        var strStartDate = ""
        var strEndDate = ""
        if let startDate = customDateRangeForLineChartByPerson.0, let endDate = customDateRangeForLineChartByPerson.1 {
            strStartDate = startDate.string(withFormat: DateFormatterInputType.inputType2)
            strEndDate = endDate.string(withFormat: DateFormatterInputType.inputType2)
        }
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_department:strSelectedDepartmentID,
                         APIKey.key_report_type:reportTypeForLineChartByPerson.rawValue,
                         APIKey.key_resource:strSelectedUserAssignID,
                         APIKey.key_start_date:strStartDate,
                         APIKey.key_end_date:strEndDate]
        ReportsWebServices.shared.getDueDateAdjustedChartList(parameter: parameter, success: { [weak self] (list) in
            self?.arr_nonComplianceChartListByPerson = list
            self?.completionHandlerNonComplianceChartByPersonApi("")
            }, failed: { [weak self] (error) in
                self?.arr_nonComplianceChartListByPerson = []
                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerNonComplianceChartByPersonApi(error)
        })
    }
    
}

