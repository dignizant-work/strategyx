//
//  ReportsVC.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts

class ReportsVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ReportsView = { [unowned self] in
        return self.view as! ReportsView
    }()
    
    fileprivate lazy var theCurrentModel:ReportsModel = {
        return ReportsModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        theCurrentModel.completionHandlerDepartmentApi = { [weak self] (error) in
            self?.theCurrentView.stopLoader(self?.theCurrentView.btnSelectionDepartment)
            
        }
        theCurrentModel.completionHandlerAssignUserApi = { [weak self] (error) in
            self?.theCurrentView.stopLoader(self?.theCurrentView.btnSelectionResources)
        }
        theCurrentModel.completionHandlerNonComplianceChartApi = { [weak self] (error) in
            if let obj = self {
                self?.theCurrentView.setDataCountForLinearLineChart(lineChartView: obj.theCurrentView.viewLineChart, arrChart: obj.theCurrentModel.arr_nonComplianceChartList)
            }
            
            self?.theCurrentView.viewMainLineChart(isHidden: false)
        }
        
        theCurrentModel.completionHandlerNonComplianceByDepartmentChartApi = { [weak self] (error) in
            if let obj = self {
                self?.theCurrentView.setDataCountForHorizontalBarChart(list: obj.theCurrentModel.arr_nonComplianceByDepartmentChartList)
            }
            self?.theCurrentView.viewMainHorizontalBarChart(isHidden: false)
        }
        
        theCurrentModel.completionHandlerNonComplianceChartByPersonApi = { [weak self] (error) in
            if let obj = self {
                self?.theCurrentView.setDataCountForLinearLineChart(lineChartView: obj.theCurrentView.viewLineChartByPerson, arrChart: obj.theCurrentModel.arr_nonComplianceChartListByPerson)
            }
            
            self?.theCurrentView.viewMainLineChartByPerson(isHidden: false)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setTextAccordingtoType(type: theCurrentModel.selectionType)
        theCurrentView.setupLinearLineChart(lineChartView: theCurrentView.viewLineChart, theDelegate: self)
        theCurrentView.setupLinearLineChart(lineChartView: theCurrentView.viewLineChartByPerson, theDelegate: self)
        theCurrentView.setupChartview(chartView: theCurrentView.HorizotalBarChart)
        
        onBtnFirstLineChartDateSectionAction(theCurrentView.btnFirstWeeklyOutlet)
        onBtnSecondLineChartDateSectionAction(theCurrentView.btnSecondWeeklyOutlet)

        theCurrentView.showLoader(theCurrentView.btnSelectionDepartment)
        theCurrentModel.departmentListService()
        theCurrentView.showLoader(theCurrentView.btnSelectionResources)
        theCurrentModel.assignUserListService()
        
        
        updateDropDownData(type: .daysCategory, str: theCurrentModel.arr_Days[4], index: 4)
        
//        updateLineChart()
//        updateHorizontalBarChart()
//        updateLineChartByPerson()
    }

    func setTheData(selectionType:ReportsSelectionSection) {
        theCurrentModel.selectionType = selectionType
    }

    
    func updateDropDownData(type:ReportsModel.categorySelectionType,str:String, index:Int)  {
        switch type {
        case .daysCategory:
            theCurrentView.lblSelectionDaysType.text = str
            theCurrentModel.periodType = str
            updateHorizontalBarChart()
            break
        case .department:
            theCurrentView.lblDepartment.text = str
            theCurrentModel.strSelectedDepartmentID = theCurrentModel.arr_departmentList[index].id
            theCurrentView.showLoader(theCurrentView.btnSelectionResources)
            theCurrentModel.assignUserListService()
            updateLineChart()
            updateHorizontalBarChart()
            updateLineChartByPerson()
            break
        case .assignTo:
            theCurrentView.lblResources.text = str
            theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].id
            updateLineChartByPerson()
            break
        }
    }
    
    func updateLineChart() {
        if theCurrentModel.selectionType == .overdueItems {
            theCurrentModel.getNonComplianceChartList()
        } else {
            theCurrentModel.getDueDateAdjustedChartList()
        }
        theCurrentView.viewMainLineChart(isHidden: true)
    }
    
    func updateHorizontalBarChart() {
        if theCurrentModel.selectionType == .overdueItems {
            theCurrentModel.getNonComplianceByDepartmentChartList()
        } else {
            theCurrentModel.getDueDateAdjustedByDepartmentChartList()
        }
        theCurrentView.viewMainHorizontalBarChart(isHidden: true)
    }
    
    func updateLineChartByPerson() {
        if theCurrentModel.selectionType == .overdueItems {
            theCurrentModel.getNonComplianceChartListByPerson()
        } else {
            theCurrentModel.getDueDateAdjustedChartListByPerson()
        }
        theCurrentView.viewMainLineChartByPerson(isHidden: true)
    }
    
}

//MARK:- Present View

extension ReportsVC {
    func presentCalenderAction(reportTypeDate:ReportsSelectionBy) {
        let vc = CalenderViewVC.init(nibName: "CalenderViewVC", bundle: nil)
       // let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        if reportTypeDate == .department {
            vc.setSelectedDateRange(firstDate: theCurrentModel.customDateRangeForHorizontalBarChart.0, lastDate: theCurrentModel.customDateRangeForHorizontalBarChart.1)
        } else {
            vc.setSelectedDateRange(firstDate: theCurrentModel.customDateRangeForLineChartByPerson.0, lastDate: theCurrentModel.customDateRangeForLineChartByPerson.1)
        }
        
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelectedDates = { [weak self] (dates) in
            if let startDate = dates.first, let lastDate = dates.last {
                if reportTypeDate == .department {
                    self?.theCurrentView.selectedState(self?.theCurrentView.btnFirstCustomDateOutlet ?? UIButton())
                    self?.theCurrentModel.customDateRangeForHorizontalBarChart = (startDate,lastDate)
                    self?.theCurrentView.btnFirstCustomDateOutlet.setTitle("\(startDate.string(withFormat: DateFormatterOutputType.outputType2))\n\(lastDate.string(withFormat: DateFormatterOutputType.outputType2))", for: .normal)
                    self?.updateHorizontalBarChart()
                } else {
                    self?.theCurrentView.selectedState(self?.theCurrentView.btnSecondCustomDateOutlet ?? UIButton())
                    self?.theCurrentModel.customDateRangeForLineChartByPerson = (startDate,lastDate)
                    self?.theCurrentView.btnSecondCustomDateOutlet.setTitle("\(startDate.string(withFormat: DateFormatterOutputType.outputType2))\n\(lastDate.string(withFormat: DateFormatterOutputType.outputType2))", for: .normal)
                    self?.updateLineChartByPerson()
                }
            }
            
        }
        self.present(vc, animated: false, completion: nil)
    }
    
    func presentActionFilter(reportBy:ReportsSelectionBy) {
        let vc = ReportFilterVC.init(nibName: "ReportFilterVC", bundle: nil)
        var point = theCurrentView.viewFilterFirst.convert(CGPoint.zero, to: self.view.superview)
        if reportBy == .person {
            point = theCurrentView.viewFilterSecond.convert(CGPoint.zero, to: self.view.superview)
        }
        point.y += 12
        print("point:=",point)
        vc.setTopConstraint(top: point.y, filterType: .reports, reportByType: reportBy)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentDropDownListVC(type:ReportsModel.categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch type {
        case .daysCategory:
            let arrDays:[String] = theCurrentModel.arr_Days
            vc.setTheData(strTitle: "",arr:arrDays)
            break
        case .department:
            let arrDepartment:[String] = theCurrentModel.arr_departmentList.map({$0.name})
            vc.setTheData(strTitle: "Department",arr:arrDepartment)
            break
        case .assignTo:
            let arrAssignedTo:[String] = theCurrentModel.arr_assignList.map({$0.userName})
            vc.setTheData(strTitle: "Resources",arr:arrAssignedTo)
            break
        }
        
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(type: type, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }

}

//MARK:- Action Zone
extension ReportsVC {
    @IBAction func onBtnDepartmentAction(_ sender:UIButton) {
        presentDropDownListVC(type: .department)
    }
    
    @IBAction func onBtnResourcesAction(_ sender:UIButton) {
        presentDropDownListVC(type: .assignTo)
    }
    
    @IBAction func btnFirstFilterAction(_ sender:UIButton) {
        presentActionFilter(reportBy: .department)
    }
    
    @IBAction func btnSecondFilterAction(_ sender:UIButton) {
        presentActionFilter(reportBy: .person)
    }
    
    @IBAction func onBtnFirstLineChartDateSectionAction(_ sender:UIButton) {
        theCurrentView.resetFirstColor()
        theCurrentView.selectedState(sender)
        theCurrentModel.updateReportType(index: sender.tag, isForLineChartByPerson: false)
        updateLineChart()
    }
    
    @IBAction func onBtnSecondLineChartDateSectionAction(_ sender:UIButton) {
        theCurrentView.resetSecondColor()
        theCurrentView.selectedState(sender)
        theCurrentModel.updateReportType(index: sender.tag, isForLineChartByPerson: true)
        updateLineChartByPerson()
    }
    
    @IBAction func btnCustomDateAction(_ sender:UIButton) {
        var type = ReportsSelectionBy.department
        if sender == theCurrentView.btnSecondCustomDateOutlet {
            type = ReportsSelectionBy.person
        }
        presentCalenderAction(reportTypeDate: type)
    }
    
    @IBAction func btnCustomDateRefreshAction(_ sender:UIButton) {
        if sender.tag == 1 {
            if theCurrentModel.customDateRangeForHorizontalBarChart.0 == nil && theCurrentModel.customDateRangeForHorizontalBarChart.1 == nil {
                return
            }
            theCurrentView.resetCustomDate(button: theCurrentView.btnFirstCustomDateOutlet)
            theCurrentModel.customDateRangeForHorizontalBarChart = (nil,nil)
            updateHorizontalBarChart()
        } else {
            if theCurrentModel.customDateRangeForLineChartByPerson.0 == nil && theCurrentModel.customDateRangeForLineChartByPerson.1 == nil {
                return
            }
            theCurrentView.resetCustomDate(button: theCurrentView.btnSecondCustomDateOutlet)
            theCurrentModel.customDateRangeForLineChartByPerson = (nil,nil)
            updateLineChartByPerson()
        }
    }
    
    @IBAction func onBtnSelectionDayTypeAction(_ sender:UIButton) {
        presentDropDownListVC(type: .daysCategory)
    }
    
    @IBAction func onBtnBackAction(_ sender:UIButton) {
        self.backAction(isAnimation: false)
    }
    
}

//MARK:- ChartViewDelegate
extension ReportsVC : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected")
    }
}

class NavigationReportsVC: UINavigationController {
    
}

