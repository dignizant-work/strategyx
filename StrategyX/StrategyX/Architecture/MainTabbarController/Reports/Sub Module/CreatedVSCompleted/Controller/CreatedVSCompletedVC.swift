//
//  CreatedVSCompletedVC.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CreatedVSCompletedVC: ParentViewController {

    internal lazy var theCurrentView:CreatedVSCompletedView = { [unowned self] in
        return self.view as! CreatedVSCompletedView
    }()
    
    internal lazy var theCurrentViewModel:CreatedVSCompletedViewModel = {
       return CreatedVSCompletedViewModel()
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        theCurrentViewModel.completionHandlerCVSCApi = { [weak self] (error, isSetCount)  in
            self?.theCurrentView.refreshControl.endRefreshing()
            if isSetCount {
                self?.theCurrentView.updateFilterCountText(strCount: "1")
            }
            if !error.isEmpty {
                self?.setBackground(strMsg: error)
            }
            self?.theCurrentView.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getCreatedVSCompletedData()
    }
    func getCreatedVSCompletedData(isRefreshing:Bool = false) {
        theCurrentViewModel.getCreatedVSCompletedWebservice(isRefreshing: isRefreshing)
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentViewModel.arr_createdVsCompletedCellViewModel == nil || theCurrentViewModel.arr_createdVsCompletedCellViewModel?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }

    //MARK:- RedirectTo
    func presentActionReportFilterCategory() {
        let vc = ReportFilterCategoryVC.init(nibName: "ReportFilterCategoryVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .cVScReport)

        var filterData = theCurrentViewModel.filterData
        if theCurrentViewModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: theCurrentViewModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentViewModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentViewModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentViewModel.filterData.assigntoID = "0"
            }
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            
            if filterData.reportType != "" && filterData.reportType != "-2" {
                totalFilterCount += 1
            }
            
            if filterData.period != "" {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentViewModel.arr_createdVsCompletedCellViewModel?.removeAll()
            self?.theCurrentViewModel.arr_createdVsCompletedCellViewModel = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getCreatedVSCompletedData(isRefreshing: true)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionReportFilterCategory()
       
    }
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
}
