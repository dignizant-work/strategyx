//
//  CreatedVSCompletedVC-UITableView+Extenstion.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:- UITableViewDataSource
extension CreatedVSCompletedVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentViewModel.arr_createdVsCompletedCellViewModel == nil || theCurrentViewModel.arr_createdVsCompletedCellViewModel?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentViewModel.arr_createdVsCompletedCellViewModel?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: theCurrentViewModel.cellID) as! CreatedVSCompletedTVCell
        if theCurrentViewModel.arr_createdVsCompletedCellViewModel != nil {
            var isApiLoading = false
            if theCurrentViewModel.apiLoadingAtCVSCID.contains(theCurrentViewModel.arr_createdVsCompletedCellViewModel![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configure(theViewModel:theCurrentViewModel.arr_createdVsCompletedCellViewModel![indexPath.row] , isApiLoading: isApiLoading)
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.nextOffset != 0 {
                theCurrentViewModel.getCreatedVSCompletedWebservice()
            }
        } else {
            cell.startAnimation()
        }
        return cell
        
    }
    
}

//MARK:- UITableViewDelegate
extension CreatedVSCompletedVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.arr_createdVsCompletedCellViewModel != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let arr_model = theCurrentViewModel.arr_createdVsCompletedCellViewModel, arr_model.count > 0, !theCurrentViewModel.apiLoadingAtCVSCID.contains(arr_model[indexPath.row].id) else { return  }
        let notification = Notification()
        notification.relatedId = arr_model[indexPath.row].relatedId
        notification.relatedTo = arr_model[indexPath.row].relatedTo
        self.redirectToModule(theModel: notification)
        
//        getFocusDetailWebService(strFocusID: arr_model[indexPath.row].id, isEdit: false)
        
    }
}
