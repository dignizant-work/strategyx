//
//  CreatedVSCompletedViewModel.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CreatedVSCompletedViewModel {

    //MARK:- Variable
    var cellID = "CreatedVSCompletedTVCell"
    var arr_createdVsCompletedCellViewModel:[CreatedVSCompleted]?
    
    var nextOffset = 0
    var apiLoadingAtCVSCID:[String] = []
    var filterData = ReportFilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)
    var completionHandlerCVSCApi:(String, Bool) -> Void = {_,_  in}
    var assignToUserFlag = -1

    //MARK:- Lifecycle
    init() {
        
        
    }
    
    func updateList(list:[CreatedVSCompleted], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_createdVsCompletedCellViewModel?.removeAll()
        }
        nextOffset = offset
        if arr_createdVsCompletedCellViewModel == nil {
            arr_createdVsCompletedCellViewModel = []
        }
        list.forEach({ arr_createdVsCompletedCellViewModel?.append($0) })
        print(arr_createdVsCompletedCellViewModel!)
    }
    
    
}

//MARK:- Api Call
extension CreatedVSCompletedViewModel {
   func getCreatedVSCompletedWebservice(isRefreshing:Bool = false) {
    if isRefreshing {
        nextOffset = 0
    }
    var parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:nextOffset,APIKey.key_assign_to:filterData.assigntoID,APIKey.key_company_id:filterData.companyID] as [String : Any]
        if !filterData.period.isEmpty && filterData.period.lowercased() != "Custom".lowercased() {
            parameter[APIKey.key_custom_rangefilter] = filterData.period
        }
        if let start = filterData.customRangeDate.0, let last = filterData.customRangeDate.1 {
            parameter[APIKey.key_custom_rangefilter] = ""
            parameter[APIKey.key_custom_range] = start.string(withFormat: DateFormatterOutputType.outputType13) + "-" + last.string(withFormat: DateFormatterOutputType.outputType13)
        }
        if !filterData.departmentID.isEmpty {
            parameter[APIKey.key_department_id] = filterData.departmentID
        }
    
        if !filterData.reportType.isEmpty && (filterData.reportType.lowercased() != "All".lowercased() || (filterData.reportType.lowercased() != "-1".lowercased()) || (filterData.reportType.lowercased() != "-2".lowercased())) {
            parameter[APIKey.key_type] = filterData.reportType
        }
        ReportsWebServices.shared.getCreatedVSCreatedList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            var setCount = false
            
            if assignToUserFlag == 1 && (self?.nextOffset ?? -1) == 0 { // Need to set filter count
//                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.filterData.assigntoID = Utility.shared.userData.id
                self?.assignToUserFlag = assignToUserFlag
                setCount = true
            }
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.completionHandlerCVSCApi("", setCount)
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
//                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerCVSCApi(error, false)
        })
    }
}
