//
//  CreatedVSCompletedTVCell.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CreatedVSCompletedTVCell: UITableViewCell {
    
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30
    @IBOutlet weak var lblTypeTitle: UILabel!

    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotalDays: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hideAnimation() {
        [viewType,img_Profile,lblTitle,lblTotalDays,lblDays,lblTypeTitle].forEach({ $0?.hideSkeleton() })
    }
    
    func startAnimation() {
        img_Profile.isHidden = false
        lblTypeTitle.text = " "
        viewType.backgroundColor = white
        [viewType,img_Profile,lblTitle,lblTotalDays,lblDays,lblTypeTitle].forEach({ $0.showAnimatedSkeleton() })
//        lblTitle.text = " "
//        lblTotalDays.text = " "
    }
    
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configure(theViewModel:CreatedVSCompleted, isApiLoading: Bool) {
        hideAnimation()
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblTypeTitle.text = ""
        lblProfile.text = theViewModel.assignedTo.acronym()
        if theViewModel.assignedToUserImage.isEmpty && theViewModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theViewModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theViewModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theViewModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theViewModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        updateProfileView(strAssignedID: theViewModel.assigned)
        lblTitle.text = theViewModel.title
        lblTotalDays.text = theViewModel.totalDays
        lblTypeTitle.text = Utility.shared.setColorBGAccordingToActions(type: theViewModel.type).1
        
//        lblTypeTitle.text = theViewModel.typeText
        lblTypeTitle.rotate(degrees: 270)
        lblTypeTitle.frame = CGRect(x:0, y:0, width:15, height:viewType.frame.size.height)
        viewType.backgroundColor = ColorType().setColor(index: Utility.shared.setColorBGAccordingToActions(type: theViewModel.type).0)
        lblDays.text = (Int(theViewModel.totalDays) ?? 0) < 2 ? "DAY" : "DAYS"
    }
    
    func updateProfileView(strAssignedID:String) {
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID:strAssignedID) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
    }
    
}
