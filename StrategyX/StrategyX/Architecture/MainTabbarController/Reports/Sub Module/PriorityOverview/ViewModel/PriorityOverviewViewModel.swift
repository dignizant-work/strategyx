//
//  PriorityOverviewViewModel.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PriorityOverviewViewModel {

    //MARK:- Variable
    var cellID = "PriorityOverviewTVCell"
//    var arr_PriorityCellViewModel = [String]()
    var arr_PriorityCellViewModel:[PriorityOverview]?

    var nextOffset = 0

    var filterData = ReportFilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)
    var apiLoadingAtPrioriyID:[String] = []
    var completionHandlerPrioriyApi:(String,PriorityOverviewCount, Bool) -> Void = {_,_,_  in}
    var completionHandlerChangeDateApi:(String,String,ActionModel.dateSelectionType,ChangeDate?) -> Void = {_,_,_,_  in}
    var assignToUserFlag = -1

    
    //MARK:- Lifecycle
    init() {
        
    }
    
    func updateList(list:[PriorityOverview], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_PriorityCellViewModel?.removeAll()
        }
        nextOffset = offset
        if arr_PriorityCellViewModel == nil {
            arr_PriorityCellViewModel = []
        }
        list.forEach({ arr_PriorityCellViewModel?.append($0) })
        print(arr_PriorityCellViewModel!)
    }
    
}

//MARK:- Api Call
extension PriorityOverviewViewModel {
    func getPriorityOverViewWebservice(isRefreshing:Bool = false) {
        if isRefreshing {
            nextOffset = 0
        }
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_page_offset:nextOffset,APIKey.key_department_id:filterData.departmentID,APIKey.key_assign_to:filterData.assigntoID,APIKey.key_company_id:filterData.companyID,APIKey.key_custom_rangefilter:filterData.period] as [String : Any]
        if !filterData.reportType.isEmpty && (filterData.reportType.lowercased() != "All".lowercased() && (filterData.reportType.lowercased() != "-1".lowercased()) && (filterData.reportType.lowercased() != "-2".lowercased())) {
            parameter[APIKey.key_type] = filterData.reportType
        }
        if let start = filterData.customRangeDate.0, let last = filterData.customRangeDate.1 {
            parameter[APIKey.key_custom_rangefilter] = ""
            parameter[APIKey.key_custom_range] = start.string(withFormat: DateFormatterOutputType.outputType13) + "-" + last.string(withFormat: DateFormatterOutputType.outputType13)
        }
        ReportsWebServices.shared.getPriorityOverviewList(parameter: parameter, success: { [weak self] (list,countModel, nextOffset, assignToUserFlag) in
            var setCount = false
            
            if assignToUserFlag == 1 && (self?.nextOffset ?? -1) == 0 { // Need to set filter count
                //                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.filterData.assigntoID = Utility.shared.userData.id
                self?.assignToUserFlag = assignToUserFlag
                setCount = true
            }
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.completionHandlerPrioriyApi("",countModel,setCount)
            }, failed: { [weak self] (error) in
                self?.updateList(list: [], offset: 0, isRefreshing: true)
                //                self?.theController.showAlertAtBottom(message: error)
                self?.completionHandlerPrioriyApi(error,PriorityOverviewCount(),false)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        var relatedToAction = ""
        
        if let theModel = arr_PriorityCellViewModel?.first(where: {$0.id == strActionLogID}) {
            relatedToAction = theModel.relatedTo
            switch relatedToAction {
            case RedirectToModule.actionLog.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
                break
            case RedirectToModule.strategyGoal.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_strategy_goals
                break
            case RedirectToModule.risk.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_risks
                break
            case RedirectToModule.tacticalProject.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_tactical_projects
                break
            case RedirectToModule.focuses.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_focuses
                break
            case RedirectToModule.successFactor.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
                break
            case RedirectToModule.ideaNproblem.rawValue:
                parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
                break
            
            default:
                break
            }
        }
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        
        if relatedToAction.isEmpty {
            completionHandlerChangeDateApi("",strActionLogID,dateType,nil)
            return
        }
        
        
        
//         updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.completionHandlerChangeDateApi("",strActionLogID,dateType,theModel)
//            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.completionHandlerChangeDateApi(error,strActionLogID,dateType,nil)

//                self?.showAlertAtBottom(message: error)
//                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
}
