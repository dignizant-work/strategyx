//
//  PriorityOverviewTVCell.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PriorityOverviewTVCell: UITableViewCell {
    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var widthBtnMore: NSLayoutConstraint!
    @IBOutlet weak var lblTypeTitle: UILabel!

    var handleDueDateAction:() -> Void = {}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func hideAnimation() {
        [lblTitle,lblDate,viewType,lblTypeTitle].forEach({ $0?.hideSkeleton() })
    }
    
    func startAnimation() {
        [lblTitle,lblDate,viewType,lblTypeTitle].forEach({ $0.showAnimatedSkeleton() })
        lblTitle.text = " "
        lblTypeTitle.text = ""
        viewType.backgroundColor = white
        moreButton(isHidden: true)
    }
    
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    func configure(theViewModel:PriorityOverview, isApiLoading: Bool) {
        hideAnimation()
        self.contentView.isUserInteractionEnabled = !isApiLoading
        moreButton(isHidden: !isApiLoading)
        btnMore.loadingIndicator(isApiLoading)
        lblProfile.text = theViewModel.assignedTo.acronym()
        if theViewModel.assignedToUserImage.isEmpty && theViewModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theViewModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theViewModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theViewModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theViewModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        updateProfileView(strAssignedID: theViewModel.assigned)
        
        lblTitle.text = theViewModel.title
        var Subtitle = ""
        let attributeString = NSMutableAttributedString()
        if let dateDue = theViewModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1) {
            Subtitle = dateDue.string(withFormat: DateFormatterOutputType.outputType2) + "\n" + dateDue.string(withFormat: DateFormatterOutputType.outputType14)
        }else{
            Subtitle = "No Due Date"
        }
        let attributeStringWork =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R10,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theViewModel.duedateColor)])
        attributeString.append(attributeStringWork)
        lblDate.attributedText = attributeString
        lblTypeTitle.text = Utility.shared.setColorBGAccordingToActions(type: theViewModel.type).1
        lblTypeTitle.rotate(degrees: 270)
        lblTypeTitle.frame = CGRect(x:0, y:0, width:15, height:viewType.frame.size.height)
        viewType.backgroundColor = ColorType().setColor(index: Utility.shared.setColorBGAccordingToActions(type: theViewModel.type).0)
        //        lblDays.text = (Int(theViewModel.totalDays) ?? 0) < 2 ? "DAY" : "DAYS"
    }
    
    func updateProfileView(strAssignedID:String) {
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID:strAssignedID) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
    }
    func moreButton(isHidden:Bool) {
        widthBtnMore.constant = isHidden ? 0 : 30
        btnMore.isHidden = isHidden
    }
    //MARK:- IBAction
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        handleDueDateAction()
    }
    
}
