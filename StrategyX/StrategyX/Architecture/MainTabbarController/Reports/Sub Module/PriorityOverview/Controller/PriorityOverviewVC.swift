//
//  PriorityOverviewVC.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PriorityOverviewVC: ParentViewController {

    internal lazy var theCurrentView:PriorityOverviewView = { [unowned self] in
        return self.view as! PriorityOverviewView
    }()
    
    internal lazy var theCurrentViewModel:PriorityOverviewViewModel = {
       return PriorityOverviewViewModel()
    }()
    

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        theCurrentViewModel.completionHandlerPrioriyApi = { [weak self] (error,countModel, isSetCount)  in
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.updateActionsCount(theModel: countModel)
            if isSetCount {
                self?.theCurrentView.updateFilterCountText(strCount: "1")
            }
            if !error.isEmpty {
                self?.setBackground(strMsg: error)
            }
            self?.theCurrentView.tableView.reloadData()
        }
        theCurrentViewModel.completionHandlerChangeDateApi = { [weak self] (error, strActionID,dateType,theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionID, dateType: dateType, theModel: theModel, isLoadingApi: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        getPriorityOverViewData()
    }
    func getPriorityOverViewData(isRefreshing:Bool = false) {
        theCurrentViewModel.getPriorityOverViewWebservice(isRefreshing: isRefreshing)
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentViewModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentViewModel.arr_PriorityCellViewModel == nil || theCurrentViewModel.arr_PriorityCellViewModel?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        updateDateAndReloadCell(strActionLogID: strActionID, dateType: type, theModel: nil, isLoadingApi: true)
        theCurrentViewModel.changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
        
    }
    
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentViewModel.arr_PriorityCellViewModel?.firstIndex(where: {$0.id == strActionLogID }) {
            if isLoadingApi {
                theCurrentViewModel.apiLoadingAtPrioriyID.append(strActionLogID)
            } else {
                if let index = theCurrentViewModel.apiLoadingAtPrioriyID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentViewModel.apiLoadingAtPrioriyID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentViewModel.arr_PriorityCellViewModel![index].revisedColor = model.revisedColor
                        theCurrentViewModel.arr_PriorityCellViewModel![index].duedateColor = model.revisedColor
                        theCurrentViewModel.arr_PriorityCellViewModel![index].revisedDate = model.revisedDate
                        theCurrentViewModel.arr_PriorityCellViewModel![index].duedate = model.revisedDate
                        break
                    case .workDate:
//                        theCurrentViewModel.arr_PriorityCellViewModel![index].workdateColor = model.workdateColor
                        //                        theCurrentViewModel.arr_PriorityCellViewModel![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }

    //MARK:- RedirectTo
    func presentActionReportFilterCategory() {
        let vc = ReportFilterCategoryVC.init(nibName: "ReportFilterCategoryVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .overViewReport)
        
        var filterData = theCurrentViewModel.filterData
        if theCurrentViewModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: theCurrentViewModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentViewModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentViewModel.nextOffset = 0
            
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentViewModel.filterData.assigntoID = "0"
            }
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            
            if filterData.reportType != "" && filterData.reportType != "-2" {
                totalFilterCount += 1
            }
            
            if filterData.period != "" {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentViewModel.arr_PriorityCellViewModel?.removeAll()
            self?.theCurrentViewModel.arr_PriorityCellViewModel = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getPriorityOverViewData(isRefreshing: true)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnFilterAction(_ sender: Any) {
       presentActionReportFilterCategory()
    }
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
}
