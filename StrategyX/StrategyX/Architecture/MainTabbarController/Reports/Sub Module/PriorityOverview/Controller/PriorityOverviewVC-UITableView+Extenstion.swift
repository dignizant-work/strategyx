//
//  PriorityOverviewVC-UITableView+Extenstion.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//MARK:- UITableViewDataSource
extension PriorityOverviewVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentViewModel.arr_PriorityCellViewModel == nil || theCurrentViewModel.arr_PriorityCellViewModel?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentViewModel.arr_PriorityCellViewModel?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: theCurrentViewModel.cellID) as! PriorityOverviewTVCell
        if theCurrentViewModel.arr_PriorityCellViewModel != nil {
            var isApiLoading = false
            if theCurrentViewModel.apiLoadingAtPrioriyID.contains(theCurrentViewModel.arr_PriorityCellViewModel![indexPath.row].id) {
                isApiLoading = true
            }
            cell.configure(theViewModel:theCurrentViewModel.arr_PriorityCellViewModel![indexPath.row] , isApiLoading: isApiLoading)
            
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentViewModel.arr_PriorityCellViewModel?[indexPath.row].status ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .due, selectedDate: self?.theCurrentViewModel.arr_PriorityCellViewModel![indexPath.row].duedate ?? "", dueDate: self?.theCurrentViewModel.arr_PriorityCellViewModel![indexPath.row].duedate ?? "", strActionID: self?.theCurrentViewModel.arr_PriorityCellViewModel![indexPath.row].id ?? "")
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.nextOffset != 0 {
                theCurrentViewModel.getPriorityOverViewWebservice()
            }
        } else {
            cell.startAnimation()
        }
//        cell.configure(strTitle: theCurrentViewModel.arr_PriorityCellViewModel[indexPath.row])
        return cell
        
    }
    
}

//MARK:- UITableViewDelegate
extension PriorityOverviewVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentViewModel.nextOffset != -1 && theCurrentViewModel.arr_PriorityCellViewModel != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let arr_model = theCurrentViewModel.arr_PriorityCellViewModel, arr_model.count > 0, !theCurrentViewModel.apiLoadingAtPrioriyID.contains(arr_model[indexPath.row].id) else { return  }
        let notification = Notification()
        notification.relatedId = arr_model[indexPath.row].relatedId
        notification.relatedTo = arr_model[indexPath.row].relatedTo
        self.redirectToModule(theModel: notification)
    }
}
