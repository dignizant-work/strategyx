//
//  PriorityOverviewView.swift
//  StrategyX
//
//  Created by Haresh on 20/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PriorityOverviewView: ViewParentWithoutXIB {
    //MARK:- Variable
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblStrategyCount: UILabel!
    @IBOutlet weak var lblRiskCount: UILabel!
    @IBOutlet weak var lblProjectCount: UILabel!
    @IBOutlet weak var lblFocusCount: UILabel!
    @IBOutlet weak var lblIdeaCount: UILabel!
    @IBOutlet weak var lblProblemCount: UILabel!
    
    //MARK:- LifeCycle
    func setupLayout()  {
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:PriorityOverviewVC) {
        //        refreshControl.tintColor = appGreen
        //        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(PriorityOverviewTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? PriorityOverviewVC)?.getPriorityOverViewData(isRefreshing: true)
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        if strCount.count == 0 {
            constraintLblFilterCountWidth.constant = 0.0
        } else {
            constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
            
        }
        self.layoutIfNeeded()
    }
    
    func updateActionsCount(theModel:PriorityOverviewCount) {
        lblStrategyCount.text = theModel.totalStrategyGoals.isEmpty ? "0" : theModel.totalStrategyGoals
        lblRiskCount.text = theModel.totalRisks.isEmpty ? "0" : theModel.totalRisks
        lblProjectCount.text = theModel.totalTacticalProjects.isEmpty ? "0" : theModel.totalTacticalProjects
        lblFocusCount.text = theModel.totalFocusGoals.isEmpty ? "0" : theModel.totalFocusGoals
        lblIdeaCount.text = theModel.totalIdeas.isEmpty ? "0" : theModel.totalIdeas
        lblProblemCount.text = theModel.totalProblems.isEmpty ? "0" : theModel.totalProblems
    }
}
