//
//  MainHomeView.swift
//  StrategyX
//
//  Created by Haresh on 28/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit


class MainHomeView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var viewTransperantBG: UIView!
    @IBOutlet weak var viewTableBG: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constrainTableHeight: NSLayoutConstraint! // 0
    
    @IBOutlet weak var constrainViewTableBGBottom: NSLayoutConstraint!//0
    
    let cellHeight:CGFloat = 40
    var bottom:CGFloat = 0.0
    var listCount:CGFloat = 7
    
    //MARK:- Life Cycle
    func setupLayout() {
        viewTransperantBG.alpha = 0.7
        self.viewTransperantBG.cornerRadius = 30.0
        viewTableBG.isHidden = true
        constrainTableHeight.constant = 0.0

    }
    
    func animateTransperantBG() {
        let scale:CGFloat = (UIScreen.main.bounds.size.height * 20.0) / 568.0
        print("scale:=",scale)
        UIView.animate(withDuration: 0.5, delay: 0.0, animations: {
            self.viewTransperantBG.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: { (finished) in
//            self.viewTransperantBG.cornerRadius = 0
            print("self.viewTransperantBG:=",self.viewTransperantBG.frame)
            
            self.showTableView(isHidden: false, height: self.listCount * self.cellHeight, bottom: self.bottom)
        })
    }
    
    
    
    func setupTableView(theDelegate:MainHomeVC) {
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = cellHeight

        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    func showTableView(isHidden:Bool, height:CGFloat,bottom:CGFloat) {
        if isHidden == false {
            viewTableBG.isHidden = isHidden
        }
        constrainViewTableBGBottom.constant = bottom
        constrainTableHeight.constant = height
        
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
        }, completion: { (finished) in
            self.viewTableBG.isHidden = isHidden
        })
    }
    
}
