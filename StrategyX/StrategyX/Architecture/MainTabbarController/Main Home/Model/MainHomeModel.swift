//
//  MainHomeModel.swift
//  StrategyX
//
//  Created by Haresh on 28/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MainHomeModel {

    //MARK:- Variable
    weak var theController:MainHomeVC!
    var arr_firstMenu = ["Strategy","Success Factors","Risk","Tactical Project","Idea","Problem","Focus","Action","Note","Recurring Task"]
    var arr_secondMenu = ["Back","Primary Area","Secondary Area","Goal"]
    var isShowSecondMenu = false
    var tabbarHeight:CGFloat = 49.0
    
    
    //MARK:- Life Cycle
    init(theController:MainHomeVC) {
        
        if UserDefault.shared.userRole == .companyAdminUser { //remove company
            if let index = arr_firstMenu.firstIndex(where: {$0 == "Company"}) {
                arr_firstMenu.remove(at: index)
            }
            if let index = arr_firstMenu.firstIndex(where: {$0 == "Success Factors"}) {
                arr_firstMenu.remove(at: index)
            }
            //            arr_firstMenu.removeLast()
            //            arr_firstMenu.removeLast()
        }
        else if UserDefault.shared.userRole == .manager {
            if let index = arr_firstMenu.firstIndex(where: {$0 == "Strategy"}) {
                arr_firstMenu.remove(at: index)
            }
        }
        else if UserDefault.shared.userRole == .staffUser || UserDefault.shared.userRole == .staffLite { // remove company and user
            //            arr_firstMenu.remove(at: 6)
            //            arr_firstMenu.remove(at: 6)
            arr_firstMenu = ["Idea","Problem","Action","Note","Recurring Task"]
        }
        self.theController = theController
    }
}
