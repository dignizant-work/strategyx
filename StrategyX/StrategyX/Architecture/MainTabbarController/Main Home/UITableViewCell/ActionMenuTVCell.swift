//
//  ActionMenuTVCell.swift
//  StrategyX
//
//  Created by Haresh on 28/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ActionMenuTVCell: UITableViewCell {

    //Cell 1
    @IBOutlet weak var btnRightArrow: UIButton!
    @IBOutlet weak var lblTitle:UILabel!
    
    //Cell 2
    @IBOutlet weak var btnBackArrow: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //Cell1
    
    func configureCell1(strTitle:String, index:Int) {
        btnRightArrow.isHidden = true
        lblTitle.text = strTitle
        if index == 0 && UserRole.staffUser != UserDefault.shared.userRole && UserRole.manager != UserDefault.shared.userRole && UserRole.staffLite != UserDefault.shared.userRole {
            btnRightArrow.isHidden = false
        }
    }
    
    func configureCell2(strTitle:String, index:Int) {
        lblTitle.text = strTitle
    }
    
}
