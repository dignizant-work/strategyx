//
//  AddDepartmentView.swift
//  StrategyX
//
//  Created by Haresh on 02/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddDepartmentView: ViewParentWithoutXIB {
    
    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var lblAddCompanyTitle: UILabel!
    @IBOutlet weak var lblAddDepartmentTitle: UILabel!
    @IBOutlet weak var txtAddDepartment: UITextField!
    
    @IBOutlet weak var constrainCollectionViewHeight: NSLayoutConstraint! // 0
    @IBOutlet weak var collectionView: UICollectionView!
    
//    @IBOutlet weak var viewTag: HTagView!
//    @IBOutlet weak var constrainViewTagHeight: NSLayoutConstraint!

    

    //MARK:- Life Cycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        constrainCollectionViewHeight.constant = 0.0
    }
    
    func setupCollectionView(theDelegate:AddDepartmentVC)  {
        collectionView.delegate = theDelegate
        collectionView.dataSource = theDelegate
        collectionView.registerCellNib(SelectFileCell.self)
        collectionView.reloadData()
    }
    
    /*
    func setupTagView(theDelegate:AddDepartmentVC)  {
        viewTag.delegate = theDelegate
        viewTag.dataSource = theDelegate
        viewTag.multiselect = false
        viewTag.marg = 2
        viewTag.btwTags = 2
        viewTag.btwLines = 2
        viewTag.tagFont = R12
        viewTag.tagMainBackColor = appGreen
        viewTag.tagMainTextColor = white
        viewTag.tagSecondBackColor = appGreen
        viewTag.tagSecondTextColor = white
        //         viewTag?.tagCancelIconRightMargin = 10
        //        viewTag.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        
        //        tagView.reloadData()
        //        tagView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        /*
         [tagView].forEach { (viewTag) in
         viewTag?.delegate = self
         viewTag?.dataSource = self
         viewTag?.multiselect = false
         viewTag?.marg = 2
         viewTag!.btwTags = 2
         viewTag?.btwLines = 2
         viewTag?.tagFont = R12
         viewTag?.tagMainBackColor = appBgLightGrayColor
         viewTag?.tagMainTextColor = white
         viewTag?.tagSecondBackColor = appBgLightGrayColor
         viewTag?.tagSecondTextColor = white
         // viewTag?.tagCancelIconRightMargin = 10
         viewTag?.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
         viewTag?.reloadData()
         }*/
        
        viewTag.reloadData()
        updateTagView()
    }

    func updateTagView() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.constrainViewTagHeight.constant = self.viewTag.frame.size.height
            if self.viewTag.frame.size.height <= 0{
                self.constrainViewTagHeight.constant = 40
            }
            self.layoutIfNeeded()
        }
    }
    */
    
}
