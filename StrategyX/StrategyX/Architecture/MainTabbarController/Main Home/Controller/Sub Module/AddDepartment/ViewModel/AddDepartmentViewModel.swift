//
//  AddDepartmentViewModel.swift
//  StrategyX
//
//  Created by Haresh on 02/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddDepartmentViewModel {

    weak var theController:AddDepartmentVC!
    var imageBG:UIImage?

    var strCompanyID = ""
    var strCompanyName = ""
    var arr_Tags_Main : [Department] = []

    
    //MARK:- LifeCycle
    init(theController:AddDepartmentVC) {
        self.theController = theController
    }
    
    func updateDepartment(){
        (theController.view as? AddDepartmentView)?.collectionView.reloadData()
        (theController.view as? AddDepartmentView)?.constrainCollectionViewHeight.constant = CGFloat(arr_Tags_Main.count * 50)
        UIView.animate(withDuration: 0.1) {
            self.theController.view.layoutIfNeeded()
        }
//        (theController.view as? AddDepartmentView)?.updateTagView()
    }
    
    func updateDepartmentList(strDepartmentID:String, isDelete:Bool)  {
        if isDelete {
            if let index = self.arr_Tags_Main.firstIndex(where: {$0.id == strDepartmentID}) {
                arr_Tags_Main.remove(at: index)
                theController.handlerDepartmentCount(arr_Tags_Main.count)
                updateDepartment()
            }
        }
    }
    
}

extension AddDepartmentViewModel {
    func departmentListService() {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strCompanyID]
        AddUserWebService.shared.getCompanyDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            Utility.shared.stopActivityIndicator()
            self?.arr_Tags_Main = list
            self?.updateDepartment()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func addDepartment(strDepartmentName:String) {
        var departmentNameDic:[[String:String]] = []
        departmentNameDic.append(["name":strDepartmentName])
        let strDepartment = JSON(departmentNameDic).rawString() ?? ""
        
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:0,APIKey.key_company_id:strCompanyID,APIKey.key_department_name:strDepartment] as [String : Any]
        
        CompanyWebService.shared.addDepartment(parameter: parameter, success: { [weak self] (msg, count, departmentID, departmentName) in
            Utility.shared.stopActivityIndicator()
            let department = Department()
            department.id = departmentID
            department.name = departmentName
            self?.arr_Tags_Main.append(department)
            self?.theController.showAlertAtBottom(message: msg)
            self?.theController.handlerDepartmentCount(self?.arr_Tags_Main.count ?? 0)
            self?.updateDepartment()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    
    func deleteDepartment(strDepartmenID:String) {
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_page_offset:0,APIKey.key_department_id:strDepartmenID] as [String : Any]
        
        CompanyWebService.shared.deleteDepartment(parameter: parameter, success: { [weak self] (msg) in
            Utility.shared.stopActivityIndicator()
            self?.theController.showAlertAtBottom(message: msg)

            self?.updateDepartmentList(strDepartmentID: strDepartmenID, isDelete: true)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
}
