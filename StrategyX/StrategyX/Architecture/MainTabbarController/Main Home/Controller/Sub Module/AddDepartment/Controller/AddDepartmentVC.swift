//
//  AddDepartmentVC.swift
//  StrategyX
//
//  Created by Haresh on 02/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddDepartmentVC: ParentViewController {

    //MARK:- Variable
    internal lazy var theCurrentView:AddDepartmentView = { [unowned self] in
        return self.view as! AddDepartmentView
    }()
    
    internal lazy var theCurrentViewModel : AddDepartmentViewModel = {
       return AddDepartmentViewModel.init(theController: self)
    }()
    
    var handlerDepartmentCount:(Int) -> Void = {_ in}
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    func setupUI()  {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentViewModel.imageBG)
        theCurrentView.setupCollectionView(theDelegate: self)
//        theCurrentView.setupTagView(theDelegate: self)
        theCurrentView.lblAddDepartmentTitle.text = theCurrentViewModel.strCompanyName
        theCurrentViewModel.departmentListService()
    }
    func setTheData(imgBG:UIImage?,strCompanyID:String, strCompanyName:String) {
        theCurrentViewModel.imageBG = imgBG
        theCurrentViewModel.strCompanyID = strCompanyID
        theCurrentViewModel.strCompanyName = strCompanyName
    }
    //MARK:- Member Function
    func deleteDepartment(strDepartmenID: String) {
        showAlertAction(msg: "Are you sure you want to remove this department?") { [weak self] in
            self?.theCurrentViewModel.deleteDepartment(strDepartmenID: strDepartmenID)
        }
    }
    
    //MARK:- IBAction
    @IBAction func onBtnRemoveAttachmentAction(_ sender: UIButton) {
        deleteDepartment(strDepartmenID: theCurrentViewModel.arr_Tags_Main[sender.tag].id)
//        let attachment = theCurrentModel.arr_selectedFile[sender.tag]
//        theCurrentModel.arr_RemoveSelectedFile.append(attachment)
//        theCurrentModel.arr_selectedFile.remove(at: sender.tag)
//        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    @IBAction func onBtnAddDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentView.txtAddDepartment.text!.trimmed().isEmpty {
            self.showAlertAtBottom(message: "Please enter department name")
            return
        }
        let department = theCurrentView.txtAddDepartment.text!
        theCurrentView.txtAddDepartment.text = ""
        theCurrentViewModel.addDepartment(strDepartmentName: department)
        
    }
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.view.endEditing(true)
        self.backAction(isAnimation: false)
    }
}


//MARK:- UICollectionView datasource & delegate

extension AddDepartmentVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_Tags_Main.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
        cell.lblFileName.setFont = "R15"
        cell.lblFileName.numberOfLines = 1
//        if theCurrentViewModel.theActionDetailModel?.actio.lowercased() == APIKey.key_archived.lowercased() {
//            cell.btnCloseOutlet.isHidden = true
//        }
        //        cell.bgView.setBackGroundColor = theCurrentViewModel.arr_Tags_Main[indexPath.row].fileType == .audio ? 3 : 8
        
        cell.lblFileName.text = theCurrentViewModel.arr_Tags_Main[indexPath.row].name
        
        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveAttachmentAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       /* if theCurrentViewModel.arr_Tags_Main[indexPath.row].fileName.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentViewModel.arr_Tags_Main[indexPath.row].fileName)
        } else {
            self.presentWebViewVC(url: theCurrentViewModel.arr_Tags_Main[indexPath.row].fileName, isLocal: !theCurrentViewModel.arr_Tags_Main[indexPath.row].isOldAttachment)
        }*/
    }
    
}
/*
//MARK:- TagView Delegate
extension AddDepartmentVC:HTagViewDelegate, HTagViewDataSource {
    func numberOfTags(_ tagView: HTagView) -> Int {
        return theCurrentViewModel.arr_Tags_Main.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return theCurrentViewModel.arr_Tags_Main[index].name
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .cancel
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    
    // MARK:- HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
    }
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        print("tag with index: '\(index)' has to be removed from tagView")
        deleteDepartment(strDepartmenID: theCurrentViewModel.arr_Tags_Main[index].id)
//        updateTagList(strTag: "", isAdded: false, at: index)
        //        handlerRemoveTagAction(index)
    }
    
}*/
