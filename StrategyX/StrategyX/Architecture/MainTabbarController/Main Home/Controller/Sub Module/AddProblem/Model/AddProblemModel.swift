//
//  AddFocusModel.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddProblemModel {

    //MARK:- Variable
    fileprivate weak var theController:AddProblemVC!
    var tabelViewCount = 6
    
    //    var tableviewCount = 10
    var imageBG:UIImage?
    var isEdit = false
    var theEditIdeaAndProblemModel:IdeaAndProblemList?

    var isDropDownLoadingAtIndex:Int = -1
    var isAddFocusApiLoading = false
    
    var arr_selectedFile:[AttachmentFiles] = []
    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","focusname":"","duedate":""])
    
    //MARK:- LifeCycle
    init(theController:AddProblemVC) {
        self.theController = theController
    }
}
//MARK:- Api Call
extension AddProblemModel {
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            
            }, failed: { [weak self](error) in
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
