//
//  AddFocusVC.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import IQAudioRecorderController
import MobileCoreServices
import SwiftyJSON

class AddProblemVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case company             = 2
        case assignTo            = 3
        case department          = 4
    }
    enum dateSelctionType:Int {
        case due                = 2
        case resived            = 3
    }
    
    fileprivate lazy var theCurrentView:AddProblemView = { [unowned self] in
       return self.view as! AddProblemView
    }()

    fileprivate lazy var theCurrentModel:AddProblemModel = {
       return AddProblemModel(theController: self)
    }()
    
    var handlerUpdateModel:(IdeaAndProblemList?) -> Void = {_ in }
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        if theCurrentModel.isEdit {
            showTheEditData()
        } else {
            setCompanyData()
        }
    }
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    func setTheEditData(theModel:IdeaAndProblemList?, isForEdit:Bool)  {
        theCurrentModel.isEdit = isForEdit
        theCurrentModel.theEditIdeaAndProblemModel = theModel
    }
    func showTheEditData() {
        guard let theModel = theCurrentModel.theEditIdeaAndProblemModel else { return }
        
        theCurrentModel.selectedData["title"].stringValue = theModel.title
        theCurrentModel.selectedData["description"].stringValue = theModel.description_ideaNproblem
        theCurrentModel.selectedData["company"].stringValue = theModel.companyName
        theCurrentModel.strSelectedCompanyID = theModel.companyId
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            theCurrentModel.arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            theCurrentModel.arr_selectedFile.append(attchment)
        }
        updateTableViewCount()
    }
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            theCurrentModel.companylistService()
        } else {
            theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
            /* if let index = tableviewCellType.firstIndex(where: {$0 == .company}) {
             tableviewCellType.remove(at: index)
             }
             strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
             selectedData["company"].stringValue = Utility.shared.userData.companyName
             assignUserListService()*/
        }
    }
    func updateBottomCell() {
        let index = IndexPath(row: theCurrentModel.tabelViewCount - 1, section: 0)
        theCurrentModel.isAddFocusApiLoading = !theCurrentModel.isAddFocusApiLoading
        UIView.performWithoutAnimation {
            let contentOffset = theCurrentView.tableView.contentOffset
            theCurrentView.tableView.reloadRows(at: [index], with: .none)
            theCurrentView.tableView.contentOffset = contentOffset
        }
    }
    func updateTableForDropDownStopLoading() {
        let index = IndexPath(row: theCurrentModel.isDropDownLoadingAtIndex, section: 0)
        theCurrentModel.isDropDownLoadingAtIndex = -1
        let contentOffset = theCurrentView.tableView.contentOffset
        UIView.performWithoutAnimation {
            theCurrentView.tableView.reloadRows(at: [index], with: .none)
            theCurrentView.tableView.contentOffset = contentOffset
        }
    }
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            theCurrentModel.strSelectedCompanyID = theCurrentModel.arr_companyList[index].id
            //            theCurrentModel.assignUserListService()
            
            theCurrentModel.selectedData["company"].stringValue = str
            //            theCurrentModel.selectedData["assignto"].stringValue = ""
            //            theCurrentModel.strSelectedUserAssignID = ""
            //            theCurrentModel.selectedData["department"].stringValue = ""
            //            theCurrentModel.strSelectedDepartmentID = ""
            
            theCurrentModel.isDropDownLoadingAtIndex = -1
            UIView.performWithoutAnimation {
                self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 3, section: 0)], with: UITableView.RowAnimation.none)
                
                /*if theCurrentModel.tabelViewCount. {
                 }
                 if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.assignTo}) {
                 theCurrentModel.isDropDownLoadingAtIndex = index
                 self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                 }
                 if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.department}) {
                 self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                 }
                 //                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 4, section: 0),IndexPath.init(row: 5, section: 0),IndexPath.init(row: 6, section: 0)], with: UITableView.RowAnimation.none)
                 */
            }
            break
        case .assignTo:
            // theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].id
            /*  theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].user_id
             theCurrentModel.selectedData["assignto"].stringValue = str
             var isEdit = false
             
             if !theCurrentModel.isEditFocus {
             theCurrentModel.departmentListService()
             theCurrentModel.selectedData["department"].stringValue = ""
             theCurrentModel.strSelectedDepartmentID = ""
             isEdit = true
             
             //                theCurrentModel.isDropDownLoadingAtIndex = 6
             } else {
             if let theModel = theCurrentModel.theEditFocusModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdBy){
             theCurrentModel.departmentListService()
             theCurrentModel.selectedData["department"].stringValue = ""
             theCurrentModel.strSelectedDepartmentID = ""
             isEdit = true
             }
             }
             
             UIView.performWithoutAnimation {
             if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.assignTo}) {
             self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
             }
             
             if isEdit, let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.department}) {
             theCurrentModel.isDropDownLoadingAtIndex = index
             self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
             }
             //                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 5, section: 0),IndexPath.init(row: 6, section: 0)], with: UITableView.RowAnimation.none)
             }*/
            break
        case .department:
            /*theCurrentModel.strSelectedDepartmentID = theCurrentModel.arr_departmentList[index].id
             theCurrentModel.selectedData["department"].stringValue = str
             UIView.performWithoutAnimation {
             if let index = theCurrentModel.tableviewCellType.firstIndex(where: {$0 == AddFocusModel.celltype.department}) {
             self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
             }
             //                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 6, section: 0)], with: UITableView.RowAnimation.none)
             }*/
            break
            
        }
    }
    
    func updateTableViewCount() {
        theCurrentModel.tabelViewCount = 6 + theCurrentModel.arr_selectedFile.count
        theCurrentView.tableView.reloadData()
    }
    func deleteAttachment(index:Int) {
        if theCurrentModel.isEdit {
            if theCurrentModel.arr_selectedFile[index].isOldAttachment {
                theCurrentModel.arr_selectedFile[index].isDeleteAttachment = true
            } else {
                theCurrentModel.arr_selectedFile.remove(at: index)
            }
        } else {
            theCurrentModel.arr_selectedFile.remove(at: index)
        }
        updateTableViewCount()
    }
    func validateForm() {
        self.view.endEditing(true)
        if theCurrentModel.selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter title")
            return
        }
        if theCurrentModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select company")
            return
        }
        
        if theCurrentModel.isEdit {
            updateIdeaNProbemWebService()
        } else {
            addIdeaNProbemWebService()
        }
    }
    
    func addIdeaAndProblemInListVC(theModel:IdeaAndProblemList) {
        if let vc = (self.tabBarController?.viewControllers?[1] as? UINavigationController)?.viewControllers[0] as? IdeasNproblemsVC {
            vc.theCurrentViewModel.ideaAndProblemListWebService(isRefreshing: true)
        }
    }
    
    //MARK:- Redirect To
    
    func presentFileManger() {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        
    }
    func presentDropDownListVC(categorySelection:categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            let arr_company:[String] = theCurrentModel.arr_companyList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Company",arr:arr_company)
            break
        case .assignTo:
            //            let arr_assignTo:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name}).filter({!$0.isEmpty})
            //            //          let arr_assignTo:[String] = theCurrentModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            //            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            //            let arr_department:[String] = theCurrentModel.arr_departmentList.map({$0.name}).filter({!$0.isEmpty})
            //            vc.setTheData(strTitle: "Depatment",arr:arr_department)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    
}
//MARK:- UITableViewDataSource
extension AddProblemVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tabelViewCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewTVCell") as! HeaderViewTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: theCurrentModel.isEdit ? "Edit Problem" : "Add Problem", isForGoal: false)
            
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Title",isMandatory: true, strPlaceHolder: "Title")
            if theCurrentModel.isEdit {
                cell.setTheTitle(inputText: theCurrentModel.selectedData["title"].stringValue)
            }
            cell.handlerText = { [weak self] (strData) in
                self?.theCurrentModel.selectedData["title"].stringValue = strData
            }
            return cell
        } else if indexPath.row == 2 { // Discription Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Description", strPlaceHolder: "Description",isMandatory:false)
            if theCurrentModel.isEdit {
                cell.setTheDescription(inputText: theCurrentModel.selectedData["description"].stringValue)
            }
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["description"].stringValue = text
            }
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTypeTVCell") as! SelectionTypeTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.isEdit {
                if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                    cell.isHidden = true
                } else {
                    cell.isHidden = false
                }
            } else {
                if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                    cell.isHidden = false
                } else {
                    cell.isHidden = true
                }
            }
            
            let isLoading = theCurrentModel.isDropDownLoadingAtIndex == indexPath.row
            cell.configure(strTitle: "Company", strSelectionType: theCurrentModel.selectedData["company"].stringValue.isEmpty ? "Select company" : theCurrentModel.selectedData["company"].stringValue, isMandatory: true,isLoading: isLoading)
            if theCurrentModel.isEdit {
                cell.dropDown(isEnable: false)
            } else {
                cell.isDisableCompanySelection()
            }
            cell.handlerBtnClick = { [weak self] in
                self?.presentDropDownListVC(categorySelection: AddProblemVC.categorySelectionType.company)
            }
            return cell
        } else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! IdeaNProblemsTVCell
            cell.handlerAttachmentOrVoiceNote = { [weak self] isAttachment in
                if isAttachment { // Add Attachment
                    self?.presentFileManger()
                } else { // Add Voice
                    self?.presentVoiceNote()
                }
            }
            return cell
        } else if theCurrentModel.arr_selectedFile.count > 0 && indexPath.row < (5 + theCurrentModel.arr_selectedFile.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            let index = indexPath.row - 5
//            cell.lblTitle.setFontColor = theCurrentModel.arr_selectedFile[index].fileType == .audio ? 1 : 5
//            cell.bgView.setBackGroundColor = theCurrentModel.arr_selectedFile[index].fileType == .audio ? 3 : 26
            cell.configure(strTitle: (theCurrentModel.arr_selectedFile[index].fileName as NSString).lastPathComponent)
            cell.handlerOnDeleteAction = { [weak self] (tag) in
                self?.deleteAttachment(index: index)
            }
            return cell
        } else { // Last Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomButtonTVCell") as! BottomButtonTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(isLoading: theCurrentModel.isAddFocusApiLoading)
            if theCurrentModel.isEdit {
                cell.configure(isLoading: theCurrentModel.isAddFocusApiLoading, strTitlebtnAdd: "Edit", strTitlebtnCancel: "Close")
            }
            cell.handlerAdd = { [weak self] in
                self?.validateForm()
            }
            cell.handlerCancel = { [weak self] in
                self?.backAction(isAnimation: false)
            }
            return cell
        }
    }
    
    
}
//MARK:-UITableViewDelegate
extension AddProblemVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            if theCurrentModel.isEdit {
                if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                    return CGFloat.leastNormalMagnitude
                }
            } else {
                if UserRole.superAdminUser == UserDefault.shared.userRole, (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) {
                    return UITableView.automaticDimension
                } else {
                    return CGFloat.leastNormalMagnitude
                }
            }
        } else if theCurrentModel.arr_selectedFile.count > 0 && indexPath.row > 4  && indexPath.row < (5 + theCurrentModel.arr_selectedFile.count) {
            if theCurrentModel.isEdit {
                let index = indexPath.row - 5
                if theCurrentModel.arr_selectedFile[index].isDeleteAttachment {
                    return CGFloat.leastNormalMagnitude
                }
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_selectedFile.count > 0 && indexPath.row < (5 + theCurrentModel.arr_selectedFile.count) {
            let index = indexPath.row - 5
            if theCurrentModel.arr_selectedFile[index].fileName.isContainAudioOrVideoFile() {
                self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[index].fileName)
            } else {
                self.presentWebViewVC(url: theCurrentModel.arr_selectedFile[index].fileName, isLocal: !theCurrentModel.arr_selectedFile[index].isOldAttachment)
            }
            /*
            if theCurrentModel.arr_selectedFile[index].fileType == .audio {
                if !theCurrentModel.arr_selectedFile[index].isOldAttachment, let audioData = theCurrentModel.arr_selectedFile[index].fileData {
                    if let url = self.setAudioFileToDocumentDirectory(audioData: audioData, audioNameWithExtension: "\(APIKey.key_Audio_name).\(APIKey.key_Extension)") {
                        self.presentAudioPlayer(url: url,isLocalURl: true)
                    }
                } else {
                    if theCurrentModel.arr_selectedFile[indexPath.row].fileName.contains(".wav") {
                        let fileName = theCurrentModel.arr_selectedFile[indexPath.row].fileName
                        let lastFile = (fileName as NSString).lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3")
                        print("lastFile:=",lastFile)
                        self.downloadAudioFileWebService(strURl: fileName) { [weak self] (fileData) in
                            if let url = self?.setAudioFileToDocumentDirectory(audioData: fileData, audioNameWithExtension: lastFile) {
                                self?.presentAudioPlayer(url: url,isLocalURl: true)
                            }
                        }
                    } else {
                        self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
                    }
//                    self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
                }
            }*/
        }
    }
}


//MARK:- UIDocumentPickerDelegate
extension AddProblemVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    }) {
                        
                    } else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        updateTableViewCount()
        

    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                }) {
                    
                } else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        } else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        updateTableViewCount()
        

    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        

    }
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension AddProblemVC:IQAudioRecorderViewControllerDelegate {
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")
        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
                //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
//                let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
//                
//                //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
//                self?.theCurrentModel.arr_selectedFile.append(objSelectedData)
                
                
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.updateTableViewCount()
                    }
                    
                }
                
            }
            
        }
        controller.dismiss(animated: true, completion: nil)
        

    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        

    }
}
//MARK:- Api Call
extension AddProblemVC {
    func addIdeaNProbemWebService() {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var voiceCount = 0
        var fileCount = 0

        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].fileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].fileName
                let mimeType = MimeType(path: path).value
//                let mimeExtension = mimeType.components(separatedBy: "/")
//                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].fileData!)
                fileMimeType.append(mimeType)
                if theCurrentModel.arr_selectedFile[i].fileType == .audio {
                    withName.append("voice_notes[\(voiceCount)]")
                    withFileName.append(path)
                    voiceCount += 1
                } else {
                    withName.append("files[\(fileCount)]")
                    withFileName.append(path)
                    fileCount += 1
                }
                index += 1
            }
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_type:IdeaAndProblemType.problem.rawValue,
                         APIKey.key_title:theCurrentModel.selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_description:theCurrentModel.selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID] as [String : String]
        
        self.updateBottomCell()
        
        IdeaAndProblemsWebServices.shared.addIdeaNProblemAttachment(parameter:parameter , files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theModel) in
            self?.updateBottomCell()
            self?.addIdeaAndProblemInListVC(theModel: theModel)
            self?.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell()
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func updateIdeaNProbemWebService() {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var voiceCount = 0
        var fileCount = 0

        for i in 0..<theCurrentModel.arr_selectedFile.count {
            if theCurrentModel.arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:theCurrentModel.arr_selectedFile[i].attachmentID])
            } else {
                if !theCurrentModel.arr_selectedFile[i].isOldAttachment && (theCurrentModel.arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = theCurrentModel.arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(theCurrentModel.arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if theCurrentModel.arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(index)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_type:IdeaAndProblemType.idea.rawValue,
                         APIKey.key_remove_file:JSON(removeFile).rawString() ?? "",
                         APIKey.key_idea_and_problem_id:theCurrentModel.theEditIdeaAndProblemModel?.id ?? "",
                         APIKey.key_title:theCurrentModel.selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_description:theCurrentModel.selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID] as [String : String]
        
        self.updateBottomCell()
        
        IdeaAndProblemsWebServices.shared.updateIdeaNProblemAttachment(parameter:parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theModel) in
            self?.updateBottomCell()
            self?.handlerUpdateModel(theModel)
            self?.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell()
                self?.showAlertAtBottom(message: error)
        })
    }
}
