//
//  AddSecondaryAreaViewModel.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddSecondaryAreaViewModel {

    //MARK:-Variable
    fileprivate weak var theController:AddSecondaryAreaVC!

    var imageBG:UIImage?

    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?

    var arr_selectedFile:[AttachmentFiles] = []
    var arr_RemoveSelectedFile:[AttachmentFiles] = []

    var selectedDueDate:Date?
    var selectedrevisedDate:Date?
    
    // getPrimaryArea
    var strSelectedPrimaryID:String = ""
    var arr_PrimaryResult:[PrimaryArea] = []

    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""

    var strNoteStatus:String = ""
    var arr_noteHistoryStatus:[JSON] = []
    
    var isForAddAction = false
    var isForAddNoteAction = false

    var isEdit = false
    var isForAddFromStrategyList = false
    var theEditSecondaryAreaModel:SecondaryArea?

    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","title":"","primaryresult":""])

    
    enum categorySelectionType:Int {
        case company             = 2
        case assignTo            = 3
        case department          = 4
        case primary             = 5
    }
    enum dateSelctionType:Int {
        case due                = 2
        case resived            = 3
    }
    
    //MARK:- LifeCyle
    init(theController:AddSecondaryAreaVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
         theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
         }*/
        if isEdit {
            (theController.view as? AddSecondaryAreaView)?.btnSelectCompany.isUserInteractionEnabled = false
        } else {
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                (theController.view as? AddSecondaryAreaView)?.btnSelectCompany.isUserInteractionEnabled = false
            }
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService()
            
        } else {
            strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            selectedData["company"].stringValue = Utility.shared.userData.companyName
            (theController.view as? AddSecondaryAreaView)?.lblCompany.text = Utility.shared.userData.companyName
            (theController.view as? AddSecondaryAreaView)?.viewCompany.isHidden = true
            assignUserListService()
        }
    }
    
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        (theController.view as? AddSecondaryAreaView)?.activitySpinner.hidesWhenStopped = true
        (theController.view as? AddSecondaryAreaView)?.constrainViewPagerHeight.constant = 40.0
        (theController.view as? AddSecondaryAreaView)?.viewOfPager.isHidden = false
        
        if isAnimating {
            (theController.view as? AddSecondaryAreaView)?.activitySpinner.startAnimating()
            (theController.view as? AddSecondaryAreaView)?.viewMore.isHidden = true
        } else {
            (theController.view as? AddSecondaryAreaView)?.activitySpinner.stopAnimating()
            (theController.view as? AddSecondaryAreaView)?.viewMore.isHidden = false
            
            if nextOffset == -1 {
                (theController.view as? AddSecondaryAreaView)?.viewOfPager.isHidden = true
                (theController.view as? AddSecondaryAreaView)?.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    
    func showTheEditData() {
        guard let theModel = theEditSecondaryAreaModel else { return }
        (theController.view as? AddSecondaryAreaView)?.lblAddTitle.text = "Edit Secondary Area"
        selectedData["title"].stringValue = theModel.title
        selectedData["description"].stringValue = theModel.descriptionArea
        selectedData["company"].stringValue = theModel.companyName
        selectedData["assignto"].stringValue = theModel.assignedTo
        selectedData["department"].stringValue = theModel.department
        selectedData["createdby"].stringValue = theModel.createdBy
        
        strSelectedPrimaryID = theModel.id
        selectedData["primaryresult"].stringValue = theModel.title

        let createdDate = theModel.createdDate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let createdDates = createdDate {
            selectedData["createdDate"].stringValue = createdDates.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        strSelectedCompanyID = theEditSecondaryAreaModel?.companyId ?? Utility.shared.userData.companyId
        strSelectedUserAssignID = theModel.assigneeId
        strSelectedDepartmentID = theModel.departmentId
        
        (theController.view as? AddSecondaryAreaView)?.txtTitle.isUserInteractionEnabled = false
        (theController.view as? AddSecondaryAreaView)?.txtDescription.isUserInteractionEnabled = false

        (theController.view as? AddSecondaryAreaView)?.showUpdateData(theModel: selectedData)
        (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: false, view: (theController.view as? AddSecondaryAreaView)!.viewSelectedCompany)
        (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: false, view: (theController.view as? AddSecondaryAreaView)!.viewSelectionPrimaryResult)

        (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: false, view: (theController.view as? AddSecondaryAreaView)!.viewSelectionAssignTo)
        (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: false, view: (theController.view as? AddSecondaryAreaView)!.viewSelectionDepartment)
        DispatchQueue.main.async { [weak self] in
            (self?.theController.view as? AddSecondaryAreaView)?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        arr_selectedFile.removeAll()
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        (theController.view as? AddSecondaryAreaView)?.viewPrimaryResult.isHidden = true
        if UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById) {
            (theController.view as? AddSecondaryAreaView)?.txtTitle.isUserInteractionEnabled = true
            (theController.view as? AddSecondaryAreaView)?.txtDescription.isUserInteractionEnabled = true

            (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: true, view: (theController.view as? AddSecondaryAreaView)!.viewSelectionAssignTo)
            (theController.view as? AddSecondaryAreaView)?.isEnableView(isEnable: true, view: (theController.view as? AddSecondaryAreaView)!.viewSelectionDepartment)
        }
    }

    
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .primary:
            strSelectedPrimaryID = arr_PrimaryResult[index].id
            selectedData["primaryresult"].stringValue = str
            strSelectedCompanyID = arr_PrimaryResult[index].companyId
            selectedData["company"].stringValue = arr_PrimaryResult[index].companyName

            assignUserListService()
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            break
        case .company:
            strSelectedCompanyID = arr_companyList[index].id
            assignUserListService()
            
            selectedData["company"].stringValue = str
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            strSelectedUserAssignID = arr_assignList[index].user_id
            selectedData["assignto"].stringValue = str
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            departmentListService()
            break
        case .department:
            strSelectedDepartmentID = arr_departmentList[index].id
            selectedData["department"].stringValue = str
            break
            
        
        }
        (theController.view as? AddSecondaryAreaView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateDate(strDate:String,type:dateSelctionType,date:Date) {
        switch type {
        case .due:
            selectedDueDate = date
            selectedData["duedate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddSecondaryAreaModel.celltype.dueDate}) {
//                self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
        //            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 7, section: 0)], with: UITableView.RowAnimation.none)
        case .resived:
            selectedrevisedDate = date
            selectedData["revisedduedate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddSecondaryAreaModel.celltype.reviseDate}) {
//                self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
            //            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 8, section: 0)], with: UITableView.RowAnimation.none)
        }
        (theController.view as? AddSecondaryAreaView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateSecondaryAreaModel(theModel:SecondaryArea) {
        theEditSecondaryAreaModel?.title = selectedData["title"].stringValue
        theEditSecondaryAreaModel?.descriptionArea = selectedData["description"].stringValue
        
        theEditSecondaryAreaModel = theModel
    }
    func validateForm() {
        theController.view.endEditing(true)
        if selectedData["title"].stringValue.trimmed().isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter title")
            return
        } else if selectedData["primaryresult"].stringValue.trimmed().isEmpty {
            self.theController.showAlertAtBottom(message: "Please select primary result")
            return
        } else if selectedData["company"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select company")
            return
        } else if selectedData["assignto"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select assigned to")
            return
        } else if selectedData["department"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select department")
            return
        }
        
        
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var removeAttachment:[String] = []
        
        var voiceCount = 0
        var fileCount = 0
        
        for item in arr_RemoveSelectedFile {
            if item.isOldAttachment {
                removeAttachment.append(item.attachmentID)
            }
            //            removeFile.append([APIKey.key_id:item.attachmentID])
        }
        
        for i in 0..<arr_selectedFile.count {
            if arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
            } else {
                if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
//        var param = parameter
//        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")
        
        if isEdit {
            let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_edit_id:theEditSecondaryAreaModel?.id ?? "",
                             APIKey.key_edit_table:APIKey.key_strategy_secondary_areas,
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_title:selectedData["title"].stringValue.trimmed(),
                             APIKey.key_description:selectedData["description"].stringValue.trimmed(),
                             APIKey.key_remove_attechment:removeAttachment.joined(separator: ",")] as [String : Any]
            
            updateStrategy(parameter: parameter,fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        } else {
            var notes = ""
            
            if let history = arr_notesHistoryList, history.count > 0 {
                var noteHistory:[[String:String]] = []
                history.forEach { (item) in
                    noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
                }
                notes = JSON(noteHistory).rawString() ?? ""
            }
            let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_title:selectedData["title"].stringValue.trimmed(),
                             APIKey.key_primary_area_id:strSelectedPrimaryID,
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_notes:notes,
                             APIKey.key_description:selectedData["description"].stringValue.trimmed()] as [String : Any]
            
            addSecondaryArea(parameter: parameter, fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        }
        
    }
}

//MARK:- Api Call
extension AddSecondaryAreaViewModel {
    func primaryArealistService() {
        (theController.view as? AddSecondaryAreaView)?.loadPrimaryResultActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id,APIKey.key_list_for:APIKey.key_all,APIKey.key_company_id:Utility.shared.userData.companyId == "0" ? "" : Utility.shared.userData.companyId]
        StrategyWebService.shared.getPrimaryAreaList(parameter: parameter, success: { [weak self] (list, offset) in
            self?.arr_PrimaryResult = list
            (self?.theController.view as? AddSecondaryAreaView)?.loadPrimaryResultActivity(isStart: false)
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                (self?.theController.view as? AddSecondaryAreaView)?.loadPrimaryResultActivity(isStart: false)
        })
        
    }
    func companylistService() {
        (theController.view as? AddSecondaryAreaView)?.loadCompanyActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            (self?.theController.view as? AddSecondaryAreaView)?.loadCompanyActivity(isStart: false)
            }, failed: { [weak self](error) in
                (self?.theController.view as? AddSecondaryAreaView)?.loadCompanyActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func assignUserListService() {
        (theController.view as? AddSecondaryAreaView)?.loadAssignToActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
//            self?.theController.updateTableForDropDownStopLoading()
            (self?.theController.view as? AddSecondaryAreaView)?.loadAssignToActivity(isStart: false)

            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddSecondaryAreaView)?.loadAssignToActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    func departmentListService() {
        (theController.view as? AddSecondaryAreaView)?.loadDepartmentActivity(isStart: true)

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            (self?.theController.view as? AddSecondaryAreaView)?.loadDepartmentActivity(isStart: false)

//            self?.theController.updateTableForDropDownStopLoading()
            if let obj = self, !obj.isEdit, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddSecondaryAreaView)?.showDropDownUpdatedData(theModel: obj.selectedData)
//                obj.theController.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            } else if let obj = self, obj.isEdit, obj.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddSecondaryAreaView)?.showDropDownUpdatedData(theModel: obj.selectedData)
            }
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddSecondaryAreaView)?.loadDepartmentActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theEditSecondaryAreaModel?.id ?? "",
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_strategy_secondary_areas,
                         APIKey.key_page_offset:"\(nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            (self?.theController.view as? AddSecondaryAreaView)?.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)
        })
    }
    
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theEditSecondaryAreaModel?.id ?? "",
                         APIKey.key_history_for: APIKey.key_strategy_secondary_areas]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }
    
    func addSecondaryArea(parameter:[String:Any], fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: true)
        StrategyWebService.shared.addSecondaryArea(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
            (self?.theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: false)
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateBottomCell()
        })
    }
    
    func updateStrategy(parameter:[String:Any],fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: true)
        StrategyWebService.shared.updateStrategy(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (strMsg, thePrimaryAreaModel, theSecondaryAreaModel, theStrategyGoalListModel) in
            if let model = thePrimaryAreaModel {
                if let theSecondary = SecondaryArea(JSON: model.toJSON()) {
                    self?.updateSecondaryAreaModel(theModel:theSecondary)
                    self?.theController.handlerEditData(theSecondary)
                }
            }
            (self?.theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: false)
            self?.theController.backAction(isAnimation: false)

        }, failed: { [weak self] (error) in
            (self?.theController.view as? AddSecondaryAreaView)?.isClickOnAdd(isLoading: false)
            self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.strNoteStatus ?? "")
            (self?.theController.view as? AddSecondaryAreaView)?.txtNote.text = ""
            completion()
            }, failed: { [weak self] (error) in
                completion()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
