//
//  AddSecondaryAreaView.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class AddSecondaryAreaView: ViewParentWithoutXIB {
    //MARK:- IBOutlet

    @IBOutlet weak var lblAddTitle: UILabel!
    @IBOutlet weak var img_Bg: UIImageView!
    
    @IBOutlet weak var tblNotesHistory: UITableView!
    @IBOutlet weak var heightOfNotesHistory: NSLayoutConstraint!
    
    @IBOutlet weak var lblDesriptionTitle: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var viewPrimaryResult: UIView!
    @IBOutlet weak var viewSelectionPrimaryResult: UIView!
    @IBOutlet weak var btnSelectPrimaryResult: UIButton!
    @IBOutlet weak var lblPrimaryResult: UILabel!
    
    
    @IBOutlet weak var viewSelectedCompany: UIView!
    @IBOutlet weak var btnSelectCompany: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var viewCompany: UIView!
    
    @IBOutlet weak var viewSelectionAssignTo: UIView!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var btnAssigned: UIButton!
    
    @IBOutlet weak var viewSelectionDepartment: UIView!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var btnDepartment: UIButton!
    
    @IBOutlet weak var lblApprovedBy: UILabel!
    
    @IBOutlet weak var lblCreatedDate: UILabel!

    @IBOutlet weak var lblAttachmentFile: UILabel!
    
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var viewOfPager: UIView!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var txtNotesHistory: UITextField!
    @IBOutlet weak var constrainViewPagerHeight: NSLayoutConstraint! // 40.0
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewNotes: UIView!
    
    @IBOutlet weak var constrainViewNoteHeight: NSLayoutConstraint! //98
    
    @IBOutlet weak var btnAddVoiceNote: UIButton!
    @IBOutlet weak var constrainViewfileNameHeight: NSLayoutConstraint! // 30.0
    
    @IBOutlet weak var viewNoteHistoryFilter: UIView!
    
    @IBOutlet weak var constrainViewNoteHistiryFIlter: NSLayoutConstraint! // 30
    
    let disposeBag = DisposeBag()

    func setupLayout(imgBG: UIImage?) {
        img_Bg.image = imgBG
        lblAddTitle.text = "Add Secondary Area"
        viewMore.isHidden = true
        activitySpinner.color = appGreen
        //Rx Actions
        rxTitleTextFieldAction()
        rxDescriptionTextViewDidChange()
    }
    
    func setupTableView(theDelegate:AddSecondaryAreaVC) {
        
        tblNotesHistory.estimatedRowHeight = 50.0
        tblNotesHistory.rowHeight = UITableView.automaticDimension
        
        tblNotesHistory.registerCellNib(HistoryCell.self)
        
        
        tblNotesHistory.delegate = theDelegate
        tblNotesHistory.dataSource = theDelegate
        tblNotesHistory.separatorStyle = .none
    }
    
    func setupCollectionView(theDelegate:AddSecondaryAreaVC)  {
        collectionSelectedFile.delegate = theDelegate
        collectionSelectedFile.dataSource = theDelegate
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.reloadData()
    }

    func setObserver(theController:AddSecondaryAreaVC)  {
//        tableView.addObserver(theController, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        collectionSelectedFile.addObserver(theController, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        tblNotesHistory.addObserver(theController, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    func removeObserver(theController:AddSecondaryAreaVC) {
//        tableView.removeObserver(theController, forKeyPath: "contentSize")
        collectionSelectedFile.removeObserver(theController, forKeyPath: "contentSize")
        tblNotesHistory.removeObserver(theController, forKeyPath: "contentSize")
    }
    func loadPrimaryResultActivity(isStart:Bool) {
        btnSelectPrimaryResult.loadingIndicator(isStart, allignment: .right)
    }
    func loadCompanyActivity(isStart:Bool) {
        btnSelectCompany.loadingIndicator(isStart, allignment: .right)
    }
    func loadAssignToActivity(isStart:Bool) {
        btnAssigned.loadingIndicator(isStart, allignment: .right)
    }
    func loadDepartmentActivity(isStart:Bool) {
        btnDepartment.loadingIndicator(isStart, allignment: .right)
    }
    
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            btnAdd.startLoadyIndicator()
            self.allButtons(isEnable: false)
        } else {
            btnAdd.stopLoadyIndicator()
            self.allButtons(isEnable: true)
        }
    }
    
    func showUpdateData(theModel:JSON) {
        txtTitle.text = theModel["title"].string
        txtDescription.text = theModel["description"].string
        lblPrimaryResult.text = theModel["primaryresult"].stringValue.isEmpty ? "Select primary result" : theModel["primaryresult"].stringValue
        lblCompany.text = theModel["company"].stringValue.isEmpty ? "Select company" : theModel["company"].stringValue
        lblAssignTo.text = theModel["assignto"].stringValue.isEmpty ? "Select assigned to" : theModel["assignto"].stringValue
        lblDepartment.text = theModel["department"].stringValue.isEmpty ? "Select department" : theModel["department"].stringValue
        
        
        lblCreatedBy.text = theModel["createdby"].stringValue.isEmpty ? "-" : theModel["createdby"].stringValue
        lblCreatedDate.text = theModel["createdDate"].stringValue.isEmpty ? "-" : theModel["createdDate"].stringValue
    }
    
    func showDropDownUpdatedData(theModel:JSON) {
        lblPrimaryResult.text = theModel["primaryresult"].stringValue.isEmpty ? "Select primary result" : theModel["primaryresult"].stringValue
        lblCompany.text = theModel["company"].stringValue.isEmpty ? "Select company" : theModel["company"].stringValue
        lblAssignTo.text = theModel["assignto"].stringValue.isEmpty ? "Select assigned to" : theModel["assignto"].stringValue
        lblDepartment.text = theModel["department"].stringValue.isEmpty ? "Select department" : theModel["department"].stringValue
    }
    
    //Rx
    func rxTitleTextFieldAction() {
        txtTitle.rx.text.orEmpty
            .subscribe({ [weak self] text in
                (self?.viewNextPresentingViewController() as? AddSecondaryAreaVC)?.theCurrentViewModel.selectedData["title"].stringValue = text.element ?? ""
            }).disposed(by:disposeBag)
    }
    func rxDescriptionTextViewDidChange() {
        txtDescription.rx.text.orEmpty
            .subscribe({ [weak self] event in
                (self?.viewNextPresentingViewController() as? AddSecondaryAreaVC)?.theCurrentViewModel.selectedData["description"].stringValue = event.element ?? ""
                if let obj = self {
                    obj.lblPlaceHolder.isHidden = obj.txtDescription.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    func isEnableView(isEnable:Bool,view:UIView) {
        view.isUserInteractionEnabled = isEnable ? true : false
        view.alpha = isEnable ? 1.0 : 0.6
    }
}
