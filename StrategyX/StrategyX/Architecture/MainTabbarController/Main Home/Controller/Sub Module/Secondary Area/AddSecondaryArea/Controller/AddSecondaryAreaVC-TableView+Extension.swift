//
//  AddSecondaryAreaVC-TableView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension AddSecondaryAreaVC:UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return theCurrentViewModel.arr_notesHistoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        if theCurrentViewModel.arr_notesHistoryList?.count != nil{
            let data = theCurrentViewModel.arr_notesHistoryList![indexPath.row]
            var strF = NSAttributedString(string: "")
            if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                strF = "\(createdDate) ".attributedString(color: black, font: R14)!
            }
            //                    attributedString(string: "\(data.createdDate) ", color: black, font: R14)!
            var strS = NSAttributedString(string: "")
            //            strS = data.actionUserName
            strS = data.actionUserName.attributedString(color: black, font: R14)!
            //                    attributedString(string: data.actionUserName, color: black, font: R14)!
            var strT = NSAttributedString(string: "")
            var strFt = NSAttributedString(string: "")
            var strSx = NSAttributedString(string: "")
            var strSth = NSAttributedString(string: "")
            
            switch data.historyStatus{
            case APIKey.key_Assigned:
                cell.imgPercentage.image = UIImage.init(named: "ic_contact")
                cell.vwOuter.backgroundColor = appColorFilterBlue
                //                    strT = "Assigned to"
                strT = " Assigned to ".attributedString(color:black, font: R14)!
                //                    strFt = data.assignedUserName
                
                strFt = " \(data.assignedUserName) ".attributedString(color: black, font: R14)!
                break
            case APIKey.key_Percentage:
                cell.imgPercentage.image = UIImage.init(named: "ic_percentage_icon")
                cell.vwOuter.backgroundColor = appColorFilterBlack
                //                    strT = "Completed \(data.percentageCompleted)%"
                strT = " Completed \(data.percentageCompleted)% ".attributedString(color: black, font: R14)!
                break
            case APIKey.key_Commented:
                cell.imgPercentage.image = UIImage.init(named: "ic_meassage")
                cell.vwOuter.backgroundColor = appColorFilterBlue
                //                    strF = data.actionUserName
                //                    let attachment = NSTextAttachment()
                //                    let img = UIImage(named: "ic_time_icon")
                //                    img?.size = CGSize(width: 12.0, height: 12.0)
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                //                    attachment.image?.size = CGSize(width: 12.0, height: 12.0)
                strFt = " \(data.actionUserName) ".attributedString(color: black, font: R14)!
                strS = NSAttributedString(string: "")
                strT = NSAttributedString(string: "")
                //                    strFt = NSAttributedString(attachment: attachment)
                //                    if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                //                        strSx = "\(createdDate)".attributedString(color: black, font: R14)!
                //                    }
                strSx = ":".attributedString(color: black, font: R14)!
                strSth = " \(data.notesHistoryDescription) ".attributedString(color: black, font: R14)!
                //                    strSth = "Notes Text \(data.noteId)"
                break
            case APIKey.key_Revised:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                strT = " Revised Due Date To ".attributedString(color: black, font: R14)!
                //                    strT = "Revised Due Date To"
                //                    let attachment = NSTextAttachment()
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                
                strFt = NSAttributedString(string: " ")
                //                    strFt = NSAttributedString(attachment: attachment)
                if let revisedDate = data.revisedDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strSx = "\(revisedDate)".attributedString(color: black, font: R14)!
                }
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Worked:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                strT = " Revised Work Date To ".attributedString(color: black, font: R14)!
                //                    strT = "Revised Work Date To"
                //                    let attachment = NSTextAttachment()
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                strFt = NSAttributedString(string: " ")
                if let workedDate = data.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strSx = "\(workedDate) ".attributedString(color: black, font: R14)!
                }
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Opened:
                cell.imgPercentage.image = UIImage.init(named: "ic_left_icon")
                cell.vwOuter.backgroundColor = appColorFilterGreen
                strT = " Opened an Action. ".attributedString(color: black, font: R14)!
                //                    strT = "Opened an Action."
                strFt = NSAttributedString(string: "")
                strSx = NSAttributedString(string: "")
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Closed:
                cell.imgPercentage.image = UIImage.init(named: "ic_right_icon")
                cell.vwOuter.backgroundColor = appColorFilterRed
                strT = " Closed an Action. ".attributedString(color: black, font: R14)!
                //                    strT = "Closed an Action."
                strFt = NSAttributedString(string: "")
                strSx = NSAttributedString(string: "")
                strSth = NSAttributedString(string: "")
                break
            default:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                break
            }
            let FormatedString = NSMutableAttributedString()
            
            FormatedString.append(strF)
            FormatedString.append(strS)
            FormatedString.append(strT)
            FormatedString.append(strFt)
            FormatedString.append(strSx)
            FormatedString.append(strSth)
            cell.lblHistory.attributedText = FormatedString
        }
        return cell
        
    }
   
}

//MARK:-UITableViewDelegate
extension AddSecondaryAreaVC:UITableViewDelegate {
    
}
