//
//  AddCriticalSuccessFactorViewModel.swift
//  StrategyX
//
//  Created by Haresh on 16/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import Foundation
import SwiftyJSON

class AddCriticalSuccessFactorViewModel {
    
    //MARK:-Variable
    fileprivate weak var theController:AddCriticalSucessFactorVC!
    var imageBG:UIImage?

    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?
    
    var arr_selectedFile:[AttachmentFiles] = []
    var arr_RemoveSelectedFile:[AttachmentFiles] = []
    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    var strNoteStatus:String = ""
    var arr_noteHistoryStatus:[JSON] = []
    
    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","title":"","duedate":""])

    var isEdit = false
    var theEditSuccessFactorDetailModel:SuccessFactorDetail?

    // Related to
    var relatedToSelectionType = AddActionModel.relatedToSelectionType.general
    var isForAddAction = false

    // Related to List
    var arr_relatedList:[RelatedTo] = []
    var strSelectedRelatedId = ""
    
    // Related to PrimaryArea List
    var arr_actionPrimaryAreaList:[PrimaryArea] = []
    var strSelectedPrimaryAreaId = ""
    
    // Related to SecondryArea List
    var arr_actionSecondryAreaList:[SecondaryArea] = []
    var strSelectedSecondryAreaId = ""
    
    // Related to StrategyGoal List
    var arr_actionStrategyGoalList:[StrategyGoalList] = []
    var strSelectedStrategyGoalId = ""
    
    var selectionRelatedToDueDate = ""
    var selectionRelatedToDueDateColor = ""
    var selectionRelatedToActionID = ""
    
    var cellType:[AddActionModel.celltype] = [.header,.createdBy,.title,.relatedToGeneral,.tag,.company,.assignTo,.department,.approvedBy,.createdDate,.dueDate,.workDate,.duration,.subtask,.attachment,.bottom]
    
    enum categorySelectionType:Int {
        case company             = 2
        case assignTo            = 3
        case department          = 4
    }
    
    //MARK:- LifeCyle
    init(theController:AddCriticalSucessFactorVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
         (theController.view as? AddCriticalSucessFactorView)?.btnSelectCompanyOutlet.isUserInteractionEnabled = false
         }*/
        if isEdit {
            (theController.view as? AddCriticalSucessFactorView)?.btnSelectCompany.isUserInteractionEnabled = false
        } else {
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                (theController.view as? AddCriticalSucessFactorView)?.btnSelectCompany.isUserInteractionEnabled = false
            }
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService()
            
        } else {
            strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            selectedData["company"].stringValue = Utility.shared.userData.companyName
            (theController.view as? AddCriticalSucessFactorView)?.lblCompany.text = Utility.shared.userData.companyName
            (theController.view as? AddCriticalSucessFactorView)?.viewCompany.isHidden = true
            assignUserListService()
        }
    }
    
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        (theController.view as? AddCriticalSucessFactorView)?.activitySpinner.hidesWhenStopped = true
        (theController.view as? AddCriticalSucessFactorView)?.constrainViewPagerHeight.constant = 40.0
        (theController.view as? AddCriticalSucessFactorView)?.viewOfPager.isHidden = false
        
        if isAnimating {
            (theController.view as? AddCriticalSucessFactorView)?.activitySpinner.startAnimating()
            (theController.view as? AddCriticalSucessFactorView)?.viewMore.isHidden = true
        } else {
            (theController.view as? AddCriticalSucessFactorView)?.activitySpinner.stopAnimating()
            (theController.view as? AddCriticalSucessFactorView)?.viewMore.isHidden = false
            
            if nextOffset == -1 {
                (theController.view as? AddCriticalSucessFactorView)?.viewOfPager.isHidden = true
                (theController.view as? AddCriticalSucessFactorView)?.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    
    func showTheEditData() {
        guard let theModel = theEditSuccessFactorDetailModel else { return }
        (theController.view as? AddCriticalSucessFactorView)?.lblAddTitle.text = "Edit Success Factor"
        
        (theController.view as? AddCriticalSucessFactorView)?.btnReportTitle.isHidden = false

        selectedData["title"].stringValue = theModel.criticalSuccessFactorName
        selectedData["description"].stringValue = theModel.descriptionSuccessFactor
        selectedData["company"].stringValue = theModel.companyName
        selectedData["assignto"].stringValue = theModel.assignedTo
        selectedData["department"].stringValue = theModel.departmentName
        selectedData["createdby"].stringValue = theModel.createdBy
        selectedData["reportTitle"].stringValue = theModel.reportTitle
        selectedData["reportURL"].stringValue = theModel.reportUrl
        selectedData["relatedto"].stringValue = theModel.label
        strSelectedRelatedId = "\(theModel.relatedId)"
        
        let createdDate = theModel.createdDate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let createdDates = createdDate {
            selectedData["createdDate"].stringValue = createdDates.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        strSelectedCompanyID = theEditSuccessFactorDetailModel?.companyId ?? Utility.shared.userData.companyId
        strSelectedUserAssignID = theModel.assigneeId
        strSelectedDepartmentID = theModel.departmentId
        
        (theController.view as? AddCriticalSucessFactorView)?.txtTitle.isUserInteractionEnabled = false
        (theController.view as? AddCriticalSucessFactorView)?.txtDescription.isUserInteractionEnabled = false
        
        (theController.view as? AddCriticalSucessFactorView)?.showUpdateData(theModel: selectedData)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectedCompany)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionAssignTo)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionDepartment)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionReportTitle)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionReportURL)
        
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewRelatedToTxt)
        (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: false, view: (theController.view as? AddCriticalSucessFactorView)!.viewCommonRelatedToTxt)
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToTitle.text = theModel.label
        (theController.view as? AddCriticalSucessFactorView)?.lblRelatedTo.text = theModel.label
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedTo.text = theModel.related_title
        (theController.view as? AddCriticalSucessFactorView)?.viewCommanRelatedTo.isHidden = false
        if theModel.relatedId == 0 {
            (theController.view as? AddCriticalSucessFactorView)?.viewCommanRelatedTo.isHidden = true
        }

        DispatchQueue.main.async { [weak self] in
            (self?.theController.view as? AddCriticalSucessFactorView)?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        arr_selectedFile.removeAll()
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        if UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdById) {
            (theController.view as? AddCriticalSucessFactorView)?.txtTitle.isUserInteractionEnabled = true
            (theController.view as? AddCriticalSucessFactorView)?.txtDescription.isUserInteractionEnabled = true
            
            (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: true, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionAssignTo)
            (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: true, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionDepartment)
//            (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: true, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionReportTitle)
            (theController.view as? AddCriticalSucessFactorView)?.isEnableView(isEnable: true, view: (theController.view as? AddCriticalSucessFactorView)!.viewSelectionReportURL)

        }
        
    }
    
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            strSelectedCompanyID = arr_companyList[index].id
            assignUserListService()
            
            selectedData["company"].stringValue = str
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            strSelectedUserAssignID = arr_assignList[index].user_id
            selectedData["assignto"].stringValue = str
            departmentListService()
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            break
        case .department:
            strSelectedDepartmentID = arr_departmentList[index].id
            selectedData["department"].stringValue = str
            break
            
        }
        (theController.view as? AddCriticalSucessFactorView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateDropDownData(categorySelection:AddActionVC.categorySelectionType, str:String, cellType:AddActionModel.celltype, index:Int)  {
        var index = 0
        if let ind = self.cellType.firstIndex(where: {$0 == cellType}) {
            index = ind
        }
        
        print("selected index \(index)")
        switch categorySelection {
        case .company:
            break
        case .department:
            break
        case .assignTo:
            break
        case .relatedTo:
            if let selectedIndex = arr_relatedList.firstIndex(where: {$0.option == str}) {
                strSelectedRelatedId = "\(arr_relatedList[selectedIndex].value)"
            }
            resetRelatedData()
            removeRelatedCellFromCelltype()
            updateViewCommanRelatedTo(str: str)
            
            self.selectedData["relatedto"].stringValue = str
            (theController.view as? AddCriticalSucessFactorView)?.lblRelatedTo.text = str
            return
        case .approvedBy:
            break
        case .relatedToStrategyPrimaryArea:
            if let selectedIndex = arr_actionPrimaryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedPrimaryAreaId = arr_actionPrimaryAreaList[selectedIndex].id
            }
            self.selectedData["strategyprimaryarea"].stringValue = str
            (theController.view as? AddCriticalSucessFactorView)?.lblPrimaryArea.text = str
            strSelectedSecondryAreaId = ""
            self.selectedData["strategysecondaryarea"].stringValue = ""
            (theController.view as? AddCriticalSucessFactorView)?.lblSecondaryArea.text = "Select secondary area"
            arr_actionSecondryAreaList.removeAll()
            
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            
            actionSecondaryAreaListService()
            
            
            break
        case .relatedToStrategySecondaryArea:
            if let selectedIndex = arr_actionSecondryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedSecondryAreaId = arr_actionSecondryAreaList[selectedIndex].id
            }
            self.selectedData["strategysecondaryarea"].stringValue = str
            (theController.view as? AddCriticalSucessFactorView)?.lblSecondaryArea.text = str
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            actionStrategyGoalListService()
            break
        case .relatedToStrategyGoal:
            (theController.view as? AddCriticalSucessFactorView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddCriticalSucessFactorView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionStrategyGoalList.firstIndex(where: {$0.title == str}) {
                strSelectedStrategyGoalId = arr_actionStrategyGoalList[selectedIndex].id
                (theController.view as? AddCriticalSucessFactorView)?.viewStrategyGoalRelatedToDueDate.isHidden = false
                selectionRelatedToDueDate = arr_actionStrategyGoalList[selectedIndex].duedate
                selectionRelatedToActionID = arr_actionStrategyGoalList[selectedIndex].id
                if UserDefault.shared.isArchiveAction(strStatus: arr_actionStrategyGoalList[selectedIndex].status) {
                    (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                    (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
                } else {
                    (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                    (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
                }
                if let view = (theController.view as? AddCriticalSucessFactorView) {
                    view.updateDateView(label: (view.lblStrategyGoalRelatedToDueDate), strDueDate: arr_actionStrategyGoalList[selectedIndex].duedate, color: arr_actionStrategyGoalList[selectedIndex].duedateColor)
                }
            }
            self.selectedData["strategygoal"].stringValue = str
            (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoal.text = str
            break
        case .relatedToRisk:
            break
        case .relatedToTacticalProject:
            break
        case .relatedToFocus:
            break
        case .relatedToSuccessFactor:
            break
        case .selectTag:
            break
        case .relatedToIdea:
            break
        case .relatedToProblem:
            break
        }
    }
    
    func resetRelatedData(){
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedTo.text = "Select"
        
        self.selectedData["strategygoal"].stringValue = ""
        strSelectedStrategyGoalId = ""
        (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoal.text = "Select strategy goal"
        
        self.selectedData["strategysecondaryarea"].stringValue = ""
        strSelectedSecondryAreaId = ""
        (theController.view as? AddCriticalSucessFactorView)?.lblSecondaryArea.text = "Select secondary area"
        
        self.selectedData["strategyprimaryarea"].stringValue = ""
        strSelectedPrimaryAreaId = ""
        (theController.view as? AddCriticalSucessFactorView)?.lblPrimaryArea.text = "Select primary area"
        
        arr_actionStrategyGoalList = []
        arr_actionPrimaryAreaList = []
        arr_actionSecondryAreaList = []
    }
    
    func removeRelatedCellFromCelltype() {
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToRisk}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToTacticalProject}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToFocus}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToSuccessFactor}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToIdea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToProblem}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.primaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.secondaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.strategyGoal}) {
            cellType.remove(at: index)
        }
        
    }
    
    func updateViewCommanRelatedTo(str:String) {
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToTitle.text = str
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedTo.text = "Select " + str
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToTitle.showAstrike = true
        (theController.view as? AddCriticalSucessFactorView)?.viewCommanRelatedTo.isHidden = "General Success Factor".lowercased() == str.lowercased()
        (theController.view as? AddCriticalSucessFactorView)?.viewPrimaryArea.isHidden = true
        (theController.view as? AddCriticalSucessFactorView)?.viewSecondaryArea.isHidden = true
        (theController.view as? AddCriticalSucessFactorView)?.viewStrategyGoal.isHidden = true
        selectionRelatedToDueDate = ""
        selectionRelatedToActionID = ""
        (theController.view as? AddCriticalSucessFactorView)?.lblStrategyGoalRelatedToDueDate.text = ""
        (theController.view as? AddCriticalSucessFactorView)?.lblCommanRelatedToDueDate.text = ""
        (theController.view as? AddCriticalSucessFactorView)?.viewCommonRelatedToDueDate.isHidden = true
        (theController.view as? AddCriticalSucessFactorView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
        
        
        switch str.localizedLowercase {
        case AddActionModel.relatedToSelectionType.general.rawValue.localizedLowercase, "General Success Factor".localizedLowercase:
            (theController.view as? AddCriticalSucessFactorView)?.viewCommanRelatedTo.isHidden = true
            relatedToSelectionType = AddActionModel.relatedToSelectionType.general
            break
        case AddActionModel.relatedToSelectionType.risk.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.tactical.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.focus.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.successFactor.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.idea.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.problem.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.strategy.rawValue.localizedLowercase:
            actionPrimaryAreaListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.strategy
            (theController.view as? AddCriticalSucessFactorView)?.viewCommanRelatedTo.isHidden = true
            (theController.view as? AddCriticalSucessFactorView)?.viewPrimaryArea.isHidden = false
            (theController.view as? AddCriticalSucessFactorView)?.viewSecondaryArea.isHidden = false
            (theController.view as? AddCriticalSucessFactorView)?.viewStrategyGoal.isHidden = false
            //            cellType.insert(AddActionModel.celltype.primaryArea, at: 4)
            //            cellType.insert(AddActionModel.celltype.secondaryArea, at: 5)
            //            cellType.insert(AddActionModel.celltype.strategyGoal, at: 6)
            break
        default:
            break
        }
    }
    
    func updateSuccessFactorDetailModel(theModel:SuccessFactorDetail) {
        
        theEditSuccessFactorDetailModel?.criticalSuccessFactorName = selectedData["title"].stringValue
        theEditSuccessFactorDetailModel?.descriptionSuccessFactor = selectedData["description"].stringValue
        
        theEditSuccessFactorDetailModel = theModel
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String, isGoal:Bool) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate, isGoal: isGoal)
    }
    func validateForm() {
        theController.view.endEditing(true)
        if selectedData["title"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter success factor")
            return
        } else if selectedData["company"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select company")
            return
        } else if selectedData["relatedto"].stringValue.isEmpty, !isEdit {
            self.theController.showAlertAtBottom(message: "Please select related to")
            return
        } else if !selectedData["relatedto"].stringValue.isEmpty, !isEdit {
            if strSelectedRelatedId == "1" {
                if selectedData["strategyprimaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select primary area")
                    return
                } else if selectedData["strategysecondaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select secondry area")
                    return
                } else if selectedData["strategygoal"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select strategy goal")
                    return
                }
            }
        }
        if selectedData["assignto"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select assigned to")
            return
        } else if selectedData["department"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select department")
            return
        } else if selectedData["reportTitle"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter report title")
            return
        } else if !selectedData["reportURL"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, !selectedData["reportURL"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).validateURL() {
            self.theController.showAlertAtBottom(message: "Please enter valid report url")
            return
        }
        
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var removeAttachment:[String] = []
        
        var voiceCount = 0
        var fileCount = 0
        
        for item in arr_RemoveSelectedFile {
            if item.isOldAttachment {
                removeAttachment.append(item.attachmentID)
            }
            //            removeFile.append([APIKey.key_id:item.attachmentID])
        }
        
        for i in 0..<arr_selectedFile.count {
            if arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
            } else {
                if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
        //        var param = parameter
        //        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")
        if isEdit {
            let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_success_factor_id:theEditSuccessFactorDetailModel?.id ?? "",
                             APIKey.key_critical_success_factor_name:selectedData["title"].stringValue.trimmed(),
                             APIKey.key_report_url:selectedData["reportURL"].stringValue.trimmed(),
                             APIKey.key_assigned:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_description:selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_remove_attechment:removeAttachment.joined(separator: ",")] as [String : Any]
            
            updateSuccessFactor(parameter: parameter,fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        } else {
            var notes = ""
            
            if let history = arr_notesHistoryList, history.count > 0 {
                var noteHistory:[[String:String]] = []
                history.forEach { (item) in
                    noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
                }
                notes = JSON(noteHistory).rawString() ?? ""
            }
            var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_critical_success_factor_name:selectedData["title"].stringValue.trimmed(),
                             APIKey.key_report_title:selectedData["reportTitle"].stringValue.trimmed(),
                             APIKey.key_report_url:selectedData["reportURL"].stringValue.trimmed(),
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_notes:notes,
                             APIKey.key_related_to:strSelectedRelatedId,
                             APIKey.key_description:selectedData["description"].stringValue.trimmed()] as [String : Any]
            if strSelectedRelatedId == "1" {
                parameter[APIKey.key_strategy_goal]  = strSelectedStrategyGoalId
            } 
            
            addSuccessFactor(parameter: parameter, fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        }
        
    }
}

//MARK:- Api Call
extension AddCriticalSuccessFactorViewModel {
    func companylistService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadCompanyActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadCompanyActivity(isStart: false)
            }, failed: { [weak self](error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadCompanyActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func assignUserListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadAssignToActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
            //            self?.theController.updateTableForDropDownStopLoading()
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadAssignToActivity(isStart: false)
            
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadAssignToActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    func departmentListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadDepartmentActivity(isStart: true)
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadDepartmentActivity(isStart: false)
            
            //            self?.theController.updateTableForDropDownStopLoading()
            if let obj = self, !obj.isEdit, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddCriticalSucessFactorView)?.showDropDownUpdatedData(theModel: obj.selectedData)
                //                obj.theController.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            } else if let obj = self, obj.isEdit, obj.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddCriticalSucessFactorView)?.showDropDownUpdatedData(theModel: obj.selectedData)
            }
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadDepartmentActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    
    func relatedListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (theController.view as? AddCriticalSucessFactorView)?.btnRelatedTo, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_related_type:APIKey.key_critical_success_factors]
        ActionsWebService.shared.getRelatedTo(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnRelatedTo, isStart: false)
            self?.arr_relatedList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnRelatedTo, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionPrimaryAreaListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (theController.view as? AddCriticalSucessFactorView)?.btnPrimaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getPrimaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnPrimaryArea, isStart: false)
            self?.arr_actionPrimaryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnPrimaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSecondaryAreaListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (theController.view as? AddCriticalSucessFactorView)?.btnSecondaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_primary_area_id:strSelectedPrimaryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getSecondaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnSecondaryArea, isStart: false)
            self?.arr_actionSecondryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnSecondaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionStrategyGoalListService() {
        (theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (theController.view as? AddCriticalSucessFactorView)?.btnStrategyGoal, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_secondary_area_id:strSelectedSecondryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getStrategyGoalList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnStrategyGoal, isStart: false)
            self?.arr_actionStrategyGoalList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.loadActivityOnButton(button: (self?.theController.view as? AddCriticalSucessFactorView)?.btnStrategyGoal, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String, isGoal:Bool) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        switch relatedToSelectionType {
        case .general:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
            break
        case .strategy:
            parameter[APIKey.key_date_for] = APIKey.key_strategy_goals
            break
        case .risk:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_risks
            break
        case .tactical:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_tactical_projects
            break
        case .focus:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_focuses
            break
        case .idea:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
        case .problem:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
            
        case .successFactor:
            break
        }
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        Utility.shared.showActivityIndicator()
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            if let obj = self, let model = theModel {
                obj.selectionRelatedToDueDate = model.revisedDate
                if let view = obj.theController.view as? AddCriticalSucessFactorView {
                    view.updateDateView(label: isGoal ? view.lblStrategyGoalRelatedToDueDate : view.lblCommanRelatedToDueDate, strDueDate: model.revisedDate, color: model.revisedColor)
                }
                
                if obj.isForAddAction {
                    obj.theController.handlerEditDueDateChanged(model.revisedDate,model.revisedColor)
                }
            }
            Utility.shared.stopActivityIndicator()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theEditSuccessFactorDetailModel?.id ?? "",
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_critical_success_factors,
                         APIKey.key_page_offset:"\(nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            (self?.theController.view as? AddCriticalSucessFactorView)?.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)
        })
    }
    
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theEditSuccessFactorDetailModel?.id ?? "",
                         APIKey.key_history_for: APIKey.key_critical_success_factors]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }
    
    func addSuccessFactor(parameter:[String:Any], fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: true)
        SuccessFactorWebservice.shared.addSuccessFactor(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
            (self?.theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: false)
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.theController.updateBottomCell()
        })
    }
    
    func updateSuccessFactor(parameter:[String:Any],fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: true)
        SuccessFactorWebservice.shared.updateSuccessFactor(parameter: parameter,files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theDetailModel) in
            self?.updateSuccessFactorDetailModel(theModel:theDetailModel)
            (self?.theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: false)
            self?.theController.handlerUpdateModel(self?.theEditSuccessFactorDetailModel)
            self?.theController.backAction(isAnimation: false)
            }, failed: {[weak self] (error) in
                (self?.theController.view as? AddCriticalSucessFactorView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
                //                self?.theController.updateBottomCell()
        })
    }
    
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.strNoteStatus ?? "")
            (self?.theController.view as? AddCriticalSucessFactorView)?.txtNote.text = ""
            completion()
            }, failed: { [weak self] (error) in
                completion()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
