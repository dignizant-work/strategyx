//
//  AddCriticalSucessFactorVC-CollectionView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 16/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

//MARK:- UICollectionView datasource & delegate

extension AddCriticalSucessFactorVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_selectedFile.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
        /*if theCurrentViewModel.theEditSuccessFactorDetailModel?.focusStatus.lowercased() == APIKey.key_archived.lowercased() {
            cell.btnCloseOutlet.isHidden = true
        }*/
        
        //        cell.bgView.setBackGroundColor = theCurrentViewModel.arr_selectedFile[indexPath.row].fileType == .audio ? 3 : 8
        
        cell.lblFileName.text = (theCurrentViewModel.arr_selectedFile[indexPath.row].fileName as NSString).lastPathComponent
        
        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveAttachmentAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentViewModel.arr_selectedFile[indexPath.row].fileName.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName)
        } else {
            presentWebViewVC(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName, isLocal: !theCurrentViewModel.arr_selectedFile[indexPath.row].isOldAttachment)
        }
    }
    
}
