//
//  AddCompanyModel.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddCompanyModel {

    //MARK:- Variable
    enum cellType:Int {
        case header = 0,companyName,companyAbbrivation,companyWebsite,companyLogo,companyProfile,companyVision,companyMission,companyValues,companyCulture,headerCompanyAddress,address,country,state,city,headerCompanyInfo,email,timezone,firstName,lastName,mobile,department,bottom
    }
    
    fileprivate weak var theController:AddCompanyVC!
    var theModelCompanyDetail:CompanyDetail? = nil {
        didSet {
            if let vcs = theController.navigationController?.viewControllers, let index = vcs.firstIndex(where: { ($0 as? CompanyProfileVC) != nil }) {
               (vcs[index] as? CompanyProfileVC)?.updatetheModel(theModel: theModelCompanyDetail!)
            }
        }
    }
    var isForEdit:Bool = false
    var tableViewCellType:[cellType] = [.header,.companyName,.companyAbbrivation,.companyWebsite,.companyLogo,.companyProfile,.companyVision,.companyMission,.companyValues,.companyCulture,.headerCompanyAddress,.address,.country,.state,.city,.headerCompanyInfo,.email,.timezone,.firstName,.lastName,.mobile,.department,.bottom]
    
    var tableviewCount = 25
    var imageBG:UIImage?
    var isAddCompanyApiLoading = false
    
    var isTimeZoneDropDownLoadingAtIndex:Int = -1
    var isStateDropDownLoadingAtIndex:Int = -1
    var isStreetDropDownLoadingAtIndex:Int = -1
    var isSuburbDropDownLoadingAtIndex:Int = -1
    
    var arr_StreetList:[Street] = []
    
    var arr_SuburbList:[Suburbs] = []
    
    var arr_StateList:[State] = []
    
    var arr_TimezoneList:[Timezone] = []
    
    // Selected File Data
    
    var arr_selectedFile:[SelectedFile] = []
    
    /*var arr_StreetType:[String] = ["Street 1","Street 2","Street 3","Street 4"]
    var arr_StateType:[String] = ["State 1","State 2","State 3","State 4"]
    var arr_SuburbType:[String] = ["Suburb 1","Suburb 2","Suburb 3","Suburb 4"]
    var arr_AdminTimeZone:[String] = ["Zone 1","Zone 2","Zone 3","Zone 4"]*/
    var img_Profile:UIImage? 
    var img_ProfileData:Data?

    
    var selectedData:JSON = JSON(["companyname":"","companyabbreviation":"","companywebsite":"","companyprofile":"","companyvision":"","companymission":"","companyvalues":"","companyculture":"","unitnumber":"","streetname":"","streettype":"","state":"","suburb":"","postcode":"","adminemail":"","admintimezone":"","adminfirstname":"","adminlastname":"","mobile":"","departmentname":""])
    
    var selectedDataId:JSON = JSON(["state_id":"","suburb_id":"","timezone_id":"","street_id":""])
    
    //MARK:- LifeCycle
    init(theController:AddCompanyVC) {
        self.theController = theController
    }
    
}
