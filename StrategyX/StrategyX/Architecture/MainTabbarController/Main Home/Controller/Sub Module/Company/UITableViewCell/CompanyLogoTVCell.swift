//
//  CompanyLogoTVCell.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class CompanyLogoTVCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDashBorder: UILabel!
    
    
    var handlerProfileAction:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
            self.lblDashBorder.addDashedBorder(color: appGreen, cornerRadius:30)
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(img:UIImage?, imageData:Data?) {
        if let imgData = imageData {
            imgProfile.image = UIImage.animatedImage(data:imgData)
        } else if let image = img {
            imgProfile.image = image
        }
    }
    func configureLoadImage(strImg:String) {
        lblDashBorder.text = ""
        imgProfile.sd_setImageLoadMultiTypeURL(url: strImg)
    }
    //MARK:- IBAction
    @IBAction func onBtnProfileAction(_ sender: Any) {
        handlerProfileAction()
    }

}
