//
//  AddCompanyVC.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import GooglePlaces

class AddCompanyVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case street                 = 1
        case state                  = 2
        case suburb                 = 3
        case adminTimezone          = 4
    }
    
    fileprivate lazy var theCurrentView:AddCompanyView = { [unowned self] in
       return self.view as! AddCompanyView
    }()
    fileprivate lazy var theCurrentModel:AddCompanyModel = {
        return AddCompanyModel(theController: self)
    }()
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        getTimezoneListService()
//        getStreetListService()
//        getStateListService()
        if theCurrentModel.isForEdit {
            theCurrentModel.tableViewCellType.remove(at: 6)
            theCurrentModel.tableViewCellType.remove(at: 6)
            theCurrentModel.tableViewCellType.remove(at: 6)
            theCurrentModel.tableViewCellType.remove(at: 6)
            setTheEditData()
        }
    }
    
    func updateTimezoneCellForDropDownStopLoading(Index:Int) {
        let index = IndexPath(row: Index, section: 0)
       // theCurrentModel.isDropDownLoadingAtIndex = -1
        theCurrentView.tableView.reloadRows(at: [index], with: .none)
    }
    func setTheData(imgBG:UIImage?,theCompanyDetailModel:CompanyDetail? = nil, isForEdit:Bool = false) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theModelCompanyDetail = theCompanyDetailModel
        theCurrentModel.isForEdit = isForEdit
    }
    func setTheEditData() {
        guard let theModel = theCurrentModel.theModelCompanyDetail else { return }
        theCurrentModel.selectedData["companyname"].stringValue = theModel.companieName
        theCurrentModel.selectedData["companyabbreviation"].stringValue = theModel.abbreviation
        theCurrentModel.selectedData["companywebsite"].stringValue = theModel.website
        theCurrentModel.selectedData["companyprofile"].stringValue = theModel.descriptionCompany
        theCurrentModel.selectedDataId["timezone_id"].stringValue = theModel.timezoneId
        theCurrentModel.selectedData["adminfirstname"].stringValue = theModel.userFirstname
        theCurrentModel.selectedData["adminemail"].stringValue = theModel.userEmail
        theCurrentModel.selectedData["adminlastname"].stringValue = theModel.userLastname
        theCurrentModel.selectedData["mobile"].stringValue = theModel.userMobile
        theCurrentModel.selectedData["address"].stringValue = theModel.mapAddress
        theCurrentModel.selectedData["state"].stringValue = theModel.state
        theCurrentModel.selectedData["city"].stringValue = theModel.city
        theCurrentModel.selectedData["country"].stringValue = theModel.country
        theCurrentModel.selectedData["admintimezone"].stringValue = theModel.timezoneName
        theCurrentModel.selectedData["departmentname"].stringValue = theModel.companyDepartment
        theCurrentView.tableView.reloadData()
    }
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .street:
            self.theCurrentModel.selectedData["streettype"].stringValue = str
            if let selectedIndex = theCurrentModel.arr_StreetList.firstIndex(where: {$0.streetName == str}) {
                theCurrentModel.selectedDataId["street_id"].stringValue = theCurrentModel.arr_StreetList[selectedIndex].id
            }
            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 13, section: 0)], with: UITableView.RowAnimation.none)
            break
        case .state:
            self.theCurrentModel.selectedData["state"].stringValue = str
            if let selectedIndex = theCurrentModel.arr_StateList.firstIndex(where: {$0.stateName == str}) {
                theCurrentModel.selectedDataId["state_id"].stringValue = theCurrentModel.arr_StateList[selectedIndex].id
                self.theCurrentModel.selectedData["suburb"].stringValue = ""
                self.theCurrentModel.selectedDataId["suburb_id"].stringValue = ""
                self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 15, section: 0)], with: UITableView.RowAnimation.none)
                getSuburbListService()
            }
            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 14, section: 0)], with: UITableView.RowAnimation.none)
            break
        case .suburb:
            self.theCurrentModel.selectedData["suburb"].stringValue = str
            if let selectedIndex = theCurrentModel.arr_SuburbList.firstIndex(where: {$0.suburbName == str}) {
                theCurrentModel.selectedDataId["suburb_id"].stringValue = theCurrentModel.arr_SuburbList[selectedIndex].id
            }
            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 15, section: 0)], with: UITableView.RowAnimation.none)
            break
        case .adminTimezone:
            self.theCurrentModel.selectedData["admintimezone"].stringValue = str
            self.theCurrentModel.selectedDataId["timezone_id"].stringValue = theCurrentModel.arr_TimezoneList[index].timezoneId
            if let index = theCurrentModel.tableViewCellType.firstIndex(where: {$0 == .timezone}) {
                UIView.performWithoutAnimation {
                    self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
                }
            }
            break
        }
    }
    func updateProfileImage(img:UIImage?,filename:String,imgData:Data?) {
        if imgData == nil {
            showAlertAtBottom(message: "File could not find")
            return
        }
        if let data = img?.pngData(), (data.count/1024) >= 2048 {
            showAlertAtBottom(message: "Please select image which is less than 2 MB")
            return
        }
        if let data = imgData, (data.count/1024) >= 2048 {
            showAlertAtBottom(message: "Please select image which is less than 2 MB")
            return
        }
        theCurrentModel.img_Profile = img
        theCurrentModel.img_ProfileData = imgData
        theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 4, section: 0)], with: UITableView.RowAnimation.none)
        
        let objSelectedData = SelectedFile.init(selectedFileName: filename, selectedFileData: imgData!)
        theCurrentModel.arr_selectedFile.append(objSelectedData)
    }
    func openImageGallery() {
        let imagePicker = ImagePickerManager()
        imagePicker.picker.allowsEditing = true
        imagePicker.pickImage(self) { [weak self] (image,filename,imageData)  in
            self?.updateProfileImage(img: image,filename:filename, imgData: imageData)
        }
    }
    func validateForm() {
        self.view.endEditing(true)
//        "companyname":"","companyabbreviation":"","companywebsite":"","companyprofile":"","companyvision":"","companymission":"","companyvalues":"","companyculture":"","unitnumber":"","streetname":"","streettype":"","state":"","suburb":"","postcode":"","adminemail":"","admintimezone":"","adminfirstname":"","adminlastname":"","mobile":"","departmentname":""
        
        if theCurrentModel.selectedData["companyname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter company name")
            return
        } else if theCurrentModel.selectedData["companyabbreviation"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter company abbreviation")
            return
        } else if !theCurrentModel.selectedData["companywebsite"].stringValue.isEmpty, !theCurrentModel.selectedData["companywebsite"].stringValue.validateURL() {
            self.showAlertAtBottom(message: "Please enter valid website url")
            return
        } else if theCurrentModel.selectedData["adminemail"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please enter admin email address")
            return
        } else if !theCurrentModel.selectedData["adminemail"].stringValue.trimmed().isValidEmail() {
            self.showAlertAtBottom(message: "Please enter valid admin email address")
            return
        } else if theCurrentModel.selectedData["admintimezone"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select admin timezone")
            return
        } else if theCurrentModel.selectedData["adminfirstname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter admin first name")
            return
        } else if theCurrentModel.selectedData["departmentname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter department name")
            return
        }
        
        updateBottomCell()

        
        var parameter = [
                        APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_name:theCurrentModel.selectedData["companyname"].stringValue.trimmed(),
                         APIKey.key_company_abbr:theCurrentModel.selectedData["companyabbreviation"].stringValue.trimmed(),
                         APIKey.key_timezone_id:theCurrentModel.selectedDataId["timezone_id"].stringValue,
                         APIKey.key_user_firstname:theCurrentModel.selectedData["adminfirstname"].stringValue.trimmed(),
                         APIKey.key_user_email:theCurrentModel.selectedData["adminemail"].stringValue.trimmed(),
                         APIKey.key_user_lastname:theCurrentModel.selectedData["adminlastname"].stringValue.trimmed(),
                         APIKey.key_mobile:theCurrentModel.selectedData["mobile"].stringValue.trimmed(),
                         APIKey.key_website:theCurrentModel.selectedData["companywebsite"].stringValue.trimmed(),
                         APIKey.key_description:theCurrentModel.selectedData["companyprofile"].stringValue.trimmed(),
                         APIKey.key_map_address:theCurrentModel.selectedData["address"].stringValue.trimmed(),
                         APIKey.key_map_state:theCurrentModel.selectedData["state"].stringValue.trimmed(),
                         APIKey.key_map_city:theCurrentModel.selectedData["city"].stringValue.trimmed(),
                         APIKey.key_map_country:theCurrentModel.selectedData["country"].stringValue.trimmed(),
//                         APIKey.key_unit_number:theCurrentModel.selectedData["unitnumber"].stringValue,
//                         APIKey.key_street_name:theCurrentModel.selectedData["streetname"].stringValue,
//                         APIKey.key_street_type:theCurrentModel.selectedDataId["street_id"].stringValue,
//                         APIKey.key_state:theCurrentModel.selectedDataId["state_id"].stringValue,
//                         APIKey.key_suburb:theCurrentModel.selectedDataId["suburb_id"].stringValue,
//                         APIKey.key_post_code:theCurrentModel.selectedData["postcode"].stringValue,
                         ]
        
        if theCurrentModel.isForEdit {
            parameter[APIKey.key_company_id] = theCurrentModel.theModelCompanyDetail?.id ?? ""
            editCompanyWithAttachment(parameter: parameter)
        } else {
            parameter[APIKey.key_user_type] = "\(UserDefault.shared.userRole.rawValue)"
            parameter[APIKey.key_vision_text] = theCurrentModel.selectedData["companyvision"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
            parameter[APIKey.key_mission_text] = theCurrentModel.selectedData["companymission"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
            parameter[APIKey.key_values_text] = theCurrentModel.selectedData["companyvalues"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
            parameter[APIKey.key_culture_text] = theCurrentModel.selectedData["companyculture"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
            parameter[APIKey.key_department_name] = theCurrentModel.selectedData["departmentname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
            self.addCompanyWithAttachment(parameter: parameter)
        }
        
        
        
    }
    
    func updateBottomCell() {
        let index = IndexPath(row: theCurrentModel.tableViewCellType.count - 1, section: 0)
        theCurrentModel.isAddCompanyApiLoading = !theCurrentModel.isAddCompanyApiLoading
        UIView.performWithoutAnimation {
            let contentOffset = theCurrentView.tableView.contentOffset
            theCurrentView.tableView.reloadRows(at: [index], with: .none)
            theCurrentView.tableView.contentOffset = contentOffset
        }
    }
    
    
    //MARK:- Redirect
    func presentDropDownListVC(categorySelection:categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .street:
            let arr_list:[String] = theCurrentModel.arr_StreetList.map({$0.streetName})
            vc.setTheData(strTitle: "Street Type",arr:arr_list)
            break
        case .state:
            let arr_list:[String] = theCurrentModel.arr_StateList.map({$0.stateName})
            vc.setTheData(strTitle: "State",arr:arr_list)
            break
        case .suburb:
            let arr_list:[String] = theCurrentModel.arr_SuburbList.map({$0.suburbName})
            vc.setTheData(strTitle: "Suburb",arr:arr_list)
            break
        case .adminTimezone:
            let arr_list:[String] = theCurrentModel.arr_TimezoneList.map({"\($0.timezoneName) (\($0.timezoneCode))"})
            vc.setTheData(strTitle: "Admin Timezone",arr:arr_list)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index:index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
}
//MARK:- UITableViewDataSource
extension AddCompanyVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.tableViewCellType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if theCurrentModel.tableViewCellType[indexPath.row] == .header {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderViewTVCell") as! HeaderViewTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: theCurrentModel.isForEdit ? "Edit Company" : "Add Company", isForGoal: false)

            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyName {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Name",isMandatory: true, strPlaceHolder: "Enter company name")
            cell.txtInputField.text = self.theCurrentModel.selectedData["companyname"].stringValue
            cell.inputFieldView(isEnable: true)
            cell.handlerText = { [weak self] (strData) in
                self?.theCurrentModel.selectedData["companyname"].stringValue = strData
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyAbbrivation {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Abbreviation",isMandatory: true, strPlaceHolder: "Enter company abbreviation")
            cell.inputFieldView(isEnable: true)
            cell.txtInputField.text = self.theCurrentModel.selectedData["companyabbreviation"].stringValue
            cell.handlerText = { [weak self] (strData) in
                self?.theCurrentModel.selectedData["companyabbreviation"].stringValue = strData
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyWebsite {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Company Website",isMandatory: false, strPlaceHolder: "Enter company website")
            cell.txtInputField.text = self.theCurrentModel.selectedData["companywebsite"].stringValue
            cell.handlerText = { [weak self] (strData) in
                self?.theCurrentModel.selectedData["companywebsite"].stringValue = strData
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyLogo {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyLogoTVCell") as! CompanyLogoTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if theCurrentModel.isForEdit {
                if theCurrentModel.img_ProfileData != nil {
                    cell.configure(img: theCurrentModel.img_Profile,imageData:theCurrentModel.img_ProfileData)
                } else {
                    cell.configureLoadImage(strImg: theCurrentModel.theModelCompanyDetail?.profile ?? "")
                }
            } else {
                cell.configure(img: theCurrentModel.img_Profile,imageData:theCurrentModel.img_ProfileData)
            }
            cell.handlerProfileAction = { [weak self] in
                self?.view.endEditing(true)
                self?.openImageGallery()
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyProfile {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Profile", strPlaceHolder: "Enter company profile",isMandatory:false)
            cell.txtDescription.text = self.theCurrentModel.selectedData["companyprofile"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["companyprofile"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyVision {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Vision", strPlaceHolder: "Enter company vision",isMandatory:false)
            cell.txtDescription.text = self.theCurrentModel.selectedData["companyvision"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["companyvision"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyMission {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Mission", strPlaceHolder: "Enter company mission",isMandatory:false)
            cell.txtDescription.text = self.theCurrentModel.selectedData["companymission"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["companymission"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyValues {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Values", strPlaceHolder: "Enter company values",isMandatory:false)
            cell.txtDescription.text = self.theCurrentModel.selectedData["companyvalues"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["companyvalues"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .companyCulture {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputDescriptionTVCell") as! InputDescriptionTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Company Culture", strPlaceHolder: "Enter company culture",isHideBottomLine: true,isMandatory:false)
            cell.txtDescription.text = self.theCurrentModel.selectedData["companyculture"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["companyculture"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .headerCompanyAddress {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSubTitleTVCell") as! HeaderSubTitleTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "COMPANY ADDRESS INFORMATION")
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .address {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldAddressTVCell") as! InputFieldAddressTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Address",isMandatory: true, strPlaceHolder: "Enter address")
            cell.lblAddress.text = self.theCurrentModel.selectedData["address"].stringValue
            cell.txtInputField.text = self.theCurrentModel.selectedData["address"].stringValue
            cell.handlerShouldBeingEditing = { [weak self]  in
                self?.openGooglePlacePicker()
//                self?.theCurrentModel.selectedData["address"].stringValue = strData
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .country {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Country",isMandatory: false, strPlaceHolder: "Enter country")
            cell.txtInputField.text = self.theCurrentModel.selectedData["country"].stringValue
            cell.handlerText = { [weak self] (strData) in
                self?.theCurrentModel.selectedData["country"].stringValue = strData
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .state {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "State", isMandatory:false, strPlaceHolder: "Enter state")
            cell.txtInputField.text = self.theCurrentModel.selectedData["state"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["state"].stringValue = text
            }
            /*
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTypeTVCell") as! SelectionTypeTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Street Type", strSelectionType: theCurrentModel.selectedData["streettype"].stringValue.isEmpty ? "Select street type" : theCurrentModel.selectedData["streettype"].stringValue, isMandatory: false)
            
            cell.handlerBtnClick = { [weak self] in
                self?.presentDropDownListVC(categorySelection: AddCompanyVC.categorySelectionType.street)
            }*/
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .city {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "City", isMandatory:false, strPlaceHolder: "Enter city")
            cell.txtInputField.text = self.theCurrentModel.selectedData["city"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["city"].stringValue = text
            }
            /*let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTypeTVCell") as! SelectionTypeTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            
            cell.configure(strTitle: "State", strSelectionType: theCurrentModel.selectedData["state"].stringValue.isEmpty ? "Select state" : theCurrentModel.selectedData["state"].stringValue, isMandatory: false)
            cell.handlerBtnClick = { [weak self] in
                self?.presentDropDownListVC(categorySelection: AddCompanyVC.categorySelectionType.state)
            }*/
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .headerCompanyInfo {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSubTitleTVCell") as! HeaderSubTitleTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "COMPANY ADMIN INFORMATION")
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .email {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Admin Email", isMandatory: true, strPlaceHolder: "Enter admin email",type:.emailAddress)
            cell.txtInputField.text = self.theCurrentModel.selectedData["adminemail"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["adminemail"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .timezone {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTypeTVCell") as! SelectionTypeTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(strTitle: "Admin Timezone", strSelectionType: theCurrentModel.selectedData["admintimezone"].stringValue.isEmpty ? "Select admin timezone" : theCurrentModel.selectedData["admintimezone"].stringValue, isMandatory: true)
            cell.handlerBtnClick = { [weak self] in
                self?.view.endEditing(true)
                self?.presentDropDownListVC(categorySelection: AddCompanyVC.categorySelectionType.adminTimezone)
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .firstName {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Admin First Name", isMandatory: true, strPlaceHolder: "Enter admin first name")
            cell.txtInputField.text = self.theCurrentModel.selectedData["adminfirstname"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["adminfirstname"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .lastName {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Admin Last Name", isMandatory: false, strPlaceHolder: "Enter admin last name")
            cell.txtInputField.text = self.theCurrentModel.selectedData["adminlastname"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["adminlastname"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .mobile {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: true)
            cell.configure(strTitle: "Mobile", isMandatory: false, strPlaceHolder: "Enter mobile",type:.asciiCapableNumberPad)
            cell.txtInputField.text = self.theCurrentModel.selectedData["mobile"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["mobile"].stringValue = text
            }
            return cell
        } else if theCurrentModel.tableViewCellType[indexPath.row] == .department {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputFieldTVCell") as! InputFieldTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.inputFieldView(isEnable: !theCurrentModel.isForEdit)
            cell.configure(strTitle: "Department Name", isMandatory: true, strPlaceHolder: "Enter department name")
            cell.txtInputField.text = self.theCurrentModel.selectedData["departmentname"].stringValue
            cell.handlerText = { [weak self] (text) in
                self?.theCurrentModel.selectedData["departmentname"].stringValue = text
            }
            return cell
        } else { // Last Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomButtonTVCell") as! BottomButtonTVCell
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            cell.configure(isLoading: theCurrentModel.isAddCompanyApiLoading)
            if theCurrentModel.isForEdit {
                cell.configure(isLoading: theCurrentModel.isAddCompanyApiLoading, strTitlebtnAdd: "Edit", strTitlebtnCancel: "Close")
            }
            cell.handlerAdd = { [weak self] in
                self?.validateForm()
            }
            cell.handlerCancel = { [weak self] in
                self?.backAction(isAnimation: false)
            }
            return cell
        }
    }
}
//MARK:-UITableViewDelegate
extension AddCompanyVC:UITableViewDelegate {
    /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     let cell = tableView.cellForRow(at: indexPath)
     let size = cell?.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
     return size?.height ?? UITableView.automaticDimension
     }*/
}
//MARK:- Google Place Picker

extension AddCompanyVC{
    func openGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
         filter.type = .address
         autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
}

//MARK:- Google Placepicker Delegate
extension AddCompanyVC:GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place ID: \(place.placeID ?? "")")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress ?? "")")
//        theCurrentView.txtAddress.text = place.formattedAddress ??  ""
//        theCurrentView.lblCompanyAddress.text = theCurrentView.txtAddress.text
        self.theCurrentModel.selectedData["address"].stringValue = place.formattedAddress ?? ""
        self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: 11, section: 0)], with: .none)
        getAddressFromLatLon(place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

//MARK:- get address
extension AddCompanyVC{
    func getAddressFromLatLon(_ coordinate:CLLocationCoordinate2D) {
        
        let ceo: CLGeocoder = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    self.theCurrentModel.selectedData["country"].stringValue = pm.country ?? ""
                    self.theCurrentModel.selectedData["state"].stringValue = pm.administrativeArea ?? ""
                    self.theCurrentModel.selectedData["city"].stringValue = pm.locality ?? ""
                    self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: 12, section: 0)], with: .none)
                    self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: 13, section: 0)], with: .none)
                    self.theCurrentView.tableView.reloadRows(at: [IndexPath(row: 14, section: 0)], with: .none)
                }
        })
        
    }
}

//MARK:- Call API
extension AddCompanyVC{
    func getStreetListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CompanyWebService.shared.getAllStreetList(parameter: parameter as [String : Any], success: { [weak self] (list) in          
            self?.theCurrentModel.arr_StreetList = list
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func getStateListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CompanyWebService.shared.getAllStateList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_StateList = list
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func getSuburbListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_state_id:theCurrentModel.selectedDataId["state_id"].stringValue]
        CompanyWebService.shared.getAllSuburbList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_SuburbList = list
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func getTimezoneListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        AddUserWebService.shared.getAllTimezoneList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_TimezoneList = list
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func addCompanyWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        
        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].selectedFileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].selectedFileName
                let mimeType = MimeType(path: path).value
//                let mimeExtension = mimeType.components(separatedBy: "/")
//                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].selectedFileData!)
                fileMimeType.append(mimeType)
                withName.append(APIKey.key_company_image)
                withFileName.append("\(path)")
            }
        }
    
        CompanyWebService.shared.uploadAddCompanyAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (msg) in
            self?.updateBottomCell()
//            self?.showAlertAtBottom(message: msg)
            self?.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell()
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
    func editCompanyWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        
        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].selectedFileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].selectedFileName
                let mimeType = MimeType(path: path).value
                //                let mimeExtension = mimeType.components(separatedBy: "/")
                //                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].selectedFileData!)
                fileMimeType.append(mimeType)
                withName.append(APIKey.key_company_image)
                withFileName.append("\(path)")
            }
        }
        
        CompanyWebService.shared.editCompanyAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (msg,theModel) in
            self?.updateBottomCell()
            self?.theCurrentModel.theModelCompanyDetail = theModel
            self?.showAlertAtBottom(message: msg)
            self?.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                self?.updateBottomCell()
                self?.showAlertAtBottom(message: error)
        })
        
    }
}
