//
//  AddCompanyView.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddCompanyView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
    }
    
    func setupTableView(theDelegate:AddCompanyVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(HeaderViewTVCell.self)
        tableView.registerCellNib(InputFieldTVCell.self)
        tableView.registerCellNib(InputDescriptionTVCell.self)
        tableView.registerCellNib(SelectionTypeTVCell.self)
        tableView.registerCellNib(BottomButtonTVCell.self)
        tableView.registerCellNib(CreatedByTVCell.self)
        tableView.registerCellNib(InputDateCell.self)
        tableView.registerCellNib(AddActionDateTVCell.self)
        tableView.registerCellNib(InputFieldAddressTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }

}
