//
//  AddGoalVC-CollectionView+Extension.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

//MARK:- UICollectionView datasource & delegate

extension AddGoalVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_selectedFile.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
//        if theCurrentViewModel.theEditPrimaryAreaModel?.status.lowercased() == APIKey.key_archived.lowercased() {
//            cell.btnCloseOutlet.isHidden = true
//        }
//        cell.bgView.setBackGroundColor = theCurrentViewModel.arr_selectedFile[indexPath.row].fileType == .audio ? 3 : 8
        
        cell.lblFileName.text = (theCurrentViewModel.arr_selectedFile[indexPath.row].fileName as NSString).lastPathComponent
        
        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveAttachmentAction(_:)), for: .touchUpInside)
        
        
        /*if theCurrentViewModel.arr_selectedAudioFile.count != 0 && theCurrentViewModel.arr_selectedAudioFile.count == 1 && indexPath.row == 0 {
         let selectFile = theCurrentViewModel.arr_selectedAudioFile[indexPath.row]
         cell.bgView.setBackGroundColor = 3
         cell.lblFileName.text = selectFile.selectedFileName
         cell.btnCloseOutlet.tag = indexPath.row
         cell.btnCloseOutlet.addTarget(self, action: #selector(btnRemoveAudioAction(_:)), for: .touchUpInside)
         } else {
         
         if theCurrentViewModel.arr_selectedAudioFile.count != 0 {
         cell.lblFileName.text = theCurrentViewModel.arr_selectedFile[indexPath.row - 1].selectedFileName
         } else {
         cell.lblFileName.text = theCurrentViewModel.arr_selectedFile[indexPath.row].selectedFileName
         }
         
         cell.btnCloseOutlet.tag = indexPath.row
         cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveFileAction(_:)), for: .touchUpInside)
         }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width 
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentViewModel.arr_selectedFile[indexPath.row].fileName.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName)
        } else {
            presentWebViewVC(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName, isLocal: !theCurrentViewModel.arr_selectedFile[indexPath.row].isOldAttachment)
        }
        /*if theCurrentViewModel.arr_selectedFile[indexPath.row].fileType == .audio {
            if !theCurrentViewModel.arr_selectedFile[indexPath.row].isOldAttachment, let audioData = theCurrentViewModel.arr_selectedFile[indexPath.row].fileData {
                if let url = self.setAudioFileToDocumentDirectory(audioData: audioData, audioNameWithExtension: "\(APIKey.key_Audio_name).\(APIKey.key_Extension)") {
                    self.presentAudioPlayer(url: url,isLocalURl: true)
                }
            } else {
                if theCurrentViewModel.arr_selectedFile[indexPath.row].fileName.contains(".wav") {
                    let fileName = theCurrentViewModel.arr_selectedFile[indexPath.row].fileName
                    let lastFile = (fileName as NSString).lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3")
                    print("lastFile:=",lastFile)
                    self.downloadAudioFileWebService(strURl: fileName) { [weak self] (fileData) in
                        if let url = self?.setAudioFileToDocumentDirectory(audioData: fileData, audioNameWithExtension: lastFile) {
                            self?.presentAudioPlayer(url: url,isLocalURl: true)
                        }
                    }
                } else {
                    self.presentAudioPlayer(url: theCurrentViewModel.arr_selectedFile[indexPath.row].fileName)
                }
            }
        }*/
    }
    
}
