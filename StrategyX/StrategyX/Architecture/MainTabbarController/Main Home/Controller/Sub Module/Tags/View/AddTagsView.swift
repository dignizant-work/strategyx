//
//  AddActionView.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Loady

class AddTagsView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtTagTitle: UITextField!
    @IBOutlet weak var btnAdd: LoadyButton!
    
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        
    }
    
    func setupTableView(theDelegate:AddTagsVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    
}
