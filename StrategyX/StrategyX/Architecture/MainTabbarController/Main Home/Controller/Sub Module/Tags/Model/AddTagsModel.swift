//
//  AddActionModel.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddTagsModel {

    //MARK:- Variable
    
    weak var theController:AddTagsVC!
    var imageBG:UIImage?
    var arr_TagList:[String] = []
   
    
    var selectedData:JSON = JSON(["category":"","company":"","department":"","description":"","assignto":"","relatedto":"","approvedby":"","projectname":"","createdate":"","duedate":"","workdate":""])
    
    
    
    //MARK:- LifeCycle
    init(theController:AddTagsVC) {
        self.theController = theController
    }
    
}
