//
//  AddActionVC.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddTagsVC: ParentViewController {

    //MARK:- Variable
    
    fileprivate lazy var theCurrentView:AddTagsView = { [unowned self] in
       return self.view as! AddTagsView
    }()
    
    fileprivate lazy var theCurrentModel:AddTagsModel = {
       return AddTagsModel(theController: self)
    }()
    var handlerAddTagData:() -> Void = {}
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height
            self.view.layoutIfNeeded()
            
            //            handlerUpdateTableView()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
    }
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    
    func updateTagList(strTag:String, index:Int, isDelete:Bool = false) {
        
        if isDelete {
            theCurrentModel.arr_TagList.remove(at: index)
        } else {
            if strTag.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                showAlertAtBottom(message: "Please enter tag title")
                return
            }
            
            theCurrentModel.arr_TagList.insert(strTag, at: 0)
        }
        
        self.theCurrentView.tableView.reloadData()
    }
    func validateForm() {
        self.view.endEditing(true)
        
        if theCurrentModel.arr_TagList.count <= 0{
            self.showAlertAtBottom(message: "Please add at least one tag")
            return
        }
        var arr_FinalTag:[JSON] = []
        for i in 0..<theCurrentModel.arr_TagList.count{
            var dict = JSON()
            dict[APIKey.key_name].stringValue = theCurrentModel.arr_TagList[i]
            arr_FinalTag.append(dict)
        }
        isClickOnAdd(isLoading: true)
        let strTagName = JSON(arr_FinalTag).rawString() ?? ""
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id: Utility.shared.userData.id,
                         APIKey.key_tag_name:strTagName]
        addTag(parameter: parameter)
    }
    func addTagModelInTagList() {
        if let index = self.navigationController?.viewControllers.firstIndex(where: {($0 as? SettingsVC) != nil}) {
            (self.navigationController?.viewControllers[index] as? SettingsVC)?.refreshTagListApi()
        }
    }
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            theCurrentView.btnAdd.startLoadyIndicator()
            self.view.allButtons(isEnable: false)
        } else {
            theCurrentView.btnAdd.stopLoadyIndicator()
            self.view.allButtons(isEnable: true)
        }
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnTagAction(_ sender: Any) {
        let text = theCurrentView.txtTagTitle.text ?? ""
        
        if(theCurrentModel.arr_TagList.contains(where: { (str) -> Bool in
            
            if(str.lowercased().contains(text.lowercased()))
            {
                return true
            }
            return false
            
        }))
        {
            self.showAlertAtBottom(message: "You already add this tag name")
            return
        }
        theCurrentView.txtTagTitle.text = nil
        updateTagList(strTag: text, index: 0)
    }
    
    @IBAction func onBtnAddAction(_ sender: Any) {
        validateForm()
    }
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
}
//MARK:- UITableViewDataSource
extension AddTagsVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return theCurrentModel.cellType.count
        return theCurrentModel.arr_TagList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.configure(strTitle: theCurrentModel.arr_TagList[indexPath.row])
        cell.handlerOnDeleteAction = { [weak self] (tag) in
            self?.updateTagList(strTag: "", index: indexPath.row, isDelete: true)
        }
        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension AddTagsVC:UITableViewDelegate {
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if theCurrentModel.cellType[indexPath.row] == AddActionModel.celltype.subtask {
            return theCurrentModel.subtaskCellHeight
        } else {
            return UITableView.automaticDimension
        }
    }*/
}

extension AddTagsVC{
    func addTag(parameter:[String:Any]) {
        AddTagWebService.shared.postAddTag(parameter: parameter, success: { [weak self] msg in
            self?.addTagModelInTagList()
            self?.isClickOnAdd(isLoading: false)
            self?.handlerAddTagData()
            self?.showAlertAtBottom(message: msg)
            self?.backAction(isAnimation: true)
        }) { [weak self] (error) in
            self?.isClickOnAdd(isLoading: false)
            self?.showAlertAtBottom(message: error)
        }
    }
}

