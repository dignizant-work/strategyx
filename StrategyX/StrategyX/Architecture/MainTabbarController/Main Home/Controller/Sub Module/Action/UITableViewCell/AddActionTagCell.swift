//
//  AddActionTagCell.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import HTagView

class AddActionTagCell: UITableViewCell {

    @IBOutlet weak var tagView: HTagView!
    @IBOutlet weak var constrainViewTagHeight: NSLayoutConstraint!
    

    var tagData:[String] = []
    
    var handlerTableViewUpdate:() -> Void = {}
    var handlerOnBtnAddTagAction:() -> Void = {}
    var handlerRemoveTagAction:(_ atIndex:Int) -> Void = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupTagView()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.constrainViewTagHeight.constant = self.tagView.frame.size.height
            self.contentView.layoutIfNeeded()
            self.handlerTableViewUpdate()
        }
    }
    /*
    override func layoutSubviews() {
        super.layoutSubviews()
        tagView.layoutIfNeeded()
        print("tagView.frame.size.height:=",tagView.frame.size.height)
        
        DispatchQueue.main.async {
            
//            self.contentView.layoutIfNeeded()
        }
        
        handlerTableViewUpdate()
    }*/
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(setTagData:[String]) {
        tagData = setTagData
        tagView.reloadData()
//        setupTagView()        
    }
    
    func setupTagView()  {
        tagView.delegate = self
        tagView.delegate = self
        tagView.dataSource = self
        tagView.multiselect = false
        tagView.marg = 2
        tagView.btwTags = 2
        tagView.btwLines = 2
        tagView.tagFont = R12
        tagView.tagMainBackColor = appBgLightGrayColor
        tagView.tagMainTextColor = white
        tagView.tagSecondBackColor = appBgLightGrayColor
        tagView.tagSecondTextColor = white
        // viewTag?.tagCancelIconRightMargin = 10
        tagView.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        
//        tagView.reloadData()
//        tagView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        /*
        [tagView].forEach { (viewTag) in
            viewTag?.delegate = self
            viewTag?.dataSource = self
            viewTag?.multiselect = false
            viewTag?.marg = 2
            viewTag!.btwTags = 2
            viewTag?.btwLines = 2
            viewTag?.tagFont = R12
            viewTag?.tagMainBackColor = appBgLightGrayColor
            viewTag?.tagMainTextColor = white
            viewTag?.tagSecondBackColor = appBgLightGrayColor
            viewTag?.tagSecondTextColor = white
            // viewTag?.tagCancelIconRightMargin = 10
            viewTag?.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
            viewTag?.reloadData()
        }*/
        
        
    }
    /*
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let obj = object as? HTagView {
            print("obj.frame.size.height:=",obj.frame.size.height)
            self.constrainViewTagHeight.constant = obj.frame.size.height
            self.contentView.layoutIfNeeded()
        }
    }*/
    
    //MARK:- IBAction
    @IBAction func onBtnAddTagAction(_ sender: Any) {
        handlerOnBtnAddTagAction()
    }
    
}
//MARK:- TagView Delegate
extension AddActionTagCell:HTagViewDelegate, HTagViewDataSource {
    func numberOfTags(_ tagView: HTagView) -> Int {
        return tagData.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return tagData[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .cancel
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    
    // MARK:- HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
    }
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        print("tag with index: '\(index)' has to be removed from tagView")
        handlerRemoveTagAction(index)
    }
    
}
