//
//  AddActionView.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import HTagView
import Loady

class AddActionView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var lblStrategyGoalRelatedToDueDate: UILabel!
    @IBOutlet weak var viewStrategyGoalRelatedToDueDate: UIView!

    @IBOutlet weak var lblCommanRelatedToDueDate: UILabel!
    @IBOutlet weak var viewCommonRelatedToDueDate: UIView!
    @IBOutlet weak var lblAddTitle: UILabel!
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblRevisedDate: UILabel!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var activityRelatedToIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgRelatedView: UIImageView!
    @IBOutlet weak var viewRelatedToTxt: UIView!
    
    @IBOutlet weak var viewCommanRelatedTo: UIView!
    @IBOutlet weak var activityCommonRelatedToIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgCommonRelatedTo: UIImageView!
    @IBOutlet weak var btnRelatedOutlet: UIButton!
    @IBOutlet weak var viewCommonRelatedToTxt: UIView!
    
    @IBOutlet weak var lblCommanRelatedToTitle: UILabel!
    @IBOutlet weak var lblCommanRelatedTo: UILabel!
    
    @IBOutlet weak var viewPrimaryArea: UIView!
    @IBOutlet weak var lblPrimaryArea: UILabel!
    @IBOutlet weak var activityPrimaryAreaIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgPrimaryArea: UIImageView!
    @IBOutlet weak var viewPrimaryAreaTxt: UIView!
    
    @IBOutlet weak var viewSecondaryArea: UIView!
    @IBOutlet weak var lblSecondaryArea: UILabel!
    @IBOutlet weak var activitySecondaryAreaIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgSecondaryArea: UIImageView!
     @IBOutlet weak var viewSecondaryAreaTxt: UIView!
    
    @IBOutlet weak var viewStrategyGoal: UIView!
    @IBOutlet weak var lblStrategyGoal: UILabel!
    @IBOutlet weak var activityStrategyGoalIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgStrategyGoal: UIImageView!
    @IBOutlet weak var viewStrategyGoalTxt: UIView!
    
    @IBOutlet weak var viewTag: HTagView!
    @IBOutlet weak var constrainViewTagHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnSelectCompanyOutlet: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var activityCompanyIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgCompany: UIImageView!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var activityAssignToIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgAssignTo: UIImageView!
    @IBOutlet weak var btnAssignedOutlet: UIButton!
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var activityDepartmentIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgDepartment: UIImageView!
    @IBOutlet weak var btnDepartmentOutlet: UIButton!
    
    @IBOutlet weak var lblApprovedBy: UILabel!
    @IBOutlet weak var activityApprovedIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgApproved: UIImageView!
    @IBOutlet weak var btnApprovedOutlet: UIButton!
    
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblWorkDate: UILabel!
    
    @IBOutlet weak var lblEditWorkDate: UILabel!
    @IBOutlet weak var viewEditWorkDate: UIView!
    
    @IBOutlet weak var viewEditRevisedDueDate: UIView!
    @IBOutlet weak var lblEditRevisedDueDate: UILabel!
    
    @IBOutlet weak var viewWorkDate: UIView!
    @IBOutlet weak var txtDuration: UITextField!
    @IBOutlet weak var txtSubTaskTitle: UITextField!
    @IBOutlet weak var lblAttachmentFile: UILabel!
    @IBOutlet weak var tblNotesHistory: UITableView!
    @IBOutlet weak var heightOfNotesHistory: NSLayoutConstraint!

    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
//    @IBOutlet weak var lblVoiceNotesName: UILabel!
    
    @IBOutlet weak var viewOfPager: UIView!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var txtNotesHistory: UITextField!
    @IBOutlet weak var constrainViewPagerHeight: NSLayoutConstraint! // 40.0

    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var constrainViewNoteHeight: NSLayoutConstraint! //98
    
    @IBOutlet weak var btnAddVoiceNote: UIButton!
//    @IBOutlet weak var constrainViewfileNameHeight: NSLayoutConstraint! // 30.0

    
    @IBOutlet weak var lblDesriptionTitle: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var viewOfSubTask: UIView!
   
    @IBOutlet weak var viewOfCompleted: UIView!
    @IBOutlet weak var txtComopleted: UITextField!
    @IBOutlet weak var viewOfRevisedDate: UIView!
    @IBOutlet weak var btnAdd: LoadyButton!
//    @IBOutlet weak var viewOfAudio: UIView!
    @IBOutlet weak var btnDueDateOutlet: UIButton!
    
    @IBOutlet weak var viewNoteHistoryFilter: UIView!
    @IBOutlet weak var constrainViewNoteHistiryFIlter: NSLayoutConstraint! // 30

    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        activitySpinner.color = appGreen
        viewMore.isHidden = true

        viewEditWorkDate.isHidden = false
        viewEditRevisedDueDate.isHidden = true
        viewCommanRelatedTo.isHidden = true
        viewPrimaryArea.isHidden = true
        viewSecondaryArea.isHidden = true
        viewStrategyGoal.isHidden = true
        lblCreatedDate.text = Date().string(withFormat: DateFormatterOutputType.outputType7)

//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
//            self.viewFileName.addDashedBorder(color: appGreen)
//        })
    }
    
    func setupTableView(theDelegate:AddActionVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none

        tblNotesHistory.estimatedRowHeight = 50.0
        tblNotesHistory.rowHeight = UITableView.automaticDimension
        tblNotesHistory.registerCellNib(HistoryCell.self)
        tblNotesHistory.delegate = theDelegate
        tblNotesHistory.dataSource = theDelegate
        tblNotesHistory.separatorStyle = .none

//        tableView.registerCellNib(HeaderViewTVCell.self)
//        tableView.registerCellNib(InputFieldTVCell.self)
//        tableView.registerCellNib(InputDescriptionTVCell.self)
//        tableView.registerCellNib(SelectionTypeTVCell.self)
//        tableView.registerCellNib(BottomButtonTVCell.self)
//        tableView.registerCellNib(CreatedByTVCell.self)
//        tableView.registerCellNib(RiskLevelTVCell.self)
//        tableView.registerCellNib(InputDateCell.self)
//        tableView.registerCellNib(AddActionDateTVCell.self)
        
        
    }
    func setupTagView(theDelegate:AddActionVC)  {
        viewTag.delegate = theDelegate
        viewTag.dataSource = theDelegate
        viewTag.multiselect = false
        viewTag.marg = 2
        viewTag.btwTags = 2
        viewTag.btwLines = 2
        viewTag.tagFont = R12
        viewTag.tagMainBackColor = appBgLightGrayColor
        viewTag.tagMainTextColor = white
        viewTag.tagSecondBackColor = appBgLightGrayColor
        viewTag.tagSecondTextColor = white
//         viewTag?.tagCancelIconRightMargin = 10
//        viewTag.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        
        //        tagView.reloadData()
        //        tagView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        /*
         [tagView].forEach { (viewTag) in
         viewTag?.delegate = self
         viewTag?.dataSource = self
         viewTag?.multiselect = false
         viewTag?.marg = 2
         viewTag!.btwTags = 2
         viewTag?.btwLines = 2
         viewTag?.tagFont = R12
         viewTag?.tagMainBackColor = appBgLightGrayColor
         viewTag?.tagMainTextColor = white
         viewTag?.tagSecondBackColor = appBgLightGrayColor
         viewTag?.tagSecondTextColor = white
         // viewTag?.tagCancelIconRightMargin = 10
         viewTag?.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
         viewTag?.reloadData()
         }*/
        
        viewTag.reloadData()
        updateTagView()
    }
    
    func setupCollectionView(theDelegate:AddActionVC)  {
        collectionSelectedFile.delegate = theDelegate
        collectionSelectedFile.dataSource = theDelegate
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.reloadData()
    }
    
    
    
    func updateTagView() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.constrainViewTagHeight.constant = self.viewTag.frame.size.height
            if self.viewTag.frame.size.height <= 0{
                self.constrainViewTagHeight.constant = 40
            }
            self.layoutIfNeeded()
        }
    }
    
    func setupActivityIndicator(){
        [activityRelatedToIndicator,activityCommonRelatedToIndicator,activityPrimaryAreaIndicator,activitySecondaryAreaIndicator,activityStrategyGoalIndicator,activityCompanyIndicator,activityAssignToIndicator,activityDepartmentIndicator,activityApprovedIndicator].forEach { (activityIndicator) in
            configureActivityIndicator(activityIndicator: activityIndicator)
        }
    }
    
    func configureActivityIndicator(activityIndicator:UIActivityIndicatorView) {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
    }
    
    func statusActivityIndicator(isLoading:Bool=false,activityIndicator:UIActivityIndicatorView,imgView:UIImageView){
        if isLoading == true{
            activityIndicator.color = appGreen
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            imgView.isHidden = true
        }else{
            activityIndicator.stopAnimating()
            imgView.isHidden = false
        }
    }
    
    func viewHidden(theView:UIView, isHidden:Bool)  {
        theView.isHidden = true
    }
    
    func updateDateView(label:UILabel, strDueDate:String, color:String) {
        var Subtitle = ""
        if let dueDate = strDueDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7),!dueDate.isEmpty {
            if Subtitle.isEmpty {
                Subtitle += "Due on - " + dueDate
            } else {
                Subtitle += "Due on - " + dueDate
//                dueDateString = "D: " + dueDate
            }
        }else{
            Subtitle += "Due on - " + "No Due Date"
        }
        label.attributedText = nil
        let attributeStringDue =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R13,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: color)])
        label.attributedText = attributeStringDue
    }
}
