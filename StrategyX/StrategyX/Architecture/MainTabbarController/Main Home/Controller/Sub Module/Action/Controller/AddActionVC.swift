//
//  AddActionVC.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQAudioRecorderController
//import HTagView
import SwiftyJSON

class AddActionVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case company                                = 2
        case assignTo                               = 3
        case department                             = 4
        case relatedTo                              = 5
        case relatedToStrategyPrimaryArea           = 6
        case relatedToStrategySecondaryArea         = 7
        case relatedToStrategyGoal                  = 8
        case relatedToRisk                          = 9
        case relatedToFocus                         = 10
        case relatedToTacticalProject               = 11
        case approvedBy                             = 12
        case selectTag                              = 13
        case relatedToIdea                          = 14
        case relatedToProblem                       = 15
        case relatedToSuccessFactor                 = 16

    }
    
    enum dateSelectionType:Int {
        case created             = 1
        case due                 = 2
        case work                = 3
        case revisedDate         = 4
    }
    
    fileprivate lazy var theCurrentView:AddActionView = { [unowned self] in
       return self.view as! AddActionView
    }()
    
    fileprivate lazy var theCurrentModel:AddActionModel = {
       return AddActionModel(theController: self)
    }()
    
    var handlerEditAction:(ActionDetail?) -> Void = {_ in }
    var handlerEditDueDateChanged:(String,String) -> Void = {_,_ in } // date, color

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        theCurrentView.collectionSelectedFile.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        theCurrentView.tblNotesHistory.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.collectionSelectedFile.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.tblNotesHistory.removeObserver(self, forKeyPath: "contentSize")

    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            if tabl == theCurrentView.tableView  {
                self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height
            }
            
            if tabl == theCurrentView.tblNotesHistory {
                self.theCurrentView.heightOfNotesHistory.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height
                
            }
//            self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height
            self.view.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        if keyPath == "contentSize", let coll = object as? UICollectionView {
            self.theCurrentView.heightOfCollectionSelectedFile.constant = coll.contentSize.height
            self.view.layoutIfNeeded()
            print("coll.contentSize.height:=",coll.contentSize.height)
        }
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
//        theCurrentView.setupTagView(theDelegate: self)
        theCurrentView.setupCollectionView(theDelegate: self)
        theCurrentView.setupActivityIndicator()
        theCurrentModel.arr_noteHistoryStatus = Utility.shared.setArrayNotesHistoryStatus()
        theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
        theCurrentView.viewCommonRelatedToDueDate.isHidden = true
        theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
        theCurrentView.lblCommanRelatedToDueDate.text = ""

        if theCurrentModel.isEditScreen == .add {
            theCurrentModel.nextOffset = -1
            loadViewMoreNoteHistory(isAnimating: false)
            theCurrentView.viewNoteHistoryFilter.isHidden = true
            theCurrentView.constrainViewNoteHistiryFIlter.constant = 0.0
            if theCurrentModel.isForAddAction {
                let isGoal =  theCurrentModel.strSelectedRelatedId == "1"
                theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = !isGoal
                theCurrentView.viewCommonRelatedToDueDate.isHidden = isGoal
                if theCurrentModel.selectionRelatedToDueDate.isEmpty {
                    theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
                    theCurrentView.viewCommonRelatedToDueDate.isHidden = true
                    theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
                    theCurrentView.lblCommanRelatedToDueDate.text = ""
                }
                theCurrentView.updateDateView(label: isGoal ? theCurrentView.lblStrategyGoalRelatedToDueDate : theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.selectionRelatedToDueDate, color: theCurrentModel.selectionRelatedToDueDateColor)
            }
            if !theCurrentModel.isForAddAction {
                relatedListService(activityIndicator: theCurrentView.activityRelatedToIndicator,imgView: theCurrentView.imgRelatedView)
            }
            setCompanyData()
        } else if theCurrentModel.isEditScreen == .edit {
            theCurrentView.viewEditWorkDate.isHidden = false
            theCurrentView.viewEditRevisedDueDate.isHidden = false
            theCurrentView.viewWorkDate.isHidden = true
            theCurrentView.viewOfRevisedDate.isHidden = true

            setupEditData()
        }
        updateUIForAddAction()
        
//        actionTagListService()
        self.theCurrentView.lblCreatedBy.text = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
        isDisableCompanySelection()
        
        (theCurrentView.lblCommanRelatedToDueDate as? LabelButton)?.onClick = { [weak self] in
            self?.redirectToCustomDatePicker(selectionType: ActionModel.dateSelectionType.due, selectedDate: self?.theCurrentModel.selectionRelatedToDueDate, dueDate: self?.theCurrentModel.selectionRelatedToDueDate ?? "", strActionID: self?.theCurrentModel.selectionRelatedToActionID ?? "", isGoal: false)
        }
        
        (theCurrentView.lblStrategyGoalRelatedToDueDate as? LabelButton)?.onClick = { [weak self] in
            self?.redirectToCustomDatePicker(selectionType: ActionModel.dateSelectionType.due, selectedDate: self?.theCurrentModel.selectionRelatedToDueDate, dueDate: self?.theCurrentModel.selectionRelatedToDueDate ?? "", strActionID: self?.theCurrentModel.selectionRelatedToActionID ?? "", isGoal: true)

        }
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    
    func setUpdateScreen(editType:viewType,actionDetail:ActionDetail)
    {
        theCurrentModel.isEditScreen = editType
        theCurrentModel.theActionDetailModel = actionDetail
    }
    
    func setAddRiskData(theRiskId:String,theRiskName:String,theDueDate:String, theDueDateColor:String,isStatusArchive:Bool) {
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "2"
        theCurrentModel.strSelectedRiskId = theRiskId
        theCurrentModel.selectedData["relatedtorisk"].stringValue = theRiskName
        theCurrentModel.relatedToSelectionType = .risk
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.selectionRelatedToActionID = theRiskId
    }
    
    func setAddTacticalProjectData(theTacticalProjectId:String,theTacticalProjectName:String,theDueDate:String, theDueDateColor:String,isStatusArchive:Bool){
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "3"
        theCurrentModel.strSelectedTacticalProjectId = theTacticalProjectId
        theCurrentModel.selectedData["relatedtotacticalproject"].stringValue = theTacticalProjectName
        theCurrentModel.relatedToSelectionType = .tactical
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.selectionRelatedToActionID = theTacticalProjectId

    }
    
    func setAddFocusData(theFocusId:String,theFocusName:String,theDueDate:String, theDueDateColor:String,isStatusArchive:Bool) {
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "4"
        theCurrentModel.strSelectedFocusId = theFocusId
        theCurrentModel.selectedData["relatedtofocus"].stringValue = theFocusName
        theCurrentModel.relatedToSelectionType = .focus
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.selectionRelatedToActionID = theFocusId
    }
    
    func setAddNotesData(theNoteId:String,theNoteName:String) {
        theCurrentModel.isForAddNoteAction = true
        theCurrentModel.selectedData["title"].stringValue = theNoteName
    }
    
    func setAddSuccessFactorData(theSuccessFactorId:String,theSuccessFactorName:String,theDueDate:String, theDueDateColor:String) {
        theCurrentModel.isForAddAction = true
        theCurrentModel.strSelectedRelatedId = "6"
        theCurrentModel.strSelectedSuccessFactorId = theSuccessFactorId
        theCurrentModel.selectedData["relatedtosuccessfactor"].stringValue = theSuccessFactorName
        theCurrentModel.relatedToSelectionType = .successFactor
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.selectionRelatedToActionID = theSuccessFactorId
        
    }
    
    func setAddIdeaData(theIdeaId:String,theIdeaName:String,theDueDate:String,theDueDateColor:String,isStatusArchive:Bool) {
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "7"
        theCurrentModel.typeIdeaAndProblem = .idea
        theCurrentModel.strSelectedIdeaAndProblemID = theIdeaId
        theCurrentModel.selectedData["relatedtoidea"].stringValue = theIdeaName
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.relatedToSelectionType = .idea
        theCurrentModel.selectionRelatedToActionID = theIdeaId

    }
    
    func setAddProblemData(theProblemId:String,theProblemName:String,theDueDate:String,theDueDateColor:String,isStatusArchive:Bool) {
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "8"
        theCurrentModel.typeIdeaAndProblem = .problem
        theCurrentModel.strSelectedIdeaAndProblemID = theProblemId
        theCurrentModel.selectedData["relatedtoproblem"].stringValue = theProblemName
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor
        theCurrentModel.relatedToSelectionType = .problem
        theCurrentModel.selectionRelatedToActionID = theProblemId
        

    }
    
    func setAddStrategyData(thePrimaryAreaId:String,thePrimaryAreaName:String,theSecondaryAreaId:String,theSecondaryAreaName:String,theGoalId:String,theGoalName:String,theDueDate:String,theDueDateColor:String, isStatusArchive:Bool){
        theCurrentModel.isForAddAction = true
        theCurrentModel.isStatusArchive = isStatusArchive
        theCurrentModel.strSelectedRelatedId = "1" //Strategy
        theCurrentModel.selectionRelatedToDueDate = theDueDate
        theCurrentModel.selectionRelatedToDueDateColor = theDueDateColor

        theCurrentModel.strSelectedPrimaryAreaId = thePrimaryAreaId
        theCurrentModel.selectedData["strategyprimaryarea"].stringValue = thePrimaryAreaName
        theCurrentView.lblPrimaryArea.text = thePrimaryAreaName
        
        theCurrentModel.strSelectedSecondryAreaId = theSecondaryAreaId
        theCurrentModel.selectedData["strategysecondaryarea"].stringValue = theSecondaryAreaName
        theCurrentView.lblSecondaryArea.text = theSecondaryAreaName
        
        theCurrentModel.strSelectedStrategyGoalId = theGoalId
        theCurrentModel.selectedData["strategygoal"].stringValue = theGoalName
        theCurrentView.lblStrategyGoal.text = theGoalName
        print("theDueDate:=",theDueDate)
        theCurrentModel.relatedToSelectionType = .strategy
        theCurrentModel.selectionRelatedToActionID = theGoalId
        

    }
    
    
    
    func updateUIForAddAction(){
        if theCurrentModel.isForAddAction{
            theCurrentView.viewRelatedToTxt.isUserInteractionEnabled = false
            theCurrentView.viewRelatedToTxt.alpha = 0.5
            if theCurrentModel.isStatusArchive {
                theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
            } else {
                theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
            }
            switch theCurrentModel.strSelectedRelatedId{
            case "0": // General Action
                break
            case "1": //Strategy
                theCurrentModel.selectedData["relatedto"].stringValue = "Strategy Goal"
                theCurrentView.lblRelatedTo.text = "Strategy Goal"
                
                theCurrentView.viewPrimaryArea.isHidden = false
                theCurrentView.viewPrimaryAreaTxt.isUserInteractionEnabled = false
                theCurrentView.viewPrimaryAreaTxt.alpha = 0.5
                theCurrentView.lblPrimaryArea.text = theCurrentModel.selectedData["strategyprimaryarea"].stringValue
                
                theCurrentView.viewSecondaryArea.isHidden = false
                theCurrentView.viewSecondaryAreaTxt.isUserInteractionEnabled = false
                theCurrentView.viewSecondaryAreaTxt.alpha = 0.5
                theCurrentView.lblSecondaryArea.text = theCurrentModel.selectedData["strategysecondaryarea"].stringValue
                
                theCurrentView.viewStrategyGoal.isHidden = false
                theCurrentView.viewStrategyGoalTxt.isUserInteractionEnabled = false
                theCurrentView.viewStrategyGoalTxt.alpha = 0.5
                theCurrentView.lblStrategyGoal.text = theCurrentModel.selectedData["strategygoal"].stringValue
                break
            case "2": //Risk
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Risk"
                theCurrentView.lblRelatedTo.text = "Risk"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtorisk"].stringValue
                break
            case "3": //Tactical Project
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Tactical Project"
                theCurrentView.lblRelatedTo.text = "Tactical Project"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtotacticalproject"].stringValue
                break
            case "4": //Focus
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Focus"
                theCurrentView.lblRelatedTo.text = "Focus"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtofocus"].stringValue
                break
            case "6": //Success Factor
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Success Factor"
                theCurrentView.lblRelatedTo.text = "Success Factor"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtosuccessfactor"].stringValue
                break
            case "7": //Idea
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Idea"
                theCurrentView.lblRelatedTo.text = "Idea"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtoidea"].stringValue
                break
            case "8": //Problem
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentModel.selectedData["relatedto"].stringValue = "Problem"
                theCurrentView.lblRelatedTo.text = "Problem"
                theCurrentView.lblCommanRelatedTo.text = theCurrentModel.selectedData["relatedtoproblem"].stringValue
                break
            default:
                break
            }
        } else if theCurrentModel.isForAddNoteAction {
            theCurrentView.txtTitle.text = theCurrentModel.selectedData["title"].stringValue
            theCurrentView.txtTitle.alpha = 0.5
            theCurrentView.txtTitle.isUserInteractionEnabled = false
        }
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func setupEditData(){
        theCurrentView.lblAddTitle.text = "Edit Action"
        theCurrentView.lblCreatedBy.text = theCurrentModel.theActionDetailModel?.createdBy
        theCurrentView.txtTitle.text = theCurrentModel.theActionDetailModel?.actionTitle
        if theCurrentModel.theActionDetailModel?.action_description != ""{
            theCurrentView.lblPlaceHolder.isHidden = true
        }
        theCurrentView.txtDescription.text = theCurrentModel.theActionDetailModel?.action_description
        theCurrentView.lblRelatedTo.text = theCurrentModel.theActionDetailModel?.label
        theCurrentView.lblCompany.text = theCurrentModel.theActionDetailModel?.companyName
        theCurrentView.lblAssignTo.text = theCurrentModel.theActionDetailModel?.assignedTo
        theCurrentView.lblDepartment.text = theCurrentModel.theActionDetailModel?.assignedDepartment
        theCurrentView.lblApprovedBy.text = theCurrentModel.theActionDetailModel?.approvedBy
        
        theCurrentModel.selectedData["company"].stringValue = theCurrentModel.theActionDetailModel?.companyName ?? ""
        theCurrentModel.selectedData["assignto"].stringValue =  theCurrentModel.theActionDetailModel?.assignedTo ?? ""
        theCurrentModel.selectedData["department"].stringValue = theCurrentModel.theActionDetailModel?.assignedDepartment ?? ""
        theCurrentModel.selectedData["approvedby"].stringValue =  theCurrentModel.theActionDetailModel?.approvedBy ?? ""

        theCurrentModel.strSelectedCompanyID = theCurrentModel.theActionDetailModel?.assigneeCompanyId ?? "0"
        theCurrentModel.strSelectedUserAssignID = theCurrentModel.theActionDetailModel?.assigneeId ?? "0"
        theCurrentModel.strSelectedDepartmentID = theCurrentModel.theActionDetailModel?.assignedDepartmentId ?? "0"
        theCurrentModel.strSelectedApprovedID = theCurrentModel.theActionDetailModel?.approvedById ?? "0"
        DispatchQueue.main.async { [weak self] in
            self?.theCurrentView.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        
        
        if theCurrentModel.theActionDetailModel?.duedate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.duedate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblDueDate.text = strFinalDate
                theCurrentModel.selectedData["duedate"].stringValue = strFinalDate
            }
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.duedate)!){
                theCurrentModel.selectedDueDate = finalDate
                print("finalDatefinalDate \(finalDate)")
            }
        }else{
            theCurrentView.lblDueDate.text = "-"
        }
        
        
        if theCurrentModel.theActionDetailModel?.workDate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.workDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblWorkDate.text = strFinalDate
                theCurrentView.lblEditWorkDate.text = strFinalDate
                theCurrentModel.selectedData["workdate"].stringValue = strFinalDate
            }
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.workDate)!){
                theCurrentModel.selectedWorkDate = finalDate
                print("work date \(finalDate)")
            }
        }else{
            theCurrentView.lblWorkDate.text = "-"
            theCurrentView.lblEditWorkDate.text = "-"
        }
        
//        theCurrentView.viewOfRevisedDate.isHidden = false
        
        if theCurrentModel.theActionDetailModel?.revisedDate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.revisedDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblRevisedDate.text = strFinalDate
                theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
            }
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.revisedDate)!){
                theCurrentModel.selectedRevisedDueDate = finalDate
                print("revised date \(finalDate)")
            }
        }else{
            theCurrentView.lblRevisedDate.text = "Select revised due date"
            theCurrentView.lblEditRevisedDueDate.text = "Select revised due date"

        }
        
        if let duration = Int((theCurrentModel.theActionDetailModel?.duration ?? "-1")), (duration > 0) {
           let timeInterval = TimeInterval(duration * 60)
            theCurrentModel.selectedDuration = String(format: "%0.2d:%0.2d", timeInterval.hours,timeInterval.minutes).convertToDate(formate: DateFormatterOutputType.outputType12)
//            print("theCurrentModel.selectedDuration:=",theCurrentModel.selectedDuration)
            theCurrentView.txtDuration.text = String(format: "%0.2d:%0.2d", timeInterval.hours,timeInterval.minutes)
        }
        theCurrentView.txtComopleted.text = theCurrentModel.theActionDetailModel?.percentageCompeleted
        
//        setupDefaulTagData()
        
        
        theCurrentModel.arr_selectedFile.removeAll()
        if let voiceNotes = theCurrentModel.theActionDetailModel?.voiceNotes {
            for item in voiceNotes {
                let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
                theCurrentModel.arr_selectedFile.append(attchment)
            }
        }
        
        if let attachment = theCurrentModel.theActionDetailModel?.attachment {
            for item in attachment {
                let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
                theCurrentModel.arr_selectedFile.append(attchment)
            }
        }
        
        self.theCurrentView.collectionSelectedFile.reloadData()
        
        self.theCurrentView.viewOfSubTask.isHidden = false
       
        theCurrentModel.arr_subTaskList = Array((theCurrentModel.theActionDetailModel?.subTaskList)!)
        
        theCurrentModel.arr_SubTaskList = theCurrentModel.arr_subTaskList.map({$0.subTaskName})
        self.theCurrentView.viewOfCompleted.isHidden = false
        self.theCurrentView.tableView.reloadData()
        
        if theCurrentModel.theActionDetailModel?.createdById != Utility.shared.userData.id {
            [theCurrentView.btnSelectCompanyOutlet,theCurrentView.btnAssignedOutlet,theCurrentView.btnDepartmentOutlet,theCurrentView.btnApprovedOutlet].forEach { (btn) in
                btn?.isUserInteractionEnabled = false
            }
        } else {
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                companylistService(activityIndicator: theCurrentView.activityCompanyIndicator, imgView: theCurrentView.imgCompany)
            } else {
                theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
                theCurrentView.lblCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompany.isHidden = true
            }
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
            departmentListService(activityIndicator: theCurrentView.activityDepartmentIndicator, imgView: theCurrentView.imgDepartment)

        }
        
        theCurrentView.btnRelatedOutlet.isUserInteractionEnabled = false
//        theCurrentView.btnDueDateOutlet.isUserInteractionEnabled = false
        [theCurrentView.viewPrimaryArea,theCurrentView.viewSecondaryArea,theCurrentView.viewStrategyGoal].forEach { (view) in
            view?.isUserInteractionEnabled = false
        }
        theCurrentView.btnAdd.setTitle("Edit", for: .normal)
    }
    
    
    func setupDefaulTagData(){
        theCurrentModel.arr_EditTagList = []
        theCurrentModel.arr_EditTagList = Array((theCurrentModel.theActionDetailModel?.tagsList)!)
        
        theCurrentModel.arr_AddTagList = theCurrentModel.arr_EditTagList.map({$0.tag_name})
        
        for i in 0..<theCurrentModel.arr_EditTagList.count{
            let tag = theCurrentModel.arr_EditTagList[i].tag_id
            for j in 0..<theCurrentModel.arr_actionTagList.count{
                if tag == theCurrentModel.arr_actionTagList[j].tag_id{
                    theCurrentModel.arr_actionTagList[j].is_selected = "1"
                    theCurrentModel.arr_actionTagList[j].is_old_selected = "1"
                }
            }
        }
        print(" acction tag list\(theCurrentModel.arr_actionTagList)")
        
        theCurrentView.viewTag.reloadData()
        theCurrentView.updateTagView()

    }
    
    func removeRelatedCellFromCelltype() {
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToRisk}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToFocus}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToTacticalProject}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToIdea}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToProblem}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.primaryArea}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.secondaryArea}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.strategyGoal}) {
            theCurrentModel.cellType.remove(at: index)
        }
        if let index = theCurrentModel.cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToSuccessFactor}) {
            theCurrentModel.cellType.remove(at: index)
        }
        
    }
    func updateViewCommanRelatedTo(str:String) {
        theCurrentView.lblCommanRelatedToTitle.text = str
        theCurrentView.lblCommanRelatedTo.text = "Select " + str
        
        theCurrentView.viewCommanRelatedTo.isHidden = false
        theCurrentView.viewPrimaryArea.isHidden = true
        theCurrentView.viewSecondaryArea.isHidden = true
        theCurrentView.viewStrategyGoal.isHidden = true
        theCurrentModel.selectionRelatedToDueDate = ""
        theCurrentModel.selectionRelatedToActionID = ""
        theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
        theCurrentView.lblCommanRelatedToDueDate.text = ""
        theCurrentView.viewCommonRelatedToDueDate.isHidden = true
        theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
        
        switch str.localizedLowercase {
        case AddActionModel.relatedToSelectionType.general.rawValue.localizedLowercase:
            theCurrentView.viewCommanRelatedTo.isHidden = true
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.general
            break
        case AddActionModel.relatedToSelectionType.risk.rawValue.localizedLowercase:
            actionRiskListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.risk
//            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToRisk, at: 4)
            break
        case AddActionModel.relatedToSelectionType.tactical.rawValue.localizedLowercase:
            actionTacticalProjectListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.tactical
            
            //            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.focus.rawValue.localizedLowercase:
            actionFocusListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.focus
            
//            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToFocus, at: 4)
            break
        case AddActionModel.relatedToSelectionType.successFactor.rawValue.localizedLowercase:
            actionSuccessFactorListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.successFactor
            
            //            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToFocus, at: 4)
            break
        case AddActionModel.relatedToSelectionType.idea.rawValue.localizedLowercase:
            theCurrentModel.typeIdeaAndProblem = .idea
            actionIdeaAndProblemListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.idea
            
            //            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.problem.rawValue.localizedLowercase:
            theCurrentModel.typeIdeaAndProblem = .problem
            actionIdeaAndProblemListService(activityIndicator: theCurrentView.activityCommonRelatedToIndicator, imgView: theCurrentView.imgCommonRelatedTo)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.problem
            
            //            theCurrentModel.cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.strategy.rawValue.localizedLowercase:
            actionPrimaryAreaListService(activityIndicator: theCurrentView.activityPrimaryAreaIndicator, imgView: theCurrentView.imgPrimaryArea)
            theCurrentModel.relatedToSelectionType = AddActionModel.relatedToSelectionType.strategy
            theCurrentView.viewCommanRelatedTo.isHidden = true
            theCurrentView.viewPrimaryArea.isHidden = false
            theCurrentView.viewSecondaryArea.isHidden = false
            theCurrentView.viewStrategyGoal.isHidden = false
//            theCurrentModel.cellType.insert(AddActionModel.celltype.primaryArea, at: 4)
//            theCurrentModel.cellType.insert(AddActionModel.celltype.secondaryArea, at: 5)
//            theCurrentModel.cellType.insert(AddActionModel.celltype.strategyGoal, at: 6)
            break
        default:
            break
        }
    }
    func updateDropDownData(categorySelection:categorySelectionType, str:String, cellType:AddActionModel.celltype, index:Int)  {
        var index = 0
        if let ind = theCurrentModel.cellType.firstIndex(where: {$0 == cellType}) {
            index = ind
        }

        print("selected index \(index)")
        switch categorySelection {
        case .company:
            if let selectedIndex = theCurrentModel.arr_companyList.firstIndex(where: {$0.name == str}) {
                theCurrentModel.strSelectedCompanyID = theCurrentModel.arr_companyList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["company"].stringValue = str
            theCurrentView.lblCompany.text = str
            theCurrentModel.strSelectedDepartmentID = ""
            self.theCurrentModel.selectedData["department"].stringValue = ""
            theCurrentModel.strSelectedUserAssignID = ""
            self.theCurrentModel.selectedData["assignto"].stringValue = ""
            theCurrentModel.strSelectedApprovedID = ""
            self.theCurrentModel.selectedData["approvedby"].stringValue = ""
            theCurrentView.lblAssignTo.text = "Select assigned to"
            theCurrentView.lblDepartment.text = "Select department"
            theCurrentView.lblApprovedBy.text = "Select approved by"
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
            theCurrentView.statusActivityIndicator(isLoading: true,activityIndicator: theCurrentView.activityApprovedIndicator, imgView: theCurrentView.imgApproved)
            break
        case .department:
            if let selectedIndex = theCurrentModel.arr_departmentList.firstIndex(where: {$0.name == str}) {
                theCurrentModel.strSelectedDepartmentID = theCurrentModel.arr_departmentList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["department"].stringValue = str
            theCurrentView.lblDepartment.text = str
            break
        case .assignTo:
            if let selectedIndex = theCurrentModel.arr_assignList.firstIndex(where: {$0.assignee_name == str}) {
                theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[selectedIndex].user_id
            }
            self.theCurrentModel.selectedData["assignto"].stringValue = str
            theCurrentView.lblAssignTo.text = str
            theCurrentModel.strSelectedDepartmentID = ""
            self.theCurrentModel.selectedData["department"].stringValue = ""
            theCurrentView.lblDepartment.text = "Select department"
            departmentListService(activityIndicator: theCurrentView.activityDepartmentIndicator, imgView: theCurrentView.imgDepartment)
            break
        case .relatedTo:
            if let selectedIndex = theCurrentModel.arr_relatedList.firstIndex(where: {$0.option == str}) {
                theCurrentModel.strSelectedRelatedId = "\(theCurrentModel.arr_relatedList[selectedIndex].value)"
            }
            resetRelatedData()
            removeRelatedCellFromCelltype()
            updateViewCommanRelatedTo(str: str)
           
            self.theCurrentModel.selectedData["relatedto"].stringValue = str
            theCurrentView.lblRelatedTo.text = str
            return
        case .approvedBy:
            if let selectedIndex = theCurrentModel.arr_assignList.firstIndex(where: {$0.assignee_name == str}) {
                theCurrentModel.strSelectedApprovedID = theCurrentModel.arr_assignList[selectedIndex].user_id
            }
            self.theCurrentModel.selectedData["approvedby"].stringValue = str
            theCurrentView.lblApprovedBy.text = str
        case .relatedToStrategyPrimaryArea:
            if let selectedIndex = theCurrentModel.arr_actionPrimaryAreaList.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedPrimaryAreaId = theCurrentModel.arr_actionPrimaryAreaList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["strategyprimaryarea"].stringValue = str
            theCurrentView.lblPrimaryArea.text = str
            theCurrentModel.strSelectedSecondryAreaId = ""
            self.theCurrentModel.selectedData["strategysecondaryarea"].stringValue = str
            theCurrentView.lblSecondaryArea.text = "Select secondary area"
            theCurrentModel.arr_actionSecondryAreaList.removeAll()

            theCurrentModel.strSelectedStrategyGoalId = ""
            self.theCurrentModel.selectedData["strategygoal"].stringValue = ""
            theCurrentView.lblStrategyGoal.text = "Select strategy goal"
            theCurrentModel.arr_actionStrategyGoalList.removeAll()


            actionSecondaryAreaListService(activityIndicator: theCurrentView.activitySecondaryAreaIndicator, imgView: theCurrentView.imgSecondaryArea)
            break
        case .relatedToStrategySecondaryArea:
            if let selectedIndex = theCurrentModel.arr_actionSecondryAreaList.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedSecondryAreaId = theCurrentModel.arr_actionSecondryAreaList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["strategysecondaryarea"].stringValue = str
            theCurrentView.lblSecondaryArea.text = str
            theCurrentModel.strSelectedStrategyGoalId = ""
            self.theCurrentModel.selectedData["strategygoal"].stringValue = ""
            theCurrentView.lblStrategyGoal.text = "Select strategy goal"
            theCurrentModel.arr_actionStrategyGoalList.removeAll()
            actionStrategyGoalListService(activityIndicator: theCurrentView.activityStrategyGoalIndicator, imgView: theCurrentView.imgStrategyGoal)
            break
        case .relatedToStrategyGoal:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""

            if let selectedIndex = theCurrentModel.arr_actionStrategyGoalList.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedStrategyGoalId = theCurrentModel.arr_actionStrategyGoalList[selectedIndex].id
                if UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.arr_actionStrategyGoalList[selectedIndex].status) {
                    theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                    theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
                } else {
                    theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                    theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
                }
                theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionStrategyGoalList[selectedIndex].duedate
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionStrategyGoalList[selectedIndex].id
                theCurrentView.updateDateView(label: theCurrentView.lblStrategyGoalRelatedToDueDate, strDueDate: theCurrentModel.arr_actionStrategyGoalList[selectedIndex].duedate, color: theCurrentModel.arr_actionStrategyGoalList[selectedIndex].duedateColor)
            }
            self.theCurrentModel.selectedData["strategygoal"].stringValue = str
            theCurrentView.lblStrategyGoal.text = str
            break
        case .relatedToRisk:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""

//            let arr_list:[String] = theCurrentModel.arr_actionRiskList.map({$0.unwanted_event })
            if let selectedIndex = theCurrentModel.arr_actionRiskList.firstIndex(where: {$0.unwanted_event == str}) {
                theCurrentModel.strSelectedRiskId = theCurrentModel.arr_actionRiskList[selectedIndex].id
                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionRiskList[selectedIndex].duedate
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionRiskList[selectedIndex].id
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.arr_actionRiskList[selectedIndex].status)
                
                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionRiskList[selectedIndex].duedate, color: theCurrentModel.arr_actionRiskList[selectedIndex].duedateColor)

            }
            theCurrentModel.selectedData["relatedtorisk"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        case .relatedToTacticalProject:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""

            if let selectedIndex = theCurrentModel.arr_actionTacticalProjectList.firstIndex(where: {$0.project_name == str}) {
                theCurrentModel.strSelectedTacticalProjectId = theCurrentModel.arr_actionTacticalProjectList[selectedIndex].id
                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionTacticalProjectList[selectedIndex].duedate
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionTacticalProjectList[selectedIndex].id
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.arr_actionTacticalProjectList[selectedIndex].status)
               
                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionTacticalProjectList[selectedIndex].duedate, color: theCurrentModel.arr_actionTacticalProjectList[selectedIndex].duedateColor)

            }
            theCurrentModel.selectedData["relatedtotacticalproject"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        case .relatedToFocus:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = theCurrentModel.arr_actionFocusList.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedFocusId = theCurrentModel.arr_actionFocusList[selectedIndex].id
                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionFocusList[selectedIndex].id
                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionFocusList[selectedIndex].duedate
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.arr_actionFocusList[selectedIndex].status)

                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionFocusList[selectedIndex].duedate, color: theCurrentModel.arr_actionFocusList[selectedIndex].duedateColor)
                
            }
            theCurrentModel.selectedData["relatedtofocus"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        case .relatedToSuccessFactor:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = theCurrentModel.arr_actionSuccessFactorList.firstIndex(where: {$0.criticalSuccessFactorName == str}) {
                theCurrentModel.strSelectedSuccessFactorId = theCurrentModel.arr_actionSuccessFactorList[selectedIndex].id
//                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionSuccessFactorList[selectedIndex].id

//                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionSuccessFactorList[selectedIndex].duedate
//                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionSuccessFactorList[selectedIndex].duedate, color: theCurrentModel.arr_actionSuccessFactorList[selectedIndex].duedateColor)
                
            }
            theCurrentModel.selectedData["relatedtosuccessfactor"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        case .selectTag:
            break
        case .relatedToIdea:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""

            if let selectedIndex = theCurrentModel.arr_actionIdeaAndProblem.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedIdeaAndProblemID = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].id
                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].id
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isIdeaAndProblemArchive(strStatus: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].status)

                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedate
                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedate, color: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedateColor)

            }
            theCurrentModel.selectedData["relatedtoidea"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        case .relatedToProblem:
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
            theCurrentView.viewCommonRelatedToDueDate.isHidden = true
            theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
            theCurrentView.lblCommanRelatedToDueDate.text = ""

            if let selectedIndex = theCurrentModel.arr_actionIdeaAndProblem.firstIndex(where: {$0.title == str}) {
                theCurrentModel.strSelectedIdeaAndProblemID = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].id
                theCurrentView.viewCommonRelatedToDueDate.isHidden = false
                theCurrentModel.selectionRelatedToActionID = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].id
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isIdeaAndProblemArchive(strStatus: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].status)

                theCurrentModel.selectionRelatedToDueDate = theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedate
                theCurrentView.updateDateView(label: theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedate, color: theCurrentModel.arr_actionIdeaAndProblem[selectedIndex].duedateColor)

            }
            theCurrentModel.selectedData["relatedtoproblem"].stringValue = str
            theCurrentView.lblCommanRelatedTo.text = str
            break
        }
        
//        self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func updateDueDate(strDate:String,dateSelectionType:dateSelectionType,date:Date) {
        switch dateSelectionType {
        case .created:
            theCurrentModel.selectedData["createdate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 9, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblCreatedDate.text = strDate
            break
        case .due:
            theCurrentModel.selectedDueDate = date
            theCurrentModel.selectedData["duedate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 10, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblDueDate.text = strDate
            break
        case .work:
            theCurrentModel.selectedWorkDate = date
            theCurrentModel.selectedData["workdate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 11, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblWorkDate.text = strDate
            theCurrentView.lblEditWorkDate.text = strDate
            break
        case .revisedDate:
            theCurrentModel.selectedData["reviseddate"].stringValue = strDate
            theCurrentModel.selectedRevisedDueDate = date
            theCurrentView.lblRevisedDate.text = strDate
            theCurrentView.lblEditRevisedDueDate.text = strDate

        }
    }
    func updateTagList(strTag:String, isAdded:Bool = true, at index:Int = -1) {
        if isAdded {
            theCurrentModel.arr_AddTagList.append(strTag)
        } else {
            let strRemoveTagName = theCurrentModel.arr_AddTagList[index]
            
            if let selectedIndex = theCurrentModel.arr_actionTagList.firstIndex(where: {$0.tag_name == strRemoveTagName}) {
                
               /* if theCurrentModel.arr_actionTagList[selectedIndex].is_added == "1"{
                    theCurrentModel.arr_actionTagList[selectedIndex].is_del = "1"
                }
                theCurrentModel.arr_actionTagList[selectedIndex].is_selected = "0"*/
                
                let data = theCurrentModel.arr_actionTagList[selectedIndex]
                if data.is_selected == "0"
                {
                    if(data.is_old_selected == "1")
                    {
                        data.is_del = "0"
                    }
                    else{
                        data.is_new_added = "1"
                    }
                    
                    data.is_selected = "1"
                }
                else{
                    
                    if(data.is_old_selected == "1")
                    {
                        data.is_del = "1"
                    }
                    else{
                        data.is_new_added = "0"
                    }
                    data.is_selected = "0"
                }
                
            }
            theCurrentModel.arr_AddTagList.remove(at: index)
        }
        print("TagList \(theCurrentModel.arr_actionTagList)")
        theCurrentView.viewTag.reloadData()
        theCurrentView.updateTagView()
    }
    func updateSubTaskList(strSubTask:String, index:Int) {
        if strSubTask.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            showAlertAtBottom(message: "Please enter subtask title")
            return
        }
        let subTask = SubTaskList()
        subTask.subTaskName = strSubTask
        subTask.is_new_task = "1"
        subTask.is_del = "0"
        subTask.is_old_task = "0"
        subTask.subTaskId = "0"
        
        theCurrentModel.theActionDetailModel?.subTaskList.append(subTask)
        theCurrentModel.arr_SubTaskList.append(strSubTask)
        self.theCurrentView.tableView.reloadData()
        
//        theCurrentModel.arr_SubTaskList.append(strSubTask)
        
//        self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func validateEditForm() {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter project name")
            return
        }  /*else if theCurrentView.txtDescription.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter description")
            return
        }*/ else if theCurrentModel.selectedData["company"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select company")
            return
        } else if theCurrentModel.selectedData["assignto"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select assign to")
            return
        } /*else if theCurrentModel.selectedData["department"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select department")
            return
        } */else if theCurrentModel.selectedData["approvedby"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select approvedby")
            return
        } else if theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select due date")
            return
        } else if !theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
            if let revisedDate1 = theCurrentModel.selectedRevisedDueDate, let workDate1 = theCurrentModel.selectedWorkDate, workDate1 > revisedDate1 {
                self.showAlertAtBottom(message: "Work date must be less than revised due date.")
                return
            } else if let dueDate1 = theCurrentModel.selectedDueDate, let workDate1 = theCurrentModel.selectedWorkDate,theCurrentModel.selectedRevisedDueDate == nil,workDate1 > dueDate1 {
                self.showAlertAtBottom(message: "Work date must be less than due date.")
                return
            }
        }
        
        if !theCurrentModel.selectedData["reviseddate"].stringValue.isEmpty {
            if let dueDate1 = theCurrentModel.selectedDueDate, let revisedDate1 = theCurrentModel.selectedRevisedDueDate,revisedDate1 < dueDate1 {
                self.showAlertAtBottom(message: "Revised date should be grater then due date")
                return
            }
        }
        
        var workDate = ""
        var dueDate = ""
        var revisedDate = ""
        
        var durationCount = ""

        
        
        if let dueDate1 = theCurrentModel.selectedDueDate {
            dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let workDate1 = theCurrentModel.selectedWorkDate {
            workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let reviseDueDate1 = theCurrentModel.selectedRevisedDueDate {
            revisedDate = reviseDueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let duration = theCurrentModel.selectedDuration {
            let minutes = (duration.hour * 60) + duration.minute
            durationCount = "\(minutes)"
        }
        
        var arraySelectedFilterIds : [JSON] = []
        var strTagName = ""
        
        theCurrentModel.arr_actionTagList.forEach { (subJson) in
            
            var newjson = JSON()
            if(subJson.is_new_added != "0" || subJson.is_del != "0" || subJson.is_old_selected != "0")
            {
                if(subJson.is_new_added == "1")
                {
                    newjson["tags"] = JSON(subJson.tag_id)
                    newjson["is_del"] = JSON(subJson.is_del)
                    arraySelectedFilterIds.append(newjson)
                }
                else if(subJson.is_old_selected == "1" && subJson.is_del != "0")
                {
                    
                    newjson["tags"] = JSON(subJson.tag_id)
                    newjson["is_del"] = JSON(subJson.is_del)
                    arraySelectedFilterIds.append(newjson)
                }
            }
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:theCurrentModel.theActionDetailModel?.actionLogid,
                         APIKey.key_title:theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_work_date:workDate,
                         APIKey.key_duedate:dueDate,
                         APIKey.key_revised_date:revisedDate,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID,
                         APIKey.key_assigned_to:theCurrentModel.strSelectedUserAssignID,
                         APIKey.key_department:theCurrentModel.strSelectedDepartmentID,
                         APIKey.key_approve_by:theCurrentModel.strSelectedApprovedID,
                         APIKey.key_description:theCurrentView.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_duration:durationCount,
                         APIKey.key_completed_percentage:theCurrentView.txtComopleted.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            ] as! [String : String]
        
      //  if theCurrentModel.theActionDetailModel?.note_file != ""
        
        print("arraySelectedFilterIds - ",arraySelectedFilterIds)
        strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""
        parameter[APIKey.key_tags] = strTagName
        
        if theCurrentModel.strRemoveAudioId != "0" || theCurrentModel.strRemoveAudioId != ""{
             parameter[APIKey.key_remove_voice_notes] = theCurrentModel.strRemoveAudioId
        }
        
        
        var arr_Subtask:[JSON] = []
        theCurrentModel.theActionDetailModel?.subTaskList.forEach({ (ObjSubTaskList) in
            var dict = JSON()
            
            if(ObjSubTaskList.is_new_task == "1")
            {
                dict["task_name"].stringValue = ObjSubTaskList.subTaskName
                dict["is_del"].stringValue = ObjSubTaskList.is_del
                arr_Subtask.append(dict)
            }
            else
            {
                if(ObjSubTaskList.is_del == "1")
                {
                    dict["task_name"].stringValue = ObjSubTaskList.subTaskId
                    dict["is_del"].stringValue = ObjSubTaskList.is_del
                    arr_Subtask.append(dict)
                }
            }
            
        })
        
        print("Task JSON - ",arr_Subtask)
        
        let strTaskname = JSON(arr_Subtask).rawString()
        parameter[APIKey.key_sub_task] = strTaskname
        
        if theCurrentModel.arr_strRemoveAttachment.count > 0{
            parameter[APIKey.key_remove_attechment] = theCurrentModel.arr_strRemoveAttachment.joined(separator: ",")
        }
        
        var notes = ""
        
        if let history = theCurrentModel.arr_notesHistoryList, history.count > 0 {
            var noteHistory:[[String:String]] = []
            history.forEach { (item) in
                noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
            }
            notes = JSON(noteHistory).rawString() ?? ""
        }
        
        parameter[APIKey.key_notes] = notes

        addActionWithAttachment(parameter: parameter)
    }
    
    func validateForm() {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter project name")
            return
        } /* else if theCurrentView.txtDescription.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter description")
            return
        } */else if theCurrentModel.selectedData["relatedto"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select related to")
            return
        } else {
            if theCurrentModel.strSelectedRelatedId == "1"{
                if theCurrentModel.selectedData["strategyprimaryarea"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select primary area")
                    return
                } else if theCurrentModel.selectedData["strategysecondaryarea"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select secondry area")
                    return
                } else if theCurrentModel.selectedData["strategygoal"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select strategy goal")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "2"{
                if theCurrentModel.selectedData["relatedtorisk"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select risk")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "3"{
                if theCurrentModel.selectedData["relatedtotacticalproject"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select tactical project")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "4"{
                if theCurrentModel.selectedData["relatedtofocus"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select focus")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "6"{
                if theCurrentModel.selectedData["relatedtosuccessfactor"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select success factor")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "7" {
                if theCurrentModel.selectedData["relatedtoidea"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select idea")
                    return
                }
            } else if theCurrentModel.strSelectedRelatedId == "8" {
                if theCurrentModel.selectedData["relatedtoproblem"].stringValue.isEmpty {
                    self.showAlertAtBottom(message: "Please select problem")
                    return
                }
            }
            /*if theCurrentModel.arr_AddTagList.count < 0{
                self.showAlertAtBottom(message: "Please select atlease one tag")
                return
            } else*/ if theCurrentModel.selectedData["company"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select company")
                return
            } else if theCurrentModel.selectedData["assignto"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select assigned to")
                return
            } /*else if theCurrentModel.selectedData["department"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select department")
                return
            }*/ else if theCurrentModel.selectedData["approvedby"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select approvedby")
                return
            } else if theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select due date")
                return
            } else if !theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
                if let dueDate1 = theCurrentModel.selectedDueDate, let workdDate1 = theCurrentModel.selectedWorkDate, workdDate1 > dueDate1 {
                    self.showAlertAtBottom(message: "Work date must be less than Due date.")
                    return
                }
            }
            
            var workDate = ""
            var dueDate = ""
            var durationCount = ""
            
            if let dueDate1 = theCurrentModel.selectedDueDate {
                dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
            }
            
            if let workDate1 = theCurrentModel.selectedWorkDate {
                workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
            }
            if let duration = theCurrentModel.selectedDuration {
                let minutes = (duration.hour * 60) + duration.minute
                durationCount = "\(minutes)"
            }
            isClickOnAdd(isLoading: true)
            var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_title:theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_work_date:workDate,
                             APIKey.key_duedate:dueDate,
                             APIKey.key_company_id:theCurrentModel.strSelectedCompanyID,
                             APIKey.key_assigned_to:theCurrentModel.strSelectedUserAssignID,
                             APIKey.key_department:theCurrentModel.strSelectedDepartmentID,
                             APIKey.key_approve_by:theCurrentModel.strSelectedApprovedID,
                        APIKey.key_description:theCurrentView.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_duration:durationCount,
                             APIKey.key_related_to:theCurrentModel.strSelectedRelatedId] as [String : String]
            
            if theCurrentModel.strSelectedRelatedId == "4"{
                parameter[APIKey.key_goal]  = theCurrentModel.strSelectedFocusId
            } else if theCurrentModel.strSelectedRelatedId == "6"{
                parameter[APIKey.key_success_factor]  = theCurrentModel.strSelectedSuccessFactorId
            } else if theCurrentModel.strSelectedRelatedId == "3"{
                 parameter[APIKey.key_tactical_projects]  = theCurrentModel.strSelectedTacticalProjectId
            } else if theCurrentModel.strSelectedRelatedId == "2"{
                parameter[APIKey.key_risk]  = theCurrentModel.strSelectedRiskId
            } else if theCurrentModel.strSelectedRelatedId == "1"{
                parameter[APIKey.key_strategy_goal]  = theCurrentModel.strSelectedStrategyGoalId
            } else if theCurrentModel.strSelectedRelatedId == "7"{
                parameter[APIKey.key_idea]  = theCurrentModel.strSelectedIdeaAndProblemID
            } else if theCurrentModel.strSelectedRelatedId == "8"{
                parameter[APIKey.key_problem] = theCurrentModel.strSelectedIdeaAndProblemID
            }
            
            var arr_Subtask:[JSON] = []
            for i in 0..<theCurrentModel.arr_SubTaskList.count{
                var dict = JSON()
                dict["task_name"].stringValue = theCurrentModel.arr_SubTaskList[i]
                arr_Subtask.append(dict)
            }
            let strTaskname = JSON(arr_Subtask).rawString()
            parameter[APIKey.key_sub_task] = strTaskname
            
            var arraySelectedFilterIds : [JSON] = []
            var strTagName = ""
           
            theCurrentModel.arr_actionTagList.filter { (subJson) -> Bool in
                    var newjson = JSON()
                    if(theCurrentModel.arr_AddTagList.contains(subJson.tag_name))
                    {
                        newjson["tags"] = JSON(subJson.tag_id)
                        arraySelectedFilterIds.append(newjson)
                        return true
                    }
                    return false
                }
               // strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""
           
            print("arraySelectedFilterIds - ",arraySelectedFilterIds)
            
            var notes = ""
            
            if let history = theCurrentModel.arr_notesHistoryList, history.count > 0 {
                var noteHistory:[[String:String]] = []
                history.forEach { (item) in
                    noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
                }
                notes = JSON(noteHistory).rawString() ?? ""
            }
            
            parameter[APIKey.key_notes] = notes
            
            strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""
            parameter[APIKey.key_tags] = strTagName
            addActionWithAttachment(parameter: parameter)
        }
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
        }*/
        if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService(activityIndicator: theCurrentView.activityCompanyIndicator, imgView: theCurrentView.imgCompany)
            
        } else {
            theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
            theCurrentView.lblCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompany.isHidden = true
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
        }
    }
    
    
    
    func resetRelatedData(){
        theCurrentModel.selectedData["relatedtotacticalproject"].stringValue = ""
        theCurrentModel.strSelectedTacticalProjectId = ""
        theCurrentView.lblCommanRelatedTo.text = "Select"
        
        theCurrentModel.selectedData["relatedtofocus"].stringValue = ""
        theCurrentModel.strSelectedFocusId = ""
        
        theCurrentModel.selectedData["relatedtosuccessfactor"].stringValue = ""
        theCurrentModel.strSelectedSuccessFactorId = ""
        
        theCurrentModel.selectedData["relatedtorisk"].stringValue = ""
        theCurrentModel.strSelectedRiskId = ""
        
        self.theCurrentModel.selectedData["strategygoal"].stringValue = ""
        theCurrentModel.strSelectedStrategyGoalId = ""
        theCurrentView.lblStrategyGoal.text = "Select strategy goal"
        
        self.theCurrentModel.selectedData["strategysecondaryarea"].stringValue = ""
        theCurrentModel.strSelectedSecondryAreaId = ""
        theCurrentView.lblSecondaryArea.text = "Select secondary area"
        
        self.theCurrentModel.selectedData["strategyprimaryarea"].stringValue = ""
        theCurrentModel.strSelectedPrimaryAreaId = ""
        theCurrentView.lblPrimaryArea.text = "Select primary area"
        
        self.theCurrentModel.selectedData["relatedtoidea"].stringValue = ""
        self.theCurrentModel.selectedData["relatedtoproblem"].stringValue = ""
        theCurrentModel.strSelectedIdeaAndProblemID = ""
//        theCurrentView.lblPrimaryArea.text = "Select Idea"
        
        theCurrentModel.arr_actionTacticalProjectList = []
        theCurrentModel.arr_actionFocusList = []
        theCurrentModel.arr_actionSuccessFactorList = []
        theCurrentModel.arr_actionStrategyGoalList = []
        theCurrentModel.arr_actionPrimaryAreaList = []
        theCurrentModel.arr_actionSecondryAreaList = []
        theCurrentModel.arr_actionIdeaAndProblem = []
    }
    
    func updateActionDetailModel() {
        if let dueDate1 = theCurrentModel.selectedDueDate {
            theCurrentModel.theActionDetailModel?.duedate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let workDate1 = theCurrentModel.selectedWorkDate {
            theCurrentModel.theActionDetailModel?.workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let reviseDueDate1 = theCurrentModel.selectedRevisedDueDate {
            theCurrentModel.theActionDetailModel?.revisedDate = reviseDueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        theCurrentModel.theActionDetailModel?.actionTitle = theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        theCurrentModel.theActionDetailModel?.percentageCompeleted = theCurrentView.txtComopleted.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//            = theCurrentModel.selectedData["description"].stringValue
    }
    
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            theCurrentView.btnAdd.startLoadyIndicator()
            self.view.allButtons(isEnable: false)
        } else {
            theCurrentView.btnAdd.stopLoadyIndicator()
            self.view.allButtons(isEnable: true)
        }
    }
    
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        theCurrentView.activitySpinner.hidesWhenStopped = true
        theCurrentView.constrainViewPagerHeight.constant = 40.0
        theCurrentView.viewOfPager.isHidden = false
        
        if isAnimating {
            theCurrentView.activitySpinner.startAnimating()
            theCurrentView.viewMore.isHidden = true
        } else {
            theCurrentView.activitySpinner.stopAnimating()
            theCurrentView.viewMore.isHidden = false
            
            if theCurrentModel.nextOffset == -1 {
                theCurrentView.viewOfPager.isHidden = true
                theCurrentView.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String, isGoal:Bool) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate, isGoal: isGoal)
    }
    
    //MARK:- Redirect To
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String,isGoal:Bool) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID, isGoal: isGoal)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToDatePicker() {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetDate = true
        vc.setDateValue = theCurrentModel.selectedDuration
        vc.minuteInterval = 5
        vc.isForDuration = true
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.countDownTimer)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1 {
//                self?.dateFormater.dateFormat = "hh mm a"
                self?.theCurrentModel.selectedDuration = date
                let arr_time = date.string(withFormat: DateFormatterOutputType.outputType12)
                print("arr_time:=",arr_time)
                self?.theCurrentView.txtDuration.text = date.string(withFormat: DateFormatterOutputType.outputType12)

//                self?.update(hours: arr_time[0], minute: arr_time[1], timeType: arr_time[2])
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    func presentDropDownListVC(categorySelection:categorySelectionType,cellType:AddActionModel.celltype) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            let arr_list:[String] = theCurrentModel.arr_companyList.map({$0.name })
            vc.setTheData(strTitle: "Company",arr:arr_list)
            break
        case .department:
            let arr_list:[String] = theCurrentModel.arr_departmentList.map({$0.name })
            vc.setTheData(strTitle: "Depatment",arr:arr_list)
            break
        case .assignTo:
            let arr_list:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name })
            vc.setTheData(strTitle: "Assigned To",arr:arr_list)
            break
        case .relatedTo:
            let arr_list:[String] = theCurrentModel.arr_relatedList.map({$0.option })
            vc.setTheData(strTitle: "Related To",arr:arr_list)
            break
        case .approvedBy:
            let arr_list:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name })
            vc.setTheData(strTitle: "Approved By",arr:arr_list)
            break
        case .relatedToStrategyPrimaryArea:
            let arr_list:[String] = theCurrentModel.arr_actionPrimaryAreaList.map({$0.title })
            vc.setTheData(strTitle: "Primary Area",arr:arr_list)
            break
        case .relatedToStrategySecondaryArea:
            let arr_list:[String] = theCurrentModel.arr_actionSecondryAreaList.map({$0.title })
            vc.setTheData(strTitle: "Secondary Area",arr:arr_list)
            break
        case .relatedToStrategyGoal:
            let arr_list:[String] = theCurrentModel.arr_actionStrategyGoalList.map({$0.title })
            vc.setTheData(strTitle: "Strategy Goal",arr:arr_list)
            break
        case .relatedToRisk:
            let arr_list:[String] = theCurrentModel.arr_actionRiskList.map({$0.unwanted_event })
            vc.setTheData(strTitle: "Related To Risk",arr:arr_list)
            break
        case .relatedToTacticalProject:
            let arr_list:[String] = theCurrentModel.arr_actionTacticalProjectList.map({$0.project_name })
            vc.setTheData(strTitle: "Related To Tactical Project",arr:arr_list)
            break
        case .relatedToFocus:
            let arr_list:[String] = theCurrentModel.arr_actionFocusList.map({$0.title })
            vc.setTheData(strTitle: "Related To Focus",arr:arr_list)
            break
        case .relatedToSuccessFactor:
            let arr_list:[String] = theCurrentModel.arr_actionSuccessFactorList.map({$0.criticalSuccessFactorName })
            vc.setTheData(strTitle: "Related To Success Factor",arr:arr_list)
            break
        case .relatedToIdea:
            let arr_list:[String] = theCurrentModel.arr_actionIdeaAndProblem.map({$0.title })
            vc.setTheData(strTitle: "Related To Idea",arr:arr_list)
            break
        case .relatedToProblem:
            let arr_list:[String] = theCurrentModel.arr_actionIdeaAndProblem.map({$0.title })
            vc.setTheData(strTitle: "Related To Problem",arr:arr_list)
            break
        case .selectTag:
            vc.isSelectTag = true
            vc.setTheTagData(strTitle: "Add Tags", arr: theCurrentModel.arr_actionTagList)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, cellType: cellType,index: index)
        }
        /*
        vc.handlerTag = {[weak self](data) in
            self?.theCurrentModel.arr_actionTagList = data
            self?.theCurrentModel.arr_AddTagList = []
            var isCheckTag = Bool()
            for i in 0..<(self?.theCurrentModel.arr_actionTagList.count ?? 0){
                
                if let data = self?.theCurrentModel.arr_actionTagList[i], data.is_selected == "1" {
                    isCheckTag = true
                    self?.updateTagList(strTag: data.tag_name, isAdded: true, at: i)
                }
            }
            if isCheckTag == false{
                self?.theCurrentView.viewTag.reloadData()
                self?.theCurrentView.updateTagView()
            }
        }*/
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    /*func redirectToDatePicker(dateSelectionType:dateSelectionType) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = Date()
        vc.isSetDate = false
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.dateAndTime)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = "dd MMM yyyy, hh:mma"
                //                self?.theCurrentModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
                self?.updateDueDate(strDate: dateFormaater.string(from: date), dateSelectionType: dateSelectionType)
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }*/
    
    
    func redirectToCustomDatePicker(selectionType:dateSelectionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .created:
            if let createdDate = theCurrentModel.selectedData["createdate"].string, !createdDate.isEmpty, let date = createdDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
            
        case .due:
            if let dueDate = theCurrentModel.selectedData["duedate"].string, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .work:
            if let workDate = theCurrentModel.selectedData["workdate"].string, !workDate.isEmpty, let date = workDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .revisedDate:
            if let reviseDate = theCurrentModel.selectedData["reviseddate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            //self?.updateDueDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
            self?.updateDueDate(strDate: dateFormaater.string(from: date), dateSelectionType: selectionType, date: date)
            
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentFileManger() {

         let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        

    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        

    }
    
   
    
    //MARK:- IBAction
    
    @IBAction func onBtnRelatedToAction(_ sender: Any) {
        self.view.endEditing(true)
        
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relatedToGeneral)
    }
    
    @IBAction func onBtnCommanRelatedToAction(_ sender: Any) {
        switch theCurrentModel.relatedToSelectionType {
        case .general:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relatedToGeneral)
            break
        case .strategy:
//            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relat)
            break
        case .risk:
            
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToRisk, cellType: AddActionModel.celltype.relatedToRisk)
            break
        case .tactical:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToTacticalProject, cellType: AddActionModel.celltype.relatedToTacticalProject)
            break
        case .focus:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToFocus, cellType: AddActionModel.celltype.relatedToFocus)
            break
        case .successFactor:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToSuccessFactor, cellType: AddActionModel.celltype.relatedToSuccessFactor)
            break
        case .idea:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToIdea, cellType: AddActionModel.celltype.relatedToIdea)
            break
        case .problem:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToProblem, cellType: AddActionModel.celltype.relatedToProblem)
            break
        }
        
    }
    @IBAction func onBtnPrimaryAreaAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategyPrimaryArea, cellType: AddActionModel.celltype.primaryArea)
    }
    
    @IBAction func onBtnSecondaryAreaAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategySecondaryArea, cellType: AddActionModel.celltype.secondaryArea)
    }
    @IBAction func onBtnStrategyGoalAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategyGoal, cellType: AddActionModel.celltype.strategyGoal)
    }
    
    @IBAction func onBtnAddTagAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.selectTag, cellType: AddActionModel.celltype.tag)
        //presentAddTagVC()
    }
    
    @IBAction func onBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.company, cellType: AddActionModel.celltype.company)
    }
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.assignTo, cellType: AddActionModel.celltype.assignTo)
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.department, cellType: AddActionModel.celltype.department)
    }
    @IBAction func onBtnApprovedByAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.approvedBy, cellType: AddActionModel.celltype.approvedBy)
    }
    @IBAction func onBtnCreatedDateAction(_ sender: Any) {
        self.view.endEditing(true)
    
    }
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: AddActionVC.dateSelectionType.due)
    }
    @IBAction func onBtnWorkDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: AddActionVC.dateSelectionType.work)
    }
    
    @IBAction func onBtnRevisedDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: AddActionVC.dateSelectionType.revisedDate)
    }
    
    @IBAction func onBtnDurationAction(_ sender: Any) {
        self.view.endEditing(true)
        self.redirectToDatePicker()
    }
    
    
    @IBAction func onBtnSubTaskAction(_ sender: Any) {
        let text = theCurrentView.txtSubTaskTitle.text ?? ""
        theCurrentView.txtSubTaskTitle.text = nil
        updateSubTaskList(strSubTask: text, index: 0)
    }
    
    @IBAction func onBtnAttachmentFileAction(_ sender: Any) {
        self.view.endEditing(true)
        presentFileManger()
    }
    
    @IBAction func onBtnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let strNoteMsg = theCurrentView.txtNote.text ?? ""
        if strNoteMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter note")
            return
        } else {
            if theCurrentModel.isEditScreen == .add {
                theCurrentView.txtNote.text = ""
                let history = NotesHistory()
                history.createdDate = Date().string(withFormat: DateFormatterInputType.inputType1)
                history.notesHistoryDescription = strNoteMsg
                history.actionUserName = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
                history.historyStatus = APIKey.key_Commented
                if theCurrentModel.arr_notesHistoryList == nil {
                    theCurrentModel.arr_notesHistoryList = []
                }
                theCurrentModel.arr_notesHistoryList?.append(history)
                theCurrentView.tblNotesHistory.reloadData()
                return
            }
            sender.startLoadyIndicator()
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentModel.actionId,
                                 APIKey.key_notes: strNoteMsg,
                                 APIKey.key_note_for: APIKey.key_action_logs
            ]
            
            addNotes(parameter: parameter, completion: {
                sender.stopLoadyIndicator()
            })
        }
    }
    
    @IBAction func onBtnSelectNotesHistoy(_ sender:UIButton){
//        presentDropDownListVC(categorySelection: <#AddActionVC.categorySelectionType#>)
    }
    
    @IBAction func onAddVoiceAction(_ sender: Any) {
        self.view.endEditing(true)
        presentVoiceNote()
    }
    
    
    @IBAction func onAddAction(_ sender: Any) {
        if theCurrentModel.isEditScreen == .edit{
            validateEditForm()
        }else{
            validateForm()
        }
    }
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnRemoveFileAction(_ sender: UIButton) {
//        if theCurrentModel.isEditScreen == .edit{
//            let fileName = self.theCurrentModel.arr_selectedFile[sender.tag].selectedFileName
//            if let selectedIndex = theCurrentModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
//                theCurrentModel.arr_strRemoveAttachment.append(theCurrentModel.arr_fileList[selectedIndex].fileId)
//            }
//        }
//        self.theCurrentModel.arr_selectedFile.remove(at: sender.tag)
//        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    @IBAction func btnTaskCompletedAction(_ sender: UIButton) {
        self.theCurrentView.txtComopleted.text = "100"
    }
    @IBAction func btnRemoveAudioAction(_ sender: UIButton) {
//        if theCurrentModel.isEditScreen == .edit{
//            if theCurrentModel.strRemoveAudioId == "0"{
//                theCurrentModel.strRemoveAudioId = theCurrentModel.theActionDetailModel?.note_file_id ?? ""
//            }
//        }
//        theCurrentModel.arr_selectedAudioFile.removeAll()
//        theCurrentView.viewOfAudio.isHidden = true
    }
    
    @IBAction func onBtnVoiceNoteAction(_ sender: Any) {
//        if theCurrentModel.arr_selectedAudioFile.count > 0 {
//            self.presentAudioPlayer(url: theCurrentModel.arr_selectedAudioFile[0].selectedFileName)
//        }
    }
    
    @IBAction func onBtnRemoveAttachmentAction(_ sender: UIButton) {
        /*if theCurrentModel.isEditScreen == .edit{
         let fileName = self.theCurrentModel.arr_selectedFile[sender.tag].selectedFileName
         if let selectedIndex = theCurrentModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
         theCurrentModel.arr_strRemoveAttachment.append(theCurrentModel.arr_fileList[selectedIndex].fileId)
         }
         }
         self.theCurrentModel.arr_selectedFile.remove(at: sender.tag)*/
        
        
        let attachment = theCurrentModel.arr_selectedFile[sender.tag]
        theCurrentModel.arr_RemoveSelectedFile.append(attachment)
        theCurrentModel.arr_selectedFile.remove(at: sender.tag)
        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    
    @IBAction func onBtnViewMoreAction(_ sender: Any) {
        loadViewMoreNoteHistory(isAnimating: true)
        getActionNotesHistoryWebService(status: theCurrentModel.strNoteStatus)
    }
    
}

//MARK:- UITextView Delegate
extension AddActionVC:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""{
            theCurrentView.lblPlaceHolder.isHidden = false
        }else{
            theCurrentView.lblPlaceHolder.isHidden = true
        }
    }
}

//MARK:- UITextField Delegate
extension AddActionVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if theCurrentView.txtDuration == textField{
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if theCurrentView.txtComopleted == textField{
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

//MARK:- UITableViewDataSource
extension AddActionVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return theCurrentModel.cellType.count
        if theCurrentView.tblNotesHistory == tableView {
            return theCurrentModel.arr_notesHistoryList?.count ?? 0
        }
        return theCurrentModel.arr_SubTaskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == theCurrentView.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            cell.configure(strTitle: theCurrentModel.arr_SubTaskList[indexPath.row])
            cell.handlerOnDeleteAction = { [weak self] (tag) in
                
                let taskName = self?.theCurrentModel.arr_SubTaskList[tag]
                if let selectedIndex = self?.theCurrentModel.arr_subTaskList.firstIndex(where: {$0.subTaskName == taskName}) {
                    
                    let objsubTask = self?.theCurrentModel.arr_subTaskList[selectedIndex]
                    if(objsubTask?.is_new_task == "1")
                    {
                        objsubTask?.is_del = "0"
                        //TODO:- Remove from array
                    }
                    else
                    {
                        objsubTask?.is_del = "1"
                        objsubTask?.subTaskName = (objsubTask?.subTaskId)!
                    }
                    self?.theCurrentModel.arr_subTaskList[selectedIndex] = objsubTask!
                }
                self?.theCurrentModel.arr_SubTaskList.remove(at: indexPath.row)
                
                tableView.reloadData()
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
            if theCurrentModel.arr_notesHistoryList?.count != nil{
                let data = theCurrentModel.arr_notesHistoryList![indexPath.row]
                var strF = NSAttributedString(string: "")
                if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strF = "\(createdDate) ".attributedString(color: black, font: R14)!
                }
                //                    attributedString(string: "\(data.createdDate) ", color: black, font: R14)!
                var strS = NSAttributedString(string: "")
                //            strS = data.actionUserName
                strS = data.actionUserName.attributedString(color: black, font: R14)!
                //                    attributedString(string: data.actionUserName, color: black, font: R14)!
                var strT = NSAttributedString(string: "")
                var strFt = NSAttributedString(string: "")
                var strSx = NSAttributedString(string: "")
                var strSth = NSAttributedString(string: "")
                
                switch data.historyStatus{
                case APIKey.key_Assigned:
                    cell.imgPercentage.image = UIImage.init(named: "ic_contact")
                    cell.vwOuter.backgroundColor = appColorFilterBlue
                    //                    strT = "Assigned to"
                    strT = " Assigned to ".attributedString(color:black, font: R14)!
                    //                    strFt = data.assignedUserName
                    
                    strFt = " \(data.assignedUserName) ".attributedString(color: black, font: R14)!
                    break
                case APIKey.key_Percentage:
                    cell.imgPercentage.image = UIImage.init(named: "ic_percentage_icon")
                    cell.vwOuter.backgroundColor = appColorFilterBlack
                    //                    strT = "Completed \(data.percentageCompleted)%"
                    strT = " Completed \(data.percentageCompleted)% ".attributedString(color: black, font: R14)!
                    break
                case APIKey.key_Commented:
                    cell.imgPercentage.image = UIImage.init(named: "ic_meassage")
                    cell.vwOuter.backgroundColor = appColorFilterBlue
                    //                    strF = data.actionUserName
                    //                    let attachment = NSTextAttachment()
                    //                    let img = UIImage(named: "ic_time_icon")
                    //                    img?.size = CGSize(width: 12.0, height: 12.0)
                    //                    attachment.image = UIImage(named: "ic_time_icon")
                    //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                    //                    attachment.image?.size = CGSize(width: 12.0, height: 12.0)
                    strFt = " \(data.actionUserName) ".attributedString(color: black, font: R14)!
                    strS = NSAttributedString(string: "")
                    strT = NSAttributedString(string: "")
                    //                    strFt = NSAttributedString(attachment: attachment)
                    //                    if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    //                        strSx = "\(createdDate)".attributedString(color: black, font: R14)!
                    //                    }
                    strSx = ":".attributedString(color: black, font: R14)!
                    strSth = " \(data.notesHistoryDescription) ".attributedString(color: black, font: R14)!
                    //                    strSth = "Notes Text \(data.noteId)"
                    break
                case APIKey.key_Revised:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    strT = " Revised Due Date To ".attributedString(color: black, font: R14)!
                    //                    strT = "Revised Due Date To"
                    //                    let attachment = NSTextAttachment()
                    //                    attachment.image = UIImage(named: "ic_time_icon")
                    //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                    
                    strFt = NSAttributedString(string: " ")
                    //                    strFt = NSAttributedString(attachment: attachment)
                    if let revisedDate = data.revisedDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                        strSx = "\(revisedDate)".attributedString(color: black, font: R14)!
                    }
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Worked:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    strT = " Revised Work Date To ".attributedString(color: black, font: R14)!
                    //                    strT = "Revised Work Date To"
                    //                    let attachment = NSTextAttachment()
                    //                    attachment.image = UIImage(named: "ic_time_icon")
                    //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                    strFt = NSAttributedString(string: " ")
                    if let workedDate = data.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                        strSx = "\(workedDate) ".attributedString(color: black, font: R14)!
                    }
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Opened:
                    cell.imgPercentage.image = UIImage.init(named: "ic_left_icon")
                    cell.vwOuter.backgroundColor = appColorFilterGreen
                    strT = " Opened an Action. ".attributedString(color: black, font: R14)!
                    //                    strT = "Opened an Action."
                    strFt = NSAttributedString(string: "")
                    strSx = NSAttributedString(string: "")
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Closed:
                    cell.imgPercentage.image = UIImage.init(named: "ic_right_icon")
                    cell.vwOuter.backgroundColor = appColorFilterRed
                    strT = " Closed an Action. ".attributedString(color: black, font: R14)!
                    //                    strT = "Closed an Action."
                    strFt = NSAttributedString(string: "")
                    strSx = NSAttributedString(string: "")
                    strSth = NSAttributedString(string: "")
                    break
                default:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    break
                }
                let FormatedString = NSMutableAttributedString()
                
                FormatedString.append(strF)
                FormatedString.append(strS)
                FormatedString.append(strT)
                FormatedString.append(strFt)
                FormatedString.append(strSx)
                FormatedString.append(strSth)
                cell.lblHistory.attributedText = FormatedString
            }
            return cell
        }
    }
}
//MARK:-UITableViewDelegate
extension AddActionVC:UITableViewDelegate {
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if theCurrentModel.cellType[indexPath.row] == AddActionModel.celltype.subtask {
            return theCurrentModel.subtaskCellHeight
        } else {
            return UITableView.automaticDimension
        }
    }*/
}

//MARK:- UICollectionView datasource & delegate

extension AddActionVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentModel.arr_selectedFile.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
        if theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
            cell.btnCloseOutlet.isHidden = true
        }
        //        cell.bgView.setBackGroundColor = theCurrentModel.arr_selectedFile[indexPath.row].fileType == .audio ? 3 : 8
        
        cell.lblFileName.text = (theCurrentModel.arr_selectedFile[indexPath.row].fileName as NSString).lastPathComponent
        
        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveAttachmentAction(_:)), for: .touchUpInside)
        
        
        /*if theCurrentModel.arr_selectedAudioFile.count != 0 && theCurrentModel.arr_selectedAudioFile.count == 1 && indexPath.row == 0 {
         let selectFile = theCurrentModel.arr_selectedAudioFile[indexPath.row]
         cell.bgView.setBackGroundColor = 3
         cell.lblFileName.text = selectFile.selectedFileName
         cell.btnCloseOutlet.tag = indexPath.row
         cell.btnCloseOutlet.addTarget(self, action: #selector(btnRemoveAudioAction(_:)), for: .touchUpInside)
         } else {
         
         if theCurrentModel.arr_selectedAudioFile.count != 0 {
         cell.lblFileName.text = theCurrentModel.arr_selectedFile[indexPath.row - 1].selectedFileName
         } else {
         cell.lblFileName.text = theCurrentModel.arr_selectedFile[indexPath.row].selectedFileName
         }
         
         cell.btnCloseOutlet.tag = indexPath.row
         cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveFileAction(_:)), for: .touchUpInside)
         }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentModel.arr_selectedFile[indexPath.row].fileName.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
        } else {
            self.presentWebViewVC(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName, isLocal: !theCurrentModel.arr_selectedFile[indexPath.row].isOldAttachment)
        }
    }
    
    
}


//MARK:- TagView Delegate
extension AddActionVC:HTagViewDelegate, HTagViewDataSource {
    func numberOfTags(_ tagView: HTagView) -> Int {
        return theCurrentModel.arr_AddTagList.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return theCurrentModel.arr_AddTagList[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .cancel
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    
    // MARK:- HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
    }
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        print("tag with index: '\(index)' has to be removed from tagView")
        updateTagList(strTag: "", isAdded: false, at: index)
//        handlerRemoveTagAction(index)
    }
    
}

//MARK:- UIDocumentPickerDelegate
extension AddActionVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    })
                    {
                        
                    }
                    else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                })
                {
                    
                }
                else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        }
        else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported, \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension AddActionVC:IQAudioRecorderViewControllerDelegate {
    
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")
        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.theCurrentView.collectionSelectedFile.reloadData()
                        //                    self?.theCurrentView.viewOfAudio.isHidden = false
                        //                    self?.theCurrentView.lblVoiceNotesName.text = objSelectedData.selectedFileName
                    }
                    
                }
            }
            
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}


//MARK:- Call API
extension AddActionVC{
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String, isGoal:Bool) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        switch theCurrentModel.relatedToSelectionType {
        case .general:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
            break
        case .strategy:
            parameter[APIKey.key_date_for] = APIKey.key_strategy_goals
            break
        case .risk:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_risks
            break
        case .tactical:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_tactical_projects
            break
        case .focus:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_focuses
            break
        case .idea:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
        case .problem:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
        
        case .successFactor:
            break
        }
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        Utility.shared.showActivityIndicator()
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            if let obj = self, let model = theModel {
                obj.theCurrentModel.selectionRelatedToDueDate = model.revisedDate
                obj.theCurrentView.updateDateView(label: isGoal ? obj.theCurrentView.lblStrategyGoalRelatedToDueDate : obj.theCurrentView.lblCommanRelatedToDueDate, strDueDate: model.revisedDate, color: model.revisedColor)
                if obj.theCurrentModel.isForAddAction {
                    obj.handlerEditDueDateChanged(model.revisedDate,model.revisedColor)
                }
            }
            Utility.shared.stopActivityIndicator()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.showAlertAtBottom(message: error)
        })
        
    }
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theCurrentModel.theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theCurrentModel.actionId,
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_action_logs,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            self?.theCurrentView.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)
                
                //                self?.setBackground(strMsg: error)
        })
    }
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentModel.strNoteStatus ?? "")
            self?.theCurrentView.txtNote.text = ""
            completion()
            }, failed: { (error) in
                completion()
                self.showAlertAtBottom(message: error)
        })
    }
    func relatedListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_related_type:APIKey.key_action_logs]
        ActionsWebService.shared.getRelatedTo(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_relatedList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionRiskListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedRiskList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionRiskList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionTacticalProjectListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedTacticalProjectList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionTacticalProjectList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionFocusListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedFocusList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionFocusList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSuccessFactorListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedSuccessFactorList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionSuccessFactorList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionPrimaryAreaListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getPrimaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionPrimaryAreaList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSecondaryAreaListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_primary_area_id:theCurrentModel.strSelectedPrimaryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getSecondaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionSecondryAreaList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionStrategyGoalListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_secondary_area_id:theCurrentModel.strSelectedSecondryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getStrategyGoalList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionStrategyGoalList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionIdeaAndProblemListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId,
                         APIKey.key_related_to:theCurrentModel.typeIdeaAndProblem.rawValue]
        ActionsWebService.shared.getRelatedIdeaAndProblemList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_actionIdeaAndProblem = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionTagListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_is_add_form_tag:"1"]
        ActionsWebService.shared.getTagList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_actionTagList = list
            if self?.theCurrentModel.isEditScreen == .edit{
//                self?.setupDefaulTagData()
            }
            }, failed: { [weak self] (error) in
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func companylistService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
             self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_companyList = list
            }, failed: { [weak self] (error) in
                 self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func assignUserListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        self.theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator,imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
             self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            if let obj = self {
                obj.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: obj.theCurrentView.activityApprovedIndicator, imgView: obj.theCurrentView.imgApproved)
            }
            self?.theCurrentModel.arr_assignList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: activityIndicator, imgView: imgView)
                if let obj = self {
                    obj.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: obj.theCurrentView.activityApprovedIndicator, imgView: obj.theCurrentView.imgApproved)
                }
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func departmentListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        self.theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator,imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:theCurrentModel.strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_departmentList = list
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            if let obj = self, obj.theCurrentModel.isEditScreen == .add, list.count > 0 {
                obj.updateDropDownData(categorySelection: .department, str: list[0].name, cellType: AddActionModel.celltype.department, index: 0)
            }
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func addActionWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        
        var removeFile:[[String:String]] = []
//        var removeAttachment:[String] = []
        
        var voiceCount = 0
        var fileCount = 0
        
//        for item in theCurrentModel.arr_RemoveSelectedFile {
//            if item.isOldAttachment {
//                removeAttachment.append(item.attachmentID)
//            }
//            //            removeFile.append([APIKey.key_id:item.attachmentID])
//        }
        
        for i in 0..<theCurrentModel.arr_selectedFile.count {
            if theCurrentModel.arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:theCurrentModel.arr_selectedFile[i].attachmentID])
            } else {
                if !theCurrentModel.arr_selectedFile[i].isOldAttachment && (theCurrentModel.arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = theCurrentModel.arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(theCurrentModel.arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if theCurrentModel.arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
//        var param = parameter
//        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")
        
        isClickOnAdd(isLoading: true)
        
        if theCurrentModel.isEditScreen == .edit {
            ActionsWebService.shared.uploadEditActionAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (actionList) in
                self?.updateActionDetailModel()
                self?.handlerEditAction(self?.theCurrentModel.theActionDetailModel)
                self?.isClickOnAdd(isLoading: false)
                self?.backAction(isAnimation: false)
                }, failed: { [weak self] (error) in
                    self?.isClickOnAdd(isLoading: false)
                    self?.showAlertAtBottom(message: error)
            })
        }else{
            ActionsWebService.shared.uploadAddActionAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
                self?.isClickOnAdd(isLoading: false)
                if let isForAddAction = self?.theCurrentModel.isForAddAction,isForAddAction {
                    self?.handlerEditAction(nil)
                }
                self?.backAction(isAnimation: false)
                }, failed: { [weak self] (error) in
                    self?.isClickOnAdd(isLoading: false)
                    self?.showAlertAtBottom(message: error)
            })
        }
       
    }
    
}
