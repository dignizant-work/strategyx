//
//  AddActionDateTVCell.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddActionDateTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var dateType = AddActionVC.dateSelectionType.created
    var handlerOnDateAction:(AddActionVC.dateSelectionType) -> Void = {_ in }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,strDate:String, selectionDateType:AddActionVC.dateSelectionType) {
        lblTitle.text = strTitle
        lblDate.text = strDate
        dateType = selectionDateType
    }
    
    //MARK:- IBAction
    @IBAction func OnBtnDateAction(_ sender: Any) {
        handlerOnDateAction(dateType)
    }
    
}
