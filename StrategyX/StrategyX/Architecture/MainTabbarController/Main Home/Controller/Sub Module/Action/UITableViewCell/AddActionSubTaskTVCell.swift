//
//  AddActionSubTaskTVCell.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddActionSubTaskListTVCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var handlerOnDeleteAction:(Int) -> Void = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func configure(strTitle:String) {
        lblTitle.text = strTitle
    }
    
    @IBAction func onBtnDeleteAction(_ sender:UIButton) {
        handlerOnDeleteAction(self.tag)
    }
    
}


class AddActionSubTaskTVCell: UITableViewCell {

    @IBOutlet weak var txtSubTask: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint! // 0.0
    
    var arr_subTaskList:[String] = []
    var handlerAddTaskAction:(String) -> Void = {_ in}
    var handlerRemoveTaskAction:(Int) -> Void = {_ in}
    var handlerUpdateTableView:() -> Void = { }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupTableView()
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTableView() {
//        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        
        constrainTableViewHeight.constant = 0.0
        self.contentView.layoutIfNeeded()
        tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            constrainTableViewHeight.constant = tabl.contentSize.height
            self.contentView.layoutIfNeeded()
            
//            handlerUpdateTableView()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
    }
    
    func configure(arr_List:[String]) {
        arr_subTaskList = arr_List
        tableView.reloadData()
    }

    //MARK:-IBAction
    @IBAction func onBtnAddSubTaskAction(_ sender: Any) {
        let text = txtSubTask.text ?? ""
        txtSubTask.text = nil
        if text.trimmed().count > 0 {
            arr_subTaskList.insert(text, at: 0)
            tableView.reloadData()
        }
        handlerAddTaskAction(text)
    }
    
}
//MARK:- UITableViewDataSource
extension AddActionSubTaskTVCell:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_subTaskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddActionSubTaskListTVCell") as! AddActionSubTaskListTVCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.configure(strTitle: arr_subTaskList[indexPath.row])
        cell.handlerOnDeleteAction = { [weak self] (tag) in
            self?.arr_subTaskList.remove(at: tag)
            tableView.reloadData()
            self?.handlerRemoveTaskAction(indexPath.row)
        }
        return cell
    }
    
}
//MARK:- UITableViewDelegate
extension AddActionSubTaskTVCell:UITableViewDelegate {
   
}
