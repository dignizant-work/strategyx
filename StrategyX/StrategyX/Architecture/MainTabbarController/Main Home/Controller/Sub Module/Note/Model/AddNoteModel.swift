//
//  AddNoteModel.swift
//  StrategyX
//
//  Created by Haresh on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddNoteModel {

    //MARK:- Variable
    fileprivate var theController:AddNoteVC!
    var imageBG:UIImage?
    var isForEdit = false
    var theEditNoteModel:Notes?


    //MARK:- Life Cycle
    init(theController:AddNoteVC) {
        self.theController = theController
    }
    
}
