//
//  AddNoteVC.swift
//  StrategyX
//
//  Created by Haresh on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddNoteVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:AddNoteView = { [unowned self] in
       return self.view as! AddNoteView
    }()
    fileprivate lazy var theCurrentModel:AddNoteModel = {
       return AddNoteModel(theController: self)
    }()
    
    var handlerUpdateModel:(Notes?) -> Void = {_ in }

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }

    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        setupEditData()

        
    }
    func setTheData(imgBG:UIImage?, isForEdit:Bool = false) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.isForEdit = isForEdit
    }
    func setTheEditData(theModel:Notes?, isForEdit:Bool = false)  {
        theCurrentModel.isForEdit = isForEdit
        theCurrentModel.theEditNoteModel = theModel
    }
    func setupEditData() {
        if !theCurrentModel.isForEdit {
            return
        }
        theCurrentView.lblAddTitle.text = "Edit Note"
        theCurrentView.btnAdd.setTitle("Edit", for: .normal)
        theCurrentView.lblCreatedBy.text  = ""
        theCurrentView.txtDescription.text = theCurrentModel.theEditNoteModel?.notes
        DispatchQueue.main.async { [weak self] in
            self?.theCurrentView.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        if let created = theCurrentModel.theEditNoteModel?.created.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outPutType11),!created.isEmpty {
            theCurrentView.lblCreatedBy.text  = created
        }
    }
    func addNotesModelInNotesList(theModel:Notes) {
        if let index = self.navigationController?.viewControllers.firstIndex(where: {($0 as? NotesVC) != nil}) {
            (self.navigationController?.viewControllers[index] as? NotesVC)?.addNotesModelInList(theModel: theModel)
        }
    }
    func validateForm() -> Bool {
        self.view.endEditing(true)
        if theCurrentView.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter description")
            return false
        }
        return true
    }
    
    //MARK:- IBAction
    @IBAction func onSaveAndExitAction(_ sender: UIButton) {
        if !validateForm() {
            return
        }
        if theCurrentModel.isForEdit {
            guard theCurrentModel.theEditNoteModel != nil else { return }
            editNoteWebService()
        } else {
            addNoteWebService()
        }
        
    }
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }

}
//MARK:- Api Call
extension AddNoteVC  {
    func addNoteWebService() {
        theCurrentView.btnAdd.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_notes:theCurrentView.txtDescription.text!.trimmed()] as [String : Any]
        
        NotesWebServices.shared.addNote(parameter: parameter, success: {  [weak self] (msg, theNotesModel) in
//            self?.showAlertAtBottom(message: msg)
            self?.addNotesModelInNotesList(theModel: theNotesModel)
            self?.theCurrentView.btnAdd.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.onBtnCancelAction("")
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.btnAdd.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
    }
    
    func editNoteWebService() {
        theCurrentView.btnAdd.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_note_id:theCurrentModel.theEditNoteModel!.id,
                         APIKey.key_notes:theCurrentView.txtDescription.text!.trimmed()] as [String : Any]
        
        NotesWebServices.shared.updateNote(parameter: parameter, success: {  [weak self] (msg) in
//            self?.showAlertAtBottom(message: msg)
            self?.theCurrentView.btnAdd.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.theCurrentModel.theEditNoteModel?.notes = self?.theCurrentView.txtDescription.text ?? ""
            self?.handlerUpdateModel(self?.theCurrentModel.theEditNoteModel)
            self?.onBtnCancelAction("")
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.btnAdd.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
        })
    }
}

