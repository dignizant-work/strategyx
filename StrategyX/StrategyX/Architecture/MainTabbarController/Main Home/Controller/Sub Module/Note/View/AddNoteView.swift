//
//  AddNoteView.swift
//  StrategyX
//
//  Created by Haresh on 16/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddNoteView: ViewParentWithoutXIB {
    //MARK:- Variable
    @IBOutlet weak var lblAddTitle: UILabel!
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblDesriptionTitle: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnAdd: UIButton!
    
    let disposeBag = DisposeBag()
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        lblCreatedBy.text = Date().string(withFormat: DateFormatterOutputType.outPutType11)
        
        //Rx Action
        rxTextDescription()
    }

    
    //Rx Action
    func rxTextDescription() {
        txtDescription.rx.text.orEmpty
            .subscribe({ [weak self] (txt) in
                self?.lblPlaceHolder.isHidden = true
                if let element = txt.element {
                   self?.lblPlaceHolder.isHidden = !element.isEmpty
                }
            }).disposed(by: disposeBag)
    }
}
