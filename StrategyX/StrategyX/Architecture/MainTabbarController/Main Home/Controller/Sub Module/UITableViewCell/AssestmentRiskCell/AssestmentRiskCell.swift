//
//  AssestmentRiskCell.swift
//  StrategyX
//
//  Created by Jaydeep on 12/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AssestmentRiskCell: UITableViewCell {

    @IBOutlet weak var heightOfBottomAssestment: NSLayoutConstraint!
    @IBOutlet weak var ViewOfBottomAssestment: UIView!
    @IBOutlet weak var viewOfLastAssetment: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
