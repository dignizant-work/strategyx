//
//  HeaderViewTVCell.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class HeaderViewTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnGoalInfo: UIButton!
    
    var handlerGoalInfoClick:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String, isForGoal:Bool = false) {
        lblTitle.text = strTitle
        btnGoalInfo.isHidden = !isForGoal
    }
    
    //MARK- IBAction
    @IBAction func onBtnGoalInfoAction(_ sender:UIButton) {
        handlerGoalInfoClick()
    }
    
}
