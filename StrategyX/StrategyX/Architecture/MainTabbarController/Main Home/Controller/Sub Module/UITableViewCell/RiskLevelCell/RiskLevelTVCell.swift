//
//  RiskLevelTVCell.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RiskLevelTVCell: UITableViewCell {

    @IBOutlet weak var viewMainOuter: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblRare: UILabel!
    @IBOutlet weak var lblUnlikely: UILabel!
    @IBOutlet weak var lblModerate: UILabel!
    @IBOutlet weak var lblLikely: UILabel!
    @IBOutlet weak var lblAlmostCertain: UILabel!
    
    
    @IBOutlet var btnLevel:[UIButton]!
    
    var handlerOnButtonLevelClick:(String) -> Void = {_ in}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI() {
        let frame = topView.frame
        
        lblRare.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblRare.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblUnlikely.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblUnlikely.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblModerate.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblModerate.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblLikely.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblLikely.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblAlmostCertain.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblAlmostCertain.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
    }
    
    func configure(strLevel:String) {
        btnLevel.forEach({
            $0.setBackGroundColor = 0
            if $0.titleLabel?.text == strLevel {$0.setBackGroundColor = 11}
        })
    }
    
    //MARK:- IBAction
    @IBAction func onBtnLevelAction(_ sender:UIButton) {
        if let title = sender.titleLabel?.text {
            handlerOnButtonLevelClick(title)
        }
    }
    
}
