//
//  BottomButtonTVCell.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Loady

class BottomButtonTVCell: UITableViewCell {

    @IBOutlet weak var btnAdd: LoadyButton!
    @IBOutlet weak var btnCancel: UIButton!
    var handlerCancel:() -> Void = {}
    var handlerAdd:() -> Void = { }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(isLoading:Bool = false, strTitlebtnAdd:String = "Add", isBtnAddHidden:Bool = false, strTitlebtnCancel:String = "Cancel", isBtnCancelHidden:Bool = false) {
        btnAdd.setTitle(strTitlebtnAdd, for: .normal)
        btnCancel.setTitle(strTitlebtnCancel, for: .normal)
        btnAdd.isHidden = isBtnAddHidden
        btnCancel.isHidden = isBtnCancelHidden
        
        if isLoading {
            btnAdd.startLoadyIndicator()
            self.contentView.allButtons(isEnable: false)
        } else {
            btnAdd.stopLoadyIndicator()
            self.contentView.allButtons(isEnable: true)
        }
    }
    
    //MARK:-IBAction
    @IBAction func onBtnAddAction(_ sender: UIButton) {
        handlerAdd()
    }
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        handlerCancel()
    }
    
}
