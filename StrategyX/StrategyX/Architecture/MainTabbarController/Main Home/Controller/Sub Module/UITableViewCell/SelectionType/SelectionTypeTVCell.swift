//
//  SelectionTypeTVCell.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SelectionTypeTVCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMandatory: UILabel!
    
    @IBOutlet weak var lblSelectedType: UILabel!
    @IBOutlet weak var viewDropDown: UIView!
    
    var handlerBtnClick:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureActivityIndicator()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureActivityIndicator() {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
    }  
   
    
    func configure(strTitle:String,strSelectionType:String, isMandatory:Bool, isLoading:Bool = false) {
        lblTitle.text = strTitle
        lblTitle.showAstrike = isMandatory
//        lblMandatory.isHidden = !isMandatory
        lblSelectedType.text = strSelectionType
        if isLoading {
            btnDropDown.isHidden = true
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
            btnDropDown.isHidden = false
            activityIndicator.isHidden = true
        }
    }
    
    func isDisableCompanySelection() {
        if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
            viewDropDown.isUserInteractionEnabled = false
        }
    }
    
    func dropDown(isEnable:Bool) {
        viewDropDown.isUserInteractionEnabled = isEnable ? true : false
        viewDropDown.alpha = isEnable ? 1.0 : 0.6
    }
    
    //MARK:- IBAction
    @IBAction func onBtnDropDownAction(_ sender: Any) {
        handlerBtnClick()
    }
    
}
