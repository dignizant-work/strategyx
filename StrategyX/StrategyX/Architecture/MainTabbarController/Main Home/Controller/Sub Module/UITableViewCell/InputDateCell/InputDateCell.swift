//
//  InputDateCell.swift
//  StrategyX
//
//  Created by Haresh on 31/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class InputDateCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMandatory: UILabel!
    
    @IBOutlet weak var lblSelectedDate: UILabel!
    @IBOutlet weak var viewDate: UIView!
    
    var handlerBtnClick:() -> Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,strSelectionDate:String, isMandatory:Bool) {
        lblTitle.text = strTitle
        lblMandatory.isHidden = !isMandatory
        lblSelectedDate.text = strSelectionDate
    }
    
    func viewDate(isEnable:Bool) {
        viewDate.isUserInteractionEnabled = isEnable
        viewDate.alpha = isEnable ? 1.0 : 0.6
    }
    
    //MARK:- IBAction
    @IBAction func onBtnDateAction(_ sender: Any) {
        handlerBtnClick()
    }
    
}
