//
//  InputFieldTVCell.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class InputFieldTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMandatory: UILabel!
    @IBOutlet weak var img_BottomLine: UIImageView!
    @IBOutlet weak var txtInputField: UITextField!
    @IBOutlet var lblAddress: UILabel!
    
    var handlerText:(String) -> Void = {_ in}
    let disposeBag = DisposeBag()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rxTextFieldAction()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,isMandatory:Bool,strPlaceHolder:String,isHideBottomLine:Bool = false,type:UIKeyboardType = .default) {
        lblTitle.text = strTitle
        lblTitle.showAstrike = isMandatory
//        lblMandatory.isHidden = !isMandatory
        txtInputField.placeholder = strPlaceHolder
        img_BottomLine.isHidden = true
        txtInputField.keyboardType = type
    }
    
    func setTheTitle(inputText:String) {
        txtInputField.text = inputText
    }
    func inputFieldView(isEnable:Bool) {
        txtInputField.isUserInteractionEnabled = isEnable ? true : false
        txtInputField.alpha = isEnable ? 1.0 : 0.6
    }
    
    //Rx
    func rxTextFieldAction() {
        txtInputField.rx.text.orEmpty
            .subscribe({ [weak self] _ in
                self?.handlerText(self?.txtInputField.text ?? "")
            }).disposed(by:disposeBag)
    }
    
}
