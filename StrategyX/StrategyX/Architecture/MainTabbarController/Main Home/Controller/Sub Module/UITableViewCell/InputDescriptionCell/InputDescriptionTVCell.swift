//
//  InputDescriptionTVCell.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class InputDescriptionTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMandatory: UILabel!
    @IBOutlet weak var img_BottomLine: UIImageView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    let disposeBag = DisposeBag()
    var handlerText:(String) -> Void = {_ in}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rxTextViewDidChange()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,strPlaceHolder:String,isHideBottomLine:Bool = false,isMandatory:Bool=false) {
        lblTitle.text = strTitle
        lblTitle.showAstrike = isMandatory
        lblPlaceHolder.text = strPlaceHolder
        img_BottomLine.isHidden = true
//        lblMandatory.isHidden = !isMandatory
    }
    func setTheDescription(inputText:String) {
        txtDescription.text = inputText
        DispatchQueue.main.async { [weak self] in
            self?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }

    }
    //RX
    func rxTextViewDidChange() {
        txtDescription.rx.text.orEmpty
            .subscribe({ [weak self] event in
                self?.handlerText(self?.txtDescription.text ?? "")
                if let obj = self {
                    obj.lblPlaceHolder.isHidden = obj.txtDescription.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    
}
