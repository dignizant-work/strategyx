//
//  AddPrimaryAreaVC.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQAudioRecorderController

class AddPrimaryAreaVC: ParentViewController {

    //MARk:- Variable
    fileprivate lazy var theCurrentView:AddPrimaryAreaView = { [unowned self] in
       return self.view as! AddPrimaryAreaView
    }()
    
    internal lazy var theCurrentViewModel:AddPrimaryAreaViewModel = {
       return AddPrimaryAreaViewModel(theController: self)
    }()
    
    var handlerEditData:(PrimaryArea) -> Void = {_ in}

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.setObserver(theController:self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.removeObserver(theController:self)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            
            if tabl == theCurrentView.tblNotesHistory {
                self.theCurrentView.heightOfNotesHistory.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height
            }
            self.view.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        if keyPath == "contentSize", let coll = object as? UICollectionView {
            self.theCurrentView.heightOfCollectionSelectedFile.constant = coll.contentSize.height
            self.view.layoutIfNeeded()
            print("coll.contentSize.height:=",coll.contentSize.height)
        }
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentViewModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.setupCollectionView(theDelegate: self)
        theCurrentViewModel.arr_noteHistoryStatus = Utility.shared.setArrayNotesHistoryStatus()
        if !theCurrentViewModel.isEdit { //New
            theCurrentViewModel.selectedData["createdDate"].stringValue = Date().string(withFormat: DateFormatterOutputType.outputType7)
            theCurrentViewModel.selectedData["createdby"].stringValue = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
            theCurrentView.showUpdateData(theModel: theCurrentViewModel.selectedData)
            theCurrentViewModel.nextOffset = -1
            theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: false)
            theCurrentView.viewNoteHistoryFilter.isHidden = true
            theCurrentView.constrainViewNoteHistiryFIlter.constant = 0.0
            theCurrentView.heightOfNotesHistory.constant = 0.0
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                theCurrentViewModel.companylistService()
            } else {
                theCurrentView.viewCompany.isHidden = true
                theCurrentViewModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentViewModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
                theCurrentView.lblCompany.text = Utility.shared.userData.companyName
                theCurrentViewModel.assignUserListService()
            }

        } else {
            theCurrentViewModel.showTheEditData()
            theCurrentView.collectionSelectedFile.reloadData()
            theCurrentView.btnSelectCompany.isUserInteractionEnabled = false
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                theCurrentView.viewCompany.isHidden = true
            }
            if theCurrentViewModel.theEditPrimaryAreaModel?.status.lowercased() == APIKey.key_archived.lowercased() {
                setEditDataForArchive()
            } else {
                if let createdBy = theCurrentViewModel.theEditPrimaryAreaModel?.createdById, UserDefault.shared.isCanEditForm(strOppoisteID: createdBy) {
                    theCurrentViewModel.strSelectedCompanyID = theCurrentViewModel.theEditPrimaryAreaModel?.companyId ?? "\(Utility.shared.userData.companyId)"
                    theCurrentViewModel.assignUserListService()
                }
                if !theCurrentViewModel.strSelectedUserAssignID.isEmpty {
                    theCurrentViewModel.departmentListService()
                }
            }
            
            theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
            theCurrentViewModel.getActionNotesHistoryWebService(isRefreshing: true, status: self.theCurrentViewModel.strNoteStatus)

        }
        theCurrentView.constrainViewPagerHeight.constant = 0.0
        theCurrentViewModel.isDisableCompanySelection()
        
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentViewModel.imageBG = imgBG
    }
    func setTheEditData(theModel:PrimaryArea?, isForEdit:Bool)  {
        theCurrentViewModel.isEdit = isForEdit
        theCurrentViewModel.theEditPrimaryAreaModel = theModel
    }
    
    func setEditDataForArchive() {
        if theCurrentViewModel.theEditPrimaryAreaModel?.status.lowercased() == APIKey.key_archived.lowercased() {
            theCurrentView.btnAdd.isHidden = true
            //             let allButtons  = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            
            theCurrentView.viewNotes.isHidden = true
            theCurrentView.constrainViewNoteHeight.constant = 0.0
            
            theCurrentView.viewFileName.isHidden = true
            theCurrentView.btnAddVoiceNote.isHidden = true
            theCurrentView.constrainViewfileNameHeight.constant = 0.0
            
            
            let view = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            view.forEach({$0.isUserInteractionEnabled = false})
            
            theCurrentView.scrollView.isUserInteractionEnabled = true
            if let view = theCurrentView.scrollView.viewWithTag(1890) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            
            if let view = theCurrentView.scrollView.viewWithTag(1891) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            if let view = theCurrentView.scrollView.viewWithTag(1700) {
                view.isUserInteractionEnabled = true
                if let viewsub = view.viewWithTag(1701) {
                    viewsub.isUserInteractionEnabled = true
                    if let viewsub1 = viewsub.viewWithTag(1702) {
                        viewsub1.isUserInteractionEnabled = true
                    }
                }
                
            }
            
            if let view = theCurrentView.scrollView.viewWithTag(200) {
                view.isUserInteractionEnabled = true
            }
            theCurrentView.viewOfPager.isUserInteractionEnabled = true
            theCurrentView.viewMore.isUserInteractionEnabled = true
        }
    }
    
    
    //MARK:- Redirect To
    
    func presentDropDownListVC(categorySelection:AddPrimaryAreaViewModel.categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            let arr_company:[String] = theCurrentViewModel.arr_companyList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Company",arr:arr_company)
            break
        case .assignTo:
            let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assignee_name}).filter({!$0.isEmpty})
            //          let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            let arr_department:[String] = theCurrentViewModel.arr_departmentList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Depatment",arr:arr_department)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToDatePicker(selectionType:AddPrimaryAreaViewModel.dateSelctionType) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = Date()
        vc.isSetDate = false
        
        if theCurrentViewModel.isEdit {
            vc.isSetDate = true
            vc.minimumDate = theCurrentViewModel.selectedrevisedDate ?? Date()
            vc.setDateValue = theCurrentViewModel.selectedrevisedDate
        }
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.dateAndTime)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = DateFormatterOutputType.outPutType4
                //                self?.theCurrentViewModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
                self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToCustomDatePicker(selectionType:AddPrimaryAreaViewModel.dateSelctionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = theCurrentViewModel.selectedData["duedate"].string, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .resived:
            if let reviseDate = theCurrentViewModel.selectedData["revisedduedate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentViewModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }

    func presentFileManger() {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        audioRecorderVC.audioFormat = .default
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        
    }
    func presentDropDownListVCForNoteHistory() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        let arr_list:[String] = theCurrentViewModel.arr_noteHistoryStatus.map({$0["name"].stringValue })
        vc.setTheData(strTitle: "Notes History", arr: arr_list)
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.arr_notesHistoryList?.removeAll()
            self?.theCurrentView.tblNotesHistory.reloadData()
            self?.theCurrentView.txtNotesHistory.text = strData
            self?.theCurrentViewModel.strNoteStatus = self?.theCurrentViewModel.arr_noteHistoryStatus[index]["status"].stringValue ?? ""
            self?.theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
            self?.theCurrentViewModel.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentViewModel.strNoteStatus ?? "")
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnSelectNotesHistoy(_ sender:UIButton){
        presentDropDownListVCForNoteHistory()
    }
    @IBAction func onBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .company)
    }
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select first company")
            return
        }
        presentDropDownListVC(categorySelection: .assignTo)
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedUserAssignID.isEmpty {
            self.showAlertAtBottom(message: "Please select first assigned to")
            return
        }
        presentDropDownListVC(categorySelection: .department)
    }
    @IBAction func onBtnApprovedByAction(_ sender: Any) {
        self.view.endEditing(true)
//        presentDropDownListVC(categorySelection: .approvedBy, cellType: .approvedBy)
    }
    
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: .due)
    }
   
    @IBAction func onBtnRevisedDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: .resived)
    }
    
    @IBAction func onBtnAttachmentFileAction(_ sender: Any) {
        self.view.endEditing(true)
        presentFileManger()
    }
    
    @IBAction func onBtnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let strNoteMsg = theCurrentView.txtNote.text ?? ""
        if strNoteMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter note")
            return
        } else {
            if !theCurrentViewModel.isEdit {
                theCurrentView.txtNote.text = ""
                let history = NotesHistory()
                history.createdDate = Date().string(withFormat: DateFormatterInputType.inputType1)
                history.notesHistoryDescription = strNoteMsg
                history.actionUserName = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
                history.historyStatus = APIKey.key_Commented
                if theCurrentViewModel.arr_notesHistoryList == nil {
                    theCurrentViewModel.arr_notesHistoryList = []
                }
                theCurrentViewModel.arr_notesHistoryList?.append(history)
                theCurrentView.tblNotesHistory.reloadData()
                return
            }
            sender.startLoadyIndicator()
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentViewModel.theEditPrimaryAreaModel?.id ?? "",
                                 APIKey.key_notes: strNoteMsg,
                                 APIKey.key_note_for: APIKey.key_strategy_primary_areas
            ]
            
            theCurrentViewModel.addNotes(parameter: parameter, completion: {
                sender.stopLoadyIndicator()
            })
        }
    }
    
    @IBAction func onAddVoiceAction(_ sender: Any) {
        self.view.endEditing(true)
        presentVoiceNote()
    }
    
    @IBAction func onAddAction(_ sender: Any) {
        theCurrentViewModel.validateForm()
    }
    
    @IBAction func onBtnCancelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if theCurrentViewModel.isEdit {
            sender.startLoadyIndicator()
            theCurrentViewModel.closeHistorySaveWebService { [weak self] in
                sender.stopLoadyIndicator()
                self?.backAction(isAnimation: false)
            }
        } else {
            self.backAction(isAnimation: false)
        }
    }
    
    @IBAction func onBtnRemoveAttachmentAction(_ sender: UIButton) {
        /*if theCurrentViewModel.isEditScreen == .edit{
         let fileName = self.theCurrentViewModel.arr_selectedFile[sender.tag].selectedFileName
         if let selectedIndex = theCurrentViewModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
         theCurrentViewModel.arr_strRemoveAttachment.append(theCurrentViewModel.arr_fileList[selectedIndex].fileId)
         }
         }
         self.theCurrentViewModel.arr_selectedFile.remove(at: sender.tag)*/
        
        
        let attachment = theCurrentViewModel.arr_selectedFile[sender.tag]
        theCurrentViewModel.arr_RemoveSelectedFile.append(attachment)
        theCurrentViewModel.arr_selectedFile.remove(at: sender.tag)
        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    @IBAction func onBtnViewMoreAction(_ sender: Any) {
        theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
        theCurrentViewModel.getActionNotesHistoryWebService(status: theCurrentViewModel.strNoteStatus)
    }
}

//MARK:- UIDocumentPickerDelegate
extension AddPrimaryAreaVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    }) {
                        
                    } else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                }) {
                    
                } else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        }
        else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported, \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension AddPrimaryAreaVC:IQAudioRecorderViewControllerDelegate {
    
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")
        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
                //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
//                let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
//                
//                //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
//                self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                
                
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.theCurrentView.collectionSelectedFile.reloadData()
                    }
                    
                }
                
            }
            
        }
        controller.dismiss(animated: true, completion: nil)
        

    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}
