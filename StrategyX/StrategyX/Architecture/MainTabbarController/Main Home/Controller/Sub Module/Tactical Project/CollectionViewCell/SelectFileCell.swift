//
//  SelectFileCell.swift
//  StrategyX
//
//  Created by Jaydeep on 22/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SelectFileCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var btnCloseOutlet: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
    //MARK:- Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
