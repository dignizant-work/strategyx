//
//  AddTacticalProjectViewModel.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddTacticalProjectViewModel {

    //MARK:-Variable
    fileprivate weak var theController:AddTacticalProjectVC!

    var imageBG:UIImage?

    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?

    var arr_selectedFile:[AttachmentFiles] = []
    var arr_RemoveSelectedFile:[AttachmentFiles] = []

    var selectedDueDate:Date?
    var selectedrevisedDate:Date?
    
    // get category
    var arr_categoryList:[Categories] = []
    var strSelectedCategoryID:String = ""

    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""

    var strNoteStatus:String = ""
    var arr_noteHistoryStatus:[JSON] = []
    
    var isForAddAction = false
    var isForAddNoteAction = false
    var isStatusArchive = false

    var selectedData:JSON = JSON(["category":"","company":"","department":"","description":"","assignto":"","projectname":"","startdate":"","duedate":"","reviseddate":""])
    
    var isEditScreen = viewType.add
    var theTacticalProjectDetailModel:TacticalProjectDetail?
    
    // Related to
    var relatedToSelectionType = AddActionModel.relatedToSelectionType.general
    
    // Related to List
    var arr_relatedList:[RelatedTo] = []
    var strSelectedRelatedId = ""
    
    // Related to PrimaryArea List
    var arr_actionPrimaryAreaList:[PrimaryArea] = []
    var strSelectedPrimaryAreaId = ""
    
    // Related to SecondryArea List
    var arr_actionSecondryAreaList:[SecondaryArea] = []
    var strSelectedSecondryAreaId = ""
    
    // Related to StrategyGoal List
    var arr_actionStrategyGoalList:[StrategyGoalList] = []
    var strSelectedStrategyGoalId = ""
    
    var selectionRelatedToDueDate = ""
    var selectionRelatedToDueDateColor = ""
    var selectionRelatedToActionID = ""
    
    var cellType:[AddActionModel.celltype] = [.header,.createdBy,.title,.relatedToGeneral,.tag,.company,.assignTo,.department,.approvedBy,.createdDate,.dueDate,.workDate,.duration,.subtask,.attachment,.bottom]
    
    enum categorySelectionType:Int {
        case category            = 1
        case company             = 2
        case assignTo            = 3
        case department          = 4
    }
    enum dateSelctionType:Int {
        case due                = 2
        case resived            = 3
    }
    
    //MARK:- LifeCyle
    init(theController:AddTacticalProjectVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
         (theController.view as? AddTacticalProjectView)?.btnSelectCompanyOutlet.isUserInteractionEnabled = false
         }*/
        if isEditScreen == .edit {
            (theController.view as? AddTacticalProjectView)?.btnSelectCompany.isUserInteractionEnabled = false
        } else {
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                (theController.view as? AddTacticalProjectView)?.btnSelectCompany.isUserInteractionEnabled = false
            }
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService()
            
        } else {
            strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            selectedData["company"].stringValue = Utility.shared.userData.companyName
            (theController.view as? AddTacticalProjectView)?.lblCompany.text = Utility.shared.userData.companyName
            (theController.view as? AddTacticalProjectView)?.viewCompany.isHidden = true
            assignUserListService()
        }
    }
    
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        (theController.view as? AddTacticalProjectView)?.activitySpinner.hidesWhenStopped = true
        (theController.view as? AddTacticalProjectView)?.constrainViewPagerHeight.constant = 40.0
        (theController.view as? AddTacticalProjectView)?.viewOfPager.isHidden = false
        
        if isAnimating {
            (theController.view as? AddTacticalProjectView)?.activitySpinner.startAnimating()
            (theController.view as? AddTacticalProjectView)?.viewMore.isHidden = true
        } else {
            (theController.view as? AddTacticalProjectView)?.activitySpinner.stopAnimating()
            (theController.view as? AddTacticalProjectView)?.viewMore.isHidden = false
            
            if nextOffset == -1 {
                (theController.view as? AddTacticalProjectView)?.viewOfPager.isHidden = true
                (theController.view as? AddTacticalProjectView)?.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    
    func showTheEditData() {
        guard let theModel = theTacticalProjectDetailModel else { return }
        (theController.view as? AddTacticalProjectView)?.lblAddTitle.text = "Edit Tactical Project"
        selectedData["projectname"].stringValue = theModel.project_name
        selectedData["description"].stringValue = theModel.tactical_description
        selectedData["company"].stringValue = theModel.company_name
        selectedData["assignto"].stringValue = theModel.assigned_to
        selectedData["department"].stringValue = theModel.department_name
        selectedData["createdby"].stringValue = theModel.created_by
        selectedData["duedate"].stringValue = theModel.duedate
        selectedData["reviseddate"].stringValue = theModel.revised_date
        selectedData["projectcost"].stringValue = theModel.cost
        selectedData["category"].stringValue = theModel.category_name
        selectedData["relatedto"].stringValue = theModel.label
        strSelectedRelatedId = "\(theModel.relatedId)"

        selectedDueDate = theModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1)
        
        if let dueDate = selectedDueDate {
            selectedData["duedate"].stringValue = dueDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        selectedrevisedDate = theModel.revised_date.convertToDate(formate: DateFormatterInputType.inputType1)
        if let revisedDate = selectedrevisedDate {
            selectedData["reviseddate"].stringValue = revisedDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        let createdDate = theModel.created_date.convertToDate(formate: DateFormatterInputType.inputType1)
        if let createdDates = createdDate {
            selectedData["createdDate"].stringValue = createdDates.string(withFormat: DateFormatterOutputType.outputType7)
        }
        strSelectedCategoryID = theModel.category_id
        strSelectedCompanyID = theModel.company_id
        strSelectedUserAssignID = theModel.assignee_id
        strSelectedDepartmentID = theModel.department_id
        
        (theController.view as? AddTacticalProjectView)?.txtTitle.isUserInteractionEnabled = false
        (theController.view as? AddTacticalProjectView)?.txtDescription.isUserInteractionEnabled = false

        (theController.view as? AddTacticalProjectView)?.showUpdateData(theModel: selectedData)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewSelectCategory)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewProjectCost)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewSelectionDueDate)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewSelectedCompany)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewSelectionAssignTo)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewSelectionDepartment)
        
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewRelatedToTxt)
        (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: false, view: (theController.view as? AddTacticalProjectView)!.viewCommonRelatedToTxt)
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToTitle.text = theModel.label
        (theController.view as? AddTacticalProjectView)?.lblRelatedTo.text = theModel.label
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedTo.text = theModel.related_title
        (theController.view as? AddTacticalProjectView)?.viewCommanRelatedTo.isHidden = false
        if theModel.relatedId == 0 {
            (theController.view as? AddTacticalProjectView)?.viewCommanRelatedTo.isHidden = true
        }
        
        DispatchQueue.main.async { [weak self] in
            (self?.theController.view as? AddTacticalProjectView)?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        arr_selectedFile.removeAll()
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        if UserDefault.shared.isCanEditForm(strOppoisteID: theModel.created_by_id) {
            (theController.view as? AddTacticalProjectView)?.txtTitle.isUserInteractionEnabled = true
            (theController.view as? AddTacticalProjectView)?.txtDescription.isUserInteractionEnabled = true

            (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: true, view: (theController.view as? AddTacticalProjectView)!.viewSelectCategory)
            (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: true, view: (theController.view as? AddTacticalProjectView)!.viewProjectCost)
            (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: true, view: (theController.view as? AddTacticalProjectView)!.viewSelectionAssignTo)
            (theController.view as? AddTacticalProjectView)?.isEnableView(isEnable: true, view: (theController.view as? AddTacticalProjectView)!.viewSelectionDepartment)
        }
            
    }
   
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .category:
            strSelectedCategoryID = arr_categoryList[index].id
            print("Category id :- \(strSelectedCategoryID )")
            selectedData["category"].stringValue = str
            break
        case .company:
            strSelectedCompanyID = arr_companyList[index].id
            assignUserListService()
            
            selectedData["company"].stringValue = str
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            strSelectedUserAssignID = arr_assignList[index].user_id
            selectedData["assignto"].stringValue = str
//            var isEdit = false
            
            if isEditScreen == .add {
                departmentListService()
                selectedData["department"].stringValue = ""
                strSelectedDepartmentID = ""
//                isEdit = true
                
                //                isDropDownLoadingAtIndex = 6
            } else {
                if let theModel = theTacticalProjectDetailModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.created_by_id) {
                    departmentListService()
                    selectedData["department"].stringValue = ""
                    strSelectedDepartmentID = ""
//                    isEdit = true
                }
            }
 
            break
        case .department:
            strSelectedDepartmentID = arr_departmentList[index].id
            selectedData["department"].stringValue = str
            break
            
        }
        (theController.view as? AddTacticalProjectView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    func updateDropDownData(categorySelection:AddActionVC.categorySelectionType, str:String, cellType:AddActionModel.celltype, index:Int)  {
        var index = 0
        if let ind = self.cellType.firstIndex(where: {$0 == cellType}) {
            index = ind
        }
        
        print("selected index \(index)")
        switch categorySelection {
        case .company:
            break
        case .department:
            break
        case .assignTo:
            break
        case .relatedTo:
            if let selectedIndex = arr_relatedList.firstIndex(where: {$0.option == str}) {
                strSelectedRelatedId = "\(arr_relatedList[selectedIndex].value)"
            }
            resetRelatedData()
            removeRelatedCellFromCelltype()
            updateViewCommanRelatedTo(str: str)
            
            self.selectedData["relatedto"].stringValue = str
            (theController.view as? AddTacticalProjectView)?.lblRelatedTo.text = str
            return
        case .approvedBy:
            break
        case .relatedToStrategyPrimaryArea:
            if let selectedIndex = arr_actionPrimaryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedPrimaryAreaId = arr_actionPrimaryAreaList[selectedIndex].id
            }
            self.selectedData["strategyprimaryarea"].stringValue = str
            (theController.view as? AddTacticalProjectView)?.lblPrimaryArea.text = str
            strSelectedSecondryAreaId = ""
            self.selectedData["strategysecondaryarea"].stringValue = ""
            (theController.view as? AddTacticalProjectView)?.lblSecondaryArea.text = "Select secondary area"
            arr_actionSecondryAreaList.removeAll()
            
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddTacticalProjectView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            
            actionSecondaryAreaListService()
            
            
            break
        case .relatedToStrategySecondaryArea:
            if let selectedIndex = arr_actionSecondryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedSecondryAreaId = arr_actionSecondryAreaList[selectedIndex].id
            }
            self.selectedData["strategysecondaryarea"].stringValue = str
            (theController.view as? AddTacticalProjectView)?.lblSecondaryArea.text = str
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddTacticalProjectView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            actionStrategyGoalListService()
            break
        case .relatedToStrategyGoal:
            (theController.view as? AddTacticalProjectView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddTacticalProjectView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddTacticalProjectView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionStrategyGoalList.firstIndex(where: {$0.title == str}) {
                strSelectedStrategyGoalId = arr_actionStrategyGoalList[selectedIndex].id
                (theController.view as? AddTacticalProjectView)?.viewStrategyGoalRelatedToDueDate.isHidden = false
                selectionRelatedToDueDate = arr_actionStrategyGoalList[selectedIndex].duedate
                selectionRelatedToActionID = arr_actionStrategyGoalList[selectedIndex].id
                if UserDefault.shared.isArchiveAction(strStatus: arr_actionStrategyGoalList[selectedIndex].status) {
                    (theController.view as? AddTacticalProjectView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                    (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
                } else {
                    (theController.view as? AddTacticalProjectView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                    (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
                }
                if let view = (theController.view as? AddTacticalProjectView) {
                    view.updateDateView(label: (view.lblStrategyGoalRelatedToDueDate), strDueDate: arr_actionStrategyGoalList[selectedIndex].duedate, color: arr_actionStrategyGoalList[selectedIndex].duedateColor)
                }
            }
            self.selectedData["strategygoal"].stringValue = str
            (theController.view as? AddTacticalProjectView)?.lblStrategyGoal.text = str
            break
        case .relatedToRisk:
            break
        case .relatedToTacticalProject:
            break
        case .relatedToFocus:
            break
        case .relatedToSuccessFactor:
            break
        case .selectTag:
            break
        case .relatedToIdea:
            break
        case .relatedToProblem:
            break
        }
    }
    
    func resetRelatedData(){
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedTo.text = "Select"
        
        self.selectedData["strategygoal"].stringValue = ""
        strSelectedStrategyGoalId = ""
        (theController.view as? AddTacticalProjectView)?.lblStrategyGoal.text = "Select strategy goal"
        
        self.selectedData["strategysecondaryarea"].stringValue = ""
        strSelectedSecondryAreaId = ""
        (theController.view as? AddTacticalProjectView)?.lblSecondaryArea.text = "Select secondary area"
        
        self.selectedData["strategyprimaryarea"].stringValue = ""
        strSelectedPrimaryAreaId = ""
        (theController.view as? AddTacticalProjectView)?.lblPrimaryArea.text = "Select primary area"
        
        arr_actionStrategyGoalList = []
        arr_actionPrimaryAreaList = []
        arr_actionSecondryAreaList = []
    }
    
    func removeRelatedCellFromCelltype() {
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToRisk}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToTacticalProject}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToFocus}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToSuccessFactor}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToIdea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToProblem}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.primaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.secondaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.strategyGoal}) {
            cellType.remove(at: index)
        }
        
    }
    
    func updateViewCommanRelatedTo(str:String) {
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToTitle.text = str
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedTo.text = "Select " + str
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToTitle.showAstrike = true
        (theController.view as? AddTacticalProjectView)?.viewCommanRelatedTo.isHidden = "General Project".lowercased() == str.lowercased()
        (theController.view as? AddTacticalProjectView)?.viewPrimaryArea.isHidden = true
        (theController.view as? AddTacticalProjectView)?.viewSecondaryArea.isHidden = true
        (theController.view as? AddTacticalProjectView)?.viewStrategyGoal.isHidden = true
        selectionRelatedToDueDate = ""
        selectionRelatedToActionID = ""
        (theController.view as? AddTacticalProjectView)?.lblStrategyGoalRelatedToDueDate.text = ""
        (theController.view as? AddTacticalProjectView)?.lblCommanRelatedToDueDate.text = ""
        (theController.view as? AddTacticalProjectView)?.viewCommonRelatedToDueDate.isHidden = true
        (theController.view as? AddTacticalProjectView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
        
        
        switch str.localizedLowercase {
        case AddActionModel.relatedToSelectionType.general.rawValue.localizedLowercase, "General Project".localizedLowercase:
            (theController.view as? AddTacticalProjectView)?.viewCommanRelatedTo.isHidden = true
            relatedToSelectionType = AddActionModel.relatedToSelectionType.general
            break
        case AddActionModel.relatedToSelectionType.risk.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.tactical.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.focus.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.successFactor.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.idea.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.problem.rawValue.localizedLowercase:
            break
        case AddActionModel.relatedToSelectionType.strategy.rawValue.localizedLowercase:
            actionPrimaryAreaListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.strategy
            (theController.view as? AddTacticalProjectView)?.viewCommanRelatedTo.isHidden = true
            (theController.view as? AddTacticalProjectView)?.viewPrimaryArea.isHidden = false
            (theController.view as? AddTacticalProjectView)?.viewSecondaryArea.isHidden = false
            (theController.view as? AddTacticalProjectView)?.viewStrategyGoal.isHidden = false
            //            cellType.insert(AddActionModel.celltype.primaryArea, at: 4)
            //            cellType.insert(AddActionModel.celltype.secondaryArea, at: 5)
            //            cellType.insert(AddActionModel.celltype.strategyGoal, at: 6)
            break
        default:
            break
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String, isGoal:Bool) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate, isGoal: isGoal)
    }
    func updateDate(strDate:String,type:dateSelctionType,date:Date) {
        switch type {
        case .due:
            selectedDueDate = date
            selectedData["duedate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddTacticalProjectModel.celltype.dueDate}) {
//                self.(theController.view as? AddTacticalProjectView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
        //            (theController.view as? AddTacticalProjectView)?.tableView.reloadRows(at: [IndexPath.init(row: 7, section: 0)], with: UITableView.RowAnimation.none)
        case .resived:
            selectedrevisedDate = date
            selectedData["reviseddate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddTacticalProjectModel.celltype.reviseDate}) {
//                self.(theController.view as? AddTacticalProjectView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
            //            (theController.view as? AddTacticalProjectView)?.tableView.reloadRows(at: [IndexPath.init(row: 8, section: 0)], with: UITableView.RowAnimation.none)
        }
        (theController.view as? AddTacticalProjectView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateTacticalProjectDetailModel(theModel:TacticalProjectDetail) {
        if let revisedDate1 = selectedrevisedDate {
            theTacticalProjectDetailModel?.revised_date = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        theTacticalProjectDetailModel?.project_name = selectedData["projectname"].stringValue
        theTacticalProjectDetailModel?.tactical_description = selectedData["description"].stringValue
        
        theTacticalProjectDetailModel = theModel
    }
    func validateForm() {
        theController.view.endEditing(true)
        if selectedData["projectname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter project name")
            return
        }  /*else if selectedData["projectcost"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter project cost")
            return
        }*/ else if selectedData["company"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select company")
            return
        } else if selectedData["relatedto"].stringValue.isEmpty, isEditScreen == .add {
            self.theController.showAlertAtBottom(message: "Please select related to")
            return
        } else if !selectedData["relatedto"].stringValue.isEmpty, isEditScreen == .add {
            if strSelectedRelatedId == "1" {
                if selectedData["strategyprimaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select primary area")
                    return
                } else if selectedData["strategysecondaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select secondry area")
                    return
                } else if selectedData["strategygoal"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select strategy goal")
                    return
                }
            }
        }
        if selectedData["category"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select category")
            return
        } else if !selectedData["projectcost"].stringValue.isEmpty, selectedData["projectcost"].stringValue.isNumeric == false {
            self.theController.showAlertAtBottom(message: "Please enter only digit for project cost")
            return
        } else if selectedData["assignto"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select assigned to")
            return
        } else if selectedData["department"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select department")
            return
        } else if selectedData["duedate"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select due date")
            return
        } else if !selectedData["reviseddate"].stringValue.isEmpty {
            if let dueDate1 = selectedDueDate, let revisedDate1 = selectedrevisedDate,revisedDate1 < dueDate1 {
                self.theController.showAlertAtBottom(message: "Revised date should be grater then due date")
                return
            }
        }
        var dueDate = ""
        var revisedDate = ""
        
        if let dueDate1 = selectedDueDate {
            dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let revisedDate1 = selectedrevisedDate {
            revisedDate = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var removeAttachment:[String] = []
        
        var voiceCount = 0
        var fileCount = 0
        
        for item in arr_RemoveSelectedFile {
            if item.isOldAttachment {
                removeAttachment.append(item.attachmentID)
            }
            //            removeFile.append([APIKey.key_id:item.attachmentID])
        }
        
        for i in 0..<arr_selectedFile.count {
            if arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
            } else {
                if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
//        var param = parameter
//        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")
        
        if isEditScreen == .edit {
            let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_tactical_project_id:theTacticalProjectDetailModel?.tactical_projects_id ?? "",
                             APIKey.key_project_name:selectedData["projectname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_description:selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_cost:selectedData["projectcost"].stringValue,
                             APIKey.key_category:strSelectedCategoryID,
                             APIKey.key_assigned_to:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_revised_date:revisedDate,
                             APIKey.key_remove_attechment:removeAttachment.joined(separator: ",")] as [String : Any]
            
            editTacticalProject(parameter: parameter,fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        } else {
            var notes = ""
            
            if let history = arr_notesHistoryList, history.count > 0 {
                var noteHistory:[[String:String]] = []
                history.forEach { (item) in
                    noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
                }
                notes = JSON(noteHistory).rawString() ?? ""
            }
            var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_project_name:selectedData["projectname"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_description:selectedData["description"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_cost:selectedData["projectcost"].stringValue,
                             APIKey.key_category:strSelectedCategoryID,
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned_to:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_notes:notes,
                             APIKey.key_related_to:strSelectedRelatedId,
                             APIKey.key_duedate:dueDate] as [String : Any]
            
            if strSelectedRelatedId == "4"{
//                parameter[APIKey.key_goal]  = strSelectedFocusId
            } else if strSelectedRelatedId == "6"{
//                parameter[APIKey.key_success_factor]  = strSelectedSuccessFactorId
            } else if strSelectedRelatedId == "3"{
//                parameter[APIKey.key_tactical_projects] = strSelectedTacticalProjectId
            } else if strSelectedRelatedId == "2"{
//                parameter[APIKey.key_risk]  = strSelectedRiskId
            } else if strSelectedRelatedId == "1"{
                parameter[APIKey.key_strategy_goal]  = strSelectedStrategyGoalId
            } else if strSelectedRelatedId == "7"{
//                parameter[APIKey.key_idea]  = strSelectedIdeaAndProblemID
            } else if strSelectedRelatedId == "8"{
//                parameter[APIKey.key_problem] = strSelectedIdeaAndProblemID
            }
            
            addTacticalProject(parameter: parameter, fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        }
        
    }
}

//MARK:- Api Call
extension AddTacticalProjectViewModel {
    func categoiesListService() {
        (theController.view as? AddTacticalProjectView)?.loadCategoryActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_category_for:CategoryFor.tacticalProject.rawValue]
        CommanListWebservice.shared.getCategories(parameter: parameter, success: { [weak self] (list) in
            (self?.theController.view as? AddTacticalProjectView)?.loadCategoryActivity(isStart: false)
            self?.arr_categoryList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadCategoryActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func companylistService() {
        (theController.view as? AddTacticalProjectView)?.loadCompanyActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            (self?.theController.view as? AddTacticalProjectView)?.loadCompanyActivity(isStart: false)
            }, failed: { [weak self](error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadCompanyActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func assignUserListService() {
        (theController.view as? AddTacticalProjectView)?.loadAssignToActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
//            self?.theController.updateTableForDropDownStopLoading()
            (self?.theController.view as? AddTacticalProjectView)?.loadAssignToActivity(isStart: false)

            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadAssignToActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    func departmentListService() {
        (theController.view as? AddTacticalProjectView)?.loadDepartmentActivity(isStart: true)

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            (self?.theController.view as? AddTacticalProjectView)?.loadDepartmentActivity(isStart: false)

//            self?.theController.updateTableForDropDownStopLoading()
            if let obj = self, obj.isEditScreen == .add, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddTacticalProjectView)?.showDropDownUpdatedData(theModel: obj.selectedData)
//                obj.theController.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            } else if let obj = self, obj.isEditScreen == .edit, obj.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddTacticalProjectView)?.showDropDownUpdatedData(theModel: obj.selectedData)
            }
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadDepartmentActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    
    func relatedListService() {
        (theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (theController.view as? AddTacticalProjectView)?.btnRelatedTo, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_related_type:APIKey.key_assign_to_for_tactical_projects]
        ActionsWebService.shared.getRelatedTo(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnRelatedTo, isStart: false)
            self?.arr_relatedList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnRelatedTo, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionPrimaryAreaListService() {
        (theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (theController.view as? AddTacticalProjectView)?.btnPrimaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getPrimaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnPrimaryArea, isStart: false)
            self?.arr_actionPrimaryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnPrimaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSecondaryAreaListService() {
        (theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (theController.view as? AddTacticalProjectView)?.btnSecondaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_primary_area_id:strSelectedPrimaryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getSecondaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnSecondaryArea, isStart: false)
            self?.arr_actionSecondryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnSecondaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionStrategyGoalListService() {
        (theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (theController.view as? AddTacticalProjectView)?.btnStrategyGoal, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_secondary_area_id:strSelectedSecondryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getStrategyGoalList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnStrategyGoal, isStart: false)
            self?.arr_actionStrategyGoalList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.loadActivityOnButton(button: (self?.theController.view as? AddTacticalProjectView)?.btnStrategyGoal, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String, isGoal:Bool) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        switch relatedToSelectionType {
        case .general:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
            break
        case .strategy:
            parameter[APIKey.key_date_for] = APIKey.key_strategy_goals
            break
        case .risk:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_risks
            break
        case .tactical:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_tactical_projects
            break
        case .focus:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_focuses
            break
        case .idea:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
        case .problem:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
            
        case .successFactor:
            break
        }
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        Utility.shared.showActivityIndicator()
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            if let obj = self, let model = theModel {
                obj.selectionRelatedToDueDate = model.revisedDate
                if let view = obj.theController.view as? AddTacticalProjectView {
                    view.updateDateView(label: isGoal ? view.lblStrategyGoalRelatedToDueDate : view.lblCommanRelatedToDueDate, strDueDate: model.revisedDate, color: model.revisedColor)
                }
                
                if obj.isForAddAction {
                    obj.theController.handlerEditDueDateChanged(model.revisedDate,model.revisedColor)
                }
            }
            Utility.shared.stopActivityIndicator()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theTacticalProjectDetailModel?.tactical_projects_id ?? "",
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_assign_to_for_tactical_projects,
                         APIKey.key_page_offset:"\(nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            (self?.theController.view as? AddTacticalProjectView)?.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)
        })
    }
    
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theTacticalProjectDetailModel?.tactical_projects_id ?? "",
                         APIKey.key_history_for: APIKey.key_assign_to_for_tactical_projects]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }
    
    func addTacticalProject(parameter:[String:Any], fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: true)
        TacticalProjectWebService.shared.addTacticalProject(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
            (self?.theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: false)
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateBottomCell()
        })
    }
    
    func editTacticalProject(parameter:[String:Any],fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: true)
        TacticalProjectWebService.shared.postEditTacticalProject(parameter: parameter,files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theDetailModel) in
            if !theDetailModel.revised_date.isEmpty {
                theDetailModel.duedate = theDetailModel.revised_date
                theDetailModel.duedateColor = theDetailModel.revisedColor
            }
            self?.updateTacticalProjectDetailModel(theModel:theDetailModel)
            (self?.theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: false)
            self?.theController.handlerUpdateModel(self?.theTacticalProjectDetailModel)
            self?.theController.backAction(isAnimation: false)
            }, failed: {[weak self] (error) in
                (self?.theController.view as? AddTacticalProjectView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateBottomCell()
        })
    }
    
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.strNoteStatus ?? "")
            (self?.theController.view as? AddTacticalProjectView)?.txtNote.text = ""
            completion()
            }, failed: { [weak self] (error) in
                completion()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
}
