//
//  TacticalProjectNotesTVCell.swift
//  StrategyX
//
//  Created by Haresh on 01/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class TacticalProjectNotesTVCell: UITableViewCell {

    @IBOutlet weak var btnAddVoiceOutlet: UIButton!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
    @IBOutlet weak var lblVoiceNotesName: UILabel!
    @IBOutlet weak var viewOfvoiceNotesName: UIView!
    
    var handlerSelectFileAction:() -> Void = {}
    var handlerAddvoiceAction:() -> Void = {}
    var handlerPostAction:() -> Void = {}
    var theModel:AddTacticalProjectViewModel?
    var handlerCollectionViewHeight:(CGFloat) -> Void = {height in}
    var handleRemoveFile:(Int) -> Void = {sender in}
    
    var handleRemoveAudioFile:() -> Void = {}
    var handleSelectedFileFromCollection:(AttachmentFiles) -> Void = {_ in}
    
    override func draw(_ rect: CGRect) {

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

//        viewFileName.addDashedBorder(color: appGreen)
//        let border = CAShapeLayer()
//        border.strokeColor = UIColor.black.cgColor
//        border.fillColor = nil
//        border.lineDashPattern = [4, 4]
//        border.path = UIBezierPath(rect: viewFileName.bounds).cgPath
//        border.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.size.width - 70.0, height: viewFileName.frame.size.width)
//        viewFileName.layer.addSublayer(border)
        
        collectionSelectedFile.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        // Initialization code
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
            self.viewFileName.addDashedBorder(color: appGreen)
        })
    }
    
    deinit {
        collectionSelectedFile.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("contentSize:= \(collectionSelectedFile.contentSize.height)")
           // self.contentView.layoutIfNeeded()
            let finalSize = collectionSelectedFile.contentSize.height
            //self.contentView.layoutIfNeeded()
            if theModel == nil{
                handlerCollectionViewHeight(finalSize)
                return
            }
            handlerCollectionViewHeight(finalSize + 50)
        }
    }
    
    

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(strFileName:String) {
        lblFileName.text = strFileName
       /* if theModel?.arr_selectedFile.count == 0{
            self.heightOfCollectionSelectedFile.constant = 0
        }
        else{
            self.heightOfCollectionSelectedFile.constant = 50
        }*/
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.delegate =  self
        collectionSelectedFile.dataSource = self
        collectionSelectedFile.reloadData()
    }
    
    //MARK:- IBAction
    @IBAction func onBtnPostAction(_ sender: Any) {
        handlerPostAction()
    }
    
    @IBAction func onAddVoiceNoteAction(_ sender: Any) {
        handlerAddvoiceAction()
    }
    
    @IBAction func onBtnSelecteFilesAction(_ sender: Any) {
        handlerSelectFileAction()
    }
    
    @IBAction func onBtnRemoveFileAction(_ sender: UIButton) {
        handleRemoveFile(sender.tag)
    }
    
    @IBAction func onBtnRemoveAudioAction(_ sender: UIButton) {
        handleRemoveAudioFile()
    }
}

extension TacticalProjectNotesTVCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if theModel == nil{
            return 0
        }
        return theModel?.arr_selectedFile.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
        let selectFile = theModel!.arr_selectedFile[indexPath.row]
        cell.lblFileName.text = selectFile.fileName
        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveFileAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width - 20
        return CGSize(width: width/2, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let arr = theModel?.arr_selectedFile, arr.count > 0, arr[indexPath.row].fileType == .audio {
            handleSelectedFileFromCollection(arr[indexPath.row])
        }
    }
    
}
