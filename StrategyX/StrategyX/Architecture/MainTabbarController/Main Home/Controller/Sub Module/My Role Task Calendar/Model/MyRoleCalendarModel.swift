//
//  MyRoleCalenderModel.swift
//  StrategyX
//
//  Created by Haresh on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class MyRoleCalendarModel {

    //MARK:- Variable
    fileprivate weak var theController:MyRoleCalendarVC!
    var imageBG:UIImage?

    var arr_month:[String] = []
    var arr_year:[String] = []
    var arr_reoccuring:[String] = []
    var arr_datesWithEvent:[String] = [] // yyyy-MM-dd
    var arr_datesWithMultipleEvents:[String] = [] // yyyy-MM-dd
    var selectedDataOfCustom = SelectedData()
    var selectedDate = Date()
    var selectedTime = Date()

    var isForEdit = false
    var theEditMyRoleModel:MyRole?
    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    var calendarReoccuringType:CalendarReoccuring?
    
    let monthDD = DropDown()
    let yearDD = DropDown()
    let reoccuringDD = DropDown()
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    //MARK:- LifeCycle
    init(theController:MyRoleCalendarVC) {
        self.theController = theController
        arr_month = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        let currentYear = Date().year
        for i in 0..<21 {
            arr_year.append("\(currentYear + i)")
        }
        arr_reoccuring = [CalendarReoccuring.everyDay.rawValue,CalendarReoccuring.everyWeek.rawValue,CalendarReoccuring.mondayToFriday.rawValue,CalendarReoccuring.everyMonth.rawValue, CalendarReoccuring.everyYear.rawValue,CalendarReoccuring.custom.rawValue]
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        print("point:=",point)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 30)

        
        switch dropDown {
        case monthDD:
            dropDown.dataSource = arr_month
            break
        case yearDD:
            dropDown.dataSource = arr_year
            break
        case reoccuringDD:
            dropDown.dataSource = arr_reoccuring
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
    
}

//MARK:- Api Call
extension MyRoleCalendarModel {
    func companylistService() {
        theController.loadingCompanyView(isLoadingApi: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            self?.theController.loadingCompanyView(isLoadingApi: false)
            
            }, failed: { [weak self](error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.theController.loadingCompanyView(isLoadingApi: false)
        })
    }
    func assignUserListService() {
        theController.loadingAssignToView(isLoadingApi: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
            self?.theController.loadingAssignToView(isLoadingApi: false)

             }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.theController.loadingAssignToView(isLoadingApi: false)
        })
    }
    func departmentListService() {
        theController.loadingDepartmentView(isLoadingApi: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            self?.theController.loadingDepartmentView(isLoadingApi: false)
            if let obj = self, !obj.isForEdit, list.count > 0 {
                obj.theController.updateDropDownData(categorySelection: MyRoleCalendarVC.categorySelectionType.department, str: list[0].name, index: 0)
            }
            }, failed: { [weak self] (error) in
                self?.theController.showAlertAtBottom(message: error)
                self?.theController.loadingDepartmentView(isLoadingApi: false)
        })
    }
    
    func addMyRole(parameter:[String:Any], completed:@escaping() -> Void) {
        MyRoleWebServices.shared.addMyRole(parameter: parameter, success: { [weak self] in
            completed()
            self?.theController.backAction(isAnimation: false)
        }, failed: { [weak self] (error) in
            completed()
            self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    func editMyRole(parameter:[String:Any], completed:@escaping() -> Void) {
        MyRoleWebServices.shared.editMyRole(parameter: parameter, success: { [weak self] (theModel) in
            self?.theController.handlerEditData(theModel)
            completed()
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                completed()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
}
