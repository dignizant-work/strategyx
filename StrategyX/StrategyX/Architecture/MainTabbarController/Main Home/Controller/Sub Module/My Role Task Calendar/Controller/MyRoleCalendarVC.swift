//
//  MyRoleCalenderVC.swift
//  StrategyX
//
//  Created by Haresh on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import FSCalendar

class MyRoleCalendarVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case company             = 2
        case assignTo            = 3
        case department          = 4
    }
    
    fileprivate lazy var theCurrentView:MyRoleCalendarView = { [unowned self] in
       return self.view as! MyRoleCalendarView
    }()
    
    fileprivate lazy var theCurrentModel:MyRoleCalendarModel = {
       return MyRoleCalendarModel(theController: self)
    }()
    
    var handlerClose:() -> Void = {}
    var handlerEditData:(MyRole) -> Void = { _ in }
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("height:=\(theCurrentView.calendar!.frame)")
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setCalenderDelegate(theDelegate: self)
        theCurrentView.calendar.select(theCurrentModel.selectedDate)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.monthDD, view: theCurrentView.viewMonth)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.yearDD, view: theCurrentView.viewYear)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.reoccuringDD, view: theCurrentView.viewReoccuring)
        
        theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[0])
        theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[0])

        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == theCurrentModel.selectedDate.monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        if let index = theCurrentModel.arr_year.firstIndex(where: {$0.localizedLowercase == "\(theCurrentModel.selectedDate.year)".localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[index])
        }
        
        if !theCurrentModel.isForEdit { //  for New Add
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                theCurrentModel.companylistService()
            } else {
                theCurrentView.viewCompany.isHidden = true
                theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentView.lblCompanyName.text = Utility.shared.userData.companyName
                theCurrentModel.assignUserListService()
            }
            
        }
        
        
        if theCurrentModel.isForEdit {
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                theCurrentView.viewCompany.isHidden = true

            } else {
                theCurrentView.viewCompany.isUserInteractionEnabled = false
                theCurrentView.lblCompanyName.alpha = 0.5

            }
            setEditData()
        }
        
    }
    func setTheData(imgBG:UIImage?, isForEdit:Bool = false, theModel:MyRole? = nil) {
        theCurrentModel.imageBG = imgBG
        theCurrentModel.theEditMyRoleModel = theModel
        theCurrentModel.isForEdit = isForEdit
        if let startDate = theModel?.startDate.convertToDate(formate: DateFormatterInputType.inputType1) {
            theCurrentModel.selectedDate = startDate
            theCurrentModel.selectedTime = startDate
        }
    }
    
    func setEditData() {
        guard let model = theCurrentModel.theEditMyRoleModel else { return }
        
//        theCurrentView.viewCompany.isHidden = true
        theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
        theCurrentView.viewAssignedTo.isUserInteractionEnabled = false
        theCurrentView.viewDepartment.isUserInteractionEnabled = false
        theCurrentView.viewReoccuring.isUserInteractionEnabled = false
        theCurrentView.calendar.isUserInteractionEnabled = false
        theCurrentView.viewMonth.isUserInteractionEnabled = false
        theCurrentView.viewYear.isUserInteractionEnabled = false
        
        theCurrentView.lblAssignedToName.alpha = 0.5
        theCurrentView.lblDepartmentName.alpha = 0.5
        theCurrentView.lblReoccuring.alpha = 0.5
        
        theCurrentModel.strSelectedCompanyID = model.companyId
        theCurrentView.lblCompanyName.text = model.companyName
        theCurrentModel.strSelectedUserAssignID = model.assigneeId
        theCurrentView.lblAssignedToName.text = model.assignedTo
        theCurrentModel.strSelectedDepartmentID = model.departmentId
        theCurrentView.lblDepartmentName.text = model.department

        if UserDefault.shared.isCanEditForm(strOppoisteID: model.createdBy) {
            theCurrentView.viewAssignedTo.isUserInteractionEnabled = true
            theCurrentView.viewDepartment.isUserInteractionEnabled = true
            theCurrentView.viewReoccuring.isUserInteractionEnabled = true
            theCurrentView.calendar.isUserInteractionEnabled = true
            theCurrentView.viewMonth.isUserInteractionEnabled = true
            theCurrentView.viewYear.isUserInteractionEnabled = true

            theCurrentView.lblAssignedToName.alpha = 1
            theCurrentView.lblDepartmentName.alpha = 1
            theCurrentView.lblReoccuring.alpha = 1
            theCurrentModel.assignUserListService()
        }
        
        theCurrentView.lblReoccuringTitle.text = "Edit Reoccurring Role Action"
        theCurrentView.btnSave.setTitle("Edit", for: .normal)
        theCurrentView.txtTitle.text = model.title
 
        if let startDate = model.startDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType9) {
            theCurrentView.lblTime.text = startDate
        }
        if let duration = Int(model.duration) {
            let hours = duration / 60
            let minute = duration % 60
            theCurrentView.txtDurationHours.text = "\(hours)"
            theCurrentView.txtDurationMint.text = "\(minute)"
        }
        
        /*
         required field : 'title','start_date','duration','reoccuring'
         
         
         duration : in minutes ,
         reoccuring : 0 = Custom,
         1 = Every Day,
         7 = Every Week,
         30 = Every Month,
         365 = Every Year
         
         custom_frequency :     1 = Daily,
         7 = Weekly,
         30 = Monthly,
         365 = Yearly
         
         custom_every : Every $ days OR $ weeks OR $ month OR $ years
         
         custom_selected :     Comma Separated : If days = 1 to 7 , If dates = 1 to 31, if months = 1 to 12
         
         each_or_on_the :     pass (On the , Each)
         Used only when custom_frequency is monthly(30)
         
         is_on_the : [0 = No, 1 = Yes] - Used only when custom_frequency is yearly(365)
         
         on_the : Used only when custom_frequency is monthly(30) or yearly(365)
         
         day : custom_frequency
         */
        
        switch model.reoccuring {
            
        case "1":
//            reoccuring = 1
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.everyDay

            break
        case "5":
            //            reoccuring = 1
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.mondayToFriday
            
            break
        case "7":
//            reoccuring = 7
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.everyWeek

            break
        case "30":
//            reoccuring = 30
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.everyMonth

            break
        case "365":
//            reoccuring = 365
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.everyYear

            break
        case "0":
//            reoccuring = 0
            theCurrentModel.calendarReoccuringType = CalendarReoccuring.custom
            switch model.customFrequency {
                
            case "1":
                
                theCurrentModel.selectedDataOfCustom.frequencyType = CalendarFrequencyType.daily
//                custom_frequency = 1
                break
            case "7":
                theCurrentModel.selectedDataOfCustom.frequencyType = CalendarFrequencyType.weekly

//                custom_frequency = 7
                let custom_selected = model.customSelected.components(separatedBy: ",").map({ Int($0) ?? 0 }).filter({$0 != 0})
                theCurrentModel.selectedDataOfCustom.weeklySelectedDays = custom_selected
                break
            case "30":
                theCurrentModel.selectedDataOfCustom.frequencyType = CalendarFrequencyType.monthly

//                custom_frequency = 30
                
                let custom_selected = model.customSelected.components(separatedBy: ",").map({ Int($0) ?? 0 }).filter({$0 != 0})
                theCurrentModel.selectedDataOfCustom.monthlySelectedDays = custom_selected
                
                let each_or_on_the = model.eachOrOnThe.isEmpty ? true : model.eachOrOnThe.localizedLowercase == "each" ? true : false
                theCurrentModel.selectedDataOfCustom.monthlyIsSelectedEach = each_or_on_the
                
//                var on_the = model.onThe.localizedLowercase
                switch model.onThe.localizedLowercase {
                case CalendarMonthlyOnTheDayNumberType.first.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.first
                    break
                case CalendarMonthlyOnTheDayNumberType.second.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.second
                    break
                case CalendarMonthlyOnTheDayNumberType.third.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.third
                    break
                case CalendarMonthlyOnTheDayNumberType.fourth.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.fourth
                    break
                case CalendarMonthlyOnTheDayNumberType.fifth.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.fifth
                    break
                case CalendarMonthlyOnTheDayNumberType.last.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.last
                    break
                
                default:
                    break
                }
                
                let day = (model.day).localizedLowercase
                switch day {
                case CalendarMonthlyOnTheDayType.monday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.monday
                    break
                case CalendarMonthlyOnTheDayType.tuesday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.tuesday
                    break
                case CalendarMonthlyOnTheDayType.wednesday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.wednesday
                    break
                case CalendarMonthlyOnTheDayType.thursday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.thursday
                    break
                case CalendarMonthlyOnTheDayType.friday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.friday
                    break
                case CalendarMonthlyOnTheDayType.saturday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.saturday
                    break
                case CalendarMonthlyOnTheDayType.sunday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.sunday
                    break
                case CalendarMonthlyOnTheDayType.day.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.day
                    break
                case CalendarMonthlyOnTheDayType.weekday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.weekday
                    break
                case CalendarMonthlyOnTheDayType.weekendDay.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.weekendDay
                    break
                default:
                    break
                }
                
//                day = theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType.rawValue
                break
            case "365":
//                custom_frequency = 365
                theCurrentModel.selectedDataOfCustom.frequencyType = CalendarFrequencyType.yearly
                let custom_selected = model.customSelected.components(separatedBy: ",").map({ Int($0) ?? 0 }).filter({$0 != 0})
                theCurrentModel.selectedDataOfCustom.yearlySelectedMonths = custom_selected

//                custom_selected = theCurrentModel.selectedDataOfCustom.yearlySelectedMonths.map({"\($0)"}).joined(separator: ",")
//                is_on_the = theCurrentModel.selectedDataOfCustom.yearlyIsSelectedOnThe ? "1" : "0"
                let is_on_the = model.isOnThe == "1" ? true : false
                theCurrentModel.selectedDataOfCustom.yearlyIsSelectedOnThe = is_on_the
                
//                on_the = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType.rawValue
                switch model.onThe.localizedLowercase {
                case CalendarMonthlyOnTheDayNumberType.first.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.first
                    break
                case CalendarMonthlyOnTheDayNumberType.second.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.second
                    break
                case CalendarMonthlyOnTheDayNumberType.third.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.third
                    break
                case CalendarMonthlyOnTheDayNumberType.fourth.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.fourth
                    break
                case CalendarMonthlyOnTheDayNumberType.fifth.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.fifth
                    break
                case CalendarMonthlyOnTheDayNumberType.last.rawValue.localizedLowercase :
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.last
                    break
                    
                default:
                    break
                }
//                day = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType.rawValue
                
                let day = (model.day).localizedLowercase
                switch day {
                case CalendarMonthlyOnTheDayType.monday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.monday
                    break
                case CalendarMonthlyOnTheDayType.tuesday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.tuesday
                    break
                case CalendarMonthlyOnTheDayType.wednesday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.wednesday
                    break
                case CalendarMonthlyOnTheDayType.thursday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.thursday
                    break
                case CalendarMonthlyOnTheDayType.friday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.friday
                    break
                case CalendarMonthlyOnTheDayType.saturday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.saturday
                    break
                case CalendarMonthlyOnTheDayType.sunday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.sunday
                    break
                case CalendarMonthlyOnTheDayType.day.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.day
                    break
                case CalendarMonthlyOnTheDayType.weekday.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.weekday
                    break
                case CalendarMonthlyOnTheDayType.weekendDay.rawValue.localizedLowercase:
                    theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType = CalendarMonthlyOnTheDayType.weekendDay
                    break
                default:
                    break
                }
                
                break
            default:
                break
            }
            
//            print("custom_frequency:=",custom_frequency)
//            print("custom_selected:=",custom_selected)
//            print("each_or_on_the:=",each_or_on_the)
//            print("is_on_the:=",is_on_the)
//            print("on_the:=",on_the)
//            print("day:=",day)
            
            break
        default:
            break
        }
        
        if let reccouring = theCurrentModel.calendarReoccuringType {
            theCurrentView.updateDroDownTextFor(reoccuring: reccouring.rawValue)
        }
        
    }
    
    //MARK:- Update ViewModel
    func loadingCompanyView(isLoadingApi:Bool) {
        theCurrentView.btnCompanyActivity.loadingIndicator(isLoadingApi)
    }
    func loadingAssignToView(isLoadingApi:Bool) {
        theCurrentView.btnAssignedToActivity.loadingIndicator(isLoadingApi)
    }
    func loadingDepartmentView(isLoadingApi:Bool) {
        theCurrentView.btnDepartmentActivity.loadingIndicator(isLoadingApi)
    }
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .company:
            theCurrentModel.strSelectedCompanyID = theCurrentModel.arr_companyList[index].id
            theCurrentModel.assignUserListService()
            
            theCurrentView.lblCompanyName.text = str
            theCurrentView.lblAssignedToName.text = "Select assigned to"
            theCurrentView.lblDepartmentName.text = "Select department"
            theCurrentModel.strSelectedUserAssignID = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.assignUserListService()
            
            break
        case .assignTo:
            // theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].id
            theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[index].user_id
            theCurrentView.lblAssignedToName.text = str

            if theCurrentModel.isForEdit {
                if let theModel = theCurrentModel.theEditMyRoleModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.createdBy){
                    theCurrentView.lblDepartmentName.text = "Select department"
                    theCurrentModel.strSelectedDepartmentID = ""
                    theCurrentModel.departmentListService()
                }
            } else {
                theCurrentView.lblDepartmentName.text = "Select department"
                theCurrentModel.strSelectedDepartmentID = ""
                theCurrentModel.departmentListService()
            }
            break
        case .department:
            theCurrentModel.strSelectedDepartmentID = theCurrentModel.arr_departmentList[index].id
            theCurrentView.lblDepartmentName.text = str
            break
            
        }
    }
    func validateForm() -> Bool {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter title")
            return false
        } else if theCurrentModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select company")
            return false
        } else if theCurrentModel.strSelectedUserAssignID.isEmpty {
            self.showAlertAtBottom(message: "Please select assigned to")
            return false
        } else if theCurrentModel.strSelectedDepartmentID.isEmpty {
            self.showAlertAtBottom(message: "Please select department")
            return false
        } else if (theCurrentView.lblReoccuring.text!.isEmpty) || ((theCurrentView.lblReoccuring.text ?? "").localizedLowercase == "none".localizedLowercase) {
            self.showAlertAtBottom(message: "Please select reoccuring")
            return false
        } else if (theCurrentView.lblTime.text!.isEmpty) {
            self.showAlertAtBottom(message: "Please select time")
            return false
        } else if (theCurrentView.txtDurationHours.text!.isEmpty) && (theCurrentView.txtDurationMint.text!.isEmpty) {
            self.showAlertAtBottom(message: "Please enter duration")
            return false
        } else {
            return true
        }
        
    }
    func update(text:String,dropDown:DropDown) {
        theCurrentModel.calendarReoccuringType = nil
        switch dropDown {
        case theCurrentModel.monthDD:
            theCurrentView.updateDroDownTextFor(month: text)
            break
        case theCurrentModel.yearDD:
            theCurrentView.updateDroDownTextFor(year: text)
            break
        case theCurrentModel.reoccuringDD:
            var type:CalendarReoccuring?
            
            switch text.localizedLowercase {
            case CalendarReoccuring.everyDay.rawValue.localizedLowercase:
                type = CalendarReoccuring.everyDay
                break
            case CalendarReoccuring.mondayToFriday.rawValue.localizedLowercase:
                type = CalendarReoccuring.mondayToFriday
                break
            case CalendarReoccuring.everyWeek.rawValue.localizedLowercase:
                type = CalendarReoccuring.everyWeek
                break
            case CalendarReoccuring.everyMonth.rawValue.localizedLowercase:
                type = CalendarReoccuring.everyMonth
                break
            case CalendarReoccuring.everyYear.rawValue.localizedLowercase:
                type = CalendarReoccuring.everyYear
                break
            case CalendarReoccuring.custom.rawValue.localizedLowercase:
                type = CalendarReoccuring.custom
                break
            default:
                break
            }
            
            theCurrentModel.calendarReoccuringType = type
            theCurrentView.updateDroDownTextFor(reoccuring: text)
            if let typeCustom = type, typeCustom == CalendarReoccuring.custom {
                if let date = theCurrentView.calendar.selectedDate {
                    redirectToCustomIOSCalendar(selectedDate: date)
                } else if let date = theCurrentView.calendar.today {
                    redirectToCustomIOSCalendar(selectedDate: date)
                }
            }
            break
        default:
            break
        }
        updatePageOfCalender()
    }
    
    func updatePageOfCalender() {
        let year = theCurrentView.lblYear.text!.trimmed()
        var month = theCurrentView.lblMonth.text!.trimmed()
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == month.localizedLowercase}) {
            month = "\(index + 1)"
            if month.count == 1 {
                month = "0" + month
            }
        }
        
        let createDate = year + "-" + month + "-" + "01"
        print(createDate)
        
        if let date = createDate.convertToUTCDateWithGMT0() {
            let localdate = date.toLocalTime()
            theCurrentView.setCurrentPageOfCalender(date: localdate)
        }
    }

    //MARK:- Redirection
    func presentDropDownListVC(categorySelection:categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            let arr_company:[String] = theCurrentModel.arr_companyList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Company",arr:arr_company)
            break
        case .assignTo:
            let arr_assignTo:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name}).filter({!$0.isEmpty})
            //          let arr_assignTo:[String] = theCurrentModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            let arr_department:[String] = theCurrentModel.arr_departmentList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Depatment",arr:arr_department)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToCustomIOSCalendar(selectedDate:Date) {
        let vc = CustomIOSCalenderVC.init(nibName: "CustomIOSCalenderVC", bundle: nil)
        vc.setTheData(selectedDate: selectedDate, customData: theCurrentModel.selectedDataOfCustom)
        vc.handlerSelectedDate = { [weak self] (selectedData) in
            self?.theCurrentModel.selectedDataOfCustom = selectedData
//           self?.addEvent(on: selectedDate, selectedData: selectedData)
            vc.dismiss(animated: true, completion: nil)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToDatePicker(selectedDate:Date) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
//        vc.isSetMinimumDate =  true
//        vc.minimumDate = selectedDate
        vc.isSetDate = true
        vc.setDateValue = selectedDate
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormate = DateFormatter()
                dateFormate.dateFormat = "hh:mm a"
                self?.theCurrentModel.selectedTime = date
                self?.theCurrentView.lblTime.text = dateFormate.string(from: date)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    /*
    func presentTimePickerVC(selectedDate:Date) {
        let vc = TimePickerVC.init(nibName: "TimePickerVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.selectedDate = selectedDate
        vc.handlerTime = { [weak self] (time) in
            self?.theCurrentView.lblTime.text = time
        }
        
        self.present(vc, animated: true, completion: nil)
    }*/
    
    //MARK:- IBAction
    @IBAction func onBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .company)
    }
    
    @IBAction func onBtnAssignedTo(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select first company")
            return
        }
        presentDropDownListVC(categorySelection: .assignTo)
    }
    
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentModel.strSelectedUserAssignID.isEmpty {
            self.showAlertAtBottom(message: "Please select first assigned to")
            return
        }
        presentDropDownListVC(categorySelection: .department)
    }
    
    @IBAction func onBtnMonthAction(_ sender: Any) {
        theCurrentModel.monthDD.show()
    }
    
    @IBAction func onBtnYearAction(_ sender: Any) {
        theCurrentModel.yearDD.show()
    }
    
    @IBAction func onBtnReoccuringAction(_ sender: Any) {
        theCurrentModel.reoccuringDD.show()
    }
    
    @IBAction func onBtnSaveAction(_ sender: UIButton) {
        if !validateForm() {
            return
        }
        
        /*
         required field : 'title','start_date','duration','reoccuring'
         
         
         duration : in minutes ,
         reoccuring : 0 = Custom,
         1 = Every Day,
         7 = Every Week,
         30 = Every Month,
         365 = Every Year
         
         custom_frequency :     1 = Daily,
         7 = Weekly,
         30 = Monthly,
         365 = Yearly
         
         custom_every : Every $ days OR $ weeks OR $ month OR $ years
         
         custom_selected :     Comma Separated : If days = 1 to 7 , If dates = 1 to 31, if months = 1 to 12
         
         each_or_on_the :     pass (On the , Each)
         Used only when custom_frequency is monthly(30)
         
         is_on_the : [0 = No, 1 = Yes] - Used only when custom_frequency is yearly(365)
         
         on_the : Used only when custom_frequency is monthly(30) or yearly(365)
         
         day : custom_frequency
         */
        
        var selectedDate = Date()
        if let date = theCurrentView.calendar.selectedDate {
            selectedDate = date
        } else if let date = theCurrentView.calendar.today {
            selectedDate = date
        }
        var selectedDateFormate = selectedDate.string(withFormat: DateFormatterInputType.inputType2) + " " + theCurrentView.lblTime.text!
        print("selectedDateFormate:=",selectedDateFormate)
        if let dateFor = selectedDateFormate.convert(fromformate: DateFormatterOutputType.outputType3, toFormate: DateFormatterInputType.inputType1) {
            selectedDateFormate = dateFor
        }
        print("selectedDateFormate:=",selectedDateFormate)
        
        let duration = ((Int(theCurrentView.txtDurationHours.text ?? "0") ?? 0) * 60) + (Int(theCurrentView.txtDurationMint.text ?? "0") ?? 0)

        var reoccuring = 1
        var custom_frequency = 1
        var custom_selected = ""
        var each_or_on_the = ""
        var is_on_the = ""
        var on_the = ""
        var day = ""
        
        if let typeCustom = theCurrentModel.calendarReoccuringType {
            switch typeCustom {
                
            case .everyDay:
                reoccuring = 1
                break
            case .mondayToFriday:
                reoccuring = 5
                break
            case .everyWeek:
                reoccuring = 7
                break
            case .everyMonth:
                reoccuring = 30
                break
            case .everyYear:
                reoccuring = 365
                break
            case .custom:
                reoccuring = 0
                
                switch theCurrentModel.selectedDataOfCustom.frequencyType {
                    
                case .daily:
                    custom_frequency = 1
                    break
                case .weekly:
                    custom_frequency = 7
                    custom_selected = theCurrentModel.selectedDataOfCustom.weeklySelectedDays.map({"\($0)"}).joined(separator: ",")
                    break
                case .monthly:
                    custom_frequency = 30
                    custom_selected = theCurrentModel.selectedDataOfCustom.monthlySelectedDays.map({"\($0)"}).joined(separator: ",")
                    each_or_on_the = theCurrentModel.selectedDataOfCustom.monthlyIsSelectedEach ? "Each" : "On the"
                    
                    on_the = theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType.rawValue
                    day = theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType.rawValue
                    if theCurrentModel.selectedDataOfCustom.monthlyIsSelectedEach {
                        on_the = ""
                        day = ""
                    } else {
                        custom_selected = ""
                    }
                    break
                case .yearly:
                    custom_frequency = 365
                    custom_selected = theCurrentModel.selectedDataOfCustom.yearlySelectedMonths.map({"\($0)"}).joined(separator: ",")
                    is_on_the = theCurrentModel.selectedDataOfCustom.yearlyIsSelectedOnThe ? "1" : "0"
                    on_the = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType.rawValue
                    day = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType.rawValue
                    
                    break
                }
                
                print("custom_frequency:=",custom_frequency)
                print("custom_selected:=",custom_selected)
                print("each_or_on_the:=",each_or_on_the)
                print("is_on_the:=",is_on_the)
                print("on_the:=",on_the)
                print("day:=",day)
                
                break
            
            }
        }
        
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_title:theCurrentView.txtTitle.text!.trimmed(),
                         APIKey.key_start_date:selectedDateFormate,
                         APIKey.key_duration:duration,
                         APIKey.key_reoccuring:reoccuring,
                         APIKey.key_custom_frequency:custom_frequency,
                         APIKey.key_custom_every:theCurrentModel.selectedDataOfCustom.everyDay,
                         APIKey.key_custom_selected:custom_selected,
                         APIKey.key_each_or_on_the:each_or_on_the,
                         APIKey.key_is_on_the:is_on_the,
                         APIKey.key_on_the:on_the,
                         APIKey.key_day:day,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID,
                         APIKey.key_assigned_to:theCurrentModel.strSelectedUserAssignID,
                         APIKey.key_department_id:theCurrentModel.strSelectedDepartmentID] as [String : Any]
        
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        if theCurrentModel.isForEdit {
            parameter[APIKey.key_role_id] = theCurrentModel.theEditMyRoleModel?.id ?? ""
            theCurrentModel.editMyRole(parameter: parameter) { [weak self] in
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
            }
        } else {
            theCurrentModel.addMyRole(parameter: parameter) { [weak self] in
                sender.stopLoadyIndicator()
                self?.view.allButtons(isEnable: true)
            }
        }
        
        
    }
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
//        handlerClose()
        self.backAction(isAnimation: false)
    }
    
   
    
    @IBAction func onBtnTimeAction(_ sender: Any) {
        redirectToDatePicker(selectedDate: theCurrentModel.selectedTime)
    }
    
}

//MARK:- FSCalendarDelegate
extension MyRoleCalendarVC:FSCalendarDelegate {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let page = calendar.currentPage.monthName()
        print("Month:=\(page),  Year:=\(calendar.currentPage.year)")
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == calendar.currentPage.monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        if let index = theCurrentModel.arr_year.firstIndex(where: {$0.localizedLowercase == "\(calendar.currentPage.year)".localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[index])
        }
    }
}
//MARK:- FSCalendarDelegateAppearance
extension MyRoleCalendarVC:FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        print("bounds:=\(bounds)")
    }
}

//MARK:- FSCalendarDataSource
extension MyRoleCalendarVC:FSCalendarDataSource {
    func minimumDate(for calendar: FSCalendar) -> Date {
        return theCurrentModel.selectedDate
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        let year = Int(theCurrentModel.arr_year.last ?? "\(Date().addYears(20))")!
        let lastDate = ("\(year)-12-31").convertToUTCDateWithGMT0()!
         print("Maximum:=\(lastDate)")
        return lastDate
    }
    /*
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = theCurrentModel.dateFormatter.string(from: date)
        
        if theCurrentModel.arr_datesWithEvent.contains(dateString) {
            return 1
        }
        
        if theCurrentModel.arr_datesWithMultipleEvents.contains(dateString) {
            return 3
        }
        
        return 0
    }*/
}
