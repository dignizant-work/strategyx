//
//  MyRoleCalenderView.swift
//  StrategyX
//
//  Created by Haresh on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import FSCalendar
import RxCocoa
import RxSwift

class MyRoleCalendarView: ViewParentWithoutXIB {


    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!

    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var viewMonth: UIView!
    @IBOutlet weak var lblMonth: UILabel!
    
    @IBOutlet weak var viewYear: UIView!
    @IBOutlet weak var lblYear: UILabel!
    
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var viewReoccuring: UIView!
    @IBOutlet weak var lblReoccuring: UILabel!
    
    @IBOutlet weak var txtDurationHours: UITextField!
    @IBOutlet weak var txtDurationMint: UITextField!
    
    @IBOutlet weak var lblReoccuringTitle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var btnCompanyActivity: UIButton!
    
    @IBOutlet weak var viewAssignedTo: UIView!
    @IBOutlet weak var lblAssignedToName: UILabel!
    @IBOutlet weak var btnAssignedToActivity: UIButton!
    
    @IBOutlet weak var viewDepartment: UIView!
    @IBOutlet weak var lblDepartmentName: UILabel!
    @IBOutlet weak var btnDepartmentActivity: UIButton!
    
    let disposeBag = DisposeBag()
    
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        setupCalender()
        lblTime.text = Date().string(withFormat: "hh:mm a")
        
        //Rx Action
        onTextHoursDidChange()
        onTextMinutesDidChange()
        onTextHours()
        onTextMinute()
    }
    
    func setupCalender() {
        calendar.headerHeight = 0.0
        calendar.weekdayHeight = 40.0
        
        calendar.appearance.weekdayTextColor = clear
        calendar.appearance.titleDefaultColor = appBlack
        calendar.appearance.titlePlaceholderColor = appPlaceHolder
        calendar.appearance.selectionColor = appGreen
        calendar.appearance.titleSelectionColor = white
        calendar.appearance.titleFont = R14
//        calendar.appearance.todayColor = appGreen
        
        calendar.placeholderType = .none
        calendar.today = nil
        calendar.select(Date())
        calendar.firstWeekday = 2
    }
    
    func setCalenderDelegate(theDelegate:MyRoleCalendarVC) {
        calendar.delegate = theDelegate
        calendar.dataSource = theDelegate
    }
    
    func setCurrentPageOfCalender(date:Date) {
//        unit = (calendarView.scope == FSCalendarScope.month) ? FSCalendarUnit.month : FSCalendarUnit.weekOfYear
//        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarView.currentPage)
//        calendarView.setCurrentPage(previousMonth!, animated: true)
        
        calendar.setCurrentPage(date, animated: true)
    }
    
    func updateDroDownTextFor(month:String) {
        lblMonth.text = month
    }
    
    func updateDroDownTextFor(year:String) {
        lblYear.text = year
    }
    
    func updateDroDownTextFor(reoccuring:String) {
        lblReoccuring.text = reoccuring
    }
    
    //MARK:- RxAction
    func onTextHoursDidChange() {
        txtDurationHours.rx.controlEvent(UIControl.Event.editingChanged)
            .subscribe(onNext: { [weak self] (txt) in
                if let obj = self, obj.txtDurationHours.text!.count >= 2 {
//                    obj.txtDurationHours.resignFirstResponder()
                    obj.txtDurationMint.becomeFirstResponder()
                    
                }
            }).disposed(by: disposeBag)
    }
    
    func onTextMinutesDidChange() {
        txtDurationMint.rx.controlEvent(UIControl.Event.editingChanged)
            .subscribe(onNext: { [weak self] (txt) in
                if let obj = self, obj.txtDurationMint.text!.count <= 0 {
//                    obj.txtDurationMint.resignFirstResponder()
                    obj.txtDurationHours.becomeFirstResponder()
                }
            }).disposed(by: disposeBag)
    }
    
    
    func onTextHours() {
        txtDurationHours.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                print("previous:=\(previous ?? "empty"), new:=\(new)")
                let num = Int(new) ?? -1
                if self.txtDurationHours.text!.count > 2 {
                    self.txtDurationMint.text = String(new.last!)
                    return previous ?? new
                }
                if num != -1 {
                    if num > 999 {
                        return previous ?? new
                    }
                    return new
                } else {
                    return new
                }
            }
            .subscribe(txtDurationHours.rx.text)
            .disposed(by: disposeBag)
    }
    
    func onTextMinute() {
        txtDurationMint.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                print("previous:=\(previous ?? "empty"), new:=\(new)")
                let num = Int(new) ?? -1
                if self.txtDurationMint.text!.count > 2 {
                    return previous ?? new
                }
                if num != -1 {
                    if num > 59 {
                        return previous ?? new
                    } else if num >= 6 && num < 10{
                        return "0\(num)"
                    }
                    return new
                } else {
                    return new
                }
            }
            .subscribe(txtDurationMint.rx.text)
            .disposed(by:disposeBag)
    }

}
