//
//  AddUserModel.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import WSTagsField
import DropDown

class AddUserModel {

    //MARK:- Variable
    fileprivate weak var theController:AddUserVC!
    
    var imageBG:UIImage?
    
   // var arr_TimeZone:[String] = ["TimeZone 1","TimeZone 2","TimeZone 3","TimeZone 4"]
  //  var arr_Type:[String] = ["Type 1","Type 2","Type 3","Type 4"]
    
    var img_Profile:UIImage?
    var img_Type:String?
    var img_URL:URL?

    var tagsField = WSTagsField()
    var arr_Tags : [String] = []
    var arr_Tags_Main : [Department] = []
    var isDropDownShowing = false
    var isFirstTimeFocus = true

    var managerTagsField = WSTagsField()
    var arr_ManagerTags : [String] = []
    var arr_ManagerTags_Main : [Managers] = []

    
    var selectedData:JSON = JSON(["firstname":"","lastname":"","email":"","mobile":"","timezone":"","type":""])
    
    let typeDD = DropDown()
    let timezoneDD = DropDown()
    let tagsDD = DropDown()
    let companyDD = DropDown()
    let managerDD = DropDown()

    // Edit profile
    var selectedUserType = editUserType.addUser
    var isComeFromEditProfile = Bool()
    var theUserDetailModel:UserList?
    var theUserModel:User?
    
       
    // Timezone    

    var arr_TimezoneList:[Timezone] = []
    
    // getCompany
    var arr_companyList:[Company] = []
    
    // Department
//    var arr_departmentList:[Department] = []
    
    // Assign
    var arr_UserTypeList:[UserType] = []
    
    // Selected File Data
    
    var arr_selectedFile:[SelectedFile] = []
    
    
    var selectedDataId:JSON = JSON(["companyId":"","departmentId":"","typeId":"","timezoneId":"","managerId":""])
    
    //MARK:- LifeCycle
    init(theController:AddUserVC) {
        self.theController = theController
    }
    func configureTagsView()
    {

        tagsField.spaceBetweenLines = 5
        tagsField.spaceBetweenTags = 10
        
        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 6)
        tagsField.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10) //old padding
        
        tagsField.placeholder = "Select department"
        tagsField.placeholderColor = appPlaceHolder
        tagsField.placeholderAlwaysVisible = true
        tagsField.backgroundColor = UIColor.white
        tagsField.returnKeyType = .default
        tagsField.delimiter = ""
        tagsField.font = R14
        tagsField.tintColor = appBgLightGrayColor
        
        tagsField.textDelegate = theController
        tagsField.acceptTagOption = .space
        theController.textFieldEvents(tagsField : tagsField)
        
        
//        let tap = UITapGestureRecognizer(target: theController, action: #selector(theController.checkFirstTime(_:)))
//        tagsField.isUserInteractionEnabled = true
//        tagsField.addGestureRecognizer(tap)
        
    }
    func configureManagerTagsView()
    {
        
        managerTagsField.spaceBetweenLines = 5
        managerTagsField.spaceBetweenTags = 10
        
        managerTagsField.layoutMargins = UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 6)
        managerTagsField.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10) //old padding
        
        managerTagsField.placeholder = "Select Manager"
        managerTagsField.placeholderColor = appPlaceHolder
        managerTagsField.placeholderAlwaysVisible = true
        managerTagsField.backgroundColor = UIColor.white
        managerTagsField.returnKeyType = .default
        managerTagsField.delimiter = ""
        managerTagsField.font = R14
        managerTagsField.tintColor = appBgLightGrayColor
        
        managerTagsField.textDelegate = theController
        managerTagsField.acceptTagOption = .space
        theController.textFieldEvents(tagsField : managerTagsField)
        
        
        //        let tap = UITapGestureRecognizer(target: theController, action: #selector(theController.checkFirstTime(_:)))
        //        tagsField.isUserInteractionEnabled = true
        //        tagsField.addGestureRecognizer(tap)
        
    }
    func FilterTagsArray(text : String, isForManager:Bool = false) -> [String]
    {
        if isForManager {
            arr_ManagerTags = []
            arr_ManagerTags_Main.forEach { (tagList) in
                if(tagList.fullName.lowercased().contains(text.lowercased()))
                {
                    arr_ManagerTags.append(tagList.fullName)
                }
            }
            self.managerDD.dataSource = arr_ManagerTags
            return arr_ManagerTags
        }
        arr_Tags = []
        arr_Tags_Main.forEach { (tagList) in
            if(tagList.name.lowercased().contains(text.lowercased()))
            {
                arr_Tags.append(tagList.name)
            }
        }
        self.tagsDD.dataSource = arr_Tags
        return arr_Tags
        
    }
    
    func getAllTag(){
        arr_Tags = []
        arr_Tags = arr_Tags_Main.map { $0.name}
        self.tagsDD.dataSource = arr_Tags
    }
    
    func getAllManagerTag(){
        arr_ManagerTags = []
        arr_ManagerTags = arr_ManagerTags_Main.map { $0.fullName}
        self.managerDD.dataSource = arr_ManagerTags
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: view.frame.origin.y + view.frame.height)
        
        switch dropDown {
        case tagsDD:
            dropDown.dataSource = arr_Tags
            break
        case managerDD:
            dropDown.dataSource = arr_ManagerTags
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
        
        dropDown.cancelAction = { [unowned self]  in
            if dropDown == self.tagsDD {
                self.theController.ResetTagsTextfield()
            } else if dropDown == self.managerDD {
                self.theController.ResetManagerTagsTextfield()
            }
        }
    }
    
    
    
}
