//
//  AddUserView.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import WSTagsField
import DropDown


class AddUserView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!

    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDashBorder: UILabel!

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    
    @IBOutlet weak var btnTimeZoneLoaderOutlet: UIButton!
    @IBOutlet weak var lblTimeZone: UILabel!
    @IBOutlet weak var lblSelectedTimeZone: UILabel!
    @IBOutlet weak var viewDropDownTimeZone: UIView!
    
    @IBOutlet weak var btnTypeLoaderOutlet: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblSelectedType: UILabel!
    @IBOutlet weak var viewDropDownType: UIView!
    
    @IBOutlet weak var btnCompanyLoaderOutlet: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblSelectedCompany: UILabel!
    @IBOutlet weak var viewDropDownCompany: UIView!
    @IBOutlet weak var viewCompany: UIView!

    @IBOutlet weak var lblManager: UILabel!
    @IBOutlet weak var viewManager: UIView!
    @IBOutlet weak var viewOfManeger: UIView!
    @IBOutlet weak var viewManagerTags: UIView!
    @IBOutlet weak var btnManagerTagLoaderOutlet: UIButton!
    @IBOutlet weak var constrainManagerViewTagsHeight: NSLayoutConstraint!

    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var viewDepartment: UIView!
    @IBOutlet weak var viewDepartmentTags: UIView!
    @IBOutlet weak var btnTagLoaderOutlet: UIButton!
    @IBOutlet weak var constrainviewDepartmentTagsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var viewOfType: UIView!
    @IBOutlet weak var viewOfDepartment: UIView!
    
//    var tap = UITapGestureRecognizer()
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
    }
    func configure(img:UIImage?) {
        
        lblProfile.text = "Profile Pic"
        imgProfile.image = img
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
            self.lblDashBorder.addDashedBorder(color: appGreen, cornerRadius:30)
        })
    }
    func configure(strTitle:String,strPlaceHolder:String,type:UIKeyboardType = .default,lblTitle:UILabel,txtInputField:UITextField,theController:AddUserVC, isMendotory:Bool)
    {
        lblTitle.text = strTitle
        lblTitle.showAstrike = isMendotory
        txtInputField.placeholder = strPlaceHolder
        txtInputField.keyboardType = type
        txtInputField.delegate = theController
    }
    func configureDropDown(strTitle:String,lblTitle:UILabel,lblText:UILabel,strText:String)
    {
        lblTitle.text = strTitle
        lblText.text = strText
        lblTitle.showAstrike = true

    }
    func configureUISetUp(theController : AddUserVC)
    {
        lblHeading.text = "Add User"
        
        configure(strTitle: "First Name", strPlaceHolder: "Enter first name", type: .default, lblTitle: lblFirstName, txtInputField: txtFirstName,theController:theController,isMendotory:true)
        configure(strTitle: "Last Name", strPlaceHolder: "Enter last name", type: .default, lblTitle: lblLastName, txtInputField: txtLastName,theController:theController,isMendotory:false)
        configure(strTitle: "Email", strPlaceHolder: "Enter email", type: .emailAddress, lblTitle: lblEmail, txtInputField: txtEmail,theController:theController,isMendotory:true)
        configure(strTitle: "Mobile", strPlaceHolder: "Enter mobile", type: .asciiCapableNumberPad, lblTitle: lblMobile, txtInputField: txtMobile,theController:theController,isMendotory:false)
        configureDropDown(strTitle: "Timezone", lblTitle: lblTimeZone, lblText: lblSelectedTimeZone, strText: "Select timezone")
        configureDropDown(strTitle: "Type", lblTitle: lblType, lblText: lblSelectedType, strText: "Select type")
        configureDropDown(strTitle: "Company", lblTitle: lblCompany, lblText: lblSelectedCompany, strText: "Select company")

        lblDepartment.text = "Department"
        lblManager.text = "Manager"

        lblDepartment.showAstrike = true
//        tap = UITapGestureRecognizer(target: theController, action: #selector(theController.checkFirstTime(_:)))
        
    }
    
    func addTagSubView(tagsField : WSTagsField)
    {
        tagsField.frame = viewDepartmentTags.bounds
        viewDepartmentTags.addSubview(tagsField)
        
//        viewDepartmentTags.addGestureRecognizer(tap)
//        tagsField.addGestureRecognizer(tap)
    }
    
    func addManagerTagSubView(tagsField : WSTagsField) {
            tagsField.frame = viewManagerTags.bounds
            viewManagerTags.addSubview(tagsField)
    }
    
}
