//
//  AddUserVC.swift
//  StrategyX
//
//  Created by Haresh on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SDWebImage
import WSTagsField
import DropDown

class AddUserVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case type              = 1
        case timezone          = 2
        case company           = 3
    }
    
    
    fileprivate lazy var theCurrentView:AddUserView = { [unowned self] in
       return self.view as! AddUserView
    }()
    fileprivate lazy var theCurrentModel:AddUserModel = {
        return AddUserModel(theController: self)
    }()
    
    var handlerEditUserData:(UserList) -> Void = {data in}
    var handlerEditUserProfileData:() -> Void = {}
    var handlerAddUserData:() -> Void = {}

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
//        ["firstname":"","lastname":"","email":"","mobile":"","timezone":"","type":""]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        enableIQKeyboardManager()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.theCurrentModel.isFirstTimeFocus = false
    }
    
    func setTheData(imgBG:UIImage?) {
       // theCurrentModel.imageBG = imgBG
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.configure(img: theCurrentModel.img_Profile)
        theCurrentView.configureUISetUp(theController : self)
        theCurrentView.addTagSubView(tagsField: theCurrentModel.tagsField)
        theCurrentView.addManagerTagSubView(tagsField: theCurrentModel.managerTagsField)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.tagsDD, view: theCurrentView.viewDepartmentTags)
        theCurrentModel.configureTagsView()
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.managerDD, view: theCurrentView.viewManagerTags)
        theCurrentModel.configureManagerTagsView()
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.typeDD, view: theCurrentView.lblSelectedType)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.companyDD, view: theCurrentView.lblSelectedCompany)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.timezoneDD, view: theCurrentView.lblSelectedTimeZone)
        addDoneButtonOnKeyboard(textfield: theCurrentView.txtMobile)
        showLoader(theCurrentView.btnTimeZoneLoaderOutlet)
        showLoader(theCurrentView.btnTypeLoaderOutlet)
        
        disableIQKeyboardManager()
        
        getTimezoneListService()
        self.theCurrentView.viewCompany.isHidden = true
        self.theCurrentView.viewManager.isHidden = true
        self.theCurrentView.viewDepartment.isHidden = true
        
        if theCurrentModel.selectedUserType == .editUser{
            setupUserEditDetail()
            isDisableCompanySelection()
            userTypeListService()
        } else if theCurrentModel.selectedUserType == .editProfile{
            setupUserEditProfile()
            self.stopLoader(self.theCurrentView.btnTypeLoaderOutlet)
        } else {
            isDisableCompanySelection()
            userTypeListService()
        }
    }
    
    func isDisableCompanySelection() {
        if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
//            self.theCurrentView.viewCompany.isUserInteractionEnabled = false
//            self.theCurrentView.viewCompany.alpha = 0.5
            theCurrentView.viewCompany.isHidden = true
            self.theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
            self.theCurrentModel.selectedDataId["companyId"].stringValue = Utility.shared.userData.companyId
            self.theCurrentView.lblSelectedCompany.text = Utility.shared.userData.companyName
            showLoader(theCurrentView.btnManagerTagLoaderOutlet)
            showLoader(theCurrentView.btnTagLoaderOutlet)
            managerListService()
            departmentListService()
        }else{
            companylistService()
        }
    }
    
    func setupEditScreenData(type:editUserType,theModel:UserList){
        theCurrentModel.selectedUserType = type
        theCurrentModel.theUserDetailModel = theModel
    }
    
    func setupEditProfileData(type:editUserType,theModel:User){
        theCurrentModel.selectedUserType = type
        theCurrentModel.theUserModel = theModel
    }
    
    func setupUserEditProfile(){
        theCurrentView.lblHeading.text = "Edit Profile"
        theCurrentView.btnAdd.setTitle("Edit", for: .normal)
        guard let theModel = theCurrentModel.theUserModel else { return }
        theCurrentView.imgProfile.sd_setImageLoadMultiTypeURL(url: theModel.image, placeholder: "ic_big_plash_holder")
        theCurrentView.txtFirstName.text = theModel.firstname
        theCurrentView.txtLastName.text = theModel.lastname
        theCurrentView.txtEmail.text = theModel.email
        theCurrentView.txtMobile.text = theModel.mobile
        
        self.theCurrentModel.selectedDataId["timezoneId"].stringValue = theModel.timezoneId
        
        self.theCurrentModel.selectedData["company"].stringValue = theModel.companyName
        self.theCurrentModel.selectedDataId["companyId"].stringValue = theModel.companyId
        self.theCurrentView.lblSelectedCompany.text = theModel.companyName
        
        self.theCurrentModel.selectedData["department"].stringValue = theModel.departmentName
        self.theCurrentModel.selectedDataId["departmentId"].stringValue = theModel.departmentId
        
        let managers:[String] = theModel.managers.map({$0.fullName})
        theCurrentModel.managerTagsField.addTags(managers)

        theCurrentModel.tagsField.addTags(theModel.departmentName.components(separatedBy: ","))
        
        self.theCurrentModel.selectedData["type"].stringValue = theModel.userType
        self.theCurrentModel.selectedDataId["typeId"].stringValue = theModel.roleId
        theCurrentView.lblSelectedType.text = theModel.userType
        
        if Int(theModel.roleId) == UserRole.companyAdminUser.rawValue || Int(theModel.roleId) == UserRole.staffUser.rawValue{
            [theCurrentView.viewCompany,theCurrentView.viewDepartment].forEach { (view) in
                view?.isHidden = false
            }
        }
        theCurrentView.viewCompany.isHidden = true
        if theCurrentModel.selectedUserType == .editProfile && Int(theModel.roleId) != UserRole.superAdminUser.rawValue {
            theCurrentView.viewCompany.isHidden = false
        }
        [theCurrentView.viewCompany,theCurrentView.viewDepartment,theCurrentView.viewManager,theCurrentView.viewOfType,theCurrentView.viewOfDepartment].forEach { (view) in
            view?.isUserInteractionEnabled = false            
        }
    }
    
    func setupUserEditDetail() {
        guard let theModel = theCurrentModel.theUserDetailModel else { return }
        theCurrentView.btnAdd.setTitle("Edit", for: .normal)
        theCurrentView.lblHeading.text = "Edit User"
        theCurrentView.imgProfile.sd_setImageLoadMultiTypeURL(url: theModel.userImage, placeholder: "ic_big_plash_holder")
        theCurrentView.txtFirstName.text = theModel.firstname
        theCurrentView.txtLastName.text = theModel.lastname
        theCurrentView.txtEmail.text = theModel.userEmail
        theCurrentView.txtMobile.text = theModel.mobileNumber
        
        self.theCurrentModel.selectedDataId["timezoneId"].stringValue = theModel.timezoneId
        
        self.theCurrentModel.selectedData["company"].stringValue = theModel.companyName
        self.theCurrentModel.selectedDataId["companyId"].stringValue = theModel.companyId
        self.theCurrentView.lblSelectedCompany.text = theModel.companyName
        
        self.theCurrentModel.selectedData["department"].stringValue = theModel.departmentName
        self.theCurrentModel.selectedDataId["departmentId"].stringValue = theModel.departmentId

        self.theCurrentModel.selectedData["manager"].stringValue = theModel.departmentName
        self.theCurrentModel.selectedDataId["managerId"].stringValue = theModel.managerId
        
        let managers:[String] = theModel.managers.map({$0.fullName})
        theCurrentModel.managerTagsField.addTags(managers)
        
        theCurrentModel.tagsField.addTags(theModel.departmentName.components(separatedBy: ","))
        
        self.showLoader(theCurrentView.btnTagLoaderOutlet)
        departmentListService()
        
        self.theCurrentModel.selectedData["type"].stringValue = theModel.userType
        self.theCurrentModel.selectedDataId["typeId"].stringValue = theModel.roleId
        theCurrentView.lblSelectedType.text = theModel.userType
        
        theCurrentView.viewCompany.isUserInteractionEnabled = false
        if Int(theModel.roleId) != UserRole.superAdminUser.rawValue {
            [theCurrentView.viewCompany,theCurrentView.viewDepartment,theCurrentView.viewManager].forEach { (view) in
                view?.isHidden = false
            }
        }
        
        
        theCurrentView.viewCompany.isHidden = true
        /*[theCurrentView.viewCompany,theCurrentView.viewDepartment,theCurrentView.viewOfType,theCurrentView.viewOfDepartment].forEach { (view) in
            view?.isUserInteractionEnabled = false
            view?.alpha = 0.5
        }
        
        */
    }
    
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .timezone:
            self.theCurrentModel.selectedData["timezone"].stringValue = str
            self.theCurrentModel.selectedDataId["timezoneId"].stringValue = theCurrentModel.arr_TimezoneList[index].timezoneId
            self.theCurrentView.lblSelectedTimeZone.text = str
            break
        case .type:
            self.theCurrentModel.selectedData["type"].stringValue = str
            self.theCurrentModel.selectedDataId["typeId"].stringValue = theCurrentModel.arr_UserTypeList[index].id
            self.theCurrentView.lblSelectedType.text = str
            if Int(theCurrentModel.arr_UserTypeList[index].id) == UserRole.superAdminUser.rawValue {
                self.theCurrentModel.selectedData["company"].stringValue = ""
                self.theCurrentModel.selectedDataId["companyId"].stringValue = ""
                self.theCurrentModel.selectedData["department"].stringValue = ""
                self.theCurrentModel.selectedDataId["departmentId"].stringValue = ""
                self.theCurrentModel.selectedDataId["managerId"].stringValue = ""
                self.theCurrentModel.tagsField.text = ""
                self.theCurrentModel.tagsField.removeTags()
                self.theCurrentModel.managerTagsField.text = ""
                self.theCurrentModel.managerTagsField.removeTags()
                [theCurrentView.viewDepartment,theCurrentView.viewCompany,theCurrentView.viewManager].forEach { (view) in
                    view?.isHidden = true
                }
            }else{
                if theCurrentModel.selectedUserType == .addUser{
                    self.theCurrentView.lblSelectedCompany.text = "Select company"
//                    self.theCurrentModel.tagsField.text = ""
//                    self.theCurrentModel.tagsField.removeTags()
//                    self.theCurrentModel.managerTagsField.text = ""
//                    self.theCurrentModel.managerTagsField.removeTags()

                    [theCurrentView.viewDepartment,theCurrentView.viewCompany, theCurrentView.viewManager].forEach { (view) in
                        view?.isHidden = false
                    }
                    
                    if Int(theCurrentModel.arr_UserTypeList[index].id) == UserRole.superAdminUser.rawValue || Int(theCurrentModel.arr_UserTypeList[index].id) == UserRole.companyAdminUser.rawValue {
                        theCurrentView.viewManager.isHidden = true
                    }
                    isDisableCompanySelection()
                }
                
                
            }
            break
        case .company:
            self.theCurrentModel.selectedData["company"].stringValue = str
            self.theCurrentModel.selectedDataId["companyId"].stringValue = theCurrentModel.arr_companyList[index].id
            self.theCurrentView.lblSelectedCompany.text = str
            self.theCurrentModel.tagsField.text = ""
            self.theCurrentModel.tagsField.removeTags()
            self.theCurrentModel.managerTagsField.text = ""
            self.theCurrentModel.managerTagsField.removeTags()
            self.showLoader(theCurrentView.btnTagLoaderOutlet)
            self.showLoader(theCurrentView.btnManagerTagLoaderOutlet)
            managerListService()
            departmentListService()
            break
        }
    }
    
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            theCurrentView.btnAdd.startLoadyIndicator()
            self.view.allButtons(isEnable: false)
        } else {
            theCurrentView.btnAdd.stopLoadyIndicator()
            self.view.allButtons(isEnable: true)
        }
    }
    
    func updateProfileImage(img:UIImage?,filename:String,imgData:Data?) {
        if imgData == nil {
            showAlertAtBottom(message: "File could not find")
            return
        }
        if let data = img?.pngData(), (data.count/1024) >= 2048 {
            showAlertAtBottom(message: "Please select image which is less than 2 MB")
            return
        }
        if let data = imgData, (data.count/1024) >= 2048 {
            showAlertAtBottom(message: "Please select image which is less than 2 MB")
            return
        }
        
        theCurrentModel.img_Profile = img
        theCurrentView.imgProfile.image = img
       // theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 3, section: 0)], with: UITableView.RowAnimation.none)
        print("imgData:=",imgData!.count)
        let objSelectedData = SelectedFile.init(selectedFileName: filename, selectedFileData: imgData!)
        theCurrentModel.arr_selectedFile.append(objSelectedData)
    }
    @objc func showAlert()
    {
        self.showAlertAtBottom(message: "Please select image which is less than 2 MB ")
    }
    
    func openImageGallery() {
        let imagePicker = ImagePickerManager()
        imagePicker.picker.allowsEditing = true
        imagePicker.pickImage(self) { [weak self] (image,filename,imageData) in
            self?.updateProfileImage(img: image,filename:filename,imgData:imageData)
        }
    }
    
    
    func validateForm() {
        self.view.endEditing(true)
        //        "companyname":"","companyabbreviation":"","companywebsite":"","companyprofile":"","companyvision":"","companymission":"","companyvalues":"","companyculture":"","unitnumber":"","streetname":"","streettype":"","state":"","suburb":"","postcode":"","adminemail":"","admintimezone":"","adminfirstname":"","adminlastname":"","mobile":"","departmentname":""
        
        let tags = theCurrentModel.tagsField.tags.map({$0.text})
        var searchTag:[String] = []
        
        for tag in tags {
            if let index = theCurrentModel.arr_Tags_Main.firstIndex(where: {$0.name == tag}) {
                searchTag.append(theCurrentModel.arr_Tags_Main[index].id)
            }
        }
        
        var managerTag:[String] = []
        let managersTagsField = theCurrentModel.managerTagsField.tags.map({$0.text})

        for tag in managersTagsField {
            if let index = theCurrentModel.arr_ManagerTags_Main.firstIndex(where: {$0.fullName == tag}) {
                managerTag.append(theCurrentModel.arr_ManagerTags_Main[index].id)
            }
        }
        
        if theCurrentView.txtFirstName.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter first name")
            return
        } else if theCurrentView.txtEmail.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter email address")
            return
        } else if !theCurrentView.txtEmail.text!.trimmed().isValidEmail() {
            self.showAlertAtBottom(message: "Please enter valid email address")
            return
        } else if theCurrentModel.selectedData["timezone"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select timezone")
            return
        } else if theCurrentModel.selectedData["type"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select type")
            return
        }
        if self.theCurrentModel.selectedDataId["typeId"].intValue == UserRole.superAdminUser.rawValue {
            theCurrentModel.selectedDataId["companyId"].stringValue = ""
        } else {
            if theCurrentModel.selectedUserType != .editProfile{
                if theCurrentModel.selectedDataId["companyId"].stringValue == ""{
                    self.showAlertAtBottom(message: "Please select company")
                    return
                }else if searchTag.count <= 0{
                    self.showAlertAtBottom(message: "Please select department")
                    return
                }
            }
        }
        
        if !theCurrentView.txtMobile.text!.isEmpty{
            if theCurrentView.txtMobile.text!.isNumeric == false{
                self.showAlertAtBottom(message: "Please enter valid mobile number")
                return
            }
        }
    
        var parameter = [
                         APIKey.key_company_id:theCurrentModel.selectedDataId["companyId"].stringValue,
                         APIKey.key_timezone_id:theCurrentModel.selectedDataId["timezoneId"].stringValue,
                         APIKey.key_firstname:(theCurrentView.txtFirstName.text ?? "").trimmed(),
                         APIKey.key_lastname:(theCurrentView.txtLastName.text ?? "").trimmed(),
                         APIKey.key_mobile:(theCurrentView.txtMobile.text ?? "").trimmed(),
                         APIKey.key_email:(theCurrentView.txtEmail.text ?? "").trimmed(),
                         APIKey.key_user_type:theCurrentModel.selectedDataId["typeId"].stringValue]
        
        if theCurrentModel.selectedUserType != .editProfile{
            if searchTag.count > 0{
                parameter[APIKey.key_department_id] = searchTag.joined(separator: ",")
            }
            parameter[APIKey.key_manager_id] = managerTag.joined(separator: ",")
        } else {
            parameter[APIKey.key_department_id] = self.theCurrentModel.selectedDataId["departmentId"].stringValue
            parameter[APIKey.key_manager_id] = self.theCurrentModel.selectedDataId["managerId"].stringValue
        }
       
        self.view.allButtons(isEnable: false)
        isClickOnAdd(isLoading: true)
        if theCurrentModel.selectedUserType == .addUser{
            parameter[APIKey.key_access_token] = Utility.shared.userData.accessToken
            parameter[APIKey.key_user_id] = Utility.shared.userData.id
            addUserWithAttachment(parameter: parameter)
        } else if theCurrentModel.selectedUserType == .editUser{
            parameter[APIKey.key_access_token] = Utility.shared.userData.accessToken
            parameter[APIKey.key_user_id] = Utility.shared.userData.id
            parameter[APIKey.key_edit_user_id] = theCurrentModel.theUserDetailModel?.userId
            parameter[APIKey.key_lang] = lang
            editUserWithAttachment(parameter: parameter)
        } else if theCurrentModel.selectedUserType == .editProfile{
            parameter[APIKey.key_lang] = lang
            parameter[APIKey.key_access_token] = Utility.shared.userData.accessToken
            parameter[APIKey.key_user_id] = Utility.shared.userData.id
            editUserProfileWithAttachment(parameter: parameter)
        }
    }
    
    //MARK:- Redirect
    func presentDropDownListVC(categorySelection:categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .timezone:
             let arr_list:[String] = theCurrentModel.arr_TimezoneList.map({"\($0.timezoneName) (\($0.timezoneCode))"})
            vc.setTheData(strTitle: "Timezone",arr:arr_list)
            break
        case .type:
            let arr_list:[String] = theCurrentModel.arr_UserTypeList.map { $0.name}
            vc.setTheData(strTitle: "Type",arr:arr_list)
            break
        case .company:
            let arr_list:[String] = theCurrentModel.arr_companyList.map { $0.name}
            vc.setTheData(strTitle: "Company",arr:arr_list)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
}

//MARK:- Other methods
extension AddUserVC
{
    func update(text:String,dropDown:DropDown)
    {
        switch dropDown {
        case theCurrentModel.typeDD:
            theCurrentView.lblSelectedType.text = text
            break
        case theCurrentModel.timezoneDD:
            theCurrentView.lblSelectedTimeZone.text = text
            break
        case theCurrentModel.managerDD:
            theCurrentModel.managerTagsField.addTag(text)
            theCurrentModel.managerTagsField.text = ""
            theCurrentModel.managerTagsField.resignFirstResponder()
            theCurrentModel.managerTagsField.beginEditing()
            self.perform(#selector(ShowManagerDropDown), with: nil, afterDelay: 0.2)
            
            break
        case theCurrentModel.tagsDD:
            theCurrentModel.tagsField.addTag(text)
            theCurrentModel.tagsField.text = ""
            theCurrentModel.tagsField.resignFirstResponder()
            theCurrentModel.tagsField.beginEditing()
            self.perform(#selector(ShowDropDown), with: nil, afterDelay: 0.2)
            
//            self.theCurrentModel.getAllTag()
//            theCurrentModel.tagsDD.dataSource = theCurrentModel.arr_Tags
//            self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.tagsDD, view: self.theCurrentView.viewDepartmentTags)
//            self.theCurrentModel.tagsDD.show()
//            if(self.theCurrentModel.FilterTagsArray(text: text).count > 0)
//            {
//                self.theCurrentModel.isDropDownShowing = true
//                self.theCurrentModel.tagsField.returnKeyType = .next
//                self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = true
//                self.theCurrentModel.tagsDD.show()
//            }
            break
        case theCurrentModel.companyDD:
            theCurrentView.lblSelectedCompany.text = text
//            theCurrentView.txtAssignedTo.text = ""
//            theCurrentView.txtDepartment.text = ""
//            theCurrentModel.strSelectedUserAssignID = ""
//            theCurrentModel.strSelectedDepartmentID = ""
//            theCurrentModel.arr_assignList = []
//            theCurrentModel.arr_departmentList = []
//            showLoader(theCurrentView.btnAssignLoaderOutlet)
//            assignUserListService()
            break
        default:
            break
        }
    }
    /*func updateCompany() {
        if let index = theCurrentModel.arr_companyList.firstIndex(where: {$0.id == theCurrentModel.selectedDataId["timezoneId"].stringValue}) {
            theCurrentView.lblSelectedCompany.text = theCurrentModel.arr_companyList[index].name
        }
    }*/
    
    
    @objc func checkFirstTime(_ sender: UITapGestureRecognizer) {
        ShowDropDown()
    }
    
    @objc func ShowDropDown()
    {
        theCurrentModel.getAllTag()
        self.theCurrentModel.tagsDD.show()
        theCurrentModel.tagsField.beginEditing()
    }
    @objc func ShowManagerDropDown()
    {
        theCurrentModel.getAllManagerTag()
        self.theCurrentModel.managerDD.show()
        theCurrentModel.managerTagsField.beginEditing()
    }
    func ResetTagsTextfield()
    {
        self.theCurrentModel.tagsField.resignFirstResponder()
        self.view.endEditing(true)
    }
    func ResetManagerTagsTextfield()
    {
        self.theCurrentModel.managerTagsField.resignFirstResponder()
        self.view.endEditing(true)
    }
    //MARK:- Actions
    @IBAction func onBtnCompnayAction(_ sender: Any) {
        self.view.endEditing(true)
        self.presentDropDownListVC(categorySelection: AddUserVC.categorySelectionType.company)
    }
    @IBAction func onBtnTypeAction(_ sender: Any) {
        self.view.endEditing(true)
        self.presentDropDownListVC(categorySelection: AddUserVC.categorySelectionType.type)
    }
    @IBAction func onBtnTimezoneAction(_ sender: Any) {
        self.view.endEditing(true)
        self.presentDropDownListVC(categorySelection: AddUserVC.categorySelectionType.timezone)
    }
    @IBAction func onSelectProfilePicAction(_ sender: Any) {
        openImageGallery()
    }
    
    @IBAction func onBtnAddAction(_ sender: Any) {
        self.view.endEditing(true)
        validateForm()
    }
    
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.backAction(isAnimation: false)
    }
}

extension WSTagsField{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("called")
        return true
    }
}
//MARK:- Tags method

extension AddUserVC
{
    func textFieldEvents(tagsField : WSTagsField)
    {
        tagsField.onDidAddTag = { _, _ in
            
            print("onDidAddTag")
        }
        
        tagsField.onDidRemoveTag = { _, _ in
            print("onDidRemoveTag")
        }
        
        tagsField.onDidChangeText = { _, text in
            print("onDidChangeText")
            if tagsField == self.theCurrentModel.tagsField {
                if(self.theCurrentModel.FilterTagsArray(text: text ?? "").count > 0)
                {
                    self.theCurrentModel.isDropDownShowing = true
                    self.theCurrentModel.tagsField.returnKeyType = .next
                    self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = true
                    self.theCurrentModel.tagsDD.show()
                }
                else{
                    self.theCurrentModel.isDropDownShowing = false
                    self.theCurrentModel.tagsField.returnKeyType = .default
                    self.theCurrentModel.tagsDD.hide()
                    self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = false
                    if !self.theCurrentModel.isFirstTimeFocus{
                        //                    self.theCurrentModel.tagsField.beginEditing()
                        self.theCurrentModel.tagsDD.show()
                    }
                }
            } else if tagsField == self.theCurrentModel.managerTagsField {
                if(self.theCurrentModel.FilterTagsArray(text: text ?? "", isForManager: true).count > 0)
                {
                    self.theCurrentModel.isDropDownShowing = true
                    self.theCurrentModel.managerTagsField.returnKeyType = .next
                    self.theCurrentModel.managerTagsField.enablesReturnKeyAutomatically = true
                    self.theCurrentModel.managerDD.show()
                }
                else{
                    self.theCurrentModel.isDropDownShowing = false
                    self.theCurrentModel.managerTagsField.returnKeyType = .default
                    self.theCurrentModel.managerDD.hide()
                    self.theCurrentModel.managerTagsField.enablesReturnKeyAutomatically = false
                    if !self.theCurrentModel.isFirstTimeFocus{
                        //                    self.theCurrentModel.tagsField.beginEditing()
                        self.theCurrentModel.managerDD.show()
                    }
                }
            }
            
        }
        
        tagsField.onDidChangeHeightTo = { _, height in
            print("HeightTo \(height)")
            if tagsField == self.theCurrentModel.tagsField {
                self.theCurrentView.constrainviewDepartmentTagsHeight.constant = height
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.tagsDD, view: self.theCurrentView.viewDepartmentTags)
            } else if tagsField == self.theCurrentModel.managerTagsField {
                self.theCurrentView.constrainManagerViewTagsHeight.constant = height
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.managerDD, view: self.theCurrentView.viewManagerTags)
            }
            
//            if !self.theCurrentModel.isFirstTimeFocus{
//                self.theCurrentModel.tagsField.beginEditing()
//                self.theCurrentModel.tagsDD.show()
//            }
        }
        tagsField.onDidSelectTagView = { _, tagView in
            print("Select \(tagView)")
            if tagsField == self.theCurrentModel.tagsField {
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.tagsDD, view: self.theCurrentView.viewDepartmentTags)
                
            } else if tagsField == self.theCurrentModel.managerTagsField {
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.managerDD, view: self.theCurrentView.viewManagerTags)
            }
        }
        tagsField.onDidUnselectTagView = { _, tagView in
            print("Unselect \(tagView)")
            if tagsField == self.theCurrentModel.tagsField {
                self.theCurrentModel.getAllTag()
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.tagsDD, view: self.theCurrentView.viewDepartmentTags)
                self.theCurrentModel.tagsDD.show()
                
            } else if tagsField == self.theCurrentModel.managerTagsField {
                self.theCurrentModel.getAllManagerTag()
                self.theCurrentModel.configureDropDown(dropDown: self.theCurrentModel.managerDD, view: self.theCurrentView.viewManagerTags)
                self.theCurrentModel.managerDD.show()
            }
        }
        
    }
}
//MARK:- Textfield delegate
extension AddUserVC : UITextFieldDelegate
{
    /*
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == self.theCurrentModel.tagsField)
        {
            print("tagsField should begin editing")
            self.theCurrentModel.getAllTag()
//            self.theCurrentModel.isDropDownShowing = false
            self.theCurrentModel.tagsDD.show()
        }
        return true
    }*/
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.theCurrentModel.tagsField) {
            print("tagsField did begin editing")
//            theCurrentModel.tagsField.beginEditing()
//            theCurrentModel.tagsField.text = ""
        
            if(self.theCurrentModel.arr_Tags.count > 0) {
                theCurrentModel.tagsDD.dataSource = theCurrentModel.arr_Tags
                self.theCurrentModel.isDropDownShowing = true
                self.theCurrentModel.tagsField.returnKeyType = .next
                self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = true
                self.theCurrentModel.tagsDD.show()
//                ShowDropDown()
            }
        } else if(textField == self.theCurrentModel.managerTagsField) {
            print("tagsField did begin editing")
            //            theCurrentModel.tagsField.beginEditing()
            //            theCurrentModel.tagsField.text = ""
            
            if(self.theCurrentModel.arr_ManagerTags.count > 0) {
                theCurrentModel.managerDD.dataSource = theCurrentModel.arr_ManagerTags
                self.theCurrentModel.isDropDownShowing = true
                self.theCurrentModel.managerTagsField.returnKeyType = .next
                self.theCurrentModel.managerTagsField.enablesReturnKeyAutomatically = true
                self.theCurrentModel.managerDD.show()
                //                ShowDropDown()
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField == theCurrentModel.tagsField {
            self.theCurrentModel.tagsDD.hide()

        } else if textField == theCurrentModel.managerTagsField {
            self.theCurrentModel.managerDD.hide()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(!self.theCurrentModel.isDropDownShowing)
        {
            print("textField did end editing")
            if(textField == self.theCurrentModel.tagsField)
            {
                print("tagsField did end editing")
                if(!(textField.text?.trimmed().isEmpty)!)
                {
                    textField.text = ""
                }
            } else if textField == theCurrentModel.managerTagsField {
                print("tagsField did end editing")
                if(!(textField.text?.trimmed().isEmpty)!)
                {
                    textField.text = ""
                }
            }
        }
    }
    
}
//MARK:- Call API
extension AddUserVC {
    
    func getCompanyList()
    {
        showLoader(theCurrentView.btnCompanyLoaderOutlet)
        companylistService()
    }
}
//MARK:- Loader
extension AddUserVC
{
    func showLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(true)
    }
    
    func stopLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(false)
    }
    
}

//MARK:- Call API
extension AddUserVC
{
    func getTimezoneListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        AddUserWebService.shared.getAllTimezoneList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_TimezoneList = list
            self?.stopLoader(self?.theCurrentView.btnTimeZoneLoaderOutlet)
            if self?.theCurrentModel.selectedUserType == .editUser{
                guard let theModel = self?.theCurrentModel.theUserDetailModel else { return }
                if let index = self?.theCurrentModel.arr_TimezoneList.firstIndex(where: {$0.timezoneId == theModel.timezoneId}) {
                    let data = self?.theCurrentModel.arr_TimezoneList[index]
                    self?.theCurrentModel.selectedData["timezone"].stringValue =
                    "\(data?.timezoneName ?? "") (\(data?.timezoneCode ?? ""))"
                    self?.theCurrentModel.selectedDataId["timezoneId"].stringValue = theModel.timezoneId
                    self?.theCurrentView.lblSelectedTimeZone.text = self?.theCurrentModel.selectedData["timezone"].stringValue
                }
            } else if self?.theCurrentModel.selectedUserType == .editProfile{
                guard let theModel = self?.theCurrentModel.theUserModel else { return }
                if let index = self?.theCurrentModel.arr_TimezoneList.firstIndex(where: {$0.timezoneId == theModel.timezoneId}) {
                    let data = self?.theCurrentModel.arr_TimezoneList[index]
                    self?.theCurrentModel.selectedData["timezone"].stringValue =
                    "\(data?.timezoneName ?? "") (\(data?.timezoneCode ?? ""))"
                    self?.theCurrentModel.selectedDataId["timezoneId"].stringValue = theModel.timezoneId
                    self?.theCurrentView.lblSelectedTimeZone.text = self?.theCurrentModel.selectedData["timezone"].stringValue
                }
            }
            
            
            }, failed: { [weak self] (error) in
                self?.stopLoader(self?.theCurrentView.btnTimeZoneLoaderOutlet)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_companyList = list
            self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
            self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
        })
    }
    
    func userTypeListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        if theCurrentModel.selectedUserType == .editUser   {
            parameter[APIKey.key_role_id] = theCurrentModel.theUserDetailModel?.roleId
        } else if theCurrentModel.selectedUserType == .editProfile{
            parameter[APIKey.key_role_id] = theCurrentModel.theUserModel?.roleId
        }
        CommanListWebservice.shared.getAllUserTypeList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_UserTypeList = list
            self?.stopLoader(self?.theCurrentView.btnTypeLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnTypeLoaderOutlet)
        })
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.selectedDataId["companyId"].stringValue]
        AddUserWebService.shared.getCompanyDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.stopLoader(self?.theCurrentView.btnTagLoaderOutlet)
            self?.theCurrentModel.arr_Tags_Main = list
            self?.theCurrentModel.arr_Tags = list.map({$0.name})
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnTagLoaderOutlet)
                
        })
    }
    func managerListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.selectedDataId["companyId"].stringValue]
        if theCurrentModel.selectedUserType == .editUser {
            parameter[APIKey.key_exclude_user_id] = theCurrentModel.theUserDetailModel?.userId ?? ""
        }
        AddUserWebService.shared.getCompanyManagers(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.stopLoader(self?.theCurrentView.btnManagerTagLoaderOutlet)
            self?.theCurrentModel.arr_ManagerTags_Main = list
            self?.theCurrentModel.arr_ManagerTags = list.map({$0.fullName})
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnTagLoaderOutlet)
                
        })
    }
    
    func addUserWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        
        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].selectedFileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].selectedFileName
                let mimeType = MimeType(path: path).value
//                let mimeExtension = mimeType.components(separatedBy: "/")
//                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].selectedFileData!)
                fileMimeType.append(mimeType)
                withName.append(APIKey.key_user_image)
                withFileName.append("\(path)")
            }
        }
        
        AddUserWebService.shared.uploadAddUserAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (msg) in
            self?.isClickOnAdd(isLoading: false)
            self?.showAlertAtBottom(message: msg)
            self?.handlerAddUserData()
            self?.backAction(isAnimation: false)
            self?.view.allButtons(isEnable: true)
            }, failed: { [weak self] (error) in
                self?.view.allButtons(isEnable: true)
                self?.isClickOnAdd(isLoading: false)
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
    func editUserWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        
        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].selectedFileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].selectedFileName
                let mimeType = MimeType(path: path).value
                //                let mimeExtension = mimeType.components(separatedBy: "/")
                //                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].selectedFileData!)
                fileMimeType.append(mimeType)
                withName.append(APIKey.key_user_image)
                withFileName.append("\(path)")
            }
        }
        
        AddUserWebService.shared.uploadEditUserAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (msg,data) in
            self?.isClickOnAdd(isLoading: false)
            self?.handlerEditUserData(data)            
            self?.showAlertAtBottom(message: msg)
            self?.backAction(isAnimation: false)
            self?.view.allButtons(isEnable: true)
            }, failed: { [weak self] (error) in
                self?.view.allButtons(isEnable: true)
                self?.isClickOnAdd(isLoading: false)
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
    func editUserProfileWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        
        for i in 0..<theCurrentModel.arr_selectedFile.count{
            if (theCurrentModel.arr_selectedFile[i].selectedFileData?.count)! > 0{
                let path = theCurrentModel.arr_selectedFile[i].selectedFileName
                let mimeType = MimeType(path: path).value
                //                let mimeExtension = mimeType.components(separatedBy: "/")
                //                let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                
                fileData.append(theCurrentModel.arr_selectedFile[i].selectedFileData!)
                fileMimeType.append(mimeType)
                withName.append(APIKey.key_user_image)
                withFileName.append("\(path)")
            }
        }
        
        AddUserWebService.shared.uploadEditUserProfileAttachment(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (msg,data) in
            self?.isClickOnAdd(isLoading: false)
            self?.handlerEditUserProfileData()
            self?.showAlertAtBottom(message: msg)
            self?.backAction(isAnimation: false)
            self?.view.allButtons(isEnable: true)
            }, failed: { [weak self] (error) in
                self?.view.allButtons(isEnable: true)
                self?.isClickOnAdd(isLoading: false)
                self?.showAlertAtBottom(message: error)
        })
        
    }
    
}
