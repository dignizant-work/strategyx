//
//  AddRiskVC.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQAudioRecorderController

class AddRiskVC: ParentViewController {

    //MARk:- Variable
    fileprivate lazy var theCurrentView:AddRiskView = { [unowned self] in
       return self.view as! AddRiskView
    }()
    
    internal lazy var theCurrentViewModel:AddRiskViewModel = {
       return AddRiskViewModel(theController: self)
    }()
    
    var handlerUpdateModel:(RiskDetail?) -> Void = {_ in }
    var handlerEditDueDateChanged:(String,String) -> Void = {_,_ in } // date, color

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        theCurrentView.setObserver(theController:self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.removeObserver(theController:self)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            
            if tabl == theCurrentView.tblNotesHistory {
                self.theCurrentView.heightOfNotesHistory.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height
                
            }
            self.view.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        if keyPath == "contentSize", let coll = object as? UICollectionView {
            self.theCurrentView.heightOfCollectionSelectedFile.constant = coll.contentSize.height
            self.view.layoutIfNeeded()
            print("coll.contentSize.height:=",coll.contentSize.height)
        }
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentViewModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.setupCollectionView(theDelegate: self)
        theCurrentViewModel.arr_noteHistoryStatus = Utility.shared.setArrayNotesHistoryStatus()
        theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
        theCurrentView.viewCommonRelatedToDueDate.isHidden = true
        theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
        theCurrentView.lblCommanRelatedToDueDate.text = ""
        if !theCurrentViewModel.isEdit { //New
            theCurrentViewModel.selectedData["createdDate"].stringValue = Date().string(withFormat: DateFormatterOutputType.outputType7)
            theCurrentViewModel.selectedData["createdby"].stringValue = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
            theCurrentView.viewRevisedDueDate.isHidden = true
            theCurrentView.showUpdateData(theModel: theCurrentViewModel.selectedData)
            theCurrentViewModel.nextOffset = -1
            theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: false)
            theCurrentView.viewNoteHistoryFilter.isHidden = true
            theCurrentView.constrainViewNoteHistiryFIlter.constant = 0.0
//            theCurrentViewModel.categoiesListService()

            
            if !theCurrentViewModel.isForAddAction {
                theCurrentViewModel.relatedListService()
            }
            
            let isGoal =  theCurrentViewModel.strSelectedRelatedId == "1"
            theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = !isGoal
            theCurrentView.viewCommonRelatedToDueDate.isHidden = isGoal
            if theCurrentViewModel.selectionRelatedToDueDate.isEmpty {
                theCurrentView.viewStrategyGoalRelatedToDueDate.isHidden = true
                theCurrentView.viewCommonRelatedToDueDate.isHidden = true
                theCurrentView.lblStrategyGoalRelatedToDueDate.text = ""
                theCurrentView.lblCommanRelatedToDueDate.text = ""
            }
            theCurrentView.updateDateView(label: isGoal ? theCurrentView.lblStrategyGoalRelatedToDueDate : theCurrentView.lblCommanRelatedToDueDate, strDueDate: theCurrentViewModel.selectionRelatedToDueDate, color: theCurrentViewModel.selectionRelatedToDueDateColor)
            
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                theCurrentViewModel.companylistService()
            } else {
                theCurrentView.viewCompany.isHidden = true
                theCurrentViewModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentViewModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
                theCurrentView.lblCompany.text = Utility.shared.userData.companyName
                theCurrentViewModel.assignUserListService()
                theCurrentViewModel.riskCategoiesListService()
                theCurrentViewModel.riskConsequenceCategoiesListService()
            }
            updateUIForAddAction()

        } else {
            theCurrentViewModel.showTheEditData()
            theCurrentView.viewRevisedDueDate.isHidden = false
            theCurrentView.collectionSelectedFile.reloadData()
            theCurrentView.btnSelectCompany.isUserInteractionEnabled = false
            theCurrentView.showUpdateData(theModel: theCurrentViewModel.selectedData)
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                theCurrentView.viewCompany.isHidden = true
            }
            if theCurrentViewModel.theEditRiskModel?.risk_status.lowercased() == APIKey.key_archived.lowercased() {
                setEditDataForArchive()
            } else {
                if let createdBy = theCurrentViewModel.theEditRiskModel?.created_by_id, UserDefault.shared.isCanEditForm(strOppoisteID: createdBy) {
//                    theCurrentViewModel.categoiesListService()
                    theCurrentViewModel.strSelectedCompanyID = theCurrentViewModel.theEditRiskModel?.company_id ?? "\(Utility.shared.userData.companyId)"
                    theCurrentViewModel.assignUserListService()
                    
                    theCurrentViewModel.riskCategoiesListService()
                    theCurrentViewModel.riskConsequenceCategoiesListService()
                    if !theCurrentViewModel.strSelectedCategoryID.isEmpty {
                        theCurrentViewModel.riskSubCategoiesListService()
                    }

                }
                if !theCurrentViewModel.strSelectedUserAssignID.isEmpty {
                    theCurrentViewModel.departmentListService()
                }

            }
            
            theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
            theCurrentViewModel.getActionNotesHistoryWebService(isRefreshing: true, status: self.theCurrentViewModel.strNoteStatus)

        }
        
        theCurrentView.constrainViewPagerHeight.constant = 0.0
        theCurrentViewModel.isDisableCompanySelection()
        
        (theCurrentView.lblCommanRelatedToDueDate as? LabelButton)?.onClick = { [weak self] in
            self?.redirectToCustomDatePicker(selectionType: ActionModel.dateSelectionType.due, selectedDate: self?.theCurrentViewModel.selectionRelatedToDueDate ?? "", dueDate: self?.theCurrentViewModel.selectionRelatedToDueDate ?? "", strActionID: self?.theCurrentViewModel.selectionRelatedToActionID ?? "", isGoal: false)
        }
        
        (theCurrentView.lblStrategyGoalRelatedToDueDate as? LabelButton)?.onClick = { [weak self] in
            self?.redirectToCustomDatePicker(selectionType: ActionModel.dateSelectionType.due, selectedDate: self?.theCurrentViewModel.selectionRelatedToDueDate, dueDate: self?.theCurrentViewModel.selectionRelatedToDueDate ?? "", strActionID: self?.theCurrentViewModel.selectionRelatedToActionID ?? "", isGoal: true)
            
        }
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentViewModel.imageBG = imgBG
    }
    func setTheEditData(theModel:RiskDetail?, isForEdit:Bool)  {
        theCurrentViewModel.isEdit = isForEdit
        theCurrentViewModel.theEditRiskModel = theModel
    }
    
    func setAddStrategyData(thePrimaryAreaId:String,thePrimaryAreaName:String,theSecondaryAreaId:String,theSecondaryAreaName:String,theGoalId:String,theGoalName:String,theDueDate:String,theDueDateColor:String,isStatusArchive:Bool) {
        theCurrentViewModel.isForAddAction = true
        theCurrentViewModel.isStatusArchive = isStatusArchive
        theCurrentViewModel.strSelectedRelatedId = "1" //Strategy
        theCurrentViewModel.selectionRelatedToDueDate = theDueDate
        theCurrentViewModel.selectionRelatedToDueDateColor = theDueDateColor
        
        theCurrentViewModel.strSelectedPrimaryAreaId = thePrimaryAreaId
        theCurrentViewModel.selectedData["strategyprimaryarea"].stringValue = thePrimaryAreaName
        theCurrentView.lblPrimaryArea.text = thePrimaryAreaName
        
        theCurrentViewModel.strSelectedSecondryAreaId = theSecondaryAreaId
        theCurrentViewModel.selectedData["strategysecondaryarea"].stringValue = theSecondaryAreaName
        theCurrentView.lblSecondaryArea.text = theSecondaryAreaName
        
        theCurrentViewModel.strSelectedStrategyGoalId = theGoalId
        theCurrentViewModel.selectedData["strategygoal"].stringValue = theGoalName
        theCurrentView.lblStrategyGoal.text = theGoalName
        print("theDueDate:=",theDueDate)
        theCurrentViewModel.relatedToSelectionType = .strategy
        theCurrentViewModel.selectionRelatedToActionID = theGoalId
    }
    
    func setEditDataForArchive() {
        if theCurrentViewModel.theEditRiskModel?.risk_status.lowercased() == APIKey.key_archived.lowercased() {
            theCurrentView.btnAdd.isHidden = true
            //             let allButtons  = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            
            theCurrentView.viewNotes.isHidden = true
            theCurrentView.constrainViewNoteHeight.constant = 0.0
            
            theCurrentView.viewFileName.isHidden = true
            theCurrentView.btnAddVoiceNote.isHidden = true
            theCurrentView.constrainViewfileNameHeight.constant = 0.0
            
            
            let view = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            view.forEach({$0.isUserInteractionEnabled = false})
            
            theCurrentView.scrollView.isUserInteractionEnabled = true
            if let view = theCurrentView.scrollView.viewWithTag(1890) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            
            if let view = theCurrentView.scrollView.viewWithTag(1891) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            if let view = theCurrentView.scrollView.viewWithTag(1700) {
                view.isUserInteractionEnabled = true
                if let viewsub = view.viewWithTag(1701) {
                    viewsub.isUserInteractionEnabled = true
                    if let viewsub1 = viewsub.viewWithTag(1702) {
                        viewsub1.isUserInteractionEnabled = true
                    }
                }
                
            }
            
            if let view = theCurrentView.scrollView.viewWithTag(200) {
                view.isUserInteractionEnabled = true
            }
            theCurrentView.viewOfPager.isUserInteractionEnabled = true
            theCurrentView.viewMore.isUserInteractionEnabled = true
        }
    }
    func updateUIForAddAction(){
        if theCurrentViewModel.isForAddAction{
            theCurrentView.viewRelatedToTxt.isUserInteractionEnabled = false
            theCurrentView.viewRelatedToTxt.alpha = 0.5
            if theCurrentViewModel.isStatusArchive {
                theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
            } else {
                theCurrentView.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                theCurrentView.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
            }
            switch theCurrentViewModel.strSelectedRelatedId{
            case "0": // General Action
                break
            case "1": //Strategy
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Strategy Goal"
                theCurrentView.lblRelatedTo.text = "Strategy Goal"
                
                theCurrentView.viewPrimaryArea.isHidden = false
                theCurrentView.viewPrimaryAreaTxt.isUserInteractionEnabled = false
                theCurrentView.viewPrimaryAreaTxt.alpha = 0.5
                theCurrentView.lblPrimaryArea.text = theCurrentViewModel.selectedData["strategyprimaryarea"].stringValue
                
                theCurrentView.viewSecondaryArea.isHidden = false
                theCurrentView.viewSecondaryAreaTxt.isUserInteractionEnabled = false
                theCurrentView.viewSecondaryAreaTxt.alpha = 0.5
                theCurrentView.lblSecondaryArea.text = theCurrentViewModel.selectedData["strategysecondaryarea"].stringValue
                
                theCurrentView.viewStrategyGoal.isHidden = false
                theCurrentView.viewStrategyGoalTxt.isUserInteractionEnabled = false
                theCurrentView.viewStrategyGoalTxt.alpha = 0.5
                theCurrentView.lblStrategyGoal.text = theCurrentViewModel.selectedData["strategygoal"].stringValue
                break
            case "2": //Risk
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Risk"
                theCurrentView.lblRelatedTo.text = "Risk"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtorisk"].stringValue
                break
            case "3": //Tactical Project
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Tactical Project"
                theCurrentView.lblRelatedTo.text = "Tactical Project"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtotacticalproject"].stringValue
                break
            case "4": //Focus
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Focus"
                theCurrentView.lblRelatedTo.text = "Focus"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtofocus"].stringValue
                break
            case "6": //Success Factor
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Success Factor"
                theCurrentView.lblRelatedTo.text = "Success Factor"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtosuccessfactor"].stringValue
                break
            case "7": //Idea
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Idea"
                theCurrentView.lblRelatedTo.text = "Idea"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtoidea"].stringValue
                break
            case "8": //Problem
                theCurrentView.viewCommonRelatedToTxt.isUserInteractionEnabled = false
                theCurrentView.viewCommonRelatedToTxt.alpha = 0.5
                theCurrentView.viewCommanRelatedTo.isHidden = false
                theCurrentViewModel.selectedData["relatedto"].stringValue = "Problem"
                theCurrentView.lblRelatedTo.text = "Problem"
                theCurrentView.lblCommanRelatedTo.text = theCurrentViewModel.selectedData["relatedtoproblem"].stringValue
                break
            default:
                break
            }
        }
    }
    
    //MARK:- Redirect To
    
    func presentDropDownListVC(categorySelection:AddRiskViewModel.categorySelectionType) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .category:
            let arr_list:[String] = theCurrentViewModel.arr_categoryList.map({$0.categoryName })
            vc.setTheData(strTitle: "Category",arr:arr_list)
            break
        case .company:
            let arr_company:[String] = theCurrentViewModel.arr_companyList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Company",arr:arr_company)
            break
        case .assignTo:
            let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assignee_name}).filter({!$0.isEmpty})
            //          let arr_assignTo:[String] = theCurrentViewModel.arr_assignList.map({$0.assign_user_name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Assigned to",arr:arr_assignTo)
            break
        case .department:
            let arr_department:[String] = theCurrentViewModel.arr_departmentList.map({$0.name}).filter({!$0.isEmpty})
            vc.setTheData(strTitle: "Depatment",arr:arr_department)
            break
        case .subCategory:
            let arr_list:[String] = theCurrentViewModel.arr_subcategoryList.map({$0.subcategoryName })
            vc.setTheData(strTitle: "Sub Category",arr:arr_list)
        case .consequenceCategory:
            let arr_list:[String] = theCurrentViewModel.arr_consequenceList.map({$0.categoryName })
            vc.setTheData(strTitle: "Consequence",arr:arr_list)
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.updateDropDownData(categorySelection:categorySelection, str: strData, index: index)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentDropDownListVC(categorySelection:AddActionVC.categorySelectionType,cellType:AddActionModel.celltype) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .relatedTo:
            let arr_list:[String] = theCurrentViewModel.arr_relatedList.map({$0.option })
            vc.setTheData(strTitle: "Related To",arr:arr_list)
            break
        case .relatedToStrategyPrimaryArea:
            let arr_list:[String] = theCurrentViewModel.arr_actionPrimaryAreaList.map({$0.title })
            vc.setTheData(strTitle: "Primary Area",arr:arr_list)
            break
        case .relatedToStrategySecondaryArea:
            let arr_list:[String] = theCurrentViewModel.arr_actionSecondryAreaList.map({$0.title })
            vc.setTheData(strTitle: "Secondary Area",arr:arr_list)
            break
        case .relatedToStrategyGoal:
            let arr_list:[String] = theCurrentViewModel.arr_actionStrategyGoalList.map({$0.title })
            vc.setTheData(strTitle: "Strategy Goal",arr:arr_list)
            break
        case .relatedToRisk:
            let arr_list:[String] = theCurrentViewModel.arr_actionRiskList.map({$0.unwanted_event })
            vc.setTheData(strTitle: "Related To Risk",arr:arr_list)
            break
        case .relatedToFocus:
            let arr_list:[String] = theCurrentViewModel.arr_actionFocusList.map({$0.title })
            vc.setTheData(strTitle: "Related To Focus",arr:arr_list)
            break
        case .relatedToSuccessFactor:
            let arr_list:[String] = theCurrentViewModel.arr_actionSuccessFactorList.map({$0.criticalSuccessFactorName })
            vc.setTheData(strTitle: "Related To Success Factor",arr:arr_list)
            break
        case .relatedToTacticalProject:
            let arr_list:[String] = theCurrentViewModel.arr_actionTacticalProjectList.map({$0.project_name })
            vc.setTheData(strTitle: "Related To Tactical Project",arr:arr_list)
            break
        case .relatedToIdea:
            let arr_list:[String] = theCurrentViewModel.arr_actionIdeaAndProblem.map({$0.title })
            vc.setTheData(strTitle: "Related To Idea",arr:arr_list)
            break
        case .relatedToProblem:
            let arr_list:[String] = theCurrentViewModel.arr_actionIdeaAndProblem.map({$0.title })
            vc.setTheData(strTitle: "Related To Problem",arr:arr_list)
            break
        case .selectTag:
            break
        case .company:
            break
        case .assignTo:
            break
        case .department:
            break
        case .approvedBy:
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.updateDropDownData(categorySelection:categorySelection, str: strData, cellType: cellType,index: index)
        }
        
        
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func redirectToDatePicker(selectionType:AddRiskViewModel.dateSelctionType) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = Date()
        vc.isSetDate = false
        
        if theCurrentViewModel.isEdit {
            vc.isSetDate = true
            vc.minimumDate = theCurrentViewModel.selectedrevisedDate ?? Date()
            vc.setDateValue = theCurrentViewModel.selectedrevisedDate
        }
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.dateAndTime)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = DateFormatterOutputType.outPutType4
                //                self?.theCurrentViewModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
                self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String,isGoal:Bool) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID, isGoal: isGoal)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func redirectToCustomDatePicker(selectionType:AddRiskViewModel.dateSelctionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = theCurrentViewModel.selectedData["duedate"].string, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .resived:
            if let reviseDate = theCurrentViewModel.selectedData["revisedduedate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentViewModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            self?.theCurrentViewModel.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }

    func presentFileManger() {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        audioRecorderVC.audioFormat = .default
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        
    }
    func presentDropDownListVCForNoteHistory() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        let arr_list:[String] = theCurrentViewModel.arr_noteHistoryStatus.map({$0["name"].stringValue })
        vc.setTheData(strTitle: "Notes History", arr: arr_list)
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentViewModel.arr_notesHistoryList?.removeAll()
            self?.theCurrentView.tblNotesHistory.reloadData()
            self?.theCurrentView.txtNotesHistory.text = strData
            self?.theCurrentViewModel.strNoteStatus = self?.theCurrentViewModel.arr_noteHistoryStatus[index]["status"].stringValue ?? ""
            self?.theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
            self?.theCurrentViewModel.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentViewModel.strNoteStatus ?? "")
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentRiskLevelVC(strLevel:String, isForResidual:Bool) {
        let vc = RiskLevelVC.init(nibName: "RiskLevelVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.setTheData(imgBG: nil, selectedLevel: strLevel)
        vc.handlerOnButtonLevelClick = { [weak self] (theLevel) in
            if isForResidual {
                self?.theCurrentViewModel.selectedData["residualLevel"].stringValue = theLevel
                if let theModel = self?.theCurrentViewModel.selectedData {
                    self?.theCurrentView.showDropDownUpdatedData(theModel: theModel)
                }
            } else {
                self?.theCurrentViewModel.selectedData["level"].stringValue = theLevel
                if let theModel = self?.theCurrentViewModel.selectedData {
                    self?.theCurrentView.showDropDownUpdatedData(theModel: theModel)
                }
            }
            
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }

    //MARK:- IBAction
    @IBAction func onBtnSelectNotesHistoy(_ sender:UIButton){
        presentDropDownListVCForNoteHistory()
    }
    @IBAction func onBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .company)
    }
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select first company")
            return
        }
        presentDropDownListVC(categorySelection: .assignTo)
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedUserAssignID.isEmpty {
            self.showAlertAtBottom(message: "Please select first assigned to")
            return
        }
        presentDropDownListVC(categorySelection: .department)
    }
    @IBAction func onBtnApprovedByAction(_ sender: Any) {
        self.view.endEditing(true)
//        presentDropDownListVC(categorySelection: .approvedBy, cellType: .approvedBy)
    }
    
    @IBAction func onBtnCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select first company")
            return
        } else {
            if theCurrentViewModel.arr_categoryList.count == 0 {
                theCurrentViewModel.riskCategoiesListService()
                return
            }
        }
        presentDropDownListVC(categorySelection: .category)
    }
    
    @IBAction func onBtnSubCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCategoryID.isEmpty {
            self.showAlertAtBottom(message: "Please select first category")
            return
        } else if theCurrentViewModel.arr_subcategoryList.count == 0 {
            self.showAlertAtBottom(message: "Sub category list not available, Please select other category")
            return
        }
        presentDropDownListVC(categorySelection: .subCategory)
    }
    
    @IBAction func onBtnConsequenceAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCompanyID.isEmpty {
            self.showAlertAtBottom(message: "Please select first company")
            return
        } else {
            if theCurrentViewModel.arr_consequenceList.count == 0 {
                theCurrentViewModel.riskConsequenceCategoiesListService()
                return
            }
        }
        presentDropDownListVC(categorySelection: .consequenceCategory)
    }
    
    
    @IBAction func onBtnlevelAction(_ sender: Any) {
        self.view.endEditing(true)
        presentRiskLevelVC(strLevel: theCurrentViewModel.selectedData["level"].stringValue, isForResidual: false)
    }
    
    @IBAction func onBtnResidualLevelAction(_ sender: Any) {
        self.view.endEditing(true)
        presentRiskLevelVC(strLevel: theCurrentViewModel.selectedData["residualLevel"].stringValue, isForResidual: true)
    }
    
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: .due)
    }
   
    @IBAction func onBtnRevisedDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: .resived)
    }
    
    @IBAction func onBtnAttachmentFileAction(_ sender: Any) {
        self.view.endEditing(true)
        presentFileManger()
    }
    
    @IBAction func onBtnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let strNoteMsg = theCurrentView.txtNote.text ?? ""
        if strNoteMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter note")
            return
        } else {
            if !theCurrentViewModel.isEdit {
                theCurrentView.txtNote.text = ""
                let history = NotesHistory()
                history.createdDate = Date().string(withFormat: DateFormatterInputType.inputType1)
                history.notesHistoryDescription = strNoteMsg
                history.actionUserName = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
                history.historyStatus = APIKey.key_Commented
                if theCurrentViewModel.arr_notesHistoryList == nil {
                    theCurrentViewModel.arr_notesHistoryList = []
                }
                theCurrentViewModel.arr_notesHistoryList?.append(history)
                theCurrentView.tblNotesHistory.reloadData()
                return
            }
            sender.startLoadyIndicator()
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentViewModel.theEditRiskModel?.risk_id ?? "",
                                 APIKey.key_notes: strNoteMsg,
                                 APIKey.key_note_for: APIKey.key_assign_to_for_risks
            ]
            
            theCurrentViewModel.addNotes(parameter: parameter, completion: {
                sender.stopLoadyIndicator()
            })
        }
    }
    
    @IBAction func onAddVoiceAction(_ sender: Any) {
        self.view.endEditing(true)
        presentVoiceNote()
    }
    
    @IBAction func onAddAction(_ sender: Any) {
        theCurrentViewModel.validateForm()
    }
    
    @IBAction func onBtnCancelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if theCurrentViewModel.isEdit {
            sender.startLoadyIndicator()
            theCurrentViewModel.closeHistorySaveWebService { [weak self] in
                sender.stopLoadyIndicator()
                self?.backAction(isAnimation: false)
            }
        } else {
            self.backAction(isAnimation: false)
        }
    }
    
    @IBAction func onBtnRemoveAttachmentAction(_ sender: UIButton) {
        /*if theCurrentViewModel.isEditScreen == .edit{
         let fileName = self.theCurrentViewModel.arr_selectedFile[sender.tag].selectedFileName
         if let selectedIndex = theCurrentViewModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
         theCurrentViewModel.arr_strRemoveAttachment.append(theCurrentViewModel.arr_fileList[selectedIndex].fileId)
         }
         }
         self.theCurrentViewModel.arr_selectedFile.remove(at: sender.tag)*/
        
        
        let attachment = theCurrentViewModel.arr_selectedFile[sender.tag]
        theCurrentViewModel.arr_RemoveSelectedFile.append(attachment)
        theCurrentViewModel.arr_selectedFile.remove(at: sender.tag)
        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    @IBAction func onBtnViewMoreAction(_ sender: Any) {
        theCurrentViewModel.loadViewMoreNoteHistory(isAnimating: true)
        theCurrentViewModel.getActionNotesHistoryWebService(status: theCurrentViewModel.strNoteStatus)
    }
    
    @IBAction func onBtnRelatedToAction(_ sender: Any) {
        self.view.endEditing(true)
        
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relatedToGeneral)
    }
    
    @IBAction func onBtnCommanRelatedToAction(_ sender: Any) {
        switch theCurrentViewModel.relatedToSelectionType {
        case .general:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relatedToGeneral)
            break
        case .strategy:
            //            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedTo, cellType: AddActionModel.celltype.relat)
            break
        case .risk:
            
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToRisk, cellType: AddActionModel.celltype.relatedToRisk)
            break
        case .focus:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToFocus, cellType: AddActionModel.celltype.relatedToFocus)
            break
        case .tactical:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToTacticalProject, cellType: AddActionModel.celltype.relatedToTacticalProject)
            break
        case .idea:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToIdea, cellType: AddActionModel.celltype.relatedToIdea)
            break
        case .problem:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToProblem, cellType: AddActionModel.celltype.relatedToProblem)
            break
        case .successFactor:
            presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToSuccessFactor, cellType: AddActionModel.celltype.relatedToSuccessFactor)
            break
        }
        
    }
    @IBAction func onBtnPrimaryAreaAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategyPrimaryArea, cellType: AddActionModel.celltype.primaryArea)
    }
    
    @IBAction func onBtnSecondaryAreaAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategySecondaryArea, cellType: AddActionModel.celltype.secondaryArea)
    }
    @IBAction func onBtnStrategyGoalAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: AddActionVC.categorySelectionType.relatedToStrategyGoal, cellType: AddActionModel.celltype.strategyGoal)
    }
}

//MARK:- UIDocumentPickerDelegate
extension AddRiskVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    }) {
                        
                    } else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentViewModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                }) {
                    
                } else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        }
        else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported, \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension AddRiskVC:IQAudioRecorderViewControllerDelegate {
    
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")
        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
                //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
//                let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                
                //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
//                self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                
                
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentViewModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.theCurrentView.collectionSelectedFile.reloadData()
                    }
                    
                }
                
            }
            
        }
        controller.dismiss(animated: true, completion: nil)
        

    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}
