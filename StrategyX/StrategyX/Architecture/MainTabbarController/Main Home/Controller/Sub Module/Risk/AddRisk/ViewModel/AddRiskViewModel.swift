//
//  AddRiskViewModel.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddRiskViewModel {

    //MARK:-Variable
    fileprivate weak var theController:AddRiskVC!

    var imageBG:UIImage?

    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?

    var arr_selectedFile:[AttachmentFiles] = []
    var arr_RemoveSelectedFile:[AttachmentFiles] = []

    var selectedDueDate:Date?
    var selectedrevisedDate:Date?
    
    // get consequnces
    var arr_consequenceList:[RiskConsequenceCategory] = []
    var strSelectedConsequenceID:String = ""
    
    // get category
    var arr_categoryList:[RiskCategory] = []
    var strSelectedCategoryID:String = ""

    // get Sub category
    var arr_subcategoryList:[RiskSubCategory] = []
    var strSelectedSubCategoryID:String = ""

    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""

    var strNoteStatus:String = ""
    var arr_noteHistoryStatus:[JSON] = []
    
    var isForAddAction = false
    var isForAddNoteAction = false

    var isEdit = false
    var theEditRiskModel:RiskDetail?

    var selectedData:JSON = JSON(["company":"","department":"","description":"","assignto":"","unwantedevent":"","duedate":""])

    var relatedToSelectionType = AddActionModel.relatedToSelectionType.general

    // Related to List
    
    var arr_relatedList:[RelatedTo] = []
    var strSelectedRelatedId = ""
    
    // Related to Risk List
    
    var arr_actionRiskList:[ActionRisk] = []
    var strSelectedRiskId = ""
    
    // Related to Tactical Project List
    
    var arr_actionTacticalProjectList:[ActionTacticalProject] = []
    var strSelectedTacticalProjectId = ""
    
    // Related to Focus List
    var arr_actionFocusList:[ActionFocus] = []
    var strSelectedFocusId = ""
    
    // Related to Focus List
    var arr_actionSuccessFactorList:[ActionSuccessFactor] = []
    var strSelectedSuccessFactorId = ""
    
    // Related to PrimaryArea List
    var arr_actionPrimaryAreaList:[PrimaryArea] = []
    var strSelectedPrimaryAreaId = ""
    
    // Related to SecondryArea List
    var arr_actionSecondryAreaList:[SecondaryArea] = []
    var strSelectedSecondryAreaId = ""
    
    // Related to StrategyGoal List
    var arr_actionStrategyGoalList:[StrategyGoalList] = []
    var strSelectedStrategyGoalId = ""
    
    //Related to IdeaAndProblem
    var arr_actionIdeaAndProblem:[RelatedToIdeaAndProblem] = []
    var strSelectedIdeaAndProblemID = ""
    var typeIdeaAndProblem = IdeaAndProblemType.idea
    
    var selectionRelatedToDueDate = ""
    var selectionRelatedToDueDateColor = ""
    var selectionRelatedToActionID = ""
    var isStatusArchive = false

    var cellType:[AddActionModel.celltype] = [.header,.createdBy,.title,.relatedToGeneral,.tag,.company,.assignTo,.department,.approvedBy,.createdDate,.dueDate,.workDate,.duration,.subtask,.attachment,.bottom]

    
    enum categorySelectionType:Int {
        case category            = 1
        case company             = 2
        case assignTo            = 3
        case department          = 4
        case subCategory         = 5
        case consequenceCategory = 6
    }
    enum dateSelctionType:Int {
        case due                = 2
        case resived            = 3
    }
    
    //MARK:- LifeCyle
    init(theController:AddRiskVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
         (theController.view as? AddRiskView)?.btnSelectCompanyOutlet.isUserInteractionEnabled = false
         }*/
        if isEdit {
            (theController.view as? AddRiskView)?.btnSelectCompany.isUserInteractionEnabled = false
        } else {
            if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
                (theController.view as? AddRiskView)?.btnSelectCompany.isUserInteractionEnabled = false
            }
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService()
            
        } else {
            strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            selectedData["company"].stringValue = Utility.shared.userData.companyName
            (theController.view as? AddRiskView)?.lblCompany.text = Utility.shared.userData.companyName
            (theController.view as? AddRiskView)?.viewCompany.isHidden = true
            assignUserListService()
        }
    }
    
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        (theController.view as? AddRiskView)?.activitySpinner.hidesWhenStopped = true
        (theController.view as? AddRiskView)?.constrainViewPagerHeight.constant = 40.0
        (theController.view as? AddRiskView)?.viewOfPager.isHidden = false
        
        if isAnimating {
            (theController.view as? AddRiskView)?.activitySpinner.startAnimating()
            (theController.view as? AddRiskView)?.viewMore.isHidden = true
        } else {
            (theController.view as? AddRiskView)?.activitySpinner.stopAnimating()
            (theController.view as? AddRiskView)?.viewMore.isHidden = false
            
            if nextOffset == -1 {
                (theController.view as? AddRiskView)?.viewOfPager.isHidden = true
                (theController.view as? AddRiskView)?.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    
    func showTheEditData() {
        guard let theModel = theEditRiskModel else { return }
        (theController.view as? AddRiskView)?.lblAddTitle.text = "Risk"
        selectedData["unwantedevent"].stringValue = theModel.unwanted_event
        selectedData["description"].stringValue = theModel.risk_description
        selectedData["scopeBoundries"].stringValue = theModel.scopeBoundries
        selectedData["company"].stringValue = theModel.company_name
        selectedData["assignto"].stringValue = theModel.assigned_to
        selectedData["department"].stringValue = theModel.department_name
        selectedData["createdby"].stringValue = theModel.created_by
        selectedData["duedate"].stringValue = theModel.duedate
        selectedData["revisedduedate"].stringValue = theModel.revisedDate
        selectedData["category"].stringValue = theModel.riskCategoryName
        selectedData["subcategory"].stringValue = theModel.riskSubCategoryName
        selectedData["consequenceCategory"].stringValue = theModel.riskConsequenceCategoryName
        selectedData["level"].stringValue = theModel.level
        selectedData["residualLevel"].stringValue = theModel.residualRiskRating
        selectedData["relatedto"].stringValue = theModel.label
    
        selectedDueDate = theModel.duedate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let dueDate = selectedDueDate {
            selectedData["duedate"].stringValue = dueDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        selectedrevisedDate = theModel.revisedDate.convertToDate(formate: DateFormatterInputType.inputType1)
        if let revisedDate = selectedrevisedDate {
            selectedData["revisedduedate"].stringValue = revisedDate.string(withFormat: DateFormatterOutputType.outputType7)
        }
        let createdDate = theModel.created_date.convertToDate(formate: DateFormatterInputType.inputType1)
        if let createdDates = createdDate {
            selectedData["createdDate"].stringValue = createdDates.string(withFormat: DateFormatterOutputType.outputType7)
        }
        
        strSelectedCategoryID = theModel.riskCategoryId
        strSelectedCompanyID = theEditRiskModel?.company_id ?? Utility.shared.userData.companyId
        strSelectedUserAssignID = theModel.assignee_id
        strSelectedDepartmentID = theModel.department_id
        strSelectedSubCategoryID = theModel.riskSubcategoryId
        strSelectedConsequenceID = theModel.riskConsequenceCategoryId
        strSelectedRelatedId = "\(theModel.relatedId)"
        
        (theController.view as? AddRiskView)?.txtTitle.isUserInteractionEnabled = false
        (theController.view as? AddRiskView)?.txtDescription.isUserInteractionEnabled = false
        (theController.view as? AddRiskView)?.txtScopBoundriesDescription.isUserInteractionEnabled = false

        (theController.view as? AddRiskView)?.showUpdateData(theModel: selectedData)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionDueDate)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectedCompany)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionAssignTo)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionDepartment)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionCategory)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionLevel)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionSubCategory)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionConsequenceCategory)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewSelectionResidualRiskLevel)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewRelatedToTxt)
        (theController.view as? AddRiskView)?.isEnableView(isEnable: false, view: (theController.view as? AddRiskView)!.viewCommonRelatedToTxt)
    
        (theController.view as? AddRiskView)?.lblCommanRelatedToTitle.text = theModel.label
        (theController.view as? AddRiskView)?.lblRelatedTo.text = theModel.label
        (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = theModel.related_title
        (theController.view as? AddRiskView)?.viewCommanRelatedTo.isHidden = false
        if theModel.relatedId == 0 {
            (theController.view as? AddRiskView)?.viewCommanRelatedTo.isHidden = true
        }
        DispatchQueue.main.async { [weak self] in
            (self?.theController.view as? AddRiskView)?.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        DispatchQueue.main.async { [weak self] in
            (self?.theController.view as? AddRiskView)?.txtScopBoundriesDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        arr_selectedFile.removeAll()
        for item in theModel.voiceNotes {
            let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        for item in theModel.attachment {
            let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
            arr_selectedFile.append(attchment)
        }
        if UserDefault.shared.isCanEditForm(strOppoisteID: theModel.created_by_id) {
            (theController.view as? AddRiskView)?.txtTitle.isUserInteractionEnabled = true
            (theController.view as? AddRiskView)?.txtDescription.isUserInteractionEnabled = true
            (theController.view as? AddRiskView)?.txtScopBoundriesDescription.isUserInteractionEnabled = true
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionCategory)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionLevel)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionAssignTo)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionDepartment)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionSubCategory)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionConsequenceCategory)
            (theController.view as? AddRiskView)?.isEnableView(isEnable: true, view: (theController.view as? AddRiskView)!.viewSelectionResidualRiskLevel)

        }
        
    }
   
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, index:Int)  {
        switch categorySelection {
        case .category:
            strSelectedCategoryID = arr_categoryList[index].id
            print("Category id :- \(strSelectedCategoryID )")
            selectedData["category"].stringValue = str
            strSelectedSubCategoryID = ""
            selectedData["subcategory"].stringValue = ""
            arr_subcategoryList.removeAll()
            riskSubCategoiesListService()
            break
        case .subCategory:
            strSelectedSubCategoryID = arr_subcategoryList[index].id
            print("Sub Category id :- \(strSelectedSubCategoryID )")
            selectedData["subcategory"].stringValue = str
            break
        case .consequenceCategory:
            strSelectedConsequenceID = arr_consequenceList[index].id
            print("Consequence Category id :- \(strSelectedConsequenceID)")
            selectedData["consequenceCategory"].stringValue = str
            break
        case .company:
            strSelectedCompanyID = arr_companyList[index].id
            assignUserListService()
            
            selectedData["company"].stringValue = str
            selectedData["assignto"].stringValue = ""
            strSelectedUserAssignID = ""
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            strSelectedCategoryID = ""
            selectedData["category"].stringValue = ""
            strSelectedSubCategoryID = ""
            selectedData["subcategory"].stringValue = ""
            riskCategoiesListService()
            
            strSelectedConsequenceID = ""
            selectedData["consequenceCategory"].stringValue = ""
            riskConsequenceCategoiesListService()
            
            break
        case .assignTo:
            // strSelectedUserAssignID = arr_assignList[index].id
            strSelectedUserAssignID = arr_assignList[index].user_id
            selectedData["assignto"].stringValue = str
            
            departmentListService()
            selectedData["department"].stringValue = ""
            strSelectedDepartmentID = ""
            
            /*var isEdit = false
             if !isEdit {
                
                isEdit = true
                
                //                isDropDownLoadingAtIndex = 6
            } else {
                if let theModel = theEditRiskModel, UserDefault.shared.isCanEditForm(strOppoisteID: theModel.created_by_id){
                    departmentListService()
                    selectedData["department"].stringValue = ""
                    strSelectedDepartmentID = ""
                    isEdit = true
                }
            }*/
 
            break
        case .department:
            strSelectedDepartmentID = arr_departmentList[index].id
            selectedData["department"].stringValue = str
            break            
        }
        (theController.view as? AddRiskView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateDropDownData(categorySelection:AddActionVC.categorySelectionType, str:String, cellType:AddActionModel.celltype, index:Int)  {
        var index = 0
        if let ind = self.cellType.firstIndex(where: {$0 == cellType}) {
            index = ind
        }
        
        print("selected index \(index)")
        switch categorySelection {
        case .company:
            break
        case .department:
            break
        case .assignTo:
            break
        case .relatedTo:
            if let selectedIndex = arr_relatedList.firstIndex(where: {$0.option == str}) {
                strSelectedRelatedId = "\(arr_relatedList[selectedIndex].value)"
            }
            resetRelatedData()
            removeRelatedCellFromCelltype()
            updateViewCommanRelatedTo(str: str)
            
            self.selectedData["relatedto"].stringValue = str
            (theController.view as? AddRiskView)?.lblRelatedTo.text = str
            return
        case .approvedBy:
            break
        case .relatedToStrategyPrimaryArea:
            if let selectedIndex = arr_actionPrimaryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedPrimaryAreaId = arr_actionPrimaryAreaList[selectedIndex].id
            }
            self.selectedData["strategyprimaryarea"].stringValue = str
            (theController.view as? AddRiskView)?.lblPrimaryArea.text = str
            strSelectedSecondryAreaId = ""
            self.selectedData["strategysecondaryarea"].stringValue = ""
            (theController.view as? AddRiskView)?.lblSecondaryArea.text = "Select secondary area"
            arr_actionSecondryAreaList.removeAll()
            
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddRiskView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            
            actionSecondaryAreaListService()
           
            
            break
        case .relatedToStrategySecondaryArea:
            if let selectedIndex = arr_actionSecondryAreaList.firstIndex(where: {$0.title == str}) {
                strSelectedSecondryAreaId = arr_actionSecondryAreaList[selectedIndex].id
            }
            self.selectedData["strategysecondaryarea"].stringValue = str
            (theController.view as? AddRiskView)?.lblSecondaryArea.text = str
            strSelectedStrategyGoalId = ""
            self.selectedData["strategygoal"].stringValue = ""
            (theController.view as? AddRiskView)?.lblStrategyGoal.text = "Select strategy goal"
            arr_actionStrategyGoalList.removeAll()
            actionStrategyGoalListService()
            break
        case .relatedToStrategyGoal:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionStrategyGoalList.firstIndex(where: {$0.title == str}) {
                strSelectedStrategyGoalId = arr_actionStrategyGoalList[selectedIndex].id
                (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = false
                selectionRelatedToDueDate = arr_actionStrategyGoalList[selectedIndex].duedate
                selectionRelatedToActionID = arr_actionStrategyGoalList[selectedIndex].id
                if UserDefault.shared.isArchiveAction(strStatus: arr_actionStrategyGoalList[selectedIndex].status) {
                    (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = false
                    (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = false
                } else {
                    (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.isUserInteractionEnabled = true
                    (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = true
                }
                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: (view.lblStrategyGoalRelatedToDueDate), strDueDate: arr_actionStrategyGoalList[selectedIndex].duedate, color: arr_actionStrategyGoalList[selectedIndex].duedateColor)
                }
            }
            self.selectedData["strategygoal"].stringValue = str
            (theController.view as? AddRiskView)?.lblStrategyGoal.text = str
            break
        case .relatedToRisk:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            //            let arr_list:[String] = arr_actionRiskList.map({$0.unwanted_event })
            if let selectedIndex = arr_actionRiskList.firstIndex(where: {$0.unwanted_event == str}) {
                strSelectedRiskId = arr_actionRiskList[selectedIndex].id
                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToDueDate = arr_actionRiskList[selectedIndex].duedate
                selectionRelatedToActionID = arr_actionRiskList[selectedIndex].id
                (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: arr_actionTacticalProjectList[selectedIndex].status)

                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionRiskList[selectedIndex].duedate, color: arr_actionRiskList[selectedIndex].duedateColor)
                }
                
            }
            selectedData["relatedtorisk"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str

            break
        case .relatedToTacticalProject:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionTacticalProjectList.firstIndex(where: {$0.project_name == str}) {
                strSelectedTacticalProjectId = arr_actionTacticalProjectList[selectedIndex].id
                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToDueDate = arr_actionTacticalProjectList[selectedIndex].duedate
                selectionRelatedToActionID = arr_actionTacticalProjectList[selectedIndex].id
                (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: arr_actionTacticalProjectList[selectedIndex].status)

                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionTacticalProjectList[selectedIndex].duedate, color: arr_actionTacticalProjectList[selectedIndex].duedateColor)
                }
            }
            selectedData["relatedtotacticalproject"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str

            break
        case .relatedToFocus:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionFocusList.firstIndex(where: {$0.title == str}) {
                strSelectedFocusId = arr_actionFocusList[selectedIndex].id
                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToActionID = arr_actionFocusList[selectedIndex].id
                selectionRelatedToDueDate = arr_actionFocusList[selectedIndex].duedate
                (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isArchiveAction(strStatus: arr_actionFocusList[selectedIndex].status)

                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionFocusList[selectedIndex].duedate, color: arr_actionFocusList[selectedIndex].duedateColor)
                }
            }
            selectedData["relatedtofocus"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str
            break
        case .relatedToSuccessFactor:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionSuccessFactorList.firstIndex(where: {$0.criticalSuccessFactorName == str}) {
                strSelectedSuccessFactorId = arr_actionSuccessFactorList[selectedIndex].id
//                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToActionID = arr_actionSuccessFactorList[selectedIndex].id
//                selectionRelatedToDueDate = arr_actionSuccessFactorList[selectedIndex].duedate
                /*if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionFocusList[selectedIndex].duedate, color: arr_actionFocusList[selectedIndex].duedateColor)
                }*/
            }
            selectedData["relatedtosuccessfactor"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str
            break
        case .selectTag:
            break
        case .relatedToIdea:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionIdeaAndProblem.firstIndex(where: {$0.title == str}) {
                strSelectedIdeaAndProblemID = arr_actionIdeaAndProblem[selectedIndex].id
                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToActionID = arr_actionIdeaAndProblem[selectedIndex].id
                (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isIdeaAndProblemArchive(strStatus: arr_actionIdeaAndProblem[selectedIndex].status)

                selectionRelatedToDueDate = arr_actionIdeaAndProblem[selectedIndex].duedate
                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionIdeaAndProblem[selectedIndex].duedate, color: arr_actionIdeaAndProblem[selectedIndex].duedateColor)
                }
            }
            selectedData["relatedtoidea"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str

            break
        case .relatedToProblem:
            (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
            (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
            (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
            
            if let selectedIndex = arr_actionIdeaAndProblem.firstIndex(where: {$0.title == str}) {
                strSelectedIdeaAndProblemID = arr_actionIdeaAndProblem[selectedIndex].id
                (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = false
                selectionRelatedToActionID = arr_actionIdeaAndProblem[selectedIndex].id
                selectionRelatedToDueDate = arr_actionIdeaAndProblem[selectedIndex].duedate
                (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.isUserInteractionEnabled = !UserDefault.shared.isIdeaAndProblemArchive(strStatus: arr_actionIdeaAndProblem[selectedIndex].status)

                if let view = (theController.view as? AddRiskView) {
                    view.updateDateView(label: view.lblCommanRelatedToDueDate, strDueDate: arr_actionIdeaAndProblem[selectedIndex].duedate, color: arr_actionIdeaAndProblem[selectedIndex].duedateColor)
                }
                
            }
            selectedData["relatedtoproblem"].stringValue = str
            (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = str

            break
        }
        
        //        self.(theController.view as? AddRiskView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func resetRelatedData(){
        selectedData["relatedtotacticalproject"].stringValue = ""
        strSelectedTacticalProjectId = ""
        (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = "Select"
        
        selectedData["relatedtofocus"].stringValue = ""
        strSelectedFocusId = ""
        
        selectedData["relatedtosuccessfactor"].stringValue = ""
        strSelectedSuccessFactorId = ""
        
        selectedData["relatedtorisk"].stringValue = ""
        strSelectedRiskId = ""
        
        self.selectedData["strategygoal"].stringValue = ""
        strSelectedStrategyGoalId = ""
        (theController.view as? AddRiskView)?.lblStrategyGoal.text = "Select strategy goal"
        
        self.selectedData["strategysecondaryarea"].stringValue = ""
        strSelectedSecondryAreaId = ""
        (theController.view as? AddRiskView)?.lblSecondaryArea.text = "Select secondary area"
        
        self.selectedData["strategyprimaryarea"].stringValue = ""
        strSelectedPrimaryAreaId = ""
        (theController.view as? AddRiskView)?.lblPrimaryArea.text = "Select primary area"
        
        self.selectedData["relatedtoidea"].stringValue = ""
        self.selectedData["relatedtoproblem"].stringValue = ""
        strSelectedIdeaAndProblemID = ""
        //        (theController.view as? AddRiskView)?.lblPrimaryArea.text = "Select Idea"
        
        arr_actionTacticalProjectList = []
        arr_actionFocusList = []
        arr_actionStrategyGoalList = []
        arr_actionPrimaryAreaList = []
        arr_actionSecondryAreaList = []
        arr_actionIdeaAndProblem = []
    }
    
    func removeRelatedCellFromCelltype() {
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToRisk}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToTacticalProject}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToFocus}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToSuccessFactor}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToIdea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.relatedToProblem}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.primaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.secondaryArea}) {
            cellType.remove(at: index)
        }
        if let index = cellType.firstIndex(where: {$0 == AddActionModel.celltype.strategyGoal}) {
            cellType.remove(at: index)
        }
        
    }
    
    func updateViewCommanRelatedTo(str:String) {
        (theController.view as? AddRiskView)?.lblCommanRelatedToTitle.text = str
        (theController.view as? AddRiskView)?.lblCommanRelatedTo.text = "Select " + str
        (theController.view as? AddRiskView)?.lblCommanRelatedToTitle.showAstrike = true
        (theController.view as? AddRiskView)?.viewCommanRelatedTo.isHidden = "General Risk".lowercased() == str.lowercased()
        (theController.view as? AddRiskView)?.viewPrimaryArea.isHidden = true
        (theController.view as? AddRiskView)?.viewSecondaryArea.isHidden = true
        (theController.view as? AddRiskView)?.viewStrategyGoal.isHidden = true
        selectionRelatedToDueDate = ""
        selectionRelatedToActionID = ""
        (theController.view as? AddRiskView)?.lblStrategyGoalRelatedToDueDate.text = ""
        (theController.view as? AddRiskView)?.lblCommanRelatedToDueDate.text = ""
        (theController.view as? AddRiskView)?.viewCommonRelatedToDueDate.isHidden = true
        (theController.view as? AddRiskView)?.viewStrategyGoalRelatedToDueDate.isHidden = true
        

        switch str.localizedLowercase {
        case AddActionModel.relatedToSelectionType.general.rawValue.localizedLowercase, "General Risk".localizedLowercase:
            (theController.view as? AddRiskView)?.viewCommanRelatedTo.isHidden = true
            relatedToSelectionType = AddActionModel.relatedToSelectionType.general
            break
        case AddActionModel.relatedToSelectionType.risk.rawValue.localizedLowercase:
            actionRiskListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.risk
            //            cellType.insert(AddActionModel.celltype.relatedToRisk, at: 4)
            break
        case AddActionModel.relatedToSelectionType.tactical.rawValue.localizedLowercase:
            actionTacticalProjectListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.tactical
            
            //            cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.focus.rawValue.localizedLowercase:
            actionFocusListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.focus
            
            //            cellType.insert(AddActionModel.celltype.relatedToFocus, at: 4)
            break
        case AddActionModel.relatedToSelectionType.successFactor.rawValue.localizedLowercase:
            actionSuccessFactorListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.successFactor
            
            //            cellType.insert(AddActionModel.celltype.relatedToFocus, at: 4)
            break
        case AddActionModel.relatedToSelectionType.idea.rawValue.localizedLowercase:
            typeIdeaAndProblem = .idea
            actionIdeaAndProblemListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.idea
            
            //            cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.problem.rawValue.localizedLowercase:
            typeIdeaAndProblem = .problem
            actionIdeaAndProblemListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.problem
            
            //            cellType.insert(AddActionModel.celltype.relatedToTacticalProject, at: 4)
            break
        case AddActionModel.relatedToSelectionType.strategy.rawValue.localizedLowercase:
            actionPrimaryAreaListService()
            relatedToSelectionType = AddActionModel.relatedToSelectionType.strategy
            (theController.view as? AddRiskView)?.viewCommanRelatedTo.isHidden = true
            (theController.view as? AddRiskView)?.viewPrimaryArea.isHidden = false
            (theController.view as? AddRiskView)?.viewSecondaryArea.isHidden = false
            (theController.view as? AddRiskView)?.viewStrategyGoal.isHidden = false
            //            cellType.insert(AddActionModel.celltype.primaryArea, at: 4)
            //            cellType.insert(AddActionModel.celltype.secondaryArea, at: 5)
            //            cellType.insert(AddActionModel.celltype.strategyGoal, at: 6)
            break
        default:
            break
        }
    }
    
    func updateDate(strDate:String,type:dateSelctionType,date:Date) {
        switch type {
        case .due:
            selectedDueDate = date
            selectedData["duedate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddRiskModel.celltype.dueDate}) {
//                self.(theController.view as? AddRiskView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
        //            (theController.view as? AddRiskView)?.tableView.reloadRows(at: [IndexPath.init(row: 7, section: 0)], with: UITableView.RowAnimation.none)
        case .resived:
            selectedrevisedDate = date
            selectedData["revisedduedate"].stringValue = strDate
//            if let index = tableviewCellType.firstIndex(where: {$0 == AddRiskModel.celltype.reviseDate}) {
//                self.(theController.view as? AddRiskView)?.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
//            }
            //            (theController.view as? AddRiskView)?.tableView.reloadRows(at: [IndexPath.init(row: 8, section: 0)], with: UITableView.RowAnimation.none)
        }
        (theController.view as? AddRiskView)?.showDropDownUpdatedData(theModel: selectedData)
    }
    
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String, isGoal:Bool) {
        switch type {
        case .due:
            //            selectedDueDate = date
            //            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
            //            selectedrevisedDate = date
            //            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate, isGoal: isGoal)
    }
    
    func updateRiskDetailModel(theModel:RiskDetail) {
        if let revisedDate1 = selectedrevisedDate {
            theEditRiskModel?.revisedDate = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        theEditRiskModel?.unwanted_event = selectedData["unwantedevent"].stringValue
        theEditRiskModel?.risk_description = selectedData["description"].stringValue
        
        theEditRiskModel = theModel
    }
    
    func validateForm() {
        theController.view.endEditing(true)
        if selectedData["unwantedevent"].stringValue.trimmed().isEmpty {
            self.theController.showAlertAtBottom(message: "Please enter unwanted title")
            return
        } else if selectedData["company"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select company")
            return
        } else if selectedData["relatedto"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select related to")
            return
        } else if !selectedData["relatedto"].stringValue.isEmpty, !isEdit {
            if strSelectedRelatedId == "1"{
                if selectedData["strategyprimaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select primary area")
                    return
                } else if selectedData["strategysecondaryarea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select secondry area")
                    return
                } else if selectedData["strategygoal"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select strategy goal")
                    return
                }
            } else if strSelectedRelatedId == "2"{
                if selectedData["relatedtorisk"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select risk")
                    return
                }
            } else if strSelectedRelatedId == "3"{
                if selectedData["relatedtotacticalproject"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select tactical project")
                    return
                }
            } else if strSelectedRelatedId == "4"{
                if selectedData["relatedtofocus"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select focus")
                    return
                }
            } else if strSelectedRelatedId == "6"{
                if selectedData["relatedtosuccessfactor"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select success factor")
                    return
                }
            } else if strSelectedRelatedId == "7" {
                if selectedData["relatedtoidea"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select idea")
                    return
                }
            } else if strSelectedRelatedId == "8" {
                if selectedData["relatedtoproblem"].stringValue.isEmpty {
                    self.theController.showAlertAtBottom(message: "Please select problem")
                    return
                }
            }
        }
       
        if selectedData["assignto"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select assigned to")
            return
        } else if selectedData["department"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select department")
            return
        } else if selectedData["category"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select category")
            return
        } else if selectedData["subcategory"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select sub category")
            return
        } else if selectedData["consequenceCategory"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select consequence")
            return
        } else if selectedData["level"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select inherent risk")
            return
        } else if selectedData["residualLevel"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select residual risk")
            return
        } else if selectedData["duedate"].stringValue.isEmpty {
            self.theController.showAlertAtBottom(message: "Please select due date")
            return
        } else if !selectedData["revisedduedate"].stringValue.isEmpty {
            if let dueDate1 = selectedDueDate, let revisedDate1 = selectedrevisedDate,revisedDate1 < dueDate1 {
                self.theController.showAlertAtBottom(message: "Revised date should be grater then due date")
                return
            }
        }
        
        var dueDate = ""
        var revisedDate = ""
        
        if let dueDate1 = selectedDueDate {
            dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let revisedDate1 = selectedrevisedDate {
            revisedDate = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var removeAttachment:[String] = []
        
        var voiceCount = 0
        var fileCount = 0
        
        for item in arr_RemoveSelectedFile {
            if item.isOldAttachment {
                removeAttachment.append(item.attachmentID)
            }
            //            removeFile.append([APIKey.key_id:item.attachmentID])
        }
        
        for i in 0..<arr_selectedFile.count {
            if arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:arr_selectedFile[i].attachmentID])
            } else {
                if !arr_selectedFile[i].isOldAttachment && (arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
//        var param = parameter
//        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")
        
        if isEdit {
            let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_risk_id:theEditRiskModel?.risk_id ?? "",
                             APIKey.key_scope_boundries: selectedData["scopeBoundries"].stringValue.trimmed(),
                             APIKey.key_unwanted_event:selectedData["unwantedevent"].stringValue.trimmed(),
                             APIKey.key_category:strSelectedCategoryID,
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned_to:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_level:selectedData["level"].stringValue,
                             APIKey.key_risk_category:strSelectedCategoryID,
                             APIKey.key_risk_subcategory:strSelectedSubCategoryID,
                             APIKey.key_residual_risk_rating:selectedData["residualLevel"].stringValue,
                             APIKey.key_risk_consequence_category:strSelectedConsequenceID,
                             APIKey.key_revised_date:revisedDate,
                             APIKey.key_description:selectedData["description"].stringValue.trimmed(),
                             APIKey.key_remove_attechment:removeAttachment.joined(separator: ",")] as [String : Any]
            
            updateRisk(parameter: parameter,fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        } else {
            var notes = ""
            
            if let history = arr_notesHistoryList, history.count > 0 {
                var noteHistory:[[String:String]] = []
                history.forEach { (item) in
                    noteHistory.append([APIKey.key_notes:item.notesHistoryDescription])
                }
                notes = JSON(noteHistory).rawString() ?? ""
            }
            
            var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_unwanted_event:selectedData["unwantedevent"].stringValue.trimmed(),
                             APIKey.key_scope_boundries: selectedData["scopeBoundries"].stringValue.trimmed(),
//                             APIKey.key_category:strSelectedCategoryID,
                             APIKey.key_related_to:strSelectedRelatedId,
                             APIKey.key_company_id:strSelectedCompanyID,
                             APIKey.key_assigned_to:strSelectedUserAssignID,
                             APIKey.key_department_id:strSelectedDepartmentID,
                             APIKey.key_level:selectedData["level"].stringValue,
                             APIKey.key_risk_category:strSelectedCategoryID,
                             APIKey.key_risk_subcategory:strSelectedSubCategoryID,
                             APIKey.key_residual_risk_rating:selectedData["residualLevel"].stringValue,
                             APIKey.key_risk_consequence_category:strSelectedConsequenceID,
                             APIKey.key_notes:notes,
                             APIKey.key_duedate:dueDate,
                             APIKey.key_description:selectedData["description"].stringValue.trimmed()] as [String : Any]
            
            if strSelectedRelatedId == "4"{
                parameter[APIKey.key_goal]  = strSelectedFocusId
            } else if strSelectedRelatedId == "6"{
                parameter[APIKey.key_success_factor]  = strSelectedSuccessFactorId
            } else if strSelectedRelatedId == "3"{
                parameter[APIKey.key_tactical_projects] = strSelectedTacticalProjectId
            } else if strSelectedRelatedId == "2"{
                parameter[APIKey.key_risk]  = strSelectedRiskId
            } else if strSelectedRelatedId == "1"{
                parameter[APIKey.key_strategy_goal]  = strSelectedStrategyGoalId
            } else if strSelectedRelatedId == "7"{
                parameter[APIKey.key_idea]  = strSelectedIdeaAndProblemID
            } else if strSelectedRelatedId == "8"{
                parameter[APIKey.key_problem] = strSelectedIdeaAndProblemID
            }
            addRisk(parameter: parameter, fileData: fileData, withName: withName, withFileName: withFileName, fileMimeType: fileMimeType)
        }
        
    }
}

//MARK:- Api Call
extension AddRiskViewModel {
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String, isGoal:Bool) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        switch relatedToSelectionType {
        case .general:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_action_logs
            break
        case .strategy:
            parameter[APIKey.key_date_for] = APIKey.key_strategy_goals
            break
        case .risk:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_risks
            break
        case .tactical:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_tactical_projects
            break
        case .focus:
            parameter[APIKey.key_date_for] = APIKey.key_assign_to_for_focuses
            break
        case .idea:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
        case .problem:
            parameter[APIKey.key_date_for] = APIKey.key_status_for_ideas_and_problems
            break
            
        case .successFactor:
            break
        }
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        Utility.shared.showActivityIndicator()
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            if let obj = self, let model = theModel {
                obj.selectionRelatedToDueDate = model.revisedDate
                if let view = (obj.theController.view as? AddRiskView) {
                    view.updateDateView(label: isGoal ? view.lblStrategyGoalRelatedToDueDate : view.lblCommanRelatedToDueDate, strDueDate: model.revisedDate, color: model.revisedColor)
                }
                if obj.isForAddAction {
                    obj.theController.handlerEditDueDateChanged(model.revisedDate,model.revisedColor)
                }
            }
            Utility.shared.stopActivityIndicator()
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
                self?.theController.showAlertAtBottom(message: error)
        })
        
    }
    func categoiesListService() {
        (theController.view as? AddRiskView)?.loadCategoryActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_category_for:CategoryFor.risk.rawValue]
        
        CommanListWebservice.shared.getCategories(parameter: parameter, success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadCategoryActivity(isStart: false)
//            self?.arr_categoryList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadCategoryActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func riskCategoiesListService() {
        (theController.view as? AddRiskView)?.loadCategoryActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        AddRiskWebService.shared.getRiskCategories(parameter: parameter, success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadCategoryActivity(isStart: false)
            self?.arr_categoryList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadCategoryActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func riskSubCategoiesListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button:(theController.view as? AddRiskView)?.btnSubCategory , isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID,
                         APIKey.key_risk_category:strSelectedCategoryID]
        AddRiskWebService.shared.getRiskSubCategories(parameter: parameter, success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button:(self?.theController.view as? AddRiskView)?.btnSubCategory , isStart: false)
            self?.arr_subcategoryList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button:(self?.theController.view as? AddRiskView)?.btnSubCategory , isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func riskConsequenceCategoiesListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button:(theController.view as? AddRiskView)?.btnConsequenceCategory , isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        AddRiskWebService.shared.getRiskConsequenceCategories(parameter: parameter, success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button:(self?.theController.view as? AddRiskView)?.btnConsequenceCategory , isStart: false)
            self?.arr_consequenceList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button:(self?.theController.view as? AddRiskView)?.btnConsequenceCategory , isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func companylistService() {
        (theController.view as? AddRiskView)?.loadCompanyActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            //            print("list:=",list)
            //            print("list:=",list[0].department, list[0].name)
            self?.arr_companyList = list
            (self?.theController.view as? AddRiskView)?.loadCompanyActivity(isStart: false)
            }, failed: { [weak self](error) in
                (self?.theController.view as? AddRiskView)?.loadCompanyActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    func assignUserListService() {
        (theController.view as? AddRiskView)?.loadAssignToActivity(isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_assignList = list
//            self?.theController.updateTableForDropDownStopLoading()
            (self?.theController.view as? AddRiskView)?.loadAssignToActivity(isStart: false)

            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadAssignToActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    func departmentListService() {
        (theController.view as? AddRiskView)?.loadDepartmentActivity(isStart: true)

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.arr_departmentList = list
            (self?.theController.view as? AddRiskView)?.loadDepartmentActivity(isStart: false)

//            self?.theController.updateTableForDropDownStopLoading()
            if let obj = self, !obj.isEdit, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddRiskView)?.showDropDownUpdatedData(theModel: obj.selectedData)
//                obj.theController.updateDropDownData(categorySelection: .department, str: list[0].name, index: 0)
            } else if let obj = self, obj.isEdit, obj.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.selectedData["department"].stringValue = list[0].name
                obj.strSelectedDepartmentID = list[0].id
                (obj.theController.view as? AddRiskView)?.showDropDownUpdatedData(theModel: obj.selectedData)
            }
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadDepartmentActivity(isStart: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateTableForDropDownStopLoading()
        })
    }
    
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theEditRiskModel?.risk_id ?? "",
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_assign_to_for_risks,
                         APIKey.key_page_offset:"\(nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            (self?.theController.view as? AddRiskView)?.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)
        })
    }
    
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theEditRiskModel?.risk_id ?? "",
                         APIKey.key_history_for: APIKey.key_assign_to_for_risks]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }
    
    func addRisk(parameter:[String:Any], fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddRiskView)?.isClickOnAdd(isLoading: true)
        AddRiskWebService.shared.postAddRisk(parameter: parameter, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
            (self?.theController.view as? AddRiskView)?.isClickOnAdd(isLoading: false)
            self?.theController.backAction(isAnimation: false)
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateBottomCell()
        })
    }
    
    func updateRisk(parameter:[String:Any],fileData: [Data], withName: [String], withFileName: [String], fileMimeType: [String]) {
        (theController.view as? AddRiskView)?.isClickOnAdd(isLoading: true)
        AddRiskWebService.shared.postEditRisk(parameter: parameter,files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (theDetailModel) in
            if !theDetailModel.revisedDate.isEmpty {
                theDetailModel.duedate = theDetailModel.revisedDate
                theDetailModel.duedateColor = theDetailModel.revisedColor
            }
            self?.updateRiskDetailModel(theModel:theDetailModel)
            (self?.theController.view as? AddRiskView)?.isClickOnAdd(isLoading: false)
            self?.theController.handlerUpdateModel(self?.theEditRiskModel)
            self?.theController.backAction(isAnimation: false)
            }, failed: {[weak self] (error) in
                (self?.theController.view as? AddRiskView)?.isClickOnAdd(isLoading: false)
                self?.theController.showAlertAtBottom(message: error)
//                self?.theController.updateBottomCell()
        })
    }
    
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.strNoteStatus ?? "")
            (self?.theController.view as? AddRiskView)?.txtNote.text = ""
            completion()
            }, failed: { [weak self] (error) in
                completion()
                self?.theController.showAlertAtBottom(message: error)
        })
    }
    
    
    func relatedListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedTo, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_related_type:APIKey.key_risks]
        ActionsWebService.shared.getRelatedTo(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedTo, isStart: false)
            self?.arr_relatedList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedTo, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionRiskListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedRiskList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
            self?.arr_actionRiskList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionTacticalProjectListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedTacticalProjectList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
            self?.arr_actionTacticalProjectList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionFocusListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedFocusList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
            self?.arr_actionFocusList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSuccessFactorListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getRelatedSuccessFactorList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
            self?.arr_actionSuccessFactorList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionPrimaryAreaListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnPrimaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getPrimaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnPrimaryArea, isStart: false)
            self?.arr_actionPrimaryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnPrimaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionSecondaryAreaListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnSecondaryArea, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_primary_area_id:strSelectedPrimaryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getSecondaryAreaList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnSecondaryArea, isStart: false)
            self?.arr_actionSecondryAreaList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnSecondaryArea, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionStrategyGoalListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnStrategyGoal, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_secondary_area_id:strSelectedSecondryAreaId,
                         APIKey.key_company_id:Utility.shared.userData.companyId]
        ActionsWebService.shared.getStrategyGoalList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnStrategyGoal, isStart: false)
            self?.arr_actionStrategyGoalList = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnStrategyGoal, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
    
    func actionIdeaAndProblemListService() {
        (theController.view as? AddRiskView)?.loadActivityOnButton(button: (theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: true)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:Utility.shared.userData.companyId,
                         APIKey.key_related_to:typeIdeaAndProblem.rawValue]
        ActionsWebService.shared.getRelatedIdeaAndProblemList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
            self?.arr_actionIdeaAndProblem = list
            }, failed: { [weak self] (error) in
                (self?.theController.view as? AddRiskView)?.loadActivityOnButton(button: (self?.theController.view as? AddRiskView)?.btnRelatedOutlet, isStart: false)
                //                self?.showAlertAtBottom(message: error)
        })
    }
}
