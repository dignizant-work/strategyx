//
//  AddRiskView.swift
//  StrategyX
//
//  Created by Haresh on 04/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import RxCocoa

class AddRiskView: ViewParentWithoutXIB {
    //MARK:- IBOutlet

    @IBOutlet weak var lblAddTitle: UILabel!
    @IBOutlet weak var img_Bg: UIImageView!
    
    @IBOutlet weak var tblNotesHistory: UITableView!
    @IBOutlet weak var heightOfNotesHistory: NSLayoutConstraint!
    
    @IBOutlet weak var lblScopeBoundriesPlaceHolder: UILabel!
    @IBOutlet weak var txtScopBoundriesDescription: UITextView!
    
    @IBOutlet weak var lblDesriptionTitle: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var viewSelectedCompany: UIView!
    @IBOutlet weak var btnSelectCompany: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var viewCompany: UIView!
    
    @IBOutlet weak var viewSelectionAssignTo: UIView!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var btnAssigned: UIButton!
    
    @IBOutlet weak var viewSelectionDepartment: UIView!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var btnDepartment: UIButton!
    
    @IBOutlet weak var viewSelectionCategory: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var viewSelectionSubCategory: UIView!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var btnSubCategory: UIButton!
    
    @IBOutlet weak var viewSelectionConsequenceCategory: UIView!
    @IBOutlet weak var lblConsequenceCategory: UILabel!
    @IBOutlet weak var btnConsequenceCategory: UIButton!
    
    @IBOutlet weak var viewSelectionLevel: UIView!
    @IBOutlet weak var lblSelectedLevel: UILabel!
    @IBOutlet weak var viewLevel: UIView!
    @IBOutlet weak var lblLevel: UILabel!
    
    @IBOutlet weak var viewSelectionResidualRiskLevel: UIView!
    @IBOutlet weak var lblSelectedResidualRiskLevel: UILabel!
    @IBOutlet weak var viewResidualRiskLevel: UIView!
    @IBOutlet weak var lblResidualRiskLevel: UILabel!
    
    @IBOutlet weak var lblApprovedBy: UILabel!
    
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    
    @IBOutlet weak var viewSelectionDueDate: UIView!
    @IBOutlet weak var viewRevisedDueDate: UIView!
    @IBOutlet weak var lblRevisedDueDate: UILabel!

    @IBOutlet weak var lblAttachmentFile: UILabel!
    
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnDueDate: UIButton!
    
    @IBOutlet weak var viewOfPager: UIView!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var txtNotesHistory: UITextField!
    @IBOutlet weak var constrainViewPagerHeight: NSLayoutConstraint! // 40.0
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var constrainViewNoteHeight: NSLayoutConstraint! //62
    
    @IBOutlet weak var btnAddVoiceNote: UIButton!
    @IBOutlet weak var constrainViewfileNameHeight: NSLayoutConstraint! // 30.0
    
    @IBOutlet weak var viewNoteHistoryFilter: UIView!
    @IBOutlet weak var constrainViewNoteHistiryFIlter: NSLayoutConstraint! // 30
    
    
    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var btnRelatedTo: UIButton!
    @IBOutlet weak var viewRelatedToTxt: UIView!
    
    @IBOutlet weak var viewCommanRelatedTo: UIView!
    @IBOutlet weak var btnRelatedOutlet: UIButton!
    @IBOutlet weak var viewCommonRelatedToTxt: UIView!
    
    @IBOutlet weak var lblCommanRelatedToTitle: UILabel!
    @IBOutlet weak var lblCommanRelatedTo: UILabel!
    
    @IBOutlet weak var viewPrimaryArea: UIView!
    @IBOutlet weak var lblPrimaryArea: UILabel!
    @IBOutlet weak var btnPrimaryArea: UIButton!
    @IBOutlet weak var viewPrimaryAreaTxt: UIView!
    
    @IBOutlet weak var viewSecondaryArea: UIView!
    @IBOutlet weak var lblSecondaryArea: UILabel!
    @IBOutlet weak var btnSecondaryArea: UIButton!
    @IBOutlet weak var viewSecondaryAreaTxt: UIView!
    
    @IBOutlet weak var viewStrategyGoal: UIView!
    @IBOutlet weak var lblStrategyGoal: UILabel!
    @IBOutlet weak var btnStrategyGoal: UIButton!
    @IBOutlet weak var viewStrategyGoalTxt: UIView!
    
    @IBOutlet weak var lblStrategyGoalRelatedToDueDate: UILabel!
    @IBOutlet weak var viewStrategyGoalRelatedToDueDate: UIView!
    
    @IBOutlet weak var lblCommanRelatedToDueDate: UILabel!
    @IBOutlet weak var viewCommonRelatedToDueDate: UIView!

    
    let disposeBag = DisposeBag()

    func setupLayout(imgBG: UIImage?) {
        img_Bg.image = imgBG
        lblAddTitle.text = "Risk"
        viewRevisedDueDate.isHidden = true
        viewMore.isHidden = true
        viewLevel.isHidden = true
        activitySpinner.color = appGreen
        
        viewCommanRelatedTo.isHidden = true
        viewPrimaryArea.isHidden = true
        viewSecondaryArea.isHidden = true
        viewStrategyGoal.isHidden = true

        //Rx Actions
        rxTitleTextFieldAction()
        rxDescriptionTextViewDidChange()
        rxScopeBoundriesDescriptionTextViewDidChange()
    }
    
    func setupTableView(theDelegate:AddRiskVC) {
        
        tblNotesHistory.estimatedRowHeight = 50.0
        tblNotesHistory.rowHeight = UITableView.automaticDimension
        
        tblNotesHistory.registerCellNib(HistoryCell.self)
        
        
        tblNotesHistory.delegate = theDelegate
        tblNotesHistory.dataSource = theDelegate
        tblNotesHistory.separatorStyle = .none
    }
    
    func setupCollectionView(theDelegate:AddRiskVC)  {
        collectionSelectedFile.delegate = theDelegate
        collectionSelectedFile.dataSource = theDelegate
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.reloadData()
    }

    func setObserver(theController:AddRiskVC)  {
//        tableView.addObserver(theController, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        collectionSelectedFile.addObserver(theController, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        tblNotesHistory.addObserver(theController, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    func removeObserver(theController:AddRiskVC) {
//        tableView.removeObserver(theController, forKeyPath: "contentSize")
        collectionSelectedFile.removeObserver(theController, forKeyPath: "contentSize")
        tblNotesHistory.removeObserver(theController, forKeyPath: "contentSize")
    }
    
    func loadCompanyActivity(isStart:Bool) {
        btnSelectCompany.loadingIndicator(isStart, allignment: .right)
    }
    func loadAssignToActivity(isStart:Bool) {
        btnAssigned.loadingIndicator(isStart, allignment: .right)
    }
    func loadDepartmentActivity(isStart:Bool) {
        btnDepartment.loadingIndicator(isStart, allignment: .right)
    }
    func loadCategoryActivity(isStart:Bool) {
        btnCategory.loadingIndicator(isStart, allignment: .right)
    }
    func loadActivityOnButton(button:UIButton?,isStart:Bool) {
        button?.loadingIndicator(isStart, allignment: .right)
    }
    
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            btnAdd.startLoadyIndicator()
            self.allButtons(isEnable: false)
        } else {
            btnAdd.stopLoadyIndicator()
            self.allButtons(isEnable: true)
        }
    }
    
    func showUpdateData(theModel:JSON) {
        txtTitle.text = theModel["unwantedevent"].string
        txtDescription.text = theModel["description"].string
        txtScopBoundriesDescription.text = theModel["scopeBoundries"].string

        lblCompany.text = theModel["company"].stringValue.isEmpty ? "Select company" : theModel["company"].stringValue
        lblAssignTo.text = theModel["assignto"].stringValue.isEmpty ? "Select assigned to" : theModel["assignto"].stringValue
        lblDepartment.text = theModel["department"].stringValue.isEmpty ? "Select department" : theModel["department"].stringValue
        
        lblDueDate.text = theModel["duedate"].stringValue.isEmpty ? "Select due date" : theModel["duedate"].stringValue
        lblRevisedDueDate.text = theModel["revisedduedate"].stringValue.isEmpty ? "Select revised date" : theModel["revisedduedate"].stringValue
        
        lblCategory.text = theModel["category"].stringValue.isEmpty ? "Select category" : theModel["category"].stringValue
        lblSubCategory.text = theModel["subcategory"].stringValue.isEmpty ? "Select sub category" : theModel["subcategory"].stringValue
        lblConsequenceCategory.text = theModel["consequenceCategory"].stringValue.isEmpty ? "Select consequence" : theModel["consequenceCategory"].stringValue

        viewLevel.isHidden = true
        lblSelectedLevel.isHidden = true
        if !theModel["level"].stringValue.isEmpty {
            lblSelectedLevel.isHidden = true
            viewLevel.isHidden = false
            lblLevel.text = theModel["level"].stringValue
            viewLevel.setBackGroundColor = getColorCodeFromRiskLevel(strLevel: theModel["level"].stringValue)
        }
        viewResidualRiskLevel.isHidden = true
        lblSelectedResidualRiskLevel.isHidden = true
        if !theModel["residualLevel"].stringValue.isEmpty {
            lblSelectedResidualRiskLevel.isHidden = true
            viewResidualRiskLevel.isHidden = false
            lblResidualRiskLevel.text = theModel["residualLevel"].stringValue
            viewResidualRiskLevel.setBackGroundColor = getColorCodeFromRiskLevel(strLevel: theModel["residualLevel"].stringValue)
        }
        lblCreatedBy.text = theModel["createdby"].stringValue.isEmpty ? "-" : theModel["createdby"].stringValue
        lblCreatedDate.text = theModel["createdDate"].stringValue.isEmpty ? "-" : theModel["createdDate"].stringValue
    }
    
    func showDropDownUpdatedData(theModel:JSON) {
        lblCompany.text = theModel["company"].stringValue.isEmpty ? "Select company" : theModel["company"].stringValue
        lblAssignTo.text = theModel["assignto"].stringValue.isEmpty ? "Select assigned to" : theModel["assignto"].stringValue
        lblDepartment.text = theModel["department"].stringValue.isEmpty ? "Select department" : theModel["department"].stringValue

        lblDueDate.text = theModel["duedate"].stringValue.isEmpty ? "Select due date" : theModel["duedate"].stringValue
        lblRevisedDueDate.text = theModel["revisedduedate"].stringValue.isEmpty ? "Select revised date" : theModel["revisedduedate"].stringValue
        
        lblCategory.text = theModel["category"].stringValue.isEmpty ? "Select category" : theModel["category"].stringValue
        lblSubCategory.text = theModel["subcategory"].stringValue.isEmpty ? "Select sub category" : theModel["subcategory"].stringValue
        lblConsequenceCategory.text = theModel["consequenceCategory"].stringValue.isEmpty ? "Select consequence" : theModel["consequenceCategory"].stringValue
        
        viewLevel.isHidden = true
        lblSelectedLevel.isHidden = true
        if !theModel["level"].stringValue.isEmpty {
            lblSelectedLevel.isHidden = true
            viewLevel.isHidden = false
            lblLevel.text = theModel["level"].stringValue
            viewLevel.setBackGroundColor = getColorCodeFromRiskLevel(strLevel: theModel["level"].stringValue)
        }
        viewResidualRiskLevel.isHidden = true
        lblSelectedResidualRiskLevel.isHidden = true
        if !theModel["residualLevel"].stringValue.isEmpty {
            lblSelectedResidualRiskLevel.isHidden = true
            viewResidualRiskLevel.isHidden = false
            lblResidualRiskLevel.text = theModel["residualLevel"].stringValue
            viewResidualRiskLevel.setBackGroundColor = getColorCodeFromRiskLevel(strLevel: theModel["residualLevel"].stringValue)
        }
        
    }
    func getColorCodeFromRiskLevel(strLevel:String) -> Int  {
        var code = 13
        switch strLevel {
            //        case "E2","E3","E4","E5","D3","D4","D5","C4","C5","B5":
            //            code = 13
        //            break
        case "E1","D1","D2","C2","C3","B4":
            code = 21
            break
        case "C1","B1","B2","B3","A1","A2","A3","A4","A5":
            code = 3
            break
        default:
            break
        }
        return code
    }
    //Rx
    func rxTitleTextFieldAction() {
        txtTitle.rx.text.orEmpty
            .subscribe({ [weak self] text in
                (self?.viewNextPresentingViewController() as? AddRiskVC)?.theCurrentViewModel.selectedData["unwantedevent"].stringValue = text.element ?? ""
            }).disposed(by:disposeBag)
    }
    func rxDescriptionTextViewDidChange() {
        txtDescription.rx.text.orEmpty
            .subscribe({ [weak self] event in
                (self?.viewNextPresentingViewController() as? AddRiskVC)?.theCurrentViewModel.selectedData["description"].stringValue = event.element ?? ""
                if let obj = self {
                    obj.lblPlaceHolder.isHidden = obj.txtDescription.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    func rxScopeBoundriesDescriptionTextViewDidChange() {
        txtScopBoundriesDescription.rx.text.orEmpty
            .subscribe({ [weak self] event in
                (self?.viewNextPresentingViewController() as? AddRiskVC)?.theCurrentViewModel.selectedData["scopeBoundries"].stringValue = event.element ?? ""
                if let obj = self {
                    obj.lblScopeBoundriesPlaceHolder.isHidden = obj.txtScopBoundriesDescription.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    func isEnableView(isEnable:Bool,view:UIView) {
        view.isUserInteractionEnabled = isEnable ? true : false
        view.alpha = isEnable ? 1.0 : 0.6
    }
    
    func viewHidden(theView:UIView, isHidden:Bool)  {
        theView.isHidden = true
    }
    
    func updateDateView(label:UILabel, strDueDate:String, color:String) {
        var Subtitle = ""
        if let dueDate = strDueDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7),!dueDate.isEmpty {
            if Subtitle.isEmpty {
                Subtitle += "Due on - " + dueDate
            } else {
                Subtitle += "Due on - " + dueDate
                //                dueDateString = "D: " + dueDate
            }
        }else{
            Subtitle += "Due on - " + "No Due Date"
        }
        label.attributedText = nil
        let attributeStringDue =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R13,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: color)])
        label.attributedText = attributeStringDue
    }
}
