//
//  MainHomeVC.swift
//  StrategyX
//
//  Created by Haresh on 28/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class MainHomeVC: ParentViewController {
    //MARK:- Variable
    fileprivate lazy var theCurrentView:MainHomeView = { [unowned self] in
       return self.view as! MainHomeView
    }()
    
    fileprivate lazy var theCurrentModel:MainHomeModel = {
       return MainHomeModel(theController: self)
    }()
    
    var handlerCloseMenu:(_ isAnimateStart:Bool) -> Void = {_ in }
    var handlerRedirecToMenu:(String) -> Void = {_ in }
    

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        theCurrentView.animateTransperantBG()
    }

    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.bottom = theCurrentModel.tabbarHeight
        theCurrentView.listCount = CGFloat(theCurrentModel.arr_firstMenu.count)
        print("theCurrentView.bottom:=",theCurrentView.bottom)
        theCurrentView.setupLayout()
        
        theCurrentView.setupTableView(theDelegate: self)
    }
    
    func setTheData(tabBarHeight:CGFloat) {
        theCurrentModel.tabbarHeight = tabBarHeight + 20
        print("tabBarHeight:=",tabBarHeight)
    }
    
    
    func stopAnimation()  {
//        let scale:CGFloat = (UIScreen.main.bounds.size.height * 20.0) / 568.0
//        print("scale:=",scale)
        theCurrentView.showTableView(isHidden: true, height: 0.0, bottom: 0.0)
        UIView.animate(withDuration: 0.7, delay: 0.0, animations: { [weak self] in
            self?.theCurrentView.viewTransperantBG.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        }, completion: { (finished) in
//            self.viewTransperantBG.cornerRadius = 0
//            print("self.viewTransperantBG:=",self.viewTransperantBG.frame)
            self.handlerCloseMenu(false)
        })
    }
    
    
    //MARK:- IBAction
    
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.handlerCloseMenu(true)
//        stopAnimation()
    }

}

//MARK:-UITableViewDataSource
extension MainHomeVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if theCurrentModel.isShowSecondMenu {
            return theCurrentModel.arr_secondMenu.count
        }
        return theCurrentModel.arr_firstMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if theCurrentModel.isShowSecondMenu {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! ActionMenuTVCell
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! ActionMenuTVCell
            cell.configureCell1(strTitle: "+ " + theCurrentModel.arr_secondMenu[indexPath.row], index: indexPath.row)
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! ActionMenuTVCell
            cell.configureCell1(strTitle: "+ " + theCurrentModel.arr_firstMenu[indexPath.row], index: indexPath.row)
            return cell
        }
        
    }
    
}
//MARK:-UITableViewDelegate
extension MainHomeVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.isShowSecondMenu {
            if indexPath.row == 0 {
                let height = CGFloat(theCurrentModel.arr_firstMenu.count) * theCurrentView.cellHeight
                theCurrentView.showTableView(isHidden: false, height: height, bottom: theCurrentModel.tabbarHeight)
                theCurrentModel.isShowSecondMenu = false
                tableView.reloadData()
            } else {
                handlerRedirecToMenu(theCurrentModel.arr_secondMenu[indexPath.row])
                stopAnimation()
            }
        } else {
            if indexPath.row == 0 && UserRole.staffUser != UserDefault.shared.userRole && UserRole.manager != UserDefault.shared.userRole && UserRole.staffLite != UserDefault.shared.userRole {
                let height = CGFloat(theCurrentModel.arr_secondMenu.count) * theCurrentView.cellHeight
                theCurrentView.showTableView(isHidden: false, height: height, bottom: theCurrentModel.tabbarHeight)
                theCurrentModel.isShowSecondMenu = true
                tableView.reloadData()
            } else  {
                handlerRedirecToMenu(theCurrentModel.arr_firstMenu[indexPath.row])
                stopAnimation()
            }
        }
    }
    

}


class NavigationMainHomeVC: UINavigationController {
    
}
