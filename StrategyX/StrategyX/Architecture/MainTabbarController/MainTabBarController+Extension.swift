//
//  MainTabBarController+Extension.swift
//  StrategyX
//
//  Created by Haresh on 07/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
extension MainTabBarController {
    
    func removeLeftAndRightMenuViewController() -> UIViewController {
        guard let vc = self.viewControllers,let currentNVC = vc[self.selectedIndex] as? UINavigationController else {
            let basevc = (self.viewControllers![self.selectedIndex] as! UINavigationController).viewControllers
            return basevc[basevc.count - 1]
        }
        
        var baseVC = currentNVC.viewControllers[currentNVC.viewControllers.count - 1]
        // Form left
        if let index = currentNVC.viewControllers.firstIndex(where: {
            switch $0 {
            case is ProfileVC:
                return true
            case is ChangePasswordVC:
                return true
            case is StrategyVC:
                return true
            case is RisksVC:
                return true
            case is TacticalProjectVC:
                return true
            case is FocusVC:
                return true
            case is MyRoleVC:
                return true
            case is CriticalSucessFactorsVC:
                return true
            case is NotesVC:
                return true
            case is ReportCategoriesVC:
                return true
            default:
                return false
            }
            
        }) {
            let vc = currentNVC.viewControllers[index - 1]
            currentNVC.popToViewController(vc, animated: false)
            baseVC = vc
        }
        // From Right
        if let index = currentNVC.viewControllers.firstIndex(where: {
            switch $0 {
            case is GeneralSettingsVC:
                return true
            case is ResourcesVC:
                return true
            case is FeedbackVC:
                return true
            case is SatisfactionVC:
                return true
            case is FeatureUpdateVC:
                return true
            case is NotificationVC:
                return true
            case is TrainingVC:
                return true
            case is TimeLogVC:
                return true
            default:
                return false
            }
            //            if let _ = $0 as? (MyRoleCalendarVC, AddPrimaryAreaVC,AddSecondaryAreaVC,AddGoalVC,AddRiskVC) { return true }
            //            return false
        }) {
            let vc = currentNVC.viewControllers[index - 1]
            currentNVC.popToViewController(vc, animated: false)
            baseVC = vc
        }
        
        return baseVC
    }

    //MARK:- LeftSideMenu
    func redirectToAccordingToSelectionOfLeftSideMenu(strTitle:String) {
        //        var arr_LeftMenuTitle:[String] = ["Strategy","Risks","Tactical Projects","Focus","My Role","Actions","Time Log"]
        //        var arr_SelectCompany_LeftMenuTitle:[String] = ["Orange UK","Sell Lease Property","Yelp"]
        //        var arr_SelectAdmin_LeftMenuTitle:[String] = ["Profile","Change Password","Logout"]
        isShowMainHome = false
        guard let _ = self.viewControllers,let currentNVC = removeLeftAndRightMenuViewController().navigationController else {
            return
        }
        if UserRole.staffLite == UserDefault.shared.userRole {
            if "Strategy".localizedLowercase == strTitle.localizedLowercase || "Tactical Projects".localizedLowercase == strTitle.localizedLowercase || "Focus".localizedLowercase == strTitle.localizedLowercase || "Success Factors".localizedLowercase == strTitle.localizedLowercase || "Risks".localizedLowercase == strTitle.localizedLowercase {
                self.showAlertAtBottom(message: "This function is only available to standard users. Please ask your manager to upgrade your permissions to a \"Standard User\".")
                return
            }
        }
        
        switch strTitle.localizedLowercase {
        case "Strategy".localizedLowercase:
            redirectToStrategyScreen(nvc: currentNVC)
            break
        case "Tactical Projects".localizedLowercase :
            redirectToTacticalProjectScreen(nvc: currentNVC)
            break
        case "Focus".localizedLowercase:
            redirectToFocusScreen(nvc: currentNVC)
            break
        case "Recurring Tasks".localizedLowercase:
            redirectToMyRoleScreen(nvc: currentNVC)
            break
        case "Success Factors".localizedLowercase:
//            self.showAlertAtBottom(message: "Coming Soon")
            redirectToCriticalSucessFactorsScreen(nvc: currentNVC)
            break
//        case "Actions".localizedLowercase:
//            (self.viewControllers![0] as! NavigationActionVC).popToRootViewController(animated: false)
//            self.selectedIndex = 0
//            break
        case "Notes".localizedLowercase:
            redirectToNotesScreen(nvc: currentNVC)
            break
        case "Reports".localizedLowercase:
            redirectToReportsCategoriesScreen(nvc: currentNVC)
            break
        case "Risks".localizedLowercase:
            redirectToRisksScreen(nvc: currentNVC)
            break
        case "Profile".localizedLowercase:
            redirectToProfileScreen(nvc: currentNVC)
            break
        case "Change Password".localizedLowercase:
//            redirectToChangePasswordScreen(nvc: currentNVC)
            break
        case "Logout".localizedLowercase:
            showAlertAction(msg: "Are you sure want to logout?") { [weak self] in
                self?.logout()
            }
            break
        default:
            break
        }
        print("rootViewcontrollerCount:=",currentNVC.viewControllers)
    }
    func redirectToProfileScreen(nvc:UINavigationController) {
        let vc = ProfileVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToChangePasswordScreen(nvc:UINavigationController) {
        let vc = ChangePasswordVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToStrategyScreen(nvc:UINavigationController) {
        let vc = StrategyVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToRisksScreen(nvc:UINavigationController) {
        let vc = RisksVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToTacticalProjectScreen(nvc:UINavigationController) {
        let vc = TacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToFocusScreen(nvc:UINavigationController) {
        let vc = FocusVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToMyRoleScreen(nvc:UINavigationController) {
        let vc = MyRoleVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.push(viewController: vc)
        //        nvc.pushViewController(vc, animated: false)
    }
    func redirectToCriticalSucessFactorsScreen(nvc:UINavigationController) {
        let vc = CriticalSucessFactorsVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToNotesScreen(nvc:UINavigationController) {
        let vc = NotesVC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToReportsCategoriesScreen(nvc:UINavigationController) {
        let vc = ReportCategoriesVC.instantiateFromAppStoryboard(appStoryboard: .reports)
        nvc.pushViewController(vc, animated: false)
    }
    func logout() {

        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)
        
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_device_id:deviceID]
        
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        let activity = UIActivityIndicatorView(style: .white)
//        activity.color = appGreen
//        activity.startAnimating()
//        AppDelegate.shared.window?.rootViewController?.
//        AppDelegate.shared.window?.rootViewController?.view.addSubview(activity)
        
        AuthenicationService.shared.logout(parameter: parameter, success: { [weak self] (msg) in
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            alert.dismiss(animated: false, completion: nil)
                self?.removeAudioDirectory()
                self?.removeAttachmentDirectory()
                AppDelegate.shared.clearNotificationCount()
                Utility.shared.dispose()
                WebService().cancelledAllRequest()
                AppDelegate.shared.redirectToRootLoginViewController()
        }, failed: { [weak self] (error) in
            alert.dismiss(animated: false, completion: {
                self?.showAlertAtBottom(message: error)
            })
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
        })
    }
    
    
    //MARK:- RightSideMenu
    func redirectToAccordingToSelectionOfRightSideMenu(strTitle: String) {
        isShowMainHome = false
        guard let _ = self.viewControllers,let currentNVC = removeLeftAndRightMenuViewController().navigationController else {
            return
        }
        
        switch strTitle.localizedLowercase {
        case "Settings".localizedLowercase:
            redirectToGeneralSettingsScreen(nvc: currentNVC)
//            redirectToSettingsScreen(nvc: currentNVC)
            break
        
        case "Notifications".localizedLowercase :
            redirectToNotificationScreen(nvc: currentNVC)
            break
        case "Satisfaction".localizedLowercase:
//            self.showAlertAtBottom(message: "Coming Soon")
             redirectToSatisfactionScreen(nvc: currentNVC)
            break
        case "Whats New".localizedLowercase:
            redirectToFeatureUpdateScreen(nvc: currentNVC)
            break
//        case "Feature Updates".localizedLowercase:
//            redirectToFeatureUpdateScreen(nvc: currentNVC)
//            break
        
        case "Training".localizedLowercase:
            redirectToTrainingScreen(nvc: currentNVC)
            break
        case "Resources".localizedLowercase:
            redirectToResourcesScreen(nvc: currentNVC)
            break
        case "Time Log".localizedLowercase:
            redirectToTimeLogScreen(nvc: currentNVC)
            break
        default:
            break
        }
        print("rootViewcontrollerCount:=",currentNVC.viewControllers)
    }
    func redirectToGeneralSettingsScreen(nvc:UINavigationController) {
        let vc = GeneralSettingsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToSettingsScreen(nvc:UINavigationController) {
        let vc = SettingsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    
    func redirectToReportsScreen(nvc:UINavigationController) {
        let vc = ReportsVC.instantiateFromAppStoryboard(appStoryboard: .reports)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToApprovalsScreen(nvc:UINavigationController) {
        let vc = ApprovalsVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToResourcesScreen(nvc:UINavigationController) {
        let vc = ResourcesVC.instantiateFromAppStoryboard(appStoryboard: .resources)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToFeedbackScreen(nvc:UINavigationController) {
        let vc = FeedbackListVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToSatisfactionScreen(nvc:UINavigationController) {
        let vc = SatisfactionVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    
    func redirectToFeatureUpdateScreen(nvc:UINavigationController) {
        let vc = FeatureUpdateVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    
    func redirectToNotificationScreen(nvc:UINavigationController) {
        let vc = NotificationVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    
    func redirectToTrainingScreen(nvc:UINavigationController) {
        let vc = TrainingVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    func redirectToTimeLogScreen(nvc:UINavigationController) {
        let vc = TimeLogVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        nvc.pushViewController(vc, animated: false)
    }
    
    //MARK:- Home Menu redirection
    func redirectAccordingToSelectionHomeMenu(strTitle:String) {
        isShowMainHome = false
        
        guard let vc = self.viewControllers,let currentNVC = vc[self.selectedIndex] as? UINavigationController else {
            return
        }
        let baseVC = currentNVC.viewControllers.last!
        
        switch strTitle.localizedLowercase {
        case "Strategy".localizedLowercase:
            
            break
        case "Risk".localizedLowercase:
            redirectToAddRiskScreen(baseVC: baseVC)
            break
        case "Tactical Project".localizedLowercase :
            redirectToAddTacticalProjectScreen(baseVC: baseVC)
            break
        case "Focus".localizedLowercase:
            redirectToAddFocusScreen(baseVC: baseVC)
            break
        case "Recurring Task".localizedLowercase:
            redirectToMyRoleCalenderScreen(baseVC: baseVC)
            break
        case "Action".localizedLowercase:
            redirectToAddActionScreen(baseVC: baseVC)
            break
        case "Company".localizedLowercase:
            redirectToAddCompanyScreen(baseVC: baseVC)
            break
        case "User".localizedLowercase:
            redirectToAddUserScreen(baseVC: baseVC)
            break
        case "Tag".localizedLowercase:
            redirectToAddTagsScreen(baseVC: baseVC)
            break
        case "Note".localizedLowercase:
            redirectToAddNoteScreen(baseVC: baseVC)
            break
        case "Success Factors".localizedLowercase:
//            self.showAlertAtBottom(message: "Coming Soon")
            redirectToAddCriticalSucessFactorScreen(baseVC: baseVC)
            break
        case "Idea".localizedLowercase:
//            redirectToAddIdeaScreen(baseVC: baseVC)
            redirectToAddIdeaAndProblemScreen(baseVC: baseVC, type: .idea)
              break
        case "Problem".localizedLowercase:
//            redirectToAddProblemScreen(baseVC: baseVC)
            redirectToAddIdeaAndProblemScreen(baseVC: baseVC, type: .problem)
            break
        case "Primary Area".localizedLowercase:
            redirectToAddPrimaryAreaScreen(baseVC: baseVC)
            break
        case "Secondary Area".localizedLowercase:
            redirectToAddSecondaryAreaScreen(baseVC: baseVC)
            break
        case "Goal".localizedLowercase:
            redirectToAddGoalScreen(baseVC: baseVC)
            break
        default:
            break
        }
        
    }
    
    func redirectToMyRoleCalenderScreen(baseVC:UIViewController) {
        let vc = MyRoleCalendarVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        vc.handlerClose = { 
            
        }
        baseVC.push(vc: vc)
    }
    func redirectToAddPrimaryAreaScreen(baseVC:UIViewController) {
        let vc = AddPrimaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddSecondaryAreaScreen(baseVC:UIViewController) {
        let vc = AddSecondaryAreaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddGoalScreen(baseVC:UIViewController) {
        let vc = AddGoalVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddRiskScreen(baseVC:UIViewController) {
        let vc = AddRiskVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddTacticalProjectScreen(baseVC:UIViewController) {
        let vc = AddTacticalProjectVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddFocusScreen(baseVC:UIViewController) {
        let vc = AddFocusVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddActionScreen(baseVC:UIViewController) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddCompanyScreen(baseVC:UIViewController) {
        let vc = AddCompanyVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddUserScreen(baseVC:UIViewController) {
        let vc = AddUserVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddTagsScreen(baseVC:UIViewController) {
        let vc = AddTagsVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddNoteScreen(baseVC:UIViewController) {
        let vc = AddNoteVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddCriticalSucessFactorScreen(baseVC:UIViewController) {
        let vc = AddCriticalSucessFactorVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddIdeaAndProblemScreen(baseVC:UIViewController, type:IdeaAndProblemType) {
        let vc = AddIdeaNProblemVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG, type: type)
        baseVC.push(vc: vc)
    }
   /* func redirectToAddIdeaScreen(baseVC:UIViewController) {
        let vc = AddIdeaVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    func redirectToAddProblemScreen(baseVC:UIViewController) {
        let vc = AddProblemVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: imgBG)
        baseVC.push(vc: vc)
    }
    */
    //MARK:- ReinitViewController while changing company from sidemenu
    func reInitViewController() {
        
        guard let vc = self.viewControllers,let currentNVC = vc[self.selectedIndex] as? UINavigationController else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
            ((self.viewControllers![0] as? NavigationActionVC)?.viewControllers[0] as? ActionVC)?.reInitData()
        }
        ((self.viewControllers![1] as? NavigationIdeasNProblemsVC)?.viewControllers[0] as? IdeasNproblemsVC)?.reInitData()
        ((self.viewControllers![3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC)?.reInitData()
        
        
        
        if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? StrategyVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index - 1], animated: false)
            redirectToStrategyScreen(nvc: currentNVC)
        } else if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? RisksVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index - 1], animated: false)
            redirectToRisksScreen(nvc: currentNVC)
        } else if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? TacticalProjectVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index - 1], animated: false)
            redirectToTacticalProjectScreen(nvc: currentNVC)
        } else if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? FocusVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index - 1], animated: false)
            redirectToFocusScreen(nvc: currentNVC)
        } else if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? CriticalSucessFactorsVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index - 1], animated: false)
            redirectToCriticalSucessFactorsScreen(nvc: currentNVC)
        } else if let index = currentNVC.viewControllers.firstIndex(where: { ($0 as? ReportCategoriesVC) != nil }) {
            currentNVC.popToViewController(currentNVC.viewControllers[index], animated: false)
//            redirectToCriticalSucessFactorsScreen(nvc: currentNVC)
        } else {
//            [actionNavigation,ideaNProblemsNaviagtion , emptyCenterNavigation ,approvalsNaviagtion,menuNavigation]
            if selectedIndex == 1 {
                (self.viewControllers![1] as? NavigationIdeasNProblemsVC)?.popToRootViewController(animated: false)
            } else if selectedIndex == 3 {
                (self.viewControllers![3] as? NavigationApprovalVC)?.popToRootViewController(animated: false)
            } else if selectedIndex == 4 {
                (self.viewControllers![4] as? NavigationMenuVC)?.popToRootViewController(animated: false)
            } else {
                (self.viewControllers![0] as! NavigationActionVC).popToRootViewController(animated: false)
                self.selectedIndex = 0
            }
            
        }
    }
    
    //MARK:- Other Member function
    func redirectToSatisfactionPopupVC() {
        let vc = SatisfactionPopupVC.init(nibName: "SatisfactionPopupVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
