//
//  SubTaskNewCell.swift
//  StrategyX
//
//  Created by Haresh on 13/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SubTaskNewCell: UITableViewCell {

    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtEdit: UITextField!
    @IBOutlet weak var viewEdit: UIView!
    @IBOutlet weak var btnCnacel: UIButton!
    let disposeBag = DisposeBag()
    
    var handlerTextChanged:(String) -> Void = {_ in}
    
    var handlertextViewEndEditText:(String) -> Void = {_ in}
    var handlerOnDeleteAction:(Int) -> Void = {_ in}
    var handlerOnCheckAction:() -> Void = {}
//    var handlerOnEditAction:() -> Void = {}

    var handlerOnEditcompletedAction:(String) -> Void = {_ in}
//    var handlerOnEditCancelAction:() -> Void = {}

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        onTextViewChange()
        onTextViewEndEditing()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,isSelectedCheck:Bool) {
//        txtEdit.isHidden = true
        viewEdit.isHidden = true
        self.lblTitle.numberOfLines = 0
        lblTitle.isHidden = false
        txtEdit.text = strTitle
//        txtEdit.isScrollEnabled = false
        btnCheck.isSelected = isSelectedCheck
        if isSelectedCheck {
            lblTitle.text = ""
            lblTitle.attributedText = nil
            let attributedString = NSMutableAttributedString(string: strTitle)
            attributedString.addAttributes([NSAttributedString.Key.strikethroughStyle : NSNumber(value: NSUnderlineStyle.single.rawValue),NSAttributedString.Key.strikethroughColor:UIColor.darkGray], range: NSMakeRange(0, strTitle.length))
//            attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
//            attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.darkGray, range: NSMakeRange(0, attributedString.length))
            
            lblTitle.attributedText = attributedString
        } else {
            lblTitle.attributedText = nil
            lblTitle.text = strTitle
        }
    }

    func onTextViewChange() {
        txtEdit.rx.text.orEmpty
            .subscribe({  event in
                let _ = event.element ?? ""
//                self?.lblTitle.numberOfLines = 1
    
//                self?.lblTitle.text = text
//                self?.handlerTextChanged(text)
//                self?.handlerText(text)
            }).disposed(by: disposeBag)
    }
    
    func onTextViewEndEditing() {
        txtEdit.rx.controlEvent(UIControl.Event.editingDidEnd)
            .subscribe(onNext: { [weak self] (event) in
                let text = self?.lblTitle.text ?? ""
                self?.lblTitle.isHidden = false
                self?.viewEdit.isHidden = true
                self?.lblTitle.numberOfLines = 0
                self?.handlerTextChanged(text)
//                self?.handlertextViewEndEditText(text)
            }).disposed(by: disposeBag)
        
        
    }
    
    func hideEditAndCloseButton() {
        btnEdit.isHidden = true
        btnCnacel.isHidden = true
        viewEdit.isHidden = true
    }
    
    //MARK:- IBAction
    @IBAction func onBtnDeleteAction(_ sender:UIButton) {
        txtEdit.resignFirstResponder()

        handlerOnDeleteAction(self.tag)
    }

    @IBAction func onBtnEditActionAction(_ sender: UIButton) {
        lblTitle.isHidden = true
//        txtEdit.isHidden = false
        viewEdit.isHidden = false
        lblTitle.numberOfLines = 1
        handlerTextChanged(lblTitle.text ?? "")
        txtEdit.becomeFirstResponder()
        
//        handlerOnEditAction()
    }
    @IBAction func onBtnCheckedAction(_ sender: UIButton) {
        lblTitle.text = txtEdit.text ?? ""
        txtEdit.resignFirstResponder()
//        handlertextViewEndEditText(lblTitle.text ?? "")
        handlerOnCheckAction()
    }
    
    @IBAction func onBtnEditCompletedAction(_ sender: Any) {
        lblTitle.text = txtEdit.text ?? ""
        txtEdit.resignFirstResponder()
//        handlertextViewEndEditText(lblTitle.text ?? "")
        handlerOnEditcompletedAction(lblTitle.text ?? "")
    }
    @IBAction func onBtnEditCloseAction(_ sender: Any) {
        txtEdit.resignFirstResponder()
//        handlerOnEditCancelAction()
        handlertextViewEndEditText(lblTitle.text ?? "")
    }
    
    
}
