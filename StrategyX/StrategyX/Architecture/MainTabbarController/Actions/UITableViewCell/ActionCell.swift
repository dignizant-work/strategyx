//
//  ActionCell.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ActionCell: UITableViewCell {

    @IBOutlet weak var constrainStackviewLeading: NSLayoutConstraint! // 10
    @IBOutlet weak var constrainViewProfileWidth: NSLayoutConstraint! // 30
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var progressBar: CircularProgressBar!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var constrainBtnMoreWidth: NSLayoutConstraint!
    @IBOutlet weak var lblNotesCount: UILabel!
    @IBOutlet weak var viewNotesCount: UIView!
    @IBOutlet weak var constrainViewNotesWidth: NSLayoutConstraint! // 22.0
//    @IBOutlet weak var constrainViewNotesLeading: NSLayoutConstraint! // 0.0
    @IBOutlet weak var lblRequest: UILabel!
    @IBOutlet weak var lblApproved: UILabel!
    
    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var viewType: UIView!

    var handlerMoreAction:() -> Void = {}
    var handleDueDateAction:() -> Void = {}
    var handleWorkDateAction:() -> Void = {}
    var handlerPercentCompletedAction:() -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        showMoreButtonForAction()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func startAnimation() {
        lblDueDate.setBackGroundColor = 0
        lblRequest.isHidden = true
        lblApproved.isHidden = true
        btnMore.isHidden = true
        img_Profile.isHidden = false
        [img_Profile,lblTitle,lblSubTitle,lblDueDate,viewNotesCount].forEach({ $0.showAnimatedSkeleton() })
        setProgressBar(progress: 0.0)
        lblTitle.text = " "
        lblSubTitle.text = " "
        lblTypeTitle.text = ""
        viewType.backgroundColor = white

    }
    func hideAnimation() {
        btnMore.isHidden = false
        [img_Profile,lblTitle,lblSubTitle,lblDueDate,viewNotesCount].forEach({ $0?.hideSkeleton() })
    }
    
    func configure(strShortName:String,strTitle:String,strSubTitle:String, progress:Double) {
//        viewProfile.backgroundColor = UIColor.random()
//        viewProfile.cornerRadius = self.viewProfile.bounds.width / 2
//        viewProfile.clipsToBounds = true
        lblProfile.text = strShortName
        lblTitle.text = strTitle
        lblSubTitle.text = strSubTitle
        setProgressBar(progress: progress)
    }
    
    func configureActions(theModel:ActionsList, isApiLoading:Bool = false) {
        hideAnimation()
        self.contentView.isUserInteractionEnabled = !isApiLoading
        showMoreButtonForAction(isHidden: !isApiLoading)
        btnMore.loadingIndicator(isApiLoading)
        self.contentView.isUserInteractionEnabled = !isApiLoading
        lblApproved.isHidden = theModel.approveFlag == 1 ? false : true
        lblRequest.isHidden = theModel.requestFlag == 1 ? false : true
        lblNotesCount.text = "\(theModel.notesCount)"
        viewNotesCount.setBackGroundColor = theModel.notesCount > 0 ? 9 : 3
        lblProfile.text = theModel.assignedTo.acronym()
        if theModel.assignedToUserImage.isEmpty && theModel.assignedTo.isEmpty {
            img_Profile.isHidden = false
            img_Profile.image = UIImage(named: "ic_mini_plash_holder")
            lblProfile.isHidden = true
        } else {
            if !theModel.assignedToUserImage.isEmpty {
                img_Profile.sd_setImageLoadMultiTypeURLForList(url: theModel.assignedToUserImage, completion: { [weak self] (isHidden) in
                    self?.lblProfile.isHidden = !isHidden
                    if theModel.assignedTo.isEmpty {
                        self?.img_Profile.isHidden = false
                    }
                })
            } else {
                img_Profile.isHidden = true
                lblProfile.isHidden = false
                if theModel.assignedTo.isEmpty {
                    img_Profile.isHidden = false
                    lblProfile.isHidden = true
                }
            }
        }
        constrainStackviewLeading.constant = 10
        constrainViewProfileWidth.constant = 30
        if UserDefault.shared.isHiddenProfileForStaffUser(strAssignedID: theModel.assigneeId) {
            constrainStackviewLeading.constant = 0
            constrainViewProfileWidth.constant = 0
        }
        
//        img_Profile.sd_setImageLoadMultiTypeURL(url: theModel.assignedToUserImage, placeholder: "ic_mini_plash_holder")
//        sd_setImage(with:  URL(string: theModel.assignedToUserImage), placeholderImage: UIImage.init(named: "ic_mini_plash_holder"))
        lblTitle.text = theModel.actionlogTitle
        let percentage = (Double(theModel.percentageCompeleted) ?? 0.0) / 100
        setProgressBar(progress: percentage)
        
//        lblDueDate.setBackGroundColor = theModel.requestedRevisedDate.isEmpty ? 0 : 30

        let attributeString = NSMutableAttributedString()

        var Subtitle = ""
        if let workDate = theModel.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType10),!workDate.isEmpty {
            Subtitle = "W: " + workDate
        }else{
            Subtitle = "W: " + "No Work Date"
        }
        
        Subtitle += " "
        let attributeStringWork =  NSAttributedString(string: Subtitle, attributes: [NSAttributedString.Key.font:R12,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theModel.workdateColor)])
        attributeString.append(attributeStringWork)
        lblSubTitle.attributedText = attributeStringWork
        var dueDateString = ""
        
        if let dueDate = theModel.duedate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType15),!dueDate.isEmpty {
            if Subtitle.isEmpty {
                Subtitle += "D: " + dueDate
                dueDateString = "D: " + dueDate
            } else {
                Subtitle += "D: " + dueDate
                dueDateString = "D: " + dueDate
            }
            dueDateString = dueDate
        }else{
            Subtitle += "D: " + "No Due Date"
            dueDateString = "D: " + "No Due Date"
            dueDateString = "No Due Date"
        }
        let attributeStringDue =  NSAttributedString(string: dueDateString, attributes: [NSAttributedString.Key.font:R12,NSAttributedString.Key.foregroundColor:UIColor.init(hexString: theModel.duedateColor)])
        attributeString.append(attributeStringDue)

        lblDueDate.attributedText = attributeStringDue
        
        lblTypeTitle.text = Utility.shared.setColorBGAccordingToActions(type: "\(theModel.type)").1
        lblTypeTitle.rotate(degrees: 270)
        lblTypeTitle.frame = CGRect(x:0, y:0, width:15, height:viewType.frame.size.height)
        viewType.backgroundColor = ColorType().setColor(index: Utility.shared.setColorBGAccordingToActions(type: "\(theModel.type)").0)
        
//        (lblSubTitle as? LabelButton)?.onClick = { [weak self] in
//            self?.handleWorkDateAction()
//        }
//        (lblDueDate as? LabelButton)?.onClick = { [weak self] in
//            self?.handleDueDateAction()
//        }
    }
    
    func setProgressBar(progress:Double) {
        
        progressBar.foregroundStrokeColor = appCyanDarkColor
        progressBar.backgroundStrokeColor = appSepratorColor
        progressBar.LabelFontColor = appTextBlackShedColor
        progressBar.labelSize = 0
        progressBar.lineWidth = 4
        progressBar.safePercent = 100
        progressBar.setProgress(to: progress, withAnimation: true)
    }
    
    func showMoreButtonForAction(isHidden:Bool = true) {
        btnMore.isHidden = isHidden
        constrainBtnMoreWidth.constant = isHidden ? 0.0 : 30.0
    }
    func hideViewNotes() {
        viewNotesCount.isHidden = true
        constrainViewNotesWidth.constant = 0
//        constrainViewNotesLeading.constant = 0
    }
    
    //MARK:- Action
    @IBAction func onBtnMoreAction(_ sender: Any) {
        handlerMoreAction()
    }
    
    @IBAction func onBtnSubTitleAction(_ sender: Any) {
        handleWorkDateAction()
    }
    
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        handleDueDateAction()
    }
    @IBAction func onBtnPercentCompletedAction(_ sender: Any) {
        handlerPercentCompletedAction()
    }
    
}
