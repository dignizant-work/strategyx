//
//  ActionArchivedVC.swift
//  StrategyX
//
//  Created by Jaydeep on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class ActionArchivedVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ActionArchivedView = { [unowned self] in
        return self.view as! ActionArchivedView
    }()
    
    fileprivate lazy var theCurrentModel:ActionArchivedModel = {
        return ActionArchivedModel(theController: self)
    }()
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        setCompanyData()
        archivedService()
        //        displayContentController(content: subListViewController(step: .step0, title: "Unwanted Events"))
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
   
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_ActionsArchivedList == nil || theCurrentModel.arr_ActionsArchivedList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
   
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_ActionsArchivedList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
            //            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            presentGraphActionList(strChartId: model.actionlogId)
            //            presentGraphAction(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
            //            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
    }
    
    func deleteActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID = strActionLogID
            self?.updateBottomCell(isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    func archiveActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID = strActionLogID
            self?.updateBottomCell(isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: false,isArchive: true)
        }
    }
    func updateBottomCell(isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_ActionsArchivedList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == theCurrentModel.apiLoadingAtActionID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            theCurrentModel.apiLoadingAtActionID = "-1"
            theCurrentModel.arr_ActionsArchivedList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
//            theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: indexRow, section: 0)], with: .fade)
            if theCurrentModel.arr_ActionsArchivedList?.count == 0 {
                setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi || isRemove {
                theCurrentModel.apiLoadingAtActionID = "-1"
            }
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [index], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateActionArchivedListModel(theActionDetailModel:ActionDetail) {
        if let index = theCurrentModel.arr_ActionsArchivedList?.firstIndex(where: {$0.actionlogId == theActionDetailModel.actionLogid}) {
            theCurrentModel.arr_ActionsArchivedList![index].actionlogTitle = theActionDetailModel.actionTitle
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        }
    }
    
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_ActionsArchivedList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_ActionsArchivedList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirect To
    func redirectToActionSubList1VC() {
      //  let vc = ActionSubList1VC.instantiateFromAppStoryboard(appStoryboard: .leftSideMenu)
      //  self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func redirectToActionDetailScreen(theModel:ActionsList) {
        let vc = ActionDetailVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setupData(theActionId: theModel.actionlogId, isCheckEditScreen: false)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        /*vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
        }*/
        self.push(vc: vc)
    }
    
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.setTheData(filterData: theCurrentModel.filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData
            
            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != ""{
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            }
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }
            
            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_ActionsArchivedList?.removeAll()
            self?.theCurrentModel.arr_ActionsArchivedList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.archivedService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphAction() {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    
    
    
    @IBAction func onBtnActionGraphAction(_ sender: Any) {
        presentGraphAction()
        
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        /*var title = ""
         if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
         title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
         }
         
         if let vc = theCurrentModel.subViewControllers.last {
         removeContentController(content: vc)
         theCurrentView.updateTitleText(strTitle: title)
         theCurrentModel.subViewControllers.removeLast()
         if theCurrentModel.subViewControllers.count == 1 {
         theCurrentView.backButtonRisk(isHidden: true)
         }
         print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
         }*/
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension ActionArchivedVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_ActionsArchivedList == nil || theCurrentModel.arr_ActionsArchivedList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_ActionsArchivedList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActionCell") as! ActionCell
        cell.showMoreButtonForAction(isHidden: false)

        if theCurrentModel.arr_ActionsArchivedList != nil {
            cell.configureActions(theModel: theCurrentModel.arr_ActionsArchivedList![indexPath.row])
            cell.progressBar.setProgress(to: 100, withAnimation: false)

            cell.handlerMoreAction = { [weak self] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_ActionsArchivedList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                archivedService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_ActionsArchivedList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtActionID == arr_model[indexPath.row].actionlogId {
            return false
        }
        return true
    }
    
}
//MARK:-UITableViewDelegate
extension ActionArchivedVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_ActionsArchivedList, arr_model.count > 0, theCurrentModel.apiLoadingAtActionID != arr_model[indexPath.row].actionlogId else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_ActionsArchivedList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.section].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        configure.performsFirstActionWithFullSwipe = false
        
        if UserDefault.shared.isCanEditForm(strOppoisteID: arr_model[indexPath.row].createdBy) {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("Chart")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        return configure
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if theCurrentModel.arr_ActionsArchivedList != nil {
            getActionDetailWebService(strActionLogID: theCurrentModel.arr_ActionsArchivedList![indexPath.row].actionlogId)
//            redirectToActionEditVC(theModel: theCurrentModel.arr_ActionsArchivedList![indexPath.row])
//            redirectToActionDetailScreen(theModel: theCurrentModel.arr_ActionsArchivedList![indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_ActionsArchivedList != nil {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
}

//MARK:- call API

extension ActionArchivedVC
{
    func archivedService(isRefreshing:Bool = false) {
        
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = theCurrentModel.filterData.workDate, !workDateFormate.isEmpty {
            //            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = theCurrentModel.filterData.dueDate, !dueDateFormate.isEmpty {
            //            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_grid_type:APIKey.key_archived,
                         APIKey.key_tags:theCurrentModel.filterData.searchTag,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID.isEmpty ? "0" : theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_workdatefilter:workDate,
                         APIKey.key_duedatefilter:dueDate,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
//        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
//                         APIKey.key_user_id:Utility.shared.userData.id,
//                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        ArchivedWebservice.shared.getActionArchivedList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                //                self?.showAlertAtBottom(message: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)*/
        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        Utility.shared.showActivityIndicator()

        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
//            alert.dismiss(animated: false, completion: nil)
            //              self?.redirectToAddActionVC(theModel: detail)
            self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()
//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell()
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell()
        })
    }
    
   
}
