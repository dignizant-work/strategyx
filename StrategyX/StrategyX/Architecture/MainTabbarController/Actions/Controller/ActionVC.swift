//
//  ActionVC.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class ActionVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ActionView = { [unowned self] in
        return self.view as! ActionView
    }()
    
    fileprivate lazy var theCurrentModel:ActionModel = {
        return ActionModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        reInitData()
    }
    func reInitData() {
        setCompanyData()
        theCurrentModel.filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)
        theCurrentView.tableView.backgroundView = nil
        theCurrentModel.arr_ActionsList?.removeAll()
        theCurrentModel.arr_ActionsList = nil
        
        theCurrentView.tableView.reloadData()
        getActionListWebService(isRefreshing: true)
    }
    //MARK:- Member Function
    func updateActionListModel(theActionDetailModel:ActionsList?) {
        guard let model = theActionDetailModel else { return }

        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
//            theCurrentModel.arr_ActionsList![index].actionlogTitle = model.actionTitle
//            theCurrentModel.arr_ActionsList![index].revisedDate = model.revisedDate
//            theCurrentModel.arr_ActionsList![index].duedate = model.duedate
//            theCurrentModel.arr_ActionsList![index].workDate = model.workDate
//            theCurrentModel.arr_ActionsList![index].percentageCompeleted = model.percentageCompeleted

            theCurrentModel.arr_ActionsList?.remove(at: index)
            theCurrentModel.arr_ActionsList?.insert(model, at: index)
            if model.percentageCompeleted == "100" {
                theCurrentModel.arr_ActionsList!.remove(at: index)
                theCurrentView.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .fade)
                if theCurrentModel.arr_ActionsList!.count == 0 {
                    setBackground(strMsg: "Action not available")
                }
            } else {
                UIView.performWithoutAnimation {
                    theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
                }
            }
        }
    }
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
            theCurrentView.updateFilterCountText(strCount: "")
        } else {
            //            let count = (Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty) ? "" : "1"
            if (Utility.shared.userData.companyId != "0" || !Utility.shared.userData.companyId.isEmpty){
                theCurrentModel.filterData.companyID = Utility.shared.userData.companyId
                theCurrentView.updateFilterCountText(strCount: "")
            }
        }
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_ActionsList == nil || theCurrentModel.arr_ActionsList?.count == 0 {
            theCurrentView.tableView.backgroundView = theCurrentView.tableView.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tableView.reloadData()
        }
    }
    
    func performDropDownAction(index:Int,item:String) {
        guard let model = theCurrentModel.arr_ActionsList?[index] else { return }
        
        switch item {
        case MoreAction.archived.rawValue:
//            archiveActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.delete.rawValue:
            deleteActionLog(strActionLogID: model.actionlogId)
            return
        case MoreAction.edit.rawValue:
            getActionDetailWebService(strActionLogID: model.actionlogId)
            break
        case MoreAction.chart.rawValue:
            presentGraphActionList(strChartId: model.actionlogId)
//            presentGraphAction(strChartId: model.actionlogId)
            break
        case MoreAction.action.rawValue:
//            redirectToAddActionScreen(theModel: model)
            break
        default:
            break
        }
        
    }
    
    func deleteActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to delete?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: true)
            //            self?.deleteStrategyService(at: index, theModel: theModel)
        }
    }
    func archiveActionLog(strActionLogID: String) {
        showAlertAction(msg: "Are you sure want to archive?") { [weak self] in
            self?.theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            self?.updateBottomCell(loadingID: strActionLogID, isLoadingApi: true)
            self?.changeActionStatusWebService(strActionLogID: strActionLogID, isDelete: false,isArchive: true)
        }
    }
    func updateBottomCell(loadingID:String,isRemove:Bool = false, isLoadingApi:Bool = false) {
        
        guard let model = theCurrentModel.arr_ActionsList else { return  }
        var indexRow = -1
        
        if let index = model.firstIndex(where: {$0.actionlogId == loadingID}) {
            indexRow = index
        }
        if isRemove && indexRow != -1 {
            if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                theCurrentModel.apiLoadingAtActionID.remove(at: index)
            }
//            theCurrentModel.apiLoadingAtActionID = "-1"
            theCurrentModel.arr_ActionsList?.remove(at: indexRow)
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadData()
            }
            if theCurrentModel.arr_ActionsList?.count == 0 {
                setBackground(strMsg: "Action not available")
            }
        } else {
            let index = IndexPath.init(row: indexRow, section: 0)
            if !isLoadingApi && !isRemove {
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == loadingID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
            }
            /*
            if !isLoadingApi || isRemove {
                theCurrentModel.apiLoadingAtActionID = "-1"
            }*/
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [index], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateRequestDueDateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId}) {
            theCurrentModel.arr_ActionsList![index].requestedRevisedDate = model.requestedRevisedDate
            theCurrentModel.arr_ActionsList![index].revisedDate = model.revisedDate
            theCurrentModel.arr_ActionsList![index].revisedColor = model.revisedColor
            theCurrentModel.arr_ActionsList![index].duedate = model.revisedDate
            theCurrentModel.arr_ActionsList![index].duedateColor = model.duedateColor
            theCurrentModel.arr_ActionsList![index].requestFlag = model.requestFlag
            theCurrentModel.arr_ActionsList![index].approveFlag = model.approveFlag
            
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateNotesCount(strActionLogID:String) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: { $0.actionlogId == strActionLogID }) {
            theCurrentModel.arr_ActionsList?[index].notesCount = 0
            let indexRow = IndexPath(row: index, section: 0)
            let contentOffset = theCurrentView.tableView.contentOffset
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func updateDate(strDate:String,type:ActionModel.dateSelectionType,date:Date, strActionID:String) {
        switch type {
        case .due:
//            selectedDueDate = date
//            selectedData["duedate"].stringValue = strDate
            break
        case .workDate:
//            selectedrevisedDate = date
//            selectedData["revisedduedate"].stringValue = strDate
            break
        }
        let changeDate = date.string(withFormat: DateFormatterInputType.inputType1)
        changeDateWebService(strActionLogID: strActionID, dateType: type, strDate: changeDate)
    }
    func updateDateAndReloadCell(strActionLogID:String,dateType:ActionModel.dateSelectionType,theModel:ChangeDate?, isLoadingApi:Bool = false) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionLogID }) {
            if isLoadingApi {
                theCurrentModel.apiLoadingAtActionID.append(strActionLogID)
            } else {
//                theCurrentModel.apiLoadingAtActionID = "-1"
                if let index = theCurrentModel.apiLoadingAtActionID.firstIndex(where: {$0 == strActionLogID}) {
                    theCurrentModel.apiLoadingAtActionID.remove(at: index)
                }
                if let model = theModel {
                    
                    switch dateType {
                    case .due:
                        theCurrentModel.arr_ActionsList![index].revisedColor = model.revisedColor
                        theCurrentModel.arr_ActionsList![index].duedateColor = model.revisedColor
                        theCurrentModel.arr_ActionsList![index].revisedDate = model.revisedDate
                        theCurrentModel.arr_ActionsList![index].duedate = model.revisedDate
                        break
                    case .workDate:
                        theCurrentModel.arr_ActionsList![index].workdateColor = model.workdateColor
                        theCurrentModel.arr_ActionsList![index].workDate = model.workDate
                        break
                    }
                }
            }
            let indexPath = IndexPath.init(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexPath], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    func updateActionListModelAndUI(theModel:ActionsList?) {
        guard let model = theModel else { return }
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == model.actionlogId }) {
            theCurrentModel.arr_ActionsList![index] = model
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    func showPopupForRequestDueDateOrDateChange(theModel:ActionsList?) {
        if UserDefault.shared.isCanApproveRequestForDueDate(strApprovedID: theModel?.approvedBy ?? "") && !(theModel?.requestedRevisedDate ?? "").isEmpty {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: true, theModel: theModel)
            
        } else {
            if UserDefault.shared.isCanRequestForDueDate(strAssignedID: theModel?.assigneeId ?? "", strApprovedID: theModel?.approvedBy ?? "", strCreatedBy: theModel?.createdBy ?? "") {
                // show reason pop
                redirectToRevisedDueDateRequestVC(isForSentRequest: true, isForDeclineRequestFromList: false, isForDisplayDeclineRequestFromList: false, theModel: theModel)
            } else {
                // change for date picker
                redirectToCustomDatePicker(selectionType: .due, selectedDate: theModel?.duedate ?? "", dueDate: theModel?.duedate ?? "", strActionID: theModel?.actionlogId ?? "")
            }
        }
    }
    
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        if let index = theCurrentModel.arr_ActionsList?.firstIndex(where: {$0.actionlogId == strActionID }) {
            theCurrentModel.arr_ActionsList?[index].percentageCompeleted = "\(percentage)"
            theCurrentModel.arr_ActionsList?[index].approveFlag = percentage == 100 ? 1 : 0
            let indexRow = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                let contentOffset = theCurrentView.tableView.contentOffset
                theCurrentView.tableView.reloadRows(at: [indexRow], with: .none)
                theCurrentView.tableView.contentOffset = contentOffset
            }
        }
    }
    
    //MARK:- Redirection
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, isForDeclineRequestFromList:Bool,isForDisplayDeclineRequestFromList:Bool, theModel:ActionsList?) {
        guard let model = theModel else { return }
        var dueDate = model.duedate
        if !model.revisedDate.isEmpty {
            dueDate = model.revisedDate
        }
        if !isForSentRequest {
            if !model.requestedRevisedDate.isEmpty {
                dueDate = model.requestedRevisedDate
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatterInputType.inputType1
        var setDueDate:Date?
        
        if let finalDate = dateFormatter.date(from: (dueDate)){
            setDueDate = finalDate
            print("revised date \(finalDate)")
        }
        if setDueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: model.actionlogId, revisedDueDate: setDueDate, reasonForDecline: model.reasonForReviseDueDate, isForSentRequest: isForSentRequest, isForDeclineRequestFromList:isForDeclineRequestFromList,isForDisplayDeclineRequestFromList:isForDisplayDeclineRequestFromList)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theActionModel)  in
            if isForSentRequest, let dueDate = dateRevisedDue {
                theModel?.requestedRevisedDate = dueDate.string(withFormat: DateFormatterInputType.inputType1)
                self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
            } else {
                /*let dateFormatter = DateFormatter()
                 if let finalDate = dateRevisedDue {
                 dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                 let strFinalDate = dateFormatter.string(from: finalDate)
                 self?.theCurrentView.lblRevisedDate.text = strFinalDate
                 self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                 self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                 self?.theCurrentModel.selectedRevisedDueDate = finalDate
                 print("revised date \(finalDate)")
                 dateFormatter.dateFormat = DateFormatterInputType.inputType1
                 self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                 }*/
                
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theActionModel) in
            theModel?.requestedRevisedDate = ""
            if let model = theActionModel {
                theModel?.duedateColor = model.duedateColor
            }
            self?.updateRequestDueDateActionListModelAndUI(theModel: theActionModel)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentGraphAction(strChartId:String = "") {
        let vc = ActionGraphVC.init(nibName: "ActionGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentGraphActionList(strChartId:String = "") {
        let vc = ActionListGraphVC.init(nibName: "ActionListGraphVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        vc.setUpChartId(strChartId: strChartId)
        print("point:=",point)
        let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setTheTopConstrain(top: point.y - 13.0, bottom: tabbarheight, chartFlagViewType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func presentActionFilter() {
        let vc = ActionNewFilterVC.init(nibName: "ActionNewFilterVC", bundle: nil)
        let point = theCurrentView.viewFilter.convert(CGPoint.zero, to: self.view.superview)
        print("point:=",point)
        vc.setTopConstraint(top: point.y + theCurrentView.viewFilter.frame.size.height + 3, filterType: .action)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        var filterData = theCurrentModel.filterData
        if theCurrentModel.assignToUserFlag == 0 {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        if filterData.assigntoID == "0" {
            filterData.assigntoID = ""
            filterData.assigntoName = ""
        }
        vc.setTheData(filterData: filterData)
        vc.handlerOnFilterSearch = {[weak self] (filterData) in
            self?.theCurrentModel.filterData = filterData

            var totalFilterCount = 0
            
            self?.theCurrentModel.nextOffset = 0
            if filterData.searchText != "" {
                totalFilterCount += 1
            }
            if filterData.searchTag != "" {
                totalFilterCount += 1
            }
            if filterData.categoryID != "" {
                totalFilterCount += 1
            }
            if filterData.relatedToValue != -1 {
                totalFilterCount += 1
            }
            if filterData.companyID != "" && filterData.companyID != Utility.shared.userData.companyId && UserRole.superAdminUser == UserDefault.shared.userRole {
                totalFilterCount += 1
            }
            if filterData.assigntoID != "" && filterData.assigntoID != "0" {
                totalFilterCount += 1
            }
            if filterData.assigntoID == "0" {
                totalFilterCount -= 1
                totalFilterCount = totalFilterCount != -1 ? totalFilterCount : 0
            } else if filterData.assigntoID.isEmpty {
                self?.theCurrentModel.filterData.assigntoID = "0"
            }
            
            
            if filterData.departmentID != ""{
                totalFilterCount += 1
            }
            if filterData.workDate != nil {
                totalFilterCount += 1
            }
            if filterData.dueDate != nil {
                totalFilterCount += 1
            }

            if totalFilterCount != 0 {
                self?.theCurrentView.updateFilterCountText(strCount: "\(totalFilterCount)")
            } else {
                self?.theCurrentView.updateFilterCountText(strCount: "")
            }
            self?.theCurrentModel.arr_ActionsList?.removeAll()
            self?.theCurrentModel.arr_ActionsList = nil
            self?.theCurrentView.tableView.backgroundView = nil
            self?.theCurrentView.tableView.reloadData()
            self?.getActionListWebService(isRefreshing: true)
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToActionArchivedVC() {
        let vc = ActionArchivedVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        self.push(vc: vc)
    }
    func redirectToActionDetailScreen(theModel:ActionsList) {
        let vc = ActionDetailVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setupData(theActionId: theModel.actionlogId, isCheckEditScreen: true)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func redirectToAddActionVC(theModel:ActionDetail) {
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { (theActionListModel) in
//            self?.updateActionListModel(theActionDetailModel: theActionDetailModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToActionEditVC(theModel:ActionDetail) {
        updateNotesCount(strActionLogID: theModel.actionLogid)
        let vc = ActionEditVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        vc.setTheData(imgBG: self.view.asImage())
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        vc.handlerEditAction = { [weak self] (theActionList) in
            self?.updateActionListModel(theActionDetailModel: theActionList)
        }
        vc.handlerReqestDueDateChangeAction = { [weak self] (theModel) in
            self?.updateRequestDueDateActionListModelAndUI(theModel: theModel)
        }
        self.push(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:ActionModel.dateSelectionType, selectedDate:String?, dueDate:String?, strActionID:String) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            if let dueDate = selectedDate, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .workDate:
            if let reviseDate = selectedDate, !reviseDate.isEmpty,let date = reviseDate.convertToDate(formate: DateFormatterInputType.inputType1) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            if selectionType == .workDate {
                if let strDueDate = dueDate, !strDueDate.isEmpty,let dateDue = strDueDate.convertToDate(formate: DateFormatterInputType.inputType1), date > dateDue  {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self?.showAlertAtBottom(message: "Work date must be less than Due date")
                    })
                    return
                }
            }
            
            self?.updateDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnRisksGraphAction(_ sender: Any) {
        presentGraphAction()
    }
    @IBAction func onBtnBackSubControllerAction(_ sender: Any) {
        /*var title = ""
         if theCurrentModel.subViewControllers.indices.contains(theCurrentModel.subViewControllers.count - 2) {
         title = theCurrentModel.subViewControllers[theCurrentModel.subViewControllers.count - 2].strTitle
         }
         
         if let vc = theCurrentModel.subViewControllers.last {
         removeContentController(content: vc)
         theCurrentView.updateTitleText(strTitle: title)
         theCurrentModel.subViewControllers.removeLast()
         if theCurrentModel.subViewControllers.count == 1 {
         theCurrentView.backButtonRisk(isHidden: true)
         }
         print("theCurrentModel.subViewControllers:=",theCurrentModel.subViewControllers.count)
         }*/
    }
    
    @IBAction func onBtnArchivedAction(_ sender: Any) {
        redirectToActionArchivedVC()
    }
    
    @IBAction func onBtnFilterAction(_ sender: Any) {
        presentActionFilter()
    }
    
}
//MARK:-UITableViewDataSource
extension ActionVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = tableView.backgroundView , (theCurrentModel.arr_ActionsList == nil || theCurrentModel.arr_ActionsList?.count == 0) {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_ActionsList?.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: theCurrentModel.identifier) as! ActionCell
        cell.showMoreButtonForAction(isHidden: true)
        if theCurrentModel.arr_ActionsList != nil {
            var isApiLoading = false
            if theCurrentModel.apiLoadingAtActionID.contains(theCurrentModel.arr_ActionsList![indexPath.row].actionlogId) {
                isApiLoading = true
            }
            cell.configureActions(theModel: theCurrentModel.arr_ActionsList![indexPath.row], isApiLoading: isApiLoading)
            cell.handleDueDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.showPopupForRequestDueDateOrDateChange(theModel: self?.theCurrentModel.arr_ActionsList?[indexPath.row])
                }
            }
            cell.handleWorkDateAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.redirectToCustomDatePicker(selectionType: .workDate, selectedDate: self?.theCurrentModel.arr_ActionsList?[indexPath.row].workDate ?? "", dueDate: self?.theCurrentModel.arr_ActionsList?[indexPath.row].duedate ?? "", strActionID: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerPercentCompletedAction = { [weak self] in
                if !UserDefault.shared.isArchiveAction(strStatus: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogStatus ?? "") {
                    self?.presentPrecentageCompletedVC(percentage: Int((self?.theCurrentModel.arr_ActionsList?[indexPath.row].percentageCompeleted ?? "0")) ?? 0, strActionID: self?.theCurrentModel.arr_ActionsList?[indexPath.row].actionlogId ?? "")
                }
            }
            cell.handlerMoreAction = { [weak self, unowned cell] in
                if let obj = self {
                    obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.moreDD, view: cell.btnMore, tableContentView: cell.contentView, at: indexPath.row, theModel: obj.theCurrentModel.arr_ActionsList![indexPath.row])
                    obj.theCurrentModel.moreDD.show()
                }
            }
            
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.nextOffset != 0 {
                getActionListWebService()
            }
        } else {
            cell.startAnimation()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0 else { return false }
        if theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            return false
        }
        return true
    }
}

//MARK:-UITableViewDelegate
extension ActionVC:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && theCurrentModel.nextOffset != -1 && theCurrentModel.arr_ActionsList != nil {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = appGreen
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            theCurrentView.tableView.tableFooterView = spinner
            theCurrentView.tableView.tableFooterView?.isHidden = false
        } else {
            theCurrentView.tableView.tableFooterView?.isHidden = true
            theCurrentView.tableView.tableFooterView = nil
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) else { return nil }
        
        let chart = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Chart") { [unowned self] (action, index) in
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Delete") { [unowned self] (action, index) in
                print("Delete")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            chart.backgroundColor = appPink
            return [delete, chart]
        }
        return [chart]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //        guard let model = theCurrentModel.arr_ApprovalList else { return nil }
        guard let arr_model = theCurrentModel.arr_ActionsList, arr_model.count > 0, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.section].actionlogId) else { return nil }
        
        let chart = UIContextualAction(style: UIContextualAction.Style.normal, title: "Chart") { [unowned self] (action, view, completionHandler) in
            completionHandler(true)
            print("Chart")
            self.presentGraphActionList(strChartId: arr_model[indexPath.row].actionlogId)
        }
        chart.backgroundColor = appGreen
        var configure = UISwipeActionsConfiguration(actions: [chart])
        
        if Utility.shared.userData.id == arr_model[indexPath.row].createdBy {
            let delete = UIContextualAction(style: UIContextualAction.Style.normal, title: "Delete") { [unowned self] (action, view, completionHandler) in
                completionHandler(true)
                print("Chart")
                self.deleteActionLog(strActionLogID: arr_model[indexPath.row].actionlogId)
            }
            delete.backgroundColor = appPink
            configure = UISwipeActionsConfiguration(actions: [delete,chart])
        }
        configure.performsFirstActionWithFullSwipe = false
        return configure
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let arr_model = theCurrentModel.arr_ActionsList, !theCurrentModel.apiLoadingAtActionID.contains(arr_model[indexPath.row].actionlogId) {
            if theCurrentModel.arr_ActionsList![indexPath.row].actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
                self.redirectToActionDetailScreen(theModel: theCurrentModel.arr_ActionsList![indexPath.row])
            } else {
                performDropDownAction(index: indexPath.row, item: MoreAction.edit.rawValue)
            }
        }
    }
}

//MARK:-  NavigationActionVC
class NavigationActionVC: UINavigationController {
    
}
/*
extension ActionVC {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -50 { //change 80 to whatever you want
            if  !self.theCurrentView.refreshControl.isRefreshing {
//                handleRefresh()
                self.theCurrentView.refresh()
            }
        }
    }
}*/
//MARK:- Api Call
extension ActionVC {
    
    func getActionListWebService(isRefreshing:Bool = false) {
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        var workDate = ""
        var dueDate = ""
        
        if let workDateFormate = theCurrentModel.filterData.workDate, !workDateFormate.isEmpty {
//            workDate = workDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            workDate = workDateFormate
        }
        if let dueDateFormate = theCurrentModel.filterData.dueDate, !dueDateFormate.isEmpty {
//            dueDate = dueDateFormate.string(withFormat: DateFormatterInputType.inputType1)
            dueDate = dueDateFormate
        }
        
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
//                         APIKey.key_tags:theCurrentModel.filterData.searchTag,
                         APIKey.key_search:theCurrentModel.filterData.searchText,
                         APIKey.key_department_id:theCurrentModel.filterData.departmentID,
                         APIKey.key_assign_to:theCurrentModel.filterData.assigntoID,
                         APIKey.key_company_id:theCurrentModel.filterData.companyID,
                         APIKey.key_workdatefilter:workDate,
                         APIKey.key_duedatefilter:dueDate,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        if theCurrentModel.filterData.relatedToValue != -1 {
            parameter[APIKey.key_related_to] = "\(theCurrentModel.filterData.relatedToValue)"
        }
        
        ActionsWebService.shared.getAllActionsList(parameter: parameter, success: { [weak self] (list, nextOffset, assignToUserFlag) in
            if assignToUserFlag == 1 && (self?.theCurrentModel.nextOffset ?? -1) == 0 { // Need to set filter count
                self?.theCurrentModel.assignToUserFlag = assignToUserFlag
                self?.theCurrentView.updateFilterCountText(strCount: "1")
                self?.theCurrentModel.filterData.assigntoID = Utility.shared.userData.id
            }
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.theCurrentView.refreshControl.endRefreshing()
            self?.theCurrentView.tableView.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.updateList(list: [], offset: 0, isRefreshing: true)
                self?.setBackground(strMsg: error)
                self?.theCurrentView.refreshControl.endRefreshing()
        })
    }
    
    func getActionDetailWebService(strActionLogID:String) {
        /*var alert = UIAlertController()
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.tintColor = appGreen
        loadingIndicator.color = appGreen
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: false, completion: nil)*/
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            Utility.shared.stopActivityIndicator()
//              alert.dismiss(animated: false, completion: nil)
//              self?.redirectToAddActionVC(theModel: detail)
              self?.redirectToActionEditVC(theModel: detail)
            }, failed: { [weak self] (error) in
                Utility.shared.stopActivityIndicator()

//                alert.dismiss(animated: false, completion: nil)
                self?.showAlertAtBottom(message: error)
        })
    }
    
    func changeActionStatusWebService(strActionLogID:String, isDelete:Bool, isArchive:Bool = false) {
        guard let jsonString = JSON([[APIKey.key_status_id:strActionLogID]]).rawString() else {
            updateBottomCell(loadingID: strActionLogID)
            return
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_status:APIKey.key_deleted.localizedLowercase,
                         APIKey.key_status_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_status_for_id:jsonString] as [String : Any]
        if isArchive {
            parameter[APIKey.key_status] = APIKey.key_archived.localizedLowercase
        }
        
        RiskWebservice.shared.deleteRisk(parameter: parameter, success: { [weak self] (msg) in
            self?.updateBottomCell(loadingID: strActionLogID, isRemove: true)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateBottomCell(loadingID: strActionLogID)
        })
    }
    
    func changeDateWebService(strActionLogID:String,dateType:ActionModel.dateSelectionType, strDate:String) {
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_date_for:APIKey.key_assign_to_for_action_logs,
                         APIKey.key_action_id:strActionLogID] as [String : Any]
        
        switch dateType {
        case .due:
            parameter[APIKey.key_revised_date] = strDate
            break
        case .workDate:
            parameter[APIKey.key_work_date] = strDate
            break
        }
        self.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: true)
        ActionsWebService.shared.changeDate(parameter: parameter, success: { [weak self] (msg, theModel) in
            self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: theModel, isLoadingApi: false)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.updateDateAndReloadCell(strActionLogID: strActionLogID, dateType: dateType, theModel: nil, isLoadingApi: false)
        })
        
    }
   
}
