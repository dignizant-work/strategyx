//
//  ActionEditView.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import HTagView
import Loady

class ActionEditView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var lblAddTitle: UILabel!
    @IBOutlet weak var img_Bg: UIImageView!
    @IBOutlet weak var tableView: DragAndDropTableView!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!

    @IBOutlet weak var tblNotesHistory: UITableView!
    @IBOutlet weak var heightOfNotesHistory: NSLayoutConstraint!

    @IBOutlet weak var lblRevisedDate: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
//    @IBOutlet weak var lblRelatedTo: UILabel!
    @IBOutlet weak var viewRelatedToTxt: UIView!
    
    @IBOutlet weak var viewTag: HTagView!
    @IBOutlet weak var constrainViewTagHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewSelectedCompany: UIView!
    @IBOutlet weak var btnSelectCompanyOutlet: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var activityCompanyIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgCompany: UIImageView!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var activityAssignToIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgAssignTo: UIImageView!
    @IBOutlet weak var btnAssignedOutlet: UIButton!
    
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var activityDepartmentIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgDepartment: UIImageView!
    @IBOutlet weak var btnDepartmentOutlet: UIButton!
    
    @IBOutlet weak var lblApprovedBy: UILabel!
    @IBOutlet weak var activityApprovedIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgApproved: UIImageView!
    @IBOutlet weak var btnApprovedOutlet: UIButton!
    
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblWorkDate: UILabel!
    
    @IBOutlet weak var lblEditWorkDate: UILabel!
    @IBOutlet weak var viewEditWorkDate: UIView!
    
    @IBOutlet weak var viewEditRevisedDueDate: UIView!
    @IBOutlet weak var lblEditRevisedDueDate: UILabel!
    
    @IBOutlet weak var viewWorkDate: UIView!
    @IBOutlet weak var txtDuration: UITextField!
    @IBOutlet weak var txtSubTaskTitle: UITextField!
    @IBOutlet weak var lblAttachmentFile: UILabel!
    
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var viewFileName: UIView!
    @IBOutlet weak var collectionSelectedFile: UICollectionView!
    @IBOutlet weak var heightOfCollectionSelectedFile: NSLayoutConstraint!
//    @IBOutlet weak var lblVoiceNotesName: UILabel!
    
    @IBOutlet weak var lblDesriptionTitle: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    @IBOutlet weak var viewOfSubTask: UIView!
   
    @IBOutlet weak var viewOfCompleted: UIView!
    @IBOutlet weak var txtComopleted: UITextField!
    @IBOutlet weak var viewOfRevisedDate: UIView!
    @IBOutlet weak var btnAdd: LoadyButton!
//    @IBOutlet weak var viewOfAudio: UIView!
    @IBOutlet weak var btnDueDateOutlet: UIButton!
    
    @IBOutlet weak var viewOfPager: UIView!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var txtNotesHistory: UITextField!

    @IBOutlet weak var constrainViewPagerHeight: NSLayoutConstraint! // 40.0
    
    @IBOutlet weak var lblBGOfRelatedTo: UILabel!
    @IBOutlet weak var constrainOfLblBGofRelatedWidth: NSLayoutConstraint! // 0.0
    @IBOutlet weak var constrainOflblBGofRelatedHeight: NSLayoutConstraint! // 0.0
    
    @IBOutlet weak var constrainClearSubTaskWidth: NSLayoutConstraint! // 0
    @IBOutlet weak var btnClearSubTask: UIButton!
    @IBOutlet weak var btnAddsubTask: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet weak var constrainViewNoteHeight: NSLayoutConstraint! //98
    
    @IBOutlet weak var btnAddVoiceNote: UIButton!
    @IBOutlet weak var constrainViewfileNameHeight: NSLayoutConstraint! // 30.0
    
    @IBOutlet weak var viewAddSubTask: UIView!
    @IBOutlet weak var constrainViewAddSubTaskHeight: NSLayoutConstraint! // 30.0
    
    @IBOutlet weak var btnPlusAddTag: UIButton!
    
    
    @IBOutlet weak var viewRelatedToGeneralAction: UIView!
    @IBOutlet weak var viewSelectionofRelatedToGeneralAction: UIView!
    @IBOutlet weak var lblRelatedToGeneralAction: UILabel!
    
    @IBOutlet weak var viewRelatedToOtherAction: UIView!
    @IBOutlet weak var lblTitleRelatedToOtherAction: UILabel!

    @IBOutlet weak var viewSelectionofRelatedToOtherAction: UIView!
    @IBOutlet weak var lblRelatedToOtherAction: UILabel!
    
    @IBOutlet weak var viewReviseddueDateRequest: UIView!
    
    @IBOutlet weak var lblRevisedDueDateRequest: UILabel!
    
    @IBOutlet weak var viewRevisedDueDateClickToView: UIView!
    
    @IBOutlet weak var constrainViewRevisedDueDateClickToViewHeight: NSLayoutConstraint! // 28
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        viewClearRevisedDueDateRequest()
        viewEditWorkDate.isHidden = true
        viewEditRevisedDueDate.isHidden = true
        activitySpinner.color = appGreen
       /* DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
            self.viewFileName.addDashedBorder(color: appGreen)
        })*/
    }
    
    func setupTableView(theDelegate:ActionEditVC) {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tblNotesHistory.estimatedRowHeight = 50.0
        tblNotesHistory.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(SubTaskNewCell.self)
//        tableView.registerCellNib(InputFieldTVCell.self)
//        tableView.registerCellNib(InputDescriptionTVCell.self)
//        tableView.registerCellNib(SelectionTypeTVCell.self)
//        tableView.registerCellNib(BottomButtonTVCell.self)
//        tableView.registerCellNib(CreatedByTVCell.self)
//        tableView.registerCellNib(RiskLevelTVCell.self)
//        tableView.registerCellNib(InputDateCell.self)
//        tableView.registerCellNib(AddActionDateTVCell.self)
        
        tblNotesHistory.registerCellNib(HistoryCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
//        tableView.isEditing = true
        
        tblNotesHistory.delegate = theDelegate
        tblNotesHistory.dataSource = theDelegate
        tblNotesHistory.separatorStyle = .none

    }
    func setupTagView(theDelegate:ActionEditVC)  {
        viewTag.delegate = theDelegate
        viewTag.dataSource = theDelegate
        viewTag.multiselect = false
        viewTag.marg = 2
        viewTag.btwTags = 2
        viewTag.btwLines = 2
        viewTag.tagFont = R12
        viewTag.tagMainBackColor = appBgLightGrayColor
        viewTag.tagMainTextColor = white
        viewTag.tagSecondBackColor = appBgLightGrayColor
        viewTag.tagSecondTextColor = white
//         viewTag?.tagCancelIconRightMargin = 10
//        viewTag.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        
        //        tagView.reloadData()
        //        tagView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        /*
         [tagView].forEach { (viewTag) in
         viewTag?.delegate = self
         viewTag?.dataSource = self
         viewTag?.multiselect = false
         viewTag?.marg = 2
         viewTag!.btwTags = 2
         viewTag?.btwLines = 2
         viewTag?.tagFont = R12
         viewTag?.tagMainBackColor = appBgLightGrayColor
         viewTag?.tagMainTextColor = white
         viewTag?.tagSecondBackColor = appBgLightGrayColor
         viewTag?.tagSecondTextColor = white
         // viewTag?.tagCancelIconRightMargin = 10
         viewTag?.tagContentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
         viewTag?.reloadData()
         }*/
        
        viewTag.reloadData()
        updateTagView()
    }
    
    func setupCollectionView(theDelegate:ActionEditVC)  {
        collectionSelectedFile.delegate = theDelegate
        collectionSelectedFile.dataSource = theDelegate
        collectionSelectedFile.registerCellNib(SelectFileCell.self)
        collectionSelectedFile.reloadData()
    }
    
    
    
    func updateTagView() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.constrainViewTagHeight.constant = self.viewTag.frame.size.height
            if self.viewTag.frame.size.height <= 0{
                self.constrainViewTagHeight.constant = 40
            }
            self.layoutIfNeeded()
        }
    }
    
    func setupActivityIndicator(){
        [activityCompanyIndicator,activityAssignToIndicator,activityDepartmentIndicator,activityApprovedIndicator,activitySpinner].forEach { (activityIndicator) in
            configureActivityIndicator(activityIndicator: activityIndicator)
        }
    }
    
    func configureActivityIndicator(activityIndicator:UIActivityIndicatorView) {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
    }
    
    func statusActivityIndicator(isLoading:Bool=false,activityIndicator:UIActivityIndicatorView,imgView:UIImageView){
        if isLoading == true{
            activityIndicator.color = appGreen
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            imgView.isHidden = true
        }else{
            activityIndicator.stopAnimating()
            imgView.isHidden = false
        }
    }
    
    func isEnableView(isEnable:Bool,view:UIView) {
        view.isUserInteractionEnabled = isEnable ? true : false
        view.alpha = isEnable ? 1.0 : 0.6
    }
    
    func viewRevisedDueDateRequest(isForSent:Bool, strRevisedDueDate:String) {
        viewRevisedDueDateClickToView.isHidden = true
        viewReviseddueDateRequest.isHidden = false
        
        var message = "Revised due date request sent for " + strRevisedDueDate
        if !isForSent {
            message = "Request revised for revised due date. " + strRevisedDueDate
            viewRevisedDueDateClickToView.isHidden = false
        }
        lblRevisedDueDateRequest.text = message
        constrainViewRevisedDueDateClickToViewHeight.constant = viewRevisedDueDateClickToView.isHidden ? 0.0 : 28

    }
    func viewClearRevisedDueDateRequest() {
        viewRevisedDueDateClickToView.isHidden = true
        constrainViewRevisedDueDateClickToViewHeight.constant = 0.0
        viewReviseddueDateRequest.isHidden = true
        lblRevisedDueDateRequest.text = ""
    }
    
    
}
