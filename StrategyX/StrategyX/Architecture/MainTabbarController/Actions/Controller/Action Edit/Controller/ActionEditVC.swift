//
//  ActionEditVC.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import MobileCoreServices
import IQAudioRecorderController
//import HTagView
import SwiftyJSON
import AVKit

class ActionEditVC: ParentViewController {

    //MARK:- Variable
    enum categorySelectionType:Int {
        case company                                = 2
        case assignTo                               = 3
        case department                             = 4
        case relatedTo                              = 5
        case relatedToStrategyPrimaryArea           = 6
        case relatedToStrategySecondaryArea         = 7
        case relatedToStrategyGoal                  = 8
        case relatedToRisk                          = 9
        case relatedToFocus                         = 10
        case relatedToTacticalProject               = 11
        case approvedBy                             = 12
        case selectTag                              = 13
    }
    
   
    
    enum dateSelectionType:Int {
        case created             = 1
        case due                 = 2
        case work                = 3
        case revisedDate         = 4
    }
    
    fileprivate lazy var theCurrentView:ActionEditView = { [unowned self] in
       return self.view as! ActionEditView
    }()
    
    fileprivate lazy var theCurrentModel:ActionEditModel = {
       return ActionEditModel(theController: self)
    }()
    var isObserverDeallocate = true
    
    var handlerEditAction:(ActionsList?) -> Void = {_ in }
    var handlerReqestDueDateChangeAction:(_ theModel:ActionsList?) -> Void = {_ in }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isObserverDeallocate = false
        setSideMenuLeftNavigationBarItem()
        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        theCurrentView.collectionSelectedFile.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        theCurrentView.tblNotesHistory.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isObserverDeallocate = true
        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.collectionSelectedFile.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.tblNotesHistory.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize", let tabl = object as? UITableView {
            
//            self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height

            if tabl == theCurrentView.tableView  {
                self.theCurrentView.constrainTableViewHeight.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height
            }
            
            if tabl == theCurrentView.tblNotesHistory {
                self.theCurrentView.heightOfNotesHistory.constant = tabl.contentSize.height <= 0 ? 0 : tabl.contentSize.height

            }
            self.view.layoutIfNeeded()
            print("tabl.contentSize.height:=",tabl.contentSize.height)
        }
        if keyPath == "contentSize", let coll = object as? UICollectionView {
            self.theCurrentView.heightOfCollectionSelectedFile.constant = coll.contentSize.height
            self.view.layoutIfNeeded()
            print("coll.contentSize.height:=",coll.contentSize.height)
        }
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        theCurrentView.setupTableView(theDelegate: self)
//        theCurrentView.setupTagView(theDelegate: self)
        theCurrentView.setupCollectionView(theDelegate: self)
        theCurrentView.setupActivityIndicator()
        theCurrentModel.arr_noteHistoryStatus = Utility.shared.setArrayNotesHistoryStatus()
        
        if theCurrentModel.isEditScreen == .edit {
            theCurrentModel.actionId = theCurrentModel.theActionDetailModel?.actionLogid ?? ""
            theCurrentView.viewEditWorkDate.isHidden = false
            theCurrentView.viewEditRevisedDueDate.isHidden = false
            theCurrentView.viewWorkDate.isHidden = true
            theCurrentView.viewOfRevisedDate.isHidden = true

            setupEditData()
        }
        theCurrentView.constrainViewPagerHeight.constant = 0.0
        loadViewMoreNoteHistory(isAnimating: true)
        getActionNotesHistoryWebService(isRefreshing: true, status: self.theCurrentModel.strNoteStatus)

//        actionTagListService()
//        self.theCurrentView.lblCreatedBy.text = "\(Utility.shared.userData.firstname) \(Utility.shared.userData.lastname)"
        isDisableCompanySelection()
        
    }
    
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    
    func setUpdateScreen(editType:viewType,actionDetail:ActionDetail) {
        theCurrentModel.isEditScreen = editType
        theCurrentModel.theActionDetailModel = actionDetail
        theCurrentModel.actionId = actionDetail.actionLogid
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func setupEditData() {
        
        if theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
            theCurrentView.btnAdd.isHidden = true
//             let allButtons  = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            
            theCurrentView.viewNotes.isHidden = true
            theCurrentView.constrainViewNoteHeight.constant = 0.0
            
            theCurrentView.viewFileName.isHidden = true
            theCurrentView.btnAddVoiceNote.isHidden = true
            theCurrentView.constrainViewfileNameHeight.constant = 0.0
            
            theCurrentView.viewAddSubTask.isHidden = true
            theCurrentView.constrainViewAddSubTaskHeight.constant = 0.0
            
            theCurrentView.btnPlusAddTag.isHidden = true
            
            let view = theCurrentView.scrollView.allSubViewsOf(type: UIView.self)
            view.forEach({$0.isUserInteractionEnabled = false})
            
            theCurrentView.scrollView.isUserInteractionEnabled = true
            if let view = theCurrentView.scrollView.viewWithTag(1890) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            
            if let view = theCurrentView.scrollView.viewWithTag(1891) {
                view.isUserInteractionEnabled = true
                self.theCurrentView.btnCancel.isUserInteractionEnabled = true
            }
            if let view = theCurrentView.scrollView.viewWithTag(1700) {
                view.isUserInteractionEnabled = true
                if let viewsub = view.viewWithTag(1701) {
                    viewsub.isUserInteractionEnabled = true
                    if let viewsub1 = viewsub.viewWithTag(1702) {
                        viewsub1.isUserInteractionEnabled = true
                    }
                }
                
            }
        
            if let view = theCurrentView.scrollView.viewWithTag(200) {
                view.isUserInteractionEnabled = true
            }
            theCurrentView.viewOfPager.isUserInteractionEnabled = true
            theCurrentView.viewMore.isUserInteractionEnabled = true
            theCurrentView.tableView.isUserInteractionEnabled = false
        }
        
        theCurrentView.isEnableView(isEnable: false, view: theCurrentView.viewSelectionofRelatedToOtherAction)
        theCurrentView.isEnableView(isEnable: false, view: theCurrentView.viewSelectionofRelatedToGeneralAction)
        theCurrentView.lblTitleRelatedToOtherAction.text = theCurrentModel.theActionDetailModel!.label
        theCurrentView.lblRelatedToGeneralAction.text = theCurrentModel.theActionDetailModel!.label
        theCurrentView.lblRelatedToOtherAction.text = theCurrentModel.theActionDetailModel!.relatedTitle

        if theCurrentModel.theActionDetailModel!.relatedId == 0 {
            theCurrentView.viewRelatedToOtherAction.isHidden = true
        }
        
        theCurrentView.lblAddTitle.text = "Action"
        theCurrentView.lblCreatedBy.text = theCurrentModel.theActionDetailModel?.createdBy
        theCurrentView.txtTitle.text = theCurrentModel.theActionDetailModel?.actionTitle
        if theCurrentModel.theActionDetailModel?.action_description != ""{
            theCurrentView.lblPlaceHolder.isHidden = true
        }
        theCurrentView.txtDescription.text = theCurrentModel.theActionDetailModel?.action_description

//        let width = (theCurrentModel.theActionDetailModel?.label ?? "").width(withConstrainedHeight: 15.0, font: R15)
//        let height = (theCurrentModel.theActionDetailModel?.label ?? "").height(withConstrainedWidth: width, font: R15)
//        theCurrentView.lblBGOfRelatedTo.frame.size = CGSize(width: width, height: height)
//        theCurrentView.constrainOfLblBGofRelatedWidth.constant = width + 8
//        theCurrentView.constrainOflblBGofRelatedHeight.constant = height + 2
//        theCurrentView.lblBGOfRelatedTo.cornerRadius = height / 2
        let attributes1 = [
            NSAttributedString.Key.font : R15,
            NSAttributedString.Key.foregroundColor: appTextLightGrayColor
            ] as [NSAttributedString.Key : Any]
        let range = NSString(string: (theCurrentModel.theActionDetailModel?.label ?? "") + "  " + (theCurrentModel.theActionDetailModel?.relatedTitle ?? "")).range(of: (theCurrentModel.theActionDetailModel?.label ?? ""))
        let attributeString = NSMutableAttributedString(string: (theCurrentModel.theActionDetailModel?.label ?? "") + "  " + (theCurrentModel.theActionDetailModel?.relatedTitle ?? ""), attributes: attributes1)
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: white, range: range)
//        let attributeStringLbl = attributedString(string: (theCurrentModel.theActionDetailModel?.label ?? ""), color: white, font: R15)
//        let attributeStringTitle = attributedString(string: "  " + (theCurrentModel.theActionDetailModel?.relatedTitle ?? ""), color: appTextLightGrayColor, font: R15)
//        theCurrentView.lblRelatedTo.attributedText = attributeString

        theCurrentView.lblCompany.text = theCurrentModel.theActionDetailModel?.companyName
        theCurrentView.lblAssignTo.text = theCurrentModel.theActionDetailModel?.assignedTo
        theCurrentView.lblDepartment.text = theCurrentModel.theActionDetailModel?.assignedDepartment
        theCurrentView.lblApprovedBy.text = theCurrentModel.theActionDetailModel?.approvedBy
        
        theCurrentModel.selectedData["company"].stringValue = theCurrentModel.theActionDetailModel?.companyName ?? ""
        theCurrentModel.selectedData["assignto"].stringValue =  theCurrentModel.theActionDetailModel?.assignedTo ?? ""
        theCurrentModel.selectedData["department"].stringValue = theCurrentModel.theActionDetailModel?.assignedDepartment ?? ""
        theCurrentModel.selectedData["approvedby"].stringValue =  theCurrentModel.theActionDetailModel?.approvedBy ?? ""

        theCurrentModel.strSelectedCompanyID = theCurrentModel.theActionDetailModel?.assigneeCompanyId ?? "0"
        theCurrentModel.strSelectedUserAssignID = theCurrentModel.theActionDetailModel?.assigneeId ?? "0"
        theCurrentModel.strSelectedDepartmentID = theCurrentModel.theActionDetailModel?.assignedDepartmentId ?? "0"
        theCurrentModel.strSelectedApprovedID = theCurrentModel.theActionDetailModel?.approvedById ?? "0"
        DispatchQueue.main.async { [weak self] in
            self?.theCurrentView.txtDescription.setContentOffset(CGPoint.zero, animated: false)
        }
        theCurrentModel.arr_selectedFile.removeAll()
        if let voiceNotes = theCurrentModel.theActionDetailModel?.voiceNotes {
            for item in voiceNotes {
                let attchment = AttachmentFiles(fileName: item.voicenotes, fileData: nil, fileType: .audio, attachmentID: item.voicenotesId, isOldAttachment: true)
                theCurrentModel.arr_selectedFile.append(attchment)
            }
        }
        
        if let attachment = theCurrentModel.theActionDetailModel?.attachment {
            for item in attachment {
                let attchment = AttachmentFiles(fileName: item.attachment, fileData: nil, fileType: .file, attachmentID: item.attachmentId, isOldAttachment: true)
                theCurrentModel.arr_selectedFile.append(attchment)
            }
        }
        
        /*
        if theCurrentModel.theActionDetailModel?.note_file != "" {
//            theCurrentView.viewOfAudio.isHidden = false
            let strFileName = URL(string: (theCurrentModel.theActionDetailModel?.note_file)!)?.lastPathComponent
//            self.theCurrentView.lblVoiceNotesName.text = strFileName
            let objSelectedData = SelectedFile.init(selectedAudioFileName: strFileName ?? "Attachment-\(Int(Date().timeIntervalSince1970))", selectedAudioFileData: Data())
            theCurrentModel.arr_selectedAudioFile.append(objSelectedData)
        }*/
        
        if let strFinalDate = theCurrentModel.theActionDetailModel?.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
            theCurrentView.lblCreatedDate.text = strFinalDate
            theCurrentModel.selectedData["createddate"].stringValue = strFinalDate
        }else{
            theCurrentView.lblCreatedDate.text = "-"
        }
        
        if theCurrentModel.theActionDetailModel?.duedate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.duedate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblDueDate.text = strFinalDate
                theCurrentModel.selectedData["duedate"].stringValue = strFinalDate
                theCurrentModel.selectedDueDate = finalDate
                print("finalDatefinalDate \(finalDate)")
            }
            
        }else{
            theCurrentView.lblDueDate.text = "-"
        }
        
        
        if theCurrentModel.theActionDetailModel?.workDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.workDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblWorkDate.text = strFinalDate
                theCurrentView.lblEditWorkDate.text = strFinalDate
                theCurrentModel.selectedData["workdate"].stringValue = strFinalDate
                theCurrentModel.selectedWorkDate = finalDate
                print("work date \(finalDate)")
            }
            
        }else{
            theCurrentView.lblWorkDate.text = "Select work date"
            theCurrentView.lblEditWorkDate.text = "Select work date"
        }
        
//        theCurrentView.viewOfRevisedDate.isHidden = false
        
        if theCurrentModel.theActionDetailModel?.revisedDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.revisedDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblRevisedDate.text = strFinalDate
                theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                theCurrentModel.selectedRevisedDueDate = finalDate
                print("revised date \(finalDate)")
            }
            
        }else{
            theCurrentView.lblRevisedDate.text = "Select revised date"
            theCurrentView.lblEditRevisedDueDate.text = "Select revised date"
        }
        if theCurrentModel.theActionDetailModel?.requestedRevisedDate != "" && theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() != APIKey.key_archived.lowercased() {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.requestedRevisedDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                print("request revised date \(finalDate)")
                theCurrentView.viewRevisedDueDateRequest(isForSent: theCurrentModel.theActionDetailModel!.approvedById != Utility.shared.userData.id, strRevisedDueDate: strFinalDate)
            }
        }
        
        if let duration = Int((theCurrentModel.theActionDetailModel?.duration ?? "-1")), (duration > 0) {
           let timeInterval = TimeInterval(duration * 60)
            theCurrentModel.selectedDuration = String(format: "%0.2d:%0.2d", timeInterval.hours,timeInterval.minutes).convertToDate(formate: DateFormatterOutputType.outputType12)
//            print("theCurrentModel.selectedDuration:=",theCurrentModel.selectedDuration)
            theCurrentView.txtDuration.text = String(format: "%0.2d:%0.2d", timeInterval.hours,timeInterval.minutes)
        }
        theCurrentView.txtComopleted.text = theCurrentModel.theActionDetailModel?.percentageCompeleted
        
//        setupDefaulTagData()
        
        
       // arr_fileList  = []
//        theCurrentModel.arr_fileList = Array((theCurrentModel.theActionDetailModel?.fileList)!)
//        for i in 0..<theCurrentModel.arr_fileList.count{
//            let strFileName = URL(string: theCurrentModel.arr_fileList[i].fileName)?.lastPathComponent
//            let objSelectedData = SelectedFile.init(selectedFileName: strFileName ?? "Attachment-\(Int(Date().timeIntervalSince1970))", selectedFileData: Data())
//            theCurrentModel.arr_selectedFile.append(objSelectedData)
//        }
        theCurrentView.collectionSelectedFile.reloadData()
        
        theCurrentView.viewOfSubTask.isHidden = false
       
        theCurrentModel.arr_subTaskList = Array((theCurrentModel.theActionDetailModel?.subTaskList)!)
        
        theCurrentView.viewOfSubTask.isHidden = theCurrentModel.arr_subTaskList.count == 0 && theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased()
        
//        theCurrentModel.arr_SubTaskList = theCurrentModel.arr_subTaskList.map({$0.subTaskName})
        theCurrentView.viewOfCompleted.isHidden = false
        self.theCurrentView.tableView.reloadData()
        if !UserDefault.shared.isCanEditForm(strOppoisteID: theCurrentModel.theActionDetailModel?.createdById ?? "") {
            theCurrentView.viewCompany.isHidden = true
            theCurrentView.btnPlusAddTag.isHidden = true
            theCurrentView.txtTitle.isUserInteractionEnabled = false
            theCurrentView.txtDescription.isUserInteractionEnabled = false
            [theCurrentView.btnAssignedOutlet,theCurrentView.btnDepartmentOutlet,theCurrentView.btnApprovedOutlet].forEach { (btn) in
                btn?.isUserInteractionEnabled = false
            }
        } else {
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
//                companylistService(activityIndicator: theCurrentView.activityCompanyIndicator, imgView: theCurrentView.imgCompany)
            } else {
                theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
                theCurrentView.lblCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompany.isHidden = true
            }
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
            departmentListService(activityIndicator: theCurrentView.activityDepartmentIndicator, imgView: theCurrentView.imgDepartment)
        }
        theCurrentView.isEnableView(isEnable: false, view: theCurrentView.viewSelectedCompany)
        
       /* if theCurrentModel.theActionDetailModel?.createdById != Utility.shared.userData.id {
            [theCurrentView.btnSelectCompanyOutlet,theCurrentView.btnAssignedOutlet,theCurrentView.btnDepartmentOutlet,theCurrentView.btnApprovedOutlet].forEach { (btn) in
                btn?.isUserInteractionEnabled = false
            }
        } else {
            if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
                companylistService(activityIndicator: theCurrentView.activityCompanyIndicator, imgView: theCurrentView.imgCompany)
            } else {
                theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
                theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
                theCurrentView.lblCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompany.isHidden = true
            }
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
            departmentListService(activityIndicator: theCurrentView.activityDepartmentIndicator, imgView: theCurrentView.imgDepartment)

        }*/
        
        theCurrentView.btnDueDateOutlet.isUserInteractionEnabled = false
        theCurrentView.btnAdd.setTitle("Save", for: .normal)
    }
    
    func setupDefaulTagData(){
        theCurrentModel.arr_EditTagList = []
        theCurrentModel.arr_EditTagList = Array((theCurrentModel.theActionDetailModel?.tagsList)!)
        
        theCurrentModel.arr_AddTagList = theCurrentModel.arr_EditTagList.map({$0.tag_name})
        
        for i in 0..<theCurrentModel.arr_EditTagList.count{
            let tag = theCurrentModel.arr_EditTagList[i].tag_id
            for j in 0..<theCurrentModel.arr_actionTagList.count{
                if tag == theCurrentModel.arr_actionTagList[j].tag_id{
                    theCurrentModel.arr_actionTagList[j].is_selected = "1"
                    theCurrentModel.arr_actionTagList[j].is_old_selected = "1"
                }
            }
        }
        print(" acction tag list\(theCurrentModel.arr_actionTagList)")
        
        theCurrentView.viewTag.reloadData()
        theCurrentView.updateTagView()

    }
    
    
    func deleteAttachment(index:Int) {
        if theCurrentModel.arr_selectedFile[index].isOldAttachment {
            theCurrentModel.arr_selectedFile[index].isDeleteAttachment = true
        } else {
            theCurrentModel.arr_selectedFile.remove(at: index)
        }
        theCurrentView.collectionSelectedFile.reloadData()
//        UIView.performWithoutAnimation {
//            (theController.view as? IdeaNProblemDetailView)?.tableView.reloadSections(IndexSet.init(integer: 0), with: .none)
//        }
        
    }
    
    func updateDropDownData(categorySelection:categorySelectionType, str:String, cellType:ActionEditModel.celltype, index:Int)  {
        var index = 0
        if let ind = theCurrentModel.cellType.firstIndex(where: {$0 == cellType}) {
            index = ind
        }
        print("selected index \(index)")
        switch categorySelection {
        case .company:
            if let selectedIndex = theCurrentModel.arr_companyList.firstIndex(where: {$0.name == str}) {
                theCurrentModel.strSelectedCompanyID = theCurrentModel.arr_companyList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["company"].stringValue = str
            theCurrentView.lblCompany.text = str
            theCurrentModel.strSelectedDepartmentID = ""
            self.theCurrentModel.selectedData["department"].stringValue = ""
            theCurrentModel.strSelectedUserAssignID = ""
            self.theCurrentModel.selectedData["assignto"].stringValue = ""
            theCurrentModel.strSelectedApprovedID = ""
            self.theCurrentModel.selectedData["approvedby"].stringValue = ""
            theCurrentView.lblAssignTo.text = "Select assigned to"
            theCurrentView.lblDepartment.text = "Select department"
            theCurrentView.lblApprovedBy.text = "Select approved by"
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
            theCurrentView.statusActivityIndicator(isLoading: true,activityIndicator: theCurrentView.activityApprovedIndicator, imgView: theCurrentView.imgApproved)
            break
        case .department:
            if let selectedIndex = theCurrentModel.arr_departmentList.firstIndex(where: {$0.name == str}) {
                theCurrentModel.strSelectedDepartmentID = theCurrentModel.arr_departmentList[selectedIndex].id
            }
            self.theCurrentModel.selectedData["department"].stringValue = str
            theCurrentView.lblDepartment.text = str
            break
        case .assignTo:
            if let selectedIndex = theCurrentModel.arr_assignList.firstIndex(where: {$0.assignee_name == str}) {
                theCurrentModel.strSelectedUserAssignID = theCurrentModel.arr_assignList[selectedIndex].user_id
            }
            self.theCurrentModel.selectedData["assignto"].stringValue = str
            theCurrentView.lblAssignTo.text = str
            theCurrentModel.strSelectedDepartmentID = ""
            self.theCurrentModel.selectedData["department"].stringValue = ""
            theCurrentView.lblDepartment.text = "Select department"
            departmentListService(activityIndicator: theCurrentView.activityDepartmentIndicator, imgView: theCurrentView.imgDepartment)
            break
        case .relatedTo:

            self.theCurrentModel.selectedData["relatedto"].stringValue = str
//            theCurrentView.lblRelatedTo.text = str
            return
        case .approvedBy:
            if let selectedIndex = theCurrentModel.arr_assignList.firstIndex(where: {$0.assignee_name == str}) {
                theCurrentModel.strSelectedApprovedID = theCurrentModel.arr_assignList[selectedIndex].user_id
            }
            self.theCurrentModel.selectedData["approvedby"].stringValue = str
            theCurrentView.lblApprovedBy.text = str
        case .relatedToStrategyPrimaryArea:
           
            break
        case .relatedToStrategySecondaryArea:
            
            break
        case .relatedToStrategyGoal:
           
            break
        case .relatedToRisk:
//            let arr_list:[String] = theCurrentModel.arr_actionRiskList.map({$0.unwanted_event })
           
            break
        case .relatedToFocus:
           
            break
        case .relatedToTacticalProject:
    
            break
        case .selectTag:
            break
        }
        
//        self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func updateDueDate(strDate:String,dateSelectionType:dateSelectionType,date:Date) {
        switch dateSelectionType {
        case .created:
            theCurrentModel.selectedData["createdate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 9, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblCreatedDate.text = strDate
            break
        case .due:
            theCurrentModel.selectedDueDate = date
            theCurrentModel.selectedData["duedate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 10, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblDueDate.text = strDate
            break
        case .work:
            theCurrentModel.selectedWorkDate = date
            theCurrentModel.selectedData["workdate"].stringValue = strDate
//            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 11, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.lblWorkDate.text = strDate
            theCurrentView.lblEditWorkDate.text = strDate
            break
        case .revisedDate:
            theCurrentModel.selectedData["reviseddate"].stringValue = strDate
            theCurrentModel.selectedRevisedDueDate = date
            theCurrentView.lblRevisedDate.text = strDate
            theCurrentView.lblEditRevisedDueDate.text = strDate

        }
    }
    func updateTagList(strTag:String, isAdded:Bool = true, at index:Int = -1) {
        if isAdded {
            theCurrentModel.arr_AddTagList.append(strTag)
        } else {
            let strRemoveTagName = theCurrentModel.arr_AddTagList[index]
            
            if let selectedIndex = theCurrentModel.arr_actionTagList.firstIndex(where: {$0.tag_name == strRemoveTagName}) {
                
               /* if theCurrentModel.arr_actionTagList[selectedIndex].is_added == "1"{
                    theCurrentModel.arr_actionTagList[selectedIndex].is_del = "1"
                }
                theCurrentModel.arr_actionTagList[selectedIndex].is_selected = "0"*/
                
                let data = theCurrentModel.arr_actionTagList[selectedIndex]
                if data.is_selected == "0"
                {
                    if(data.is_old_selected == "1")
                    {
                        data.is_del = "0"
                    }
                    else{
                        data.is_new_added = "1"
                    }
                    
                    data.is_selected = "1"
                }
                else{
                    
                    if(data.is_old_selected == "1")
                    {
                        data.is_del = "1"
                    }
                    else{
                        data.is_new_added = "0"
                    }
                    data.is_selected = "0"
                }
                
            }
            theCurrentModel.arr_AddTagList.remove(at: index)
        }
        print("TagList \(theCurrentModel.arr_actionTagList)")
        theCurrentView.viewTag.reloadData()
        theCurrentView.updateTagView()
    }
    
    func updateSubTaskOperation(theModel:SubTaskList, index:Int,isEdit: Bool, isUpdateStatus: (Bool,Bool), isRemove: Bool) {
        if isRemove {
            theCurrentModel.arr_subTaskList.remove(at: index)
        } else if isUpdateStatus.0 { // completed
            theModel.isSelected = true
            theCurrentModel.arr_subTaskList[index] = theModel
        } else if isUpdateStatus.1 { // inProgress
            theModel.isSelected = false
            theCurrentModel.arr_subTaskList[index] = theModel
        } else if isEdit {
            theCurrentModel.arr_subTaskList[index] = theModel
        } else {
            // For Add
            theCurrentModel.arr_subTaskList.insert(theModel, at: 0)
        }
        theCurrentView.tableView.reloadData()
    }
    
    func updateSubTaskList(strSubTask:String, index:Int) {
        if strSubTask.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            showAlertAtBottom(message: "Please enter subtask title")
            return
        }
        let subTask = SubTaskList()
        subTask.subTaskName = strSubTask
        subTask.is_new_task = "1"
        subTask.is_del = "0"
        subTask.is_old_task = "0"
        subTask.subTaskId = "0"
        subTask.isSelected = false
        addAndUpdateSubTaskWebService(theModel: subTask, index: 0, isEdit: false, isUpdateStatus: (false,false), isRemove: false)
        
//        theCurrentModel.theActionDetailModel?.subTaskList.insert(subTask, at: 0)
//        theCurrentModel.arr_subTaskList.insert(subTask, at: 0)
        
//        theCurrentModel.arr_SubTaskList.insert(strSubTask, at: 0)
        /*
        theCurrentModel.theActionDetailModel?.subTaskList.append(subTask)
        theCurrentModel.arr_subTaskList.append(subTask)
        theCurrentModel.arr_SubTaskList.append(strSubTask)*/
//        self.theCurrentView.tableView.reloadData()
        
//        theCurrentModel.arr_SubTaskList.append(strSubTask)
        
//        self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func handleAddOrEditSubTaskButton(index:Int, isEdit:Bool) {
        if isEdit {
            theCurrentView.btnAddsubTask.isSelected = true
            theCurrentView.btnAddsubTask.setTitle("", for: .selected)
            theCurrentView.btnAddsubTask.tag = index
            theCurrentView.btnClearSubTask.isHidden = false
            theCurrentView.constrainClearSubTaskWidth.constant = 26.0
            theCurrentView.txtSubTaskTitle.text = theCurrentModel.arr_subTaskList[index].subTaskName
        } else {
            theCurrentView.btnAddsubTask.tag = 0
            theCurrentView.btnAddsubTask.isSelected = false
            theCurrentView.btnAddsubTask.setTitle("+", for: .normal)
            theCurrentView.btnClearSubTask.isHidden = true
            theCurrentView.constrainClearSubTaskWidth.constant = 0.0
        }
    }
    func uppdateEditSubTaskText(strSubTask:String, index:Int) {
        let subtaskTitle = theCurrentModel.arr_subTaskList[index].subTaskName
        
        theCurrentModel.arr_subTaskList[index].subTaskName = strSubTask.isEmpty ? subtaskTitle : strSubTask
        if strSubTask.isEmpty {
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            }
        } else {
            theCurrentView.tableView.beginUpdates()
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
            theCurrentView.tableView.endUpdates()
        }
        
        if let index = theCurrentModel.theActionDetailModel?.subTaskList.firstIndex(where: {$0.subTaskName == subtaskTitle}) {
            theCurrentModel.theActionDetailModel?.subTaskList[index].subTaskName = strSubTask.isEmpty ? subtaskTitle : strSubTask
        }
    }
    func updateEditSubTaskList(strSubTask:String, index:Int) {
        if strSubTask.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            showAlertAtBottom(message: "Please enter subtask title")
            return
        }
        let subtaskTitle = theCurrentModel.arr_subTaskList[index].subTaskName
        theCurrentModel.arr_subTaskList[index].subTaskName = strSubTask
        if let index = theCurrentModel.theActionDetailModel?.subTaskList.firstIndex(where: {$0.subTaskName == subtaskTitle}) {
            theCurrentModel.theActionDetailModel?.subTaskList[index].subTaskName = strSubTask
        }

        UIView.performWithoutAnimation {
            theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
        }
        
        handleAddOrEditSubTaskButton(index: index, isEdit: false)
//        self.theCurrentView.tableView.reloadData()
        
        //        theCurrentModel.arr_SubTaskList.append(strSubTask)
        
        //        self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    func handleCheckSubTask(index:Int) {
        if theCurrentModel.arr_subTaskList.indices.contains(index) {
            theCurrentModel.arr_subTaskList[index].isSelected = !theCurrentModel.arr_subTaskList[index].isSelected
            UIView.performWithoutAnimation {
                theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: UITableView.RowAnimation.none)
            }
        }
    }
    
    func validateEditForm() {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter project name")
            return
        }  /*else if theCurrentView.txtDescription.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter description")
            return
        }*/ else if theCurrentModel.selectedData["company"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select company")
            return
        } else if theCurrentModel.selectedData["assignto"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select assigned to")
            return
        } else if theCurrentModel.selectedData["department"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select department")
            return
        } else if theCurrentModel.selectedData["approvedby"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select approvedby")
            return
        } else if theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select due date")
            return
        } else if !theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
            if let revisedDate1 = theCurrentModel.selectedRevisedDueDate, let workDate1 = theCurrentModel.selectedWorkDate, workDate1 > revisedDate1 {
                self.showAlertAtBottom(message: "Work date must be less than revised date.")
                return
            } else if let dueDate1 = theCurrentModel.selectedDueDate, let workDate1 = theCurrentModel.selectedWorkDate,theCurrentModel.selectedRevisedDueDate == nil,workDate1 > dueDate1 {
                self.showAlertAtBottom(message: "Work date must be less than due date.")
                return
            }
        }
        
        if !theCurrentModel.selectedData["reviseddate"].stringValue.isEmpty {
            if let dueDate1 = theCurrentModel.selectedDueDate, let revisedDate1 = theCurrentModel.selectedRevisedDueDate,revisedDate1 < dueDate1 {
                self.showAlertAtBottom(message: "Revised date should be grater then due date")
                return
            }
        }
        
        var workDate = ""
        var dueDate = ""
        var revisedDate = ""
        
        var durationCount = ""

        
        
        if let dueDate1 = theCurrentModel.selectedDueDate {
            dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let workDate1 = theCurrentModel.selectedWorkDate {
            workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let reviseDueDate1 = theCurrentModel.selectedRevisedDueDate {
            revisedDate = reviseDueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        if let duration = theCurrentModel.selectedDuration {
            let minutes = (duration.hour * 60) + duration.minute
            durationCount = "\(minutes)"
        }
        
        var arraySelectedFilterIds : [JSON] = []
        var strTagName = ""
        
        theCurrentModel.arr_actionTagList.forEach { (subJson) in
            
            var newjson = JSON()
            if(subJson.is_new_added != "0" || subJson.is_del != "0" || subJson.is_old_selected != "0")
            {
                if(subJson.is_new_added == "1") {
                    newjson["tags"] = JSON(subJson.tag_id)
                    newjson["is_del"] = JSON(subJson.is_del)
                    arraySelectedFilterIds.append(newjson)
                }
                else if(subJson.is_old_selected == "1" && subJson.is_del != "0") {
                    
                    newjson["tags"] = JSON(subJson.tag_id)
                    newjson["is_del"] = JSON(subJson.is_del)
                    arraySelectedFilterIds.append(newjson)
                }
            }
        }
        
        print("arraySelectedFilterIds - ",arraySelectedFilterIds)
        strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""

        
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:theCurrentModel.theActionDetailModel?.actionLogid,
                         APIKey.key_title:theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_work_date:workDate,
                         APIKey.key_duedate:dueDate,
                         APIKey.key_revised_date:revisedDate,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID,
                         APIKey.key_assigned_to:theCurrentModel.strSelectedUserAssignID,
                         APIKey.key_department:theCurrentModel.strSelectedDepartmentID,
                         APIKey.key_approved_by:theCurrentModel.strSelectedApprovedID,
                         APIKey.key_description:theCurrentView.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_duration:durationCount,
                         APIKey.key_completed_percentage:theCurrentView.txtComopleted.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         APIKey.key_tags:strTagName
            ] as! [String : String]
        
      //  if theCurrentModel.theActionDetailModel?.note_file != ""
        
//        parameter[APIKey.key_tags] = strTagName
        
//        if theCurrentModel.strRemoveAudioId != "0" || theCurrentModel.strRemoveAudioId != ""{
//             parameter[APIKey.key_remove_voice_notes] = theCurrentModel.strRemoveAudioId
//        }
        
        
        /*var arr_Subtask:[JSON] = []
        let subtaskList = Array(theCurrentModel.theActionDetailModel!.subTaskList)
        var order = subtaskList.count
        
        for ObjSubTaskList in subtaskList {
            var dict = JSON()
            
            if(ObjSubTaskList.is_new_task == "1")
            {
                dict["task_name"].stringValue = ObjSubTaskList.subTaskName
                dict["is_del"].stringValue = ObjSubTaskList.is_del
                dict["task_id"].stringValue = ""
                dict[APIKey.key_order].stringValue = "\(order)"
                dict["is_status"].stringValue = ObjSubTaskList.isSelected ? "1" : "0"
                arr_Subtask.append(dict)
            }
            else
            {
                if(ObjSubTaskList.is_del == "1")
                {
                    dict["task_id"].stringValue = ObjSubTaskList.subTaskId
                    dict["is_del"].stringValue = ObjSubTaskList.is_del
                    dict[APIKey.key_order].stringValue = "\(order)"
                    arr_Subtask.append(dict)
                } else if (ObjSubTaskList.is_old_task == "") {
                    dict["task_name"].stringValue = ObjSubTaskList.subTaskName
                    dict[APIKey.key_order].stringValue = "\(order)"
                    dict["is_del"].stringValue = "0"
                    dict["task_id"].stringValue = ObjSubTaskList.subTaskId
                    dict["is_status"].stringValue = ObjSubTaskList.isSelected ? "1" : "0"
                    arr_Subtask.append(dict)
                }
            }
            order -= 1
        }
        
        print("Task JSON - ",arr_Subtask)
        
        let strTaskname = JSON(arr_Subtask).rawString()
        parameter[APIKey.key_sub_task] = strTaskname*/
        
//        if theCurrentModel.arr_strRemoveAttachment.count > 0{
//            parameter[APIKey.key_remove_attechment] = theCurrentModel.arr_strRemoveAttachment.joined(separator: ",")
//        }
        addActionWithAttachment(parameter: parameter)
    }
    
    func validateForm() {
        self.view.endEditing(true)
        if theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter project name")
            return
        } /* else if theCurrentView.txtDescription.text!.isEmpty {
            self.showAlertAtBottom(message: "Please enter description")
            return
        } */else if theCurrentModel.selectedData["relatedto"].stringValue.isEmpty {
            self.showAlertAtBottom(message: "Please select related to")
            return
        } else {
            
            /*if theCurrentModel.arr_AddTagList.count < 0{
                self.showAlertAtBottom(message: "Please select atlease one tag")
                return
            } else*/ if theCurrentModel.selectedData["company"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select company")
                return
            } else if theCurrentModel.selectedData["assignto"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select assign to")
                return
            } /*else if theCurrentModel.selectedData["department"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select department")
                return
            }*/ else if theCurrentModel.selectedData["approvedby"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select approvedby")
                return
            } else if theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
                self.showAlertAtBottom(message: "Please select due date")
                return
            } else if !theCurrentModel.selectedData["duedate"].stringValue.isEmpty {
                if let revisedDate1 = theCurrentModel.selectedRevisedDueDate, let workDate1 = theCurrentModel.selectedWorkDate, workDate1 > revisedDate1 {
                    self.showAlertAtBottom(message: "Work date must be less than revised date.")
                    return
                } else if let dueDate1 = theCurrentModel.selectedDueDate, let workDate1 = theCurrentModel.selectedWorkDate,theCurrentModel.selectedRevisedDueDate == nil,workDate1 > dueDate1 {
                    self.showAlertAtBottom(message: "Work date must be less than due date.")
                    return
                }
            }
            
            if !theCurrentModel.selectedData["reviseddate"].stringValue.isEmpty {
                if let dueDate1 = theCurrentModel.selectedDueDate, let revisedDate1 = theCurrentModel.selectedRevisedDueDate,revisedDate1 < dueDate1 {
                    self.showAlertAtBottom(message: "Revised date should be grater then due date")
                    return
                }
            }
            
            var workDate = ""
            var dueDate = ""
            var durationCount = ""
            
            if let dueDate1 = theCurrentModel.selectedDueDate {
                dueDate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
            }
            
            if let workDate1 = theCurrentModel.selectedWorkDate {
                workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
            }
            if let duration = theCurrentModel.selectedDuration {
                let minutes = (duration.hour * 60) + duration.minute
                durationCount = "\(minutes)"
            }
            isClickOnAdd(isLoading: true)
            var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                             APIKey.key_access_token:Utility.shared.userData.accessToken,
                             APIKey.key_title:theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_work_date:workDate,
                             APIKey.key_duedate:dueDate,
                             APIKey.key_company_id:theCurrentModel.strSelectedCompanyID,
                             APIKey.key_assigned_to:theCurrentModel.strSelectedUserAssignID,
                             APIKey.key_department:theCurrentModel.strSelectedDepartmentID,
                             APIKey.key_approve_by:theCurrentModel.strSelectedApprovedID,
                        APIKey.key_description:theCurrentView.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                             APIKey.key_duration:durationCount] as [String : String]
            
            
            
            let arr_Subtask:[JSON] = []
            /*for i in 0..<theCurrentModel.arr_SubTaskList.count{
                var dict = JSON()
                dict["task_name"].stringValue = theCurrentModel.arr_SubTaskList[i]
                arr_Subtask.append(dict)
            }*/
            let strTaskname = JSON(arr_Subtask).rawString()
            parameter[APIKey.key_sub_task] = strTaskname
            
            var arraySelectedFilterIds : [JSON] = []
            var strTagName = ""
           
           let _ = theCurrentModel.arr_actionTagList.filter { (subJson) -> Bool in
                    var newjson = JSON()
                    if(theCurrentModel.arr_AddTagList.contains(subJson.tag_name))
                    {
                        newjson["tags"] = JSON(subJson.tag_id)
                        arraySelectedFilterIds.append(newjson)
                        return true
                    }
                    return false
                }
               // strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""
           
            print("arraySelectedFilterIds - ",arraySelectedFilterIds)
            strTagName = JSON(arraySelectedFilterIds).rawString() ?? ""
            parameter[APIKey.key_tags] = strTagName
            addActionWithAttachment(parameter: parameter)
        }
    }
    
    func isDisableCompanySelection() {
        /*if UserRole.companyAdmin == UserDefault.shared.userRole || UserRole.staffAdmin == UserDefault.shared.userRole || Utility.shared.userData.companyId != "0" {
            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
        }*/
        
        if UserRole.superAdminUser != UserDefault.shared.userRole || (Utility.shared.userData.companyId != "0" && !Utility.shared.userData.companyId.isEmpty) {
            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
        }
    }
    
    func setCompanyData() { //  for New Add
        if UserRole.superAdminUser == UserDefault.shared.userRole, Utility.shared.userData.companyId == "0" || Utility.shared.userData.companyId.isEmpty {
            companylistService(activityIndicator: theCurrentView.activityCompanyIndicator, imgView: theCurrentView.imgCompany)
            
        } else {
            theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            theCurrentModel.selectedData["company"].stringValue = Utility.shared.userData.companyName
            theCurrentView.lblCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompany.isHidden = true
            assignUserListService(activityIndicator:theCurrentView.activityAssignToIndicator, imgView: theCurrentView.imgAssignTo)
        }
    }
    
    func updateActionDetailModel() {
        
        if let dueDate1 = theCurrentModel.selectedDueDate {
            theCurrentModel.theActionDetailModel?.duedate = dueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let workDate1 = theCurrentModel.selectedWorkDate {
            theCurrentModel.theActionDetailModel?.workDate = workDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        if let reviseDueDate1 = theCurrentModel.selectedRevisedDueDate {
            theCurrentModel.theActionDetailModel?.revisedDate = reviseDueDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        theCurrentModel.theActionDetailModel?.actionTitle = theCurrentView.txtTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        theCurrentModel.theActionDetailModel?.percentageCompeleted = theCurrentView.txtComopleted.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//            = theCurrentModel.selectedData["description"].stringValue
    }
    
    func isClickOnAdd(isLoading:Bool = false){
        if isLoading {
            theCurrentView.btnAdd.startLoadyIndicator()
            self.view.allButtons(isEnable: false)
        } else {
            theCurrentView.btnAdd.stopLoadyIndicator()
            self.view.allButtons(isEnable: true)
        }
    }
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        theCurrentView.activitySpinner.hidesWhenStopped = true
        theCurrentView.constrainViewPagerHeight.constant = 40.0
        theCurrentView.viewOfPager.isHidden = false

        if isAnimating {
            theCurrentView.activitySpinner.startAnimating()
            theCurrentView.viewMore.isHidden = true
        } else {
            theCurrentView.activitySpinner.stopAnimating()
            theCurrentView.viewMore.isHidden = false

            if theCurrentModel.nextOffset == -1 {
                theCurrentView.viewOfPager.isHidden = true
                theCurrentView.constrainViewPagerHeight.constant = 0.0
            }
        }
    }
    /*func attributedString(string:String,color: UIColor,font:UIFont) -> NSAttributedString? {
        let attributes1 = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor: color
            ] as [NSAttributedString.Key : Any]
        
        let attributedString = NSAttributedString(string: "\(string)", attributes: attributes1)
        
        return attributedString
    }*/
    
    func updatePercentageCompleted(percentage:Int,strActionID:String) {
        theCurrentView.txtComopleted.text = "\(percentage)"
    }
    
    //MARK:- Redirection
    func presentPrecentageCompletedVC(percentage:Int,strActionID:String)  {
        let vc = PercentcompletedVC.init(nibName: "PercentcompletedVC", bundle: nil)
        vc.setTheData(thePercent: percentage, strActionID: strActionID,isForEdit: true)
        vc.handlerSelectedPercent = { [weak self] (percent) in
            self?.updatePercentageCompleted(percentage:percent, strActionID: strActionID)
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToRevisedDueDateRequestVC(isForSentRequest:Bool, reasonForDecline:String) {
        var dueDate = theCurrentModel.selectedDueDate
        if theCurrentModel.selectedRevisedDueDate != nil {
            dueDate = theCurrentModel.selectedRevisedDueDate
        }
        if !isForSentRequest {
            if theCurrentModel.theActionDetailModel?.requestedRevisedDate != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DateFormatterInputType.inputType1
                if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.requestedRevisedDate)!){
                    dueDate = finalDate
                    print("revised date \(finalDate)")
                }
                
            }
        }
        if dueDate == nil {
            self.showAlertAtBottom(message: "Due date is not available")
            return
        }
        let vc = RevisedDueDateRequestVC.init(nibName: "RevisedDueDateRequestVC", bundle: nil)
        vc.setRevisedDueDate(strActionID: theCurrentModel.actionId, revisedDueDate: dueDate, reasonForDecline: reasonForDecline, isForSentRequest: isForSentRequest)
        vc.handlerRevisedDueDate = { [weak self] (revisedDueDate, dateRevisedDue, theModel)  in
            if isForSentRequest {
                self?.theCurrentView.viewRevisedDueDateRequest(isForSent: true, strRevisedDueDate: revisedDueDate)
                self?.handlerReqestDueDateChangeAction(theModel)
            } else {
                let dateFormatter = DateFormatter()
                if let finalDate = dateRevisedDue {
                    dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                    let strFinalDate = dateFormatter.string(from: finalDate)
                    self?.theCurrentView.lblRevisedDate.text = strFinalDate
                    self?.theCurrentView.lblEditRevisedDueDate.text = strFinalDate
                    self?.theCurrentModel.selectedData["reviseddate"].stringValue = strFinalDate
                    self?.theCurrentModel.selectedRevisedDueDate = finalDate
                    print("revised date \(finalDate)")
                    dateFormatter.dateFormat = DateFormatterInputType.inputType1
                    self?.theCurrentModel.theActionDetailModel?.revisedDate = dateFormatter.string(from: finalDate)
                    self?.handlerReqestDueDateChangeAction(theModel)
                }

                /*if theCurrentModel.theActionDetailModel?.revisedDate != "" {
                   
                    
                }else{
                    self?.theCurrentView.lblRevisedDate.text = "Select revised date"
                    self?.theCurrentView.lblEditRevisedDueDate.text = "Select revised date"
                }*/
                self?.theCurrentView.viewClearRevisedDueDateRequest()
            }
        }
        vc.handlerConfirmDecline = { [weak self] (theModel) in
            self?.handlerReqestDueDateChangeAction(theModel)
            self?.theCurrentView.viewClearRevisedDueDateRequest()
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToDatePicker() {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetDate = true
        vc.setDateValue = theCurrentModel.selectedDuration
        vc.minuteInterval = 5
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.countDownTimer)
        vc.isForDuration = true
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1 {
//                self?.dateFormater.dateFormat = "hh mm a"
                self?.theCurrentModel.selectedDuration = date
                let arr_time = date.string(withFormat: DateFormatterOutputType.outputType12)
                print("arr_time:=",arr_time)
                self?.theCurrentView.txtDuration.text = date.string(withFormat: DateFormatterOutputType.outputType12)

//                self?.update(hours: arr_time[0], minute: arr_time[1], timeType: arr_time[2])
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    func presentDropDownListVC(categorySelection:categorySelectionType,cellType:ActionEditModel.celltype) {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        switch categorySelection {
        case .company:
            let arr_list:[String] = theCurrentModel.arr_companyList.map({$0.name })
            vc.setTheData(strTitle: "Company",arr:arr_list)
            break
        case .department:
            let arr_list:[String] = theCurrentModel.arr_departmentList.map({$0.name })
            vc.setTheData(strTitle: "Depatment",arr:arr_list)
            break
        case .assignTo:
            let arr_list:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name })
            vc.setTheData(strTitle: "Assigned to",arr:arr_list)
            break
        case .relatedTo:
            break
        case .approvedBy:
            let arr_list:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name })
            vc.setTheData(strTitle: "Approved By",arr:arr_list)
            break
        case .relatedToStrategyPrimaryArea:
            break
        case .relatedToStrategySecondaryArea:
            break
        case .relatedToStrategyGoal:
            break
        case .relatedToRisk:
            break
        case .relatedToFocus:
            break
        case .relatedToTacticalProject:
            break
        case .selectTag:
            vc.isSelectTag = true
            vc.setTheTagData(strTitle: "Add Tags", arr: theCurrentModel.arr_actionTagList)
            break
        }
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.updateDropDownData(categorySelection:categorySelection, str: strData, cellType: cellType,index: index)
        }
        
        vc.handlerTag = {[weak self](data) in
            self?.theCurrentModel.arr_actionTagList = data
            self?.theCurrentModel.arr_AddTagList = []
            var isCheckTag = Bool()
            for i in 0..<(self?.theCurrentModel.arr_actionTagList.count ?? 0){
                
                if let data = self?.theCurrentModel.arr_actionTagList[i], data.is_selected == "1" {
                    isCheckTag = true
                    self?.updateTagList(strTag: data.tag_name, isAdded: true, at: i)
                }
            }
            if isCheckTag == false{
                self?.theCurrentView.viewTag.reloadData()
                self?.theCurrentView.updateTagView()
            }
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    func redirectToCustomDatePicker(selectionType:dateSelectionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .created:
            if let createdDate = theCurrentModel.selectedData["createdate"].string, !createdDate.isEmpty, let date = createdDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
            
        case .due:
            if let dueDate = theCurrentModel.selectedData["duedate"].string, !dueDate.isEmpty, let date = dueDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .work:
            if let workDate = theCurrentModel.selectedData["workdate"].string, !workDate.isEmpty, let date = workDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        case .revisedDate:
            if let reviseDate = theCurrentModel.selectedData["reviseddate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outputType7) {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            //self?.updateDueDate(strDate: dateFormaater.string(from: date), type: selectionType, date: date)
            self?.updateDueDate(strDate: dateFormaater.string(from: date), dateSelectionType: selectionType, date: date)
            
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func presentFileManger() {

         let documentPicker = UIDocumentPickerViewController(documentTypes: fileType(), in: .import)
        //        "public.item",public.data
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = true
        }
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .overFullScreen
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func presentVoiceNote() {
        let audioRecorderVC = IQAudioRecorderViewController()
        audioRecorderVC.audioFormat = .default
        audioRecorderVC.allowCropping = false
        audioRecorderVC.delegate = self
        audioRecorderVC.maximumRecordDuration = 10.0
        //        audioRecorderVC.modalTransitionStyle = .crossDissolve
        audioRecorderVC.modalPresentationStyle = .overCurrentContext
        self.presentAudioRecorderViewControllerAnimated(audioRecorderVC)
        
    }
    func presentDropDownListVC() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        let arr_list:[String] = theCurrentModel.arr_noteHistoryStatus.map({$0["name"].stringValue })
        vc.setTheData(strTitle: "Notes History", arr: arr_list)
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentModel.arr_notesHistoryList?.removeAll()
            self?.theCurrentView.tblNotesHistory.reloadData()
            self?.theCurrentView.txtNotesHistory.text = strData
            self?.theCurrentModel.strNoteStatus = self?.theCurrentModel.arr_noteHistoryStatus[index]["status"].stringValue ?? ""
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentModel.strNoteStatus ?? "")
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    
    
    
//    func presentAudioPlayer(url:String) {
//        let videoPlayer = AVPlayer(url: URL.init(string: url)!)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = videoPlayer
//        playerViewController.delegate = self
//
//        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerViewController.player?.currentItem)
////        
//        self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
//    }
//    @objc func playerDidFinishPlaying(note: NSNotification) {
////        playerController.dismiss(animated: true, completion: nil)
//    }
    
    //MARK:- IBAction
    @IBAction func onBtnPercentCompletedAction(_ sender: Any) {
        if !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.theActionDetailModel?.actionlogStatus ?? "") {
            self.presentPrecentageCompletedVC(percentage: Int((theCurrentModel.theActionDetailModel?.percentageCompeleted ?? "0")) ?? 0, strActionID: theCurrentModel.theActionDetailModel?.actionLogid ?? "")
        }
    }
    @IBAction func onBtnRevisedDueDateClickToViewAction(_ sender: Any) {
        self.view.endEditing(true)
        if !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.theActionDetailModel?.actionlogStatus ?? "") {
            redirectToRevisedDueDateRequestVC(isForSentRequest: false, reasonForDecline: theCurrentModel.theActionDetailModel?.reasonForReviseDueDate ?? "")
        }
    }
    
    @IBAction func onBtnSelectNotesHistoy(_ sender:UIButton){
        self.view.endEditing(true)

        presentDropDownListVC()
    }
    @IBAction func onBtnAddTagAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .selectTag, cellType: .tag)
        //presentAddTagVC()
    }
    
    @IBAction func onBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .company, cellType: .company)
    }
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .assignTo, cellType: .assignTo)
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .department, cellType: .department)
    }
    @IBAction func onBtnApprovedByAction(_ sender: Any) {
        self.view.endEditing(true)
        presentDropDownListVC(categorySelection: .approvedBy, cellType: .approvedBy)
    }
    @IBAction func onBtnCreatedDateAction(_ sender: Any) {
        self.view.endEditing(true)
    
    }
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        if !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.theActionDetailModel?.actionlogStatus ?? "") {
            redirectToCustomDatePicker(selectionType: .due)
        }
        
    }
    @IBAction func onBtnWorkDateAction(_ sender: Any) {
        self.view.endEditing(true)
        if !UserDefault.shared.isArchiveAction(strStatus: theCurrentModel.theActionDetailModel?.actionlogStatus ?? "") {
            redirectToCustomDatePicker(selectionType: .work)
        }
    }
    
    @IBAction func onBtnRevisedDateAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentModel.theActionDetailModel?.assigneeId == Utility.shared.userData.id {
            redirectToRevisedDueDateRequestVC(isForSentRequest: true, reasonForDecline: "")
        } else {
            redirectToCustomDatePicker(selectionType: .revisedDate)
        }
    }
    
    @IBAction func onBtnDurationAction(_ sender: Any) {
        self.view.endEditing(true)
        self.redirectToDatePicker()
    }
    
    
    @IBAction func onBtnSubTaskAction(_ sender: UIButton) {
        if sender.isSelected {
            let text = theCurrentView.txtSubTaskTitle.text ?? ""
            theCurrentView.txtSubTaskTitle.text = nil
            updateEditSubTaskList(strSubTask: text, index: sender.tag)
        } else {
            let text = theCurrentView.txtSubTaskTitle.text ?? ""
            theCurrentView.txtSubTaskTitle.text = nil
            updateSubTaskList(strSubTask: text, index: 0)
        }
    }
    @IBAction func onBtnClearSubTaskAction(_ sender: UIButton) {
        theCurrentView.txtSubTaskTitle.text = nil
        self.view.endEditing(true)
        handleAddOrEditSubTaskButton(index: sender.tag, isEdit: false)
    }
    
    @IBAction func onBtnAttachmentFileAction(_ sender: Any) {
        self.view.endEditing(true)
        presentFileManger()
    }
    
    @IBAction func onBtnPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let strNoteMsg = theCurrentView.txtNote.text ?? ""
        if strNoteMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter note")
            return
        } else {
            sender.startLoadyIndicator()
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentModel.actionId,
                                 APIKey.key_notes: strNoteMsg,
                                 APIKey.key_note_for: APIKey.key_action_logs
            ]
            
            addNotes(parameter: parameter, completion: {
                sender.stopLoadyIndicator()
            })
        }
    }
    
    @IBAction func onAddVoiceAction(_ sender: Any) {
        self.view.endEditing(true)
        presentVoiceNote()
    }
    
    
    @IBAction func onAddAction(_ sender: Any) {
        if theCurrentModel.isEditScreen == .edit{
            validateEditForm()
        }else{
            validateForm()
        }
    }
    
    @IBAction func onBtnCancelAction(_ sender: UIButton) {
        sender.startLoadyIndicator()
        closeHistorySaveWebService { [weak self] in
            sender.stopLoadyIndicator()
            self?.backAction(isAnimation: false)
        }
//        self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnRemoveAttachmentAction(_ sender: UIButton) {
        /*if theCurrentModel.isEditScreen == .edit{
            let fileName = self.theCurrentModel.arr_selectedFile[sender.tag].selectedFileName
            if let selectedIndex = theCurrentModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
                theCurrentModel.arr_strRemoveAttachment.append(theCurrentModel.arr_fileList[selectedIndex].fileId)
            }
        }
        self.theCurrentModel.arr_selectedFile.remove(at: sender.tag)*/
        
        
        let attachment = theCurrentModel.arr_selectedFile[sender.tag]
        theCurrentModel.arr_RemoveSelectedFile.append(attachment)
        theCurrentModel.arr_selectedFile.remove(at: sender.tag)
        self.theCurrentView.collectionSelectedFile.reloadData()
    }
    /*
    @IBAction func onBtnRemoveFileAction(_ sender: UIButton) {
        if theCurrentModel.isEditScreen == .edit{
            let fileName = self.theCurrentModel.arr_selectedFile[sender.tag].selectedFileName
            if let selectedIndex = theCurrentModel.arr_fileList.firstIndex(where: {$0.fileName == fileName}) {
                theCurrentModel.arr_strRemoveAttachment.append(theCurrentModel.arr_fileList[selectedIndex].fileId)
            }
        }
        self.theCurrentModel.arr_selectedFile.remove(at: sender.tag)
        self.theCurrentView.collectionSelectedFile.reloadData()
    }
     
    @IBAction func btnRemoveAudioAction(_ sender: UIButton) {
        if theCurrentModel.isEditScreen == .edit{
            if theCurrentModel.strRemoveAudioId == "0" {
                theCurrentModel.strRemoveAudioId = theCurrentModel.theActionDetailModel?.note_file_id ?? ""
            }
        }
        
        theCurrentModel.arr_selectedAudioFile.removeAll()
        theCurrentView.collectionSelectedFile.reloadData()
        //        theCurrentView.viewOfAudio.isHidden = true
    }*/
    
    @IBAction func btnTaskCompletedAction(_ sender: UIButton) {
        self.theCurrentView.txtComopleted.text = "100"
    }
    
    
    @IBAction func onBtnViewMoreAction(_ sender: Any) {
        loadViewMoreNoteHistory(isAnimating: true)
        getActionNotesHistoryWebService(status: theCurrentModel.strNoteStatus)
    }
    
    deinit {
        if !isObserverDeallocate {
            theCurrentView.tableView?.removeObserver(self, forKeyPath: "contentSize")
            theCurrentView.collectionSelectedFile?.removeObserver(self, forKeyPath: "contentSize")
            theCurrentView.tblNotesHistory?.removeObserver(self, forKeyPath: "contentSize")
        }
    }
}

//MARK:- UITextView Delegate
extension ActionEditVC:UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""{
            theCurrentView.lblPlaceHolder.isHidden = false
        }else{
            theCurrentView.lblPlaceHolder.isHidden = true
        }
    }
}

//MARK:- UITextField Delegate
extension ActionEditVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if theCurrentView.txtDuration == textField{
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if theCurrentView.txtComopleted == textField{
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

//MARK:- UITableViewDataSource
extension ActionEditVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return theCurrentModel.cellType.count
        if tableView == theCurrentView.tableView {
            return theCurrentModel.arr_subTaskList.count
        }
        return theCurrentModel.arr_notesHistoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == theCurrentView.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTaskNewCell") as! SubTaskNewCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            cell.isHidden = theCurrentModel.arr_subTaskList[indexPath.row].is_del == "1"
            if theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
                cell.hideEditAndCloseButton()
                cell.isUserInteractionEnabled = false
            }
            cell.configure(strTitle: theCurrentModel.arr_subTaskList[indexPath.row].subTaskName,isSelectedCheck: theCurrentModel.arr_subTaskList[indexPath.row].isSelected)
//            cell.handlerOnEditAction = { [weak self] in
//                 self?.handleAddOrEditSubTaskButton(index: indexPath.row, isEdit: true)
//            }
            cell.handlerTextChanged = { (text) in
                tableView.beginUpdates()
                tableView.endUpdates()
            }
            cell.handlerOnEditcompletedAction = { [weak self] (text) in
                if let model = self?.theCurrentModel.arr_subTaskList[indexPath.row] {
                    model.subTaskName = text
                    self?.addAndUpdateSubTaskWebService(theModel: model, index: indexPath.row, isEdit: true, isUpdateStatus: (false,false), isRemove: false)
                }
            }
            cell.handlertextViewEndEditText = { [weak self] (text) in
                self?.uppdateEditSubTaskText(strSubTask: text, index: indexPath.row)
            }
            cell.handlerOnCheckAction = { [weak self, unowned cell] in
                self?.theCurrentModel.arr_subTaskList[indexPath.row].subTaskName = cell.lblTitle.text ?? ""
                if let model = self?.theCurrentModel.arr_subTaskList[indexPath.row] {
                    let completed = !model.isSelected ? true : false
                    self?.addAndUpdateSubTaskWebService(theModel: model, index: indexPath.row, isEdit: false, isUpdateStatus: (completed,!completed), isRemove: false)
                }
//                self?.handleCheckSubTask(index: indexPath.row)
            }
            cell.handlerOnDeleteAction = { [weak self] (tag) in
                
                if let model = self?.theCurrentModel.arr_subTaskList[indexPath.row] {
                    self?.addAndUpdateSubTaskWebService(theModel: model, index: indexPath.row, isEdit: false, isUpdateStatus: (false,false), isRemove: true)
                }
                /*
                let taskName = self?.theCurrentModel.arr_subTaskList[indexPath.row].subTaskName
                if let selectedIndex = self?.theCurrentModel.theActionDetailModel?.subTaskList.firstIndex(where: {$0.subTaskName == taskName}) {
                    
                    let objsubTask = self?.theCurrentModel.arr_subTaskList[indexPath.row]
                    if(objsubTask?.is_new_task == "1")
                    {
                        objsubTask?.is_del = "0"
                        //TODO:- Remove from array
                        self?.theCurrentModel.theActionDetailModel?.subTaskList.remove(at: selectedIndex)
                    }
                    else
                    {
                        objsubTask?.is_del = "1"
                        objsubTask?.subTaskName = (objsubTask?.subTaskId)!
                        self?.theCurrentModel.arr_subTaskList[indexPath.row] = objsubTask!
                        self?.theCurrentModel.theActionDetailModel?.subTaskList[selectedIndex] = objsubTask!

                    }
//                    self?.theCurrentModel.arr_subTaskList.remove(at: indexPath.row)

                }*/
                
                tableView.reloadData()
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
            if theCurrentModel.arr_notesHistoryList?.count != nil{
                let data = theCurrentModel.arr_notesHistoryList![indexPath.row]
                var strF = NSAttributedString(string: "")
                if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strF = "\(createdDate) ".attributedString(color: black, font: R14)!
                }
//                    attributedString(string: "\(data.createdDate) ", color: black, font: R14)!
                var strS = NSAttributedString(string: "")
                //            strS = data.actionUserName
                strS = data.actionUserName.attributedString(color: black, font: R14)!
//                    attributedString(string: data.actionUserName, color: black, font: R14)!
                var strT = NSAttributedString(string: "")
                var strFt = NSAttributedString(string: "")
                var strSx = NSAttributedString(string: "")
                var strSth = NSAttributedString(string: "")
                
                switch data.historyStatus{
                case APIKey.key_Assigned:
                    cell.imgPercentage.image = UIImage.init(named: "ic_contact")
                    cell.vwOuter.backgroundColor = appColorFilterBlue
                    //                    strT = "Assigned to"
                    strT = " Assigned to ".attributedString(color:black, font: R14)!
                    //                    strFt = data.assignedUserName
                    
                    strFt = " \(data.assignedUserName) ".attributedString(color: black, font: R14)!
                    break
                case APIKey.key_Percentage:
                    cell.imgPercentage.image = UIImage.init(named: "ic_percentage_icon")
                    cell.vwOuter.backgroundColor = appColorFilterBlack
                    //                    strT = "Completed \(data.percentageCompleted)%"
                    strT = " Completed \(data.percentageCompleted)% ".attributedString(color: black, font: R14)!
                    break
                case APIKey.key_Commented:
                    cell.imgPercentage.image = UIImage.init(named: "ic_meassage")
                    cell.vwOuter.backgroundColor = appColorFilterBlue
                    //                    strF = data.actionUserName
//                    let attachment = NSTextAttachment()
//                    let img = UIImage(named: "ic_time_icon")
//                    img?.size = CGSize(width: 12.0, height: 12.0)
//                    attachment.image = UIImage(named: "ic_time_icon")
//                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
//                    attachment.image?.size = CGSize(width: 12.0, height: 12.0)
                    strFt = " \(data.actionUserName) ".attributedString(color: black, font: R14)!
                    strS = NSAttributedString(string: "")
                    strT = NSAttributedString(string: "")
//                    strFt = NSAttributedString(attachment: attachment)
//                    if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
//                        strSx = "\(createdDate)".attributedString(color: black, font: R14)!
//                    }
                    strSx = ":".attributedString(color: black, font: R14)!
                    strSth = " \(data.notesHistoryDescription) ".attributedString(color: black, font: R14)!
                    //                    strSth = "Notes Text \(data.noteId)"
                    break
                case APIKey.key_Revised:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    strT = " Revised Due Date To ".attributedString(color: black, font: R14)!
                    //                    strT = "Revised Due Date To"
//                    let attachment = NSTextAttachment()
//                    attachment.image = UIImage(named: "ic_time_icon")
//                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)

                    strFt = NSAttributedString(string: " ")
//                    strFt = NSAttributedString(attachment: attachment)
                    if let revisedDate = data.revisedDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                        strSx = "\(revisedDate)".attributedString(color: black, font: R14)!
                    }
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Worked:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    strT = " Revised Work Date To ".attributedString(color: black, font: R14)!
                    //                    strT = "Revised Work Date To"
//                    let attachment = NSTextAttachment()
//                    attachment.image = UIImage(named: "ic_time_icon")
//                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                    strFt = NSAttributedString(string: " ")
                    if let workedDate = data.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                        strSx = "\(workedDate) ".attributedString(color: black, font: R14)!
                    }
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Opened:
                    cell.imgPercentage.image = UIImage.init(named: "ic_left_icon")
                    cell.vwOuter.backgroundColor = appColorFilterGreen
                    strT = " Opened an Action. ".attributedString(color: black, font: R14)!
                    //                    strT = "Opened an Action."
                    strFt = NSAttributedString(string: "")
                    strSx = NSAttributedString(string: "")
                    strSth = NSAttributedString(string: "")
                    break
                case APIKey.key_Closed:
                    cell.imgPercentage.image = UIImage.init(named: "ic_right_icon")
                    cell.vwOuter.backgroundColor = appColorFilterRed
                    strT = " Closed an Action. ".attributedString(color: black, font: R14)!
                    //                    strT = "Closed an Action."
                    strFt = NSAttributedString(string: "")
                    strSx = NSAttributedString(string: "")
                    strSth = NSAttributedString(string: "")
                    break
                default:
                    cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                    cell.vwOuter.backgroundColor = appColorFilterYellow
                    break
                }
                let FormatedString = NSMutableAttributedString()
                
                FormatedString.append(strF)
                FormatedString.append(strS)
                FormatedString.append(strT)
                FormatedString.append(strFt)
                FormatedString.append(strSx)
                FormatedString.append(strSth)
                cell.lblHistory.attributedText = FormatedString
            }
            return cell
        }
        
    }
}
//MARK:-UITableViewDelegate
extension ActionEditVC:UITableViewDelegate {
    /*func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }*/
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == theCurrentView.tableView {
            return theCurrentModel.arr_subTaskList[indexPath.row].is_del == "1" ? CGFloat.leastNormalMagnitude : UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        if tableView == theCurrentView.tableView {
            return true
        }
        return false
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if tableView == theCurrentView.tableView {
            let object = theCurrentModel.arr_subTaskList[sourceIndexPath.row]
            theCurrentModel.arr_subTaskList.remove(at: sourceIndexPath.row)
            theCurrentModel.arr_subTaskList.insert(object, at: destinationIndexPath.row)
            
            let objectNew = theCurrentModel.theActionDetailModel!.subTaskList[sourceIndexPath.row]
            theCurrentModel.theActionDetailModel!.subTaskList.remove(at: sourceIndexPath.row)
            theCurrentModel.theActionDetailModel!.subTaskList.insert(objectNew, at: destinationIndexPath.row)
            print("new changes")
        }
    }
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if theCurrentModel.cellType[indexPath.row] == .subtask {
            return theCurrentModel.subtaskCellHeight
        } else {
            return UITableView.automaticDimension
        }
    }*/
}

//MARK:- DragAndDropTableViewDataSource And DragAndDropTableViewDelegate
extension ActionEditVC:DragAndDropTableViewDataSource,DragAndDropTableViewDelegate {
    func canCreateNewSection(_ section: Int) -> Bool {
        return false
    }
    func tableView(_ tableView: DragAndDropTableView!, willBeginDraggingCellAt indexPath: IndexPath!, placeholderImageView placeHolderImageView: UIImageView!) {
        placeHolderImageView.layer.shadowOpacity = 0.3
        placeHolderImageView.layer.shadowRadius = 1
    }
    func tableView(_ tableView: DragAndDropTableView!, didEndDraggingCellAt sourceIndexPath: IndexPath!, to toIndexPath: IndexPath!, placeHolderView placeholderImageView: UIImageView!) {
        print("delegate called")
        print("theCurrentModel.arr_subTaskList:=",theCurrentModel.arr_subTaskList)
//        let model = theCurrentModel.arr_subTaskList[sourceIndexPath.row]
//        theCurrentModel.arr_subTaskList.remove(at: sourceIndexPath.row)
//        theCurrentModel.arr_subTaskList.insert(model, at: toIndexPath.row)
        tableView.beginUpdates()
//        tableView.reloadRows(at: [sourceIndexPath,toIndexPath], with: .none)
        tableView.reloadData()
        tableView.endUpdates()
        changeSubTaskListOrderWebService()
    }
    
    
    /*func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
     return false
     }
     func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
     return UITableViewCell.EditingStyle.none
     }
     func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
     return false
     }
     func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     if tableView == theCurrentView.tableView {
     return true
     }
     
     return false
     }
     func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
     if tableView == theCurrentView.tableView {
     
     }
     }*/
    /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     if theCurrentModel.cellType[indexPath.row] == .subtask {
     return theCurrentModel.subtaskCellHeight
     } else {
     return UITableView.automaticDimension
     }
     }*/
}

//MARK:- UICollectionView datasource & delegate

extension ActionEditVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentModel.arr_selectedFile.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectFileCell", for: indexPath) as! SelectFileCell
        if theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
            cell.btnCloseOutlet.isHidden = true
        }
//        cell.bgView.setBackGroundColor = theCurrentModel.arr_selectedFile[indexPath.row].fileType == .audio ? 3 : 8
    
        cell.lblFileName.text = (theCurrentModel.arr_selectedFile[indexPath.row].fileName as NSString).lastPathComponent

        cell.btnCloseOutlet.tag = indexPath.row
        cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveAttachmentAction(_:)), for: .touchUpInside)

        
        /*if theCurrentModel.arr_selectedAudioFile.count != 0 && theCurrentModel.arr_selectedAudioFile.count == 1 && indexPath.row == 0 {
            let selectFile = theCurrentModel.arr_selectedAudioFile[indexPath.row]
            cell.bgView.setBackGroundColor = 3
            cell.lblFileName.text = selectFile.selectedFileName
            cell.btnCloseOutlet.tag = indexPath.row
            cell.btnCloseOutlet.addTarget(self, action: #selector(btnRemoveAudioAction(_:)), for: .touchUpInside)
        } else {
            
            if theCurrentModel.arr_selectedAudioFile.count != 0 {
                cell.lblFileName.text = theCurrentModel.arr_selectedFile[indexPath.row - 1].selectedFileName
            } else {
                cell.lblFileName.text = theCurrentModel.arr_selectedFile[indexPath.row].selectedFileName
            }
            
            cell.btnCloseOutlet.tag = indexPath.row
            cell.btnCloseOutlet.addTarget(self, action: #selector(onBtnRemoveFileAction(_:)), for: .touchUpInside)
        }*/
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if theCurrentModel.arr_selectedFile[indexPath.row].fileName.isContainAudioOrVideoFile() {
            self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
        } else {
            self.presentWebViewVC(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName, isLocal: !theCurrentModel.arr_selectedFile[indexPath.row].isOldAttachment)
        }
        
        /*
        if theCurrentModel.arr_selectedFile[indexPath.row].fileType == .audio {
            if !theCurrentModel.arr_selectedFile[indexPath.row].isOldAttachment, let audioData = theCurrentModel.arr_selectedFile[indexPath.row].fileData {
                if let url = self.setAudioFileToDocumentDirectory(audioData: audioData, audioNameWithExtension: "\(APIKey.key_Audio_name).\(APIKey.key_Extension)") {
                    self.presentAudioPlayer(url: url,isLocalURl: true)
                }
            } else {
                if theCurrentModel.arr_selectedFile[indexPath.row].fileName.contains(".wav") {
                    let fileName = theCurrentModel.arr_selectedFile[indexPath.row].fileName
                    let lastFile = (fileName as NSString).lastPathComponent.replacingOccurrences(of: ".wav", with: ".mp3")
                    print("lastFile:=",lastFile)
                    self.downloadAudioFileWebService(strURl: fileName) { [weak self] (fileData) in
                        if let url = self?.setAudioFileToDocumentDirectory(audioData: fileData, audioNameWithExtension: lastFile) {
                            self?.presentAudioPlayer(url: url,isLocalURl: true)
                        }
                    }
                } else {
                    self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
                }
//                self.presentAudioPlayer(url: theCurrentModel.arr_selectedFile[indexPath.row].fileName)
            }
        }*/
    }
    
}


//MARK:- TagView Delegate
extension ActionEditVC:HTagViewDelegate, HTagViewDataSource {
    func numberOfTags(_ tagView: HTagView) -> Int {
        return theCurrentModel.arr_AddTagList.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return theCurrentModel.arr_AddTagList[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .cancel
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
    
    
    // MARK:- HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
    }
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        print("tag with index: '\(index)' has to be removed from tagView")
        updateTagList(strTag: "", isAdded: false, at: index)
//        handlerRemoveTagAction(index)
    }
    
}

//MARK:- UIDocumentPickerDelegate
extension ActionEditVC:UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("urls:=",urls)
        var arrayInvalidFilesFormate : [String] = []
        for i in 0..<urls.count{
            let fileName = urls[i].lastPathComponent
            print("fileName:=",fileName)
            let strExtension = GetFileExtension(strFileName: fileName)
            if(CheckValidFile(strExtension: strExtension))
            {
                let data = try! Data(contentsOf: urls[i])
                
                if !data.isLessThen10MB() {
                    self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
                } else {
                    var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                    if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                        if(structSelectedFile.fileName == fileName)
                        {
                            self.showAlertAtBottom(message: "File already exists,please select another file")
                            return true
                        }
                        return false
                    })
                    {
                        
                    }
                    else{
                        if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                            objSelectedData.fileName = localFile
                            theCurrentModel.arr_selectedFile.append(objSelectedData)
                        } else {
                            self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                        }
                    }
                }
            }
            else{
                arrayInvalidFilesFormate.append(fileName)
            }
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        print("fileName:=",fileName)
        var arrayInvalidFilesFormate : [String] = []
        let strExtension = GetFileExtension(strFileName: fileName)
        if(CheckValidFile(strExtension: strExtension))
        {
            let data = try! Data(contentsOf: url)
            if !data.isLessThen10MB() {
                self.showAlertAtBottom(message: "This \(fileName) file should be less then 10 MB")
            } else {
                var objSelectedData = AttachmentFiles.init(fileName: fileName, fileData: data)
                if theCurrentModel.arr_selectedFile.contains(where: { (structSelectedFile) -> Bool in
                    if(structSelectedFile.fileName == fileName)
                    {
                        self.showAlertAtBottom(message: "File already exists,please select another file")
                        return true
                    }
                    return false
                })
                {
                    
                }
                else{
                    if let localFile = self.setAttachmentFileToDocumentDirectory(data: data, fileNameWithExtension: fileName.replacingOccurrences(of: " ", with: "_")) {
                        objSelectedData.fileName = localFile
                        theCurrentModel.arr_selectedFile.append(objSelectedData)
                    } else {
                        self.showAlertAtBottom(message: "This file is not supported \(fileName)")
                    }
                }
            }
        }
        else{
            arrayInvalidFilesFormate.append(fileName)
        }
        if(arrayInvalidFilesFormate.count > 0)
        {
            print("Invalid files - ",arrayInvalidFilesFormate)
            self.showAlertAtBottom(message: "This file is not supported, \(arrayInvalidFilesFormate.joined(separator: ","))")
        }
        controller.dismiss(animated: true, completion: nil)
        self.theCurrentView.collectionSelectedFile.reloadData()
        

    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}

//MARK:- IQAudioRecorderViewControllerDelegate
extension ActionEditVC:IQAudioRecorderViewControllerDelegate {

    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        print("filepath:=\(filePath)")
        ConvertAudio(audioUrl: URL(fileURLWithPath: filePath)) { [weak self] (url) in
            if let data = try? Data(contentsOf: url!)
            {
                if !data.isLessThen10MB() {
                    DispatchQueue.main.async { [weak self] in
                        self?.showAlertAtBottom(message: "Voice note should be less then 10 MB")
                    }
                } else {
                    //                self?.removeFileFromDocumentDirectory(itemName: APIKey.key_Audio_name, fileExtension: APIKey.key_Extension)
                    let objSelectedData = AttachmentFiles.init(fileName: url?.absoluteString ?? "", fileData: data, fileType: .audio)
                    
                    //                let objSelectedData = SelectedFile.init(selectedAudioFileName: "\(APIKey.key_Audio_name)-\(Int(Date().timeIntervalSince1970)).\(APIKey.key_Extension)", selectedAudioFileData: data)
                    self?.theCurrentModel.arr_selectedFile.append(objSelectedData)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.theCurrentView.collectionSelectedFile.reloadData()
                        //                    self?.theCurrentView.viewOfAudio.isHidden = false
                        //                    self?.theCurrentView.lblVoiceNotesName.text = objSelectedData.selectedFileName
                    }

                }
            }
           
        }
        controller.dismiss(animated: true, completion: nil)
        
    }
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
}


//MARK:- Call API
extension ActionEditVC {
    func changeSubTaskListOrderWebService() {
        let filterSubTaskData = theCurrentModel.arr_subTaskList.filter({ $0.is_new_task != "1" && ($0.is_del != "1") })
        print("filterSubTaskData:=",filterSubTaskData)
        if filterSubTaskData.count == 0 {
            return
        }
        var jsonData:[[String:String]] = []
        var reverseCount = filterSubTaskData.count
        for index in 0..<filterSubTaskData.count {
            let data = [APIKey.key_id:filterSubTaskData[index].subTaskId,APIKey.key_order:"\(reverseCount)"]
            jsonData.append(data)
            reverseCount -= 1
        }
        
        var jsonString = ""
        if let strJson = Utility.shared.json(from: jsonData) {
            jsonString = strJson
        }
        Utility.shared.showActivityIndicator()
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_action:jsonString]
        ActionsWebService.shared.changeSubTaskListOrder(parameter: parameter, success: { (msg) in
            Utility.shared.stopActivityIndicator()
        }, failed: { (error) in
            Utility.shared.stopActivityIndicator()
        })
    }
    
    func addAndUpdateSubTaskWebService(theModel:SubTaskList,index:Int,isEdit:Bool,isUpdateStatus:(Bool,Bool),isRemove:Bool) {
        self.view.endEditing(true)
        
        Utility.shared.showActivityIndicator()
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_action_log_id:theCurrentModel.actionId,
                         APIKey.key_task_name:theModel.subTaskName]
        if isRemove {
            parameter[APIKey.key_sub_task_id] = theModel.subTaskId
            parameter[APIKey.key_status] = APIKey.key_removed
        } else if isUpdateStatus.0 { // completed
            parameter[APIKey.key_sub_task_id] = theModel.subTaskId
            parameter[APIKey.key_status] = APIKey.key_completed_status
        } else if isUpdateStatus.1 { // inProgress
            parameter[APIKey.key_status] = APIKey.key_in_progress
        } else if isEdit {
            parameter[APIKey.key_sub_task_id] = theModel.subTaskId
        } else {
            // For Add
        }
        
        ActionsWebService.shared.addAndUpdateSubTask(parameter: parameter, success: { [weak self] (strMsg, theModelData) in
            if isRemove , let model = self?.theCurrentModel.arr_subTaskList[index] {
                self?.updateSubTaskOperation(theModel: model, index: index, isEdit: isEdit, isUpdateStatus: isUpdateStatus, isRemove: isRemove)
            } else if let model = theModelData {
                self?.updateSubTaskOperation(theModel: model, index: index, isEdit: isEdit, isUpdateStatus: isUpdateStatus, isRemove: isRemove)
            }
            Utility.shared.stopActivityIndicator()
        }, failed: { (error) in
            Utility.shared.stopActivityIndicator()
        })
    }
    
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theCurrentModel.actionId,
                         APIKey.key_history_for: APIKey.key_action_logs]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
        
        //guard let actionId = theCurrentModel.theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:theCurrentModel.actionId,
            APIKey.key_status : status,
            APIKey.key_note_for: APIKey.key_action_logs,
            APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            self?.theCurrentView.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.theCurrentModel.nextOffset = -1
                self?.loadViewMoreNoteHistory(isAnimating: false)

//                self?.setBackground(strMsg: error)
        })
    }
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentModel.strNoteStatus ?? "")
            self?.theCurrentView.txtNote.text = ""
            completion()
            }, failed: { (error) in
                completion()
                self.showAlertAtBottom(message: error)
        })
    }
    func actionTagListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_is_add_form_tag:"1"]
        ActionsWebService.shared.getTagList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_actionTagList = list
            if self?.theCurrentModel.isEditScreen == .edit{
//                self?.setupDefaulTagData()
            }
            }, failed: { (error) in
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func companylistService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator, imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
             self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            self?.theCurrentModel.arr_companyList = list
            }, failed: { [weak self] (error) in
                 self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func assignUserListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        self.theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator,imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
             self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            if let obj = self {
                obj.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: obj.theCurrentView.activityApprovedIndicator, imgView: obj.theCurrentView.imgApproved)
            }
            self?.theCurrentModel.arr_assignList = list
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: activityIndicator, imgView: imgView)
                if let obj = self {
                    obj.theCurrentView.statusActivityIndicator(isLoading: false,activityIndicator: obj.theCurrentView.activityApprovedIndicator, imgView: obj.theCurrentView.imgApproved)
                }
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func departmentListService(activityIndicator:UIActivityIndicatorView,imgView:UIImageView) {
        self.theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: activityIndicator,imgView: imgView)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:theCurrentModel.strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_departmentList = list
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
            if let obj = self, obj.theCurrentModel.isEditScreen == .add, list.count > 0 {
                obj.updateDropDownData(categorySelection: .department, str: list[0].name, cellType: .department, index: 0)
            } else if let obj = self, obj.theCurrentModel.isEditScreen == .edit, obj.theCurrentModel.strSelectedDepartmentID.isEmpty, list.count > 0 {
                obj.updateDropDownData(categorySelection: .department, str: list[0].name, cellType: .department, index: 0)
            }
            }, failed: { [weak self] (error) in
                self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: activityIndicator,imgView: imgView)
//                self?.showAlertAtBottom(message: error)
        })
    }
    
    func addActionWithAttachment(parameter:[String:Any]) {
        var fileData:[Data] = []
        var fileMimeType:[String] = []
        var withName:[String] = []
        var withFileName:[String] = []
        var index = 0
        var removeFile:[[String:String]] = []
        var removeAttachment:[String] = []

        var voiceCount = 0
        var fileCount = 0
        
        for item in theCurrentModel.arr_RemoveSelectedFile {
            if item.isOldAttachment {
                removeAttachment.append(item.attachmentID)
            }
//            removeFile.append([APIKey.key_id:item.attachmentID])
        }
        
        for i in 0..<theCurrentModel.arr_selectedFile.count {
            if theCurrentModel.arr_selectedFile[i].isDeleteAttachment {
                removeFile.append([APIKey.key_id:theCurrentModel.arr_selectedFile[i].attachmentID])
            } else {
                if !theCurrentModel.arr_selectedFile[i].isOldAttachment && (theCurrentModel.arr_selectedFile[i].fileData?.count)! > 0 {
                    let path = theCurrentModel.arr_selectedFile[i].fileName
                    let mimeType = MimeType(path: path).value
                    //                    let mimeExtension = mimeType.components(separatedBy: "/")
                    //                    let extensionName = mimeExtension.indices.contains(1) ? mimeExtension[1] : ""
                    
                    fileData.append(theCurrentModel.arr_selectedFile[i].fileData!)
                    fileMimeType.append(mimeType)
                    if theCurrentModel.arr_selectedFile[i].fileType == .audio {
                        withName.append("voice_notes[\(voiceCount)]")
                        withFileName.append(path)
                        voiceCount += 1
                    } else {
                        withName.append("files[\(fileCount)]")
                        withFileName.append(path)
                        fileCount += 1
                    }
                    index += 1
                }
            }
        }
        
        var param = parameter
        param[APIKey.key_remove_attechment] = removeAttachment.joined(separator: ",")

        
        isClickOnAdd(isLoading: true)
        
        if theCurrentModel.isEditScreen == .edit{
            ActionsWebService.shared.uploadEditActionAttachment(parameter: param, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] (actionList) in
//                self?.updateActionDetailModel()
                self?.handlerEditAction(actionList)
                self?.isClickOnAdd(isLoading: false)
                self?.backAction(isAnimation: false)
                }, failed: { [weak self] (error) in
                    self?.isClickOnAdd(isLoading: false)
                    self?.showAlertAtBottom(message: error)
            })
        }else{
            ActionsWebService.shared.uploadAddActionAttachment(parameter: param, files: fileData, withName: withName, withFileName: withFileName, mimeType: fileMimeType, success: { [weak self] in
                self?.isClickOnAdd(isLoading: false)
                if let isForAddAction = self?.theCurrentModel.isForAddAction,isForAddAction {
                    self?.handlerEditAction(nil)
                }
                self?.backAction(isAnimation: false)
                }, failed: { [weak self] (error) in
                    self?.isClickOnAdd(isLoading: false)
                    self?.showAlertAtBottom(message: error)
            })
        }
       
    }
    
}
