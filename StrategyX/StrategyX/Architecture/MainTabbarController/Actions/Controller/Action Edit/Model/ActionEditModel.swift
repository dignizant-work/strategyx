//
//  ActionEditModel.swift
//  StrategyX
//
//  Created by Haresh on 04/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class ActionEditModel {

    //MARK:- Variable
    enum celltype : Int {
        case header                         = 0
        case createdBy                      = 1
        case title                          = 2
        case relatedToGeneral               = 3
        case relatedToRisk                  = 4
        case relatedToFocus                 = 5
        case relatedToTacticalProject       = 6
        case primaryArea                    = 7
        case secondaryArea                  = 8
        case strategyGoal                   = 9
        case tag                            = 10
        case company                        = 11
        case assignTo                       = 12
        case department                     = 13
        case approvedBy                     = 14
        case createdDate                    = 15
        case dueDate                        = 16
        case workDate                       = 17
        case duration                       = 18
        case subtask                        = 19
        case attachment                     = 20
        case bottom                         = 21
    }
    
    enum relatedToSelectionType:String {
        case strategy            = "Strategy"
        case risk                = "Risk"
        case tactical            = "Tactical Project"
        case focus               = "Focus"
        case general             = "General Action"
    }
    
    private weak var theController:ActionEditVC!
    var cellType:[celltype] = [.header,.createdBy,.title,.relatedToGeneral,.tag,.company,.assignTo,.department,.approvedBy,.createdDate,.dueDate,.workDate,.duration,.subtask,.attachment,.bottom]
    var tableviewCount = 16
    var imageBG:UIImage?
//    var subtaskCellHeight:CGFloat = 85.0
   
//    var arr_SubTaskList:[String] = []
    var arr_subTaskList : [SubTaskList] = []
    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?

    var isForAddAction = false
    var isForAddNoteAction = false

    var relatedToSelectionType = AddActionModel.relatedToSelectionType.general
   
    // RiskDetailModel
    
    var theActionDetailModel:ActionDetail?
    var isEditScreen = viewType.add
    
    var selectedData:JSON = JSON(["category":"","company":"","department":"","description":"","assignto":"","relatedto":"","approvedby":"","projectname":"","createdate":"","duedate":"","workdate":"","reviseddate":""])
    
    var arr_selectedFile:[AttachmentFiles] = []
    var arr_RemoveSelectedFile:[AttachmentFiles] = []

    
//    // Select Audio file
//    var arr_selectedAudioFile:[SelectedFile] = []
//    
//    // Selected File Data
//    var arr_selectedFile:[SelectedFile] = []
    
    
    // Related to tag List
    var arr_actionTagList:[TagList] = []
    var arr_AddTagList:[String] = []
    var arr_EditTagList:[TagList] = []
  //  var strSelectedStrategyGoalId = ""
    
    // Selected due date and  work dat
    var selectedDueDate:Date?
    var selectedWorkDate:Date?
    var selectedRevisedDueDate:Date?
    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    var strSelectedApprovedID:String = ""
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    // Duration
    var selectedDuration:Date? = Date()
    
    
//    // remove audio file
//    var strRemoveAudioId:String = "0"
//
//    var arr_fileList : [FileList] = []
//
//    var arr_strRemoveAttachment:[String] = []
    
    var strNoteStatus:String = ""
    var actionId = ""
    var arr_noteHistoryStatus:[JSON] = []

    
    //MARK:- LifeCycle
    init(theController:ActionEditVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
    
}




