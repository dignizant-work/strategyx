//
//  ActionView.swift
//  StrategyX
//
//  Created by Jaydeep on 25/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import HTagView

class ActionDetailView: ViewParentWithoutXIB {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblActionTitleName: UILabel!
    @IBOutlet weak var tagView: HTagView!
    
    @IBOutlet weak var lblSubtaskDash: UILabel!
    @IBOutlet weak var tblSubTask: UITableView!
    
    @IBOutlet weak var lblAttachmentDash: UILabel!
    @IBOutlet weak var tblAttachment: UITableView!
    @IBOutlet weak var tblNotesHistory: UITableView!
    @IBOutlet weak var heightOfTblSubTask: NSLayoutConstraint!
    @IBOutlet weak var heightOfTblAttachment: NSLayoutConstraint!
    @IBOutlet weak var heightOfNotesHistory: NSLayoutConstraint!
    @IBOutlet weak var heightOfTag: NSLayoutConstraint!
    @IBOutlet weak var lblCreatedByName: UILabel!
    @IBOutlet weak var lblRelatedToName: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAssignName: UILabel!
    @IBOutlet weak var lblDepartmentName: UILabel!
    @IBOutlet weak var lblApprovedName: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblWorkDate: UILabel!
    @IBOutlet weak var lblRevisedDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblCompleted: UILabel!
    @IBOutlet weak var txtNotesHistory: UITextField!
    @IBOutlet weak var mainScrollview: UIScrollView!
    @IBOutlet weak var viewOfPager: UIView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var lblTagMsg: UILabel!
    @IBOutlet weak var btnEditOutlet: UIButton!
    @IBOutlet weak var widthOfEditOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var viewCompany: UIView!
    @IBOutlet weak var viewAddPost: UIView!
    @IBOutlet weak var txtAddPost: UITextField!
    
    @IBOutlet weak var viewMore: UIView!
    
    //MARK:- Life Cycle
    func setupLayout() {
        lblAttachmentDash.isHidden = true
        lblSubtaskDash.isHidden = true
        if UserDefault.shared.userRole != UserRole.superAdminUser {
            viewCompany.isHidden = true
        }
    }
    func setupTableView(theDelegate:ActionDetailVC) {
      
        [tblSubTask,tblAttachment,tblNotesHistory].forEach { (tableView) in
            tableView?.estimatedRowHeight = 30
            tableView?.rowHeight = UITableView.automaticDimension            
            tableView?.delegate = theDelegate
            tableView?.dataSource = theDelegate
            tableView?.separatorStyle = .none
        }
        
        tblSubTask.registerCellNib(SubTaskCell.self)
        tblAttachment.registerCellNib(SubTaskCell.self)
        tblNotesHistory.registerCellNib(HistoryCell.self)
        
    }
    
    func setupTagView(theDelegate:ActionDetailVC)  {
        [tagView].forEach { (viewTag) in
            viewTag?.delegate = theDelegate
            viewTag?.dataSource = theDelegate
            viewTag?.multiselect = false
            viewTag?.marg = 2
            viewTag!.btwTags = 2
            viewTag?.btwLines = 2
            viewTag?.tagFont = R12
            viewTag?.tagMainBackColor = appBgLightGrayColor
            viewTag?.tagMainTextColor = white
            viewTag?.tagSecondBackColor = appBgLightGrayColor
            viewTag?.tagSecondTextColor = white       
            viewTag?.reloadData()
        }
        self.heightOfTag.constant = 50
    }
    
    func configureActivityIndicator(activityIndicator:UIActivityIndicatorView) {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
//        activityIndicator.isHidden = true
    }
    
    func showSkeletonAnimation(){
        [lblActionTitleName,lblCreatedByName,lblRelatedToName,lblCompanyName,lblAssignName,lblDepartmentName,lblApprovedName,lblCreatedDate,lblWorkDate,lblRevisedDate,lblDueDate,lblDuration,lblCompleted].forEach({ $0.showAnimatedSkeleton() })
    }
    
    func hideSkeletonAnimation() {
        [lblActionTitleName,lblCreatedByName,lblRelatedToName,lblCompanyName,lblAssignName,lblDepartmentName,lblApprovedName,lblCreatedDate,lblWorkDate,lblRevisedDate,lblDueDate,lblDuration,lblCompleted].forEach({ $0?.hideSkeleton() })
    }
}
