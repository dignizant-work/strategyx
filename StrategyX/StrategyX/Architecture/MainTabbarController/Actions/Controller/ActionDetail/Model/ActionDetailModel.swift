//
//  ActionDetailModel.swift
//  StrategyX
//
//  Created by Jaydeep on 25/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class ActionDetailModel {
    
    //MARK:- Variable
    private weak var theController:ActionDetailVC!
//    var theActionsListModel:ActionsList?
//    var theActionsArchivedListModel:ActionsArchivedList?
        
    var arr_tagList:[TagList] = []
    var arr_fileList:[FileList] = []
    var arr_subTaskList:[SubTaskList] = []
    var arr_tag:[String] = []
    var nextOffset = 0
    var arr_notesHistoryList:[NotesHistory]?
    var arr_noteHistoryStatus:[JSON] = []
    var strNoteStatus:String = ""
    var isComeFromAction:Bool = false
    var actionId = String()
    
    var theActionDetailModel:ActionDetail? = nil{
        didSet {
            if let controller = theController.navigationController?.viewControllers{
                for vc in controller {
//                    (vc as? ActionVC)?.updateActionListModel(theActionDetailModel: theActionDetailModel!)
                    (vc as? ActionArchivedVC)?.updateActionArchivedListModel(theActionDetailModel: theActionDetailModel!)
                }
            }
        }
    }
    
    //MARK:- LifeCycle
    init(theController:ActionDetailVC) {
        self.theController = theController
    }
    
    func updateList(list:[NotesHistory], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_notesHistoryList?.removeAll()
        }
        nextOffset = offset
        if arr_notesHistoryList == nil {
            arr_notesHistoryList = []
        }
        list.forEach({ arr_notesHistoryList?.append($0) })
        print(arr_notesHistoryList!)
    }
}
