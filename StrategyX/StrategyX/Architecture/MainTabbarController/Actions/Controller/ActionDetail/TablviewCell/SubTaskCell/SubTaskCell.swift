//
//  SubTaskCell.swift
//  StrategyX
//
//  Created by Jaydeep on 25/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SubTaskCell: UITableViewCell {
    
    
    //MARK:- Outlet Zone
    
    
    @IBOutlet weak var lblHistory: UILabel!
    //MARK:- View Life Cycle
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
