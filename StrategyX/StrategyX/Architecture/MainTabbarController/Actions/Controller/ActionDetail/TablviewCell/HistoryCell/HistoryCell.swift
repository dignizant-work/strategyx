//
//  HistoryCell.swift
//  StrategyX
//
//  Created by Jaydeep on 25/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgPercentage: UIImageView!
    @IBOutlet weak var lblHistory: UILabel!
    
    
    //MARK:- ViewLifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attStr1 = attributedString(string1: "20 Mar 2018, 10:42 PM", color1: appBgLightGrayColor, font1: R15)
        
        let attStr2 = attributedString(string1: " JD ", color1: appTextLightGrayColor, font1: B15)
        
        let attStr3 = attributedString(string1: "Assigned to CFO", color1: appBgLightGrayColor, font1: R15)
        
        let FormatedString = NSMutableAttributedString()
        
        FormatedString.append(attStr1!)
        FormatedString.append(attStr2!)
        FormatedString.append(attStr3!)
        
        lblHistory.attributedText = FormatedString
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
        // Configure the view for the selected state
    }
    
    //MARK:- Other mehods
    func attributedString(string1 : String, color1: UIColor,font1:UIFont) -> NSAttributedString?
    {
        let attributes1 = [
            NSAttributedString.Key.font : font1,
            NSAttributedString.Key.foregroundColor: color1
            ] as [NSAttributedString.Key : Any]
        
       
        let attributedString2 = NSAttributedString(string:string1, attributes: attributes1)
        
        let FormatedString = NSMutableAttributedString()
        
        FormatedString.append(attributedString2)
        ///
        return FormatedString
    }
    
}
