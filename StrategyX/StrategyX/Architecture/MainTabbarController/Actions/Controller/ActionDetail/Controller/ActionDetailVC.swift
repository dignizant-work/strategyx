//
//  ActionDetailVC.swift
//  StrategyX
//
//  Created by Jaydeep on 25/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
//import HTagView
import SwiftyJSON

class ActionDetailVC: ParentViewController {
    
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ActionDetailView = { [unowned self] in
        return self.view as! ActionDetailView
        }()
    
    fileprivate lazy var theCurrentModel:ActionDetailModel = {
        return ActionDetailModel(theController: self)
    }()
    
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuLeftNavigationBarItem()
        
        theCurrentView.tblSubTask.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        theCurrentView.tblAttachment.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        theCurrentView.tblNotesHistory.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
       
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        theCurrentView.tblSubTask.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.tblAttachment.removeObserver(self, forKeyPath: "contentSize")
        theCurrentView.tblNotesHistory.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
           // print("contentSize:= \(theCurrentView.tblAttachment.contentSize.height)")
            self.theCurrentView.heightOfTblAttachment.constant = theCurrentView.tblAttachment.contentSize.height
            
            self.theCurrentView.heightOfTblSubTask.constant = theCurrentView.tblSubTask.contentSize.height
            
            self.theCurrentView.heightOfNotesHistory.constant = theCurrentView.tblNotesHistory.contentSize.height
            
            if theCurrentView.tblAttachment.contentSize.height <= 0{
                self.theCurrentView.heightOfTblAttachment.constant = 0
            }
            
            if theCurrentView.tblSubTask.contentSize.height <= 0{
                self.theCurrentView.heightOfTblSubTask.constant = 0
            }
            
            if theCurrentView.tblNotesHistory.contentSize.height <= 0{
                self.theCurrentView.heightOfNotesHistory.constant = 0
            }
           
        }
    }

    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.setupTagView(theDelegate: self)
        getActionDetailWebService()
        setArrayNotesHistoryStatus()
        theCurrentView.configureActivityIndicator(activityIndicator: theCurrentView.activitySpinner)
        getActionNotesHistoryWebService(isRefreshing: true, status: self.theCurrentModel.strNoteStatus)
        if theCurrentModel.isComeFromAction == false{
            theCurrentView.btnEditOutlet.isHidden = true
            theCurrentView.widthOfEditOutlet.constant = 0
        }
    }
    
    func setupData(theActionId:String,isCheckEditScreen:Bool){
//        theCurrentModel.theActionsListModel = theModel
        theCurrentModel.actionId = theActionId
        theCurrentModel.isComeFromAction = isCheckEditScreen
    }
    
    /*func setupArchivedData(theModel:ActionsArchivedList){
        theCurrentModel.theActionsArchivedListModel = theModel
        theCurrentModel.isComeFromAction = false
    }*/
    
    func setArrayNotesHistoryStatus(){
        theCurrentModel.arr_noteHistoryStatus = []
       
        var dict = JSON()
        dict["name"].stringValue = APIKey.key_All
        dict["status"] = ""
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Assigned_To
        dict["status"] = "assigned"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_completed
        dict["status"] = "percentage"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_posted_notes
        dict["status"] = "commented"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_revised_due_date
        dict["status"] = "revised"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Work_Date
        dict["status"] = "worked"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Opened_an_action
        dict["status"] = "opened"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = APIKey.key_Closed_an_action
        dict["status"] = "closed"
        theCurrentModel.arr_noteHistoryStatus.append(dict)
    }
    func setupData() {
        if theCurrentModel.theActionDetailModel?.actionlogStatus.lowercased() == APIKey.key_archived.lowercased() {
            theCurrentView.viewAddPost.isHidden = true
        }
        theCurrentView.lblActionTitleName.text = theCurrentModel.theActionDetailModel?.actionTitle
        theCurrentView.lblCreatedByName.text = theCurrentModel.theActionDetailModel?.createdBy
        
        if theCurrentModel.theActionDetailModel?.label == "General Action"{
            theCurrentView.lblRelatedToName.text = "\(theCurrentModel.theActionDetailModel?.label ?? "")"
        }else{
            theCurrentView.lblRelatedToName.text = "\(theCurrentModel.theActionDetailModel?.label ?? "") - \(theCurrentModel.theActionDetailModel?.relatedTitle ?? "")"
        }
        theCurrentModel.arr_tag = theCurrentModel.arr_tagList.map({$0.tag_name })
        theCurrentView.tagView.reloadData()
        theCurrentView.lblCompanyName.text = theCurrentModel.theActionDetailModel?.companyName
        theCurrentView.lblAssignName.text = theCurrentModel.theActionDetailModel?.assignedTo
        theCurrentView.lblDepartmentName.text = theCurrentModel.theActionDetailModel?.assignedDepartment.isEmpty ?? false ? "-" : theCurrentModel.theActionDetailModel?.assignedDepartment
        theCurrentView.lblApprovedName.text = theCurrentModel.theActionDetailModel?.approvedBy.isEmpty ?? false ? "-" :theCurrentModel.theActionDetailModel?.approvedBy
        
        if theCurrentModel.theActionDetailModel?.createdDate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.createdDate)!){
                dateFormatter.dateFormat = DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblCreatedDate.text = strFinalDate
            }
        }else{
            theCurrentView.lblCreatedDate.text = "-"
        }
        
        if theCurrentModel.theActionDetailModel?.duedate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.duedate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblDueDate.text = strFinalDate
            }
        }else{
            theCurrentView.lblDueDate.text = "-"
        }
        
        if theCurrentModel.theActionDetailModel?.workDate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.workDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblWorkDate.text = strFinalDate
            }
        }else{
            theCurrentView.lblWorkDate.text = "-"
        }
        
        if theCurrentModel.theActionDetailModel?.revisedDate != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterInputType.inputType1
            if let finalDate = dateFormatter.date(from: (theCurrentModel.theActionDetailModel?.revisedDate)!){
                dateFormatter.dateFormat =  DateFormatterOutputType.outputType7
                let strFinalDate = dateFormatter.string(from: finalDate)
                theCurrentView.lblRevisedDate.text = strFinalDate
            }
        }else{
            theCurrentView.lblRevisedDate.text = "-"
        }
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        
        let duration = Int((theCurrentModel.theActionDetailModel?.duration)!)
        theCurrentView.lblDuration.text = "\(formatter.string(from: TimeInterval(duration!))!)"
        theCurrentView.lblCompleted.text = "\(theCurrentModel.theActionDetailModel?.percentageCompeleted ?? "")%"
        
        self.theCurrentView.heightOfTag.constant = theCurrentView.tagView.frame.size.height
        if self.theCurrentView.heightOfTag.constant <= 0{
            self.theCurrentView.heightOfTag.constant = 50
            self.theCurrentView.tagView.isHidden = true
            self.theCurrentView.lblTagMsg.text = "-"
//            self.theCurrentView.lblTagMsg.textColor = red
        }else {
            self.theCurrentView.tagView.isHidden = false
            self.theCurrentView.lblTagMsg.isHidden = true
        }
        
        self.theCurrentView.tblSubTask.reloadData()
        self.theCurrentView.tblAttachment.reloadData()
        if theCurrentModel.arr_fileList.count == 0 {
//            theCurrentView.tblAttachment.backgroundView = theCurrentView.tblAttachment.backGroundMessageView(strMsg: "No records found", textColor:red)
//            theCurrentView.tblAttachment.reloadData()
            theCurrentView.heightOfTblAttachment.constant = 0.0
            theCurrentView.lblAttachmentDash.isHidden = false
        }
        if theCurrentModel.arr_subTaskList.count == 0 {
//            theCurrentView.tblSubTask.backgroundView = theCurrentView.tblSubTask.backGroundMessageView(strMsg: "No records found", textColor:red)
//            theCurrentView.tblSubTask.reloadData()
            theCurrentView.heightOfTblSubTask.constant = 0.0
            theCurrentView.lblSubtaskDash.isHidden = false
        }
    }
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_notesHistoryList == nil || theCurrentModel.arr_notesHistoryList?.count == 0 {
//            theCurrentView.tblNotesHistory.backgroundView = theCurrentView.tblNotesHistory.backGroundMessageView(strMsg: strMsg)
//            theCurrentView.tblNotesHistory.reloadData()
            theCurrentView.heightOfNotesHistory.constant = 0
        }
    }
    func loadViewMoreNoteHistory(isAnimating:Bool) {
        theCurrentView.activitySpinner.hidesWhenStopped = true

        if isAnimating {
            theCurrentView.activitySpinner.startAnimating()
            theCurrentView.viewOfPager.isHidden = false
            theCurrentView.viewMore.isHidden = true
        } else {
            theCurrentView.activitySpinner.stopAnimating()
            theCurrentView.viewMore.isHidden = false
            if theCurrentModel.nextOffset == -1 {
                theCurrentView.viewOfPager.isHidden = true
            }
        }
    }
    func attributedString(string:String,color: UIColor,font:UIFont) -> NSAttributedString?
    {
        let attributes1 = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor: color
            ] as [NSAttributedString.Key : Any]
        
        let attributedString = NSAttributedString(string: "\(string)", attributes: attributes1)
        
        return attributedString
    }
    //MARK:- Redirect To
    func presentDropDownListVC() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        let arr_list:[String] = theCurrentModel.arr_noteHistoryStatus.map({$0["name"].stringValue })
        vc.setTheData(strTitle: "Notes History", arr: arr_list)
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.theCurrentModel.arr_notesHistoryList?.removeAll()
            self?.theCurrentView.tblNotesHistory.reloadData()
            self?.theCurrentView.txtNotesHistory.text = strData
            self?.theCurrentModel.strNoteStatus = self?.theCurrentModel.arr_noteHistoryStatus[index]["status"].stringValue ?? ""
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentModel.strNoteStatus ?? "")
        }
        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnAddPostAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let strNoteMsg = theCurrentView.txtAddPost.text ?? ""
        if strNoteMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            self.showAlertAtBottom(message: "Please enter post name")
            return
        } else {
            sender.startLoadyIndicator()
            let parameter =     [APIKey.key_access_token:Utility.shared.userData.accessToken,
                                 
                                 APIKey.key_user_id: Utility.shared.userData.id,
                                 APIKey.key_note_for_id: theCurrentModel.actionId,
                                 APIKey.key_notes: strNoteMsg,
                                 APIKey.key_note_for: APIKey.key_action_logs
            ]
            
            addNotes(parameter: parameter, completion: {
                sender.stopLoadyIndicator()
            })
        }
    }
    @IBAction func onBtnCloseAction(_ sender: UIButton) {
        sender.startLoadyIndicator()
        closeHistorySaveWebService { [weak self] in
            sender.stopLoadyIndicator()
            self?.backAction(isAnimation: false)
        }
    }
    
    @IBAction func onBtnSelectNotesHistoy(_ sender:UIButton){
        presentDropDownListVC()
    }
    
    @IBAction func onBtnEditAction(_ sender: UIButton) {
        guard let theModel = theCurrentModel.theActionDetailModel else { return }
        if theCurrentModel.isComeFromAction == false{
            return
        }
        if self.theCurrentModel.theActionDetailModel == nil{
            return
        }
        let vc = AddActionVC.instantiateFromAppStoryboard(appStoryboard: .main)
        vc.setTheData(imgBG: self.view.asImage())
        vc.handlerEditAction = { [weak self] (theActionDetailModel) in
            self?.getActionDetailWebService()
        }
        vc.setUpdateScreen(editType: .edit, actionDetail: theModel)
        self.push(vc: vc)
    }
    
    @IBAction func onBtnViewMoreAction(_ sender: Any) {
        loadViewMoreNoteHistory(isAnimating: true)
        getActionNotesHistoryWebService(status: theCurrentModel.strNoteStatus)

    }
    
    
    
}

//MARK:- UITableViewDataSource
extension ActionDetailVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == theCurrentView.tblSubTask{
            return theCurrentModel.arr_subTaskList.count
        } else if tableView == theCurrentView.tblAttachment {
            if let _ = tableView.backgroundView , (theCurrentModel.arr_fileList.count == 0) {
                return 0
            }
            tableView.backgroundView = nil
            return theCurrentModel.arr_fileList.count
        }
        return theCurrentModel.arr_notesHistoryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == theCurrentView.tblSubTask
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTaskCell") as! SubTaskCell
            let data = theCurrentModel.arr_subTaskList[indexPath.row]
            cell.lblHistory.text = data.subTaskName
            return cell
        }
        else if tableView == theCurrentView.tblAttachment
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubTaskCell") as! SubTaskCell
            let data = theCurrentModel.arr_fileList[indexPath.row]
            cell.lblHistory.text = URL(string: data.fileName)?.lastPathComponent
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        if theCurrentModel.arr_notesHistoryList?.count != nil{
            let data = theCurrentModel.arr_notesHistoryList![indexPath.row]
            var strF = NSAttributedString(string: "")
            if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                strF = "\(createdDate) ".attributedString(color: black, font: R14)!
            }
            //                    attributedString(string: "\(data.createdDate) ", color: black, font: R14)!
            var strS = NSAttributedString(string: "")
            //            strS = data.actionUserName
            strS = data.actionUserName.attributedString(color: black, font: R14)!
            //                    attributedString(string: data.actionUserName, color: black, font: R14)!
            var strT = NSAttributedString(string: "")
            var strFt = NSAttributedString(string: "")
            var strSx = NSAttributedString(string: "")
            var strSth = NSAttributedString(string: "")
            
            switch data.historyStatus{
            case APIKey.key_Assigned:
                cell.imgPercentage.image = UIImage.init(named: "ic_contact")
                cell.vwOuter.backgroundColor = appColorFilterBlue
                //                    strT = "Assigned to"
                strT = " Assigned to ".attributedString(color:black, font: R14)!
                //                    strFt = data.assignedUserName
                
                strFt = " \(data.assignedUserName) ".attributedString(color: black, font: R14)!
                break
            case APIKey.key_Percentage:
                cell.imgPercentage.image = UIImage.init(named: "ic_percentage_icon")
                cell.vwOuter.backgroundColor = appColorFilterBlack
                //                    strT = "Completed \(data.percentageCompleted)%"
                strT = " Completed \(data.percentageCompleted)% ".attributedString(color: black, font: R14)!
                break
            case APIKey.key_Commented:
                cell.imgPercentage.image = UIImage.init(named: "ic_meassage")
                cell.vwOuter.backgroundColor = appColorFilterBlue
                //                    strF = data.actionUserName
                //                    let attachment = NSTextAttachment()
                //                    let img = UIImage(named: "ic_time_icon")
                //                    img?.size = CGSize(width: 12.0, height: 12.0)
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                //                    attachment.image?.size = CGSize(width: 12.0, height: 12.0)
                strFt = " \(data.actionUserName) ".attributedString(color: black, font: R14)!
                strS = NSAttributedString(string: "")
                strT = NSAttributedString(string: "")
                //                    strFt = NSAttributedString(attachment: attachment)
                //                    if let createdDate = data.createdDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                //                        strSx = "\(createdDate)".attributedString(color: black, font: R14)!
                //                    }
                strSx = ":".attributedString(color: black, font: R14)!
                strSth = " \(data.notesHistoryDescription) ".attributedString(color: black, font: R14)!
                //                    strSth = "Notes Text \(data.noteId)"
                break
            case APIKey.key_Revised:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                strT = " Revised Due Date To ".attributedString(color: black, font: R14)!
                //                    strT = "Revised Due Date To"
                //                    let attachment = NSTextAttachment()
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                
                strFt = NSAttributedString(string: " ")
                //                    strFt = NSAttributedString(attachment: attachment)
                if let revisedDate = data.revisedDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strSx = "\(revisedDate)".attributedString(color: black, font: R14)!
                }
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Worked:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                strT = " Revised Work Date To ".attributedString(color: black, font: R14)!
                //                    strT = "Revised Work Date To"
                //                    let attachment = NSTextAttachment()
                //                    attachment.image = UIImage(named: "ic_time_icon")
                //                    attachment.bounds.size = CGSize(width: 13.0, height: 13.0)
                strFt = NSAttributedString(string: " ")
                if let workedDate = data.workDate.convert(fromformate: DateFormatterInputType.inputType1, toFormate: DateFormatterOutputType.outputType7) {
                    strSx = "\(workedDate) ".attributedString(color: black, font: R14)!
                }
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Opened:
                cell.imgPercentage.image = UIImage.init(named: "ic_left_icon")
                cell.vwOuter.backgroundColor = appColorFilterGreen
                strT = " Opened an Action. ".attributedString(color: black, font: R14)!
                //                    strT = "Opened an Action."
                strFt = NSAttributedString(string: "")
                strSx = NSAttributedString(string: "")
                strSth = NSAttributedString(string: "")
                break
            case APIKey.key_Closed:
                cell.imgPercentage.image = UIImage.init(named: "ic_right_icon")
                cell.vwOuter.backgroundColor = appColorFilterRed
                strT = " Closed an Action. ".attributedString(color: black, font: R14)!
                //                    strT = "Closed an Action."
                strFt = NSAttributedString(string: "")
                strSx = NSAttributedString(string: "")
                strSth = NSAttributedString(string: "")
                break
            default:
                cell.imgPercentage.image = UIImage.init(named: "ic_set_timer_butten")
                cell.vwOuter.backgroundColor = appColorFilterYellow
                break
            }
            let FormatedString = NSMutableAttributedString()
            
            FormatedString.append(strF)
            FormatedString.append(strS)
            FormatedString.append(strT)
            FormatedString.append(strFt)
            FormatedString.append(strSx)
            FormatedString.append(strSth)
            cell.lblHistory.attributedText = FormatedString
        }
        return cell
    }
    
}
//MARK:-UITableViewDelegate
extension ActionDetailVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
}



//MARK:- TagView Delegate

extension ActionDetailVC:HTagViewDelegate, HTagViewDataSource
{
    func numberOfTags(_ tagView: HTagView) -> Int {
       return theCurrentModel.arr_tag.count
    }
    
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return theCurrentModel.arr_tag[index]
    }
    
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
//        return index > 0 ? .select : .cancel
        return .select
    }
    
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
        //        return 150
    }
   
    
    // MARK: - HTagViewDelegate
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        print("tag with indices \(selectedIndices) are selected")
    }
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        print("tag with index: '\(index)' has to be removed from tagView")
       
        tagView.reloadData()
    }
    
}

//MARK:- Api Call
extension ActionDetailVC {
    func getActionDetailWebService() {
        
        theCurrentView.showSkeletonAnimation()
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:theCurrentModel.actionId] as [String : Any]
        
        ActionsWebService.shared.getActionsDetail(parameter: parameter, success: { [weak self] (detail) in
            self?.theCurrentView.hideSkeletonAnimation()
            self?.theCurrentModel.theActionDetailModel = detail
            self?.theCurrentModel.arr_tagList = []
            self?.theCurrentModel.arr_tagList = Array(detail.tagsList)
            self?.theCurrentModel.arr_fileList = []
//            self?.theCurrentModel.arr_fileList = Array(detail.fileList)
            self?.theCurrentModel.arr_subTaskList = []
            self?.theCurrentModel.arr_subTaskList = Array(detail.subTaskList)
            self?.setupData()
            }, failed: { [weak self] (error) in
                self?.theCurrentView.hideSkeletonAnimation()
                self?.view.allButtons(isEnable: true)
                self?.showAlertAtBottom(message: error)
        })
    }
    func closeHistorySaveWebService(complettion:@escaping() -> Void) {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_history_id:theCurrentModel.actionId,
                         APIKey.key_history_for: APIKey.key_action_logs]
        ActionsWebService.shared.closeHistroySave(parameter: parameter, success: { (msg) in
            complettion()
        }, failed: { (error) in
            complettion()
        })
    }   
    
    func getActionNotesHistoryWebService(isRefreshing:Bool = false,status:String = "") {
       
        //guard let actionId = theCurrentModel.theActionsListModel?.actionlogId else { return }
        if isRefreshing {
            theCurrentModel.nextOffset = 0
        }
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_note_for_id:"\(theCurrentModel.actionId)",
                         APIKey.key_status : status,
                         APIKey.key_note_for: APIKey.key_action_logs,
                         APIKey.key_page_offset:"\(theCurrentModel.nextOffset)"]
        ActionsWebService.shared.getAllNotesHistroyList(parameter: parameter, success: { [weak self] (list, nextOffset) in
            self?.theCurrentModel.updateList(list: list, offset: nextOffset, isRefreshing: isRefreshing)
            self?.loadViewMoreNoteHistory(isAnimating: false)
            self?.theCurrentView.tblNotesHistory.reloadData()
            }, failed: { [weak self] (error) in
                self?.setBackground(strMsg: error)
        })
    }
    
    func addNotes(parameter:[String:Any], completion:@escaping() -> Void) {
        TacticalProjectWebService.shared.postAddTacticalProjectNotes(parameter: parameter, success: { [weak self] (data)in
            self?.getActionNotesHistoryWebService(isRefreshing: true, status: self?.theCurrentModel.strNoteStatus ?? "")
            self?.loadViewMoreNoteHistory(isAnimating: true)
            self?.theCurrentView.txtAddPost.text = ""
            completion()
            }, failed: { (error) in
                completion()
                self.showAlertAtBottom(message: error)
        })
    }
}
/*
//MARK:- Scrollview Delegate
extension ActionDetailVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if  self.theCurrentModel.nextOffset != -1 &&  self.theCurrentModel.nextOffset != 0
        {
            if theCurrentView.mainScrollview.contentOffset.y >= (theCurrentView.mainScrollview.contentSize.height - theCurrentView.mainScrollview.bounds.size.height)
            {
                theCurrentView.viewOfPager.isHidden = false
                theCurrentView.activitySpinner.startAnimating()
                getActionNotesHistoryWebService(status: theCurrentModel.strNoteStatus)
            }
        }else{
            theCurrentView.activitySpinner.stopAnimating()
            theCurrentView.viewOfPager.isHidden = true
        }
    }
}
*/




