//
//  ActionView.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ActionView: ViewParentWithoutXIB {
    
    //MARK:- Variabel
    @IBOutlet weak var btnBackStrategy: UIButton!
    
    @IBOutlet weak var btnGraph: UIButton!
    
    @IBOutlet weak var lblFilterCount: UILabel!
    @IBOutlet weak var constraintLblFilterCountWidth: NSLayoutConstraint! // 18.0
    @IBOutlet weak var viewFilter: UIView!   
    
    @IBOutlet weak var viewArchived: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
//    let refreshControl = UIRefreshControl()
    
    
    //MARK:- Life Cycle
    func setupLayout() {
        updateFilterCountText(strCount: "")
    }
    
    func setupTableView(theDelegate:ActionVC) {
//        refreshControl.tintColor = appGreen
//        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)

        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCellNib(ActionCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
    }
    
    @objc override func refresh() {
        refreshControl.beginRefreshing()
        (self.parentContainerViewController() as? ActionVC)?.getActionListWebService(isRefreshing: true)
    }
    
    func updateFilterCountText(strCount:String) {
        lblFilterCount.text = strCount
        constraintLblFilterCountWidth.constant = strCount.count > 1 ? CGFloat(strCount.count * 12) : CGFloat(strCount.count * 18)
//        self.layer.layoutIfNeeded()

//        self.layoutIfNeeded()
    }
}
