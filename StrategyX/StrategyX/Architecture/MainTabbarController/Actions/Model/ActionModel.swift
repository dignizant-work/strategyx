//
//  ActionModel.swift
//  StrategyX
//
//  Created by Jaydeep on 23/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class ActionModel {
    
    weak var theController:ActionVC!
    let identifier = "ActionCell"
    var arr_ActionsList:[ActionsList]?
    var nextOffset = 0
    let moreDD = DropDown()
    var apiLoadingAtActionID:[String] = []
    var assignToUserFlag = -1
    
    //    var subControllerStpes = RisksSubControllerType.step0
    //    var subViewControllers:[RisksSubListVC] = []
    enum dateSelectionType {
        case due, workDate
    }
    var filterData = FilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName)

    //MARK:- Variable
    init(theController:ActionVC) {
        self.theController = theController
    }
    
    func updateList(list:[ActionsList], offset:Int, isRefreshing:Bool) {
        if isRefreshing {
            arr_ActionsList?.removeAll()
        }
        nextOffset = offset
        if arr_ActionsList == nil {
            arr_ActionsList = []
        }
        list.forEach({ arr_ActionsList?.append($0) })
        print(arr_ActionsList!)
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView, tableContentView:UIView, at indexRow:Int,theModel:ActionsList) {
        //        let cellView = tableView.cellForRow(at: IndexPath.init(row: indexRow, section: 0))!
        //        print("cellView.contentView.frame:=",cellView.contentView.frame)
        
        dropDown.anchorView = view
        dropDown.cellNib = UINib(nibName: "MoreActionTVCell", bundle: nil)
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: -100, y: -(tableContentView.frame.size.height - view.frame.height))
        dropDown.bottomOffset = CGPoint(x: -100, y: tableContentView.frame.size.height - view.frame.height)
        dropDown.cellHeight = 44.0
        dropDown.width = 135.0
        var dataSource = [MoreAction.chart.rawValue, MoreAction.edit.rawValue, MoreAction.delete.rawValue]
        var dataSourceIcon = ["ic_report_profile_screen", "ic_edit_icon_popup", "ic_delete_icon_popup"]
        
        if Utility.shared.userData.id != theModel.createdBy {
            if let index = dataSource.firstIndex(where: {$0 == MoreAction.delete.rawValue}) {
                dataSource.remove(at: index) // delete
                dataSourceIcon.remove(at: index)
            }
        }
        /*if theModel.completeActionFlag != 1 {
            dataSource.removeLast() // archive
            dataSourceIcon.removeLast()
        }*/
        
        dropDown.dataSource = dataSource
        //        dropDown.width = 100
        dropDown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? MoreActionTVCell else { return }
            cell.configure(iconName: dataSourceIcon[index], title: item)
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.moreDD:
                self.theController.performDropDownAction(index: indexRow, item: item)
                break
                
            default:
                break
            }
            
        }
    }
    
}
