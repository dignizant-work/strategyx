//
//  MainTabBarController.swift
//  StrategyX
//
//  Created by Haresh on 12/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import AVKit

class MainTabBarController: UITabBarController {

    let btnAction = UIButton(type: .custom)
    var viewAction:UIView?
    var imgBG:UIImage?
    
    weak var mainHomeVC:MainHomeVC? = MainHomeVC.instantiateFromAppStoryboard(appStoryboard: .main)
    var isShowMainHome:Bool = false {
        didSet {
            if isShowMainHome {
                presentMainHomeVC()
                animateAction(isAnimate: true)
            } else {
                if oldValue != isShowMainHome {
                    mainHomeVC?.stopAnimation()
                    
                    self.animateAction(isAnimate: true)
//                    animateAction(isAnimate: true)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            Utility.shared.appCurrentVersion = appVersion
        }
        
        AppDelegate.shared.intercomLogin()
        self.delegate = self
        setTabbarItem()
        
        AppDelegate.shared.getPushCount(completion: {
            AppDelegate.shared.approvalNotificationCount = Int(Utility.shared.pushData.approvalCount) ?? 0
            AppDelegate.shared.updateMenuNotificationCount()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Tabbar viewWillAppear")
        checkPushNotificationData()
        if let _ = self.presentedViewController as? AVPlayerViewController {
            print("avController")
            Utility.shared.playerViewController = nil
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Tabbar viewDidAppear")
        let subview = self.view.subviews
        if let _ = subview.firstIndex(where: {$0.tag == 4843549}) {
            
        } else {
//            self.view.window?.addSubview(AppDelegate.shared.setupIntercomView())
            self.view.addSubview(AppDelegate.shared.setupIntercomView())
        }

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("self.tabBar.bounds.height:=",self.tabBar.frame.size.height)
//        print("self.tabBar.center.y:=",self.tabBar.center.y)
        btnAction.frame = CGRect(x: self.tabBar.center.x - 20, y: (self.view.bounds.height - tabBar.frame.height) + 5 , width: 40, height: 40)
    }
    
    //setup
    func setTabbarItem() {
        let actionNavigation = NavigationActionVC.instantiateFromAppStoryboard(appStoryboard: .actions)
        let tabbarOneItem = UITabBarItem(title:"Actions", image: UIImage(named: "ic_actons_menu_screen"), selectedImage: UIImage(named: "ic_actons_menu_screen"))
//        tabbarOneItem.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        actionNavigation.tabBarItem = tabbarOneItem
        
        let ideaNProblemsNaviagtion = NavigationIdeasNProblemsVC.instantiateFromAppStoryboard(appStoryboard: .ideasNProblems)
        let tabbarTwoItem = UITabBarItem(title: "Ideas & Problems", image: UIImage(named: "ic_ideas_unselect_icon"), selectedImage: UIImage(named: "ic_ideas_select_icon"))
        //        tabbarTwoItem.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        ideaNProblemsNaviagtion.tabBarItem = tabbarTwoItem
        
//        let resourcesNaviagtion = NavigationResourcesVC.instantiateFromAppStoryboard(appStoryboard: .resources)
//        let tabbarTwoItem = UITabBarItem(title: "Resources", image: UIImage(named: "ic_recsources_profile_screen"), selectedImage: UIImage(named: "ic_recsources_profile_screen"))
////        tabbarTwoItem.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
//        resourcesNaviagtion.tabBarItem = tabbarTwoItem
    
        let emptyCenterNavigation = UINavigationController(rootViewController: UIViewController())
        let tabbarCenterItem = UITabBarItem(title: "", image: nil, selectedImage: nil) 
        //        tabbarTwoItem.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        emptyCenterNavigation.tabBarItem = tabbarCenterItem
        
//        btnAction.setTitle("+", for: .normal)
//        btnAction.titleLabel?.font = UIFont(name: FontStyle().bold, size: 30.0)
//        btnAction.setTitleColor(white, for: .normal)
        
        btnAction.setImage(UIImage(named: "ic_plush_profile_screen"), for: .normal)
        btnAction.frame = CGRect(x: 100, y: 0, width: 60, height: 60)
        btnAction.backgroundColor = appGreen
        btnAction.layer.cornerRadius = 20.0
        btnAction.addTarget(self, action: #selector(onBtnAction(_:)), for: .touchUpInside)
        
//        btnAction.layer.borderWidth = 4.0
//        btnAction.layer.borderColor = white.cgColor
        self.view.insertSubview(btnAction, aboveSubview: self.tabBar)
        
        let approvalsNaviagtion = NavigationApprovalVC.instantiateFromAppStoryboard(appStoryboard: .approval)
        let tabbarThreeItem = UITabBarItem(title: "Approvals", image: UIImage(named: "ic_unselect_approvals_menu_screen"), selectedImage: UIImage(named: "ic_select_approvals_menu_screen"))
        //        tabbarTwoItem.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        approvalsNaviagtion.tabBarItem = tabbarThreeItem
        
//        let reportsNavigation = NavigationReportsVC.instantiateFromAppStoryboard(appStoryboard: .reports)
//        let tabbarThreeItem = UITabBarItem(title:"Reports", image: UIImage(named: "ic_report_profile_screen"), selectedImage: UIImage(named: "ic_select_report_profile_screen")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate))
////        tabbarThreeItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -5)
//        reportsNavigation.tabBarItem = tabbarThreeItem
        
        let menuNavigation = NavigationMenuVC.instantiateFromAppStoryboard(appStoryboard: .menu)
        let tabbarFourItem = UITabBarItem(title: "Menu", image: UIImage(named: "ic_menu_profile_screen"), selectedImage: UIImage(named: "ic_menu_profile_screen"))
//        tabbarFourItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -5)
        menuNavigation.tabBarItem = tabbarFourItem
        
        
        self.viewControllers = [actionNavigation,ideaNProblemsNaviagtion , emptyCenterNavigation ,approvalsNaviagtion,menuNavigation]
        self.tabBar.isTranslucent = false
        self.tabBar.items![2].isEnabled = false
        self.tabBar.tintColor = appGreen
        self.tabBar.unselectedItemTintColor = appBgLightGrayColor
        self.tabBar.items![3].badgeColor = UIColor.gray
        self.tabBar.items![4].badgeColor = red
    }
    
    func checkPushNotificationData() {
        if let theModel = Utility.shared.pushNotificationData {
            let relatedTo = theModel.relatedTo
            let relatedID = theModel.relatedId
            
            let Model = Notification()
            Model.relatedId = "\(relatedID)"
            Model.relatedTo = relatedTo
            
            Utility.shared.pushNotificationData = nil
            
            if let vc = ((self.viewControllers![0] as? NavigationActionVC)?.viewControllers[0] as? ActionVC) {
                vc.redirectToModule(theModel: Model)
            }
        }
    }
    
    //MARK:- MainHomeAction
    @IBAction func onBtnAction(_ sender:UIButton) {
        isShowMainHome = !isShowMainHome
    }
    
    //MARK:- Main Home
    func presentMainHomeVC() {
        if viewAction != nil {
            viewAction?.removeFromSuperview()
            mainHomeVC = nil
            viewAction = nil
        }
        
        if mainHomeVC != nil {
            mainHomeVC = nil
        }
        mainHomeVC = MainHomeVC.instantiateFromAppStoryboard(appStoryboard: .main)
        mainHomeVC?.modalTransitionStyle = .crossDissolve
        mainHomeVC?.modalPresentationStyle = .overCurrentContext
        
        mainHomeVC?.setTheData(tabBarHeight: 49.0)
        mainHomeVC?.handlerCloseMenu = { [weak self] (isAnimationStart) in
            if isAnimationStart {
                self?.onBtnAction(UIButton())
            } else {
                self?.mainHomeVC?.willMove(toParent: nil)
                self?.mainHomeVC?.view.removeFromSuperview()
                self?.mainHomeVC?.removeFromParent()
                self?.viewAction?.removeFromSuperview()
            }
        }
        
        mainHomeVC?.handlerRedirecToMenu = { [weak self] (strTitle) in
            self?.tabBar.tintColor = appBgLightGrayColor
            self?.redirectAccordingToSelectionHomeMenu(strTitle: strTitle)
        }
        
        guard let vc = self.viewControllers,let currentNVC = vc[self.selectedIndex] as? UINavigationController else {
            return
        }
        var baseVC = currentNVC.viewControllers[currentNVC.viewControllers.count - 1]
        if let index = currentNVC.viewControllers.firstIndex(where: {
            switch $0 {
            case is MyRoleCalendarVC:
                return true
            case is AddPrimaryAreaVC:
                return true
            case is AddSecondaryAreaVC:
                return true
            case is AddGoalVC:
                return true
            case is AddRiskVC:
                return true
            case is AddFocusVC:
                return true
            case is AddTacticalProjectVC:
                return true
            case is AddActionVC:
                return true
            case is AddCompanyVC:
                return true
//            case is AddUserVC:
//                return true
//            case is AddTagsVC:
//                return true
            case is AddNoteVC:
                return true
            case is AddCriticalSucessFactorVC:
                return true
            case is AddIdeaNProblemVC:
                return true
            default:
                return false
            }
            //            if let _ = $0 as? (MyRoleCalendarVC, AddPrimaryAreaVC,AddSecondaryAreaVC,AddGoalVC,AddRiskVC) { return true }
            //            return false
        }) {
            let vc = currentNVC.viewControllers[index - 1]
            currentNVC.popToViewController(vc, animated: false)
            baseVC = vc
        }
        
        imgBG = baseVC.view.asImage()
        
        let size = currentNVC.viewControllers[0].view.frame.size
        print("size:=",size)
        viewAction = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height + 49.0))
        viewAction?.tag = 501
        viewAction?.backgroundColor = clear
        viewAction?.addSubview(mainHomeVC!.view)
        mainHomeVC?.view.frame =  CGRect(x: 0, y: 0, width: size.width, height: size.height + 49.0)
        currentNVC.viewControllers[currentNVC.viewControllers.count - 1].view.addSubview(viewAction!)
        currentNVC.viewControllers[currentNVC.viewControllers.count - 1].addChild(mainHomeVC!)
        mainHomeVC!.didMove(toParent: self)

    }
    
    func animateAction(isAnimate:Bool = false) {
        
        let identity =  self.btnAction.transform.isIdentity ? CGAffineTransform(rotationAngle: CGFloat.pi) : CGAffineTransform.identity
        let bgColor = self.btnAction.transform.isIdentity ? 13 : 3
        
        UIView.animate(withDuration: 0.3, animations: {
            self.btnAction.setBackGroundColor = bgColor
            self.btnAction.transform = identity
        }, completion: { (finished) in
            self.btnAction.setImage(UIImage(named: !self.btnAction.transform.isIdentity ? "ic_cross_approval_ss" : "ic_plush_profile_screen"), for: .normal)
        })
    }
    
    
    
    
    deinit {
        print("\(self) DEALLOCATED")
    }
    
}
//MARK:- UITabBarControllerDelegate
extension MainTabBarController:UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let tabItem = tabBar.items?[0], tabItem == item  {
            print("Action Tab")
            if let vc = ((self.viewControllers![0] as? NavigationActionVC)?.viewControllers[0] as? ActionVC), vc.isViewLoaded {
                print("isViewLoaded:=",vc.isViewLoaded)
                vc.reInitData()
            }
        }
        
        if let tabItem = tabBar.items?[1], tabItem == item  {
            print("Approval Tab")
            if let vc = ((self.viewControllers![1] as? NavigationIdeasNProblemsVC)?.viewControllers[0] as? IdeasNproblemsVC), vc.isViewLoaded {
                print("isViewLoaded:=",isViewLoaded)
                vc.reInitData()
            }
        }
        
        if let tabItem = tabBar.items?[3], tabItem == item  {
            print("Approval Tab")
            if let vc = ((self.viewControllers![3] as? NavigationApprovalVC)?.viewControllers[0] as? ApprovalsVC),vc.isViewLoaded {
                print("isViewLoaded:=",isViewLoaded)
                vc.reInitData()
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        print("vc:=",viewController)
        isShowMainHome = false
        
        if let nvc = viewController as? UINavigationController {
            nvc.popToRootViewController(animated: false)
        }
        
        if let navigationMenuVC = viewController as? NavigationMenuVC {
            navigationMenuVC.slideMenuController()?.closeLeft()
            navigationMenuVC.slideMenuController()?.openRight()
            return false
        }
        self.tabBar.tintColor = appGreen
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
        print("didSelect:=", tabBarController.selectedIndex)
        
    }
}
//MARK:- LeftSideMenuDelegate
extension MainTabBarController:LeftSideMenuDelegate {
    func redirectToLeftSideMenuViewController(strTitle: String) {
        if "Logout".lowercased() != strTitle.lowercased() {
            self.tabBar.tintColor = appBgLightGrayColor
        }
        redirectToAccordingToSelectionOfLeftSideMenu(strTitle: strTitle)
    }
}

//MARK:- RightSideMenuDelegate
extension MainTabBarController:RightSideMenuDelegate {
    func redirectToRightSideMenuViewController(strTitle: String) {
        self.tabBar.tintColor = appBgLightGrayColor
        redirectToAccordingToSelectionOfRightSideMenu(strTitle: strTitle)
    }
}

//MARK:- SlideMenuControllerDelegate
extension MainTabBarController:SlideMenuControllerDelegate {
    func leftWillOpen() {
        isShowMainHome = false
//
    }
    func leftWillClose() {
        
    }

    func rightWillOpen() {
        isShowMainHome = false
        
    }
    func rightWillClose() {
        
    }
    func rightDidClose() {
        
    }
    
}

extension BinaryInteger {
    var degreesToRadians: CGFloat { return CGFloat(Int(self)) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension CGFloat {
    func degrees() -> CGFloat {
        return self * .pi / 180
    }
    
    init(degrees: CGFloat) {
        self = degrees.degrees()
    }
}

