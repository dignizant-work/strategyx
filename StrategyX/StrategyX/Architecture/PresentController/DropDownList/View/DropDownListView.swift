//
//  DropDownListView.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DropDownListView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint! // 100
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfBtnSubmit: NSLayoutConstraint!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    @IBOutlet weak var heightOfSearchView: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblPhoneCode: UITableView!
    @IBOutlet weak var viewPhoneCode: UIView!
    @IBOutlet weak var lblPhoneCodeTitle: UILabel!
    
    
    //MARK:- LifeCycle
    func setupLayout(strTitle:String) {
        lblTitle.text = strTitle
        lblPhoneCodeTitle.text = strTitle
    }
    
    func setupTableView(theDelegate:DropDownListVC) {
        tableView.estimatedRowHeight = 50.0
        tblPhoneCode.estimatedRowHeight = 50.0
        
        tableView.rowHeight = UITableView.automaticDimension
        tblPhoneCode.rowHeight = UITableView.automaticDimension
        
        tableView.registerCellNib(DropDownListTVCell.self)
        tblPhoneCode.registerCellNib(DropDownListTVCell.self)
        
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
        tableView.separatorStyle = .none
        
        tblPhoneCode.delegate = theDelegate
        tblPhoneCode.dataSource = theDelegate
        tblPhoneCode.separatorStyle = .none
    }
    
    func setupTextfield(theDelegate:DropDownListVC) {
         txtSearch.delegate = theDelegate
    }
    
    func setTableViewHeight(height:CGFloat) {
        if height > self.frame.size.height - 200 {
            constrainTableViewHeight.constant = self.frame.size.height - 200
//            tableView.isScrollEnabled = true
        } else {
            constrainTableViewHeight.constant = height
//            tableView.isScrollEnabled = false
           
        }
        self.layoutIfNeeded()
    }
    
    
    //RX
    
}
