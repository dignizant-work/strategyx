//
//  DropDownListModel.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import SwiftyJSON

class DropDownListModel {

    //MARK:- Variabel
    weak var theController:DropDownListVC!
    
    var strTitle = ""
//    var arr_List:[String] = ["Select 1","Select 2","Select 3","Select 4","Select 5"]
    var arr_List:[String] = []
    var arr_tagList:[TagList] = []
    var arr_CancelTagList:[TagList] = []
    var arr_MainList:[String] = []
    var isComeFromPhoneCode = Bool()
    
    //MARK:- LifeCycle
    init(theController:DropDownListVC) {
        self.theController = theController
    }
    
}
