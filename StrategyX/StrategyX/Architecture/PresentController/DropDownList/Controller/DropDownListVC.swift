//
//  DropDownListVC.swift
//  StrategyX
//
//  Created by Haresh on 29/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class DropDownListVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:DropDownListView = { [unowned self] in
       return self.view as! DropDownListView
    }()
    
    fileprivate lazy var theCurrentModel:DropDownListModel = {
       return DropDownListModel(theController: self)
    }()
    
    //MARK:- Outlet Zone
    
   
    
    //MARK:- Variable Declaration
    var hanlderSelectedData:(String, Int) -> Void = {_,_  in}
    var isSelectTag = Bool()
    var handlerTag:([TagList]) -> Void = {_ in}
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if theCurrentModel.isComeFromPhoneCode == true{
            theCurrentView.tblPhoneCode.reloadData()
        }else{
            theCurrentView.tableView.reloadData()
        }
        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
//        enableIQKeyboardManager()
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if theCurrentModel.isComeFromPhoneCode == false{
            if let obj = object as? UITableView, keyPath == "contentSize" {
                theCurrentView.setTableViewHeight(height: obj.contentSize.height)
            }
        }
       
    }
    
    func setBackground(strMsg:String) {
        if theCurrentModel.arr_List.count == 0 {
            theCurrentView.tblPhoneCode.backgroundView = theCurrentView.tblPhoneCode.backGroundMessageView(strMsg: strMsg)
            theCurrentView.tblPhoneCode.reloadData()
        }
    }
    
    func setupUI() {
//        disableIQKeyboardManager()
        theCurrentView.setupLayout(strTitle:theCurrentModel.strTitle)
        theCurrentView.setupTableView(theDelegate: self)
        if theCurrentModel.isComeFromPhoneCode == true{
             theCurrentView.tblPhoneCode.reloadData()
        }else{
            theCurrentView.tableView.reloadData()
        }
        theCurrentView.setupTextfield(theDelegate: self)
        theCurrentView.txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func setTheData(strTitle:String,arr:[String],isSearchHidden:Bool=true) {
        print("main arr \(arr)")
        theCurrentModel.arr_List = arr
        theCurrentModel.strTitle = strTitle
        theCurrentModel.arr_MainList = arr
        [theCurrentView.btnCancelOutlet,theCurrentView.btnSubmitOutlet].forEach { (btn) in
            btn?.isHidden = true
        }
        if isSearchHidden == false{
            theCurrentView.viewPhoneCode.isHidden = isSearchHidden
            theCurrentModel.isComeFromPhoneCode = true
        }
    }
    
    func setTheTagData(strTitle:String,arr:[TagList],isSearchHidden:Bool=true) {
        theCurrentModel.arr_tagList = arr
        theCurrentModel.strTitle = strTitle
        theCurrentView.heightOfBtnSubmit.constant = 40
        theCurrentModel.arr_CancelTagList = arr
        [theCurrentView.btnCancelOutlet,theCurrentView.btnSubmitOutlet].forEach { (btn) in
            btn?.isHidden = false
        }
        if isSearchHidden == false{
            theCurrentView.viewPhoneCode.isHidden = isSearchHidden
            theCurrentModel.isComeFromPhoneCode = true
        }
    }
    
   
    
    //MARK:-IBAction
    @IBAction func onBtnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK:- UITableViewDataSource
extension DropDownListVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSelectTag == true{
            return theCurrentModel.arr_tagList.count
        }
        if let _ = tableView.backgroundView , theCurrentModel.arr_List.count == 0 {
            return 0
        }
        tableView.backgroundView = nil
        return theCurrentModel.arr_List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownListTVCell") as! DropDownListTVCell
        cell.selectionStyle = .none
        if isSelectTag == true{
            cell.widthOfCheckBox.constant = 20
            let data = theCurrentModel.arr_tagList[indexPath.row]
            cell.configure(strTitle: data.tag_name)
            if data.is_selected == "0"{
                cell.btnCheckOutlet.isSelected = false
            }
            else{
               cell.btnCheckOutlet.isSelected = true
            }
        }else{
            cell.configure(strTitle: theCurrentModel.arr_List[indexPath.row])
        }
        
        return cell
    }
}
//MARK:-UITableViewDelegate
extension DropDownListVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSelectTag == true{
            let data = theCurrentModel.arr_tagList[indexPath.row]
            if data.is_selected == "0"
            {
                if(data.is_old_selected == "1")
                {
                    data.is_del = "0"
                }
                else{
                    data.is_new_added = "1"
                }
                
                data.is_selected = "1"
            }
            else{
                
                if(data.is_old_selected == "1")
                {
                    data.is_del = "1"
                }
                else{
                    data.is_new_added = "0"
                }
                data.is_selected = "0"
            }
            
            theCurrentModel.arr_tagList[indexPath.row] = data
            theCurrentView.tableView.reloadData()
        }else{
            let data = theCurrentModel.arr_List[indexPath.row]
            let index = indexPath.row
            self.hanlderSelectedData(data, index)

            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

//MARK:- Action Zone

extension DropDownListVC{
    @IBAction func  btnCancelAction(sender:UIButton){
        self.dismiss(animated: true, completion: {
            self.handlerTag(self.theCurrentModel.arr_CancelTagList)
        })
    }
    
    @IBAction func  btnSubmitAction(sender:UIButton){
        self.dismiss(animated: true, completion: {
            self.handlerTag(self.theCurrentModel.arr_tagList)
        })
    }
}

//MARK:- UITextfield Delegate

extension DropDownListVC:UITextFieldDelegate{
    
    @objc func textFieldDidChange(_ textField: UITextField) {
       theCurrentModel.arr_List = theCurrentModel.arr_MainList.filter { $0.lowercased().contains(textField.text?.lowercased() ?? "")
        
        }
        if theCurrentModel.arr_List.count == 0{
            setBackground(strMsg: "No record found")
        }
        theCurrentView.tblPhoneCode.reloadData()
    }
    
}

