//
//  ActionFilterView.swift
//  StrategyX
//
//  Created by Jaydeep on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import WSTagsField

class ActionNewFilterView: ViewParentWithoutXIB {
    
    //MARK:- IBOutlet
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    //    @IBOutlet weak var btnDepartmentArrow: UIButton!
    @IBOutlet weak var viewDepartment: UIView!
    
    @IBOutlet weak var txtCategory: UITextField!
    //    @IBOutlet weak var btnCategoryArrow: UIButton!
    @IBOutlet weak var viewCategory: UIView!
    
    @IBOutlet weak var txtAssignedTo: UITextField!
    //    @IBOutlet weak var btnAssignedToArrow: UIButton!
    @IBOutlet weak var viewAssignedTo: UIView!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!

    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
    @IBOutlet weak var txtWorkDate:UITextField!
    
    @IBOutlet weak var txtDueDate:UITextField!
    
    @IBOutlet weak var vwTags: UIView!
    @IBOutlet weak var btnTagLoaderOutlet: UIButton!

    @IBOutlet weak var constrainvwTagsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var viewCompnay: UIView!
    
    @IBOutlet weak var btnCompanyLoaderOutlet: UIButton!
    @IBOutlet weak var btnAssignLoaderOutlet: UIButton!
    @IBOutlet weak var btnDepartmentLoaderOutlet: UIButton!
    @IBOutlet weak var btnCategoryLoaderOutlet: UIButton!
    
    @IBOutlet weak var collectionDueDate: UICollectionView!
    @IBOutlet weak var collectionWorkDate: UICollectionView!
    
    @IBOutlet weak var viewDueDate: UIView!
    @IBOutlet weak var viewWorkDate: UIView!
    @IBOutlet weak var viewRelatedTo: UIView!
    
    @IBOutlet weak var txtRelatedTo: UITextField!
    @IBOutlet weak var btnRelatedToLoderOutlet: UIButton!
    
    //MARK:- LifeCycle
    func setupLayout() {
        btnReset(isHidden: false)
        
    }
    
    func setupDelegate(theDelegate:ActionNewFilterVC) {
        collectionWorkDate.registerCellNib(DateSlotCVCell.self)
        collectionWorkDate.delegate = theDelegate
        collectionWorkDate.dataSource = theDelegate
        
        
        collectionDueDate.registerCellNib(DateSlotCVCell.self)
        collectionDueDate.delegate = theDelegate
        collectionDueDate.dataSource = theDelegate
    }
    
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        btnSubmit.isHidden = isHidden

        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
            self?.btnSubmit.isHidden = isHidden
        })
    }
    func updateViewAccordingTo(type:FilterType) {
        viewSearch.isHidden = false
        viewDueDate.isHidden = false
        viewWorkDate.isHidden = false
        viewDepartment.isHidden = false
        viewCategory.isHidden = false
        viewAssignedTo.isHidden = false
        viewRelatedTo.isHidden = true
        
        if type == FilterType.focus {
            viewSearch.isHidden = true
            viewCategory.isHidden = true
        } else if type == FilterType.myRole {
            viewDepartment.isHidden = true
            viewCategory.isHidden = true
            viewAssignedTo.isHidden = true
        } else if type == FilterType.action {
            viewCategory.isHidden = true
            viewRelatedTo.isHidden = false
        } else if type == FilterType.approvals {
            viewCategory.isHidden = true
            viewSearch.isHidden = true
            viewRelatedTo.isHidden = false
            viewDueDate.isHidden = true
            viewWorkDate.isHidden = true
        }
    }
    func addTagSubView(tagsField : WSTagsField)
    {
        tagsField.frame = vwTags.bounds
        vwTags.addSubview(tagsField)
    }
    
}
