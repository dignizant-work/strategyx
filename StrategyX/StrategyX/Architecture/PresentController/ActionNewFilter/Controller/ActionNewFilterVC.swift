//
//  ActionFilterVC.swift
//  StrategyX
//
//  Created by Jaydeep on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import WSTagsField

class ActionNewFilterVC: ParentViewController {
    
    enum dateSelectionType:Int {
        case created             = 1
        case due                 = 2
        case work                = 3
    }
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ActionNewFilterView = { [unowned self] in
        return self.view as! ActionNewFilterView
        }()
    
    fileprivate lazy var theCurrentModel:ActionNewFilterModel = {
        return ActionNewFilterModel(theController: self)
    }()
    
    // handler
    var handlerOnFilterSearch:(_ filterData:FilterData) -> Void = {_ in }
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupDelegate(theDelegate: self)
        theCurrentView.addTagSubView(tagsField: theCurrentModel.tagsField)

        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.departmentDD, view: theCurrentView.viewDepartment)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.categoryDD, view: theCurrentView.viewCategory)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.assignedToDD, view: theCurrentView.viewAssignedTo)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.tagsDD, view: theCurrentView.vwTags)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.companyDD, view:theCurrentView.viewCompnay)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.relatedToDD, view:theCurrentView.viewRelatedTo)
        
//        self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.categoryDD)!, view: (self?.theCurrentView.viewCategory)!)

        theCurrentModel.configureTagsView()

        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
        
        
        if let workDate = theCurrentModel.filterData.workDate {
            theCurrentModel.selectedWorkDate = workDate
            if let index = theCurrentModel.arr_dateSlot.firstIndex(where: {$0 == workDate }) {
                theCurrentView.collectionWorkDate.selectItem(at: IndexPath.init(row: index, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
            }
//            theCurrentView.txtWorkDate.text = workDate.string(withFormat: DateFormatterOutputType.outPutType4)
        }
        if let dueDate = theCurrentModel.filterData.dueDate {
            theCurrentModel.selectedDueDate = dueDate
            if let index = theCurrentModel.arr_dateSlot.firstIndex(where: {$0 == dueDate }) {
                theCurrentView.collectionDueDate.selectItem(at: IndexPath.init(row: index, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
            }
//            theCurrentView.txtDueDate.text = dueDate.string(withFormat: DateFormatterOutputType.outPutType4)
        }
        /*
        if FilterType.action != theCurrentModel.filterType {
            getCategories()
        }*/
//        getTagsList()
        setCompanyData()
        
    }
    func setCompanyData() {
        if theCurrentModel.filterType == .action || theCurrentModel.filterType == .approvals {
            relatedListService()
        }
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
            theCurrentView.txtCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompnay.isHidden = true
//            theCurrentView.btnCompanyLoaderOutlet.isUserInteractionEnabled = false
//            theCurrentView.viewCompnay.alpha = 0.5
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
            
        } else {
            if !(Utility.shared.userData.companyId.isEmpty) {
                theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
                theCurrentView.txtCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompnay.isHidden = true
//                theCurrentView.btnCompanyLoaderOutlet.isUserInteractionEnabled = false
//                theCurrentView.viewCompnay.alpha = 0.5
                showLoader(theCurrentView.btnAssignLoaderOutlet)
                assignUserListService()
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()
                return
            }
            getCompanyList()
            assignUserListService()
        }
        
    }
    
    func setTopConstraint(top:CGFloat,filterType:FilterType) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
    }
    
    func setTheData(filterData:FilterData) {
        theCurrentModel.filterData = filterData
        theCurrentView.txtSearch.text = filterData.searchText
        theCurrentModel.strSelectedCategoryID = filterData.categoryID
        theCurrentModel.strSelectedCompanyID = filterData.companyID
        theCurrentModel.strSelectedUserAssignID = filterData.assigntoID
        theCurrentModel.strSelectedDepartmentID = filterData.departmentID
        theCurrentModel.strSelectedRelatedId = filterData.relatedToValue
    }
    
    func update(text:String,dropDown:DropDown)
    {
        switch dropDown {
        
        case theCurrentModel.categoryDD:
            theCurrentView.txtCategory.text = text
            break
        case theCurrentModel.relatedToDD:
            theCurrentView.txtRelatedTo.text = text
            break
        case theCurrentModel.companyDD:
            theCurrentView.txtCompany.text = text
            theCurrentView.txtAssignedTo.text = ""
            theCurrentView.txtDepartment.text = ""
            theCurrentModel.strSelectedUserAssignID = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.arr_assignList = []
            theCurrentModel.arr_departmentList = []
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
            break
        case theCurrentModel.assignedToDD:
            theCurrentView.txtAssignedTo.text = text
//            theCurrentView.txtDepartment.text = ""
//            theCurrentModel.strSelectedDepartmentID = ""
//            theCurrentModel.arr_departmentList = []
//            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
//            departmentListService()
            break
        case theCurrentModel.departmentDD:
            theCurrentView.txtDepartment.text = text
            break
        case theCurrentModel.tagsDD:
            theCurrentModel.tagsField.addTag(text)
            theCurrentModel.tagsField.text = ""
            break
        
        default:
            break
        }
        theCurrentView.btnReset(isHidden: false)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnCompnayAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.companyDD.show()
    }
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.theCurrentModel.strSelectedCompanyID != "" {
            if self.theCurrentModel.arr_departmentList.count != 0 {
                theCurrentModel.departmentDD.show()
            } else {
                self.departmentListService()
            }
        } else {
            self.showAlertAtBottom(message: "Please select company first")
        }
    }
    
    @IBAction func onBtnRelatedToAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.theCurrentModel.arr_relatedList.count > 1 {
            theCurrentModel.relatedToDD.show()
        } else {
            self.relatedListService()
        }
        
    }
    
    
    @IBAction func onBtnCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.categoryDD.show()
    }
    
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue{
            if self.theCurrentModel.strSelectedCompanyID != ""{
                if self.theCurrentModel.arr_assignList.count != 0{
                    theCurrentModel.assignedToDD.show()
                } else {
                    self.assignUserListService()
                }
            } else {
                self.showAlertAtBottom(message: "Please select company")
            }
        } else {
            if self.theCurrentModel.arr_assignList.count != 0{
                theCurrentModel.assignedToDD.show()
            } else {
                self.assignUserListService()
            }
        }
    }
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: ActionNewFilterVC.dateSelectionType.due)
    }
    
    @IBAction func onBtnWorkDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: ActionNewFilterVC.dateSelectionType.work)
    }
    @IBAction func onBtnResetAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.superAdminUser == UserDefault.shared.userRole {
            if Utility.shared.userData.companyId.isEmpty {
                theCurrentModel.strSelectedCompanyID = ""
                theCurrentView.txtCompany.text = ""
            }
        }
        theCurrentModel.filterData = FilterData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
        handlerOnFilterSearch(theCurrentModel.filterData)

        /*
        if UserRole.superAdminUser != UserDefault.shared.userRole {
         theCurrentModel.filterData = FilterData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
         handlerOnFilterSearch(theCurrentModel.filterData)*/

 
        /*if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.filterData = FilterData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
            handlerOnFilterSearch(theCurrentModel.filterData)
        } else {
            handlerOnFilterSearch(FilterData.init(assigntoID: "0", assigntoName: ""))
        }*/
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSubmitFilterAction(_ sender: Any) {
        self.view.endEditing(true)
        let tags = theCurrentModel.tagsField.tags.map({$0.text})
        var searchTag:[String] = []
        
        for tag in tags {
            if let index = theCurrentModel.arr_Tags_Main.firstIndex(where: {$0.tag_name == tag}) {
                searchTag.append(theCurrentModel.arr_Tags_Main[index].tag_id)
            }
        }
        
        let filterDatanew = theCurrentModel.filterData
        theCurrentModel.filterData = FilterData.init(searchText: theCurrentView.txtSearch.text ?? "",searchTag:searchTag.joined(separator: ","),categoryID: theCurrentModel.strSelectedCategoryID, categoryName: theCurrentView.txtCategory.text ?? "" , companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "", assigntoID: theCurrentModel.strSelectedUserAssignID, assigntoName: theCurrentView.txtAssignedTo.text ?? "", departmentID: theCurrentModel.strSelectedDepartmentID, departmentName: theCurrentView.txtDepartment.text ?? "",relatedToValue: theCurrentModel.strSelectedRelatedId, relatedToName:theCurrentView.txtRelatedTo.text ?? "" ,workDate:filterDatanew.workDate,dueDate:filterDatanew.dueDate)
        handlerOnFilterSearch(theCurrentModel.filterData)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Tags method
    func textFieldEvents(tagsField : WSTagsField)
    {
        tagsField.onDidAddTag = { _, _ in

            print("onDidAddTag")
         }
        
        tagsField.onDidRemoveTag = { _, _ in
            print("onDidRemoveTag")
        }
        
        tagsField.onDidChangeText = { _, text in
            print("onDidChangeText")
            
            if(self.theCurrentModel.FilterTagsArray(text: text ?? "").count > 0)
            {
                self.theCurrentModel.isDropDownShowing = true
                self.theCurrentModel.tagsField.returnKeyType = .next
                self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = true
                self.theCurrentModel.tagsDD.show()
            }
            else{
//                self.theCurrentModel.tagsField.text = ""
                self.theCurrentModel.isDropDownShowing = false
                self.theCurrentModel.tagsField.returnKeyType = .default
                self.theCurrentModel.tagsDD.hide()
                self.theCurrentModel.tagsField.enablesReturnKeyAutomatically = false
            }
        }

        tagsField.onDidChangeHeightTo = { _, height in
            print("HeightTo \(height)")
            self.theCurrentView.constrainvwTagsHeight.constant = height
        }
        tagsField.onDidSelectTagView = { _, tagView in
            print("Select \(tagView)")
        }
        tagsField.onDidUnselectTagView = { _, tagView in
            print("Unselect \(tagView)")
        }
    }
}

extension ActionNewFilterVC : UITextFieldDelegate
{
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(!self.theCurrentModel.isDropDownShowing)
        {
            print("textField did end editing")
            if(textField == self.theCurrentModel.tagsField)
            {
                print("tagsField did end editing")
                if(self.theCurrentModel.FilterTagsArray(text: textField.text ?? "").count == 0)
                {
                    textField.text = ""
                }
            }
        }
        
    }
    /*func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        print("textField did end editing")
        
        if(textField == self.theCurrentModel.tagsField)
        {
            print("tagsField did end editing")
            if(self.theCurrentModel.FilterTagsArray(text: textField.text ?? "").count == 0)
            {
                textField.text = ""
            }
        }
        return true
    }*/
    /*func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.theCurrentModel.tagsField)
        {
            if(textField.returnKeyType == .default)
            {
                self.theCurrentModel.tagsField.text = ""
                self.view.endEditing(true)
            }
        }
        return true
    }*/
    
    
}
//MARK:- Redirect To
extension ActionNewFilterVC {

    func redirectToCustomDatePicker(selectionType:dateSelectionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        /*if theCurrentModel.isEditFocus {
         if let reviseDate = theCurrentModel.selectedData["revisedduedate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outPutType4) {
         vc.setTheDate(selectedDate: date)
         }
         }*/
        /*
        switch selectionType {
        case .work:
            if let workDate = theCurrentModel.filterData.workDate {
                vc.setTheDate(selectedDate: workDate)
            }
            break
        case .due:
            if let dueDate = theCurrentModel.filterData.dueDate {
                vc.setTheDate(selectedDate: dueDate)
            }
            break
        default: break
            
        }*/
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            self?.updateDueDate(strDate: dateFormaater.string(from: date), dateSelectionType: selectionType, date: date)
            
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
//MARK:- UICollectionViewDelegate
extension ActionNewFilterVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentModel.arr_dateSlot.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateSlotCVCell", for: indexPath) as! DateSlotCVCell
        cell.configureCell(strTitle: theCurrentModel.arr_dateSlot[indexPath.row])

        if collectionView == theCurrentView.collectionWorkDate {
            if let workDate = theCurrentModel.selectedWorkDate, !workDate.isEmpty, workDate.lowercased() ==  theCurrentModel.arr_dateSlot[indexPath.row] {
                cell.isSelected = true
            }
        } else {
            if let dueDate = theCurrentModel.selectedDueDate, !dueDate.isEmpty, dueDate.lowercased() ==  theCurrentModel.arr_dateSlot[indexPath.row] {
                cell.isSelected = true
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == theCurrentView.collectionWorkDate {
            if theCurrentModel.selectedWorkDate == theCurrentModel.arr_dateSlot[indexPath.row] {
                theCurrentModel.selectedWorkDate = nil
                theCurrentModel.filterData.workDate = nil
                collectionView.deselectItem(at: indexPath, animated: false)
            } else {
                theCurrentModel.selectedWorkDate = theCurrentModel.arr_dateSlot[indexPath.row]
                theCurrentModel.filterData.workDate = theCurrentModel.arr_dateSlot[indexPath.row]
            }
        } else {
            if theCurrentModel.selectedDueDate == theCurrentModel.arr_dateSlot[indexPath.row] {
                theCurrentModel.selectedDueDate = nil
                theCurrentModel.filterData.dueDate = nil
                collectionView.deselectItem(at: indexPath, animated: false)
            } else {
                theCurrentModel.selectedDueDate = theCurrentModel.arr_dateSlot[indexPath.row]
                theCurrentModel.filterData.dueDate = theCurrentModel.arr_dateSlot[indexPath.row]
            }
        }
    }
    /*
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == theCurrentView.collectionWorkDate {
            theCurrentModel.selectedWorkDate = nil
        } else {
            theCurrentModel.selectedDueDate = nil
        }
        collectionView.reloadData()
    }*/
}


//MARK:- Call API
extension ActionNewFilterVC {
    func getCategories()
    {
        showLoader(theCurrentView.btnCategoryLoaderOutlet)
        categoiesListService()
    }
    
    func getCompanyList()
    {
        showLoader(theCurrentView.btnCompanyLoaderOutlet)
        companylistService()
        
    }
    func getTagsList()
    {
        showLoader(theCurrentView.btnTagLoaderOutlet)
        actionFilterTagListService()
    }
}
//MARK:- Other methods
extension ActionNewFilterVC
{
    func updateDueDate(strDate:String,dateSelectionType:dateSelectionType,date:Date) {
        switch dateSelectionType {
        case .created:
//            theCurrentModel.selectedData["createdate"].stringValue = strDate
            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 9, section: 0)], with: UITableView.RowAnimation.none)
            break
        case .due:
            theCurrentModel.selectedDueDate = strDate
//            theCurrentModel.selectedData["duedate"].stringValue = strDate
            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 10, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentModel.filterData.dueDate = strDate
            theCurrentView.txtDueDate.text = strDate
            break
        case .work:
            theCurrentModel.selectedWorkDate = strDate
            theCurrentModel.filterData.workDate = strDate

//            theCurrentModel.selectedData["workdate"].stringValue = strDate
            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 11, section: 0)], with: UITableView.RowAnimation.none)
            theCurrentView.txtWorkDate.text = strDate
            break
        }
    }
    func updateTags() {
        theCurrentModel.tagsDD.dataSource = theCurrentModel.arr_Tags

        let tagIDs = theCurrentModel.filterData.searchTag.components(separatedBy: ",")
        for tagid in tagIDs {
            if let index = theCurrentModel.arr_Tags_Main.firstIndex(where: {$0.tag_id == tagid}) {
                theCurrentModel.tagsField.addTag(theCurrentModel.arr_Tags_Main[index].tag_name)
                theCurrentModel.tagsField.text = ""
            }
        }
        
    }
    func updateCategory() {
        theCurrentModel.categoryDD.dataSource = theCurrentModel.arr_categoryList.map({$0.category_name })
        if let index = theCurrentModel.arr_categoryList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCategoryID}) {
            theCurrentView.txtCategory.text = theCurrentModel.arr_categoryList[index].category_name
        }
    }
    func updateCompany() {
        theCurrentModel.companyDD.dataSource = theCurrentModel.arr_companyList.map({$0.name })

        if let index = theCurrentModel.arr_companyList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCompanyID}) {
            theCurrentView.txtCompany.text = theCurrentModel.arr_companyList[index].name
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
        }
    }
    func updateAssignTo() {
        theCurrentModel.assignedToDD.dataSource = theCurrentModel.arr_assignList.map({$0.assignee_name })

        if let index = theCurrentModel.arr_assignList.firstIndex(where: {$0.user_id == theCurrentModel.strSelectedUserAssignID}) {
            theCurrentView.txtAssignedTo.text = theCurrentModel.arr_assignList[index].assignee_name
//            departmentListService()
        }
    }
    func updateDepartment() {
        theCurrentModel.departmentDD.dataSource = theCurrentModel.arr_departmentList.map({$0.name })

        if let index = theCurrentModel.arr_departmentList.firstIndex(where: {$0.id == theCurrentModel.strSelectedDepartmentID}) {
            theCurrentView.txtDepartment.text = theCurrentModel.arr_departmentList[index].name
        }
    }
    func updateRelatedTo() {
        theCurrentModel.relatedToDD.dataSource = theCurrentModel.arr_relatedList.map({$0.option })
        
        if let index = theCurrentModel.arr_relatedList.firstIndex(where: {$0.value == theCurrentModel.strSelectedRelatedId}) {
            theCurrentView.txtRelatedTo.text = theCurrentModel.arr_relatedList[index].option
        }
    }
}
//MARK:- Loader
extension ActionNewFilterVC
{
    func showLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(true)
    }
    
    func stopLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(false)
    }
    
}

//MARK:- Call API
extension ActionNewFilterVC
{
    func actionFilterTagListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        ActionsWebService.shared.getTagList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.stopLoader(self?.theCurrentView.btnTagLoaderOutlet)
//            self?.theCurrentModel.arr_Tags_Main = list
            self?.theCurrentModel.arr_Tags_Main = list.filter { $0.tagStatus.lowercased() == APIKey.key_tag_active_status
            }
//            print("arr_activeTag \(self?.theCurrentModel.arr_Tags_Main)")
            self?.theCurrentModel.arr_Tags = self?.theCurrentModel.arr_Tags_Main.map({$0.tag_name}) ?? []
//            print("string tag \(self?.theCurrentModel.arr_Tags)")
//            self?.theCurrentModel.arr_Tags = list.map({$0.tag_name})
            self?.updateTags()
            }, failed: { [weak self] (error) in
                self?.stopLoader(self?.theCurrentView.btnTagLoaderOutlet)
//                self?.showAlertAtBottom(message: error)
        })
    }
    func categoiesListService() {
        let categoryFor = theCurrentModel.filterType == FilterType.risks ? CategoryFor.risk.rawValue : CategoryFor.tacticalProject.rawValue

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_category_for:categoryFor]
        CommanListWebservice.shared.getCategories(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_categoryList = list
            self?.updateCategory()
            self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
        })
    }
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_companyList = list
            self?.updateCompany()
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.companyDD)!, view: (self?.theCurrentView.viewCompnay)!)
            self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
        })
    }
    
    func assignUserListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        
        if theCurrentModel.filterType == FilterType.action {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
        } else if theCurrentModel.filterType == FilterType.focus {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_focuses
        } else if theCurrentModel.filterType == FilterType.risks {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_risks
        } else if theCurrentModel.filterType == FilterType.tacticalProject {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_tactical_projects
        } else if theCurrentModel.filterType == FilterType.approvals {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
            parameter[APIKey.key_status] = APIKey.key_assign_to_for_approval
        }
        
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_assignList = list
            self?.updateAssignTo()
            self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.assignedToDD)!, view: (self?.theCurrentView.viewAssignedTo)!)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
    }
    
    func relatedListService() {
        showLoader(theCurrentView.btnRelatedToLoderOutlet)
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_related_type:APIKey.key_action_log_filter]
        ActionsWebService.shared.getRelatedTo(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.stopLoader(self?.theCurrentView.btnRelatedToLoderOutlet)
            list.forEach({self?.theCurrentModel.arr_relatedList.append($0)})
//            self?.theCurrentModel.arr_relatedList = list
            self?.updateRelatedTo()
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnRelatedToLoderOutlet)
        })
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        AddUserWebService.shared.getCompanyDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_departmentList = list
            self?.updateDepartment()
            self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.departmentDD)!, view: (self?.theCurrentView.viewDepartment)!)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
        })
    }
    
}
