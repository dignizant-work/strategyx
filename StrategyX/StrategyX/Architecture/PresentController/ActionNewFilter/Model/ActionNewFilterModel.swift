//
//  ActionFilterModel.swift
//  StrategyX
//
//  Created by Jaydeep on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import WSTagsField

class ActionNewFilterModel {
    
    //MARK:- Variable
    fileprivate weak var theController:ActionNewFilterVC!
    var topConstraint:CGFloat = 10.0
    
    var filterData:FilterData = FilterData()

    let departmentDD = DropDown()
    let relatedToDD = DropDown()
    let categoryDD = DropDown()
    let assignedToDD = DropDown()
    let tagsDD = DropDown()
    let companyDD = DropDown()

    
    var filterType = FilterType.strategy
    var tagsField = WSTagsField()
    var arr_Tags : [String] = []
    var arr_Tags_Main : [TagList] = []
    var isDropDownShowing = false
    
    var arr_dateSlot = ["Today", "Tomorrow", "This Week", "Next Week", "This Month", "Next Month", "Later"]
    
    
    // Related to List
    var arr_relatedList:[RelatedTo] = []
    var strSelectedRelatedId:Int = -1
    
    // get category
    
    var arr_categoryList:[Categories] = []
    var strSelectedCategoryID:String = ""
    
    // getCompany
    
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Assign
    
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    var selectedDueDate:String?
    var selectedWorkDate:String?
    
    
    //MARK:- LifeCycle
    init(theController:ActionNewFilterVC) {
        self.theController = theController
        let relatedTo = RelatedTo()
        relatedTo.id = ""
        relatedTo.option = "Related To"
        relatedTo.value = -1
        arr_relatedList = [relatedTo]
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case categoryDD:
            let arr_Categorylist:[String] = arr_categoryList.map({$0.category_name })
            dropDown.dataSource = arr_Categorylist
            break
        case relatedToDD:
            let arr_relatedTo:[String] = arr_relatedList.map({$0.option })
            dropDown.dataSource = arr_relatedTo
            break
        case companyDD:
            let arr_Companylist:[String] = arr_companyList.map({$0.name })
            dropDown.dataSource = arr_Companylist
            break
            
        case assignedToDD:
            let arr_AssignList:[String] = arr_assignList.map({$0.assignee_name })
            //            let arr_AssignList:[String] = arr_assignList.map({$0.assign_user_name })
            dropDown.dataSource = arr_AssignList
            break
        case departmentDD:
            let arr_departmentList:[String] = self.arr_departmentList.map({$0.name })
            dropDown.dataSource = arr_departmentList
            break
            
        case tagsDD:
            dropDown.dataSource = arr_Tags
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.relatedToDD:
                self.strSelectedRelatedId = self.arr_relatedList[index].value
                break
            case self.categoryDD:
                self.strSelectedCategoryID = self.arr_categoryList[index].id
                break
            case self.companyDD:
                self.strSelectedCompanyID = self.arr_companyList[index].id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.assignedToDD:
                //                self.strSelectedUserAssignID = self.arr_assignList[index].id
                self.strSelectedUserAssignID = self.arr_assignList[index].user_id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.departmentDD:
                self.strSelectedDepartmentID = self.arr_departmentList[index].id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.tagsDD:
//                self.theController.update(text: item, dropDown: dropDown)
                break
            default:
                break
            }
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
    func configureTagsView()
    {
//        tagsField.frame = tagsView.bounds
//        tagsView.addSubview(tagsField)
        
        //tagsField.translatesAutoresizingMaskIntoConstraints = false
        //tagsField.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        

        tagsField.spaceBetweenLines = 5
        tagsField.spaceBetweenTags = 10
        
        //tagsField.numberOfLines = 3
        //tagsField.maxHeight = 100.0
        
        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 6)
        tagsField.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10) //old padding
        
        tagsField.placeholder = "Tags search"
        tagsField.placeholderColor = appPlaceHolder
        tagsField.placeholderAlwaysVisible = true
        tagsField.backgroundColor = UIColor.white
        tagsField.returnKeyType = .next
        tagsField.delimiter = ""
        tagsField.font = R14
        tagsField.tintColor = appBgLightGrayColor
        
        //        tagsField.keyboardAppearance = .dark
        
        tagsField.textDelegate = theController
        tagsField.acceptTagOption = .space
        theController.textFieldEvents(tagsField : tagsField)
    }
    func FilterTagsArray(text : String) -> [String]
    {
        arr_Tags = []
        arr_Tags_Main.forEach { (tagList) in
            if(tagList.tag_name.lowercased().contains(text.lowercased()))
            {
                arr_Tags.append(tagList.tag_name)
            }
        }
        self.tagsDD.dataSource = arr_Tags
        return arr_Tags
        
    }
}
