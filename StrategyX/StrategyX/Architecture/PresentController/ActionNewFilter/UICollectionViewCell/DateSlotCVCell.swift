//
//  DateSlotCVCell.swift
//  StrategyX
//
//  Created by Haresh on 08/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class DateSlotCVCell: UICollectionViewCell {

    @IBOutlet weak var lblDate: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.contentView.setBackGroundColor = isSelected ? 3 : 1
            lblDate.setFontColor = isSelected ? 1 : 7
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureCell(strTitle:String) {
        lblDate.text = strTitle
    }
    

}
