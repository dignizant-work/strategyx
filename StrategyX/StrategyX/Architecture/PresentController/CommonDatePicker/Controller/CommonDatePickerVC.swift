//
//  CommonDatePickerVC.swift
//  StrategyX
//
//  Created by Jaydeep on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit


struct datepickerMode
{
    var pickerMode : UIDatePicker.Mode
    
    init(customPickerMode:UIDatePicker.Mode)
    {
        self.pickerMode = customPickerMode
    }
}

class CommonDatePickerVC: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var vwButtonHeader : UIView!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnDone : UIButton!
    @IBOutlet weak var datePicker : UIDatePicker!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
    var isSetMaximumDate = false
    var isSetMinimumDate = false
    
    var maximumDate = Date()
    var minimumDate = Date()
    var isSetDate = false
    var minuteInterval = -1
    var isForDuration = false
    
    var setDateValue : Date?
    var handlerSelected:(_ date:Date,_ type:Int) -> Void = {_,_ in }
    
    
    //MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetupUI()
    }
    
    
}
//MARK: - Setup UI
extension CommonDatePickerVC
{
    func SetupUI()
    {
        if isForDuration {
            lblTitle.text = "  Select Duration"
        }
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        if minuteInterval != -1 {
            datePicker.minuteInterval = minuteInterval
        }
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
        if(isSetDate)
        {
            datePicker.setDate(setDateValue ?? Date(), animated: false)
        }
    }
}
//MARK: - Button Action
extension CommonDatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        handlerSelected(datePicker.date, 1)  
       
        
    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        handlerSelected(datePicker.date, 2)
        
        
    }
}
