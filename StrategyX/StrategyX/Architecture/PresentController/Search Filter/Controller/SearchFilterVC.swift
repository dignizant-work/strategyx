//
//  SearchFilterVC.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SearchFilterVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:SearchFilterView = { [unowned self] in
       return self.view as! SearchFilterView
    }()
    fileprivate lazy var theCurrentModel:SearchFilterModel = {
       return SearchFilterModel(theController: self)
    }()
    
    var handlerSearchText:(String) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
        
        theCurrentView.handlerOnTextChange = { [weak self] (text) in
            self?.handlerSearchText(text)
        }
    }
    
    func setTopConstraint(top:CGFloat,filterType:SearchFilterModel.searchFilterType) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
    }
    
    //MARK:- IBAction
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}
