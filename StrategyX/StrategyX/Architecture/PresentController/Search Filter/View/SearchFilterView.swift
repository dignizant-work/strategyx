//
//  SearchFilterView.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SearchFilterView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
 
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
    let disposeBag = DisposeBag()
    var handlerOnTextChange:(String) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        btnReset(isHidden: true)
        onTextSearchAction()
    }
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        constrainBtnResetBottom.constant = isHidden ? 0.0 : 15.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
        })
    }
    func updateViewAccordingTo(type:SearchFilterModel.searchFilterType) {
        viewSearch.isHidden = false

    }
    
    //RX
    func onTextSearchAction() {
        txtSearch.rx.text.orEmpty
            .subscribe(onNext: { [weak self] (text) in
                self?.handlerOnTextChange(text)
            }).disposed(by: disposeBag)
    }

}
