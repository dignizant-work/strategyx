//
//  SearchFilterModel.swift
//  StrategyX
//
//  Created by Haresh on 06/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SearchFilterModel {

    //MARK:- Variabel
    enum searchFilterType:Int {
        case company        = 1
        case users          = 2
        case tags           = 3
    }
    
    fileprivate weak var theController:SearchFilterVC!
    var topConstraint:CGFloat = 10.0
    
    
    var filterType = searchFilterType.company
    
    //MARK:- LifeCycle
    init(theController:SearchFilterVC) {
        self.theController = theController
    }
    
}
