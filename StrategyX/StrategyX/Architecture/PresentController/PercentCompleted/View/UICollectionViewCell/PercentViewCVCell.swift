//
//  PercentViewCVCell.swift
//  StrategyX
//
//  Created by Haresh on 06/09/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PercentViewCVCell: UICollectionViewCell {

    @IBOutlet weak var lblPrecent: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.contentView.backgroundColor = isSelected ? appGreen : UIColor.clear
            lblPrecent.setFontColor = isSelected ? 1 : 4
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(percent:Int) {
        lblPrecent.text = "\(percent)%"
    }

}
