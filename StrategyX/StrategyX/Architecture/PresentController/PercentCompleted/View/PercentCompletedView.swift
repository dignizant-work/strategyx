//
//  PercentCompletedView.swift
//  StrategyX
//
//  Created by Haresh on 06/09/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PercentCompletedView: ViewParentWithoutXIB {
    //MARK:- IBOutlet
    
    @IBOutlet weak var constantCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView! // 250.0
    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }
    func setupCollectionView(theDelegate:PercentcompletedVC) {
        let width = collectionView.frame.size.width/4
        print("collectionView.frame.size.width:=\(collectionView.frame.size.width), width:=",width)
//        constantCollectionViewHeight.constant = width * 5
        collectionView.isScrollEnabled = true
        collectionView.registerCellNib(PercentViewCVCell.self)
        collectionView.delegate = theDelegate
        collectionView.dataSource = theDelegate
    }

    
}
