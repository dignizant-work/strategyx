//
//  PercentcompletedVC_CollectionViewDelegate+Extension.swift
//  StrategyX
//
//  Created by Haresh on 06/09/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

extension PercentcompletedVC:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_percentage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PercentViewCVCell", for: indexPath) as! PercentViewCVCell
        cell.configureCell(percent: theCurrentViewModel.arr_percentage[indexPath.row])
//        if theCurrentViewModel.selectedPercent == theCurrentViewModel.arr_percentage[indexPath.row] {
//            cell.isSelected = true
//        }
        return cell
    }
}

extension PercentcompletedVC:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        theCurrentViewModel.selectedPercent = theCurrentViewModel.arr_percentage[indexPath.row]
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension PercentcompletedVC:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (UIScreen.main.bounds.size.width - 70)/4
        print("size:=",size)    
        theCurrentView.constantCollectionViewHeight.constant = size * 5
        return CGSize(width: size, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
