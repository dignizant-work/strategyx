//
//  PercentcompletedVC.swift
//  StrategyX
//
//  Created by Haresh on 06/09/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class PercentcompletedVC: ParentViewController {

    internal lazy var theCurrentView:PercentCompletedView = { [unowned self] in
       return self.view as! PercentCompletedView
    }()
    
    internal lazy var theCurrentViewModel:PercentCompletedViewModel = {
       return PercentCompletedViewModel()
    }()
    
    var handlerSelectedPercent:(Int) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let _ = self.view
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let size = theCurrentView.collectionView.frame.size.width/4
//        print("size:=",size)
//        theCurrentView.constantCollectionViewHeight.constant = size * 5
        if let index = theCurrentViewModel.arr_percentage.firstIndex(where: {$0 == theCurrentViewModel.selectedPercent}) {
            theCurrentView.collectionView.selectItem(at: IndexPath.init(row: index, section: 0), animated: false, scrollPosition: .left)
        }
    }

    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupCollectionView(theDelegate: self)
        theCurrentView.collectionView.reloadData()
    }
    func setTheData(thePercent:Int, strActionID:String, isForEdit:Bool = false) {
        theCurrentViewModel.selectedPercent = thePercent
        theCurrentViewModel.strActionID = strActionID
        theCurrentViewModel.isForEdit = isForEdit
    }
    
    //MARK:- IBAction
    @IBAction func onBtnSaveAction(_ sender: UIButton) {
        if theCurrentViewModel.selectedPercent < 0 {
            self.showAlertAtBottom(message: "Please select % completed fot this task")
            return
        }
        if theCurrentViewModel.isForEdit {
            self.handlerSelectedPercent(self.theCurrentViewModel.selectedPercent)
            self.onBtnCancelAction("")
            return
        }
        self.view.allButtons(isEnable: false)
        sender.startLoadyIndicator()
        theCurrentViewModel.changeCompletedPercentageWebserview(success: { [weak self] (msg) in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            print("completed:=",self?.theCurrentViewModel.selectedPercent ?? 0)
            self?.handlerSelectedPercent(self?.theCurrentViewModel.selectedPercent ?? 0)
            self?.onBtnCancelAction("")
        }, failed: { [weak self] (error) in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            self?.showAlertAtBottom(message: error)
        })
        
    }
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
