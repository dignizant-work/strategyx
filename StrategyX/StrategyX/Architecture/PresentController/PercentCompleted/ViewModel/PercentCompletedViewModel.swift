//
//  PercentCompletedViewModel.swift
//  StrategyX
//
//  Created by Haresh on 06/09/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PercentCompletedViewModel {

    //MARK:- Variable
    var arr_percentage:[Int] = []
    var selectedPercent = -1
//    let disposeBag = DisposeBag()
    var strActionID = ""
    var isForEdit = false
    
    //MARK:- LifeCycle
    init() {
        var firstValue = 0
       
        for _ in 0...4 {
            firstValue += 5
            var secondValue = firstValue
            for _ in 0...3 {
                arr_percentage.append(secondValue)
                secondValue += 25
            }
        }
        print("arr_percentage:=",arr_percentage)
    }
}

extension PercentCompletedViewModel {
    func changeCompletedPercentageWebserview(success:@escaping(String) -> Void,failed:@escaping(String) -> Void) {
        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:strActionID,
                         APIKey.key_completed_percentage:selectedPercent] as [String : Any]
        
        ActionsWebService.shared.changeCompletedPrecentage(parameter: parameter, success: { (msg) in
            success(msg)
            }, failed: { (error) in
                failed(error)
        })
        
    }
}
