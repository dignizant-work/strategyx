//
//  ActionGraphView.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts

class ActionGraphView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var activityIndicatorChart: UIActivityIndicatorView!
    @IBOutlet weak var constrainBGViewTop: NSLayoutConstraint! // 0
    @IBOutlet weak var constrainBGViewBottom: NSLayoutConstraint! // 0
    
    @IBOutlet weak var viewLineChart: LineChartView!
    @IBOutlet weak var viewBottomChart: UIView!
    
    //MARK:- Life Cycle
    func setupLayout() {
        setupActivityIndicator()
    }
    
    func setupActivityIndicator(){
        [activityIndicatorChart].forEach { (activityIndicator) in
            configureActivityIndicator(activityIndicator: activityIndicator)
        }
    }
    
    func configureActivityIndicator(activityIndicator:UIActivityIndicatorView) {
        activityIndicator.color = appGreen
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
    }
    
    func updateConstrain(top:CGFloat,bottom:CGFloat) {
        constrainBGViewTop.constant = top
        constrainBGViewBottom.constant = bottom
    }
    
    //MARK:- Chart
    func setupChart(theDelegate:ActionGraphVC) {
        viewLineChart.delegate = theDelegate
        
        viewLineChart.chartDescription?.enabled = false
        viewLineChart.setScaleEnabled(false)
        viewLineChart.pinchZoomEnabled = true
        viewLineChart.scaleXEnabled = true
        viewLineChart.doubleTapToZoomEnabled = true
        viewLineChart.autoScaleMinMaxEnabled = false
        viewLineChart.clipsToBounds = false
        viewLineChart.clipValuesToContentEnabled = false
        //viewLineChart.setVisibleXRangeMaximum(3)
        viewLineChart.extraRightOffset = 0
        viewLineChart.extraLeftOffset = 0
        viewLineChart.legend.enabled = false
        viewLineChart.extraTopOffset = 20
        // viewLineChart.drawBordersEnabled = false
        
        let l = viewLineChart.legend
        l.form = .line
        l.font = R12
        l.textColor = appPlaceHolder
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.formLineWidth  = 10
        l.drawInside = false
        
        let xAxis = viewLineChart.xAxis
        xAxis.labelFont = R12
        xAxis.labelTextColor = appPlaceHolder
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelRotationAngle = 280
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.yOffset = 20
        xAxis.axisLineColor = appSepratorColor
        xAxis.labelCount = 5
        
        let yAxis = viewLineChart.leftAxis
        yAxis.labelTextColor = appPlaceHolder
        yAxis.axisMinimum = 0
        yAxis.drawGridLinesEnabled = true
        yAxis.granularityEnabled = false
        yAxis.enabled = true
        yAxis.labelTextColor = appPlaceHolder
        yAxis.drawAxisLineEnabled = false
        yAxis.xOffset = 20
        yAxis.labelFont = R12
        yAxis.axisLineColor = appSepratorColor
        
        let rightAxis = viewLineChart.rightAxis
        rightAxis.enabled = false
    }
    
    
    func setDataCount(arrChart:[ChartList]) {
     
        /*let chartData = ChartList()
        chartData.date = ""
        chartData.totalAction = 0
        chartData.totalCompleteAction = 0
        var arrChartList = arrChart
        arrChartList.insert(chartData, at: 0)*/
        
        var yVals1:[ChartDataEntry] = []
        for i in 0..<arrChart.count{
            yVals1.append(ChartDataEntry(x: Double(i), y: Double(arrChart[i].totalAction)))
        }
        
        var yVals2:[ChartDataEntry] = []
        for i in 0..<arrChart.count{
            yVals2.append(ChartDataEntry(x: Double(i), y: Double(arrChart[i].totalCompleteAction)))
        }
        
        let set1 = LineChartDataSet(values: yVals1, label: "DataSet 1")
        set1.axisDependency = .left
        set1.setColor(appGraphCyanColor)
        set1.lineWidth = 2
        set1.circleRadius = 0
        set1.fillAlpha = 65/255
        set1.fillColor = UIColor.blue
        set1.highlightEnabled = false
        set1.mode = .linear
        
        let set2 = LineChartDataSet(values: yVals2, label: "DataSet 2")
        set2.axisDependency = .left
        set2.setColor(appGreen)
        set2.lineWidth = 2
        set2.circleRadius = 0
        set2.fillAlpha = 65/255
        set2.fillColor = .red
        set2.highlightEnabled = false
        set2.mode = .cubicBezier
        
        let data = LineChartData(dataSets: [set1,set2])
        data.setValueTextColor(.clear)
        data.setValueFont(R14)
        
        var str : [String] = []
        for i in 0..<arrChart.count{
            str.append(arrChart[i].date)
        }
        
        viewLineChart.xAxis.valueFormatter = DayAxisValueFormatter(chart: viewLineChart, array: str)
        viewLineChart.data = data
    }
    
    func statusActivityIndicator(isLoading:Bool=false,activityIndicator:UIActivityIndicatorView?){
        if isLoading == true{
            activityIndicator?.color = appGreen
            activityIndicator?.isHidden = false
            activityIndicator?.startAnimating()
        }else{
            activityIndicator?.stopAnimating()
        }
    }
}
