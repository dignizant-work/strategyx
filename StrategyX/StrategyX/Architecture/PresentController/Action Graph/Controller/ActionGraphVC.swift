//
//  ActionGraphVC.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import Charts
class ActionGraphVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:ActionGraphView = { [unowned self] in
       return self.view as! ActionGraphView
    }()
    
    fileprivate lazy var theCurrentModel:ActionGraphModel = {
       return ActionGraphModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupChart(theDelegate: self)
        theCurrentView.statusActivityIndicator(isLoading: true, activityIndicator: theCurrentView.activityIndicatorChart)
        theCurrentView.updateConstrain(top: theCurrentModel.topConstrain, bottom: theCurrentModel.bottomConstrain)
        getChartListWebService()
    }
    
    func setTheTopConstrain(top:CGFloat,bottom:CGFloat, chartFlagViewType:ChartFlagView) {
        theCurrentModel.topConstrain = top
        theCurrentModel.bottomConstrain = bottom
        theCurrentModel.chartFlagViewType = chartFlagViewType
    }
    
    func setUpChartId(strChartId:String){
        theCurrentModel.strChartId = strChartId
    }
    
    func skeletonAnimation(isAnimating:Bool) {
        if isAnimating {
            theCurrentView.viewLineChart.showAnimatedSkeleton()
        } else {
            theCurrentView.viewLineChart.hideSkeleton()
        }
    }

    //MARK:-IBAction
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- ChartViewDelegate
extension ActionGraphVC : ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected")
    }
}
//MARK:- Api Call
extension ActionGraphVC {
    func getChartListWebService() {
        theCurrentView.viewLineChart.isHidden = true
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_flag_view:theCurrentModel.chartFlagViewType.rawValue] as [String : Any]
        if !theCurrentModel.strChartId.isEmpty {
            parameter[APIKey.key_chart_id] = theCurrentModel.strChartId
        }
        if theCurrentModel.chartFlagViewType == .primary {
            parameter[APIKey.key_flag_view] = ChartFlagView.strategy.rawValue
            parameter[APIKey.key_chart_for] = APIKey.key_primary
        } else if theCurrentModel.chartFlagViewType == .secondary {
            parameter[APIKey.key_flag_view] = ChartFlagView.strategy.rawValue
            parameter[APIKey.key_chart_for] = APIKey.key_secondary
        } else if theCurrentModel.chartFlagViewType == .strategyGoal {
            parameter[APIKey.key_flag_view] = ChartFlagView.strategy.rawValue
            parameter[APIKey.key_chart_for] = APIKey.key_strategy_goal
        }
        
        CommanListWebservice.shared.chartList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: self?.theCurrentView.activityIndicatorChart)
            self?.theCurrentView.viewLineChart.isHidden = false
            self?.theCurrentModel.arr_ChartAction = list
            self?.theCurrentView.setDataCount(arrChart: self?.theCurrentModel.arr_ChartAction ?? [])
            }, failed: {  [weak self] (error) in
                 self?.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: self?.theCurrentView.activityIndicatorChart)
                self?.theCurrentView.viewLineChart.isHidden = false
                self?.theCurrentView.viewBottomChart.isHidden = true
                self?.showAlertAtBottom(message: error)
        })
    }
    
}
