//
//  GoalInfoVC.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddFocusInfoVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:AddFocusInfoView = { [unowned self] in
       return self.view as! AddFocusInfoView
    }()
    fileprivate lazy var theCurrentModel:AddFocusInfoModel = {
       return AddFocusInfoModel(theController: self)
    }()
    
    var handlerSMARTSelection:([Int]) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        theCurrentView.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    func setTheData(arr:[Int], isEdit:Bool = true) {
        theCurrentModel.isEdit = isEdit
        for i in 0..<arr.count {
            theCurrentModel.arr_SMART[i].2 = arr[i]
        }
    }
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupTableView(theDelegate: self)
        theCurrentView.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        theCurrentView.tableView.reloadData()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" , let obj = object as? UITableView {
            theCurrentView.constrainTableViewHeight.constant = obj.contentSize.height
            self.view.layoutIfNeeded()
//            UIView.animate(withDuration: 0.1) {
//                
//            }
        }
    }

    //MARK:-IBAction
    @IBAction func onBtnCloseAction(_ sender: Any) {
        let selection:[Int] = theCurrentModel.arr_SMART.map({$0.2})
        
        handlerSMARTSelection(selection)
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:-UITableViewDataSource
extension AddFocusInfoVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theCurrentModel.arr_SMART.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddFocusInfoTVCell")  as! AddFocusInfoTVCell
        cell.selectionStyle = .none
        if !theCurrentModel.isEdit {
            cell.contentView.isUserInteractionEnabled = false
        }
        cell.configure(strTitle: theCurrentModel.arr_SMART[indexPath.row].0, strDescription: theCurrentModel.arr_SMART[indexPath.row].1, isActive: theCurrentModel.arr_SMART[indexPath.row].2 == 1 ? true : false)
        cell.handlerBtnActiveIsSelected = { [weak self] (isSelected) in
            self?.theCurrentModel.arr_SMART[indexPath.row].2 = isSelected ? 1 : 0
        }
        return cell
    }
    
}
//MARK:-UITableViewDataSource
extension AddFocusInfoVC:UITableViewDelegate {

}
