//
//  GoalInfoView.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddFocusInfoView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint! // 50.0
    
    //MARK:- LifeCycle
    func setupLayout() {
        setTitle()
    }

    func setTitle() {
        lblTitle.text = "A goal must be stated in the context of X? to Y? by When?.\n\nExample: Go from selling 100 Widgets each calendar Month to selling 150 Widgets each  calendar month by December 2018.\n\nA Goal Must Also be S.M.A.R.T."
    }
    func setupTableView(theDelegate:AddFocusInfoVC) {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50.0
        tableView.separatorStyle = .none
        tableView.registerCellNib(AddFocusInfoTVCell.self)
        tableView.delegate = theDelegate
        tableView.dataSource = theDelegate
    }
    
    
}
