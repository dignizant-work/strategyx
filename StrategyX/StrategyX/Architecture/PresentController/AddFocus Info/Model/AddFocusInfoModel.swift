//
//  GoalInfoModel.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddFocusInfoModel {

    //MARK:- Variable
    fileprivate weak var theController:AddFocusInfoVC!
    var isEdit = true
    
    var arr_SMART = [("Specific","It must be clear to any one what you want to achieve.", 0),("Measurable","You must be able to measure it on a regular basis (Week / Month)", 0),("Attainable","You Must be able to Attain the Goal in the Time Frame Set", 0),("Realistic","The Goal must be realistic.", 0),("Timely","There must be a time frame set for the achievement of the goal.", 0)]
    
    
    
    //MARK:- LifeCycle
    init(theController:AddFocusInfoVC) {
        self.theController = theController
    }
}
