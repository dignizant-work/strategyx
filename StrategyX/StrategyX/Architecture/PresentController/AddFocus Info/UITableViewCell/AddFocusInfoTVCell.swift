//
//  AddFocusInfoTVCell.swift
//  StrategyX
//
//  Created by Haresh on 13/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class AddFocusInfoTVCell: UITableViewCell {

    @IBOutlet weak var btnActive: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var handlerBtnActiveIsSelected:(Bool) -> Void = {_ in}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(strTitle:String,strDescription:String,isActive:Bool) {
        lblTitle.text = strTitle
        lblDescription.text = strDescription
        btnActive.isSelected = isActive
    }
    
    @IBAction func onBtnActiveAction(_ sender: UIButton) {
        btnActive.isSelected = !btnActive.isSelected
        handlerBtnActiveIsSelected(sender.isSelected)
    }
}
