//
//  ReportFilterVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import RxSwift
import RxCocoa

struct ReportFilterData {
    var companyID = ""
    var companyName = ""
    var assigntoID = ""
    var assigntoName = ""
    var departmentID = ""
    var departmentName = ""
    var reportType = ""
    var period = ""
    
    var customRangeDate:(Date?,Date?) = (nil,nil)
    
    
    init(companyID:String = "",companyName:String = "",assigntoID:String = "",assigntoName:String = "", departmentID:String = "", departmentName:String = "", reportType:String = "",period:String = "", customRangeDate:(Date?,Date?) = (nil,nil)) {
        self.companyID = companyID
        self.companyName = companyName
        self.assigntoID = assigntoID
        self.assigntoName = assigntoName
        self.departmentID = departmentID
        self.departmentName = departmentName
        self.reportType = reportType
        self.period = period
        self.customRangeDate = customRangeDate
    }
    
    
}


class ReportFilterCategoryViewModel {
    
    //MARK:- Variable
//    weak var theController:ReportFilterCategoryVC!
    var topConstraint:CGFloat = 10.0
    var filterData = ReportFilterData()
    let companyDD = DropDown()
    let assignedToDD = DropDown()
    let departmentDD = DropDown()
    let typeDD = DropDown()
    var filterType = FilterType.reports
//    var reportByType = ReportsSelectionBy.department
    var arr_type = [("Type",-2),("All",-1),("Strategy",1),("Success Factor",6),("Risk",2),("Tactical Project",3),("Idea",7),("Problem",8),("Focus",4),("Action",0)]
    var arr_dateSlot = ["Today", "This Week", "This Month", "Custom"]
//        ["All","Today","Yesterday","Tomorrow","This Week","Last Week","Next Week","This Month","Last Month", "Next Month","Custom Range"]
    
    var companyName:BehaviorRelay<String> = BehaviorRelay(value: "")
    var assignedToUserName:BehaviorRelay<String> = BehaviorRelay(value: "")
    var departmentName:BehaviorRelay<String> = BehaviorRelay(value: "")
    var typeName:BehaviorRelay<String> = BehaviorRelay(value: "")
    var periodName:BehaviorRelay<String> = BehaviorRelay(value: "")
    
    // getCompany
    
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // department
    
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    var completionHandlerForCompanyApi:(String) -> Void = {_ in}
    var completionHandlerForAssignToApi:(String) -> Void = {_ in}
    var completionHandlerForDepartmentToApi:(String) -> Void = {_ in}
    let disposeBag = DisposeBag()
    
    //MARK:- LifeCycle
    init() {
//        self.theController = theController
        
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case companyDD:
            var arr_Companylist:[String] = arr_companyList.map({$0.name })
            arr_Companylist.insert("Select company", at: 0)
            dropDown.dataSource = arr_Companylist
            break
        case assignedToDD:
            var userList = arr_assignList.map({$0.assignee_name })
            userList.insert("Select assignedto", at: 0)
            dropDown.dataSource = userList
            break
        case departmentDD:
            var department = arr_departmentList.map({$0.name })
            department.insert("Select department", at: 0)
            dropDown.dataSource = department
        case typeDD:
            dropDown.dataSource = arr_type.map({$0.0})
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.companyDD:
                self.strSelectedCompanyID = index == 0 ? "" : self.arr_companyList[index-1].id
                //                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.assignedToDD:
                //                self.strSelectedUserAssignID = self.arr_assignList[index].id
                self.strSelectedUserAssignID = index == 0 ? "" : self.arr_assignList[index-1].user_id
                //                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.departmentDD:
                self.strSelectedDepartmentID = index == 0 ? "" : self.arr_departmentList[index-1].id
                //                self.theController.update(text: item, dropDown: dropDown)
                break
            default:
                break
            }
            self.update(text: item, dropDown: dropDown)
        }
    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case companyDD:
            strSelectedUserAssignID = ""
            strSelectedDepartmentID = ""
            arr_assignList = []
            arr_departmentList = []
            companyName.accept(text)
            assignedToUserName.accept("")
            departmentName.accept("")
            break
        case assignedToDD:
            strSelectedDepartmentID = ""
            departmentName.accept("")
            assignedToUserName.accept(text)
            break
        case departmentDD:
            departmentName.accept(text)
            break
        case typeDD:
            if let index = arr_type.firstIndex(where: {$0.0 == text}) {
                typeName.accept("\(arr_type[index].1)")
            }
            break
        default:
            break
        }
        //        theCurrentView.btnReset(isHidden: false)
    }
    
    func updateCompany(list:[Company]) {
        arr_companyList = list
        var companyList = arr_companyList.map({$0.name })
        companyList.insert("Select assigned to", at: 0)
        companyDD.dataSource = companyList
        
        if let index = arr_companyList.firstIndex(where: {$0.id == strSelectedCompanyID}) {
            companyName.accept(arr_companyList[index].name)
        }
    }
    
    func updateAssignTo(list:[UserAssign]) {
        arr_assignList = list
        var userList = arr_assignList.map({$0.assignee_name })
        userList.insert("Select assigned to", at: 0)
        assignedToDD.dataSource = userList
        
        if let index = arr_assignList.firstIndex(where: {$0.user_id == strSelectedUserAssignID}) {
            assignedToUserName.accept(arr_assignList[index].assignee_name)
            //            theCurrentView.txtAssignedTo.text = arr_assignList[index].assign_user_name
            
        }
    }
    func updateDepartment(list:[Department]) {
        arr_departmentList = list
        var department = arr_departmentList.map({$0.name })
        department.insert("Select department", at: 0)
        departmentDD.dataSource = department
        if let index = arr_departmentList.firstIndex(where: {$0.id == strSelectedDepartmentID}) {
            departmentName.accept(arr_departmentList[index].name)
        }
    }
}

//MARK:- Api Call
extension ReportFilterCategoryViewModel {
    
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.updateCompany(list: list)
            self?.completionHandlerForCompanyApi("")
            }, failed: { [weak self] (error) in
                self?.strSelectedCompanyID = ""
                self?.updateCompany(list: [])
                self?.companyName.accept("")
                self?.completionHandlerForCompanyApi(error)
        })
    }
    
    func assignUserListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:strSelectedCompanyID]
        
        if filterType == FilterType.action {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
        } else if filterType == FilterType.focus {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_focuses
        } else if filterType == FilterType.risks {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_risks
        } else if filterType == FilterType.tacticalProject {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_tactical_projects
        } else if filterType == FilterType.approvals {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
            parameter[APIKey.key_status] = APIKey.key_assign_to_for_approval
        } else if filterType == FilterType.overViewReport {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_overview_reports
        } else if filterType == FilterType.cVScReport {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_created_vs_completed_reports
        }
        
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
//            self?.arr_assignList = list
            self?.updateAssignTo(list: list)
            self?.completionHandlerForAssignToApi("")
//            self?.theController.stopLoader(self?.theController.theCurrentView.btnAssignLoaderOutlet)
            //            if let obj = self {
            //                obj.configureDropDown(dropDown: obj.assignedToDD, view: obj.theCurrentView.viewAssignedTo)
            //            }
            }, failed: { [weak self] (error) in
                self?.updateAssignTo(list: [])
                self?.completionHandlerForAssignToApi(error)
//                self?.theController.showAlertAtBottom(message: error)
//                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_assign_to:strSelectedUserAssignID]
        CommanListWebservice.shared.getDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
//            self?.arr_departmentList =
            self?.updateDepartment(list: list)
            self?.completionHandlerForDepartmentToApi("")
//            self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
            //            if let obj = self {
            //                obj.configureDropDown(dropDown: obj.departmentDD, view: obj.theCurrentView.viewDepartment)
            //            }
            //            self?.configureDropDown(dropDown: (self?.departmentDD)!, view: (self?.theCurrentView.viewDepartment)!)
            }, failed: { [weak self] (error) in
                self?.updateDepartment(list: [])
                self?.completionHandlerForDepartmentToApi(error)
//                self?.theController.showAlertAtBottom(message: error)
//                self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
        })
    }
    
}

