//
//  ReportFilterCategoryVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class ReportFilterCategoryVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ReportFilterCategoryView = { [unowned self] in
        return self.view as! ReportFilterCategoryView
    }()
    
    fileprivate lazy var theCurrentViewModel:ReportFilterCategoryViewModel = {
        return ReportFilterCategoryViewModel()
    }()
    var handlerOnFilterSearch:(_ filterData:ReportFilterData) -> Void = {_ in }
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setupDelegate(theDelegate: self)
        theCurrentView.updateViewAccordingTo(type: theCurrentViewModel.filterType)
        if theCurrentViewModel.filterType == .overViewReport {
            theCurrentView.lblFilterByTitle.text = "Filter By Due Date"
        }
        theCurrentViewModel.configureDropDown(dropDown: theCurrentViewModel.assignedToDD, view: theCurrentView.viewResource)
        theCurrentViewModel.configureDropDown(dropDown: theCurrentViewModel.departmentDD, view: theCurrentView.viewDepartment)
        theCurrentViewModel.configureDropDown(dropDown: theCurrentViewModel.typeDD, view: theCurrentView.viewType)
        
        theCurrentView.constrainViewTop.constant = theCurrentViewModel.topConstraint
        
//        onBtnDateSectionAction(theCurrentView.btnCustomDate)
        theCurrentView.resetColor()
        theCurrentViewModel.periodName.accept(theCurrentViewModel.filterData.period)
        if let index = theCurrentViewModel.arr_dateSlot.firstIndex(where: {$0 == theCurrentViewModel.filterData.period }) {
            theCurrentView.collectionWorkDate.selectItem(at: IndexPath.init(row: index, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
        }
        theCurrentViewModel.companyName.asObservable()
            .subscribe(onNext: { [weak self] (newValue) in
                self?.theCurrentView.txtCompany.text = newValue
                if !(self?.theCurrentViewModel.strSelectedCompanyID ?? "").isEmpty && !newValue.isEmpty {
                    self?.assignToApiLoader(isAnimate: true)
                    self?.theCurrentViewModel.assignUserListService()
                }
            }).disposed(by: theCurrentViewModel.disposeBag)
        theCurrentViewModel.assignedToUserName.asObservable()
            .subscribe(onNext: { [weak self] (newValue) in
                self?.theCurrentView.txtResource.text = newValue
                if !(self?.theCurrentViewModel.strSelectedUserAssignID ?? "").isEmpty && !newValue.isEmpty {
                    self?.departmentApiLoader(isAnimate: true)
                    self?.theCurrentViewModel.departmentListService()
                }
            }).disposed(by: theCurrentViewModel.disposeBag)
        
        theCurrentViewModel.departmentName.asObservable()
            .subscribe(onNext: { [weak self] (newValue) in
                self?.theCurrentView.txtDepartment.text = newValue
            }).disposed(by: theCurrentViewModel.disposeBag)
        
        theCurrentViewModel.typeName.asObservable()
            .subscribe(onNext: { [weak self] (newValue) in
                if let newSelection = self?.theCurrentViewModel.arr_type.last(where: {$0.1 == (Int(newValue) ?? -2) }) {
                    self?.theCurrentView.txtType.text = newSelection.0
                }
            }).disposed(by: theCurrentViewModel.disposeBag)
        
        theCurrentViewModel.completionHandlerForCompanyApi = { [weak self] (error) in
            if !error.isEmpty {
                self?.showAlertAtBottom(message: error)
            } else {
                
            }
            self?.companyApiLoader(isAnimate: false)
        }
        theCurrentViewModel.completionHandlerForAssignToApi = { [weak self] (error) in
            if !error.isEmpty {
                self?.showAlertAtBottom(message: error)
            } else {
                
            }
            self?.assignToApiLoader(isAnimate: false)
        }
        theCurrentViewModel.completionHandlerForDepartmentToApi = { [weak self] (error) in
            if !error.isEmpty {
                self?.showAlertAtBottom(message: error)
            }
            self?.departmentApiLoader(isAnimate: false)
        }
        if !theCurrentViewModel.filterData.reportType.isEmpty {
            theCurrentViewModel.typeName.accept(theCurrentViewModel.filterData.reportType)
        }
        
//        callInitialWebServiceAccordingToFilterType()
        setCompanyData()
    }
    
    func setTheData(filterData:ReportFilterData) {
        theCurrentViewModel.filterData = filterData
        theCurrentViewModel.strSelectedCompanyID = filterData.companyID
        theCurrentViewModel.strSelectedUserAssignID = filterData.assigntoID
        theCurrentViewModel.strSelectedDepartmentID = filterData.departmentID
    }
    
    func setTopConstraint(top:CGFloat,filterType:FilterType) {
        theCurrentViewModel.topConstraint = top
        theCurrentViewModel.filterType = filterType
    }
    
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentViewModel.strSelectedCompanyID = Utility.shared.userData.companyId
            theCurrentViewModel.companyName.accept(Utility.shared.userData.companyName)
//            theCurrentView.txtCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompnay.isHidden = true
            //            theCurrentView.btnCompanyLoaderOutlet.isUserInteractionEnabled = false
            //            theCurrentView.viewCompnay.alpha = 0.5
//            assignToApiLoader(isAnimate: true)
//            theCurrentViewModel.assignUserListService()
        } else {
            if !(Utility.shared.userData.companyId.isEmpty) {
                theCurrentViewModel.strSelectedCompanyID = Utility.shared.userData.companyId
                theCurrentViewModel.companyName.accept(Utility.shared.userData.companyName)
                theCurrentView.viewCompnay.isHidden = true
                //                theCurrentView.btnCompanyLoaderOutlet.isUserInteractionEnabled = false
                //                theCurrentView.viewCompnay.alpha = 0.5
//                assignToApiLoader(isAnimate: true)
//                theCurrentViewModel.assignUserListService()
                return
            }
            companyApiLoader(isAnimate: true)
            theCurrentViewModel.companylistService()
        }
        
    }
    
    func callInitialWebServiceAccordingToFilterType() {
        theCurrentViewModel.strSelectedCompanyID = Utility.shared.userData.companyId.isEmpty ? "0" : Utility.shared.userData.companyId
        assignToApiLoader(isAnimate: true)
        theCurrentViewModel.assignUserListService()
    }
    func companyApiLoader(isAnimate:Bool) {
        theCurrentView.btnCompanyLoader.loadingIndicator(isAnimate)
    }
    func assignToApiLoader(isAnimate:Bool) {
        theCurrentView.btnAssignLoader.loadingIndicator(isAnimate)
    }
    
    func departmentApiLoader(isAnimate:Bool) {
        theCurrentView.btnDepartmentLoader.loadingIndicator(isAnimate)
    }
    //MARK:- Redirection
    func presentCalenderAction() {
        let vc = CalenderViewVC.init(nibName: "CalenderViewVC", bundle: nil)
        // let tabbarheight = self.tabBarController?.tabBar.frame.size.height ?? 49.0
        vc.setSelectedDateRange(firstDate: theCurrentViewModel.filterData.customRangeDate.0, lastDate: theCurrentViewModel.filterData.customRangeDate.1)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelectedDates = { [weak self] (dates) in
            if let startDate = dates.first, let lastDate = dates.last {
//                self?.theCurrentView.btnCustomDate.setTitle("\(startDate.string(withFormat: DateFormatterOutputType.outputType2))\n\(lastDate.string(withFormat: DateFormatterOutputType.outputType2))", for: .normal)
                self?.theCurrentViewModel.periodName.accept("Custom")
                self?.theCurrentViewModel.filterData.customRangeDate = (startDate,lastDate)
                if let index = self?.theCurrentViewModel.arr_dateSlot.count {
                    self?.theCurrentView.collectionWorkDate.reloadItems(at: [IndexPath.init(row: index - 1, section: 0)])
//                    self?.theCurrentView.collectionWorkDate.selectItem(at: IndexPath.init(row: index - 1, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.left)
                }
                
            }
        }
        self.present(vc, animated: false, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnResetAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentViewModel.filterData = ReportFilterData.init(companyID: Utility.shared.userData.companyId, companyName: Utility.shared.userData.companyName,assigntoID: "0", assigntoName: "")
        handlerOnFilterSearch(theCurrentViewModel.filterData)
        btnBtnBackAction(sender)
    }
    @IBAction func onBtnSubmitAction(_ sender: Any) {
         self.view.endEditing(true)
        theCurrentViewModel.filterData = ReportFilterData.init(companyID: theCurrentViewModel.strSelectedCompanyID, companyName:theCurrentView.txtCompany.text ?? "", assigntoID: theCurrentViewModel.strSelectedUserAssignID, assigntoName: theCurrentView.txtResource.text ?? "", departmentID: theCurrentViewModel.strSelectedDepartmentID, departmentName: theCurrentView.txtDepartment.text ?? "",reportType: theCurrentViewModel.typeName.value,period: theCurrentViewModel.periodName.value,customRangeDate: theCurrentViewModel.filterData.customRangeDate)
        handlerOnFilterSearch(theCurrentViewModel.filterData)
        btnBtnBackAction(sender)
    }
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnCompnayAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.theCurrentViewModel.arr_companyList.count != 0 {
            theCurrentViewModel.companyDD.show()
        } else {
            companyApiLoader(isAnimate: true)
            theCurrentViewModel.companylistService()
        }
    }
    @IBAction func onBtnResourcesAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedCompanyID != "" {
            if self.theCurrentViewModel.arr_assignList.count != 0 {
                theCurrentViewModel.assignedToDD.show()
            } else {
                assignToApiLoader(isAnimate: true)
                theCurrentViewModel.assignUserListService()
            }
        } else {
             self.showAlertAtBottom(message: "Please select company first")
        }
        
    }
    
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentViewModel.strSelectedUserAssignID != "" {
            if theCurrentViewModel.arr_departmentList.count != 0 {
                theCurrentViewModel.departmentDD.show()
            } else {
                departmentApiLoader(isAnimate: true)
                theCurrentViewModel.departmentListService()
            }
        } else {
            self.showAlertAtBottom(message: "Please select assigned to first")
            
        }
    }
    @IBAction func onBtnTypeAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentViewModel.typeDD.show()
    }
    
    @IBAction func onBtnDateSectionAction(_ sender:UIButton) {
        theCurrentView.resetColor()
        theCurrentView.selectedState(sender)
        theCurrentViewModel.periodName.accept(sender.titleLabel?.text ?? "")
        if sender == theCurrentView.btnCustomDate {
            presentCalenderAction()
        }
    }
    
}

//MARK:- UICollectionViewDelegate
extension ReportFilterCategoryVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentViewModel.arr_dateSlot.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateSlotCVCell", for: indexPath) as! DateSlotCVCell
        cell.lblDate.numberOfLines = 1
        cell.configureCell(strTitle: theCurrentViewModel.arr_dateSlot[indexPath.row])
        if theCurrentViewModel.arr_dateSlot[indexPath.row].lowercased() == "Custom".lowercased() {
            cell.lblDate.numberOfLines = 2
            if let start = theCurrentViewModel.filterData.customRangeDate.0, let end = theCurrentViewModel.filterData.customRangeDate.1 {
                cell.configureCell(strTitle: start.string(withFormat: DateFormatterOutputType.outputType2) + "\n" + end.string(withFormat: DateFormatterOutputType.outputType2))
            }
        }
        let workDate = theCurrentViewModel.periodName.value
        if  !workDate.isEmpty, workDate.lowercased() ==  theCurrentViewModel.arr_dateSlot[indexPath.row].lowercased() {
            cell.isSelected = true
            
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == theCurrentView.collectionWorkDate {
            if theCurrentViewModel.periodName.value == theCurrentViewModel.arr_dateSlot[indexPath.row] {
                if theCurrentViewModel.arr_dateSlot[indexPath.row].lowercased() == "Custom".lowercased() {
                    self.presentCalenderAction()
                    return
                }
                theCurrentViewModel.periodName.accept("")
                collectionView.deselectItem(at: indexPath, animated: false)
            } else {
                if theCurrentViewModel.arr_dateSlot[indexPath.row].lowercased() == "Custom".lowercased() {
                    self.presentCalenderAction()
                    return
                }
                if theCurrentViewModel.filterData.customRangeDate.0 != nil || theCurrentViewModel.filterData.customRangeDate.1 != nil {
                    theCurrentViewModel.filterData.customRangeDate = (nil, nil)
                    theCurrentViewModel.periodName.accept("")
                    collectionView.reloadItems(at: [IndexPath.init(row: theCurrentViewModel.arr_dateSlot.count - 1, section: 0)])
                }
                theCurrentViewModel.periodName.accept(theCurrentViewModel.arr_dateSlot[indexPath.row])
            }
        }
    }
}
