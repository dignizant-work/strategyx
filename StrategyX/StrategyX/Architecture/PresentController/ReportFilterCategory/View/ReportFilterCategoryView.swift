//
//  ReportFilterCategoryView.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportFilterCategoryView: UIView {
    
    //MARK:- Outlet ZOne
    @IBOutlet weak var btnDaily: UIButton!
    @IBOutlet weak var btnMontly: UIButton!
    @IBOutlet weak var btnWeekly: UIButton!
    @IBOutlet weak var btnCustomDate: UIButton!

    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var txtType: UITextField!

    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var viewCompnay: UIView!
    @IBOutlet weak var btnCompanyLoader: UIButton!

    
    @IBOutlet weak var viewResource: UIView!
    @IBOutlet weak var viewDepartment: UIView!
    @IBOutlet weak var txtResource: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
    @IBOutlet weak var lblFilterByTitle: UILabel!
    @IBOutlet weak var btnAssignLoader: UIButton!
    @IBOutlet weak var btnDepartmentLoader: UIButton!
    @IBOutlet weak var btnTypeLoader: UIButton!

    @IBOutlet weak var collectionWorkDate: UICollectionView!

    
    //MARK:- LifeCycle
    func setupLayout() {
        btnReset(isHidden: false)
        viewCompnay.isHidden = false
        viewDepartment.isHidden = false
        viewResource.isHidden = false
        viewType.isHidden = false
    }
    func setupDelegate(theDelegate:ReportFilterCategoryVC) {
        collectionWorkDate.registerCellNib(DateSlotCVCell.self)
        collectionWorkDate.delegate = theDelegate
        collectionWorkDate.dataSource = theDelegate
    }
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        
        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        constrainBtnResetBottom.constant = isHidden ? 0.0 : 15.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
        })
    }
    
    func updateViewAccordingTo(type:FilterType) {
       
    }
    
    func resetColor() {
        [btnDaily,btnMontly,btnWeekly,btnCustomDate].forEach { (btn) in
            btn?.borderColor = 6
            btn?.setFontColor = 5
        }
    }
    
    func selectedState(_ sender:UIButton) {
        sender.borderColor = 3
        sender.setFontColor = 3
    }
    
    func showLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(true)
    }
    
    func stopLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(false)
    }
    
    
}
