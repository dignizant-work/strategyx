//
//  WebViewVC.swift
//  StrategyX
//
//  Created by Haresh on 22/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: ParentViewController {
    
    @IBOutlet weak var btnLoader: UIButton!
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var constrainTopLayout: NSLayoutConstraint!
    
    var strURl = ""
    var isLocal:Bool = false
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setTheData(url:String, isLocal:Bool) {
        strURl = url
        self.isLocal = isLocal
    }
    
    func setupUI()  {
        constrainTopLayout.constant = UIApplication.shared.statusBarFrame.size.height
        //MARK:- Test
        print("strURl:=",strURl)
        var url = URL.init(string: strURl)
        if !isLocal {
            strURl = strURl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
            guard let urlEnd = URL.init(string: strURl) else {
                self.showAlertAtBottom(message: "URL is not valid")
                return
            }
            url = urlEnd
        } else {
            
            url = URL.init(fileURLWithPath: strURl)
        }
        guard let endURL = url else {
            self.showAlertAtBottom(message: "URL is not valid")
            return
        }
        
        webview.allowsLinkPreview = true
//        webview.interactions.allowsInlineMediaPlayback = true
//        webview.allowsPictureInPictureMediaPlayback = true

        let urlRequest = URLRequest(url: endURL)
//        webview.delegate = self
        webview.uiDelegate = self
        webview.navigationDelegate = self
        webview.load(urlRequest)

    }
    
    //MARK:- IBAction
    @IBAction func onBtnCloseAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK:- WKUIDelegate
extension WebViewVC:WKUIDelegate {
    func webViewDidClose(_ webView: WKWebView) {
        btnLoader.stopLoadyIndicator()
    }
}

//MARK:- WKNavigationDelegate
extension WebViewVC:WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        let strError = error.localizedDescription
        print("strError:=",strError)
        self.showAlertAtBottom(message: strError)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        btnLoader.startLoadyIndicator()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        btnLoader.stopLoadyIndicator()
    }
}
