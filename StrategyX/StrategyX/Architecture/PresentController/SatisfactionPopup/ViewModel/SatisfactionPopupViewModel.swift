//
//  SatisfactionPopupViewModel.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionPopupViewModel {

    weak var theController: SatisfactionPopupVC!
    var commentType = CommentType.veryHappy

    //MARK:- LifeCycle
    init(theController:SatisfactionPopupVC) {
        self.theController = theController
    }
}

//MARK:- Api Call
extension SatisfactionPopupViewModel {
    func addSatisfactionWebservice(strComment:String, completion:@escaping(Bool) -> Void)  {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_comment_type:commentType.rawValue, APIKey.key_comment:strComment]
        
        SettingsWebService.shared.addSatisfaction(parameter: parameter, success: { [weak self] (strMSg) in
            self?.theController.showAlertAtBottom(message: strMSg)
            completion(true)
        }, failed: { [weak self] (error) in
            self?.theController.showAlertAtBottom(message: error)
            completion(false)
        })
    }
}
