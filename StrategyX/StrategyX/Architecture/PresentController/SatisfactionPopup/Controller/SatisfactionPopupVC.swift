//
//  SatisfactionPopupVC.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class SatisfactionPopupVC: ParentViewController {

    //MARK:- Variable
    internal lazy var theCurrentView :SatisfactionPopupView = { [unowned self] in
       return self.view as! SatisfactionPopupView
    }()
    internal lazy var theCurrentViewModel :SatisfactionPopupViewModel = {
       return SatisfactionPopupViewModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI()  {
        theCurrentView.setupLayout()
    }
    
    //MARK:- IBAction
    @IBAction func onBtnHappyFaceAction(_ sender: UIButton) {
        theCurrentView.updateHappyFaceSelectionView(index: sender.tag)
    }
    @IBAction func onBtnSaveAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if theCurrentView.txtViewReason.text.trimmed().isEmpty {
            self.showAlertAtBottom(message: "Please enter your feedback")
            return
        }
        let comment = theCurrentView.txtViewReason.text!
        sender.startLoadyIndicator()
        sender.allButtons(isEnable: false)
        theCurrentViewModel.addSatisfactionWebservice(strComment: comment) { (isCompleted) in
            sender.stopLoadyIndicator()
            sender.allButtons(isEnable: true)
            if isCompleted {
                Utility.shared.pushData.lastFeedbackSubmitted = 0
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    

}
