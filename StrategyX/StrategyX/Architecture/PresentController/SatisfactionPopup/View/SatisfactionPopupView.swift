//
//  SatisfactionPopupView.swift
//  StrategyX
//
//  Created by Haresh on 07/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SatisfactionPopupView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    @IBOutlet var btnHappyFace:[UIButton]!
    @IBOutlet weak var lblVeryHappy: UILabel!
    @IBOutlet weak var lblHappy: UILabel!
    @IBOutlet weak var lblNotSure: UILabel!
    @IBOutlet weak var lblNotHappy: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_BottomLine: UIImageView!
    @IBOutlet weak var lblPlaceholderReason: UILabel!
    @IBOutlet weak var txtViewReason: UITextView!

    
    let disposeBag = DisposeBag()

    
    //MARK:- Life Cycle
    func setupLayout() {
        let strTitle = NSString.init(string: "How are you feeling about StrategyX ?")
        let attributeString = NSMutableAttributedString(string: "How are you feeling about StrategyX ?")
        attributeString.addAttributes([NSAttributedString.Key.font : B21, NSAttributedString.Key.foregroundColor:appBlack], range: strTitle.range(of: "\(strTitle)"))
        attributeString.addAttributes([NSAttributedString.Key.font : B21, NSAttributedString.Key.foregroundColor:appGreen], range: strTitle.range(of: "X"))
        lblTitle.attributedText = attributeString
        
        updateHappyFaceSelectionView(index: 0)
        rxReasonDidChange()
        DispatchQueue.main.async {
            self.img_BottomLine.frame.origin.x = 17.0
        }
    }
    
    //RX
    func rxReasonDidChange() {
        txtViewReason.rx.text.orEmpty
            .subscribe({ [weak self] event in
                if let obj = self {
                    obj.lblPlaceholderReason.isHidden = obj.txtViewReason.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    
    func updateHappyFaceSelectionView(index:Int) {
        btnHappyFace.forEach({$0.isSelected = false})
        [lblVeryHappy,lblHappy,lblNotSure,lblNotHappy].forEach({$0?.setFontColor = 4})
        
        var x:CGFloat = 17.0
        
        switch index {
        case 0:
            lblVeryHappy.setFontColor = 33
            x = lblVeryHappy.frame.origin.x
            btnHappyFace[0].isSelected = true
            break
        case 1:
            lblHappy.setFontColor = 33
            x = lblHappy.frame.origin.x
            btnHappyFace[1].isSelected = true
            break
        case 2:
            lblNotSure.setFontColor = 33
            x = lblNotSure.frame.origin.x
            btnHappyFace[2].isSelected = true
            break
        case 3:
            lblNotHappy.setFontColor = 33
            x = lblNotHappy.frame.origin.x
            btnHappyFace[3].isSelected = true
            break
        default:
            break
        }
        UIView.animate(withDuration: 0.2) {
            self.img_BottomLine.frame.origin.x = x
        }
    }

}
