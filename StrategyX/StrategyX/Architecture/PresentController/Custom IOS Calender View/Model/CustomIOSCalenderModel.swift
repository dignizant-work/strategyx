//
//  CustomIOSCalenderModel.swift
//  StrategyX
//
//  Created by Haresh on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class CustomIOSCalenderModel {

    //MARK:- Variable
    fileprivate weak var theController:CustomIOSCalenderVC!
    
    var selectedDate:Date = Date()
    var selectedDataOfCustom = SelectedData()
    var isForEdit = false
    
    var arr_frequency = [CalendarFrequencyType.daily.rawValue,CalendarFrequencyType.weekly.rawValue,CalendarFrequencyType.monthly.rawValue,CalendarFrequencyType.yearly.rawValue]
    let frequencyDD = DropDown()
    var frequencytype = CalendarFrequencyType.daily
    
    let monthlyOnTheDayNumberDD = DropDown()
    var monthlyOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.first
    var arr_monthlyOnTheDayNumber = [CalendarMonthlyOnTheDayNumberType.first.rawValue,CalendarMonthlyOnTheDayNumberType.second.rawValue,CalendarMonthlyOnTheDayNumberType.third.rawValue,CalendarMonthlyOnTheDayNumberType.fourth.rawValue,CalendarMonthlyOnTheDayNumberType.fifth.rawValue,CalendarMonthlyOnTheDayNumberType.last.rawValue]
    
    let monthlyOnTheDayTypeDD = DropDown()
    var monthlyOnTheDayType = CalendarMonthlyOnTheDayType.day
    var arr_monthlyOnTheDayType = [CalendarMonthlyOnTheDayType.sunday.rawValue,CalendarMonthlyOnTheDayType.monday.rawValue,CalendarMonthlyOnTheDayType.tuesday.rawValue,CalendarMonthlyOnTheDayType.wednesday.rawValue,CalendarMonthlyOnTheDayType.thursday.rawValue,CalendarMonthlyOnTheDayType.friday.rawValue,CalendarMonthlyOnTheDayType.saturday.rawValue,CalendarMonthlyOnTheDayType.day.rawValue,CalendarMonthlyOnTheDayType.weekday.rawValue,CalendarMonthlyOnTheDayType.weekendDay.rawValue]
    
    let yearlyOnTheDayNumberDD = DropDown()
    var yearlyOnTheDayNumberType = CalendarMonthlyOnTheDayNumberType.first
    var arr_yearlyOnTheDayNumber = [CalendarMonthlyOnTheDayNumberType.first.rawValue,CalendarMonthlyOnTheDayNumberType.second.rawValue,CalendarMonthlyOnTheDayNumberType.third.rawValue,CalendarMonthlyOnTheDayNumberType.fourth.rawValue,CalendarMonthlyOnTheDayNumberType.fifth.rawValue,CalendarMonthlyOnTheDayNumberType.last.rawValue]
    
    let yearlyOnTheDayTypeDD = DropDown()
    var yearlyOnTheDayType = CalendarMonthlyOnTheDayType.day
    var arr_yearlyOnTheDayType = [CalendarMonthlyOnTheDayType.sunday.rawValue,CalendarMonthlyOnTheDayType.monday.rawValue,CalendarMonthlyOnTheDayType.tuesday.rawValue,CalendarMonthlyOnTheDayType.wednesday.rawValue,CalendarMonthlyOnTheDayType.thursday.rawValue,CalendarMonthlyOnTheDayType.friday.rawValue,CalendarMonthlyOnTheDayType.saturday.rawValue,CalendarMonthlyOnTheDayType.day.rawValue,CalendarMonthlyOnTheDayType.weekday.rawValue,CalendarMonthlyOnTheDayType.weekendDay.rawValue]
    
    //MARK:- LifeCycle
    init(theController:CustomIOSCalenderVC) {
        self.theController = theController
    }
    
    func configure(dropDown:DropDown,view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 30)
    
        switch dropDown {
        case frequencyDD:
            dropDown.dataSource = arr_frequency
            break
        case monthlyOnTheDayNumberDD:
            dropDown.dataSource = arr_monthlyOnTheDayNumber
            break
        case monthlyOnTheDayTypeDD:
            dropDown.dataSource = arr_monthlyOnTheDayType
            break
        case yearlyOnTheDayNumberDD:
            dropDown.dataSource = arr_yearlyOnTheDayNumber
            break
        case yearlyOnTheDayTypeDD:
            dropDown.dataSource = arr_yearlyOnTheDayType
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }


    
}

struct SelectedData {
    
    var frequencyType:CalendarFrequencyType = .daily
    var everyDay:Int = 1
    var weeklySelectedDays:[Int] = []
    
    var monthlyIsSelectedEach:Bool = true
    var monthlySelectedDays:[Int] = []
    var monthlySelectedOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType = .first
    var monthlySelectedOnTheDayType:CalendarMonthlyOnTheDayType = .day
    
    var yearlyIsSelectedOnThe:Bool = false
    var yearlySelectedMonths:[Int] = []
    var yearlySelectedOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType = .first
    var yearlySelectedOnTheDayType:CalendarMonthlyOnTheDayType = .day
    
    init(frequencyType:CalendarFrequencyType = .daily,everyDay:Int = 1,weeklySelectedDays:[Int] = [],monthlyIsSelectedEach:Bool = true ,monthlySelectedDays:[Int] = [],monthlySelectedOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType = .first,monthlySelectedOnTheDayType:CalendarMonthlyOnTheDayType = .day,yearlyIsSelectedOnThe:Bool = false,yearlySelectedMonths:[Int] = [],yearlySelectedOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType = .first,yearlySelectedOnTheDayType:CalendarMonthlyOnTheDayType = .day) {
        
        self.frequencyType = frequencyType
        self.everyDay = everyDay
        self.weeklySelectedDays = weeklySelectedDays
        self.monthlyIsSelectedEach = monthlyIsSelectedEach
        self.monthlySelectedDays = monthlySelectedDays
        self.monthlySelectedOnTheDayNumberType = monthlySelectedOnTheDayNumberType
        self.monthlySelectedOnTheDayType = monthlySelectedOnTheDayType
        
        self.yearlyIsSelectedOnThe = yearlyIsSelectedOnThe
        self.yearlySelectedMonths = yearlySelectedMonths
        self.yearlySelectedOnTheDayNumberType = yearlySelectedOnTheDayNumberType
        self.yearlySelectedOnTheDayType = yearlySelectedOnTheDayType
    }
    
}
