//
//  CustomIOSCalenderView.swift
//  StrategyX
//
//  Created by Haresh on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CustomIOSCalenderView: ViewParentWithoutXIB {
    
    //MARK:- IBAction
    
    @IBOutlet weak var viewFrequencyType: UIView!
    @IBOutlet weak var lblFrequencyType: UILabel!
    @IBOutlet weak var txtEvery: UITextField!
    @IBOutlet weak var lblEveryType: UILabel!
    
    //Weekly
    @IBOutlet weak var viewWeekly: UIView!
    @IBOutlet var btnWeekly:[UIButton]!
    
    //Monthly
    @IBOutlet weak var viewMonthly: UIView!
    @IBOutlet weak var lblEachSelected: UILabel!
    @IBOutlet var btnMonthly:[UIButton]!
    
    @IBOutlet weak var stackViewMonthly: UIStackView!
    @IBOutlet weak var viewMonthlyOnTheDayNumber: UIView!
    @IBOutlet weak var viewMonthlyOnTheDayType: UIView!
    @IBOutlet weak var lblOntheDaySelected: UILabel!
    @IBOutlet weak var lblOntheDayNumber: UILabel!
    @IBOutlet weak var lblOntheDayType: UILabel!
    @IBOutlet weak var lastStackViewOfMonth: UIStackView!
    
    //Yearly
    
    @IBOutlet weak var btnYearlyOnTheDay: UIButton!
    @IBOutlet weak var viewYearly: UIView!
    @IBOutlet var btnYearlyMonths:[UIButton]!
    @IBOutlet weak var lblYearlyOntheDayNumber: UILabel!
    @IBOutlet weak var lblYearlyOntheDayType: UILabel!
    @IBOutlet weak var viewYearlyOnTheDayNumber: UIView!
    @IBOutlet weak var viewYearlyOnTheDayType: UIView!
    
    
    @IBOutlet weak var btnSave:UIButton!
    
    
    let disposeBag = DisposeBag()
    
    func setupLayout() {
        onTextFieldEvery()
        updateViewOfFrequnceyType(type: .daily)
        
    }
    func setMonth(numOfDays:Int) {
        if numOfDays == 28 {
            lastStackViewOfMonth.isHidden = true
        }
        for index in 0..<btnMonthly.count {
            if (index + 1) <= numOfDays {
                btnMonthly[index].setTitle("\(index + 1)", for: .normal)
                btnMonthly[index].isUserInteractionEnabled = true
            } else {
                btnMonthly[index].setTitle("", for: .normal)
                btnMonthly[index].borderColor = 0
                btnMonthly[index].borderWidth = 0
                btnMonthly[index].isUserInteractionEnabled = false
            }
        }

    }
    
    func updateViewOfFrequnceyType(type:CalendarFrequencyType) {
        viewWeekly.isHidden = true
        viewMonthly.isHidden = true
        viewYearly.isHidden = true
        switch type {
        case .daily:
            lblEveryType.text = "day(s)"
        case .weekly:
            lblEveryType.text = "week(s) on:"
            viewWeekly.isHidden = false
        case .monthly:
            lblEveryType.text = "month(s)"
            viewMonthly.isHidden = false
        case .yearly:
            lblEveryType.text = "year(s) in:"
            viewYearly.isHidden = false
        }
    }
    
    //MARK:- Daily
    func updateViewDaily() {
        
    }
    //MARK:- Weekly
    func updateViewWeekly(selectedIndex:Int) {
        
        if let index = btnWeekly.firstIndex(where: { $0.tag == selectedIndex}) {
            let selectedWeeks = btnWeekly.compactMap({$0.isSelected}).filter({$0 == true})
            btnWeekly[index].isSelected = !btnWeekly[index].isSelected
            if btnWeekly[index].isSelected {
                btnWeekly[index].borderColor = 3
                btnWeekly[index].setFontColor = 3
            } else {
                if selectedWeeks.count == 1 {
                    btnWeekly[index].isSelected = true
                    return
                }
                btnWeekly[index].borderColor = 5
                btnWeekly[index].setFontColor = 5
            }
        }
    }
    //MARK:- Monthly
    func updateViewMonthlyForDay(isForEachDay:Bool) {
        if isForEachDay {
            lblOntheDaySelected.isHidden = true
            viewMonthlyOnTheDayNumber.isUserInteractionEnabled = false
            viewMonthlyOnTheDayType.isUserInteractionEnabled = false
            lblEachSelected.isHidden = false
            stackViewMonthly.isUserInteractionEnabled = true
        } else {
            lblOntheDaySelected.isHidden = false
            viewMonthlyOnTheDayNumber.isUserInteractionEnabled = true
            viewMonthlyOnTheDayType.isUserInteractionEnabled = true
            lblEachSelected.isHidden = true
            stackViewMonthly.isUserInteractionEnabled = false
        }
    }
    func updateViewMonthlyForEachDay(selectedIndex:Int) {
        lblOntheDaySelected.isHidden = true
        viewMonthlyOnTheDayNumber.isUserInteractionEnabled = false
        viewMonthlyOnTheDayType.isUserInteractionEnabled = false
        lblEachSelected.isHidden = false
        stackViewMonthly.isUserInteractionEnabled = true
        
        if let index = btnMonthly.firstIndex(where: { $0.tag == selectedIndex}) {
            let selectedMonthDays = btnMonthly.compactMap({$0.isSelected}).filter({$0 == true})
            btnMonthly[index].isSelected = !btnMonthly[index].isSelected
            if btnMonthly[index].isSelected {
                btnMonthly[index].borderColor = 3
                btnMonthly[index].setFontColor = 3
            } else {
                if selectedMonthDays.count == 1 {
                    btnMonthly[index].isSelected = true
                    return
                }
                btnMonthly[index].borderColor = 5
                btnMonthly[index].setFontColor = 5
            }
        }
    }
    
    func updateViewMonthlyForOntheDay(selectedIndex:Int) {
        lblOntheDaySelected.isHidden = false
        viewMonthlyOnTheDayNumber.isUserInteractionEnabled = true
        viewMonthlyOnTheDayType.isUserInteractionEnabled = true
        lblEachSelected.isHidden = true
        stackViewMonthly.isUserInteractionEnabled = false
        
        if let index = btnMonthly.firstIndex(where: { $0.tag == selectedIndex}) {
            let selectedMonthDays = btnMonthly.compactMap({$0.isSelected}).filter({$0 == true})
            btnMonthly[index].isSelected = !btnMonthly[index].isSelected
            if btnMonthly[index].isSelected {
                btnMonthly[index].borderColor = 3
                btnMonthly[index].setFontColor = 3
            } else {
                if selectedMonthDays.count == 1 {
                    btnMonthly[index].isSelected = true
                    return
                }
                btnMonthly[index].borderColor = 5
                btnMonthly[index].setFontColor = 5
            }
        }
    }
    
    //MARK:- Yearly
    func updateViewYealyForDay(isForOnTheDay:Bool) {
        viewYearlyOnTheDayNumber.isUserInteractionEnabled = isForOnTheDay
        viewYearlyOnTheDayType.isUserInteractionEnabled = isForOnTheDay
    }
    func updateViewYearly(selectedIndex:Int) {
        if let index = btnYearlyMonths.firstIndex(where: { $0.tag == selectedIndex}) {
            let selectedMonthDays = btnYearlyMonths.compactMap({$0.isSelected}).filter({$0 == true})
            btnYearlyMonths[index].isSelected = !btnYearlyMonths[index].isSelected
            if btnYearlyMonths[index].isSelected {
                btnYearlyMonths[index].borderColor = 3
                btnYearlyMonths[index].setFontColor = 3
            } else {
                if selectedMonthDays.count == 1 {
                    btnYearlyMonths[index].isSelected = true
                    return
                }
                btnYearlyMonths[index].borderColor = 5
                btnYearlyMonths[index].setFontColor = 5
            }
        }
    }
    
    //MARK:- RxAction
    func onTextFieldEvery() {
//         var newString = textField.text.replacingCharacters(inRange: range, with: string)
        txtEvery.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                print("previous:=\(previous ?? "empty"), new:=\(new)")
                let num = Int(new) ?? -1
                self.btnSave.isEnabled = true
                self.btnSave.alpha = 1
                if num != -1 {
                    if num > 400 {
                        return previous ?? new
                    }
                    return new
                } else {
                    self.btnSave.isEnabled = false
                    self.btnSave.alpha = 0.5
                    return new
                }
            }
            .subscribe(txtEvery.rx.text)
            .disposed(by: disposeBag)
    }
}
