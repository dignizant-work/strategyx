//
//  CustomIOSCalenderVC.swift
//  StrategyX
//
//  Created by Haresh on 24/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class CustomIOSCalenderVC: ParentViewController {

    fileprivate lazy var theCurrentView:CustomIOSCalenderView = { [unowned self] in
       return self.view as! CustomIOSCalenderView
    }()
    
    fileprivate lazy var theCurrentModel:CustomIOSCalenderModel = {
       return CustomIOSCalenderModel(theController: self)
    }()
    
    var handlerSelectedDate:(_ selectedData:SelectedData) -> Void = {_ in }
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentModel.configure(dropDown: theCurrentModel.frequencyDD, view: theCurrentView.viewFrequencyType)
        theCurrentModel.configure(dropDown: theCurrentModel.monthlyOnTheDayNumberDD, view: theCurrentView.viewMonthlyOnTheDayNumber)
        theCurrentModel.configure(dropDown: theCurrentModel.monthlyOnTheDayTypeDD, view: theCurrentView.viewMonthlyOnTheDayType)
        theCurrentModel.configure(dropDown: theCurrentModel.yearlyOnTheDayNumberDD, view: theCurrentView.viewYearlyOnTheDayNumber)
        theCurrentModel.configure(dropDown: theCurrentModel.yearlyOnTheDayTypeDD, view: theCurrentView.viewYearlyOnTheDayType)
        
        
        let month = theCurrentModel.selectedDate.month
        let day = theCurrentModel.selectedDate.day
        let weekDay = theCurrentModel.selectedDate.weekday
        
        print("day:=\(day) month:=\(month) weekDay:=\(weekDay)")
        
        //=== for Daily ===
        theCurrentView.txtEvery.text = "\(theCurrentModel.selectedDataOfCustom.everyDay)"
        
        //=== for weekly ===
        let selectedWeekDay = theCurrentModel.selectedDataOfCustom.weeklySelectedDays.count == 0 ? [weekDay - 1]: theCurrentModel.selectedDataOfCustom.weeklySelectedDays
        for weekday in selectedWeekDay {
            theCurrentView.updateViewWeekly(selectedIndex: weekday)
        }
        //=== for monthly ===
        theCurrentView.setMonth(numOfDays: theCurrentModel.selectedDate.numOfDaysInMonth)
        let selectedDaysofMonth = theCurrentModel.selectedDataOfCustom.monthlySelectedDays.count == 0 ? [day]: theCurrentModel.selectedDataOfCustom.monthlySelectedDays
        for day in selectedDaysofMonth {
            theCurrentView.updateViewMonthlyForEachDay(selectedIndex: day)
        }
        theCurrentModel.monthlyOnTheDayNumberType = theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayNumberType
        updateMonthlyOnthDayNumberTypeViewAndModel(text: theCurrentModel.monthlyOnTheDayNumberType.rawValue)
        theCurrentModel.monthlyOnTheDayType = theCurrentModel.selectedDataOfCustom.monthlySelectedOnTheDayType
        updateMonthlyOnthDayTypeViewAndModel(text: theCurrentModel.monthlyOnTheDayType.rawValue)
        theCurrentView.updateViewMonthlyForDay(isForEachDay: theCurrentModel.selectedDataOfCustom.monthlyIsSelectedEach)

        //=== for yearly ===
        let selectedMonths = theCurrentModel.selectedDataOfCustom.yearlySelectedMonths.count == 0 ? [month]: theCurrentModel.selectedDataOfCustom.yearlySelectedMonths
        for months in selectedMonths {
            theCurrentView.updateViewYearly(selectedIndex: months)
        }
        theCurrentModel.yearlyOnTheDayNumberType = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayNumberType
        updateYearlyOnthDayNumberTypeViewAndModel(text: theCurrentModel.yearlyOnTheDayNumberType.rawValue)
        theCurrentModel.yearlyOnTheDayType = theCurrentModel.selectedDataOfCustom.yearlySelectedOnTheDayType
        updateYearlyOnthDayTypeViewAndModel(text: theCurrentModel.yearlyOnTheDayType.rawValue)
        theCurrentView.btnYearlyOnTheDay.isSelected = theCurrentModel.selectedDataOfCustom.yearlyIsSelectedOnThe
        theCurrentView.updateViewYealyForDay(isForOnTheDay: theCurrentView.btnYearlyOnTheDay.isSelected)

        
        update(text:theCurrentModel.selectedDataOfCustom.frequencyType.rawValue, dropDown: theCurrentModel.frequencyDD)

    }
    func setTheData(selectedDate:Date,customData:SelectedData) {
        theCurrentModel.selectedDate = selectedDate
        theCurrentModel.selectedDataOfCustom = customData
    }
    
    
    //MARK:- View Model
    func updateFrequencyViewAndModel(text:String) {
        theCurrentView.lblFrequencyType.text = text
        switch text.localizedLowercase {
        case CalendarFrequencyType.daily.rawValue.localizedLowercase:
            theCurrentModel.frequencytype = .daily
            break
        case CalendarFrequencyType.weekly.rawValue.localizedLowercase :
            theCurrentModel.frequencytype = .weekly
            break
        case CalendarFrequencyType.monthly.rawValue.localizedLowercase:
            theCurrentModel.frequencytype = .monthly
            break
        case CalendarFrequencyType.yearly.rawValue.localizedLowercase:
            theCurrentModel.frequencytype = .yearly
            break
        default:
            break
        }
        theCurrentView.updateViewOfFrequnceyType(type: theCurrentModel.frequencytype)
    }
    func updateMonthlyOnthDayNumberTypeViewAndModel(text:String) {
        theCurrentView.lblOntheDayNumber.text = text
        switch text.localizedLowercase {
        case CalendarMonthlyOnTheDayNumberType.first.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .first
            break
        case CalendarMonthlyOnTheDayNumberType.second.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .second
            break
        case CalendarMonthlyOnTheDayNumberType.third.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .third
            break
        case CalendarMonthlyOnTheDayNumberType.fourth.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .fourth
            break
        case CalendarMonthlyOnTheDayNumberType.fifth.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .fifth
            break
        case CalendarMonthlyOnTheDayNumberType.last.rawValue:
            theCurrentModel.monthlyOnTheDayNumberType = .last
            break
        default:
            break
        }
    }
    func updateMonthlyOnthDayTypeViewAndModel(text:String) {
        theCurrentView.lblOntheDayType.text = text
        switch text.localizedLowercase {
        case CalendarMonthlyOnTheDayType.sunday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .sunday
            break
        case CalendarMonthlyOnTheDayType.monday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .monday
            break
        case CalendarMonthlyOnTheDayType.tuesday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .tuesday
            break
        case CalendarMonthlyOnTheDayType.wednesday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .wednesday
            break
        case CalendarMonthlyOnTheDayType.thursday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .thursday
            break
        case CalendarMonthlyOnTheDayType.friday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .friday
            break
        case CalendarMonthlyOnTheDayType.saturday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .saturday
            break
        case CalendarMonthlyOnTheDayType.day.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .day
            break
        case CalendarMonthlyOnTheDayType.weekday.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .weekday
            break
        case CalendarMonthlyOnTheDayType.weekendDay.rawValue.localizedLowercase:
            theCurrentModel.monthlyOnTheDayType = .weekendDay
            break
        default:
            break
        }
    }
    
    func updateYearlyOnthDayNumberTypeViewAndModel(text:String) {
        theCurrentView.lblYearlyOntheDayNumber.text = text
        switch text.localizedLowercase {
        case CalendarMonthlyOnTheDayNumberType.first.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .first
            break
        case CalendarMonthlyOnTheDayNumberType.second.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .second
            break
        case CalendarMonthlyOnTheDayNumberType.third.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .third
            break
        case CalendarMonthlyOnTheDayNumberType.fourth.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .fourth
            break
        case CalendarMonthlyOnTheDayNumberType.fifth.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .fifth
            break
        case CalendarMonthlyOnTheDayNumberType.last.rawValue:
            theCurrentModel.yearlyOnTheDayNumberType = .last
            break
        default:
            break
        }
    }
    func updateYearlyOnthDayTypeViewAndModel(text:String) {
        theCurrentView.lblYearlyOntheDayType.text = text
        switch text.localizedLowercase {
        case CalendarMonthlyOnTheDayType.sunday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .sunday
            break
        case CalendarMonthlyOnTheDayType.monday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .monday
            break
        case CalendarMonthlyOnTheDayType.tuesday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .tuesday
            break
        case CalendarMonthlyOnTheDayType.wednesday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .wednesday
            break
        case CalendarMonthlyOnTheDayType.thursday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .thursday
            break
        case CalendarMonthlyOnTheDayType.friday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .friday
            break
        case CalendarMonthlyOnTheDayType.saturday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .saturday
            break
        case CalendarMonthlyOnTheDayType.day.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .day
            break
        case CalendarMonthlyOnTheDayType.weekday.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .weekday
            break
        case CalendarMonthlyOnTheDayType.weekendDay.rawValue.localizedLowercase:
            theCurrentModel.yearlyOnTheDayType = .weekendDay
            break
        default:
            break
        }
    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.frequencyDD:
            updateFrequencyViewAndModel(text: text)
            break
            
        case theCurrentModel.monthlyOnTheDayNumberDD:
            updateMonthlyOnthDayNumberTypeViewAndModel(text: text)
            break
        case theCurrentModel.monthlyOnTheDayTypeDD:
            updateMonthlyOnthDayTypeViewAndModel(text: text)
            break
            
        case theCurrentModel.yearlyOnTheDayNumberDD:
            updateYearlyOnthDayNumberTypeViewAndModel(text: text)
            break
        case theCurrentModel.yearlyOnTheDayTypeDD:
            updateYearlyOnthDayTypeViewAndModel(text: text)
            break
            
        default:
            break
        }
       
    }
    
    
    //MARK:- IBAction
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        self.view.endEditing(true)
        let everyDays = Int(theCurrentView.txtEvery.text!) ?? 1
        let weeklySelectedDays = theCurrentView.btnWeekly.filter({$0.isSelected == true}).map({$0.tag})
        
        let monthlyIsSelectedEach = !theCurrentView.lblEachSelected.isHidden
        let monthlySelectedDays = theCurrentView.btnMonthly.filter({$0.isSelected == true}).map({$0.tag})
        let monthlySelectedOnTheDayNumberType = theCurrentModel.monthlyOnTheDayNumberType
        let monthlySelectedOnTheDayType = theCurrentModel.monthlyOnTheDayType
        
        let yearlyIsSelectedOnthDay = theCurrentView.btnYearlyOnTheDay.isSelected
        let yearlySelectedMonth = theCurrentView.btnYearlyMonths.filter({$0.isSelected == true}).map({$0.tag})
        let yearlySelectedOnTheDayNumberType = theCurrentModel.yearlyOnTheDayNumberType
        let yearlySelectedOnTheDayType = theCurrentModel.yearlyOnTheDayType
        
        
        let selectedData = SelectedData(frequencyType: theCurrentModel.frequencytype, everyDay: everyDays, weeklySelectedDays: weeklySelectedDays, monthlyIsSelectedEach: monthlyIsSelectedEach, monthlySelectedDays: monthlySelectedDays, monthlySelectedOnTheDayNumberType: monthlySelectedOnTheDayNumberType, monthlySelectedOnTheDayType: monthlySelectedOnTheDayType, yearlyIsSelectedOnThe: yearlyIsSelectedOnthDay, yearlySelectedMonths: yearlySelectedMonth, yearlySelectedOnTheDayNumberType: yearlySelectedOnTheDayNumberType, yearlySelectedOnTheDayType: yearlySelectedOnTheDayType)
        
        print("selectedData:=",selectedData)
        handlerSelectedDate(selectedData)
        
    }
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnFrequencyTypeAction(_ sender: Any) {
        theCurrentModel.frequencyDD.show()
    }
    
    //Weekly
    @IBAction func onBtnWeeklyAction(_ sender: UIButton) {
        theCurrentView.updateViewWeekly(selectedIndex: sender.tag)
    }
    
    //Monthly
    @IBAction func onBtnMonthEachAction(_ sender: UIButton) {
        
        if sender.tag == 0 {
            // for Each
            theCurrentView.updateViewMonthlyForDay(isForEachDay: true)
        } else {
            // for On the
            theCurrentView.updateViewMonthlyForDay(isForEachDay: false)
        }
        
    }
    
    @IBAction func OnBtnMonthsDayAction(_ sender: UIButton) {
        theCurrentView.updateViewMonthlyForEachDay(selectedIndex: sender.tag)
    }
    
    @IBAction func onBtnOnTheDayNumberAction(_ sender: Any) {
        theCurrentModel.monthlyOnTheDayNumberDD.show()
    }
    
    @IBAction func onBtnOnTheDayTypeAction(_ sender: Any) {
        theCurrentModel.monthlyOnTheDayTypeDD.show()
    }
    
    
    //Yearly
    @IBAction func onBtnYearlyonTheAction(_ sender: UIButton) {
        theCurrentView.btnYearlyOnTheDay.isSelected = !theCurrentView.btnYearlyOnTheDay.isSelected
        theCurrentView.updateViewYealyForDay(isForOnTheDay: theCurrentView.btnYearlyOnTheDay.isSelected)
    }
    
    @IBAction func onBtnYearlyMonthsAction(_ sender: UIButton) {
        theCurrentView.updateViewYearly(selectedIndex: sender.tag)
    }
    
    @IBAction func onBtnYearlyOnTheDayNumberAction(_ sender: Any) {
        theCurrentModel.yearlyOnTheDayNumberDD.show()
    }
    
    @IBAction func onBtnYearlyOnTheDayTypeAction(_ sender: Any) {
        theCurrentModel.yearlyOnTheDayTypeDD.show()
    }
    
}
