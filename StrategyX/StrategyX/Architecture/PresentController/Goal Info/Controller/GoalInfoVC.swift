//
//  GoalInfoVC.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class GoalInfoVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:GoalInfoView = { [unowned self] in
       return self.view as! GoalInfoView
    }()
    fileprivate lazy var theCurrentModel:GoalInfoModel = {
       return GoalInfoModel(theController: self)
    }()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setAttributeString(textContent: "Specific – target a specific area for improvement.\n\nMeasurable – quantify or at least suggest an indicator of progress.\n\nAttainable - the goal must be able to be achieved\n\nRelevant - the goal needs to be relevant and aligned to the organisations objectives.\n\nTime-related – specify when the result(s) can be achieved.")
    }

    //MARK:-IBAction
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
