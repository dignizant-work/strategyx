//
//  GoalInfoView.swift
//  StrategyX
//
//  Created by Haresh on 30/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class GoalInfoView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }

    func setAttributeString(textContent:String) {

        lblDescription.numberOfLines = 0
        let attributeString = NSMutableAttributedString(string: textContent, attributes: [NSAttributedString.Key.font : R14,NSAttributedString.Key.foregroundColor: appPlaceHolder])
        let range1 = (textContent as NSString).range(of: "S")
        let range2 = (textContent as NSString).range(of: "M")
        let range3 = (textContent as NSString).range(of: "A")
        let range4 = (textContent as NSString).range(of: "R")
        let range5 = (textContent as NSString).range(of: "T")

        let attribute:[NSAttributedString.Key:Any] = [NSAttributedString.Key.font : R15, NSAttributedString.Key.foregroundColor : appTextBlackShedColor]
        attributeString.addAttributes(attribute, range: range1)
        attributeString.addAttributes(attribute, range: range2)
        attributeString.addAttributes(attribute, range: range3)
        attributeString.addAttributes(attribute, range: range4)
        attributeString.addAttributes(attribute, range: range5)
        
        lblDescription.attributedText = attributeString
    }
    
}
