//
//  ActionListGraphModel.swift
//  StrategyX
//
//  Created by Haresh on 26/03/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ActionListGraphModel {
    //MARK:- Variable
    fileprivate weak var theController:ActionListGraphVC!
    var topConstrain:CGFloat = 0.0
    var bottomConstrain:CGFloat = 0.0
    var chartFlagViewType = ChartFlagView.action
    var arr_ChartAction:[ChartActionList] = []
    var strChartId:String = ""
    
    init(theController:ActionListGraphVC) {
        self.theController = theController
    }
}
