//
//  RiskFilterVC.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class CriticalSuccessFilterVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CriticalSuccessFilterView = { [unowned self] in
       return self.view as! CriticalSuccessFilterView
    }()
    
    fileprivate lazy var theCurrentModel:CriticalSuccessFilterModel = {
       return CriticalSuccessFilterModel(theController: self)
    }()
    
    
    // handler
    
    var handlerOnFilterSearch:(_ filterData:FilterCreticalFectorsData) -> Void = {_ in }
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.createdByDD, view: theCurrentView.viewCreatedBy)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.categoryDD, view: theCurrentView.viewCategory)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.companyDD, view: theCurrentView.viewCompnay)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.assignedToDD, view: theCurrentView.viewAssignedTo)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.departmentDD, view: theCurrentView.viewDepartment)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.reportFrequencyDD, view: theCurrentView.viewReportFrequency)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.metricDD, view: theCurrentView.viewMetric)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.stageDD, view: theCurrentView.viewStage)
        
       

//        callInitialWebServiceAccordingToFilterType()
        setCompanyData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateStage()
    }
    
    func setTopConstraint(top:CGFloat,filterType:FilterType, isForArchiveINP:Bool = false) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
        theCurrentModel.isForArchiveINP = isForArchiveINP
    }
    func setTheData(filterData:FilterCreticalFectorsData) {
        theCurrentModel.filterData = filterData
        theCurrentView.txtSearchByCFS.text = filterData.searchByCSFText
        theCurrentView.txtSearchByReportTitle.text = filterData.searchByReportTitleText
        theCurrentModel.strSelectedCreatedByID = filterData.createdByID
        theCurrentModel.strSelectedCategoryID = filterData.categoryID
        theCurrentModel.strSelectedCompanyID = filterData.companyID
        theCurrentModel.strSelectedUserAssignID = filterData.assigntoID
        theCurrentModel.strSelectedDepartmentID = filterData.departmentID
        theCurrentModel.strSelectedReportFrequencyID = filterData.reportFrequencyID
        theCurrentModel.strSelectedMetricID = filterData.metricID
        theCurrentModel.strSelectedStage = filterData.stage
    }
    func callInitialWebServiceAccordingToFilterType() {
        
        /*
        if theCurrentModel.filterType == FilterType.focus || theCurrentModel.filterType == FilterType.approvals {
            theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            setCompanyData()
            
        } else if theCurrentModel.filterType == FilterType.myRole {
            // only for search
        } else {
//            getCategories()
//            setCompanyData()
        }*/
    }
    func assignToUserListService() {
        showLoader(theCurrentView.btnAssignLoaderOutlet)
        if theCurrentModel.filterType == .ideaAndProblem {
            assignListForINPService()
        } else {
            assignUserListService()
        }
    }
    func updateCategory() {
        var arr_Categorylist:[String] = theCurrentModel.arr_categoryList.map({$0.category_name })
        arr_Categorylist.insert("Select category", at: 0)
        theCurrentModel.categoryDD.dataSource = arr_Categorylist
//        theCurrentModel.categoryDD.dataSource = theCurrentModel.arr_categoryList.map({$0.category_name })
        
        if let index = theCurrentModel.arr_categoryList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCategoryID}) {
            theCurrentView.txtCategory.text = theCurrentModel.arr_categoryList[index].category_name
        }
    }
    func updateCompany() {
        var arr_Companylist:[String] = theCurrentModel.arr_companyList.map({$0.name })
        arr_Companylist.insert("Select company", at: 0)
        theCurrentModel.companyDD.dataSource = arr_Companylist
//        theCurrentModel.companyDD.dataSource = theCurrentModel.arr_companyList.map({$0.name })

        if let index = theCurrentModel.arr_companyList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCompanyID}) {
            theCurrentView.txtCompany.text = theCurrentModel.arr_companyList[index].name
            assignToUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
        }
    }
    func updateAssignTo() {
        var arr_AssignList:[String] = theCurrentModel.arr_assignList.map({$0.assignee_name })
        arr_AssignList.insert("Select assigned to", at: 0)
        theCurrentModel.assignedToDD.dataSource = arr_AssignList
//        theCurrentModel.assignedToDD.dataSource = theCurrentModel.arr_assignList.map({$0.assignee_name })
        if let index = theCurrentModel.arr_assignList.firstIndex(where: {$0.user_id == theCurrentModel.strSelectedUserAssignID}) {
            
            theCurrentView.txtAssignedTo.text = theCurrentModel.arr_assignList[index].assignee_name
            //            theCurrentView.txtAssignedTo.text = theCurrentModel.arr_assignList[index].assign_user_name
//            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
//            departmentListService()
        }
    
        
    }
    func updateDepartment() {
        var arr_departmentList:[String] = theCurrentModel.arr_departmentList.map({$0.name })
        arr_departmentList.insert("Select department", at: 0)
        theCurrentModel.departmentDD.dataSource = arr_departmentList
//        theCurrentModel.departmentDD.dataSource = theCurrentModel.arr_departmentList.map({$0.name })
        if let index = theCurrentModel.arr_departmentList.firstIndex(where: {$0.id == theCurrentModel.strSelectedDepartmentID}) {
            theCurrentView.txtDepartment.text = theCurrentModel.arr_departmentList[index].name
        }
    }
    func updateCreatedBy() {
        var arr_CreatedByList:[String] = theCurrentModel.arr_CreatedBy.map({$0.createrName})
        arr_CreatedByList.insert("Select created by", at: 0)
        theCurrentModel.createdByDD.dataSource = arr_CreatedByList
        
        if let index = theCurrentModel.arr_CreatedBy.firstIndex(where: {$0.id == theCurrentModel.strSelectedCreatedByID}) {
            theCurrentView.txtCreatedBy.text = theCurrentModel.arr_CreatedBy[index].createrName
        }
    }
    func updateStage() {
        let arr_Stagelist:[String] = theCurrentModel.arr_StageList.map({$0.1})
        let arr_Stagelist0:[String] = theCurrentModel.arr_StageList.map({$0.0})
//        arr_Stagelist.insert("Select Stage", at: 0)
//        theCurrentModel.stageDD.dataSource = arr_Stagelist
        //        theCurrentModel.categoryDD.dataSource = theCurrentModel.arr_categoryList.map({$0.category_name })
        
        if !theCurrentModel.strSelectedStage.isEmpty,let index = arr_Stagelist.firstIndex(where: {$0 == theCurrentModel.strSelectedStage}) {
            theCurrentView.txtStage.text = arr_Stagelist0[index]
        }
    }
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.createdByDD:
            theCurrentView.txtCreatedBy.text = text
            break
        case theCurrentModel.categoryDD:
            theCurrentView.txtCategory.text = text
            break
        case theCurrentModel.companyDD:
            theCurrentView.txtCompany.text = text
            theCurrentView.txtAssignedTo.text = ""
            theCurrentView.txtDepartment.text = ""
            theCurrentModel.strSelectedUserAssignID = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.arr_assignList = []
            theCurrentModel.arr_departmentList = []
            if !theCurrentModel.strSelectedCompanyID.isEmpty {
                assignToUserListService()
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()
            }
            break
        case theCurrentModel.assignedToDD:
            
            theCurrentView.txtAssignedTo.text = text
            /*theCurrentView.txtDepartment.text = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.arr_departmentList = []
            if !theCurrentModel.strSelectedUserAssignID.isEmpty {
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()
            }*/
            break
        case theCurrentModel.departmentDD:
            theCurrentView.txtDepartment.text = text
            break
        case theCurrentModel.reportFrequencyDD:
            theCurrentView.txtReportFrequency.text = text
            break
        case theCurrentModel.metricDD:
            theCurrentView.txtMetric.text = text
            break
        case theCurrentModel.stageDD:
            theCurrentView.txtStage.text = text
        default:
            break
        }
        theCurrentView.btnReset(isHidden: false)
    }
    
    
    //MARK:- IBAction
    
    @IBAction func onBtnStageAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.stageDD.show()
    }
    
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.theCurrentModel.strSelectedCompanyID != "" {
            if self.theCurrentModel.arr_departmentList.count != 0 {
                theCurrentModel.departmentDD.show()
            } else {
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()
            }
        } else {
             self.showAlertAtBottom(message: "Please select company first")
        }
        
    }
    
    @IBAction func onBtnCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.categoryDD.show()
    }
    @IBAction func onBtCreatedByAction(_ sender: Any) {
        self.view.endEditing(true)
        if !theCurrentModel.strSelectedCompanyID.trimmed().isEmpty {
            if self.theCurrentModel.arr_CreatedBy.count != 0{
                theCurrentModel.createdByDD.show()
            } else {
                self.createdByListService()
            }
        } else {
            self.showAlertAtBottom(message: "Please select company first")
        }
//        theCurrentModel.createdByDD.show()
    }
    @IBAction func onBtReportFrequencyAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.reportFrequencyDD.show()
        
    }
    @IBAction func onBtMetricAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.metricDD.show()
    }
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue{
            if self.theCurrentModel.strSelectedCompanyID != ""{
                if self.theCurrentModel.arr_assignList.count != 0{
                    theCurrentModel.assignedToDD.show()
                } else {
                    assignToUserListService()
                }
            } else {
                self.showAlertAtBottom(message: "Please select company first")
            }
        }
        else{
            if self.theCurrentModel.arr_assignList.count != 0 {
                theCurrentModel.assignedToDD.show()
            } else {
                assignToUserListService()
            }
        }
        
    }
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onBtnResetAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.superAdminUser == UserDefault.shared.userRole {
            if Utility.shared.userData.companyId.isEmpty {
                theCurrentModel.strSelectedCompanyID = ""
                theCurrentView.txtCompany.text = ""
            }
        }
        if theCurrentModel.filterType == .ideaAndProblem {
            theCurrentModel.filterData = FilterCreticalFectorsData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "", assigntoName: "")
        } else {
            theCurrentModel.filterData = FilterCreticalFectorsData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
        }
        
        handlerOnFilterSearch(theCurrentModel.filterData)
        /*if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue{
            theCurrentModel.filterData = FilterCreticalFectorsData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "")
            handlerOnFilterSearch(theCurrentModel.filterData)
        } else {
            handlerOnFilterSearch(FilterCreticalFectorsData.init())
        }*/
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.companyDD.show()
    }
    
    @IBAction func btnBtnSubmitAction(_ sender: Any) {
        self.view.endEditing(true)
        
        
        theCurrentModel.filterData = FilterCreticalFectorsData.init(createdByID:theCurrentModel.strSelectedCreatedByID, createdByName: theCurrentView.txtCreatedBy.text ?? "", searchByCSFText: theCurrentView.txtSearchByCFS.text ?? "", categoryID: theCurrentModel.strSelectedCategoryID, categoryName: theCurrentView.txtCategory.text ?? "", companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "", assigntoID: theCurrentModel.strSelectedUserAssignID, assigntoName: theCurrentView.txtAssignedTo.text ?? "", departmentID: theCurrentModel.strSelectedDepartmentID, departmentName: theCurrentView.txtDepartment.text ?? "", searchByReportTitleText: theCurrentView.txtSearchByReportTitle.text ?? "", reportFrequencyID: theCurrentModel.strSelectedReportFrequencyID, reportFrequencyName: theCurrentView.txtReportFrequency.text ?? "", metricID: theCurrentModel.strSelectedMetricID, metricName: theCurrentView.txtMetric.text ?? "",stage:theCurrentModel.strSelectedStage)

        handlerOnFilterSearch(theCurrentModel.filterData)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Call API
extension CriticalSuccessFilterVC {
    func getCategories() {
        showLoader(theCurrentView.btnCategoryLoaderOutlet)
        categoiesListService()
    }
    
    func getCompanyList() {
        showLoader(theCurrentView.btnCompanyLoaderOutlet)
        companylistService()
//        assignUserListService()
    }
}

//MARK:- Show Loader

extension CriticalSuccessFilterVC {
    func showLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(true)
    }
    
    func stopLoader(_ sender:UIButton?) {
        sender?.loadingIndicator(false)
    }
    
}

//MARK:- Set Company Data
extension CriticalSuccessFilterVC {
    func setCompanyData() {
        
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
            theCurrentView.txtCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompnay.isHidden = true
//            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
//            theCurrentView.viewCompnay.alpha = 0.5
            assignToUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()

            if theCurrentModel.filterType == .ideaAndProblem {
                showLoader(theCurrentView.btnCreatedByLoaderOutlet)
                createdByListService()
            }
        } else {
            if !(Utility.shared.userData.companyId.isEmpty) {
                theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
                theCurrentView.txtCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompnay.isHidden = true

//                theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
//                theCurrentView.viewCompnay.alpha = 0.5
                assignToUserListService()
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()

                if theCurrentModel.filterType == .ideaAndProblem {
                    showLoader(theCurrentView.btnCreatedByLoaderOutlet)
                    createdByListService()
                }
                return
            }
            getCompanyList()
            assignToUserListService()
        }
        
        
    }
}

//MARK:- Api Call
extension CriticalSuccessFilterVC {
    func categoiesListService() {
        let categoryFor = theCurrentModel.filterType == FilterType.risks ? CategoryFor.risk.rawValue : CategoryFor.tacticalProject.rawValue

        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_category_for:categoryFor]
        CommanListWebservice.shared.getCategories(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_categoryList = list
            self?.updateCategory()
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.categoryDD)!, view: (self?.theCurrentView.viewCategory)!)
            self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
        })
    }
    
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_companyList = list
            self?.updateCompany()
//            if let obj = self {
//            
//            }
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.companyDD)!, view: (self?.theCurrentView.viewCompnay)!)
            self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
        })
    }
    
    func assignUserListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        
        if theCurrentModel.filterType == FilterType.action {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
        } else if theCurrentModel.filterType == FilterType.focus {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_focuses
        } else if theCurrentModel.filterType == FilterType.risks {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_risks
        } else if theCurrentModel.filterType == FilterType.tacticalProject {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_tactical_projects
        } else if theCurrentModel.filterType == FilterType.approvals {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
            parameter[APIKey.key_status] = APIKey.key_assign_to_for_approval
        } else if theCurrentModel.filterType == FilterType.successFactor {
            parameter[APIKey.key_assign_to_for] = APIKey.key_critical_success_factors
        } else if theCurrentModel.filterType == FilterType.ideaAndProblem {
            parameter[APIKey.key_assign_to_for] = APIKey.key_critical_success_factors
        }
        
        
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_assignList = list
            self?.updateAssignTo()
            self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
//            if let obj = self {
//                obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.assignedToDD, view: obj.theCurrentView.viewAssignedTo)
//            }
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        AddUserWebService.shared.getCompanyDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_departmentList = list
            self?.updateDepartment()
            self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
//            if let obj = self {
//                obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.departmentDD, view: obj.theCurrentView.viewDepartment)
//            }
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.departmentDD)!, view: (self?.theCurrentView.viewDepartment)!)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
        })
    }
    
    func createdByListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        if theCurrentModel.isForArchiveINP {
            parameter[APIKey.key_grid_type] = APIKey.key_archive
        }
        IdeaAndProblemsWebServices.shared.getCreatedByList(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_CreatedBy = list
            self?.updateCreatedBy()
            self?.stopLoader(self?.theCurrentView.btnCreatedByLoaderOutlet)
            
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCreatedByLoaderOutlet)
        })
    }
    
    func assignListForINPService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        if theCurrentModel.isForArchiveINP {
            parameter[APIKey.key_grid_type] = APIKey.key_archive
        }
        IdeaAndProblemsWebServices.shared.getAssignListForINP(parameter: parameter as [String : Any], success: { [weak self] (list) in
            var assignUserList:[UserAssign] = []
            for item in list {
                let assignuser = UserAssign()
                assignuser.id = item.id
                assignuser.user_id = item.id
                assignuser.assignee_name = item.assignName
                assignUserList.append(assignuser)
//                self?.theCurrentModel.arr_assignList =
            }
            self?.theCurrentModel.arr_assignList = assignUserList
//            self?.theCurrentModel.arr_assignListForINP = list
            self?.updateAssignTo()
            self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
            
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
    }
    
    
}
