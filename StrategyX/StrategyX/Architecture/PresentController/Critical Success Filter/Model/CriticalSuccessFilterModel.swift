//
//  RiskFilterModel.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

struct FilterCreticalFectorsData {
    var searchByCSFText = ""
    var searchByReportTitleText = ""
    var createdByID = ""
    var createdByName = ""
    var categoryID = ""
    var categoryName = ""
    var companyID = ""
    var companyName = ""
    var assigntoID = ""
    var assigntoName = ""
    var departmentID = ""
    var departmentName = ""
    var reportFrequencyID = ""
    var reportFrequencyName = ""
    var metricID = ""
    var metricName = ""
    var stage = ""
    

    
    init(createdByID:String = "", createdByName:String = "", searchByCSFText:String = "",categoryID:String = "", categoryName:String = "",companyID:String = "",companyName:String = "",assigntoID:String = "",assigntoName:String = "", departmentID:String = "", departmentName:String = "",searchByReportTitleText:String = "",reportFrequencyID:String = "", reportFrequencyName:String = "",metricID:String = "", metricName:String = "", stage:String = "") {
        self.createdByID = createdByID
        self.createdByName = createdByName
        self.searchByCSFText = searchByCSFText
        self.categoryID = categoryID
        self.categoryName = categoryName
        self.companyID = companyID
        self.companyName = companyName
        self.assigntoID = assigntoID
        self.assigntoName = assigntoName
        self.departmentID = departmentID
        self.departmentName = departmentName
        self.searchByReportTitleText = searchByReportTitleText
        self.reportFrequencyID = reportFrequencyID
        self.reportFrequencyName = reportFrequencyName
        self.metricID = metricID
        self.metricName = metricName
        self.stage = stage
    }
}

class CriticalSuccessFilterModel {

    //MARK:- Variable
    fileprivate weak var theController:CriticalSuccessFilterVC!
    var topConstraint:CGFloat = 10.0
    
    var filterData:FilterCreticalFectorsData = FilterCreticalFectorsData()
    
    let createdByDD = DropDown()
    let reportFrequencyDD = DropDown()
    let metricDD = DropDown()
    let departmentDD = DropDown()
    let categoryDD = DropDown()
    let assignedToDD = DropDown()
    let companyDD = DropDown()
    let stageDD = DropDown()
    
    var filterType = FilterType.strategy
    var isForArchiveINP = false
    
    // get category
    var arr_categoryList:[Categories] = []
    var strSelectedCategoryID:String = ""
    
    // getCompany
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // getAssign
//    var arr_assignListForINP:[AssignListForINP] = []
    
    // Assign
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
    
    // CreatedBy
    var arr_CreatedBy:[CreatedByList] = []
    var strSelectedCreatedByID = ""
    
    // ReportFrequency
    var arr_ReportFrequency:[String] = ["Report Frequency 1", "Report Frequency 2", "Report Frequency 3", "Report Frequency 4", "Report Frequency 5", "Report Frequency 6", "Report Frequency 7"]
    var strSelectedReportFrequencyID = ""
    
    // Metric
    var arr_Metric:[String] = ["Metric 1","Metric 2", "Metric 3","Metric 4", "Metric 5", "Metric 6", "Metric 7", "Metric 8"]
    var strSelectedMetricID = ""
    // Stage
    let arr_StageList = Utility.shared.getStageList()
    var strSelectedStage = ""
    
    
    
    //MARK:- LifeCycle
    init(theController:CriticalSuccessFilterVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case categoryDD:
            var arr_Categorylist:[String] = arr_categoryList.map({$0.category_name })
            arr_Categorylist.insert("Select category", at: 0)
            dropDown.dataSource = arr_Categorylist
            break
        case createdByDD:
            var arr_CreatedByList:[String] = arr_CreatedBy.map({$0.createrName})
            arr_CreatedByList.insert("Select created by", at: 0)
            dropDown.dataSource = arr_CreatedBy.map({$0.createrName})
            break
        case companyDD:
            var arr_Companylist:[String] = arr_companyList.map({$0.name })
            arr_Companylist.insert("Select company", at: 0)
            dropDown.dataSource = arr_Companylist
            break
        case assignedToDD:
            var arr_AssignList:[String] = arr_assignList.map({$0.assignee_name })
            arr_AssignList.insert("Select assigned to", at: 0)
            dropDown.dataSource = arr_AssignList
            break
        case departmentDD:
            var arr_departmentList:[String] = self.arr_departmentList.map({$0.name })
            arr_departmentList.insert("Select department", at: 0)
            dropDown.dataSource = arr_departmentList
            break
            
        case reportFrequencyDD:
            var arr_Categorylist:[String] = arr_ReportFrequency
            arr_Categorylist.insert("Select report frequency", at: 0)
            dropDown.dataSource = arr_Categorylist
            break
            
        case metricDD:
            var arr_Categorylist:[String] = arr_Metric
            arr_Categorylist.insert("Select metric", at: 0)
            dropDown.dataSource = arr_Metric
            break
            
        case stageDD:
            var arr_Stageslist:[String] = arr_StageList.map({$0.0})
            arr_Stageslist.insert("Select stage", at: 0)
            dropDown.dataSource = arr_Stageslist
       
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            
            switch dropDown {
            case self.createdByDD:
                self.strSelectedCreatedByID = index == 0 ? "" : self.arr_CreatedBy[index - 1].id
                break
            case self.categoryDD:
                self.strSelectedCategoryID = index == 0 ? "" : self.arr_categoryList[index - 1].id
                break
            case self.companyDD:
                self.strSelectedCompanyID = index == 0 ? "" : self.arr_companyList[index - 1].id
                break
            case self.assignedToDD:
                self.strSelectedUserAssignID = index == 0 ? "" : self.arr_assignList[index - 1].user_id
                break
            case self.departmentDD:
                self.strSelectedDepartmentID = index == 0 ? "" : self.arr_departmentList[index - 1].id

                break
            case self.reportFrequencyDD:
//                 self.strSelectedReportFrequencyID = self.arr_categoryList[index].id
                break
            case self.metricDD:
//                 self.strSelectedMetricID = self.arr_categoryList[index].id
                break
            case self.stageDD:
                self.strSelectedStage = index == 0 ? "" : self.arr_StageList[index - 1].1
            default:
                break
            }
            self.theController.update(text: item, dropDown: dropDown)

        }
    }    
    
}


