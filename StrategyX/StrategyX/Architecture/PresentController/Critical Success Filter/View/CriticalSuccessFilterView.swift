//
//  RiskFilterView.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown


class CriticalSuccessFilterView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var btnCategoryLoaderOutlet: UIButton!
//    @IBOutlet weak var viewSearch: UIView!
//    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var viewSearchByCFS: UIView!
    @IBOutlet weak var txtSearchByCFS: UITextField!

    @IBOutlet weak var viewSearchByReportTitle: UIView!
    @IBOutlet weak var txtSearchByReportTitle: UITextField!

    @IBOutlet weak var txtCreatedBy: UITextField!
    @IBOutlet weak var viewCreatedBy: UIView!
    @IBOutlet weak var btnCreatedByLoaderOutlet: UIButton!
    
    @IBOutlet weak var txtReportFrequency: UITextField!
    @IBOutlet weak var viewReportFrequency: UIView!
    @IBOutlet weak var btnReportFrequencyLoaderOutlet: UIButton!
    
    @IBOutlet weak var txtMetric: UITextField!
    @IBOutlet weak var viewMetric: UIView!
    @IBOutlet weak var btnMetricLoaderOutlet: UIButton!
    
    @IBOutlet weak var txtDepartment: UITextField!
//    @IBOutlet weak var btnDepartmentArrow: UIButton!
    @IBOutlet weak var viewDepartment: UIView!
    
    
    @IBOutlet weak var txtCategory: UITextField!
//    @IBOutlet weak var btnCategoryArrow: UIButton!
    @IBOutlet weak var viewCategory: UIView!
    
    @IBOutlet weak var txtAssignedTo: UITextField!
//    @IBOutlet weak var btnAssignedToArrow: UIButton!
    @IBOutlet weak var viewAssignedTo: UIView!
    
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var viewCompnay: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnCompanyLoaderOutlet: UIButton!
    @IBOutlet weak var btnAssignLoaderOutlet: UIButton!
    @IBOutlet weak var btnDepartmentLoaderOutlet: UIButton!
    @IBOutlet weak var btnSelectCompanyOutlet: UIButton!
    
    @IBOutlet weak var viewStage: UIView!
    @IBOutlet weak var txtStage: UITextField!
    @IBOutlet weak var btnStageOutlet: UIButton!
    
    //MARK:- LifeCycle
    func setupLayout() {
        btnReset(isHidden: false)
    }
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        constrainBtnResetBottom.constant = isHidden ? 0.0 : 15.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
        })
    }
    func updateViewAccordingTo(type:FilterType) {
        viewStage.isHidden = true
        viewCategory.isHidden = true
        viewCreatedBy.isHidden = true
        viewSearchByReportTitle.isHidden = true
        viewReportFrequency.isHidden = true
        viewMetric.isHidden = true
        if type == .ideaAndProblem {
            viewStage.isHidden = false
            viewSearchByCFS.isHidden = true
            viewCreatedBy.isHidden = false
        }
        
    }
    

    
}
