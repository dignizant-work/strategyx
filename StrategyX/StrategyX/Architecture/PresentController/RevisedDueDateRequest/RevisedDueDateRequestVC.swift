//
//  RevisedDueDateRequestVC.swift
//  StrategyX
//
//  Created by Haresh on 01/08/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RevisedDueDateRequestVC: ParentViewController {

    //MARK:- IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewRevisedDueDate: UIView!
    @IBOutlet weak var lblReasonTitle: UILabel!
    @IBOutlet weak var constrainViewRevisedDueDateHeight: NSLayoutConstraint! // 59
    @IBOutlet weak var lblRequestDescription: UILabel!
    @IBOutlet weak var lblRevisedDueDate: UILabel!
    @IBOutlet weak var lblPlaceholderReason: UILabel!
    @IBOutlet weak var txtViewReason: UITextView!

    @IBOutlet weak var btnConfirmDecline: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    //MARK:- Variable
    var actionID = ""
    var reasonForDecline = ""
    var isForSentRequest = false
    var isForDeclineRequestFromList = false
    var isForDisplayDeclineRequestFromList = false
    
    var selectedReviseDate:Date?
    var handlerRevisedDueDate:(String,Date?,_ theModel:ActionsList?) -> Void = {_,_,_ in}
    var handlerConfirmDecline:(_ theModel:ActionsList?) -> Void = {_ in}

    let disposeBag = DisposeBag()
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setRevisedDueDate(strActionID:String,revisedDueDate:Date?, reasonForDecline:String, isForSentRequest:Bool, isForDeclineRequestFromList:Bool = false, isForDisplayDeclineRequestFromList:Bool = false) {
        actionID = strActionID
        selectedReviseDate = revisedDueDate
        self.isForSentRequest = isForSentRequest
        self.reasonForDecline = reasonForDecline
        self.isForDeclineRequestFromList = isForDeclineRequestFromList
        self.isForDisplayDeclineRequestFromList = isForDisplayDeclineRequestFromList
    }
    func setupUI() {
        btnDecline.isHidden = isForSentRequest
        btnConfirmDecline.isHidden = true
        btnSave.isHidden = false
        
        if !isForSentRequest {
            txtViewReason.text = reasonForDecline
            btnSave.setTitle("Approve", for: .normal)
            txtViewReason.isEditable = false
        }
        
        if isForDisplayDeclineRequestFromList {
            [btnSave,btnDecline,btnConfirmDecline].forEach({$0?.isHidden = true})
//            lblTitle.text = "Reason"
//            lblReasonTitle.text = ""
            constrainViewRevisedDueDateHeight.constant = 0.0
            lblRequestDescription.text = ""
            viewRevisedDueDate.isHidden = true
            lblReasonTitle.text = "Reason"
            txtViewReason.isEditable = false
            txtViewReason.text = reasonForDecline
        }
        
        rxReasonDidChange()
        setViewRevisedDueDate()

        if isForDeclineRequestFromList {
//            lblTitle.text = "Decline Reason"
            lblReasonTitle.text = "Decline Reason"
            onBtnDeclineAction(btnDecline)
        }
        
    }
    
    //RX
    func rxReasonDidChange() {
        txtViewReason.rx.text.orEmpty
            .subscribe({ [weak self] event in
                if let obj = self {
                    obj.lblPlaceholderReason.isHidden = obj.txtViewReason.text.trimmingCharacters(in: .whitespaces).isEmpty ? false : true
                }
            }).disposed(by: disposeBag)
    }
    func setViewRevisedDueDate() {
        if let date = selectedReviseDate {
            lblRevisedDueDate.text = date.string(withFormat: DateFormatterOutputType.outputType7)
        }
    }
    
    func validateForm() -> Bool {
        if selectedReviseDate == nil {
            self.showAlertAtBottom(message: "Please select revised due date")
            return  false
        } else if txtViewReason.text!.trimmed().isEmpty {
            self.showAlertAtBottom(message: "Please enter reason")
            return false
        }
        return true
    }
    
    //MARK:- RedirectTo
    func redirectToCustomDatePicker(selectionType:AddFocusViewModel.dateSelctionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        switch selectionType {
        case .due:
            break
        case .resived:
            if let date = selectedReviseDate {
                vc.setTheDate(selectedDate: date)
            }
            break
        }
        vc.handlerDate = { [weak self] (date) in
            self?.selectedReviseDate = date
            self?.setViewRevisedDueDate()
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnRevisedDueDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: .resived)
    }
    
    @IBAction func onBtnSaveAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !validateForm() {
            return
        }
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)
        
        var revisedDate = ""
        if let revisedDate1 = selectedReviseDate {
            revisedDate = revisedDate1.string(withFormat: DateFormatterInputType.inputType1)
        }
        
        var parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:actionID,
                         APIKey.key_revised_date:revisedDate]
        if isForSentRequest {
//            parameter[APIKey.key_revised_date] = revisedDate
            parameter[APIKey.key_reason_for_revise_duedate] = txtViewReason.text!.trimmed()
        } else {
            parameter[APIKey.key_respond_val] = "1"
        }
        sendRevisedDueDateRequestWebservice(parameter: parameter) { [weak self, unowned sender] (isSuccess,theModel)  in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            if isSuccess {
                if let revisedDate2 = self?.selectedReviseDate {
                    self?.handlerRevisedDueDate(revisedDate2.string(withFormat: DateFormatterOutputType.outputType7), revisedDate2, theModel)
                }
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func onBtnDeclineAction(_ sender: UIButton) {
        self.view.endEditing(true)
        constrainViewRevisedDueDateHeight.constant = 0.0
        lblRequestDescription.text = ""
        viewRevisedDueDate.isHidden = true
        btnSave.isHidden = true
        btnDecline.isHidden = true
        btnConfirmDecline.isHidden = false
        lblReasonTitle.text = "Decline Reason"
        txtViewReason.isEditable = true
        txtViewReason.text = ""
    }
    
    @IBAction func onBtnConfirmDeclineAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !validateForm() {
            return
        }
        sender.startLoadyIndicator()
        self.view.allButtons(isEnable: false)

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_action_id:actionID,
                         APIKey.key_respond_val: "0",
                         APIKey.key_decline_reason:txtViewReason.text!.trimmed()]
        
        sendRevisedDueDateRequestWebservice(parameter: parameter) { [weak self, unowned sender] (isSuccess, theModel) in
            sender.stopLoadyIndicator()
            self?.view.allButtons(isEnable: true)
            if isSuccess {
                self?.handlerConfirmDecline(theModel)
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
//MARK:- Api Called
extension RevisedDueDateRequestVC {
    func sendRevisedDueDateRequestWebservice(parameter:[String:Any], completion:@escaping(Bool, ActionsList?) -> Void) {
        ActionsWebService.shared.uploadEditActionAttachment(parameter: parameter, files: [], withName: [], withFileName: [], mimeType: [], success: { (actionList) in
            completion(true,actionList)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                completion(false, nil)
        })
    
    }
    
}
