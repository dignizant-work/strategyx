//
//  TimeLogFilterVC.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TimeLogFilterVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:TimeLogFilterView = { [unowned self] in
        return self.view as! TimeLogFilterView
        }()
    
    fileprivate lazy var theCurrentModel:TimeLogFilterModel = {
        return TimeLogFilterModel(theController: self)
    }()
    
    //MARK:- ViewLife Cycle

    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.categoryDD, view: theCurrentView.viewCategory)
        
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
    }
    
    
    func setTopConstraint(top:CGFloat,filterType:FilterType) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
       
        case theCurrentModel.categoryDD:
            theCurrentView.txtCategory.text = text
            break
       
        default:
            break
        }
        theCurrentView.btnReset(isHidden: false)
    }


    //MARK:- IBAction
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func onBtnCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.categoryDD.show()
    }  
   
    
   
    
    @IBAction func onBtnDateFromAction(_ sender: Any) {
        self.view.endEditing(true)
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = Date()
        if theCurrentView.txtDateTo.text != ""
        {
            vc.isSetMaximumDate = true
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = "dd-MM-yyyy"
            vc.maximumDate = dateFormaater.date(from: theCurrentView.txtDateTo.text!)!
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = "dd-MM-yyyy"
                self?.theCurrentView.txtDateFrom.text = dateFormaater.string(from: date)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnBtnDateToAction(_ sender: Any) {
        self.view.endEditing(true)
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        if theCurrentView.txtDateFrom.text != ""
        {
            vc.isSetMinimumDate = true
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = "dd-MM-yyyy"
            vc.minimumDate = dateFormaater.date(from: theCurrentView.txtDateFrom.text!)!
        }
        else
        {
            vc.isSetMinimumDate =  true
            vc.minimumDate = Date()
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = "dd-MM-yyyy"
                self?.theCurrentView.txtDateTo.text = dateFormaater.string(from: date)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
}


