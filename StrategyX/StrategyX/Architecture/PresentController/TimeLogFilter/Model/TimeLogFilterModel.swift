//
//  TimeLogFilterModel.swift
//  StrategyX
//
//  Created by Jaydeep on 26/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TimeLogFilterModel {
    
    //MARK:- Variable
    weak var theController:TimeLogFilterVC!
    var topConstraint:CGFloat = 10.0
    
    let categoryDD = DropDown()
   
    
    
    var filterType = FilterType.strategy
    
    
    //MARK:- LifeCycle
    init(theController:TimeLogFilterVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {        
        case categoryDD:
            dropDown.dataSource = ["Category1","Category2","Category3"]
            break
        
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
}
