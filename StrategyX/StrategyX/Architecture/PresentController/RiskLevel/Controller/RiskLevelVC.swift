//
//  RiskLevelVC.swift
//  StrategyX
//
//  Created by Haresh on 19/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RiskLevelVC: ParentViewController {
    
    fileprivate lazy var theCurrentView:RiskLevelView = {
       return self.view as! RiskLevelView
    }()
    
    internal lazy var theCurrentViewModel:RiskLevelViewModel = {
       return RiskLevelViewModel(theContoller: self)
    }()
    
    var handlerOnButtonLevelClick:(String) -> Void = {_ in}

    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setTheData(imgBG:UIImage?, selectedLevel:String) {
        theCurrentViewModel.imageBG = imgBG
        theCurrentViewModel.selectedLevel = selectedLevel
    }
    
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentViewModel.imageBG)
        if !theCurrentViewModel.selectedLevel.isEmpty {
            theCurrentView.configure(strLevel: theCurrentViewModel.selectedLevel)
        }
    }
    
    //MARK:- IBAction
    @IBAction func onBtnLevelAction(_ sender:UIButton) {
        if let title = sender.titleLabel?.text {
            handlerOnButtonLevelClick(title)
            theCurrentViewModel.selectedLevel = title
            theCurrentView.configure(strLevel: theCurrentViewModel.selectedLevel)
        }
    }
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
