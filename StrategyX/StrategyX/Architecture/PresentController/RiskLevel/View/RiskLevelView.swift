//
//  RiskLevelView.swift
//  StrategyX
//
//  Created by Haresh on 19/06/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RiskLevelView: ViewParentWithoutXIB {
    //MARK:- IBOutlet

    @IBOutlet weak var viewMainOuter: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblRare: UILabel!
    @IBOutlet weak var lblUnlikely: UILabel!
    @IBOutlet weak var lblModerate: UILabel!
    @IBOutlet weak var lblLikely: UILabel!
    @IBOutlet weak var lblAlmostCertain: UILabel!
    @IBOutlet var btnLevel:[UIButton]!
    @IBOutlet weak var img_Bg: UIImageView!
    
    @IBOutlet weak var heightOfBottomAssestment: NSLayoutConstraint!
    @IBOutlet weak var ViewOfBottomAssestment: UIView!
    @IBOutlet weak var viewOfLastAssetment: UIView!

    func setupLayout(imgBG: UIImage?) {
//        img_Bg.image = imgBG
        let frame = topView.frame
        
        lblRare.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblRare.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblUnlikely.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblUnlikely.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblModerate.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblModerate.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblLikely.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblLikely.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        lblAlmostCertain.transform = CGAffineTransform(rotationAngle: CGFloat(-90 / 180.0 * CGFloat.pi))
        lblAlmostCertain.frame = CGRect(x:4, y:4, width:frame.width - 8, height:frame.height - 8)
        
        if UIScreen.main.bounds.width == 320 {
            viewOfLastAssetment.isHidden = true
            ViewOfBottomAssestment.isHidden = false
        } else {
            ViewOfBottomAssestment.isHidden = true
            heightOfBottomAssestment.constant = 0
        }
    }
    
    func configure(strLevel:String) {
        btnLevel.forEach({
            $0.setBackGroundColor = 0
            if $0.titleLabel?.text == strLevel {$0.setBackGroundColor = 11}
        })
    }
}
