//
//  CalenderViewVC.swift
//  StrategyX
//
//  Created by Jaydeep on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import FSCalendar

class CalenderViewVC: UIViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:CalendarView = { [unowned self] in
        return self.view as! CalendarView
    }()
    
    fileprivate lazy var theCurrentModel:CalenderModel = {
        return CalenderModel(theController: self)
    }()
    
    var handlerSelectedDates:([Date]) -> Void = {_ in}
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // setSideMenuLeftNavigationBarItem()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("height:=\(theCurrentView.calendar!.frame)")
    }
    
    func setupUI() {
        setLogoOnNavigationHeader()
        theCurrentView.setupLayout(imgBG: theCurrentModel.imageBG)
        setTheCalendar(calendar: theCurrentView.calendar)
        theCurrentView.setCalenderDelegate(theDelegate: self)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.monthDD, view: theCurrentView.viewMonth)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.yearDD, view: theCurrentView.viewYear)
        //theCurrentModel.configureDropDown(dropDown: theCurrentModel.reoccuringDD, view: theCurrentView.viewReoccuring)
        /*let dates = [
         theCurrentModel.gregorian.date(byAdding: .day, value: -1, to: Date()),
         Date(),
         theCurrentModel.gregorian.date(byAdding: .day, value: 1, to: Date())
         ]
         dates.forEach { (date) in
         theCurrentView.calendar.select(date, scrollToDate: false)
         }*/
        
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == Date().monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[0])
        
        if let sDate = theCurrentModel.customRangeDate.0,let lDate = theCurrentModel.customRangeDate.1 {
            theCurrentModel.firstDate = sDate
            theCurrentModel.lastDate = lDate
            let range = datesRange(from: sDate, to: lDate)
            
            for d in range {
                theCurrentView.calendar.select(d)
            }
            
            theCurrentModel.datesRange = range
            theCurrentView.calendar.reloadData()
            configureMultipleVisibleCells()
        }
       
    }
    func setSelectedDateRange(firstDate:Date?,lastDate:Date?) {
        theCurrentModel.customRangeDate = (firstDate,lastDate)
    }
    func setTheData(imgBG:UIImage?) {
        theCurrentModel.imageBG = imgBG
    }
    
    func setTheCalendar(calendar:FSCalendar) {
        theCurrentModel.calenderView = calendar
    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.monthDD:
            theCurrentView.updateDroDownTextFor(month: text)
            break
        case theCurrentModel.yearDD:
            theCurrentView.updateDroDownTextFor(year: text)
            break
        case theCurrentModel.reoccuringDD:
            break
        default:
            break
        }
        updatePageOfCalender()
    }
    
    func updatePageOfCalender() {
        let year = theCurrentView.lblYear.text!.trimmed()
        var month = theCurrentView.lblMonth.text!.trimmed()
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == month.localizedLowercase}) {
            month = "\(index + 1)"
            if month.count == 1 {
                month = "0" + month
            }
        }
        
        let createDate = year + "-" + month + "-" + "01"
        print(createDate)
        
        if let date = createDate.convertToUTCDateWithGMT0() {
            let localdate = date.toLocalTime()
            theCurrentView.setCurrentPageOfCalender(date: localdate)
        }
    }
    /*
    func calculateDaysBetweenTwoDates(for component:Calendar.Component, from start: Date, to end: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: component, in: .era, for: start) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: component, in: .era, for: end) else {
            return 0
        }
        return end - start
    }
    func getNextDates(on date:Date,monthlyOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType,everyWeekDay:Int) -> [String] {
        var previousDate = date
        var makeDates:[String] = []
        
        switch monthlyOnTheDayNumberType {
        case CalendarMonthlyOnTheDayNumberType.first:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                    makeDates.append(theCurrentModel.dateFormatter.string(from: firstDate))
                }
            }
            break
        case CalendarMonthlyOnTheDayNumberType.second:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                //                previousDate = previousDate.addDays(previousDate.weekday - everyWeekDay)
                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                    previousDate = firstDate.addDays(7)
                    makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                }
            }
            break
        case CalendarMonthlyOnTheDayNumberType.third:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                //                previousDate = previousDate.addDays(previousDate.weekday - everyWeekDay)
                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                    previousDate = firstDate.addDays(14)
                    makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                }
            }
            break
        case CalendarMonthlyOnTheDayNumberType.fourth:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                //                previousDate = previousDate.addDays(previousDate.weekday - everyWeekDay)
                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                    previousDate = firstDate.addDays(21)
                    makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                }
            }
            break
        case CalendarMonthlyOnTheDayNumberType.fifth:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                //                previousDate = previousDate.addDays(previousDate.weekday - everyWeekDay)
                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                    previousDate = firstDate.addDays(28)
                    makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                }
            }
            break
        case CalendarMonthlyOnTheDayNumberType.last:
            if everyWeekDay >= 1 && everyWeekDay <= 7 {
                makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
            }
            break
        }
        
        return makeDates
    }
    
    
    func addEventForDaily(on date:Date, everyDay:Int) {
        let endDate = theCurrentView.calendar.maximumDate
        //        let currentYear = date.year
        
        var makeDates:[String] = []
        let totalCountDays = Int((Double(calculateDaysBetweenTwoDates(for: .day, from:  date, to: endDate))/Double(everyDay)).rounded())
        
        var previousDate = date
        for _ in 0...totalCountDays {
            previousDate = previousDate.addDays(everyDay)
            makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
        }
        print("makeDates:=",makeDates)
        theCurrentModel.arr_datesWithEvent = makeDates
    }
    
    func addEventForWeekly(on date:Date, everyWeek:Int,weekDay:[Int]) {
        let endDate = theCurrentView.calendar.maximumDate
        //        let currentYear = date.year
        
        var makeDates:[String] = []
        let totalCountDays = Int((Double(calculateDaysBetweenTwoDates(for: .day, from: date, to: endDate))/Double(everyWeek * 7)).rounded())
        //        let weekDaySymbol = Calendar.current.weekdaySymbols // ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        //        let firstWeekDay = Calendar.current.firstWeekday // 1
        
        
        var previousDate = date
        for _ in 0...totalCountDays {
            for i in 0..<weekDay.count {
                let dayOfWeek = weekDay[i]
                if dayOfWeek >= previousDate.weekday {
                    previousDate = previousDate.addDays(dayOfWeek - previousDate.weekday)
                    makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                }
            }
            
            previousDate = previousDate.endOfWeek.addDays(((everyWeek - 1) * 7) + 1)
        }
        print("makeDates:=",makeDates)
        theCurrentModel.arr_datesWithEvent = makeDates
    }
    
    func addEventForMonthly(on date:Date, everyMonth:Int,weekDays:[Int],isForEachMonthDay:Bool, monthlyOnTheDayNumberType:CalendarMonthlyOnTheDayNumberType, monthlyOnTheDayType:CalendarMonthlyOnTheDayType) {
        let endDate = theCurrentView.calendar.maximumDate
        //        let currentYear = date.year
        
        var makeDates:[String] = []
        let totalCountDays = Int((Double(calculateDaysBetweenTwoDates(for: .day, from: date, to: endDate))/Double(everyMonth)).rounded())
        //        let weekDaySymbol = Calendar.current.weekdaySymbols // ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        //        let firstWeekDay = Calendar.current.firstWeekday // 1
        
        
        var previousDate = date
        if isForEachMonthDay {
            for _ in 0...totalCountDays {
                for i in 0..<weekDays.count {
                    let dayOfMonth = weekDays[i]
                    if dayOfMonth >= previousDate.day {
                        previousDate = previousDate.addDays(dayOfMonth - previousDate.day)
                        makeDates.append(theCurrentModel.dateFormatter.string(from: previousDate))
                    }
                }
                for _ in 0..<everyMonth {
                    previousDate = previousDate.endOfMonth().addDays(1)
                }
            }
        } else {
            
            var everyWeekDay = 1
            
            //            previousDate = previousDate.startOfMonth()
            for _ in 0...totalCountDays {
                //                let month = previousDate
                
                switch monthlyOnTheDayType {
                case CalendarMonthlyOnTheDayType.sunday:
                    everyWeekDay = 1
                    break
                case CalendarMonthlyOnTheDayType.monday:
                    everyWeekDay = 2
                    break
                case CalendarMonthlyOnTheDayType.tuesday:
                    everyWeekDay = 3
                    break
                case CalendarMonthlyOnTheDayType.wednesday:
                    everyWeekDay = 4
                    break
                case CalendarMonthlyOnTheDayType.thursday:
                    everyWeekDay = 5
                    break
                case CalendarMonthlyOnTheDayType.friday:
                    everyWeekDay = 6
                    break
                case CalendarMonthlyOnTheDayType.saturday:
                    everyWeekDay = 7
                    break
                case CalendarMonthlyOnTheDayType.day:
                    
                    break
                case CalendarMonthlyOnTheDayType.weekday:
                    everyWeekDay = 9
                    break
                case CalendarMonthlyOnTheDayType.weekendDay:
                    everyWeekDay = 10
                    break
                }
                
                switch monthlyOnTheDayNumberType {
                case CalendarMonthlyOnTheDayNumberType.first:
                    let second = previousDate.startOfMonth()
                    if previousDate > second {
                        previousDate = previousDate.endOfMonth().addDays(1)
                    }
                    break
                case CalendarMonthlyOnTheDayNumberType.second:
                    let second = previousDate.startOfMonth().addDays(2 * 7)
                    if previousDate > second {
                        previousDate = previousDate.endOfMonth().addDays(1)
                    }
                    break
                case CalendarMonthlyOnTheDayNumberType.third:
                    let second = previousDate.startOfMonth().addDays(3 * 7)
                    if previousDate > second {
                        previousDate = previousDate.endOfMonth().addDays(1)
                    }
                    break
                case CalendarMonthlyOnTheDayNumberType.fourth:
                    let second = previousDate.startOfMonth().addDays(4 * 7)
                    if previousDate < second {
                        previousDate = previousDate.endOfMonth().addDays(1)
                    }
                    break
                case CalendarMonthlyOnTheDayNumberType.fifth:
                    let second = previousDate.startOfMonth().addDays(5 * 7)
                    if previousDate > second {
                        previousDate = previousDate.endOfMonth().addDays(1)
                    }
                    break
                case CalendarMonthlyOnTheDayNumberType.last:
                    let index = previousDate.endOfMonth().day
                    previousDate = previousDate.endOfMonth().startOfWeek.addDays(index - everyWeekDay)
                    //                    previousDate = second
                    
                    break
                }
                
                
                //                if let firstDate = previousDate.weekDayOfDate(everyWeekDay) {
                //                    if firstDate < date {
                //                        previousDate = previousDate.endOfMonth().addDays(everyWeekDay)
                //                    }
                //                }
                
                for item in getNextDates(on: previousDate, monthlyOnTheDayNumberType: monthlyOnTheDayNumberType, everyWeekDay: everyWeekDay) {
                    makeDates.append(item)
                }
                for _ in 0..<everyMonth {
                    previousDate = previousDate.endOfMonth().addDays(1)
                }
            }
        }
        
        print("makeDates:=",makeDates)
        theCurrentModel.arr_datesWithEvent = makeDates
    }
    
    func addEvent(on date:Date,selectedData:SelectedData) {
        
        switch selectedData.frequencyType {
        case .daily:
            addEventForDaily(on: date, everyDay: selectedData.everyDay)
            break
        case .weekly:
            addEventForWeekly(on: date, everyWeek: selectedData.everyDay, weekDay: selectedData.weeklySelectedDays)
            break
        case .monthly:
            addEventForMonthly(on: date, everyMonth: selectedData.everyDay, weekDays: selectedData.monthlySelectedDays, isForEachMonthDay: selectedData.monthlyIsSelectedEach, monthlyOnTheDayNumberType: selectedData.monthlySelectedOnTheDayNumberType, monthlyOnTheDayType: selectedData.monthlySelectedOnTheDayType)
            break
        case .yearly:
            
            break
        }
        
        theCurrentView.calendar.reloadData()
    }*/
    
    //MARK:- Redirection
    func redirectToDatePicker(selectedDate:Date) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        //        vc.isSetMinimumDate =  true
        //        vc.minimumDate = selectedDate
        vc.isSetDate = true
        vc.setDateValue = selectedDate
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "hh:mm a"
                self?.theCurrentView.lblTime.text = dateFormater.string(from: date)
//                self?.updateSelectedDate(strTime: dateFormater.string(from: date))
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    func redirectToDatePicker() {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = Date()
        vc.isSetDate = false
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormaater = DateFormatter()
                dateFormaater.dateFormat = "HH:mm"
                self?.theCurrentView.txtDuration.text = dateFormaater.string(from: date)
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    /*func presentTimePickerVC() {
        let vc = TimePickerVC.init(nibName: "TimePickerVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerTime = { [weak self] (time) in
            self?.theCurrentView.lblTime.text = time
        }
        
        self.present(vc, animated: true, completion: nil)
    }*/
    
    //MARK:- Private
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnMonthAction(_ sender: Any) {
        theCurrentModel.monthDD.show()
    }
    
    @IBAction func onBtnYearAction(_ sender: Any) {
        theCurrentModel.yearDD.show()
    }
    
    @IBAction func onBtnReoccuringAction(_ sender: Any) {
        theCurrentModel.reoccuringDD.show()
    }
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        if let dateRange = theCurrentModel.datesRange,dateRange.count > 1 {
            handlerSelectedDates(dateRange)
        } else {
            self.showAlertAtBottom(message: "Please select start and end date")
            return
        }
        onBtnCloseAction(sender)
    }
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
        //        handlerClose()
        self.dismiss(animated: false, completion: nil)
      //  self.backAction(isAnimation: false)
    }
    
    @IBAction func onBtnDurationAction(_ sender: Any) {
        //        redirectToDatePicker()
    }
    
    @IBAction func onBtnTimeAction(_ sender: Any) {
        redirectToDatePicker(selectedDate: theCurrentModel.selectedTime)
    }
    
}

//MARK:- FSCalendarDelegate
extension CalenderViewVC:FSCalendarDelegate {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let page = calendar.currentPage.monthName()
        print("Month:=\(page),  Year:=\(calendar.currentPage.year)")
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == calendar.currentPage.monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        if let index = theCurrentModel.arr_year.firstIndex(where: {$0.localizedLowercase == "\(calendar.currentPage.year)".localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[index])
        }
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if theCurrentModel.firstDate == nil {
            theCurrentModel.firstDate = date
            theCurrentModel.datesRange = [theCurrentModel.firstDate!]
            configureVisibleCells()
            print("datesRange contains: \(theCurrentModel.datesRange!)")
            
            return
        }
        
        // only first date is selected:
        if theCurrentModel.firstDate != nil && theCurrentModel.lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= theCurrentModel.firstDate! {
                calendar.deselect(theCurrentModel.firstDate!)
                theCurrentModel.lastDate = theCurrentModel.firstDate!
                theCurrentModel.firstDate = date
                //theCurrentModel.datesRange = [theCurrentModel.firstDate!]
                let range = datesRange(from: theCurrentModel.firstDate!, to: theCurrentModel.lastDate!)
                
                
                for d in range {
                    calendar.select(d)
                }
                theCurrentModel.datesRange = range
                configureMultipleVisibleCells()
                print("datesRange contains: \(theCurrentModel.datesRange!)")
                
                return
            }
            
           // let range = theCurrentModel.datesRange(from)
            
            let range = datesRange(from: theCurrentModel.firstDate!, to: date)
            
            theCurrentModel.lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
           
            theCurrentModel.datesRange = range            
            configureMultipleVisibleCells()
            print("datesRange contains: \(theCurrentModel.datesRange!)")
            
            return
        }
        
        // both are selected:
        if theCurrentModel.firstDate != nil && theCurrentModel.lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            theCurrentModel.lastDate = nil
            theCurrentModel.firstDate = nil
            
            theCurrentModel.datesRange = []
            if theCurrentModel.firstDate == nil {
                theCurrentModel.firstDate = date
                theCurrentModel.datesRange = [theCurrentModel.firstDate!]
                calendar.select(date)
                print("datesRange contains: \(theCurrentModel.datesRange!)")
                configureVisibleCells()
                return
            }
           // print("datesRange contains: \(theCurrentModel.datesRange!)")
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if theCurrentModel.firstDate != nil && theCurrentModel.lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            theCurrentModel.lastDate = nil
            theCurrentModel.firstDate = nil
            
            theCurrentModel.datesRange = []
            print("datesRange contains: \(theCurrentModel.datesRange!)")
        }
        if theCurrentModel.firstDate == nil {
            theCurrentModel.firstDate = date
            theCurrentModel.datesRange = [theCurrentModel.firstDate!]
            calendar.select(date)
            print("datesRange contains: \(theCurrentModel.datesRange!)")
            configureVisibleCells()
            return
        }
    }
    
}
//MARK:- FSCalendarDelegateAppearance
extension CalenderViewVC:FSCalendarDelegateAppearance {
    
}

//MARK:- FSCalendarDataSource
extension CalenderViewVC:FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "DIYCalendarCell", for: date, at: position)
        cell.titleLabel.textColor = appBlack
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
   /* func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if theCurrentModel.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }*/
    
    
    
   /* func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        let year = Int(theCurrentModel.arr_year.last ?? "\(Date().addYears(20))")!
        let lastDate = ("\(year)-12-31").convertToUTCDateWithGMT0()!
        print("Maximum:=\(lastDate)")
        return lastDate
    }*/
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        let dateString = theCurrentModel.dateFormatter.string(from: date)
        
       /* if theCurrentModel.arr_datesWithEvent.contains(dateString) {
            return 1
        }
        
        if theCurrentModel.arr_datesWithMultipleEvents.contains(dateString) {
            return 3
        }*/
        
        return 0
    }
    
    /*func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }*/
    
    /*
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
       // print("did select date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
      //  print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }*/
    
   /* func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }*/
    
    // MARK: - Private functions
    
    private func configureVisibleCells() {
        theCurrentView.calendar.visibleCells().forEach { (cell) in
            let date = theCurrentView.calendar.date(for: cell)
            let position = theCurrentView.calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    
    func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as? DIYCalendarCell)
        // Custom today circle
        diyCell?.contentView.backgroundColor = .white
        if let img = diyCell?.circleImageView
        {
           img.isHidden = !theCurrentModel.gregorian.isDateInToday(date)
        }
        
        // Configure selection layer
      //  if position == .current {
            
            var selectionType = SelectionType.none
           /// print("selected Date \(theCurrentView.calendar.selectedDates)")
            if theCurrentView.calendar.selectedDates.contains(date) {
                let previousDate = theCurrentModel.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = theCurrentModel.gregorian.date(byAdding: .day, value: 1, to: date)!
                if theCurrentView.calendar.selectedDates.contains(date) {
                    if theCurrentView.calendar.selectedDates.contains(previousDate) && theCurrentView.calendar.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if theCurrentView.calendar.selectedDates.contains(previousDate) && theCurrentView.calendar.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if theCurrentView.calendar.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                if let cell = cell as? DIYCalendarCell
                {
                    cell.selectionLayer?.isHidden = true
                }
                return
            }
            if let cell = cell as? DIYCalendarCell
            {
                cell.selectionLayer?.isHidden = false
                cell.selectionType = selectionType
            }
            
        /*} else {
            if let cell = cell as? DIYCalendarCell{
                cell.circleImageView?.isHidden = true
                cell.selectionLayer?.isHidden = true
            }
           
        }*/
    }
    private func configureMultipleVisibleCells() {
        theCurrentView.calendar.visibleCells().forEach { (cell) in
            let date = theCurrentView.calendar.date(for: cell)
            let position = theCurrentView.calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
}
