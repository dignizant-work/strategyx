//
//  CalendarView.swift
//  StrategyX
//
//  Created by Jaydeep on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarView: ViewParentWithoutXIB {
    
    
    //MARK:- IBOutlet
    @IBOutlet weak var img_Bg: UIImageView!
    
    @IBOutlet weak var viewMonth: UIView!
    @IBOutlet weak var lblMonth: UILabel!
    
    @IBOutlet weak var viewYear: UIView!
    @IBOutlet weak var lblYear: UILabel!
    
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var viewReoccuring: UIView!
    @IBOutlet weak var lblReoccuring: UILabel!
    
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var txtDuration: UITextField!
    
    @IBOutlet weak var lblTime: UILabel!
    
    
    //MARK:- LifeCycle
    func setupLayout(imgBG:UIImage?) {
        img_Bg.image = imgBG
        setupCalender()
    }
    
    func setupCalender() {
        calendar.headerHeight = 0.0
        calendar.weekdayHeight = 40.0
       // calendar.appearance.eventDefaultColor = .white
    //    calendar.appearance.eventSelectionColor = .white
        calendar.appearance.weekdayTextColor = clear
        calendar.appearance.titleDefaultColor = appBlack
       // calendar.appearance.titlePlaceholderColor = appBlack
        //calendar.appearance.subtitlePlaceholderColor  = appBlack
        calendar.appearance.selectionColor = appGreen
        calendar.appearance.titleSelectionColor = appBlack
        calendar.appearance.titleFont = R14
        
        //        calendar.appearance.todayColor = appGreen
       
     //   calendar.placeholderType = .fillHeadTail
        calendar.today = nil
       // calendar.select(Date())
        calendar.firstWeekday = 2
        
        calendar.allowsMultipleSelection = true
      //  calendar.allowsSelection = true
        
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "DIYCalendarCell")
        
    }
    
    func setCalenderDelegate(theDelegate:CalenderViewVC) {
        calendar.delegate = theDelegate
        calendar.dataSource = theDelegate
    }
    
    func setCurrentPageOfCalender(date:Date) {
        //        unit = (calendarView.scope == FSCalendarScope.month) ? FSCalendarUnit.month : FSCalendarUnit.weekOfYear
        //        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarView.currentPage)
        //        calendarView.setCurrentPage(previousMonth!, animated: true)
        
        calendar.setCurrentPage(date, animated: true)
    }
    
    func updateDroDownTextFor(month:String) {
        lblMonth.text = month
    }
    
    func updateDroDownTextFor(year:String) {
        lblYear.text = year
    }
    
    func updateDroDownTextFor(reoccuring:String) {
        lblReoccuring.text = reoccuring
    }
}
