//
//  CalenderModel.swift
//  StrategyX
//
//  Created by Jaydeep on 05/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import FSCalendar

class CalenderModel {
    
    //MARK:- Variable
    weak var theController:CalenderViewVC!
    var imageBG:UIImage?
    var calenderView:FSCalendar?
    
    var arr_month:[String] = []
    var arr_year:[String] = []
    var arr_reoccuring:[String] = []
    var arr_datesWithEvent:[String] = [] // yyyy-MM-dd
    var arr_datesWithMultipleEvents:[String] = [] // yyyy-MM-dd
    
    let monthDD = DropDown()
    let yearDD = DropDown()
    let reoccuringDD = DropDown()
    
    var selectedTime = Date()

    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    var customRangeDate:(Date?,Date?) = (nil,nil)

    // first date in the range
    var firstDate: Date?
    // last date in the range
    var lastDate: Date?
    
    var datesRange: [Date]?
    
    let gregorian = Calendar(identifier: .gregorian)
    
   /* var startDate: Date? {
        didSet {
            startDate = startDate?.startOfDay
        }
    }
    var endDate: Date? {
        didSet {
            endDate = endDate?.endOfDay
        }
    }
    var selectedDateArray: [Date] = []
    {
        didSet { // sort the array
            selectedDateArray = calenderView!.selectedDates.sorted()
        switch selectedDateArray.count
        {
            case 0:
                startDate = nil
                endDate = nil
            case 1:
                startDate = selectedDateArray.first
                endDate = nil
            case _ where selectedDateArray.count > 1:
                startDate = selectedDateArray.first
                endDate = selectedDateArray.last
                
            var nextDay = Calendar.current.date(byAdding: .day, value: 1, to: startDate!)
            while nextDay!.startOfDay <= endDate!
            {
                calenderView!.select(nextDay)
                nextDay = Calendar.current.date(byAdding: .day, value: 1, to: nextDay!)
            }
            default: return
            }
        }
        
    }*/
    
    //MARK:- LifeCycle
    init(theController:CalenderViewVC) {
        self.theController = theController
        arr_month = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        let currentYear = Date().year
        for i in 0..<21 {
            arr_year.append("\(currentYear + i)")
        }
        arr_reoccuring = [CalendarReoccuring.everyDay.rawValue,CalendarReoccuring.everyMonth.rawValue,CalendarReoccuring.everyWeek.rawValue,CalendarReoccuring.everyYear.rawValue,CalendarReoccuring.custom.rawValue]
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        print("point:=",point)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 30)
        
        
        switch dropDown {
        case monthDD:
            dropDown.dataSource = arr_month
            break
        case yearDD:
            dropDown.dataSource = arr_year
            break
        case reoccuringDD:
            dropDown.dataSource = arr_reoccuring
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
    
    
    
}
