//
//  CustomDateTimePikerVC.swift
//  StrategyX
//
//  Created by Haresh on 19/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown
import FSCalendar

class CustomDateTimePikerVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:CustomDateTimePikerView = { [unowned self] in
       return self.view as! CustomDateTimePikerView
    }()
    fileprivate lazy var theCurrentModel:CustomDateTimePikerModel = {
        return CustomDateTimePikerModel(theController: self)
    }()
    
    var handlerDate:(Date) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("height:=\(theCurrentView.calendar!.frame)")
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.setCalenderDelegate(theDelegate: self)
        theCurrentView.calendar.select(theCurrentModel.selectedDate)

        theCurrentModel.configureDropDown(dropDown: theCurrentModel.monthDD, view: theCurrentView.viewMonth)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.yearDD, view: theCurrentView.viewYear)
        
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == theCurrentModel.selectedDate.monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        if let index = theCurrentModel.arr_year.firstIndex(where: {$0.localizedLowercase == "\(theCurrentModel.selectedDate.year)".localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[index])
        }
        theCurrentModel.dateFormatter.dateFormat = "hh:mm a"
        theCurrentView.updateStartTime(time: theCurrentModel.dateFormatter.string(from: theCurrentModel.selectedDate))
    }
    
    func setTheDate(selectedDate:Date) {
        theCurrentModel.selectedDate = selectedDate
    }
    
    func updateSelectedDate(strTime:String)  {
        theCurrentModel.dateFormatter.dateFormat = DateFormatterInputType.inputType2
        let date = theCurrentModel.dateFormatter.string(from: theCurrentModel.selectedDate) + " " + strTime
        print("newDate:=",date)
        theCurrentModel.dateFormatter.dateFormat = DateFormatterOutputType.outputType3
        if let dateAdd = theCurrentModel.dateFormatter.date(from: date) {
            theCurrentModel.selectedDate = dateAdd
        }

    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.monthDD:
            theCurrentView.updateDroDownTextFor(month: text)
            break
        case theCurrentModel.yearDD:
            theCurrentView.updateDroDownTextFor(year: text)
            break
        default:
            break
        }
        updatePageOfCalender()
    }
    
    func updatePageOfCalender() {
        let year = theCurrentView.lblYear.text!.trimmed()
        var month = theCurrentView.lblMonth.text!.trimmed()
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == month.localizedLowercase}) {
            month = "\(index + 1)"
            if month.count == 1 {
                month = "0" + month
            }
        }
        
        let createDate = year + "-" + month + "-" + "01"
        print(createDate)
        
        if let date = createDate.convertToUTCDateWithGMT0() {
            let localdate = date.toLocalTime()
            theCurrentView.setCurrentPageOfCalender(date: localdate)
        }
    }
    
    //MARK:- Redirection
    func redirectToDatePicker(selectedDate:Date) {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
//        vc.isSetMinimumDate =  true
//        vc.minimumDate = selectedDate
        vc.isSetDate = true
        vc.setDateValue = selectedDate
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "hh:mm a"
                self?.theCurrentView.txtStartTime.text = dateFormater.string(from: date)
                self?.updateSelectedDate(strTime: dateFormater.string(from: date))
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    /*func presentTimePickerVC(selectedDate:Date) {
        let vc = TimePickerVC.init(nibName: "TimePickerVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.selectedDate = selectedDate
        vc.handlerTime = { [weak self] (time) in
            self?.theCurrentView.txtStartTime.text = time
            self?.updateSelectedDate(strTime: time)
        }
        
        self.present(vc, animated: true, completion: nil)
    }*/
    
    
    //MARK:- IBAction
    @IBAction func onBtnMonthAction(_ sender: Any) {
        theCurrentModel.monthDD.show()
    }
    
    @IBAction func onBtnYearAction(_ sender: Any) {
        theCurrentModel.yearDD.show()
    }
    
    @IBAction func onBtnStartTimeAction(_ sender: Any) {
        
        redirectToDatePicker(selectedDate: theCurrentModel.selectedDate)
    }
    
    @IBAction func onBtnSaveAction(_ sender: Any) {
        if let date = theCurrentView.calendar.selectedDate {
            let newdate = date.string(withFormat: DateFormatterInputType.inputType2) + " " + (theCurrentView.txtStartTime.text ?? "")
            if let convertedDate = newdate.convertToDate(formate: DateFormatterOutputType.outputType3) {
                handlerDate(convertedDate)
                onBtnCloseAction("")
            }
        }
    }
    
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    

}
//MARK:- FSCalendarDelegate
extension CustomDateTimePikerVC:FSCalendarDelegate {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let page = calendar.currentPage.monthName()
        print("Month:=\(page),  Year:=\(calendar.currentPage.year)")
        if let index = theCurrentModel.arr_month.firstIndex(where: {$0.localizedLowercase == calendar.currentPage.monthName().localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(month: theCurrentModel.arr_month[index])
        }
        if let index = theCurrentModel.arr_year.firstIndex(where: {$0.localizedLowercase == "\(calendar.currentPage.year)".localizedLowercase }) {
            theCurrentView.updateDroDownTextFor(year: theCurrentModel.arr_year[index])
        }
    }
}
//MARK:- FSCalendarDelegateAppearance
extension CustomDateTimePikerVC:FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        print("bounds:=\(bounds)")
    }
}

//MARK:- FSCalendarDataSource
extension CustomDateTimePikerVC:FSCalendarDataSource {
//    func minimumDate(for calendar: FSCalendar) -> Date {
//        return Date()
//    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        let year = Int(theCurrentModel.arr_year.last ?? "\(Date().addYears(20))")!
        let lastDate = ("\(year)-12-31").convertToUTCDateWithGMT0()!
        print("Maximum:=\(lastDate)")
        return lastDate
    }
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = theCurrentModel.dateFormatter.string(from: date)
        
        if theCurrentModel.arr_datesWithEvent.contains(dateString) {
            return 1
        }
        
        if theCurrentModel.arr_datesWithMultipleEvents.contains(dateString) {
            return 3
        }
        
        return 0
    }
}
