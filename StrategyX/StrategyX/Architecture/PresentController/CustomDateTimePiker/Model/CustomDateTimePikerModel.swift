//
//  CustomDateTimePikerModel.swift
//  StrategyX
//
//  Created by Haresh on 19/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class CustomDateTimePikerModel {

    //MARK:- Variable
    fileprivate weak var theController:CustomDateTimePikerVC!
    var selectedDate = Date()
    
    var arr_month:[String] = []
    var arr_year:[String] = []
    var arr_reoccuring:[String] = []
    var arr_datesWithEvent:[String] = [] // yyyy-MM-dd
    var arr_datesWithMultipleEvents:[String] = [] // yyyy-MM-dd
    
    let monthDD = DropDown()
    let yearDD = DropDown()
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    //MARK:- LifeCycle
    init(theController:CustomDateTimePikerVC) {
        self.theController = theController
        arr_month = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        let currentYear = selectedDate.year
        for i in 0..<21 {
            arr_year.append("\(currentYear + i)")
        }
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        print("point:=",point)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 30)
        
        
        switch dropDown {
        case monthDD:
            dropDown.dataSource = arr_month
            break
        case yearDD:
            dropDown.dataSource = arr_year
            break
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
    
}
