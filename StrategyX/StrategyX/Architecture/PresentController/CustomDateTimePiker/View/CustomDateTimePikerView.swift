//
//  CustomDateTimePikerView.swift
//  StrategyX
//
//  Created by Haresh on 19/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import FSCalendar

class CustomDateTimePikerView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var viewMonth: UIView!
    @IBOutlet weak var lblMonth: UILabel!
    
    @IBOutlet weak var viewYear: UIView!
    @IBOutlet weak var lblYear: UILabel!
    
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var txtStartTime: UITextField!
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        setupCalender()
    }
    
    func setupCalender() {
        calendar.headerHeight = 0.0
        calendar.weekdayHeight = 40.0
        
        calendar.appearance.weekdayTextColor = clear
        calendar.appearance.titleDefaultColor = appBlack
        calendar.appearance.titlePlaceholderColor = appPlaceHolder
        calendar.appearance.selectionColor = appGreen
        calendar.appearance.titleSelectionColor = white
        calendar.appearance.titleFont = R14
        //        calendar.appearance.todayColor = appGreen
        
        calendar.placeholderType = .none
        calendar.today = nil
//        calendar.select(Date())
        calendar.firstWeekday = 2
    }
    
    func setCalenderDelegate(theDelegate:CustomDateTimePikerVC) {
        calendar.delegate = theDelegate
        calendar.dataSource = theDelegate
    }
    
    func setCurrentPageOfCalender(date:Date) {
        //        unit = (calendarView.scope == FSCalendarScope.month) ? FSCalendarUnit.month : FSCalendarUnit.weekOfYear
        //        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarView.currentPage)
        //        calendarView.setCurrentPage(previousMonth!, animated: true)
        
        calendar.setCurrentPage(date, animated: true)
    }
    
    func updateDroDownTextFor(month:String) {
        lblMonth.text = month
    }
    
    func updateDroDownTextFor(year:String) {
        lblYear.text = year
    }
    
    func updateStartTime(time:String) {
        txtStartTime.text = time
    }

}
