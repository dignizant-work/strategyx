//
//  ReportFilterVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class ReportFilterVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:ReportFilterView = { [unowned self] in
        return self.view as! ReportFilterView
        }()
    
    fileprivate lazy var theCurrentModel:ReportFilterModel = {
        return ReportFilterModel(theController: self)
    }()
    
    //MARK:- ViewLife Cycle
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout(reportType:theCurrentModel.reportByType)
        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.resourceDD, view: theCurrentView.viewResource)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.departmentDD, view: theCurrentView.viewDepartment)
        
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
    }
    
    
    func setTopConstraint(top:CGFloat,filterType:FilterType, reportByType:ReportsSelectionBy) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
        theCurrentModel.reportByType = reportByType
    }
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
            
        case theCurrentModel.resourceDD:
            theCurrentView.txtResource.text = text
            break
          
        case theCurrentModel.departmentDD:
            theCurrentView.txtDepartment.text = text
            break
            
        default:
            break
        }
//        theCurrentView.btnReset(isHidden: false)
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnResetAction(_ sender: Any) {
        btnBtnBackAction(sender)
    }
    @IBAction func onBtnSubmitAction(_ sender: Any) {
        btnBtnBackAction(sender)
    }
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnResourcesAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.resourceDD.show()
    }
    
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.departmentDD.show()
    }
    
    
    
    
    
}
