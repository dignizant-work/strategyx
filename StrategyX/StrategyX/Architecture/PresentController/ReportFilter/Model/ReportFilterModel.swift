//
//  ReportFilterVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class ReportFilterModel {
    
    //MARK:- Variable
    weak var theController:ReportFilterVC!
    var topConstraint:CGFloat = 10.0
    
    let resourceDD = DropDown()
    let departmentDD = DropDown()
    var filterType = FilterType.strategy
    var reportByType = ReportsSelectionBy.department
    
    
    //MARK:- LifeCycle
    init(theController:ReportFilterVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case resourceDD:
            dropDown.dataSource = ["Resource1","Resource2","Resource3"]
            break
        case departmentDD:
            dropDown.dataSource = ["Department1","Department2","Department3"]
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
}
