//
//  ReportFilterView.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportFilterView: UIView {
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var viewResource: UIView!
    @IBOutlet weak var viewDepartment: UIView!
    @IBOutlet weak var txtResource: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
   
    
    //MARK:- LifeCycle
    func setupLayout(reportType:ReportsSelectionBy) {
        btnReset(isHidden: false)
        viewDepartment.isHidden = true
        viewResource.isHidden = true
        if reportType == .department {
            viewDepartment.isHidden = false
        } else {
            viewResource.isHidden = false
        }
    }
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        
        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        constrainBtnResetBottom.constant = isHidden ? 0.0 : 15.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
        })
    }
    
    func updateViewAccordingTo(type:FilterType) {
       
    }
    
    
    
}
