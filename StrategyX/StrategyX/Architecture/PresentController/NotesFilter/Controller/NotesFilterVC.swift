//
//  NotesFilterVC.swift
//  StrategyX
//
//  Created by Haresh on 10/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotesFilterVC: ParentViewController {
    //MARK:- Variable
    enum dateSelectionType:Int {
        case created             = 1
        case due                 = 2
        case work                = 3
    }
    fileprivate lazy var theCurrentView:NotesFilterView = { [unowned self] in
        return self.view as! NotesFilterView
        }()
    
    fileprivate lazy var theCurrentModel:NotesFilterModel = {
        return NotesFilterModel(theController: self)
    }()
    
    // handler
    var handlerOnFilterSearch:(_ filterData:FilterNotesData) -> Void = {_ in }

    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
        
        if let workDate = theCurrentModel.filterData.createdDate {
            theCurrentView.txtCreatedDate.text = workDate.string(withFormat: DateFormatterOutputType.outPutType4)
        }
    }
    
    func setTopConstraint(top:CGFloat) {
        theCurrentModel.topConstraint = top
    }
    
    func setTheData(filterData:FilterNotesData) {
        theCurrentModel.filterData = filterData
        theCurrentView.txtSearch.text = filterData.searchText
    }
    
    func updateDueDate(strDate:String,dateSelectionType:dateSelectionType,date:Date) {
        switch dateSelectionType {
        case .created:
            theCurrentModel.selectedCreatedDate = date
            theCurrentModel.filterData.createdDate = date
            theCurrentView.txtCreatedDate.text = strDate

            //            theCurrentModel.selectedData["createdate"].stringValue = strDate
            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 9, section: 0)], with: UITableView.RowAnimation.none)
            break
        case .due:
//            theCurrentModel.selectedCreatedDate = date
//            //            theCurrentModel.selectedData["duedate"].stringValue = strDate
//            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 10, section: 0)], with: UITableView.RowAnimation.none)
//            theCurrentModel.filterData.dueDate = date
//            theCurrentView.txtDueDate.text = strDate
            break
        case .work:
//            theCurrentModel.selectedWorkDate = date
//            theCurrentModel.filterData.workDate = date
//
            //            theCurrentModel.selectedData["workdate"].stringValue = strDate
            //            self.theCurrentView.tableView.reloadRows(at: [IndexPath.init(row: 11, section: 0)], with: UITableView.RowAnimation.none)
//            theCurrentView.txtWorkDate.text = strDate
            break
        }
    }
    
    //MARK:- redirect To
    func redirectToCustomDatePicker(selectionType:dateSelectionType) {
        let vc = CustomDateTimePikerVC.init(nibName: "CustomDateTimePikerVC", bundle: nil)
        /*if theCurrentModel.isEditFocus {
         if let reviseDate = theCurrentModel.selectedData["revisedduedate"].string, !reviseDate.isEmpty, let date = reviseDate.convertToDate(formate: DateFormatterOutputType.outPutType4) {
         vc.setTheDate(selectedDate: date)
         }
         }*/
        switch selectionType {
        case .created:
            if let workDate = theCurrentModel.filterData.createdDate {
                vc.setTheDate(selectedDate: workDate)
            }
            break
        case .work:
//            if let workDate = theCurrentModel.filterData.workDate {
//                vc.setTheDate(selectedDate: workDate)
//            }
            break
        case .due:
//            if let dueDate = theCurrentModel.filterData.dueDate {
//                vc.setTheDate(selectedDate: dueDate)
//            }
            break
            
        }
        
        vc.handlerDate = { [weak self] (date) in
            let dateFormaater = DateFormatter()
            dateFormaater.dateFormat = DateFormatterOutputType.outputType7
            //                self?.theCurrentModel.selectedData["duedate"].stringValue = dateFormaater.string(from: date)
            self?.updateDueDate(strDate: dateFormaater.string(from: date), dateSelectionType: selectionType, date: date)
            
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnCreatedDateAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToCustomDatePicker(selectionType: dateSelectionType.created)
    }
    
    
    @IBAction func onBtnResetAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue {
            theCurrentModel.filterData = FilterNotesData.init()
            handlerOnFilterSearch(theCurrentModel.filterData)
        } else {
            handlerOnFilterSearch(FilterNotesData.init())
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSubmitFilterAction(_ sender: Any) {
        self.view.endEditing(true)
        
        let filterDatanew = theCurrentModel.filterData
        theCurrentModel.filterData = FilterNotesData.init(searchText: theCurrentView.txtSearch.text ?? "",createdDate:filterDatanew.createdDate)
        handlerOnFilterSearch(theCurrentModel.filterData)
        self.dismiss(animated: true, completion: nil)
    }

}
