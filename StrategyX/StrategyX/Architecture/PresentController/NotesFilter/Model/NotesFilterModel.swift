//
//  NotesFilterModel.swift
//  StrategyX
//
//  Created by Haresh on 10/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
struct FilterNotesData {
    var searchText = ""
    var createdDate:Date? = nil
    
    
    init(searchText:String = "",createdDate:Date? = nil) {
        self.searchText = searchText
        self.createdDate = createdDate
    }
    
    
}
class NotesFilterModel {
    //MARK:- Variable
    fileprivate weak var theController:NotesFilterVC!
    var topConstraint:CGFloat = 10.0
    var selectedCreatedDate:Date?

    var filterData:FilterNotesData = FilterNotesData()

    //MARK:- LifeCycle
    init(theController:NotesFilterVC) {
        self.theController = theController
    }
}
