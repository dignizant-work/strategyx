//
//  NotesFilterView.swift
//  StrategyX
//
//  Created by Haresh on 10/04/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class NotesFilterView: ViewParentWithoutXIB {

    // MARK:- IBOutlet
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0

    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var txtCreatedDate:UITextField!
    @IBOutlet weak var viewCreatedDate: UIView!
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        
    }

}
