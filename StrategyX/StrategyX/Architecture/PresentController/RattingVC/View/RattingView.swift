//
//  RattingView.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RattingView: ViewParentWithoutXIB {

    //MARK:- Outlet ZOne
    
    @IBOutlet weak var viewRatting: HCSStarRatingView!
    
    
    func setupLayout() {
        viewRatting.allowsHalfStars = false
        viewRatting.continuous = true
        viewRatting.minimumValue = 0
        viewRatting.maximumValue = 5
    }

}
