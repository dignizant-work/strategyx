//
//  RattingVC.swift
//  StrategyX
//
//  Created by Jaydeep on 07/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class RattingVC: ParentViewController {
    
    //MARK:- Variable
    fileprivate lazy var theCurrentView:RattingView = { [unowned self] in
        return self.view as! RattingView
        }()
    
    fileprivate lazy var theCurrentModel:RattingModel = {
        return RattingModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
    }
    
    func validateForm() -> Bool {
        self.view.endEditing(true)
        let rating = theCurrentView.viewRatting.value
        
        if rating == 0 {
            self.showAlertAtBottom(message: "Please rate us, we need your support!")
            return false
        }
        return true
    }
}

//MARK:- Action Zone

extension RattingVC {
    @IBAction func btnCancelAction(_ sender:Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitAction(_ sender:UIButton) {
        if !validateForm() {
            return
        }
        
        sender.loadingIndicator(true)
        self.view.allButtons(isEnable: false)
        
        let feedbackType = Int(theCurrentView.viewRatting.value)

        let parameter = [APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_rating_star:feedbackType] as [String : Any]
        SettingsWebService.shared.userRating(parameter: parameter, success: { [weak self] (msg) in
                sender.loadingIndicator(false)
                self?.view.allButtons(isEnable: true)
                self?.btnCancelAction("")

            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                sender.loadingIndicator(false)
                self?.view.allButtons(isEnable: true)
        })
    }
    
}
