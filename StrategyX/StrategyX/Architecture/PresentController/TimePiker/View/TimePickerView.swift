//
//  TimePickerView.swift
//  StrategyX
//
//  Created by Haresh on 02/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TimePickerView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var txtMinute: UITextField!
    @IBOutlet weak var txtHours: UITextField!
    @IBOutlet weak var viewTimeType: UIView!
    @IBOutlet weak var lblTimeType: UILabel!
    @IBOutlet weak var lblErrorMessage: UILabel!
    
    let disposeBag = DisposeBag()
    
    
    //MARK:- LifeCycle
    func setupLayout() {
        setErrorMessage(strMsg: "")
        
        //Rx Action
        onTextHours()
        onTextMinute()

    }
    
    func setErrorMessage(strMsg:String) {
        lblErrorMessage.text = strMsg
    }
    
    func validateTime() -> Bool {
        setErrorMessage(strMsg: "")
        let hours = Int(txtHours.text ?? "-1") ?? -1
        let minute = Int(txtMinute.text ?? "-1") ?? -1
        
        if (hours > 0 && hours <= 12) && (minute >= 0 && minute <= 59)  {
          return true
        }
        setErrorMessage(strMsg: "Enter valid time")
        return false
    }
    
    //MARK:- RxAction
    func onTextHours() {
        txtHours.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                print("previous:=\(previous ?? "empty"), new:=\(new)")
                let num = Int(new) ?? -1
                if self.txtHours.text!.count > 2 {
                     return previous ?? new
                }
                if num != -1 {
                    if num > 11 {
                        return previous ?? new
                    }
                    return new
                } else {
                    return new
                }
            }
            .subscribe(txtHours.rx.text)
            .disposed(by: disposeBag)
    }
    
    func onTextMinute() {
        txtMinute.rx.text.orEmpty
            .scan("") { (previous, new) -> String in
                print("previous:=\(previous ?? "empty"), new:=\(new)")
                let num = Int(new) ?? -1
                if self.txtHours.text!.count > 2 {
                    return previous ?? new
                }
                if num != -1 {
                    if num > 59 {
                        return previous ?? new
                    }
                    return new
                } else {
                    return new
                }
            }
            .subscribe(txtMinute.rx.text)
            .disposed(by:disposeBag)
    }

}
