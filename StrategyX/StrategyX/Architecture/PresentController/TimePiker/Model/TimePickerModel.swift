//
//  TimePickerModel.swift
//  StrategyX
//
//  Created by Haresh on 02/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TimePickerModel {

    //MARK:- Variable
    weak var theController:TimePickerVC!
    
    var timeTypeDD = DropDown()
    let arr_TimeType = ["AM","PM"]
    
    
    //LifeCycle
    init(theController:TimePickerVC) {
        self.theController = theController
    }
    
    func configure(dropDown:DropDown,view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        let point = view.convert(CGPoint.zero, to: theController.view.superview)
        dropDown.bottomOffset = CGPoint(x: point.x, y: 30)
        
        switch dropDown {
        case timeTypeDD:
            dropDown.dataSource = arr_TimeType
            break
        default:
            break
        }
        dropDown.selectionAction = { [unowned self] (index,item) in
            print("selected item: \(item) at index \(index)")
            self.theController.update(text: item, dropDown: dropDown)
        }
    }
}
