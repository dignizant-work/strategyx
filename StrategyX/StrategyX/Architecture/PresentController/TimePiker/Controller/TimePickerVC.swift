//
//  TimePickerVC.swift
//  StrategyX
//
//  Created by Haresh on 02/02/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class TimePickerVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:TimePickerView = { [unowned self] in
        return self.view as! TimePickerView
    }()
    fileprivate lazy var theCurrentModel:TimePickerModel = {
        return TimePickerModel(theController: self)
    }()
    
    lazy var dateFormater: DateFormatter = {
        return DateFormatter()
    }()
    
    var selectedDate = Date()
    
    var handlerTime:(String) -> Void = {_ in}
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentModel.configure(dropDown: theCurrentModel.timeTypeDD, view: theCurrentView.viewTimeType)
        
        dateFormater.dateFormat = "hh mm a"
        let arr_time = dateFormater.string(from: selectedDate).components(separatedBy: " ")
        print("arr_time:=",arr_time)
        update(hours: arr_time[0], minute: arr_time[1], timeType: arr_time[2])
        
    }
    
    
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.timeTypeDD:
            theCurrentView.lblTimeType.text = text
            break
            
        default:
            break
        }
        
    }
    func update(hours:String,minute:String, timeType:String) {
        theCurrentView.txtHours.text = hours
        theCurrentView.txtMinute.text = minute
        theCurrentView.lblTimeType.text = timeType
    }
    
    //MARK:- Redirection
    func redirectToDatePicker() {
        let vc = CommonDatePickerVC.init(nibName: "CommonDatePickerVC", bundle: nil)
        vc.isSetMinimumDate =  true
        vc.minimumDate = selectedDate
        vc.isSetDate = true
        vc.setDateValue = selectedDate
        vc.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.handlerSelected = { [weak self] (date,type)    in
            if type == 1
            {
                self?.dateFormater.dateFormat = "hh mm a"
                if let arr_time = self?.dateFormater.string(from: date).components(separatedBy: " ") {
                    print("arr_time:=",arr_time)
                    self?.update(hours: arr_time[0], minute: arr_time[1], timeType: arr_time[2])
                }
                
            }
            
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    //MARK:- IBAction
    @IBAction func onBtnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnOkAction(_ sender: Any) {
        self.view.endEditing(true)
        if theCurrentView.validateTime() {
            let time = theCurrentView.txtHours.text! + ":" + theCurrentView.txtMinute.text! + " " + theCurrentView.lblTimeType.text!
            handlerTime(time)
            onBtnCancelAction("")
        }
    }
    
    @IBAction func onBtnTimePickerAction(_ sender: Any) {
        self.view.endEditing(true)
        redirectToDatePicker()
    }
    
    @IBAction func onBtnTimeTypeAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.timeTypeDD.show()
    }
    
}
