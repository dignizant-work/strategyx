//
//  ReportActionSelectionVC.swift
//  StrategyX
//
//  Created by Haresh on 19/07/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit

class ReportActionSelectionVC: UIViewController {

    @IBOutlet weak var lblCompletedAction: UILabel!
    @IBOutlet weak var lblNotCompletedAction: UILabel!
    
    @IBOutlet weak var lblSelectedText: UILabel!
    var completedAction = 0
    var notCompletedAction = 0

    var handleSelection:(Int) -> Void = {_ in}
    var selectedIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCompletedAction.text = "\(completedAction) action(s) completed"
        lblNotCompletedAction.text = "\(notCompletedAction) action(s) not completed"
    }
    
    func setTheData(comletedAction:Int, notCompletedAction:Int) {
        self.completedAction = comletedAction
        self.notCompletedAction = notCompletedAction
    }
    

    //MARK:- redirection
    func presentDropDownListVC() {
        let vc = DropDownListVC.init(nibName: "DropDownListVC", bundle: nil)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        vc.setTheData(strTitle: "",arr:["Move to next report","Mark as 100% complete"])
        
        vc.hanlderSelectedData = { [weak self] (strData, index) in
            self?.lblSelectedText.text = strData
            self?.selectedIndex = index
        }
        self.present(vc, animated: true, completion: nil)
//        AppDelegate.shared.presentOnWindow(vc: vc)
    }
    
    //MARK:- IBAction
    
    @IBAction func onBtnSelectionAction(_ sender: Any) {
        presentDropDownListVC()
    }
    @IBAction func onBtnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnCompleteAction(_ sender: Any) {
        handleSelection(selectedIndex)
        onBtnCloseAction("")
    }
}
