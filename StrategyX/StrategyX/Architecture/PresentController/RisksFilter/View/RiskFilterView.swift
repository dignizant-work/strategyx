//
//  RiskFilterView.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown


class RiskFilterView: ViewParentWithoutXIB {

    //MARK:- IBOutlet
    
    @IBOutlet weak var btnCategoryLoaderOutlet: UIButton!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
//    @IBOutlet weak var btnDepartmentArrow: UIButton!
    @IBOutlet weak var viewDepartment: UIView!
    
    @IBOutlet weak var txtCategory: UITextField!
//    @IBOutlet weak var btnCategoryArrow: UIButton!
    @IBOutlet weak var viewCategory: UIView!
    
    @IBOutlet weak var txtAssignedTo: UITextField!
//    @IBOutlet weak var btnAssignedToArrow: UIButton!
    @IBOutlet weak var viewAssignedTo: UIView!
    
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var viewCompnay: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var constrainBtnResetHeight: NSLayoutConstraint! // 35.0
    @IBOutlet weak var constrainBtnResetBottom: NSLayoutConstraint! // 15.0
    
    @IBOutlet weak var constrainViewTop: NSLayoutConstraint! // 10.0
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnCompanyLoaderOutlet: UIButton!
    @IBOutlet weak var btnAssignLoaderOutlet: UIButton!
    @IBOutlet weak var btnDepartmentLoaderOutlet: UIButton!
    @IBOutlet weak var btnSelectCompanyOutlet: UIButton!
    
    //MARK:- LifeCycle
    func setupLayout() {
        btnReset(isHidden: false)
    }
    func btnReset(isHidden:Bool) {
        btnReset.isHidden = isHidden
        constrainBtnResetHeight.constant = isHidden ? 0.0 : 35.0
        constrainBtnResetBottom.constant = isHidden ? 0.0 : 15.0
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: { [weak self] (finished) in
            self?.btnReset.isHidden = isHidden
        })
    }
    func updateViewAccordingTo(type:FilterType) {
        viewSearch.isHidden = false
        viewDepartment.isHidden = false
        viewCategory.isHidden = false
        viewAssignedTo.isHidden = false
        if type == FilterType.risks {
            viewCategory.isHidden = true
        }
        if type == FilterType.focus || type == FilterType.approvals {
            viewSearch.isHidden = true
            viewCategory.isHidden = true
        } else if type == FilterType.myRole {
            viewCompnay.isHidden = true
            viewDepartment.isHidden = true
            viewCategory.isHidden = true
            viewAssignedTo.isHidden = true
        }
    }
    

    
}
