//
//  RiskFilterModel.swift
//  StrategyX
//
//  Created by Haresh on 19/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

struct FilterData {
    var searchText = ""
    var searchTag = ""
    var categoryID = ""
    var categoryName = ""
    var companyID = ""
    var companyName = ""
    var assigntoID = ""
    var assigntoName = ""
    var departmentID = ""
    var departmentName = ""
    var relatedToValue:Int = -1
    var relatedToName = ""
    var workDate:String? = nil
    var dueDate:String? = nil

    
    init(searchText:String = "",searchTag:String = "",categoryID:String = "", categoryName:String = "",companyID:String = "",companyName:String = "",assigntoID:String = "",assigntoName:String = "", departmentID:String = "", departmentName:String = "", relatedToValue:Int = -1, relatedToName:String = "", workDate:String? = nil,dueDate:String? = nil) {
        self.searchText = searchText
        self.searchTag = searchTag
        self.categoryID = categoryID
        self.categoryName = categoryName
        self.companyID = companyID
        self.companyName = companyName
        self.assigntoID = assigntoID
        self.assigntoName = assigntoName
        self.departmentID = departmentID
        self.departmentName = departmentName
        self.workDate = workDate
        self.dueDate = dueDate
        self.relatedToValue = relatedToValue
        self.relatedToName = relatedToName
        
    }
    

}

class RiskFilterModel {

    //MARK:- Variable
    fileprivate weak var theController:RiskFilterVC!
    var topConstraint:CGFloat = 10.0
    
    var filterData:FilterData = FilterData()
    
    let departmentDD = DropDown()
    let categoryDD = DropDown()
    let assignedToDD = DropDown()
    let companyDD = DropDown()
    
    var filterType = FilterType.strategy
    
    // get category
    
    var arr_categoryList:[Categories] = []
    var strSelectedCategoryID:String = ""
    
    // getCompany
    
    var arr_companyList:[Company] = []
    var strSelectedCompanyID:String = ""
    
    // getAssign
    
    var arr_assignList:[UserAssign] = []
    var strSelectedUserAssignID:String = ""
    
    // Department
    
    var arr_departmentList:[Department] = []
    var strSelectedDepartmentID:String = ""
   
    
    //MARK:- LifeCycle
    init(theController:RiskFilterVC) {
        self.theController = theController
    }
    
    func configureDropDown(dropDown:DropDown, view:UIView) {
        dropDown.anchorView = view
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: view.frame.origin.x, y: 40)
        switch dropDown {
        case categoryDD:
            let arr_Categorylist:[String] = arr_categoryList.map({$0.category_name })
            dropDown.dataSource = arr_Categorylist
            break
        case companyDD:
            let arr_Companylist:[String] = arr_companyList.map({$0.name })
            dropDown.dataSource = arr_Companylist
            break       
        
        case assignedToDD:
            let arr_AssignList:[String] = arr_assignList.map({$0.assignee_name })
//            let arr_AssignList:[String] = arr_assignList.map({$0.assign_user_name })
            dropDown.dataSource = arr_AssignList
            break
        case departmentDD:
            let arr_departmentList:[String] = self.arr_departmentList.map({$0.name })
            dropDown.dataSource = arr_departmentList
            break
       
        default:
            break
        }
        
        dropDown.selectionAction = { [unowned self] (index,item) in
            //print("selected item: \(item) at index \(index)")
            switch dropDown {
            case self.categoryDD:
                self.strSelectedCategoryID = self.arr_categoryList[index].id
                break
            case self.companyDD:
                self.strSelectedCompanyID = self.arr_companyList[index].id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.assignedToDD:
//                self.strSelectedUserAssignID = self.arr_assignList[index].id
                self.strSelectedUserAssignID = self.arr_assignList[index].user_id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            case self.departmentDD:
                self.strSelectedDepartmentID = self.arr_departmentList[index].id
//                self.theController.update(text: item, dropDown: dropDown)
                break
            default:
                break
            }
            self.theController.update(text: item, dropDown: dropDown)

        }
    }    
    
}


