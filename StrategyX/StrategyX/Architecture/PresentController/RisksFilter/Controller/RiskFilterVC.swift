//
//  RiskFilterVC.swift
//  StrategyX
//
//  Created by Haresh on 18/01/19.
//  Copyright © 2019 Outsourced Global Limited. All rights reserved.
//

import UIKit
import DropDown

class RiskFilterVC: ParentViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:RiskFilterView = { [unowned self] in
       return self.view as! RiskFilterView
    }()
    
    fileprivate lazy var theCurrentModel:RiskFilterModel = {
       return RiskFilterModel(theController: self)
    }()
    
    
    // handler
    
    var handlerOnFilterSearch:(_ filterData:FilterData) -> Void = {_ in }
    
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        theCurrentView.setupLayout()
        theCurrentView.updateViewAccordingTo(type: theCurrentModel.filterType)
        theCurrentView.constrainViewTop.constant = theCurrentModel.topConstraint
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.categoryDD, view: theCurrentView.viewCategory)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.companyDD, view: theCurrentView.viewCompnay)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.assignedToDD, view: theCurrentView.viewAssignedTo)
        theCurrentModel.configureDropDown(dropDown: theCurrentModel.departmentDD, view: theCurrentView.viewDepartment)

        callInitialWebServiceAccordingToFilterType()
    }
    
    func setTopConstraint(top:CGFloat,filterType:FilterType) {
        theCurrentModel.topConstraint = top
        theCurrentModel.filterType = filterType
    }
    func setTheData(filterData:FilterData) {
        theCurrentModel.filterData = filterData
        theCurrentView.txtSearch.text = filterData.searchText
        theCurrentModel.strSelectedCategoryID = filterData.categoryID
        theCurrentModel.strSelectedCompanyID = filterData.companyID
        theCurrentModel.strSelectedUserAssignID = filterData.assigntoID
        theCurrentModel.strSelectedDepartmentID = filterData.departmentID
    }
    func callInitialWebServiceAccordingToFilterType() {
        if theCurrentModel.filterType == FilterType.focus || theCurrentModel.filterType == FilterType.approvals {
            theCurrentModel.strSelectedCompanyID = "\(Utility.shared.userData.companyId)"
            setCompanyData()
            
        } else if theCurrentModel.filterType == FilterType.myRole {
            // only for search
        } else {
            getCategories()
            setCompanyData()
        }
    }
    func updateCategory() {
        theCurrentModel.categoryDD.dataSource = theCurrentModel.arr_categoryList.map({$0.category_name })
        
        if let index = theCurrentModel.arr_categoryList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCategoryID}) {
            theCurrentView.txtCategory.text = theCurrentModel.arr_categoryList[index].category_name
        }
    }
    func updateCompany() {
        theCurrentModel.companyDD.dataSource = theCurrentModel.arr_companyList.map({$0.name })

        if let index = theCurrentModel.arr_companyList.firstIndex(where: {$0.id == theCurrentModel.strSelectedCompanyID}) {
            theCurrentView.txtCompany.text = theCurrentModel.arr_companyList[index].name
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
        }
    }
    func updateAssignTo() {
        theCurrentModel.assignedToDD.dataSource = theCurrentModel.arr_assignList.map({$0.assignee_name })

        if let index = theCurrentModel.arr_assignList.firstIndex(where: {$0.user_id == theCurrentModel.strSelectedUserAssignID}) {
            theCurrentView.txtAssignedTo.text = theCurrentModel.arr_assignList[index].assignee_name
//            theCurrentView.txtAssignedTo.text = theCurrentModel.arr_assignList[index].assign_user_name
//            departmentListService()
        }
    }
    func updateDepartment() {
        theCurrentModel.departmentDD.dataSource = theCurrentModel.arr_departmentList.map({$0.name })

        if let index = theCurrentModel.arr_departmentList.firstIndex(where: {$0.id == theCurrentModel.strSelectedDepartmentID}) {
            theCurrentView.txtDepartment.text = theCurrentModel.arr_departmentList[index].name
            
        }
    }
    func update(text:String,dropDown:DropDown) {
        switch dropDown {
        case theCurrentModel.categoryDD:
            theCurrentView.txtCategory.text = text
            break
        case theCurrentModel.companyDD:
            theCurrentView.txtCompany.text = text
            theCurrentView.txtAssignedTo.text = ""
            theCurrentView.txtDepartment.text = ""
            theCurrentModel.strSelectedUserAssignID = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.arr_assignList = []
            theCurrentModel.arr_departmentList = []
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
            break
        case theCurrentModel.assignedToDD:
            theCurrentView.txtAssignedTo.text = text
            /*theCurrentView.txtDepartment.text = ""
            theCurrentModel.strSelectedDepartmentID = ""
            theCurrentModel.arr_departmentList = []
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()*/
            break
        case theCurrentModel.departmentDD:
            theCurrentView.txtDepartment.text = text
            break
        default:
            break
        }
        theCurrentView.btnReset(isHidden: false)
    }
    
    
    //MARK:- IBAction
    @IBAction func onBtnDepartmentAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.theCurrentModel.strSelectedCompanyID != "" {
            if self.theCurrentModel.arr_departmentList.count != 0 {
                theCurrentModel.departmentDD.show()
            } else {
                self.departmentListService()
            }
        } else {
             self.showAlertAtBottom(message: "Please select company first")
            
        }
        
    }
    
    @IBAction func onBtnCategoryAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.categoryDD.show()
    }
    
    @IBAction func onBtnAssignToAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue{
            if self.theCurrentModel.strSelectedCompanyID != ""{
                if self.theCurrentModel.arr_assignList.count != 0{
                    theCurrentModel.assignedToDD.show()
                } else {
                    self.assignUserListService()
                }
            } else {
                self.showAlertAtBottom(message: "Please select company first")
            }
        }
        else{
            if self.theCurrentModel.arr_assignList.count != 0{
                theCurrentModel.assignedToDD.show()
            } else {
                self.assignUserListService()
            }
        }
        
    }
    
    @IBAction func btnBtnBackAction(_ sender: Any) {
        self.view.endEditing(true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onBtnResetAction(_ sender: Any) {
        self.view.endEditing(true)
        if UserRole.superAdminUser == UserDefault.shared.userRole {
            if Utility.shared.userData.companyId.isEmpty {
                theCurrentModel.strSelectedCompanyID = ""
                theCurrentView.txtCompany.text = ""
            }
        }
        theCurrentModel.filterData = FilterData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
        handlerOnFilterSearch(theCurrentModel.filterData)
        
        /*if UserRole.companyAdminUser.rawValue == UserDefault.shared.userRole.rawValue || UserRole.staffUser.rawValue == UserDefault.shared.userRole.rawValue{
            theCurrentModel.filterData = FilterData.init(companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "",assigntoID: "0", assigntoName: "")
            handlerOnFilterSearch(theCurrentModel.filterData)
        } else {
            handlerOnFilterSearch(FilterData.init(assigntoID: "0", assigntoName: ""))
        }*/
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBtnCompanyAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.companyDD.show()
    }
    
    @IBAction func btnBtnSubmitAction(_ sender: Any) {
        self.view.endEditing(true)
        theCurrentModel.filterData = FilterData.init(searchText: theCurrentView.txtSearch.text ?? "",categoryID: theCurrentModel.strSelectedCategoryID, categoryName: theCurrentView.txtCategory.text ?? "" , companyID: theCurrentModel.strSelectedCompanyID, companyName: theCurrentView.txtCompany.text ?? "", assigntoID: theCurrentModel.strSelectedUserAssignID, assigntoName: theCurrentView.txtAssignedTo.text ?? "", departmentID: theCurrentModel.strSelectedDepartmentID, departmentName: theCurrentView.txtDepartment.text ?? "")
        handlerOnFilterSearch(theCurrentModel.filterData)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Call API
extension RiskFilterVC {
    func getCategories()
    {
        showLoader(theCurrentView.btnCategoryLoaderOutlet)
        categoiesListService()
    }
    
    func getCompanyList()
    {
        showLoader(theCurrentView.btnCompanyLoaderOutlet)
        companylistService()
        showLoader(theCurrentView.btnAssignLoaderOutlet)
        assignUserListService()
    }
}

//MARK:- Show Loader

extension RiskFilterVC
{
    func showLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(true)
    }
    
    func stopLoader(_ sender:UIButton?)
    {
        sender?.loadingIndicator(false)
    }
    
}

//MARK:- Set Company Data
extension RiskFilterVC{
    func setCompanyData() {
        if UserRole.superAdminUser != UserDefault.shared.userRole {
            theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
            theCurrentView.txtCompany.text = Utility.shared.userData.companyName
            theCurrentView.viewCompnay.isHidden = true
//            theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
//            theCurrentView.viewCompnay.alpha = 0.5
            showLoader(theCurrentView.btnAssignLoaderOutlet)
            assignUserListService()
            showLoader(theCurrentView.btnDepartmentLoaderOutlet)
            departmentListService()
        } else {
            if !(Utility.shared.userData.companyId.isEmpty) {
                theCurrentModel.strSelectedCompanyID = Utility.shared.userData.companyId
                theCurrentView.txtCompany.text = Utility.shared.userData.companyName
                theCurrentView.viewCompnay.isHidden = true

//                theCurrentView.btnSelectCompanyOutlet.isUserInteractionEnabled = false
//                theCurrentView.viewCompnay.alpha = 0.5
                showLoader(theCurrentView.btnAssignLoaderOutlet)
                assignUserListService()
                showLoader(theCurrentView.btnDepartmentLoaderOutlet)
                departmentListService()
                return
            }
            getCompanyList()

        }
        
    }
}

//MARK:- Api Call
extension RiskFilterVC {
    func categoiesListService() {
        let categoryFor = theCurrentModel.filterType == FilterType.risks ? CategoryFor.risk.rawValue : CategoryFor.tacticalProject.rawValue
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_category_for:categoryFor]
        CommanListWebservice.shared.getCategories(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_categoryList = list
            self?.updateCategory()
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.categoryDD)!, view: (self?.theCurrentView.viewCategory)!)
            self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCategoryLoaderOutlet)
        })
    }
    
    func companylistService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id]
        CommanListWebservice.shared.companyList(parameter: parameter, success: { [weak self] (list) in
            self?.theCurrentModel.arr_companyList = list
            self?.updateCompany()
//            if let obj = self {
//            
//            }
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.companyDD)!, view: (self?.theCurrentView.viewCompnay)!)
            self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnCompanyLoaderOutlet)
        })
    }
    
    func assignUserListService() {
        var parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken ,
                         APIKey.key_user_id:Utility.shared.userData.id ,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        
        if theCurrentModel.filterType == FilterType.action {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
        } else if theCurrentModel.filterType == FilterType.focus {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_focuses
        } else if theCurrentModel.filterType == FilterType.risks {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_risks
        } else if theCurrentModel.filterType == FilterType.tacticalProject {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_tactical_projects
        } else if theCurrentModel.filterType == FilterType.approvals {
            parameter[APIKey.key_assign_to_for] = APIKey.key_assign_to_for_action_logs
            parameter[APIKey.key_status] = APIKey.key_assign_to_for_approval
        }
        
        CommanListWebservice.shared.getRiskAssign(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_assignList = list
            self?.updateAssignTo()
            self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
//            if let obj = self {
//                obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.assignedToDD, view: obj.theCurrentView.viewAssignedTo)
//            }
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnAssignLoaderOutlet)
        })
    }
    
    func departmentListService() {
        let parameter = [APIKey.key_access_token:Utility.shared.userData.accessToken,
                         APIKey.key_user_id:Utility.shared.userData.id,
                         APIKey.key_company_id:theCurrentModel.strSelectedCompanyID]
        AddUserWebService.shared.getCompanyDepartment(parameter: parameter as [String : Any], success: { [weak self] (list) in
            self?.theCurrentModel.arr_departmentList = list
            self?.updateDepartment()
            self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
//            if let obj = self {
//                obj.theCurrentModel.configureDropDown(dropDown: obj.theCurrentModel.departmentDD, view: obj.theCurrentView.viewDepartment)
//            }
//            self?.theCurrentModel.configureDropDown(dropDown: (self?.theCurrentModel.departmentDD)!, view: (self?.theCurrentView.viewDepartment)!)
            }, failed: { [weak self] (error) in
                self?.showAlertAtBottom(message: error)
                self?.stopLoader(self?.theCurrentView.btnDepartmentLoaderOutlet)
        })
    }
    
}
